import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';


@Injectable()
export class FirstLoginGuard implements CanActivate {


  constructor(private router: Router) { }

  canActivate() {
    console.log('localStorage isFirstLogin : [' + localStorage.getItem('isFirstLogin') + ']');
    if (localStorage.getItem('isFirstLogin')) {
      console.log('canACTIVATE FIRSTLOGIN TRUE');
      return true;
    }
    console.log('canACTIVATE FIRSTLOGIN FALSE');
    this.router.navigate(['/login']);
    return false;
  }
}
