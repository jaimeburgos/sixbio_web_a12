import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { IObjVerificacion } from 'src/app/nucleo/interface/IObjVerificacion';
import { IValidation } from 'src/app/nucleo/interface/IValidation';
import { IErrorValidador } from 'src/app/nucleo/interface/IErrorValidador';
import { IResponseDatosConsolidado } from 'src/app/nucleo/interface/IResponseDatosConsolidado';
import { IRespuestaServicio } from 'src/app/nucleo/interface/IRespuestaServicio';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { ServicioService } from '../../servicio/servicio.service';
import { IDatosHuellaDerecha } from 'src/app/nucleo/interface/IDatosHuellaDerecha';
import { IDatosHuellaIzquierda } from 'src/app/nucleo/interface/IDatosHuellaIzquierda';
import { Utilitario } from 'src/app/nucleo/util/Utilitario';
import { IReporteValidaciones } from 'src/app/nucleo/interface/IReporteValidaciones';
import { HuellaService } from './serviciosDeUI/huella.service';
import { Router } from '@angular/router';


@Component({
    selector: 'app-verificacion',
    templateUrl: './verificacion.component.html',
    styleUrls: ['./verificacion.component.scss'],
    animations: [routerTransition()]
})
export class VerificacionComponent implements OnInit {
    isWithDniValidator = false;
    footer = Constant.FOOTER_NOVATRONIC;
    messageValidatorByDniValidator: String = '';
    messageValidatorByDniConsultor: String = '';

    isDatosConsolidado = true;
    isDniValidatorValid = false;
    isDniValidator = false;
    dniValidadorLS = 'false';
    data: Object = {};
    isDniConsultorValid = false;

    isValidDedoDerecho = true;
    isValidDedoIzquierdo: boolean;

    isValidMessageServices = false;

    mensageRespuestaServicio: String = '';

    codigoRespuestaServicio: String = '';

    tituloDeTipoHuellaDerecha: String = 'Indice Derecho';

    tituloDeTipoHuellaIzquierdo: String = 'Indice Izquierdo';

    datosHuellaDerecha: IDatosHuellaDerecha = {
        huellaD: '',
        huellaCompaCardD: '',
        huellaWSQD: '',
        huellaTemplateD: '',
        nistD: '',
        huellavivaD: '',
        imagenNistD: '',
        imagenhuellavivaD: ''
    };
    datosHuellaIzquierda: IDatosHuellaIzquierda = {
        huellaI: '',
        huellaCompaCardI: '',
        huellaWSQI: '',
        huellaTemplateI: '',
        nistI: '',
        huellavivaI: '',
        imagenNistI: '',
        imagenhuellavivaI: ''
    };
    datosHuellaDerechaBlank: IDatosHuellaDerecha = {
        huellaCompaCardD: '',
        huellaWSQD: '',
        huellaTemplateD: '',
    };
    datosHuellaIzquierdaBlank: IDatosHuellaIzquierda = {
        huellaCompaCardI: '',
        huellaWSQI: '',
        huellaTemplateI: '',
    };
    datosConsolidadoLimpio: IResponseDatosConsolidado = {
        apellidos: '',
        fechaNacimiento: '',
        constanciaVotacionDesc: '',
        nombreMadre: '',
        codigoUbigeoProvDomicilio: '',
        direccion: '',
        estatura: '',
        constanciaVotacionCodigo: '',
        localidadDomicilio: '',
        provinciaNacimiento: '',
        codigoUbigeoDistNacimiento: '',
        nombres: '',
        restricciones: '',
        caducidadDescripcion: '',
        codigoUbigeoDistDomicilio: '',
        codigoUbigeoProvNacimiento: '',
        nombrePadre: '',
        codigoUbigeoLocalidadNacimiento: '',
        numeroLibro: '',
        localidad: '',
        distritoNacimiento: '',
        ubigeoVotacion: '',
        dni: '',
        codigoUbigeoLocalidadDomicilio: '',
        departamentoDomicilio: '',
        grupoVotacion: '',
        provinciaDomicilio: '',
        restriccionesDesc: '',
        caducidadCodigo: '',
        anioEstudio: '',
        departamentoNacimiento: '',
        numeroDocSustentarioIdentidad: '',
        estadoCivilDescripcion: '',
        fechaInscripcion: '',
        codigoUbigeoDeptoDomicilio: '',
        estadoCivilCodigo: '',
        fechaExpedicion: '',
        tipoDocSustentarioIdentidad: '',
        sexoCodigo: '',
        codigoUbigeoDeptoNacimiento: '',
        sexoDescripcion: '',
        foto: 'assets/images/perfil.png',
        firma: 'assets/images/firma.jpg'
    };

    datosConsolidado: IResponseDatosConsolidado = {
        apellidos: '',
        fechaNacimiento: '',
        constanciaVotacionDesc: '',
        nombreMadre: '',
        codigoUbigeoProvDomicilio: '',
        direccion: '',
        estatura: '',
        constanciaVotacionCodigo: '',
        localidadDomicilio: '',
        provinciaNacimiento: '',
        codigoUbigeoDistNacimiento: '',
        nombres: '',
        restricciones: '',
        caducidadDescripcion: '',
        codigoUbigeoDistDomicilio: '',
        codigoUbigeoProvNacimiento: '',
        nombrePadre: '',
        codigoUbigeoLocalidadNacimiento: '',
        numeroLibro: '',
        localidad: '',
        distritoNacimiento: '',
        ubigeoVotacion: '',
        dni: '',
        codigoUbigeoLocalidadDomicilio: '',
        departamentoDomicilio: '',
        grupoVotacion: '',
        provinciaDomicilio: '',
        restriccionesDesc: '',
        caducidadCodigo: '',
        anioEstudio: '',
        departamentoNacimiento: '',
        numeroDocSustentarioIdentidad: '',
        estadoCivilDescripcion: '',
        fechaInscripcion: '',
        codigoUbigeoDeptoDomicilio: '',
        estadoCivilCodigo: '',
        fechaExpedicion: '',
        tipoDocSustentarioIdentidad: '',
        sexoCodigo: '',
        codigoUbigeoDeptoNacimiento: '',
        sexoDescripcion: '',
        foto: '',
        firma: ''
    };

    foto: String = 'assets/images/perfil.png';
    firma: String = 'assets/images/firma.jpg';
    huellaDerecha: String = 'assets/images/blank.png';
    huellaIzquierda: String = 'assets/images/blank.png';
    nistDerecha: String = '';
    nistIzquierda: String = '';
    huellaVivaIzquierda: String = '';
    huellaVivaDerecha: String = '';
    imagenHuellaVivaIzquierda: String = 'assets/images/blank2.png';
    imagenHuellaVivaDerecha: String = 'assets/images/blank2.png';
    imagenNistIzquierda: String = 'assets/images/blank2.png';
    imagenNistDerecha: String = 'assets/images/blank2.png';
    msgError: String = '';
    siSixSer: string = JSON.parse(localStorage.getItem('datosSesion'))._visualizar;
    siDniValidador: boolean = JSON.parse(localStorage.getItem('datosSesion'))._validarDniValidador;
    entidad: IObjVerificacion = {
        dniValidador: '',
        dniConsultor: '',
        tipoCaptura: 0
    };
    utilitario: Utilitario;
    constructor(private huellaService: HuellaService, private servicioService: ServicioService, private router: Router) {
        this.isWithDniValidator = this.verificatorConDniValidator();
        this.isDatosConsolidado = this.verificatorConSixSer();
        if ( localStorage.getItem('RolUsuario') === 'EMP_ADM') {
            this.router.navigate(['/home']);
        }
    }

    ngOnInit() {
        this.dniValidadorLS = JSON.parse(localStorage.getItem('datosSesion'))._validarDniValidador;
        localStorage.setItem('isWithDniValidator', this.dniValidadorLS);
        this.setIsDniValidatorValid();
        if ( localStorage.getItem('RolUsuario') === 'EMP_ADM') {
            this.router.navigate(['/home']);
        }
    }

    setIsDniValidatorValid() {
        if (localStorage.getItem('isWithDniValidator') !== null && localStorage.getItem('isWithDniValidator') === 'true') {
            this.isDniValidator = true;
        } else {
            this.isDniValidator = false;
        }
    }

    verificatorConDniValidator(): boolean {
        if (this.siDniValidador) {
            return true;
        } else {
            return false;
        }
    }

    verificatorConSixSer(): boolean {
        if (this.siSixSer === 'true') {
            return true;
        } else {
            return false;
        }
    }

    changeOfHtmlByTypCapture() {
        switch (this.entidad.tipoCaptura) {
            case 0:
                this.isValidDedoDerecho = true;
                this.isValidDedoIzquierdo = false;
                break;
            case 1:
                this.isValidDedoDerecho = false;
                this.isValidDedoIzquierdo = true;
                break;
            case 2:
                this.isValidDedoDerecho = true;
                this.isValidDedoIzquierdo = true;
                break;
            default:
                break;
        }
    }

    mostrarFoto(foto: String) {
        this.foto = foto;
    }

    mostrarHuellaDerecha(datos: IDatosHuellaDerecha) {
        this.huellaDerecha = datos.huellaD;
        this.nistDerecha = datos.nistD;
        this.huellaVivaDerecha = datos.huellavivaD;
        this.imagenNistDerecha = datos.imagenNistD;
        this.imagenHuellaVivaDerecha = datos.imagenhuellavivaD;
    }

    mostrarHuellaIzquierda(datos: IDatosHuellaIzquierda) {
        this.huellaIzquierda = datos.huellaI;
        this.nistIzquierda = datos.nistI;
        this.huellaVivaIzquierda = datos.huellavivaI;
        this.imagenNistIzquierda = datos.imagenNistI;
        this.imagenHuellaVivaIzquierda = datos.imagenhuellavivaI;
    }

    mostrarMensajeError(error: IErrorValidador) {
        if (error.tipoInput === 'dniValidador') {
            this.messageValidatorByDniValidator = error.mensajeError;
            this.isDniValidatorValid = error.banderaError;
        } else if (error.tipoInput === 'dniConsultor') {
            this.messageValidatorByDniConsultor = error.mensajeError;
            this.isDniConsultorValid = error.banderaError;
            // this.toast.addToast({ title: 'Error', msg: Constant.MENSAJE_ERROR_CONECTIVIDAD, type: 'error' });
        } else {
            this.messageValidatorByDniValidator = error.mensajeError;
            this.isDniValidatorValid = error.banderaError;
            this.messageValidatorByDniConsultor = error.mensajeError;
            this.isDniConsultorValid = error.banderaError;
        }
    }

    respuestaServicio(respuesta: IRespuestaServicio) {

        if (respuesta.codigoRespuesta === Constant.CODIGO_VACIO && respuesta.mensajeRespuesta === Constant.MENSAJE_VACIO) {
            this.cleanMensajes();
        } else if (respuesta.mensajeRespuesta === 'cleanFront') {
/*             console.log('REALIZADO LIMPIEZA FRONT'); */
            this.messageValidatorByDniValidator = '';
            this.codigoRespuestaServicio = '';
            this.cleanMensajes();
            this.isValidMessageServices = false;
            this.isDniConsultorValid = false;
            this.clearDatosConsolidado();
        } else if (respuesta.codigoRespuesta === Constant.CODIGO_VACIO && respuesta.mensajeRespuesta !== Constant.MENSAJE_VACIO) {
            this.mensageRespuestaServicio = respuesta.mensajeRespuesta;
            this.isValidMessageServices = true;
        } else if (respuesta.codigoRespuesta === Constant.CODIGO_OPERACION_ERROR) {
            this.codigoRespuestaServicio = respuesta.codigoRespuesta;
            this.mensageRespuestaServicio = respuesta.mensajeRespuesta;
            this.isValidMessageServices = true;
            this.clearDatosConsolidado();
            this.cleanImagen();
            this.cleanFirma();
        } else {
            this.codigoRespuestaServicio = respuesta.codigoRespuesta;
            this.mensageRespuestaServicio = respuesta.mensajeRespuesta;
            this.isValidMessageServices = true;
        }

    }

    cambiarTituloDeTipoHuella(data: any) {
        this.tituloDeTipoHuellaDerecha = data.descripcionTipoHuellaDerecha;
        this.tituloDeTipoHuellaIzquierdo = data.descripcionTipoHuellaIzquierda;
    }


    mostrarDatosConsolidado(datos: IResponseDatosConsolidado) {
/*         this.limpiarDatos(); */
        this.mostrarFoto(datos.foto);
        this.mostrarFirma(datos.firma);
        this.mostrarDatosPersona(datos);
    }

    mostrarMensajeCaptura(mensaje: IRespuestaServicio) {

    }

    mostrarFirma(firma: String) {
        this.firma = firma;
    }

    mostrarDatosPersona(datos: IResponseDatosConsolidado) {
        this.datosConsolidado = datos;
    }

    // CLEAR EVERYTHING //

    cleanMensajes() {
        this.codigoRespuestaServicio = '';
        this.mensageRespuestaServicio = '';
        this.isValidMessageServices = false;
        this.clearDatosConsolidado();
        this.cleanImagen();
        this.cleanFirma();

    }
    limpiarTodo() {
        localStorage.removeItem('dataResponseMejorHuella');
        /* this.entidad.dniConsultor = '';
        this.entidad.dniValidador = ''; */
        this.codigoRespuestaServicio = '';
        this.mensageRespuestaServicio = '';
        this.messageValidatorByDniValidator = '';
        this.isValidMessageServices = false;
        this.isDniConsultorValid = false;
        this.desHabilitarBotones();
        this.clearDatosConsolidado();
        this.cleanImagen();
        this.cleanFirma();
        this.cleanHuella();
        this.servicioService.serviceEndCapturaDactilar(this.data).subscribe((dataR: any) => {});
    }
    limpiarDatos() {
        this.codigoRespuestaServicio = '';
        this.mensageRespuestaServicio = '';
        this.messageValidatorByDniValidator = '';
        this.isValidMessageServices = false;
        this.isDniConsultorValid = false;
        this.clearDatosConsolidado();
    }

    desHabilitarBotones() {
        localStorage.setItem('botonVerificar', 'false');
        localStorage.setItem('botonMOC', 'false');
        localStorage.setItem('botonFoto', 'false');
        localStorage.setItem('botonDatos', 'false');
    }

    clearDatosConsolidado() {
        this.datosConsolidado.apellidos = '';
        this.datosConsolidado.fechaNacimiento = '';
        this.datosConsolidado.constanciaVotacionDesc = '';
        this.datosConsolidado.nombreMadre = '';
        this.datosConsolidado.codigoUbigeoProvDomicilio = '';
        this.datosConsolidado.direccion = '';
        this.datosConsolidado.estatura = '';
        this.datosConsolidado.constanciaVotacionCodigo = '';
        this.datosConsolidado.localidadDomicilio = '';
        this.datosConsolidado.provinciaNacimiento = '';
        this.datosConsolidado.codigoUbigeoDistNacimiento = '';
        this.datosConsolidado.nombres = '';
        this.datosConsolidado.restricciones = '';
        this.datosConsolidado.caducidadDescripcion = '';
        this.datosConsolidado.codigoUbigeoDistDomicilio = '';
        this.datosConsolidado.codigoUbigeoProvNacimiento = '';
        this.datosConsolidado.nombrePadre = '';
        this.datosConsolidado.codigoUbigeoLocalidadNacimiento = '';
        this.datosConsolidado.numeroLibro = '';
        this.datosConsolidado.localidad = '';
        this.datosConsolidado.distritoNacimiento = '';
        this.datosConsolidado.ubigeoVotacion = '';
        this.datosConsolidado.dni = '';
        this.datosConsolidado.codigoUbigeoLocalidadDomicilio = '';
        this.datosConsolidado.departamentoDomicilio = '';
        this.datosConsolidado.grupoVotacion = '';
        this.datosConsolidado.provinciaDomicilio = '';
        this.datosConsolidado.restriccionesDesc = '';
        this.datosConsolidado.caducidadCodigo = '';
        this.datosConsolidado.anioEstudio = '';
        this.datosConsolidado.departamentoNacimiento = '';
        this.datosConsolidado.numeroDocSustentarioIdentidad = '';
        this.datosConsolidado.estadoCivilDescripcion = '';
        this.datosConsolidado.fechaInscripcion = '';
        this.datosConsolidado.codigoUbigeoDeptoDomicilio = '';
        this.datosConsolidado.estadoCivilCodigo = '';
        this.datosConsolidado.fechaExpedicion = '';
        this.datosConsolidado.tipoDocSustentarioIdentidad = '';
        this.datosConsolidado.sexoCodigo = '';
        this.datosConsolidado.codigoUbigeoDeptoNacimiento = '';
        this.datosConsolidado.sexoDescripcion = '';
        this.datosConsolidado.foto = 'assets/images/perfil.png';
        this.datosConsolidado.firma = 'assets/images/firma.jpg';


    }

    cleanImagen() {
        this.foto = 'assets/images/perfil.png';
    }

    cleanFirma() {
        this.firma = 'assets/images/firma.jpg';
    }

    cleanHuella() {
        this.huellaDerecha = 'assets/images/blank.png';
        this.nistDerecha = '';
        this.huellaVivaDerecha = '';
        this.imagenNistDerecha = 'assets/images/blank2.png';
        this.imagenHuellaVivaDerecha = 'assets/images/blank2.png';
        this.huellaIzquierda = 'assets/images/blank.png';
        this.nistIzquierda = '';
        this.huellaVivaIzquierda = '';
        this.imagenNistIzquierda = 'assets/images/blank2.png';
        this.imagenHuellaVivaIzquierda = 'assets/images/blank2.png';
        this.huellaService.agregarHuellaDerecha(this.datosHuellaDerechaBlank);
        this.huellaService.agregarHuellaIzquierda(this.datosHuellaIzquierdaBlank);
    }
    cambioDNIValidador() {
        this.isDniConsultorValid = false;
    }
}

