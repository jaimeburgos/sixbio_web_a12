import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { IDatosHuellaDerecha } from 'src/app/nucleo/interface/IDatosHuellaDerecha';
import { IDatosHuellaIzquierda } from 'src/app/nucleo/interface/IDatosHuellaIzquierda';

@Injectable({
  providedIn: 'root'
})
export class HuellaService {

  public readonly huellaDerecha: Subject<IDatosHuellaDerecha> = new Subject();
  public readonly huellaIzquierda: Subject<IDatosHuellaIzquierda> = new Subject();

  constructor() { }

  agregarHuellaDerecha(huellaDerecha: IDatosHuellaDerecha) {
    this.huellaDerecha.next(huellaDerecha);
  }

  agregarHuellaIzquierda(huellaIzquierda: IDatosHuellaIzquierda) {
    this.huellaIzquierda.next(huellaIzquierda);
  }

}
