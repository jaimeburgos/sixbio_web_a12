import { Component, OnInit, EventEmitter, Output , Input, ViewChild } from '@angular/core';
import { ServicioService } from '../../../../servicio/servicio.service';
import { Constant } from '../../../../nucleo/constante/Constant';
import { IErrorValidador } from 'src/app/nucleo/interface/IErrorValidador';
import { IRespuestaServicio } from 'src/app/nucleo/interface/IRespuestaServicio';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-foto',
  templateUrl: './foto.component.html',
  styleUrls: ['./foto.component.scss']
})
export class FotoComponent implements OnInit {

    @ViewChild(ToastComponent, {static: true}) toast: ToastComponent;

    @Output()
    propagarFoto = new EventEmitter<String>();

    @Output()
    propagarMensajeError = new EventEmitter<IErrorValidador>();

    @Output()
    propagarRespuestaServicio = new EventEmitter<IRespuestaServicio>();

    @Input()
    dniConsultor;

    @Input()
    dniValidador;

    errorValidador: IErrorValidador = {
        banderaError: false,
        mensajeError: '',
        tipoInput: ''
    };

    respuestaServicio: IRespuestaServicio = {
        codigoRespuesta: '',
        mensajeRespuesta: ''
    };

      constructor(private router: Router, private rutaActual: ActivatedRoute, private servicioService: ServicioService) { }

      dataRequest: Object = {numeroDocumentoPersona: '48287359'};

      regexpNumber = new RegExp ('^[+0-9]{8}$');

      ngOnInit() {
      }

      setDataObtenerFoto(dniConsultor: String) {
        return {
            numeroDocumentoPersona: dniConsultor
        };
      }

      obtenerFoto() {
        this.desHabilitarBoton();
        this.setRespuestaServicio(null, 'cleanFront');
        this.propagarRespuestaServicio.emit(this.respuestaServicio);
        this.setErrorValidador(false, Constant.MENSAJE_VACIO, 'dniValidador');
        this.propagarMensajeError.emit(this.errorValidador);

            if (localStorage.getItem('isWithDniValidator') !== null && localStorage.getItem('isWithDniValidator') === 'true') {


                if (this.dniValidador.length === 0) {
                    this.setErrorValidador(true, Constant.MENSAJE_ERROR_DNI_VALIDADOR_VACIO, 'dniValidador');
                    this.propagarMensajeError.emit(this.errorValidador);
                    return;
                } else if (!this.regexpNumber.test(this.dniValidador)) {
                    this.setErrorValidador(true, Constant.MENSAJE_ERROR_DNI_VALIDADOR_INVALIDO, 'dniValidador');
                    this.propagarMensajeError.emit(this.errorValidador);
                    return;
                }
            }

            this.setErrorValidador(false, Constant.MENSAJE_VACIO, 'dniConsultor');
            this.propagarMensajeError.emit(this.errorValidador);

            if (this.regexpNumber.test(this.dniConsultor) ) {
                    // desactivar el boton foto
                    this.dataRequest = this.setDataObtenerFoto(this.dniConsultor);
                    this.setRespuestaServicio('', Constant.MENSAJE_ESPERA_OBTENER_FOTO);
                    this.propagarRespuestaServicio.emit(this.respuestaServicio);
                    this.servicioService.serviceObtenerFoto(this.dataRequest).subscribe(
                        (result: any) => {
                          if (result.codigoRespuesta === Constant.COD_ERROR_SESION) {
                            this.salir();
                            return;
                        }
                            // eslint-disable-next-line max-len
                            if ((result.hasOwnProperty('foto') && result.foto !== null) && result.codigoRespuesta === '0000' ) {
                                this.propagarFoto.emit('data:image/png;base64,' + result.foto);
                                this.setErrorValidador(false, '', 'OK');
                                this.propagarMensajeError.emit(this.errorValidador);

                                this.setRespuestaServicio(Constant.CODIGO_OPERACION_EXITOSA, Constant.MENSAJE_OPERACION_FOTO_EXITOSA);
                                this.propagarRespuestaServicio.emit(this.respuestaServicio);
                                this.habilitarBotonSi();

                            } else {
                                this.setRespuestaServicio(Constant.CODIGO_OPERACION_ERROR, Constant.MENSAJE_OPERACION_ERROR);
                                this.propagarRespuestaServicio.emit(this.respuestaServicio);
                            }

                        }, error => {
                            this.setRespuestaServicio('', Constant.MENSAJE_VACIO);
                            this.propagarRespuestaServicio.emit(this.respuestaServicio);
                            this.toast.addToast({ title: 'Error', msg: Constant.MENSAJE_ERROR_CONECTIVIDAD, type: 'error' });
                            setTimeout(() => {this.salir(); }, 500);
                          }
                    );


            } else if (this.dniConsultor.length === 0) {
                    this.setErrorValidador(true, Constant.MENSAJE_ERROR_DNI_CONSULTOR_VACIO, 'dniConsultor');
                    this.propagarMensajeError.emit(this.errorValidador);
            } else {
                    this.setErrorValidador(true, Constant.MENSAJE_ERROR_DNI_CONSULTOR_INVALIDO, 'dniConsultor');
                    this.propagarMensajeError.emit(this.errorValidador);
            }


        }

      setErrorValidador(bandera: boolean, msgError: String, tipoInput: String) {
        this.errorValidador.banderaError = bandera;
        this.errorValidador.mensajeError = msgError;
        this.errorValidador.tipoInput = tipoInput;
      }

      setRespuestaServicio(codigo: String, mensaje: String) {
        this.respuestaServicio.codigoRespuesta = codigo;
        this.respuestaServicio.mensajeRespuesta = mensaje;
      }
      habilitarBoton() {
        if (localStorage.getItem('botonFoto') === 'true') {
         return true;
        } else {
          return false;
        }
      }
      habilitarBotonSi() {
        localStorage.setItem('true', 'true');
      }
      desHabilitarBoton() {
        localStorage.setItem('true', 'false');
      }
      salir() {
        this.servicioService.serviceSignOff(this.dataRequest).subscribe((dataResponse: any) => {

            if (dataResponse.codigoRespuesta === Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO) {
                localStorage.clear();
                this.router.navigate(['/login']);
            } else {

                localStorage.clear();
                this.router.navigate(['/login']);

            }
        }
            , error => {
                localStorage.clear();
                this.router.navigate(['/login']);
            });
    }
}
