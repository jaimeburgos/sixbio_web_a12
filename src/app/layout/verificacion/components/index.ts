export * from './foto/foto.component';
export * from './datos/datos.component';
export * from './mejor-huella/mejor-huella.component';
export * from './capturar/capturar.component';
export * from './verificar/verificar.component';
export * from './verificarmoc/verificarmoc.component';
