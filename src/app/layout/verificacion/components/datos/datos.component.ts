import { Component, OnInit , EventEmitter, Output , Input, ViewChild} from '@angular/core';
import { IErrorValidador } from 'src/app/nucleo/interface/IErrorValidador';
import { IRespuestaServicio } from 'src/app/nucleo/interface/IRespuestaServicio';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { Constant } from '../../../../nucleo/constante/Constant';
import { IResponseDatosConsolidado } from 'src/app/nucleo/interface/IResponseDatosConsolidado';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-datos',
  templateUrl: './datos.component.html',
  styleUrls: ['./datos.component.scss']
})
export class DatosComponent implements OnInit {

    @ViewChild(ToastComponent, {static: true}) toast: ToastComponent;

    @Input()
    dniConsultor;

    @Input()
    dniValidador;

    @Output()
    propagarDatos = new EventEmitter<IResponseDatosConsolidado>();

    @Output()
    propagarMensajeError = new EventEmitter<IErrorValidador>();

    @Output()
    propagarRespuestaServicio = new EventEmitter<IRespuestaServicio>();

    errorValidador: IErrorValidador = {
        banderaError: false,
        mensajeError: '',
        tipoInput: ''
    };

    respuestaServicio: IRespuestaServicio = {
        codigoRespuesta: '',
        mensajeRespuesta: ''
    };
    respuesta = {

    };
    foto: String = 'assets/images/perfil.png';
    firma: String = 'assets/images/firma.jpg';
    regexpNumber = new RegExp ('^[+0-9]{8}$');

    dataRequest: Object = {numeroDocumentoPersona: ''};
    object = {};
    datosConsolidadoLimpio: IResponseDatosConsolidado = {
        apellidos: '',
        fechaNacimiento: '',
        constanciaVotacionDesc: '',
        nombreMadre: '',
        codigoUbigeoProvDomicilio: '',
        direccion: '',
        estatura: '',
        constanciaVotacionCodigo: '',
        localidadDomicilio: '',
        provinciaNacimiento: '',
        codigoUbigeoDistNacimiento: '',
        nombres: '',
        restricciones: '',
        caducidadDescripcion: '',
        codigoUbigeoDistDomicilio: '',
        codigoUbigeoProvNacimiento: '',
        nombrePadre: '',
        codigoUbigeoLocalidadNacimiento: '',
        numeroLibro: '',
        localidad: '',
        distritoNacimiento: '',
        ubigeoVotacion: '',
        dni: '',
        codigoUbigeoLocalidadDomicilio: '',
        departamentoDomicilio: '',
        grupoVotacion: '',
        provinciaDomicilio: '',
        restriccionesDesc: '',
        caducidadCodigo: '',
        anioEstudio: '',
        departamentoNacimiento: '',
        numeroDocSustentarioIdentidad: '',
        estadoCivilDescripcion: '',
        fechaInscripcion: '',
        codigoUbigeoDeptoDomicilio: '',
        estadoCivilCodigo: '',
        fechaExpedicion: '',
        tipoDocSustentarioIdentidad: '',
        sexoCodigo: '',
        codigoUbigeoDeptoNacimiento: '',
        sexoDescripcion: '',
        foto: '',
        firma: ''
    };

    datosConsolidado: IResponseDatosConsolidado = {
        apellidos: '',
        fechaNacimiento: '',
        constanciaVotacionDesc: '',
        nombreMadre: '',
        codigoUbigeoProvDomicilio: '',
        direccion: '',
        estatura: '',
        constanciaVotacionCodigo: '',
        localidadDomicilio: '',
        provinciaNacimiento: '',
        codigoUbigeoDistNacimiento: '',
        nombres: '',
        restricciones: '',
        caducidadDescripcion: '',
        codigoUbigeoDistDomicilio: '',
        codigoUbigeoProvNacimiento: '',
        nombrePadre: '',
        codigoUbigeoLocalidadNacimiento: '',
        numeroLibro: '',
        localidad: '',
        distritoNacimiento: '',
        ubigeoVotacion: '',
        dni: '',
        codigoUbigeoLocalidadDomicilio: '',
        departamentoDomicilio: '',
        grupoVotacion: '',
        provinciaDomicilio: '',
        restriccionesDesc: '',
        caducidadCodigo: '',
        anioEstudio: '',
        departamentoNacimiento: '',
        numeroDocSustentarioIdentidad: '',
        estadoCivilDescripcion: '',
        fechaInscripcion: '',
        codigoUbigeoDeptoDomicilio: '',
        estadoCivilCodigo: '',
        fechaExpedicion: '',
        tipoDocSustentarioIdentidad: '',
        sexoCodigo: '',
        codigoUbigeoDeptoNacimiento: '',
        sexoDescripcion: '',
        foto: '',
        firma: ''
    };

    constructor(private router: Router, private rutaActual: ActivatedRoute, private servicioService: ServicioService) { }

    ngOnInit() {
    }

    obtenerDatos() {
        this.desHabilitarBoton();
        this.setRespuestaServicio(null, 'cleanFront');
        this.propagarRespuestaServicio.emit(this.respuestaServicio);
        this.setErrorValidador(false, Constant.MENSAJE_VACIO, 'dniValidador');
        this.propagarMensajeError.emit(this.errorValidador);

        if (localStorage.getItem('isWithDniValidator') !== null && localStorage.getItem('isWithDniValidator') === 'true') {
            if (this.dniValidador.length === 0) {
                this.setErrorValidador(true, Constant.MENSAJE_ERROR_DNI_VALIDADOR_VACIO, 'dniValidador');
                this.propagarMensajeError.emit(this.errorValidador);
                this.habilitarBotonSi();
                return;
            } else if (!this.regexpNumber.test(this.dniValidador)) {
                this.setErrorValidador(true, Constant.MENSAJE_ERROR_DNI_VALIDADOR_INVALIDO, 'dniValidador');
                this.propagarMensajeError.emit(this.errorValidador);
                this.habilitarBotonSi();
                return;
            }
        }
        this.setErrorValidador(false, Constant.MENSAJE_VACIO, 'dniConsultor');
        this.propagarMensajeError.emit(this.errorValidador);

        if (this.regexpNumber.test(this.dniConsultor) ) {

                this.dataRequest = this.setDataRequestDatos(this.dniConsultor);
                this.setRespuestaServicio('', Constant.MENSAJE_ESPERA_OBTENER_CONSOLIDADO);
                this.propagarRespuestaServicio.emit(this.respuestaServicio);

                this.servicioService.serviceObtenerConsolidado(this.dataRequest).subscribe(
                    (result: any) => {
                        this.desHabilitarBoton();
                        this.clearAll();
                        // eslint-disable-next-line max-len
                        if (Object.entries(result.datoPersona).length !== 0 && result.codigoRespuesta === '0000' ) {
                            this.habilitarBotonSi();

                            this.setDatosResponseWithObjtDatosConsolidado(result);
                            this.propagarDatos.emit(this.datosConsolidado);

                            this.setErrorValidador(false, '', 'OK');
                            this.propagarMensajeError.emit(this.errorValidador);

                            this.setRespuestaServicio(Constant.CODIGO_OPERACION_EXITOSA, Constant.MENSAJE_OPERACION_CONSOLIDADO_EXITOSA);
                            this.propagarRespuestaServicio.emit(this.respuestaServicio);
                            this.habilitarBotonSi();

                        } if (result.codigoRespuesta === Constant.COD_ERROR_SESION) {
                            this.salir();
                            return;
                        } else {
                            this.setRespuestaServicio(Constant.CODIGO_OPERACION_ERROR, Constant.MENSAJE_OPERACION_DATOS);
                            this.propagarRespuestaServicio.emit(this.respuestaServicio);
                            this.clearAll();
                            this.habilitarBotonSi();
                        }
                    }, error => {
                        this.setRespuestaServicio('', Constant.MENSAJE_VACIO);
                        this.propagarRespuestaServicio.emit(this.respuestaServicio);
                        this.clearAll();
                        this.habilitarBotonSi();
                        this.toast.addToast({ title: 'Error', msg: Constant.MENSAJE_ERROR_CONECTIVIDAD, type: 'error' });
                        setTimeout(() => {this.salir(); }, 500);
                    }
                );

        } else if (this.dniConsultor.length === 0) {
                this.setErrorValidador(true, Constant.MENSAJE_ERROR_DNI_CONSULTOR_VACIO, 'dniConsultor');
                this.propagarMensajeError.emit(this.errorValidador);
                this.habilitarBotonSi();
        } else {
                this.setErrorValidador(true, Constant.MENSAJE_ERROR_DNI_CONSULTOR_INVALIDO, 'dniConsultor');
                this.propagarMensajeError.emit(this.errorValidador);
                this.habilitarBotonSi();
        }

    }

    setDataRequestDatos(dniConsultor: string) {
        return {
            numeroDocumentoPersona: dniConsultor
        };
    }

    setErrorValidador(bandera: boolean, msgError: string, tipoInput: string) {
        this.errorValidador.banderaError = bandera;
        this.errorValidador.mensajeError = msgError;
        this.errorValidador.tipoInput = tipoInput;
    }

    setRespuestaServicio(codigo: string, mensaje: string) {
        this.respuestaServicio.codigoRespuesta = codigo;
        this.respuestaServicio.mensajeRespuesta = mensaje;
    }

    setDatosResponseWithObjtDatosConsolidado(response: any): void {
        this.datosConsolidado.apellidos = response.datoPersona.apellidoPaterno + ' ' + response.datoPersona.apellidoMaterno;
        this.datosConsolidado.fechaNacimiento = this.formatearFecha(response.datoPersona.fechaNacimiento);
        this.datosConsolidado.constanciaVotacionDesc = response.datoPersona.constanciaVotacionDesc;
        this.datosConsolidado.nombreMadre = response.datoPersona.nombreMadre;
        this.datosConsolidado.codigoUbigeoProvDomicilio = response.datoPersona.codigoUbigeoProvDomicilio;
        this.datosConsolidado.direccion = response.datoPersona.direccionDomicilio + ' / ' + response.datoPersona.distritoDomicilio;
        this.datosConsolidado.estatura = response.datoPersona.estatura;
        this.datosConsolidado.constanciaVotacionCodigo = response.datoPersona.constanciaVotacionCodigo;
        this.datosConsolidado.localidadDomicilio = response.datoPersona.localidadDomicilio;
        this.datosConsolidado.provinciaNacimiento = response.datoPersona.provinciaNacimiento;
        this.datosConsolidado.codigoUbigeoDistNacimiento = response.datoPersona.codigoUbigeoDistNacimiento;
        this.datosConsolidado.nombres = response.datoPersona.nombres;
        this.datosConsolidado.restricciones = response.datoPersona.restricciones;
        this.datosConsolidado.caducidadDescripcion = response.datoPersona.caducidadDescripcion;
        this.datosConsolidado.codigoUbigeoDistDomicilio = response.datoPersona.codigoUbigeoDistDomicilio;
        this.datosConsolidado.codigoUbigeoProvNacimiento = response.datoPersona.codigoUbigeoProvNacimiento;
        this.datosConsolidado.nombrePadre = response.datoPersona.nombrePadre;
        this.datosConsolidado.codigoUbigeoLocalidadNacimiento = response.datoPersona.codigoUbigeoLocalidadNacimiento;
        this.datosConsolidado.numeroLibro = response.datoPersona.numeroLibro;
        this.datosConsolidado.localidad = response.datoPersona.localidad;
        this.datosConsolidado.distritoNacimiento = response.datoPersona.distritoNacimiento;
        this.datosConsolidado.ubigeoVotacion = response.datoPersona.ubigeoVotacion;
        this.datosConsolidado.dni = response.datoPersona.dni;
        this.datosConsolidado.codigoUbigeoLocalidadDomicilio = response.datoPersona.codigoUbigeoLocalidadDomicilio;
        this.datosConsolidado.departamentoDomicilio = response.datoPersona.departamentoDomicilio;
        this.datosConsolidado.grupoVotacion = response.datoPersona.grupoVotacion;
        this.datosConsolidado.provinciaDomicilio = response.datoPersona.provinciaDomicilio;
        this.datosConsolidado.restriccionesDesc = response.datoPersona.restriccionesDesc;
        this.datosConsolidado.caducidadCodigo = response.datoPersona.caducidadCodigo;
        this.datosConsolidado.anioEstudio = response.datoPersona.anioEstudio;
        this.datosConsolidado.departamentoNacimiento = response.datoPersona.departamentoNacimiento;
        this.datosConsolidado.numeroDocSustentarioIdentidad = response.datoPersona.numeroDocSustentarioIdentidad;
        this.datosConsolidado.estadoCivilDescripcion = response.datoPersona.estadoCivilDescripcion;
        this.datosConsolidado.fechaInscripcion = this.formatearFecha(response.datoPersona.fechaInscripcion);
        this.datosConsolidado.codigoUbigeoDeptoDomicilio = response.datoPersona.codigoUbigeoDeptoDomicilio;
        this.datosConsolidado.estadoCivilCodigo = response.datoPersona.estadoCivilCodigo;
        this.datosConsolidado.fechaExpedicion = this.formatearFecha(response.datoPersona.fechaExpedicion);
        this.datosConsolidado.tipoDocSustentarioIdentidad = response.datoPersona.tipoDocSustentarioIdentidad;
        this.datosConsolidado.tipoDocSustentarioIdentidad = response.datoPersona.tipoDocSustentarioIdentidad;
        this.datosConsolidado.sexoCodigo = response.datoPersona.sexoCodigo;
        this.datosConsolidado.codigoUbigeoDeptoNacimiento = response.datoPersona.codigoUbigeoDeptoNacimiento;
        this.datosConsolidado.sexoDescripcion = response.datoPersona.sexoDescripcion;
        this.datosConsolidado.foto = 'data:image/png;base64,' + response.foto;
        this.datosConsolidado.firma = 'data:image/png;base64,' + response.firma;
    }

    formatearFecha(fecha: string): string {
        const fechaSinFormateada = fecha;
        const fechaFormateada = fechaSinFormateada.split(' ').join('-');
        return fechaFormateada;
    }
    habilitarBoton() {
        if (localStorage.getItem('botonDatos') === 'true') {
         return true;
        } else {
          return false;
        }
      }

      habilitarBotonSi() {
        localStorage.setItem('botonDatos', 'true');
    }

      desHabilitarBoton() {
          localStorage.setItem('botonDatos', 'false');
      }

    clearAll() {
        this.clearDatosConsolidado();
        this.cleanFirma();
        this.cleanImagen();
    }
      clearDatosConsolidado() {
        this.datosConsolidado.apellidos = '';
        this.datosConsolidado.fechaNacimiento = '';
        this.datosConsolidado.constanciaVotacionDesc = '';
        this.datosConsolidado.nombreMadre = '';
        this.datosConsolidado.codigoUbigeoProvDomicilio = '';
        this.datosConsolidado.direccion = '';
        this.datosConsolidado.estatura = '';
        this.datosConsolidado.constanciaVotacionCodigo = '';
        this.datosConsolidado.localidadDomicilio = '';
        this.datosConsolidado.provinciaNacimiento = '';
        this.datosConsolidado.codigoUbigeoDistNacimiento = '';
        this.datosConsolidado.nombres = '';
        this.datosConsolidado.restricciones = '';
        this.datosConsolidado.caducidadDescripcion = '';
        this.datosConsolidado.codigoUbigeoDistDomicilio = '';
        this.datosConsolidado.codigoUbigeoProvNacimiento = '';
        this.datosConsolidado.nombrePadre = '';
        this.datosConsolidado.codigoUbigeoLocalidadNacimiento = '';
        this.datosConsolidado.numeroLibro = '';
        this.datosConsolidado.localidad = '';
        this.datosConsolidado.distritoNacimiento = '';
        this.datosConsolidado.ubigeoVotacion = '';
        this.datosConsolidado.dni = '';
        this.datosConsolidado.codigoUbigeoLocalidadDomicilio = '';
        this.datosConsolidado.departamentoDomicilio = '';
        this.datosConsolidado.grupoVotacion = '';
        this.datosConsolidado.provinciaDomicilio = '';
        this.datosConsolidado.restriccionesDesc = '';
        this.datosConsolidado.caducidadCodigo = '';
        this.datosConsolidado.anioEstudio = '';
        this.datosConsolidado.departamentoNacimiento = '';
        this.datosConsolidado.numeroDocSustentarioIdentidad = '';
        this.datosConsolidado.estadoCivilDescripcion = '';
        this.datosConsolidado.fechaInscripcion = '';
        this.datosConsolidado.codigoUbigeoDeptoDomicilio = '';
        this.datosConsolidado.estadoCivilCodigo = '';
        this.datosConsolidado.fechaExpedicion = '';
        this.datosConsolidado.tipoDocSustentarioIdentidad = '';
        this.datosConsolidado.sexoCodigo = '';
        this.datosConsolidado.codigoUbigeoDeptoNacimiento = '';
        this.datosConsolidado.sexoDescripcion = '';
        this.datosConsolidado.foto = 'assets/images/perfil.png';
        this.datosConsolidado.firma = 'assets/images/firma.jpg';
        this.propagarDatos.emit(this.datosConsolidado);
    }

    cleanImagen() {
        this.foto = 'assets/images/perfil.png';
    }

    cleanFirma() {
        this.firma = 'assets/images/firma.jpg';
    }
    salir() {
        this.servicioService.serviceSignOff(this.dataRequest).subscribe((dataResponse: any) => {

            if (dataResponse.codigoRespuesta === Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO) {
                localStorage.clear();
                this.router.navigate(['/login']);
            } else {

                localStorage.clear();
                this.router.navigate(['/login']);

            }
        }
            , error => {
                localStorage.clear();
                this.router.navigate(['/login']);
            });
    }

}

