import { Component, OnInit, Input, Output, ViewChild, EventEmitter } from '@angular/core';
import { ServicioService } from '../../../../servicio/servicio.service';
import { IErrorValidador } from 'src/app/nucleo/interface/IErrorValidador';
import { IRespuestaServicio } from 'src/app/nucleo/interface/IRespuestaServicio';
import { ButtonDirective } from 'src/app/my-directive/directive/button/button.directive';
import { VerificacionComponent } from '../../verificacion.component';
import { IObjVerificacion } from 'src/app/nucleo/interface/IObjVerificacion';
import { Constant } from '../../../../nucleo/constante/Constant';
import { Router } from '@angular/router';
import { IDatosHuellaDerecha } from 'src/app/nucleo/interface/IDatosHuellaDerecha';
import { IDatosHuellaIzquierda } from 'src/app/nucleo/interface/IDatosHuellaIzquierda';
import { HuellaService } from '../../serviciosDeUI/huella.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { HeaderComponent } from 'src/app/layout/components/header/header.component';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';

@Component({
    selector: 'app-capturar',
    templateUrl: './capturar.component.html',
    styleUrls: ['./capturar.component.scss']
})
export class CapturarComponent implements OnInit {
    constructor(

        private servicioService: ServicioService,
        private router: Router,
        private huellaService: HuellaService,
    ) {
        this.constant = new Constant();
/*         this.verificacionComponent = new VerificacionComponent(); */
    }
    wrongImage: String = 'assets/images/wrong.png';
    rightImage: String = 'assets/images/right.png';
    nullImage: String = 'assets/images/blank2.png';
    nullHuella = 'assets/images/blank.png';
    warningImage: String = 'assets/images/warning.png';
    constant: Constant;
    verificacionComponent: VerificacionComponent;
    siHabilitacion = true;
    @ViewChild(ButtonDirective, { static: true })
    buttonDirective = null;
    respuestaServicio: IRespuestaServicio = {
        codigoRespuesta: '',
        mensajeRespuesta: ''
    };
    valorNuloAgente = 'R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
    @ViewChild(ToastComponent, { static: true }) toast: ToastComponent;
    huellaDerecha: IDatosHuellaDerecha = {
        huellaD: '',
        huellaCompaCardD: '',
        huellaWSQD: '',
        huellaTemplateD: '',
        nistD: '',
        huellavivaD: '',
        imagenNistD: '',
        imagenhuellavivaD: ''
    };
    huellaIzquierda: IDatosHuellaIzquierda = {
        huellaI: '',
        huellaCompaCardI: '',
        huellaWSQI: '',
        huellaTemplateI: '',
        nistI: '',
        huellavivaI: '',
        imagenNistI: '',
        imagenhuellavivaI: ''
    };
    huellaDerechaTest: IDatosHuellaDerecha = {
        huellaD: 'data:image/png;base64,' + Constant.HUELLATEST,
        huellaCompaCardD: Constant.HUELLACOMPACARDTEST,
        nistD: '1',
        huellavivaD: '70.65',
        imagenNistD: 'assets/images/right.png',
        imagenhuellavivaD: 'assets/images/right.png'
    };


    @Input()
    dniConsultor;

    @Input()
    tipoCaptura;

    @Output()
    propagarMensajeError = new EventEmitter<IErrorValidador>();

    @Output()
    propagarRespuestaServicio = new EventEmitter<IRespuestaServicio>();

    @Output()
    propagarMensajeRespuesta = new EventEmitter();

    @Output()
    propagarHuellaDerecha = new EventEmitter<IDatosHuellaDerecha>();

    @Output()
    propagarHuellaIzquierda = new EventEmitter<IDatosHuellaIzquierda>();


    entidad: IObjVerificacion = {
        dniValidador: '',
        dniConsultor: '',
        tipoCaptura: 0
    };

    umbralHuellaViva = JSON.parse(localStorage.getItem('datosSesion'))._umbralHuellaViva;
    siValidarHuellaViva  = JSON.parse(localStorage.getItem('datosSesion'))._validarHuellaViva;
    // eslint-disable-next-line max-len
    siValidarDniValidador = JSON.parse(localStorage.getItem('datosSesion'))._validarDniValidador;
    siVisualizar = JSON.parse(localStorage.getItem('datosSesion'))._visualizar;
    siNoHuellaCapturada = JSON.parse(localStorage.getItem('datosSesion'))._noHuellaCapturada;
    siHuellaVivaAmarillo = JSON.parse(localStorage.getItem('datosSesion'))._huellaVivaAmarillo;
    siNoHuellaViva = JSON.parse(localStorage.getItem('datosSesion'))._noHuellaViva;
    siNistRojo = JSON.parse(localStorage.getItem('datosSesion'))._nistRojo;
    siNistAmarillo	= JSON.parse(localStorage.getItem('datosSesion'))._nistAmarillo;
    siNingunaHuella	= JSON.parse(localStorage.getItem('datosSesion'))._ningunaHuella;
    siNingunaHuellaRojo	= JSON.parse(localStorage.getItem('datosSesion'))._huellaVivaRojo;
    nivel_fake = parseInt(JSON.parse(localStorage.getItem('datosSesion'))._umbralHuellaViva, 10);


    attemptD = 1;
    attemptI = 1;
    NISTCaptura: number;
    data: Object = {};
    verifiqu;

    ngOnInit() {
        this.desHabilitarBotones();
        this.entidad.tipoCaptura = this.tipoCaptura;
        this.entidad.dniConsultor = this.dniConsultor;
    }

    servicioCapturaDactilar() {
        this.siHabilitacion = false;
        this.desHabilitarBotones();
        this.attemptD = 1;
        this.attemptI = 1;
        this.limpiarHuellasLogica();
        this.huellaDerechaBlank();
        this.huellaIzquierdaBlank();
        this.setSendRespuesta(null, 'cleanFront');
        this.setSendRespuesta(null, this.constant.CAPTURA_DACTILAR.INICIALIZAR_DISPOSITIVO.MENSAJE_ESPERA);
            this.servicioService.serviceInicializarCaptura(this.data).subscribe((dataResponse: any) => {
                if (dataResponse.codigoRespuesta === this.constant.CAPTURA_DACTILAR.INICIALIZAR_DISPOSITIVO.SUCCES.CODIGO) {
                    switch (this.tipoCaptura) {
                        case 0:
                            this.executeCapturaDactilarDerecha();
                            break;
                        case 1:
                            this.executeCapturaDactilarIzquierda();
                            break;
                        case 2:
                            this.executeCapturaAmbasHuellas();
                            break;
                        default:
                            break;
                    }
                } else {
                    this.setSendRespuesta('', dataResponse.mensajeRespuesta);
                    this.liberarDispositivo();
                }
            }, error => {
                this.liberarDispositivo();
                // eslint-disable-next-line max-len
                {this.setSendRespuesta('', this.constant.CAPTURA_DACTILAR.INICIALIZAR_DISPOSITIVO.ERROR.MENSAJE); }
              });
    }
    executeCapturaDactilarDerecha() {

        if (this.attemptD < 4) {
            this.mensajeEspera(0);
        this.servicioService.serviceCapturaDerecha(this.data).subscribe((dataResponseCapturaD: any) => {
            this.attemptD++;

              if (this.evaluarCodigoRespuesta(dataResponseCapturaD.codigoRespuesta, this.attemptD)) {
                // eslint-disable-next-line max-len
                if (dataResponseCapturaD.fakeDetection >= this.nivel_fake && dataResponseCapturaD.calidadNIST < this.constant.NISTMinima) {
                        // eslint-disable-next-line max-len
                        this.setSendHuellaD('data:image/png;base64,' +  dataResponseCapturaD.huellaPNG, dataResponseCapturaD.huellaTemplateCompaCard , dataResponseCapturaD.huellaWSQ, dataResponseCapturaD.huellaTemplate, dataResponseCapturaD.calidadNIST, dataResponseCapturaD.fakeDetection, this.mostrarNivelNist(dataResponseCapturaD.calidadNIST), this.mostrarNivelFakeDetection(dataResponseCapturaD.fakeDetection));
                        // eslint-disable-next-line max-len
                        this.setSendRespuesta(this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCES.CODIGO, this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCES.MENSAJE);
                        this.habilitarBotones();
                        this.liberarDispositivo();
                    } else {
                        this.executeCapturaDactilarDerecha();
                    }
              // eslint-disable-next-line max-len
              } else if (dataResponseCapturaD.fakeDetection <= this.nivel_fake) {
               // this.attemptD = 3;
                    if ( this.validarIntento(this.attemptD) ) {
                        // eslint-disable-next-line max-len
                        this.setSendHuellaD('data:image/png;base64,' +  dataResponseCapturaD.huellaPNG, dataResponseCapturaD.huellaTemplateCompaCard , dataResponseCapturaD.huellaWSQ, dataResponseCapturaD.huellaTemplate, dataResponseCapturaD.calidadNIST, dataResponseCapturaD.fakeDetection, this.mostrarNivelNist(dataResponseCapturaD.calidadNIST), this.mostrarNivelFakeDetection(dataResponseCapturaD.fakeDetection));
                        // eslint-disable-next-line max-len
                        this.setSendRespuesta(this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.FAKE_WARN.CODIGO, this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.FAKE_WARN.MENSAJE);
                        this.evaluarHabilitacionBotones(dataResponseCapturaD.fakeDetection, dataResponseCapturaD.calidadNIST);
                        this.liberarDispositivo();
                    } else {
                        this.executeCapturaDactilarDerecha();
                    }
              // eslint-disable-next-line max-len
              } else if (dataResponseCapturaD.codigoRespuesta === '40000' || dataResponseCapturaD.codigoRespuesta === 'null' || dataResponseCapturaD.codigoRespuesta === '') {
                  if (this.validarIntento(this.attemptD)) {
                    this.evaluarHabilitacionBotones(dataResponseCapturaD.fakeDetection, dataResponseCapturaD.calidadNIST);
                    // eslint-disable-next-line max-len
                    this.setSendRespuesta(this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCESS_WARN.CODIGO, this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCESS_WARN.MENSAJE);
                    this.liberarDispositivo();
                    } else {
                        this.executeCapturaDactilarDerecha();
                    }
              } else if (dataResponseCapturaD.calidadNIST > this.constant.NISTMinima) {
                if (this.validarIntento(this.attemptD)) {
                    // eslint-disable-next-line max-len
                    this.setSendHuellaD('data:image/png;base64,' +  dataResponseCapturaD.huellaPNG, dataResponseCapturaD.huellaTemplateCompaCard , dataResponseCapturaD.huellaWSQ, dataResponseCapturaD.huellaTemplate, dataResponseCapturaD.calidadNIST, dataResponseCapturaD.fakeDetection, this.mostrarNivelNist(dataResponseCapturaD.calidadNIST), this.mostrarNivelFakeDetection(dataResponseCapturaD.fakeDetection));
                    this.evaluarHabilitacionBotones(dataResponseCapturaD.fakeDetection, dataResponseCapturaD.calidadNIST);
                    this.setSendRespuesta(dataResponseCapturaD.codigoRespuesta, dataResponseCapturaD.mensajeRespuesta);
                    this.liberarDispositivo();
                } else {
                        this.executeCapturaDactilarDerecha();
                    }
              } else {
                  if (this.validarIntento(this.attemptD)) {
                    this.evaluarHabilitacionBotones(dataResponseCapturaD.fakeDetection, dataResponseCapturaD.calidadNIST);
                    // eslint-disable-next-line max-len
                    this.setSendRespuesta(this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.ERROR.CODIGO, this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.ERROR.MENSAJE);
                    this.liberarDispositivo();
                  } else {
                    this.executeCapturaDactilarDerecha();
                  }
              }
          });

    } else {
        this.setSendRespuesta(this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCESS_WARN.CODIGO, this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCESS_WARN.MENSAJE);
        this.liberarDispositivo();
    }
    }


    validarIntento(value): boolean {
        if (value === 3) {
            return true;
        } else {
            return false;
        }
    }


    executeCapturaDactilarIzquierda() {
        if (this.attemptI < 4) {
            this.mensajeEspera(1);
            this.servicioService.serviceCapturaIzquierda(this.data).subscribe((dataResponseCapturaI: any) => {
                this.attemptI++;
                  if (this.evaluarCodigoRespuesta(dataResponseCapturaI.codigoRespuesta, this.attemptI)) {
                    // eslint-disable-next-line max-len
                    if (dataResponseCapturaI.fakeDetection >= this.nivel_fake && dataResponseCapturaI.calidadNIST < this.constant.NISTMinima) {
                            // eslint-disable-next-line max-len
                            this.setSendHuellaI('data:image/png;base64,' +  dataResponseCapturaI.huellaPNG, dataResponseCapturaI.huellaTemplateCompaCard , dataResponseCapturaI.huellaWSQ, dataResponseCapturaI.huellaTemplate, dataResponseCapturaI.calidadNIST, dataResponseCapturaI.fakeDetection, this.mostrarNivelNist(dataResponseCapturaI.calidadNIST), this.mostrarNivelFakeDetection(dataResponseCapturaI.fakeDetection));
                            // eslint-disable-next-line max-len
                            this.setSendRespuesta(this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCES.CODIGO, this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCES.MENSAJE);
                            this.habilitarBotones();
                            this.liberarDispositivo();
                        } else {
                            this.executeCapturaDactilarIzquierda();
                        }
                  // eslint-disable-next-line max-len
                  } else if (dataResponseCapturaI.fakeDetection <= this.nivel_fake) {
                        if (this.validarIntento(this.attemptI)) {
                            // eslint-disable-next-line max-len
                            this.setSendRespuesta(this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.FAKE_WARN.CODIGO, this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.FAKE_WARN.MENSAJE);
                            // eslint-disable-next-line max-len
                            this.setSendHuellaI('data:image/png;base64,' +  dataResponseCapturaI.huellaPNG, dataResponseCapturaI.huellaTemplateCompaCard , dataResponseCapturaI.huellaWSQ, dataResponseCapturaI.huellaTemplate, dataResponseCapturaI.calidadNIST, dataResponseCapturaI.fakeDetection, this.mostrarNivelNist(dataResponseCapturaI.calidadNIST), this.mostrarNivelFakeDetection(dataResponseCapturaI.fakeDetection));
                            this.evaluarHabilitacionBotones(dataResponseCapturaI.fakeDetection, dataResponseCapturaI.calidadNIST);
                            this.liberarDispositivo();
                        } else {
                            this.executeCapturaDactilarIzquierda();
                        }

                  } else if (dataResponseCapturaI.codigoRespuesta === '40000' || dataResponseCapturaI.codigoRespuesta === 'null') {
                      if (this.validarIntento(this.attemptI)) {
                          // eslint-disable-next-line max-len
                        this.setSendRespuesta(this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCESS_WARN.CODIGO, this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCESS_WARN.MENSAJE);
                        this.evaluarHabilitacionBotones(dataResponseCapturaI.fakeDetection, dataResponseCapturaI.calidadNIST);
                        this.liberarDispositivo();
                        } else {
                            this.executeCapturaDactilarIzquierda();
                        }

                  } else if (dataResponseCapturaI.calidadNIST > this.constant.NISTMinima) {
                    if (this.validarIntento(this.attemptI)) {
                        // eslint-disable-next-line max-len
                        this.setSendHuellaI('data:image/png;base64,' +  dataResponseCapturaI.huellaPNG, dataResponseCapturaI.huellaTemplateCompaCard , dataResponseCapturaI.huellaWSQ, dataResponseCapturaI.huellaTemplate, dataResponseCapturaI.calidadNIST, dataResponseCapturaI.fakeDetection, this.mostrarNivelNist(dataResponseCapturaI.calidadNIST), this.mostrarNivelFakeDetection(dataResponseCapturaI.fakeDetection));
                        this.evaluarHabilitacionBotones(dataResponseCapturaI.fakeDetection, dataResponseCapturaI.calidadNIST);
                        this.setSendRespuesta(dataResponseCapturaI.codigoRespuesta, dataResponseCapturaI.mensajeRespuesta);
                        // Se agrego this.liberarDispositivo();
                        this.liberarDispositivo();
                    } else {
                        // Se cambio this.executeCapturaDactilarDerecha();
                        this.executeCapturaDactilarIzquierda();
                    }
                  } else {
                      if (this.validarIntento(this.attemptI)) {
                        this.evaluarHabilitacionBotones(dataResponseCapturaI.fakeDetection, dataResponseCapturaI.calidadNIST);
                        // eslint-disable-next-line max-len
                        this.setSendRespuesta(this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.ERROR.CODIGO, this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.ERROR.MENSAJE);
                        this.liberarDispositivo();
                      } else {
                        this.executeCapturaDactilarIzquierda();
                      }
                  }
              });
        } else {
                    // eslint-disable-next-line max-len
                    this.setSendRespuesta(this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCESS_WARN.CODIGO, this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCESS_WARN.MENSAJE);
                    this.liberarDispositivo();
        }
/*     }, 1500); */
    }
    executeCapturaAmbasHuellas() {
        if (this.attemptD < 4) {
            this.mensajeEspera(0);
            this.servicioService.serviceCapturaDerecha(this.data).subscribe((dataResponseCapturaD: any) => {
                this.attemptD++;
                  if (this.evaluarCodigoRespuesta(dataResponseCapturaD.codigoRespuesta, this.attemptD)) {
                    // eslint-disable-next-line max-len
                    if (dataResponseCapturaD.fakeDetection >= this.nivel_fake && dataResponseCapturaD.calidadNIST < this.constant.NISTMinima) {
                            // eslint-disable-next-line max-len
                            this.setSendHuellaD('data:image/png;base64,' +  dataResponseCapturaD.huellaPNG, dataResponseCapturaD.huellaTemplateCompaCard , dataResponseCapturaD.huellaWSQ, dataResponseCapturaD.huellaTemplate, dataResponseCapturaD.calidadNIST, dataResponseCapturaD.fakeDetection, this.mostrarNivelNist(dataResponseCapturaD.calidadNIST), this.mostrarNivelFakeDetection(dataResponseCapturaD.fakeDetection));
                            // eslint-disable-next-line max-len
                            this.setSendRespuesta(this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCES.CODIGO, this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCES.MENSAJE);
                            setTimeout(() => {
                                this.executeCapturaDactilarIzquierda();
                             }, 3000);
                        } else {
                            this.executeCapturaAmbasHuellas();
                        }
                        // parseInt(dataResponseCapturaD.fakeDetection, 10)
                  // eslint-disable-next-line max-len
                  } else if (parseInt(dataResponseCapturaD.fakeDetection, 10) <= this.nivel_fake || dataResponseCapturaD.fakeDetection <= this.nivel_fake ) {
                        if (this.validarIntento(this.attemptD)) {
                            if (this.siNoHuellaCapturada === '1' || this.siNingunaHuella === '1') {
                                // eslint-disable-next-line max-len
                                this.setSendHuellaD('data:image/png;base64,' +  dataResponseCapturaD.huellaPNG, dataResponseCapturaD.huellaTemplateCompaCard , dataResponseCapturaD.huellaWSQ, dataResponseCapturaD.huellaTemplate, dataResponseCapturaD.calidadNIST, dataResponseCapturaD.fakeDetection, this.mostrarNivelNist(dataResponseCapturaD.calidadNIST), this.mostrarNivelFakeDetection(dataResponseCapturaD.fakeDetection));
                                this.attemptI = 1;
                                setTimeout(() => {this.executeCapturaDactilarIzquierda(); }, 3000);
                            } else {
                                // eslint-disable-next-line max-len
                                this.setSendHuellaD('data:image/png;base64,' +  dataResponseCapturaD.huellaPNG, dataResponseCapturaD.huellaTemplateCompaCard , dataResponseCapturaD.huellaWSQ, dataResponseCapturaD.huellaTemplate, dataResponseCapturaD.calidadNIST, dataResponseCapturaD.fakeDetection, this.mostrarNivelNist(dataResponseCapturaD.calidadNIST), this.mostrarNivelFakeDetection(dataResponseCapturaD.fakeDetection));
                                // eslint-disable-next-line max-len
                                this.setSendRespuesta(this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.FAKE_WARN.CODIGO, this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.FAKE_WARN.MENSAJE);
                                this.liberarDispositivo();
                            }
                        } else {
                            this.executeCapturaAmbasHuellas();
                        }
                  } else if (dataResponseCapturaD.codigoRespuesta === '40000' || dataResponseCapturaD.codigoRespuesta === 'null') {
                      if (this.validarIntento(this.attemptD)) {
                        if (this.siNoHuellaCapturada === '1' || this.siNingunaHuella === '1') {
                            this.attemptI = 1;
                            setTimeout(() => {this.executeCapturaDactilarIzquierda(); }, 3000);
                        } else {
                            // eslint-disable-next-line max-len
                            this.setSendRespuesta(this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCESS_WARN.CODIGO, this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCESS_WARN.MENSAJE);
                            this.liberarDispositivo();
                        }
                        } else {
                            this.executeCapturaAmbasHuellas();
                        }

                  } else if (dataResponseCapturaD.calidadNIST > this.constant.NISTMinima) {
                    if (this.validarIntento(this.attemptD)) {
                        if (this.siNoHuellaCapturada === '1' || this.siNingunaHuella === '1') {
                            // eslint-disable-next-line max-len
                            this.setSendHuellaD('data:image/png;base64,' +  dataResponseCapturaD.huellaPNG, dataResponseCapturaD.huellaTemplateCompaCard , dataResponseCapturaD.huellaWSQ, dataResponseCapturaD.huellaTemplate, dataResponseCapturaD.calidadNIST, dataResponseCapturaD.fakeDetection, this.mostrarNivelNist(dataResponseCapturaD.calidadNIST), this.mostrarNivelFakeDetection(dataResponseCapturaD.fakeDetection));
                            this.attemptI = 1;
                            setTimeout(() => {this.executeCapturaDactilarIzquierda(); }, 3000);
                        } else {
                            this.setSendRespuesta(dataResponseCapturaD.codigoRespuesta, dataResponseCapturaD.mensajeRespuesta);
                            this.liberarDispositivo();
                        }
                    } else {
                            this.executeCapturaAmbasHuellas();
                        }
                  } else {
                      if (this.validarIntento(this.attemptD)) {
                        if (this.siNoHuellaCapturada === '1' || this.siNingunaHuella === '1') {
                            this.attemptI = 1;
                            setTimeout(() => {this.executeCapturaDactilarIzquierda(); }, 3000);
                        } else {
                            // eslint-disable-next-line max-len
                            this.setSendRespuesta(this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.ERROR.CODIGO, this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.ERROR.MENSAJE);
                            this.liberarDispositivo();
                        }
                      } else {
                        this.executeCapturaAmbasHuellas();
                      }
                  }
              });
        } else {
            // eslint-disable-next-line max-len
            this.setSendRespuesta(this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCESS_WARN.CODIGO, this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCESS_WARN.MENSAJE);
            this.liberarDispositivo();
        }
    }

    liberarDispositivo() {
    this.siHabilitacion = true;
    this.servicioService.serviceEndCapturaDactilar(this.data).subscribe((dataR: any) => {});
    }
    setRespuestaServicio(codigo: string, mensaje: string) {
        this.respuestaServicio.codigoRespuesta = '';
        this.respuestaServicio.mensajeRespuesta = mensaje;
    }
    // eslint-disable-next-line max-len
    setHuellaDerecha(huella: string, huellaCompaCard: string , huellaWSQ: string, huellaTemplate: string, nist: string, huellaviva: string, imagenNist: String, imagenNivelFakeDetection: String) {
        this.huellaDerecha.huellaD = huella;
        this.huellaDerecha.huellaCompaCardD = huellaCompaCard;
        this.huellaDerecha.huellaWSQD = huellaWSQ;
        this.huellaDerecha.huellaTemplateD = huellaTemplate;
        this.huellaDerecha.nistD = this.evaluarNist(nist);
        this.huellaDerecha.huellavivaD = this.convertirFakeDetection(huellaviva);
        this.huellaDerecha.imagenNistD = imagenNist;
        this.huellaDerecha.imagenhuellavivaD = imagenNivelFakeDetection;
    }
    // eslint-disable-next-line max-len
    setHuellaIzquierda(huella: string, huellaCompaCard: string , huellaWSQ: string, huellaTemplate: string, nist: string, huellaviva: string, imagenNivelNist: String, imagenNivelFakeDetection: String) {
        this.huellaIzquierda.huellaI = huella;
        this.huellaIzquierda.huellaCompaCardI = huellaCompaCard;
        this.huellaIzquierda.huellaWSQI = huellaWSQ;
        this.huellaIzquierda.huellaTemplateI = huellaTemplate;
        this.huellaIzquierda.nistI = this.evaluarNist(nist);
        this.huellaIzquierda.huellavivaI = this.convertirFakeDetection(huellaviva);
        this.huellaIzquierda.imagenNistI = imagenNivelNist;
        this.huellaIzquierda.imagenhuellavivaI = imagenNivelFakeDetection;
    }

    convertirFakeDetection(fakeDetection) {
        if (fakeDetection != null) {
            fakeDetection = fakeDetection.replace('.', ',');
        } else {
            fakeDetection = '0';
        }
        const newFakeDetection = fakeDetection;
        return newFakeDetection;
    }

    mostrarNivelNist(nivelNist) {
        if (nivelNist < 3 && nivelNist >= 1) {
            nivelNist = this.rightImage;
        } else if (nivelNist === 3 || nivelNist === '3') {
            nivelNist = this.warningImage;
        } else if (nivelNist > 3 && nivelNist <= 5) {
            nivelNist = this.wrongImage;
        } else {
          nivelNist = this.nullImage;
        }
        return nivelNist;
    }
    mostrarNivelFakeDetection(fakeDetection) {

      if (fakeDetection === 0) {
        fakeDetection = this.nullImage;
      } else if (fakeDetection >= this.nivel_fake) {
        fakeDetection = this.rightImage;
      } else if (fakeDetection <= this.nivel_fake) {
        fakeDetection = this.wrongImage;
      } else {
        fakeDetection = this.nullImage;
      }
      return fakeDetection;
  }

    evaluarHuellaViva(nivelHuellaViva) {
    if (this.siValidarHuellaViva === 'false') {
        return true;
    } else if (nivelHuellaViva >= this.nivel_fake) {
        return true;
    }

    return false;
}
    habilitarBotones() {
        localStorage.setItem('botonVerificar', 'true');
        localStorage.setItem('botonMOC', 'true');
    }
    desHabilitarBotones() {
        localStorage.setItem('botonVerificar', 'false');
        localStorage.setItem('botonMOC', 'false');
        localStorage.setItem('botonFoto', 'false');
        localStorage.setItem('botonDatos', 'false');
    }

    evaluarNISTCaptura(calidadNIST) {

    return calidadNIST !== 0 && calidadNIST <= this.NISTCaptura;
}

    evaluarCodigoRespuestaCapturaDactilar(dataViaJsonP) {

    return dataViaJsonP.codigoRespuesta === this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCES.CODIGO;
}
    huellaDerechaBlank() {
        this.setHuellaDerecha('assets/images/blank.png', '', '', '', '', '', 'assets/images/blank2.png', 'assets/images/blank2.png');
        this.propagarHuellaDerecha.emit(this.huellaDerecha);
    }
    huellaIzquierdaBlank() {
        this.setHuellaIzquierda('./assets/images/blank.png', '', '', '', '', '', './assets/images/blank2.png', './assets/images/blank2.png');
        this.propagarHuellaIzquierda.emit(this.huellaIzquierda);
    }

    evaluarNist(nist) {
        if (nist !== '') {
            nist = parseInt(nist, 10).toString();
        } else {
            nist = '';
        }
        return nist;
    }
    evaluarCodigoRespuesta(ResponseCode: string, intentos: number) {
        return ResponseCode === this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCES.CODIGO && intentos < this.constant.NumeroIntentos + 1;
    }

    setSendRespuesta(codigoR, mensajeR) {
            this.setRespuestaServicio(null, mensajeR);
            this.propagarRespuestaServicio.emit(this.respuestaServicio);
    }
    // eslint-disable-next-line max-len
    setSendHuellaD(huella: string, huellaCompaCard: string , huellaWSQ: string, huellaTemplate: string, nist: string, huellaviva: string, imagenNist: String, imagenNivelFakeDetection: String) {
        this.setHuellaDerecha(huella, huellaCompaCard, huellaWSQ, huellaTemplate, nist , huellaviva, imagenNist, imagenNivelFakeDetection);
        this.propagarHuellaDerecha.emit(this.huellaDerecha);
        this.huellaService.agregarHuellaDerecha(this.huellaDerecha);
    }
    pruebaHuella() {
        this.propagarHuellaDerecha.emit(this.huellaDerechaTest);
        this.huellaService.agregarHuellaDerecha(this.huellaDerechaTest);
    }
    // eslint-disable-next-line max-len
    setSendHuellaI(huella: string, huellaCompaCard: string , huellaWSQ: string, huellaTemplate: string, nist: string, huellaviva: string, imagenNist: String, imagenNivelFakeDetection: String) {
        this.setHuellaIzquierda(huella, huellaCompaCard, huellaWSQ, huellaTemplate, nist , huellaviva, imagenNist, imagenNivelFakeDetection);
        this.propagarHuellaIzquierda.emit(this.huellaIzquierda);
        this.huellaService.agregarHuellaIzquierda(this.huellaIzquierda);
    }

    evaluarHabilitacionBotones(huellaViva, nist) {
        if (this.siNingunaHuellaRojo === '1' && huellaViva  < this.nivel_fake) {
            this.habilitarBotones();
        }
        if (this.siNoHuellaViva === '1' && (huellaViva === '' || huellaViva === null || huellaViva === 'null')) {
            this.habilitarBotones();
        }
        if (this.siNistRojo === '1' && nist > 3) {
                this.habilitarBotones();
        }
        if ((nist === 3 || nist === '3') && (this.siNistAmarillo === '1' && huellaViva < this.umbralHuellaViva)) {
            this.habilitarBotones();
        }
        // eslint-disable-next-line max-len
        if (this.siNoHuellaCapturada === '1' && this.nuevoEvaluarNulo(this.huellaDerecha.huellaD, this.huellaIzquierda.huellaI)) {
            this.habilitarBotones();
        }
        if (this.siNingunaHuella === '1' && (this.huellaDerecha.huellaWSQD === '' || this.huellaIzquierda.huellaWSQI === '')) {
            this.habilitarBotones();
        }
        if (this.siHuellaVivaAmarillo === '1' && huellaViva  === this.nivel_fake) {
            this.habilitarBotones();
        }
    }

    habilitarBotonCapturar() {
        if (this.siHabilitacion) {
            return true;
        } else {
            return false;
        }
    }
    mensajeEspera(lado: number) {
        if (localStorage.getItem('dataResponseMejorHuella') === null || localStorage.getItem('dataResponseMejorHuella')   === '' ) {
                if (lado === 0) {
            // eslint-disable-next-line max-len
            this.setSendRespuesta('', this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.MENSAJE_ESPERA.replace('dedo', 'Índice Derecho'));
                } else {
                // eslint-disable-next-line max-len
                this.setSendRespuesta('', this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.MENSAJE_ESPERA.replace('dedo', 'Índice Izquierdo'));
                }
        } else {
            if (lado === 0) {
            // eslint-disable-next-line max-len
            this.setSendRespuesta('', (this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.MENSAJE_ESPERA.replace('dedo', JSON.parse(localStorage.getItem('dataResponseMejorHuella')).descripcionHuellaDerecha)).replace('Menique', 'Meñique'));
            } else {
            // eslint-disable-next-line max-len
            this.setSendRespuesta('', (this.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.MENSAJE_ESPERA.replace('dedo', JSON.parse(localStorage.getItem('dataResponseMejorHuella')).descripcionHuellaIzquierda)).replace('Menique', 'Meñique'));
            }
        }

    }
    limpiarHuellasLogica() {
        this.setSendHuellaD('', '', '', '', '', '', '', '');
        this.setSendHuellaI('', '', '', '', '', '', '', '');
    }
    evaluarNulo(d, i) {
        if (d === 'assets/images/blank.png' && i === 'assets/images/blank.png') {
            return false;
        }
        if (d === this.valorNuloAgente && i === 'assets/images/blank.png') {
            return false;
        }
        if (d === 'assets/images/blank.png' && i === this.valorNuloAgente) {
            return false;
        }
        if (d === this.valorNuloAgente && i === this.valorNuloAgente) {
            return false;
        }
        if (d !== this.valorNuloAgente && i === 'assets/images/blank.png') {
            return true;
        }
        if (d !== this.valorNuloAgente && i !== this.valorNuloAgente) {
            return true;
        }

    }
    nuevoEvaluarNulo(d, i) {
        if (d !== this.valorNuloAgente && this.huellaDerecha.huellavivaD !== '') {
            return true;
        }
        if (i !== this.valorNuloAgente && this.huellaIzquierda.huellavivaI !== '') {
            return true;
        } else {
            return false;
        }
    }


}
