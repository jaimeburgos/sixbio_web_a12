import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, flush, TestBed, tick, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { of, Subject, throwError } from 'rxjs';
import { EnvService } from 'src/app/env.service';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { HuellaService } from '../../serviciosDeUI/huella.service';
import { CapturarComponent } from './capturar.component';
class FakeRouter {
  navigate(params) {

  }
}

class FakeActivatedRoute {
  // params: Observable<any> = Observable.empty();
  private subject = new Subject();

  push(valor) {
    this.subject.next(valor);
  }
  get params() {
    return this.subject.asObservable();
  }
}
fdescribe('CapturarComponent', () => {
  let component: CapturarComponent;
  let fixture: ComponentFixture<CapturarComponent>;
  // Instanciamos los servicios que utilizaremos
  let translateService: TranslateService;
  let servicio: ServicioService;
  let huellaService: HuellaService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CapturarComponent],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService,
        HuellaService,
        { provide: Router, useClass: FakeRouter },
        { provide: ToastrService, useClass: ToastrService },
        { provide: ActivatedRoute, useClass: FakeActivatedRoute }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CapturarComponent);
    // Injectamos nuestro servicio con TestBed.inject
    servicio = TestBed.inject(ServicioService);
    // Injectamos nuestro servicio de huella con TestBed.inject
    huellaService = TestBed.inject(HuellaService);
    component = fixture.componentInstance;
    fixture.detectChanges();
    // Mockeamos valor en localstorage de datosSesion
    const mockDatosSesion = {
      '_ningunaHuella': '0',
      '_huellaVivaAmarillo': '0',
      '_noHuellaViva': '0',
      '_nistRojo': '0',
      '_umbralHuellaViva': '50.0',
      '_nistAmarillo': '0',
      '_huellaVivaRojo': '0',
      '_validarSixser': false,
      '_serviciosVerificacion': {
        'default': '301001',
        'tipos': [
          {
            'descripcion': 'T33-ANSI',
            'codigo': '301001'
          },
          {
            'descripcion': 'T33-ANSI',
            'codigo': '301001'
          },
          {
            'descripcion': 'T35-WSQ',
            'codigo': '301100'
          }
        ],
        'tiposServicioVerificacion': [
          {
            '_descripcion': 'T33-ANSI',
            '_codigo': '301001'
          },
          {
            '_descripcion': 'T33-ANSI',
            '_codigo': '301001'
          },
          {
            '_descripcion': 'T35-WSQ',
            '_codigo': '301100'
          }
        ]
      },
      '_acciones': [
        {
          '_descripcion': 'Mejor Huella',
          '_codigo': 'sixbio'
        },
        {
          '_descripcion': 'Capturar',
          '_codigo': 'sixbio'
        },
        {
          '_descripcion': 'Verificar',
          '_codigo': 'sixbio'
        },
        {
          '_descripcion': 'VerificarMOCard',
          '_codigo': 'sixbio'
        },
        {
          '_descripcion': 'Foto',
          '_codigo': 'sixser'
        },
        {
          '_descripcion': 'Datos',
          '_codigo': 'sixser'
        }
      ],
      '_idSesion': '000000202104051226028320041',
      '_validarHuellaViva': true,
      '_validarDniValidador': true,
      '_visualizar': 'true',
      '_sixbioVersion': '1.6',
      '_usuario': 'jburgos5',
      '_noHuellaCapturada': '0',
      '_validarIpCliente': true,
      '_rolesSCA': [
        {
          '_permisos': [],
          '_rol': 'EMP_VER'
        },
        {
          '_permisos': [],
          '_rol': 'VAL'
        }
      ],
      '_codigoRespuesta': '00000',
      '_mensajeRespuesta': 'OPERACION REALIZADA SATISFACTORIAMENTE'
    };
    // Habilitamos en jasmine el re espiar las funciones , caso contrario tendriamos un error
    jasmine.getEnv().allowRespy(true);
    localStorage.setItem('datosSesion', JSON.stringify(mockDatosSesion));

  });

  it('CapturarComponent creado correctamente', () => {
    expect(component).toBeTruthy();
  });

  it('Verificamos la funcion ngOnInit()', () => {
    // espiamos nuestra funcion desHabilitarBotones()
    const spydesHabilitarBotones = spyOn(component, 'desHabilitarBotones').and.callThrough();
    // Mockeamos los valores los @input dniConsultor y tipoCaptura
    const mockDniConsultor = '12345678';
    const mockTipoCaptura = 5;
    // Asigamos los valores mockeados a los @input del componente
    component.dniConsultor = mockDniConsultor;
    component.tipoCaptura = mockTipoCaptura;
    // Llamamos nuestra funcion ngOnInit()
    component.ngOnInit();
    // Verificamos que nuestra funcion espiada fuera llamada
    expect(spydesHabilitarBotones).toHaveBeenCalled();
    // Verificamos que los valores asignados al objeto Entidad
    expect(component.entidad.dniConsultor).toEqual(mockDniConsultor);
    expect(component.entidad.tipoCaptura).toEqual(mockTipoCaptura);
  });

  it('Verificamos la funcion servicioCapturaDactilar()', () => {
    // espiamos nuestra funcion desHabilitarBotones()
    const spydesHabilitarBotones = spyOn(component, 'desHabilitarBotones').and.callThrough();
    // espiamos nuestra funcion limpiarHuellasLogica()
    const spylimpiarHuellasLogicas = spyOn(component, 'limpiarHuellasLogica').and.callThrough();
    // espiamos nuestra funcion huellaDerechaBlank()
    const spyhuellaDerechaBlank = spyOn(component, 'huellaDerechaBlank').and.callThrough();
    // espiamos nuestra funcion huellaIzquierdaBlank()
    const spyhuellaIzquierdaBlank = spyOn(component, 'huellaIzquierdaBlank').and.callThrough();
    // espiamos nuestra funcion setSendRespuesta() , la cual se llamara 2 veces
    const spysetSendRespuesta = spyOn(component, 'setSendRespuesta').and.callThrough();
    // Espiamos nuestro servcio serviceInicializarCaptura
    const spyService = spyOn(servicio, 'serviceInicializarCaptura').and.callThrough();
    // Llamamos nuestra funcion
    component.servicioCapturaDactilar();
    // Verificamos los valores que se asignan a las variables
    expect(component.siHabilitacion).toBeFalsy();
    expect(component.attemptD).toBe(1);
    expect(component.attemptI).toBe(1);
    // Verificamos que nuestra funcion espiada fuera llamada desHabilitarBotones
    expect(spydesHabilitarBotones).toHaveBeenCalled();
    // Verificamos que nuestra funcion espiada fuera llamada limpiarHuellasLogica
    expect(spylimpiarHuellasLogicas).toHaveBeenCalled();
    // Verificamos que nuestra funcion espiada fuera llamada huellaDerechaBlank
    expect(spyhuellaDerechaBlank).toHaveBeenCalled();
    // Verificamos que nuestra funcion espiada fuera llamada huellaIzquierdaBlank
    expect(spyhuellaIzquierdaBlank).toHaveBeenCalled();
    // Verificamos que nuestra funcion espiada fuera llamada setSendRespuesta
    expect(spysetSendRespuesta).toHaveBeenCalledTimes(2);
    // Verificamos que se llame nuestro servicio serviceInicializarCaptura
    expect(spyService).toHaveBeenCalled();

    // eslint-disable-next-line max-len
    // Ahora verificamos la primera condicion cuando el codigoRespuesta del servicio sea constant.CAPTURA_DACTILAR.INICIALIZAR_DISPOSITIVO.SUCCES.CODIGO
    // Mockeamos un dataResponse que posteriormente nos devolvera nuestro servicio usando callFake
    const mockDataResponse = {
      codigoRespuesta: component.constant.CAPTURA_DACTILAR.INICIALIZAR_DISPOSITIVO.SUCCES.CODIGO,
      mensajeRespuesta: 'Test'
    };
    const spyServiceFake = spyOn(servicio, 'serviceInicializarCaptura').and.callFake(() => {
      return of(mockDataResponse);
    });
    // Llamamos nuestra funcion
    component.servicioCapturaDactilar();
    // Verificamos que llame nuestro servicio fake
    expect(spyServiceFake).toHaveBeenCalled();

    // Ahora verificamos para los caso del switch dentro de la primera condicion
    // Numero aleatorio , que empieza desde 3 el cual esta fuera del rango de los case del switch , verificamos que entre en default
    // asigamos el valor aleatorio a tipoCaptura
    const aleatorio = Math.floor(Math.random() * 101) + 3;
    component.tipoCaptura = aleatorio;
    debugger;
    // Espiamos nuestra funcion executeCapturaDactilarDerecha()
    const spyexecuteCapturaDactilarDerecha = spyOn(component, 'executeCapturaDactilarDerecha').and.callThrough();
    // Espiamos nuestra funcion executeCapturaDactilarIzquierda()
    const spyexecuteCapturaDactilarIzquierda = spyOn(component, 'executeCapturaDactilarIzquierda');
    // espiamos nuestra funcion executeCapturaAmbasHuellas();
    const spyexecuteCapturaAmbasHuellas = spyOn(component, 'executeCapturaAmbasHuellas').and.callThrough();
    // Llamamos nuestra funcion
    component.servicioCapturaDactilar();
    // Verificamos que NO llame ninguno de nuestros espias
    expect(spyexecuteCapturaDactilarDerecha).not.toHaveBeenCalled();
    expect(spyexecuteCapturaAmbasHuellas).not.toHaveBeenCalled();
    expect(spyexecuteCapturaDactilarIzquierda).not.toHaveBeenCalled();

    // Probamos el caso que  sea 0 tipoCaptura
    component.tipoCaptura = 0;
    // Llamamos nuestra funcion
    component.servicioCapturaDactilar();
    // Verificamos que llame nuestro funcion executeCapturaDactilarDerecha
    expect(spyexecuteCapturaDactilarDerecha).toHaveBeenCalled();

    // Ahora verificamos para el 1 de tipoCaptura
    component.tipoCaptura = 1;
    // Llamamos nuestra funcion
    component.servicioCapturaDactilar();
    // Verificamos que llame nuestro funcion executeCapturaDactilarIzquierda
    expect(spyexecuteCapturaDactilarIzquierda).toHaveBeenCalled();

    // Ahora verificamos para el caso que 2 tipoCaptura
    component.tipoCaptura = 2;
    // Llamamos nuestra funcion
    component.servicioCapturaDactilar();
    // Verificamos que llame nuestro funcion spyexecuteCapturaAmbasHuellas
    expect(spyexecuteCapturaAmbasHuellas).toHaveBeenCalled();

    // eslint-disable-next-line max-len
    // Ahora verificamos para el caso que codigoRespuesta diferente de component.constant.CAPTURA_DACTILAR.INICIALIZAR_DISPOSITIVO.SUCCES.CODIGO
    mockDataResponse.codigoRespuesta = 'Diferente de component.constant.CAPTURA_DACTILAR.INICIALIZAR_DISPOSITIVO.SUCCES.CODIGO';
    // Espiamos nuestra funcion liberarDispositivo
    const spyliberarDispositivo = spyOn(component, 'liberarDispositivo').and.callThrough();
    // Llamamos nuestra funcion
    component.servicioCapturaDactilar();
    // Verificamos que fuera llamada nuestra funcion
    expect(spyliberarDispositivo).toHaveBeenCalled();
    // Verificamos que nuestra funcion setSendRespuesta
    expect(spysetSendRespuesta).toHaveBeenCalled();


    // Ahora verificamos para el caso que nuestro servicio nos retorne un error
    // Para ello forzamos al servicio con returnValue a darnos un error con throwError
    const spyServiceError = spyOn(servicio, 'serviceInicializarCaptura').and.returnValue(throwError('Error serviceInicializarCaptura'));

    // Llamamos nuestra funcion
    component.servicioCapturaDactilar();
    // Verificamos que fuera llamada nuestra funcion
    expect(spyliberarDispositivo).toHaveBeenCalled();
    // Verificamos que nuestro servicio error fuera llamado correctamente
    expect(spyServiceError).toHaveBeenCalled();
    // Verificamos que nuestra funcion setSendRespuesta
    expect(spysetSendRespuesta).toHaveBeenCalled();
    // Verificamos que nuestra funcion setSendRespuesta pasandole los argumentos funciona correctamente
    expect(spysetSendRespuesta).toHaveBeenCalledWith('', mockDataResponse.mensajeRespuesta);
  });



  it('Verificamos nuestra funcion executeCapturaDactilarDerecha()', () => {

    // Para probar nuestra primera condicion asigamos un valor menor que 4 a attemptD
    component.attemptD = 1;
    debugger;
    // Espiamos nuestra funcion mensajeEspera
    const spymensajeEspera = spyOn(component, 'mensajeEspera').and.callThrough();
    // Espiamos nuestro servicio serviceCapturaDerecha
    const spyserviceCapturaDerecha = spyOn(servicio, 'serviceCapturaDerecha').and.callThrough();
    // Llamamos nuestra funcion
    component.executeCapturaDactilarDerecha();
    // Verificamos que nuestro servicio fuera llamada
    expect(spyserviceCapturaDerecha).toHaveBeenCalled();
    // Verificamos que nuestra funcionmensajeEspera fuera llamada
    expect(spymensajeEspera).toHaveBeenCalled();

    // Ahora mockeamos el dataResponse de nuestro servicio para probar las condiciones
    // para ello asigamos codigoRespuesta el valor de constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCES.CODIGO
    // Asigamos un valor a la variable nivel_fake
    component.nivel_fake = 2;
    const mockDataResponse = {
      codigoRespuesta: component.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCES.CODIGO,
      // Asigamos este valor menor que constant.NISTMinima para probar la primera validacion
      calidadNIST: 2,
      // Asigamos este valor mayor que component.nivel_fake para probar la primera validacion
      fakeDetection: '3',
      huellaPNG: 'Test_HuellaPNG',
      huellaTemplateCompaCard: 'Test huellaTemplateCompaCard',
      huellaWSQ: 'Test huellaWSQ',
      huellaTemplate: 'Test huellaTemplate',
    };
    // espiamos nuestra funcion setSendHuellaD()
    const spysetSendHuellaD = spyOn(component, 'setSendHuellaD');
    // Espiamos nuestra funcion mostrarNivelNist()
    const spymostrarNivelNist = spyOn(component, 'mostrarNivelNist').and.callThrough();
    // Espiamos nuestra funcion mostrarNivelFakeDetection()
    const spymostrarNivelFakeDetection = spyOn(component, 'mostrarNivelFakeDetection').and.callThrough();
    // Espiamos nuestra funcion setSendRespuesta()
    const spysetSendRespuesta = spyOn(component, 'setSendRespuesta').and.callThrough();
    // Espiamos nuestra funcion habilitarBotones()
    const spyhabilitarBotones = spyOn(component, 'habilitarBotones').and.callThrough();
    // Espiamos nuestra funcion liberarDispositivo()
    const spyliberarDispositivo = spyOn(component, 'liberarDispositivo').and.callThrough();

    // Espiamos nuestra funcion executeCapturaDactilarDerecha()
    const spyexecuteCapturaDactilarDerecha = spyOn(component, 'executeCapturaDactilarDerecha').and.callThrough();
    // Llamamos nuestra servicio con callFake para que nos retorne el mockDataResponse
    const spyServiceFake = spyOn(servicio, 'serviceCapturaDerecha').and.callFake(() => {
      return of(mockDataResponse);
    });
    // Llamamos nuestra funcion
    component.executeCapturaDactilarDerecha();
    // Verificamos que llame nuestro servicio Fake
    expect(spyServiceFake).toHaveBeenCalled();
    // Verificamos que llame nuestra funcion setSendHuellaD
    expect(spysetSendHuellaD).toHaveBeenCalled();
    // Verificamos que llame nuestra funcion spymostrarNivelNist
    expect(spymostrarNivelNist).toHaveBeenCalled();
    // Verificamos que llame nuestra funcion mostrarNivelFakeDetection
    expect(spymostrarNivelFakeDetection).toHaveBeenCalled();
    // Verificamos que llame nuestra funcion setSendRespuesta
    expect(spysetSendRespuesta).toHaveBeenCalled();
    // Verificamos que llame nuestra funcion habilitarBotones
    expect(spyhabilitarBotones).toHaveBeenCalled();
    // Verificamos que llame nuestra funcion liberarDispositivo
    expect(spyliberarDispositivo).toHaveBeenCalled();

    // Ahora verificamos la condicion contraria
    // Para ello asigamos valores que no cumplen la condicion anterior

    component.nivel_fake = 3;
    mockDataResponse.fakeDetection = '1';
    mockDataResponse.calidadNIST = 999999;
    const test = 3;
    component.attemptD = test;
    // Espiamos nuestra funcion evaluarHabilitacionBotones
    const spyevaluarHabilitacionBotones = spyOn(component, 'evaluarHabilitacionBotones');

    spyOn(component, 'validarIntento').and.callFake(() => true);
    // Llamamos nuestra funcion
    component.executeCapturaDactilarDerecha();
    // Verificamos que llame nuestra funcion executeCapturaDactilarDerecha fuera llamada 1 veces hasta este punto en total
    expect(spyexecuteCapturaDactilarDerecha).toHaveBeenCalledTimes(2);
    // Verificamos que llame nuestra funcion setSendHuellaD
    expect(spysetSendHuellaD).toHaveBeenCalledTimes(2);
    // Verificamos que llame nuestra funcion setSendRespuesta
    expect(spysetSendRespuesta).toHaveBeenCalledTimes(4);
    // Verificamos que llame nuestra funcion evaluarHabilitacionBotones
    expect(spyevaluarHabilitacionBotones).toHaveBeenCalled();
    // Verificamos que llame nuestra funcion liberarDispositivo
    expect(spyliberarDispositivo).toHaveBeenCalledTimes(2);
    // Verificamos que llame nuestra funcion habilitarBotones
    expect(spyhabilitarBotones).toHaveBeenCalledTimes(1);
    // Verificamos que llame nuestra funcion liberarDispositivo
    expect(spyliberarDispositivo).toHaveBeenCalledTimes(2);

    component.nivel_fake = 3;
    mockDataResponse.fakeDetection = '1';
    mockDataResponse.calidadNIST = 999999;
    component.attemptD = test;
    // Ahora verificamos para el caso contrario cuando validarIntento sea false
    spyOn(component, 'validarIntento').and.callFake(() => false);
    // Llamamos nuestra funcion
    component.executeCapturaDactilarDerecha();
    expect(spyexecuteCapturaDactilarDerecha).toHaveBeenCalledTimes(4);

    // Ahora probamos para el caso que codigoRespuesta sea '40000' o 'null' o ''
    component.attemptD = 1;
    mockDataResponse.codigoRespuesta = 'null';
    component.nivel_fake = 2;
    mockDataResponse.fakeDetection = undefined;
    // Fakeamos un retorno false para la funcion evaluarCodigoRespuesta
    spyOn(component, 'evaluarCodigoRespuesta').and.callFake(() => false);
    spyOn(component, 'validarIntento').and.callFake(() => true);

    // Llamamos nuestra funcion
    component.executeCapturaDactilarDerecha();
    // Verificamos que llame nuestra funcion evaluarHabilitacionBotones
    expect(spyevaluarHabilitacionBotones).toHaveBeenCalledTimes(2);
    // Verificamos que llame nuestra funcion setSendRespuesta
    expect(spysetSendRespuesta).toHaveBeenCalledTimes(8);
    // Verificamos que llame nuestra funcion liberarDispositivo
    expect(spyliberarDispositivo).toHaveBeenCalledTimes(4);

    component.attemptD = 1;
    // Ahora verificamos para el caso dataResponseCapturaD.calidadNIST > this.constant.NISTMinima
    // Para ello asiganmos undefined a codigoRespuesta
    mockDataResponse.codigoRespuesta = undefined;
    mockDataResponse.calidadNIST = component.constant.NISTMinima + 1;
    // Llamamos nuestra funcion
    component.executeCapturaDactilarDerecha();
    // Verificamos que llame nuestros espias
    expect(spysetSendHuellaD).toHaveBeenCalledTimes(3);
    expect(spyevaluarHabilitacionBotones).toHaveBeenCalledTimes(3);
    expect(spysetSendHuellaD).toHaveBeenCalledTimes(3);
    expect(spysetSendRespuesta).toHaveBeenCalledTimes(10);
    expect(spyliberarDispositivo).toHaveBeenCalledTimes(5);

    // Ahora verificamos que ninguna de la condiciones anteriores se cumpla
    component.attemptD = 1;
    mockDataResponse.fakeDetection = undefined;
    mockDataResponse.calidadNIST = undefined;
    mockDataResponse.huellaPNG = undefined;
    mockDataResponse.huellaTemplateCompaCard = undefined;
    mockDataResponse.huellaWSQ = undefined;
    mockDataResponse.huellaTemplate = undefined;
    // Llamamos nuestra funcion
    component.executeCapturaDactilarDerecha();
    // Verificamos que llame nuestros espias
    expect(spyevaluarHabilitacionBotones).toHaveBeenCalledTimes(4);
    expect(spysetSendRespuesta).toHaveBeenCalledTimes(12);
    expect(spyliberarDispositivo).toHaveBeenCalledTimes(6);

    // Ahora verificamos para el caso contrario
    // forzamos a validarIntento A RETORNAR FALSE
    spyOn(component, 'validarIntento').and.callFake(() => false);
    // Llamamos nuestra funcion
    component.executeCapturaDactilarDerecha();
    expect(spysetSendRespuesta).toHaveBeenCalledTimes(15);
    expect(spyliberarDispositivo).toHaveBeenCalledTimes(7);

  });

  it('Verificamos la funcion validarIntento(value) : boolean ', () => {
    // Mockeamos un valor de entrada
    let mockValue = 3;
    // Llamamos nuestra funcion y le pasamos el valor de entrada y verificamos que retorne true
    expect(component.validarIntento(mockValue)).toBeTruthy();
    // Para cualquier valor diferente de 3 es false
    mockValue = 666;
    expect(component.validarIntento(mockValue)).toBeFalsy();
  });

  it('Verificamos la funcion evaluarNulo(d,i)', () => {
    // Mockeamos sus valores de entrada
    let mockD = 'assets/images/blank.png';
    let mockI = 'assets/images/blank.png';
    // Llamamos nuestra funcion y la evaluamos
    expect(component.evaluarNulo(mockD, mockI)).toBeFalsy();

    mockD = component.valorNuloAgente;
    mockI = 'assets/images/blank.png';
    // Llamamos nuestra funcion y la evaluamos
    expect(component.evaluarNulo(mockD, mockI)).toBeFalsy();

    mockD = 'assets/images/blank.png';
    mockI = component.valorNuloAgente;
    // Llamamos nuestra funcion y la evaluamos
    expect(component.evaluarNulo(mockD, mockI)).toBeFalsy();

    mockD = component.valorNuloAgente;
    mockI = component.valorNuloAgente;
    // Llamamos nuestra funcion y la evaluamos
    expect(component.evaluarNulo(mockD, mockI)).toBeFalsy();

    mockD = 'Testing Diferente de valor Nulo Agente';
    mockI = 'assets/images/blank.png';
    // Llamamos nuestra funcion y la evaluamos
    expect(component.evaluarNulo(mockD, mockI)).toBeTruthy();

    mockD = 'Testing Diferente de valor Nulo Agente';
    mockI = 'Testing Diferente de valor Nulo Agente';
    // Llamamos nuestra funcion y la evaluamos
    expect(component.evaluarNulo(mockD, mockI)).toBeTruthy();
  });


  it('Verificamos nuestra funcion nuevoEvaluarNulo(d,i)', () => {
    // Validamos el primer caso que nos devuelve TRUE nuestra funcion
    let mockD = 'Diferente de this.valorNuloAgente';
    component.huellaDerecha.huellavivaD = 'Cualquier cosa';
    let mockI = 'Diferente de this.valorNuloAgente';
    // Le pasamos los parametro de entrada y evaluamos el retorno de nuestra funcion
    expect(component.nuevoEvaluarNulo(mockD, mockI)).toBeTruthy();

    // Ahora validamos la siguiente condicion
    mockD = component.valorNuloAgente;
    mockI = 'Diferente de this.valorNuloAgente';
    component.huellaIzquierda.huellavivaI = 'Cualquier cosa';
    // Le pasamos los parametro de entrada y evaluamos el retorno de nuestra funcion
    expect(component.nuevoEvaluarNulo(mockD, mockI)).toBeTruthy();

    // Ahora probamos para el caso que incumpla las condiciones anteriores
    component.huellaIzquierda.huellavivaI = '';
    component.huellaDerecha.huellavivaD = '';
    mockD = component.valorNuloAgente;
    mockI = component.valorNuloAgente;
    // Le pasamos los parametro de entrada y evaluamos el retorno de nuestra funcion
    expect(component.nuevoEvaluarNulo(mockD, mockI)).toBeFalsy();
  });


  it('Verificamos nuestra funcion habilitarBotonCapturar()', () => {
    // Asigamos true  siHabilitacion para verificar la primera condicion
    component.siHabilitacion = true;
    // Llamamos nuestra funcion y la evaluamos
    expect(component.habilitarBotonCapturar()).toBeTruthy();

    // Si siHabilitacion es false nos retornara false
    component.siHabilitacion = false;
    expect(component.habilitarBotonCapturar()).toBeFalsy();
  });


  it('Verificamos la funcion evaluarHabilitacionBotones(huellaViva,nist)', () => {
    // Mockeamos los valores de entrada de nuestra funcion
    let mockHuellaViva = 2;
    let mockNist = null;
    // Espiamos nuestra funcion habilitarBotones();
    const spyhabilitarBotones = spyOn(component, 'habilitarBotones');
    component.siNingunaHuellaRojo = '1';
    component.nivel_fake = mockHuellaViva + 1;
    // Llamamos nuestra funcion y le pasamos los valores de entrada
    component.evaluarHabilitacionBotones(mockHuellaViva, mockNist);
    // Verificamops que llame nuestro espia una vez
    expect(spyhabilitarBotones).toHaveBeenCalledTimes(1);

    // Ahora verificamos la segunda condicion
    component.siNoHuellaViva = '1';
    mockHuellaViva = null;
    // Llamamos nuestra funcion y le pasamos los valores de entrada
    component.evaluarHabilitacionBotones(mockHuellaViva, mockNist);
    // Verificamops que llame nuestro espia
    expect(spyhabilitarBotones).toHaveBeenCalledTimes(3);

    // Ahora verificamos la tercera condicion
    component.siNistRojo = '1';
    mockNist = 4;
    // Llamamos nuestra funcion y le pasamos los valores de entrada
    component.evaluarHabilitacionBotones(mockHuellaViva, mockNist);
    // Verificamops que llame nuestro espia
    expect(spyhabilitarBotones).toHaveBeenCalledTimes(6);

    // Ahora verificamos la cuarta condicion
    component.siNistAmarillo = '1';
    mockHuellaViva = component.umbralHuellaViva - 1;
    mockNist = 3;
    // Llamamos nuestra funcion y le pasamos los valores de entrada
    component.evaluarHabilitacionBotones(mockHuellaViva, mockNist);
    // Verificamops que llame nuestro espia
    expect(spyhabilitarBotones).toHaveBeenCalledTimes(7);

    // Evaluamos las quinta condicion para ello fakeamos la respuesta de la funcion nuevoEvaluarNulo
    spyOn(component, 'nuevoEvaluarNulo').and.callFake(() => true);
    component.siNoHuellaCapturada = '1';
    mockNist = 4;
    // Llamamos nuestra funcion y le pasamos los valores de entrada
    component.evaluarHabilitacionBotones(mockHuellaViva, mockNist);
    // Verificamops que llame nuestro espia
    expect(spyhabilitarBotones).toHaveBeenCalledTimes(9);

    // Evaluamos las sexta condicion para ello fakeamos la respuesta de la funcion nuevoEvaluarNulo
    spyOn(component, 'nuevoEvaluarNulo').and.callFake(() => false);
    component.siNingunaHuella = '1';
    component.huellaDerecha.huellaWSQD = '';
    component.huellaIzquierda.huellaWSQI = '';
    mockNist = 4;
    // Llamamos nuestra funcion y le pasamos los valores de entrada
    component.evaluarHabilitacionBotones(mockHuellaViva, mockNist);
    // Verificamops que llame nuestro espia
    expect(spyhabilitarBotones).toHaveBeenCalledTimes(11);


    // Evaluamos las sexta condicion para ello fakeamos la respuesta de la funcion nuevoEvaluarNulo
    component.siHuellaVivaAmarillo = '1';
    mockHuellaViva = component.nivel_fake;
    // Llamamos nuestra funcion y le pasamos los valores de entrada
    component.evaluarHabilitacionBotones(mockHuellaViva, mockNist);
    // Verificamops que llame nuestro espia
    expect(spyhabilitarBotones).toHaveBeenCalledTimes(14);
  });



  it('Verificamos la funcion  pruebaHuella()', () => {
    // Espiamos el evento emit de propagarHuellaDerecha
    const spypropagarHuellaDerecha = spyOn(component.propagarHuellaDerecha, 'emit');
    // Espiamos nuestro servicio huellaService
    const spyhuellaService = spyOn(huellaService, 'agregarHuellaDerecha');
    // Llamamos nuestra funcion
    component.pruebaHuella();
    // Verificamos nuestros espias fueran llamado
    expect(spypropagarHuellaDerecha).toHaveBeenCalled();
    expect(spyhuellaService).toHaveBeenCalled();

  });


  it('Verificamos la funcion evaluarNist(nist)', () => {
    // Mockeamos un valor de entrada para nuestra funcion
    let mockNist = '12345678905';
    // le pasamos el valor a nuestra funcion y la evaluamos , solo tomara los primeros 10 caracteres de numero en string
    const parseMockNist = parseInt(mockNist, 10).toString();
    expect(component.evaluarNist(mockNist)).toEqual(parseMockNist);

    // Caso contrario sea '' nos retorna ''
    mockNist = '';
    // le pasamos el valor a nuestra funcion y la evaluamos
    expect(component.evaluarNist(mockNist)).toEqual(mockNist);

  });

  it('verificamos la funcion evaluarHuellaViva(nivelHuellaViva)', () => {
    // Mockeamos el valor entrada de nuestra funcion
    let mockNivelHuellaViva = null;
    // Mockeamos el valor de la variable siValidarHuellaViva para probar la primera condicion cuando es false la funcion retorna true
    component.siValidarHuellaViva = 'false';
    // Llamamos nuestra funcion y la evaluamos
    expect(component.evaluarHuellaViva(mockNivelHuellaViva)).toBeTruthy();

    // Ahora verificamos para el caso contrario cuando nivelHuellaViva >= this.nivel_fake
    component.siValidarHuellaViva = 'true';
    mockNivelHuellaViva = component.nivel_fake + 1;
    // Llamamos nuestra funcion y la evaluamos
    expect(component.evaluarHuellaViva(mockNivelHuellaViva)).toBeTruthy();

    // Ahora para el caso contrario que no se cumpla ninguna de nuestras condiciones anteriores la funcion nos retornara false
    mockNivelHuellaViva = component.nivel_fake - 1;
    // Llamamos nuestra funcion y la evaluamos
    expect(component.evaluarHuellaViva(mockNivelHuellaViva)).toBeFalsy();

  });


  it('Verificamos la funcion mensajeEspera(lado:number)', () => {
    // Seteamos en nuestro localStorage el item dataResponseMejorHuella con valor null o dataResponseMejorHuella con valor ''
    localStorage.setItem('dataResponseMejorHuella', '');
    // Mockeamos el valor de entrada para la funcion
    let mockLado = 0;
    // Espiamos nuestra funcion setSendRespuesta
    // Y la fakeamos para evitar problemas por conversion a JSON de item dataResponseMejorHuella  del localSotrage
    const spysetSendRespuesta = spyOn(component, 'setSendRespuesta').and.callFake(() => { 'NADA!'; });
    // Llamamos nuestra funcion y le pasamos su valor de entrada
    component.mensajeEspera(mockLado);
    // Verificamos que llame nuestro espia de setSendRespuesta
    expect(spysetSendRespuesta).toHaveBeenCalledTimes(1);

    // Ahora verificamos para el caso contrario que mockLado sea diferente de 0
    mockLado = 1;
    // Llamamos nuestra funcion y le pasamos su valor de entrada
    component.mensajeEspera(mockLado);
    // Verificamos que llame nuestro espia de setSendRespuesta
    expect(spysetSendRespuesta).toHaveBeenCalledTimes(2);

    // Ahora verificamos la condicion cuando dataResponseMejorHuella es diferente de null o dataResponseMejorHuella diferente de ''
    const mockHuella = {
      dni: '12345678',
      codigoHuellaDerecha: '12343534534543',
      descripcionHuellaDerecha: 'Descripcion Der',
      codigoHuellaIzquierda: '12343534534543',
      descripcionHuellaIzquierda: 'Descripcion Izq'
    };
    localStorage.setItem('dataResponseMejorHuella', JSON.stringify(mockHuella));

    // y mockLado sea 0
    mockLado = 0;
    // Llamamos nuestra funcion y le pasamos su valor de entrada
    component.mensajeEspera(mockLado);
    // Verificamos que llame nuestro espia de setSendRespuesta
    expect(spysetSendRespuesta).toHaveBeenCalledTimes(3);

    // Para el caso contrario que lado sea diferente de 0
    mockLado = 5;
    // Llamamos nuestra funcion y le pasamos su valor de entrada
    component.mensajeEspera(mockLado);
    // Verificamos que llame nuestro espia de setSendRespuesta
    expect(spysetSendRespuesta).toHaveBeenCalledTimes(4);
  });

  it('Verificamos nuestra funcion evaluarNISTCaptura', () => {
    // Mockeamos el valor de entrada para nuestra funcion
    const mockCalidadNIST = 0;
    // Verificamos para este la funcion nos retorna false
    expect(component.evaluarNISTCaptura(mockCalidadNIST)).toBeFalsy();

    // Ahora verificamos para su caso contrario , cuando es diferente de 0 y calidadNIST es menor que la variable NISTCaptura
    component.NISTCaptura = 2;
    expect(component.evaluarNISTCaptura(1)).toBeTruthy();
  });

  it('Verificamos la funcion evaluarCodigoRespuestaCapturaDactilar(dataViaJsonP)', () => {
    // Mockeamos el valor de entrada para nuestra funcion
    const mockdataViaJsonP = {
      codigoRespuesta: component.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCES.CODIGO
    };
    // Pasamos el valor de entrada a nuestra funcion y verificamos que nos retorne true si es igual a constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCES.CODIGO
    expect(component.evaluarCodigoRespuestaCapturaDactilar(mockdataViaJsonP)).toBeTruthy();

    // Ahora verificamos para el caso contrario cuando es diferente deconstant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCES.CODIGO , retorna false
    mockdataViaJsonP.codigoRespuesta = 'Something';
    // Pasamos el valor de entrada a nuestra funcion y verificamos que retorne false
    expect(component.evaluarCodigoRespuestaCapturaDactilar(mockdataViaJsonP)).toBeFalsy();
  });


  it('Verificamos la funcion convertirFakeDetection(fakeDetection)', () => {
    // Mockeamos el valor de entrada para nuestra funcion
    let mockFakeDetection = 'Test,Prueba';
    // Pasamos el valor de entrada nuestra funcion y verificamos que nos retorne un replace('.',',')
    expect(component.convertirFakeDetection(mockFakeDetection)).toEqual(mockFakeDetection.replace('.', ','));

    // Para el caso contrario cuando el valor de entrada sea null nos retornara '0'
    mockFakeDetection = null;
    // Pasamos el valor de entrada nuestra funcion y nos retorne '0'
    expect(component.convertirFakeDetection(mockFakeDetection)).toEqual('0');
  });

  it('Verificamos la funcion mostrarNivelNist(nivelNist)', () => {
    // Mockeamos el valor de entrada para nuestra funcion
    let mocknivelNist = 2;
    // Verificamos la primera condicion cuando nivelNist sea igual rightImage
    expect(component.mostrarNivelNist(mocknivelNist)).toEqual(component.rightImage);
    // Ahora verificamos cuando nivelNist sea igual a 3 o '3'
    mocknivelNist = 3;
    // Verificamos la funcion nos retorne warningImage
    expect(component.mostrarNivelNist(mocknivelNist)).toEqual(component.warningImage);
    // Verificamos para el caso que el valor de nivelNist sea nivelNist > 3 && nivelNist <= 5
    // Verificamos la funcio nos retorne warningImage
    expect(component.mostrarNivelNist(mocknivelNist)).toEqual(component.warningImage);

    // Ahora verificamos cuando no se cumple ninguna de las condiciones anteriores , nos retorna nullImage
    mocknivelNist = 6;
    expect(component.mostrarNivelNist(mocknivelNist)).toEqual(component.nullImage);
  });


  it('Verificamos la funcion mostrarNivelFakeDetection(fakeDetection)', () => {
    // Mockeamos el valor de entrada para nuestra funcion
    let mockFakeDetection = 0;
    // Verificamos nuestra funcio nos retorne nullImage cuando fakeDetection sea 0
    expect(component.mostrarNivelFakeDetection(mockFakeDetection)).toEqual(component.nullImage);

    // Para el caso que fakeDetection >= this.nivel_fake retornara rightImage
    mockFakeDetection = component.nivel_fake + 1;
    // Verificamos nuestra funcion nos retorne rightImage
    expect(component.mostrarNivelFakeDetection(mockFakeDetection)).toEqual(component.rightImage);

    // Para el caso fakeDetection <= this.nivel_fake
    mockFakeDetection = component.nivel_fake - 1;
    // Verificamos nuestra funcion nos retorne wrongImage
    expect(component.mostrarNivelFakeDetection(mockFakeDetection)).toEqual(component.wrongImage);

    // Cuando no se cumple ninguna de las condiciones anteriores nos retornara nullImage
    mockFakeDetection = undefined;
    // Verificamos nuestra funcion nos retorne nullImage
    expect(component.mostrarNivelFakeDetection(mockFakeDetection)).toEqual(component.nullImage);
  });



  it('Verificamos nuestra funcion executeCapturaDactilarIzquierda()', () => {
    // Para probar nuestra primera condicion asigamos un valor menor que 4 a attemptI
    component.attemptI  = 1;
    // Espiamos nuestra funcion mensajeEspera
    const spymensajeEspera = spyOn(component, 'mensajeEspera').and.callThrough();
    // Espiamos nuestro servicio serviceCapturaIzquierda
    const spyserviceCapturaIzquierda = spyOn(servicio, 'serviceCapturaIzquierda').and.callThrough();
    // Llamamos nuestra funcion
    component.executeCapturaDactilarIzquierda();
    // Verificamos que nuestro servicio fuera llamada
    expect(spyserviceCapturaIzquierda).toHaveBeenCalled();
    // Verificamos que nuestra funcionmensajeEspera fuera llamada
    expect(spymensajeEspera).toHaveBeenCalled();

    // Ahora mockeamos el dataResponse de nuestro servicio para probar las condiciones
    // para ello asigamos codigoRespuesta el valor de constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCES.CODIGO
    // Asigamos un valor a la variable nivel_fake
    component.nivel_fake = 2;
    const mockDataResponse = {
      codigoRespuesta: component.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCES.CODIGO,
      // Asigamos este valor menor que constant.NISTMinima para probar la primera validacion
      calidadNIST: 2,
      // Asigamos este valor mayor que component.nivel_fake para probar la primera validacion
      fakeDetection: '3',
      huellaPNG: 'Test_HuellaPNG',
      huellaTemplateCompaCard: 'Test huellaTemplateCompaCard',
      huellaWSQ: 'Test huellaWSQ',
      huellaTemplate: 'Test huellaTemplate',

    };
    // espiamos nuestra funcion setSendHuellaI()
    const spysetSendHuellaI = spyOn(component, 'setSendHuellaI');
    // Espiamos nuestra funcion mostrarNivelNist()
    const spymostrarNivelNist = spyOn(component, 'mostrarNivelNist').and.callThrough();
    // Espiamos nuestra funcion mostrarNivelFakeDetection()
    const spymostrarNivelFakeDetection = spyOn(component, 'mostrarNivelFakeDetection').and.callThrough();
    // Espiamos nuestra funcion setSendRespuesta()
    const spysetSendRespuesta = spyOn(component, 'setSendRespuesta').and.callThrough();
    // Espiamos nuestra funcion habilitarBotones()
    const spyhabilitarBotones = spyOn(component, 'habilitarBotones').and.callThrough();
    // Espiamos nuestra funcion liberarDispositivo()
    const spyliberarDispositivo = spyOn(component, 'liberarDispositivo').and.callThrough();

    // Espiamos nuestra funcion executeCapturaDactilarIzquierda()
    const spyexecuteCapturaDactilarIzquierda = spyOn(component, 'executeCapturaDactilarIzquierda').and.callThrough();
    // Llamamos nuestra servicio con callFake para que nos retorne el mockDataResponse
    const spyServiceFake = spyOn(servicio, 'serviceCapturaIzquierda').and.callFake(() => {
      return of(mockDataResponse);
    });
    // Llamamos nuestra funcion
    component.executeCapturaDactilarIzquierda();
    // Verificamos que llame nuestro servicio Fake
    expect(spyServiceFake).toHaveBeenCalled();
    // Verificamos que llame nuestra funcion setSendHuellaD
    expect(spysetSendHuellaI).toHaveBeenCalled();
    // Verificamos que llame nuestra funcion spymostrarNivelNist
    expect(spymostrarNivelNist).toHaveBeenCalled();
    // Verificamos que llame nuestra funcion mostrarNivelFakeDetection
    expect(spymostrarNivelFakeDetection).toHaveBeenCalled();
    // Verificamos que llame nuestra funcion setSendRespuesta
    expect(spysetSendRespuesta).toHaveBeenCalled();
    // Verificamos que llame nuestra funcion habilitarBotones
    expect(spyhabilitarBotones).toHaveBeenCalled();
    // Verificamos que llame nuestra funcion liberarDispositivo
    expect(spyliberarDispositivo).toHaveBeenCalled();


    // Ahora verificamos la condicion contraria
    // Para ello asigamos valores que no cumplen la condicion anterior

    component.nivel_fake = 3;
    mockDataResponse.fakeDetection = '1';
    mockDataResponse.calidadNIST = 999999;
    const test = 3;
    component.attemptI = test;
    // Espiamos nuestra funcion evaluarHabilitacionBotones
    const spyevaluarHabilitacionBotones = spyOn(component, 'evaluarHabilitacionBotones');

    spyOn(component, 'validarIntento').and.callFake(() => true);
    // Llamamos nuestra funcion
    component.executeCapturaDactilarIzquierda();
    // Verificamos que llame nuestra funcion executeCapturaDactilarIzquierda fuera llamada 1 veces hasta este punto en total
    expect(spyexecuteCapturaDactilarIzquierda).toHaveBeenCalledTimes(2);
    // Verificamos que llame nuestra funcion setSendHuellaI
    expect(spysetSendHuellaI).toHaveBeenCalledTimes(2);
    // Verificamos que llame nuestra funcion setSendRespuesta
    expect(spysetSendRespuesta).toHaveBeenCalledTimes(4);
    // Verificamos que llame nuestra funcion evaluarHabilitacionBotones
    expect(spyevaluarHabilitacionBotones).toHaveBeenCalled();
    // Verificamos que llame nuestra funcion liberarDispositivo
    expect(spyliberarDispositivo).toHaveBeenCalledTimes(2);
    // Verificamos que llame nuestra funcion habilitarBotones
    expect(spyhabilitarBotones).toHaveBeenCalledTimes(1);
    // Verificamos que llame nuestra funcion liberarDispositivo
    expect(spyliberarDispositivo).toHaveBeenCalledTimes(2);

    component.nivel_fake = 3;
    mockDataResponse.fakeDetection = '1';
    mockDataResponse.calidadNIST = 999999;
    component.attemptI = test;
    // Ahora verificamos para el caso contrario cuando validarIntento sea false
    spyOn(component, 'validarIntento').and.callFake(() => false);
    // Llamamos nuestra funcion
    component.executeCapturaDactilarIzquierda();
    expect(spyexecuteCapturaDactilarIzquierda).toHaveBeenCalledTimes(4);

    // Ahora probamos para el caso que codigoRespuesta sea '40000' o 'null' o ''
    component.attemptI = 1;
    mockDataResponse.codigoRespuesta = 'null';
    component.nivel_fake = 2;
    mockDataResponse.fakeDetection = undefined;
    // Fakeamos un retorno false para la funcion evaluarCodigoRespuesta
    spyOn(component, 'evaluarCodigoRespuesta').and.callFake(() => false);
    spyOn(component, 'validarIntento').and.callFake(() => true);

    // Llamamos nuestra funcion
    component.executeCapturaDactilarIzquierda();
    // Verificamos que llame nuestra funcion evaluarHabilitacionBotones
    expect(spyevaluarHabilitacionBotones).toHaveBeenCalledTimes(2);
    // Verificamos que llame nuestra funcion setSendRespuesta
    expect(spysetSendRespuesta).toHaveBeenCalledTimes(8);
    // Verificamos que llame nuestra funcion liberarDispositivo
    expect(spyliberarDispositivo).toHaveBeenCalledTimes(4);

    component.attemptI = 1;
    // Ahora verificamos para el caso dataResponseCapturaD.calidadNIST > this.constant.NISTMinima
    // Para ello asiganmos undefined a codigoRespuesta
    mockDataResponse.codigoRespuesta = undefined;
    mockDataResponse.calidadNIST = component.constant.NISTMinima + 1;
    // Llamamos nuestra funcion
    component.executeCapturaDactilarIzquierda();
    // Verificamos que llame nuestros espias
    expect(spysetSendHuellaI).toHaveBeenCalledTimes(3);
    expect(spyevaluarHabilitacionBotones).toHaveBeenCalledTimes(3);
    expect(spysetSendHuellaI).toHaveBeenCalledTimes(3);
    expect(spysetSendRespuesta).toHaveBeenCalledTimes(10);
    expect(spyliberarDispositivo).toHaveBeenCalledTimes(5);

    // Ahora verificamos que ninguna de la condiciones anteriores se cumpla
    component.attemptI = 1;
    mockDataResponse.fakeDetection = undefined;
    mockDataResponse.calidadNIST = undefined;
    mockDataResponse.huellaPNG = undefined;
    mockDataResponse.huellaTemplateCompaCard = undefined;
    mockDataResponse.huellaWSQ = undefined;
    mockDataResponse.huellaTemplate = undefined;
    // Llamamos nuestra funcion
    component.executeCapturaDactilarIzquierda();
    // Verificamos que llame nuestros espias
    expect(spyevaluarHabilitacionBotones).toHaveBeenCalledTimes(4);
    expect(spysetSendRespuesta).toHaveBeenCalledTimes(12);
    expect(spyliberarDispositivo).toHaveBeenCalledTimes(6);

    // Ahora verificamos para el caso contrario
    // forzamos a validarIntento A RETORNAR FALSE
    spyOn(component, 'validarIntento').and.callFake(() => false);
    // Llamamos nuestra funcion
    component.executeCapturaDactilarIzquierda();
    expect(spysetSendRespuesta).toHaveBeenCalledTimes(15);
    expect(spyliberarDispositivo).toHaveBeenCalledTimes(7);
  });

// Agregamos fakeAsync para poder simular con tick un setTimeout
  it('Verificamos la funcion executeCapturaAmbasHuellas()', fakeAsync(() => {
    // Espiamos nuestra funcion mensajeEspera
    const spymensajeEspera = spyOn(component, 'mensajeEspera');
    // Asigamos el valor attemptD=1
    component.attemptD = 1;
    // Espiamos nuestro servicio serviceCapturaDerecha
    const spyserviceCapturaDerecha = spyOn(servicio, 'serviceCapturaDerecha').and.callThrough();
    // Llamamos nuestra funcion executeCapturaAmbasHuellas()
    component.executeCapturaAmbasHuellas();
    // Verificamos que que llamara nuestros espias
    expect(spymensajeEspera).toHaveBeenCalled();
    expect(spyserviceCapturaDerecha).toHaveBeenCalled();

    // Para este caso fakeamos un mockDataResponseD que nos retornara nuestro servicioCapturaDerecha
    component.nivel_fake = 2;
    const mockDataResponseD = {
      codigoRespuesta: component.constant.CAPTURA_DACTILAR.INICIAR_CAPTURA.SUCCES.CODIGO,
      // Asigamos este valor menor que constant.NISTMinima para probar la primera validacion
      calidadNIST: 2,
      // Asigamos este valor mayor que component.nivel_fake para probar la primera validacion
      fakeDetection: '3',
      huellaPNG: 'Test_HuellaPNG',
      huellaTemplateCompaCard: 'Test huellaTemplateCompaCard',
      huellaWSQ: 'Test huellaWSQ',
      huellaTemplate: 'Test huellaTemplate',
    };
    // Fakeamos el retorno con and.callFake
    const spyServiceDerechaFake = spyOn(servicio, 'serviceCapturaDerecha').and.callFake(() => {
      return of(mockDataResponseD);
    });
    // Espiamos nuestra funcion setSendHuellaD
    const spysetSendHuellaD = spyOn(component, 'setSendHuellaD');
    // Espiamos nuestra funcion mostrarNivelFakeDetection
    const spymostrarNivelFakeDetection = spyOn(component, 'mostrarNivelFakeDetection');
    // Espiamos nuestra funcion setSendRespuesta
    const spysetSendRespuesta = spyOn(component, 'setSendRespuesta');
    // Espiamos nuestra funcion executeCapturaDactilarIzquierda
    const spyexecuteCapturaDactilarIzquierda = spyOn(component, 'executeCapturaDactilarIzquierda').and.callThrough();
    // Llamamos nuestra funcion executeCapturaAmbasHuellas()

    component.executeCapturaAmbasHuellas();
     // Verificamos que llame nuestro servicio FAKE
    expect(spyServiceDerechaFake).toHaveBeenCalled();
    // Verificamos que llame nuestro espia de setSendHuellaD
    expect(spysetSendHuellaD).toHaveBeenCalled();
    // Verificamos que llame nuestro espia de mostrarNivelFakeDetection
    expect(spymostrarNivelFakeDetection).toHaveBeenCalled();
    // Verificamos que llame nuestro espia de setSendRespuesta
    expect(spysetSendRespuesta).toHaveBeenCalled();
    // Verificamos que llame nuestro espia de executeCapturaDactilarIzquierda
    // Agregamos tick para simular un setTimeout para que verificar que fue llamada nuestra funcion adecuadamente
    tick(3001);
    expect(spyexecuteCapturaDactilarIzquierda).toHaveBeenCalled();
    // Agregamos flush para evitar errores
    flush();





     // Ahora verificamos la condicion contraria
    // Para ello asigamos valores que no cumplen la condicion anterior
    component.nivel_fake = 3;
    mockDataResponseD.fakeDetection = '1';
    mockDataResponseD.calidadNIST = 999999;
    const test = 3;
    component.attemptD = test;
    // Espiamos nuestra funcion liberarDispositivo
    const spyliberarDispositivo = spyOn(component, 'liberarDispositivo');
    // Forzamos que la funcion validarIntento nos devuelva true para probar la condicion
    spyOn(component, 'validarIntento').and.callFake(() => true);
    component.siNoHuellaCapturada = null;
    // Espiamos nuestra funcion mostrarNivelNist
     // Llamamos nuestra funcion
    component.executeCapturaAmbasHuellas();
    // verificamos que llame nuestros espias
    expect(spysetSendHuellaD).toHaveBeenCalledTimes(2);
    expect(spysetSendRespuesta).toHaveBeenCalledTimes(2);
    expect(spyliberarDispositivo).toHaveBeenCalledTimes(1);


     // Ahora verificamos la condicion contraria
    // Para ello asigamos valores que no cumplen la condicion anterior
    component.nivel_fake = 3;
    mockDataResponseD.fakeDetection = '1';
    mockDataResponseD.calidadNIST = 999999;
    component.attemptD = test;
    // Forzamos que la funcion validarIntento nos devuelva true para probar la condicion
    spyOn(component, 'validarIntento').and.callFake(() => true);
    component.siNoHuellaCapturada = '1';
    // Espiamos nuestra funcion mostrarNivelNist
    const spymostrarNivelNist = spyOn(component, 'mostrarNivelNist');
     // Llamamos nuestra funcion
    component.executeCapturaAmbasHuellas();
    // Verificamos que llame nuestra funcion spysetSendHuellaD
    expect(spysetSendHuellaD).toHaveBeenCalledTimes(3);
    // Verificamos que se asigne 1 a attemptI
    expect(component.attemptI).toBe(1);
    // Verificamos que llame nuestra funcion spymostrarNivelFakeDetection
    expect(spymostrarNivelFakeDetection).toHaveBeenCalledTimes(3);
    // Verificamos que llame nuestro espia de mostrarNivelNist
    expect(spymostrarNivelNist).toHaveBeenCalled();
    // Verificamos que llame nuestro espia de executeCapturaDactilarIzquierda
    // Agregamos tick para simular un setTimeout para que verificar que fue llamada nuestra funcion adecuadamente
    tick(3001);
    expect(spyexecuteCapturaDactilarIzquierda).toHaveBeenCalledTimes(2);
    // Agregamos flush para evitar errores
    flush();


    // Ahora probamos para el caso que codigoRespuesta sea '40000' o 'null' o ''
    component.attemptD = 1;
    mockDataResponseD.codigoRespuesta = 'null';
    component.nivel_fake = 2;
    mockDataResponseD.fakeDetection = undefined;
    // Fakeamos un retorno false para la funcion validarIntento
    spyOn(component, 'validarIntento').and.callFake(() => true);
    // Asigamos '1' a la variable siNoHuellaCapturada , para probar su condicion
    component.siNoHuellaCapturada = null;
    // Llamamos nuestra funcion
    component.executeCapturaAmbasHuellas();
    // Verificamos que llame nuestros espias
    expect(spysetSendRespuesta).toHaveBeenCalledTimes(3);
    expect(spyliberarDispositivo).toHaveBeenCalledTimes(2);


    // Ahora probamos para el caso contrario al anterior
    component.attemptD = 1;
    mockDataResponseD.codigoRespuesta = 'null';
    component.nivel_fake = 2;
    mockDataResponseD.fakeDetection = undefined;
    // Fakeamos un retorno false para la funcion validarIntento
    spyOn(component, 'validarIntento').and.callFake(() => true);
    component.siNoHuellaCapturada = '1';
    component.siNingunaHuella = '1';
    // Llamamos nuestra funcion
    component.executeCapturaAmbasHuellas();
    // Verificamos que se asigne 1 a attemptI
    expect(component.attemptI).toBe(1);
    // Verificamos que se asigne 1 a attemptI
    expect(component.attemptI).toBe(1);
    // Verificamos que llame nuestro espia de executeCapturaDactilarIzquierda
    // Agregamos tick para simular un setTimeout para que verificar que fue llamada nuestra funcion adecuadamente
    tick(3001);
    expect(spyexecuteCapturaDactilarIzquierda).toHaveBeenCalledTimes(3);
    // Agregamos flush para evitar errores
    flush();

    // Ahora verificamos para el caso dataResponseCapturaD.calidadNIST > this.constant.NISTMinima
    // Para ello asiganmos undefined a codigoRespuesta
    component.attemptD = 1;
    mockDataResponseD.codigoRespuesta = undefined;
    spyOn(component, 'validarIntento').and.callFake(() => true);
    mockDataResponseD.calidadNIST = component.constant.NISTMinima + 1;
    // Llamamos nuestra funcion
    component.executeCapturaAmbasHuellas();
    // Verificamos que llame nuestros espias
    expect(spysetSendHuellaD).toHaveBeenCalledTimes(4);
    expect(spysetSendHuellaD).toHaveBeenCalledTimes(4);
    expect(spysetSendRespuesta).toHaveBeenCalledTimes(3);
    expect(spyliberarDispositivo).toHaveBeenCalledTimes(2);
    // Verificamos que llame nuestro espia de executeCapturaDactilarIzquierda
    // Agregamos tick para simular un setTimeout para que verificar que fue llamada nuestra funcion adecuadamente
    tick(3001);
    expect(spyexecuteCapturaDactilarIzquierda).toHaveBeenCalledTimes(4);
    // Agregamos flush para evitar errores
    flush();

    // Ahora verificamos para el caso contrario
    component.attemptD = 1;
    mockDataResponseD.codigoRespuesta = undefined;
    spyOn(component, 'validarIntento').and.callFake(() => true);
    mockDataResponseD.calidadNIST = component.constant.NISTMinima + 1;
    component.siNoHuellaCapturada = undefined;
    component.siNingunaHuella = undefined;
    // Llamamos nuestra funcion
    component.executeCapturaAmbasHuellas();
    // Verificamos que llame nuestros espias
    expect(spysetSendRespuesta).toHaveBeenCalledTimes(4);
    expect(spyliberarDispositivo).toHaveBeenCalledTimes(3);

    // Ahora verificamos que ninguna de la condiciones anteriores se cumpla
    component.attemptD = 1;
    mockDataResponseD.fakeDetection = undefined;
    mockDataResponseD.calidadNIST = undefined;
    mockDataResponseD.huellaPNG = undefined;
    mockDataResponseD.huellaTemplateCompaCard = undefined;
    mockDataResponseD.huellaWSQ = undefined;
    mockDataResponseD.huellaTemplate = undefined;
    component.siNoHuellaCapturada = '1';
    spyOn(component, 'validarIntento').and.callFake(() => true);
    // Llamamos nuestra funcion
    component.executeCapturaAmbasHuellas();
    // Verificamos que el valor de attemptI  sea 1
    expect(component.attemptI).toBe(1);
    // Verificamos que llame nuestro espia de executeCapturaDactilarIzquierda
    // Agregamos tick para simular un setTimeout para que verificar que fue llamada nuestra funcion adecuadamente
    tick(3001);
    expect(spyexecuteCapturaDactilarIzquierda).toHaveBeenCalledTimes(5);
    // Agregamos flush para evitar errores
    flush();

    // Verificamos para su condicion contraria
    spyOn(component, 'validarIntento').and.callFake(() => true);
    component.siNoHuellaCapturada = undefined;
    component.siNingunaHuella = undefined;
    // Llamamos nuestra funcion
    component.executeCapturaAmbasHuellas();
    // Verificamos que llame nuestros espias
    expect(spysetSendRespuesta).toHaveBeenCalledTimes(5);
    expect(spyliberarDispositivo).toHaveBeenCalledTimes(4);

    // Espiamos nuestra funcion executeCapturaAmbasHuellas
    const spyexecuteCapturaAmbasHuellas = spyOn(component, 'executeCapturaAmbasHuellas').and.callThrough();
    // Verificamos la condicion contraria para validarIntento false
    spyOn(component, 'validarIntento').and.callFake(() => false);
    // Llamamos nuestra funcion
    component.executeCapturaAmbasHuellas();
    // Verificamos que llame nuestro espia de spyexecuteCapturaAmbasHuellas
    expect(spyexecuteCapturaAmbasHuellas).toHaveBeenCalled();



  }));

  it('Verificamos la funcion liberarDispositivo()', () => {
    // Espiamos nuestro servicio serviceEndCapturaDactilar
    const spyserviceEndCapturaDactilar = spyOn(servicio, 'serviceEndCapturaDactilar').and.callThrough();
    // Llamamos nuestra funcion
    component.liberarDispositivo();
    // Verificamos que la variable siHabilitacion sea TRUE
    expect(component.siHabilitacion).toBeTruthy();
    // Verificamos que llame nuestro servicio serviceEndCapturaDactilar
    expect(spyserviceEndCapturaDactilar).toHaveBeenCalled();
  });



});
