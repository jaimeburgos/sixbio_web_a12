import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { GestionUsuariosRoutingModule } from './gestion-usuarios-routing.module';
import { GestionUsuariosComponent } from './gestion-usuarios.component';
import { CrearUsuarioComponent, EditarUsuarioComponent, DetalleUsuarioComponent } from './components';
import { NgxPaginationModule } from 'ngx-pagination';
import { Routes } from '@angular/router';
import { FilterPipe } from 'src/app/pipes/filter.pipe';
import { ExporterService } from 'src/app/services/exporter.service';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { ReactiveFormsModule } from '@angular/forms';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
// DatePicker
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
import { DatePipe } from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';
import { InterceptorService } from 'src/app/services/interceptor.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { EnvService } from 'src/app/env.service';
defineLocale('es', esLocale);
@NgModule({
  declarations: [
      GestionUsuariosComponent,
      CrearUsuarioComponent,
      EditarUsuarioComponent,
      DetalleUsuarioComponent,
      FilterPipe
    ],
  imports: [
    CommonModule,
    GestionUsuariosRoutingModule,
    NgxSpinnerModule,
    NgxPaginationModule,
    BsComponentModule,
    FormsModule,
    ReactiveFormsModule,
    TranslateModule,
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
  ],
  providers: [
    ExporterService,
    BsLocaleService,
    TranslateService,
    DatePipe,
    EnvService,
    ServicioService ,
    ExporterService, {provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true}]
})
export class GestionUsuariosModule { }
