import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GestionUsuariosComponent } from './gestion-usuarios.component';
import { CrearUsuarioComponent, EditarUsuarioComponent, DetalleUsuarioComponent } from './components';

const routes: Routes = [
    {path: '',
    children: [
        {path: '', component: GestionUsuariosComponent},
        {path: 'editar-usuario', component: EditarUsuarioComponent},
        {path: 'crear-usuario', component: CrearUsuarioComponent},
        {path: 'detalle-usuario', component: DetalleUsuarioComponent }
    ]
    }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GestionUsuariosRoutingModule { }
