import { async, ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CrearUsuarioComponent } from './crear-usuario.component';
import { CommonModule, DatePipe } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing'; // Recordar usar RouterTesting NO usar Router
import { NgxPaginationModule } from 'ngx-pagination';
import { HttpClientTestingModule} from '@angular/common/http/testing';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { EnvService } from 'src/app/env.service';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { TranslateModule, TranslateService } from '@ngx-translate/core'; // Faltaba importar Translate Service
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { of, Subject, throwError } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';

class FakeRouter {
  navigate(params) {
  }
}

class FakeActivatedRoute {
  // params: Observable<any> = Observable.empty();
  private subject = new Subject();
  push( valor ) {
    this.subject.next( valor );
  }
  get params() {
    return this.subject.asObservable();
  }
}

fdescribe('CrearUsuarioComponent', () => {
  let component: CrearUsuarioComponent;
  let fixture: ComponentFixture<CrearUsuarioComponent>;
  let translateService: TranslateService;
  let servicio: ServicioService;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearUsuarioComponent , ToastComponent],
      imports: [
        CommonModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService ,
        {provide: Router , useClass: FakeRouter},
        {provide: ToastrService, useClass: ToastrService},
        { provide: ActivatedRoute, useClass: FakeActivatedRoute },
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    translateService = TestBed.inject(TranslateService);
    servicio = TestBed.inject(ServicioService);
    fixture = TestBed.createComponent(CrearUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Componente CrearUsuario creado correctamente', () => {
    expect(component).toBeTruthy();
  });

  it('usuario es obligatorio en crearUserForm', () => {
    const usuario = component.crearUserForm.get('usuario');
    usuario.setValue('');
    expect(usuario.valid).toBeFalsy();
    usuario.setValue(null);
    expect(usuario.valid).toBeFalsy();
  });

  it('apPaterno es obligatorio en crearUserForm', () => {
    const apPaterno = component.crearUserForm.get('apPaterno');
    apPaterno.setValue('');
    expect(apPaterno.valid).toBeFalsy();
    apPaterno.setValue(null);
    expect(apPaterno.valid).toBeFalsy();
  });

  it('nombre es obligatorio en crearUserForm', () => {
    const nombre = component.crearUserForm.get('nombre');
    nombre.setValue('');
    expect(nombre.valid).toBeFalsy();
    nombre.setValue(null);
    expect(nombre.valid).toBeFalsy();
  });

  it('apMaterno es obligatorio en crearUserForm', () => {
    const apMaterno = component.crearUserForm.get('apMaterno');
    apMaterno.setValue('');
    expect(apMaterno.valid).toBeFalsy();
    apMaterno.setValue(null);
    expect(apMaterno.valid).toBeFalsy();
  });

  it('apMaterno es obligatorio en crearUserForm', () => {
    const fecNac = component.crearUserForm.get('fecNac');
    fecNac.setValue('');
    expect(fecNac.valid).toBeFalsy();
    fecNac.setValue(null);
    expect(fecNac.valid).toBeFalsy();
  });

  it('tipoDoc es obligatorio en crearUserForm', () => {
    const tipoDoc = component.crearUserForm.get('tipoDoc');
    tipoDoc.setValue('');
    expect(tipoDoc.valid).toBeFalsy();
    tipoDoc.setValue(null);
    expect(tipoDoc.valid).toBeFalsy();
  });

  it('numDoc es obligatorio en crearUserForm', () => {
    const numDoc = component.crearUserForm.get('tipoDoc');
    numDoc.setValue('');
    expect(numDoc.valid).toBeFalsy();
    numDoc.setValue(null);
    expect(numDoc.valid).toBeFalsy();
  });

  it('correo es obligatorio en crearUserForm', () => {
    const correo = component.crearUserForm.get('correo');
    correo.setValue('');
    expect(correo.valid).toBeFalsy();
    correo.setValue(null);
    expect(correo.valid).toBeFalsy();
  });

  it('rol es obligatorio en crearUserForm', () => {
    const rol = component.crearUserForm.get('rol');
    rol.setValue('');
    expect(rol.valid).toBeFalsy();
    rol.setValue(null);
    expect(rol.valid).toBeFalsy();
  });

  it('Campo "usuario" puede tener maximo 20 caracteres crearUserForm', () => {
    const usuario = component.crearUserForm.get('usuario');
    usuario.setValue('12345678901234567890');
    expect(usuario.valid).toBeTruthy();
    usuario.setValue('123456789012345678901');
    expect(usuario.valid).toBeFalsy();
  });

  it('Campo "usuario" debe tener minimo 5 caracteres crearUserForm', () => {
    const usuario = component.crearUserForm.get('usuario');
    usuario.setValue('12345');
    expect(usuario.valid).toBeTruthy();
    usuario.setValue('1234');
    expect(usuario.valid).toBeFalsy();
  });

  it('Campo "nombre" puede tener maximo 50 caracteres crearUserForm', () => {
    const nombre = component.crearUserForm.get('nombre');
    nombre.setValue('12345678901234567890123456789012345678901234567890');
    expect(nombre.valid).toBeTruthy();
    nombre.setValue('123456789012345678901234567890123456789012345678901');
    expect(nombre.valid).toBeFalsy();
  });

  it('Campo "nombre" puede tener minimo 1 caracter crearUserForm', () => {
    const nombre = component.crearUserForm.get('nombre');
    nombre.setValue('1');
    expect(nombre.valid).toBeTruthy();
    nombre.setValue('');
    expect(nombre.valid).toBeFalsy();
  });

  it('Campo "apPaterno" puede tener maximo 50 caracteres crearUserForm', () => {
    const apPaterno = component.crearUserForm.get('apPaterno');
    apPaterno.setValue('12345678901234567890123456789012345678901234567890');
    expect(apPaterno.valid).toBeTruthy();
    apPaterno.setValue('123456789012345678901234567890123456789012345678901');
    expect(apPaterno.valid).toBeFalsy();
  });

  it('Campo "apPaterno" puede tener minimo 1 caracter crearUserForm', () => {
    const apPaterno = component.crearUserForm.get('apPaterno');
    apPaterno.setValue('1');
    expect(apPaterno.valid).toBeTruthy();
    apPaterno.setValue('');
    expect(apPaterno.valid).toBeFalsy();
  });

  it('Campo "apMaterno" puede tener maximo 50 caracteres crearUserForm', () => {
    const apMaterno = component.crearUserForm.get('apMaterno');
    apMaterno.setValue('12345678901234567890123456789012345678901234567890');
    expect(apMaterno.valid).toBeTruthy();
    apMaterno.setValue('123456789012345678901234567890123456789012345678901');
    expect(apMaterno.valid).toBeFalsy();
  });

  it('Campo "apMaterno" puede tener minimo 1 caracter crearUserForm', () => {
    const apMaterno = component.crearUserForm.get('apMaterno');
    apMaterno.setValue('1');
    expect(apMaterno.valid).toBeTruthy();
    apMaterno.setValue('');
    expect(apMaterno.valid).toBeFalsy();
  });

  it('Verificar Longitud Maxima y Minima segun tipo Documento al Numero Documento', () => {
    // Obtenemos los formControl segun sus nombres
    const tipoDoc = component.crearUserForm.get('tipoDoc');
    const numDoc = component.crearUserForm.get('numDoc');
    // Les seteamos un valor segun su tipo definido en el Switch , caso DNI
    tipoDoc.setValue(Constant.TIPO_DOC_DNI);
    // Llamamos nuestro componente
    component.onChangeTipoDoc();
    // Verificamos que se cumplan los Validators
    expect(component.valueMaxLenght).toBe(8);
    expect(component.valueMinLenght).toBe(8);
    numDoc.setValue('12345678');
    expect(numDoc.valid).toBeTruthy();
    numDoc.setValue('123456789');
    expect(numDoc.valid).toBeFalsy();
    numDoc.setValue('1234567');
    expect(numDoc.valid).toBeFalsy();
    // Validamos para el caso Pasaporte
    tipoDoc.setValue(Constant.TIPO_DOC_PASAPORTE);
    component.onChangeTipoDoc();
    expect(component.valueMaxLenght).toBe(12);
    expect(component.valueMinLenght).toBe(12);
    numDoc.setValue('123456789012');
    expect(numDoc.valid).toBeTruthy();
    numDoc.setValue('12345678901');
    expect(numDoc.valid).toBeFalsy();
    numDoc.setValue('1234567890123');
    expect(numDoc.valid).toBeFalsy();
    // Validamos para el caso TIPO_DOC_RUC
    tipoDoc.setValue(Constant.TIPO_DOC_RUC);
    component.onChangeTipoDoc();
    expect(component.valueMaxLenght).toBe(11);
    expect(component.valueMinLenght).toBe(11);
    numDoc.setValue('12345678901');
    expect(numDoc.valid).toBeTruthy();
    numDoc.setValue('123456789012');
    expect(numDoc.valid).toBeFalsy();
    numDoc.setValue('1234567890');
    expect(numDoc.valid).toBeFalsy();
    // Ahora verificamos para el caso Default
    tipoDoc.setValue('Default o cualquier cosa');
    component.onChangeTipoDoc();
    expect(numDoc.valid).toBeFalsy();
  });

  // Usuario solo permite letras y numeros en crearUserForm
  it('Usuario con caracteres especiales en FormLogin', () => {
    const user = component.crearUserForm.get('usuario');
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b'];
    const U_No_Permitidos =
      ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
      '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
      '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]', '          '];

      // Bucle para caracteres permitidos en usuario crearUserForm
    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
        user.setValue(U_Permitidos[j]);
        expect(user.valid).toBe(true);
        // Cuando sea verdadero user.valid! no se cumple la condicion de verificacion
        if ( !user.valid) {
          console.log('Fallo en el caracter Permitido ' + U_Permitidos[j] );
        }
    }
      // Bucle para caracteres no permitidos en Usuario crearUserForm
    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
        user.setValue(U_No_Permitidos[i]);
        expect(user.valid).toBe(false);
        // Cuando sea verdadero no se cumple la condicion de verificacion
        if (user.valid) {
          console.log('Fallo en el caracter No Permitido ' + U_No_Permitidos[i] );
        }
      }
  });

  it('nombre con caracteres especiales en crearUserForm', () => {
    const nombre = component.crearUserForm.get('nombre');
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b', 'c123 bbb 4b', ' sdfds sdfsd'];
    const U_No_Permitidos =
      ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
      '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
      '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]'];
    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      nombre.setValue(U_Permitidos[j]);
        expect(nombre.valid).toBe(true);
        if ( !nombre.valid) {
          console.log('Fallo en el caracter Permitido ' + U_Permitidos[j] );
        }
    }
    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      nombre.setValue(U_No_Permitidos[i]);
        expect(nombre.valid).toBe(false);
        if (nombre.valid) {
          console.log('Fallo en el caracter No Permitido ' + U_No_Permitidos[i] );
        }
      }
  });

  it('apMaterno con caracteres especiales en crearUserForm', () => {
    const apMaterno = component.crearUserForm.get('apMaterno');
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b', 'c123 bbb 4b', ' sdfds sdfsd'];
    const U_No_Permitidos =
      ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
      '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
      '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]'];
    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      apMaterno.setValue(U_Permitidos[j]);
        expect(apMaterno.valid).toBe(true);
        if ( !apMaterno.valid) {
          console.log('Fallo en el caracter Permitido ' + U_Permitidos[j] );
        }
    }
    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      apMaterno.setValue(U_No_Permitidos[i]);
        expect(apMaterno.valid).toBe(false);
        if (apMaterno.valid) {
          console.log('Fallo en el caracter No Permitido ' + U_No_Permitidos[i] );
        }
      }
  });

  it('apPaterno con caracteres especiales en crearUserForm', () => {
    const apPaterno = component.crearUserForm.get('apPaterno');
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b', 'c123 bbb 4b', ' sdfds sdfsd'];
    const U_No_Permitidos =
      ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
      '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
      '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]'];
    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      apPaterno.setValue(U_Permitidos[j]);
        expect(apPaterno.valid).toBe(true);
        if ( !apPaterno.valid) {
          console.log('Fallo en el caracter Permitido ' + U_Permitidos[j] );
        }
    }
    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      apPaterno.setValue(U_No_Permitidos[i]);
        expect(apPaterno.valid).toBe(false);
        if (apPaterno.valid) {
          console.log('Fallo en el caracter No Permitido ' + U_No_Permitidos[i] );
        }
      }
  });

  /*  it('correo es cada maxima de caracteres es 100 en crearUserForm', () => {

      const correo = component.crearUserForm.get('correo');
      correo.setValue('1234567890123456789012345678901234567890123456789012345678901234567890123456789012@test.com');
      expect(correo.valid).toBeTruthy();
      correo.setValue('testing_2-@test.com');
      expect(correo.valid).toBeTruthy();
      correo.setValue('1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901@test23.com');
      expect(correo.valid).toBeFalsy();
    });*/

  it('Verificamos funcionamiento cancelar()', () => {
    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate').and.callThrough();
    // Llamamos nuestra funcion
    component.cancelar();
    // Verificamos que fuera llamado Router navigate
    expect(spyRouter).toHaveBeenCalled();
  });


  it('Verificamos la funcion salir()', () => {
    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate').and.callThrough();
    // Espiamos nuestro servicio
    const spyService = spyOn(servicio, 'serviceSignOff').and.callThrough();
    // Llamamos nuestra funcion salir
    component.salir();
    // Verificamos que fuera llamada nuestro servicio
    expect(spyService).toHaveBeenCalled();

    // Ahora mockeamos un dataResponse del servicio
    // Evaluamos el primer caso donde codigoRespuesta = Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    const mockDataResponse = {
      codigoRespuesta : Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    };

    // Fakeamos un retorno de nuestro servicio
    const spyServiceFake = spyOn(servicio, 'serviceSignOff').and.callFake(() => {
      return of (mockDataResponse);
    });
    // Llamamos nuestra funcion salir
    component.salir();
    // Verificamos llame nuestro servicio
    expect(spyServiceFake).toHaveBeenCalled();
    // Verificamos que fuera llamado Router navigate
    expect(spyRouter).toHaveBeenCalled();

    // Ahora probamos el caso contrario donde no nos responda Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    mockDataResponse.codigoRespuesta = 'Diferente del Anterior';
    // Llamamos nuestra funcion salir
    component.salir();
    // Verificamos llame nuestro servicio
    expect(spyServiceFake).toHaveBeenCalled();
    // Verificamos que fuera llamado Router navigate
    expect(spyRouter).toHaveBeenCalled();

    // Ahora verificamos en caso nos retorne un error nuestro servicio
    const spyServiceError = spyOn(servicio, 'serviceSignOff').and.returnValue(throwError('Esto un error SignOff'));
    // Llamamos nuestra funcion salir
    component.salir();
    // Verificamos llame nuestro servicio Error
    expect(spyServiceError).toHaveBeenCalled();
    // Verificamos que fuera llamado Router navigate
    expect(spyRouter).toHaveBeenCalled();
  });


  it('Verificamos la funcion validarRespuesta', () => {
    // Asigamos un valor que nos devuelve TRUE
    let mockRespuesta = '99999';

    // Verificamos que sea true la respuesta
    expect( component.validarRespuesta(mockRespuesta)).toBeTruthy();
    // Ahora validamos de manera analoga con el resto de valores
    mockRespuesta = '3001';
    // Verificamos que sea true la respuesta
    expect( component.validarRespuesta(mockRespuesta)).toBeTruthy();
    mockRespuesta = '9999';
    // Verificamos que sea true la respuesta
    expect( component.validarRespuesta(mockRespuesta)).toBeTruthy();

    // Ahora verificamos cuando nos retorna FALSE cuando no es ninguno de los anteriores
    mockRespuesta = 'Ninguno de los Anteiores';
    expect( component.validarRespuesta(mockRespuesta)).toBeFalsy();
  });

  it('Verificamos funcion onChangeVacioNombre()', () => {
    // Seteamos a nuestro campo nombre de nuestro formGroup crearUserForm
    component.crearUserForm.controls.nombre.setValue('         ');
    // Ahora llamamos la funcion
    component.onChangeVacioNombre();
    // Ahoraverificamos que debe retornar vacio
    expect(component.crearUserForm.getRawValue().nombre).toEqual('');

  });

  it('Verificamos funcion onChangeVacioMaterno()', () => {
    // Seteamos a nuestro campo nombre de nuestro formGroup crearUserForm
    component.crearUserForm.controls.apMaterno.setValue('         ');
    // Ahora llamamos la funcion
    component.onChangeVacioMaterno();
    // Ahoraverificamos que debe retornar vacio
    expect(component.crearUserForm.getRawValue().apMaterno).toEqual('');

  });

  it('Verificamos funcion onChangeVacioPaterno()', () => {
    // Seteamos a nuestro campo nombre de nuestro formGroup crearUserForm
    component.crearUserForm.controls.apPaterno.setValue('         ');
    // Ahora llamamos la funcion
    component.onChangeVacioPaterno();
    // Ahoraverificamos que debe retornar vacio
    expect(component.crearUserForm.getRawValue().apPaterno).toEqual('');

  });

  it('Verificamos funcion setDataRequestCrearUsuario()', () => {
    component.crearUserForm.controls.nombre.setValue('Test ');
    // Le pasamos una fecha a nuestra funcion la cual debe transformarla con un datePipe a un formato ddMMyyyy
    component.setDataRequestCrearUsuario('01/01/2000');
    expect(component.crearUserForm.getRawValue().fecNac).toEqual('01012000');
  });

  it('Verificamos funcion crearUsuario(values)', () => {
    // Espiamos nuestra funcion setDataRequestCrearUsuario del componente
    const spysetDataRequestCrearUsuario = spyOn(component, 'setDataRequestCrearUsuario').and.callThrough();
    // Espiamos nuestro servicio serviceCrearUsuario
    const spyServiceCrearUsuario = spyOn(servicio, 'serviceCrearUsuario').and.callThrough();
    // Mockeamos values
    const mockValues = {
      usuario: 'TestingUnitTest',
      apPaterno: 'TestingUnit',
      nombre: 'Testing',
      apMaterno: 'Unit',
      fecNac: '01032003',
      numDoc: '55555555',
      tipoDoc: 'DNI',
      correo: 'jburgos@novatronic.com',
      telef: '111111111',
      rol: 'sixbioweb_ver'
     };
     // Asigamos los valores a nuestro formGroup
     component.crearUserForm.patchValue(mockValues);
     // Llamamos nuestra funcion crearUsuario(values)
     component.crearUsuario(mockValues);
     // Verificamos que llame a nuestra funcion setDataRequestCrearUsuario
     expect(spysetDataRequestCrearUsuario).toHaveBeenCalled();
     // Verificamos que llame nuestro servicio serviceCrearUsuario
     expect(spysetDataRequestCrearUsuario).toHaveBeenCalled();

     // Ahora fakeamos un dataResponse de nuestro servicio serviceCrearUsuario con callFake
     // Verificaremos la primera condicion con codigoRespuesta = COD_OPERACION_SATISFACTORIA_SCA
     const mockDataResponse = {
       codigoRespuesta : Constant.COD_OPERACION_SATISFACTORIA_SCA
     };
     const spyServiceFake = spyOn(servicio, 'serviceCrearUsuario').and.callFake(() => {
       return of (mockDataResponse);
     });
     // Espiamos nuestra funcion limpiar();
     const spyLimpiar = spyOn(component, 'limpiar').and.callThrough();
     // Fakeamos la llamada del Toast , caso contrario nos dara un error por la libreria Translate
     const spyToastFake = spyOn(component.toast, 'addToast').and.callFake(() => {'Nada'; });
     // Llamamos nuestra funcion crearUsuario(values)
     component.crearUsuario(mockValues);
     // Verificamos que llame nuestro servicio Fake
     expect(spyServiceFake).toHaveBeenCalled();
     // Verificamos que llame nuestra funcion limpiar
     expect(spyLimpiar).toHaveBeenCalled();
     // Verificamos que llame nuestro Toast
     expect(spyToastFake).toHaveBeenCalled();

     // Ahora verificamos el caso que codigoRespuesta sea Constant.COD_ERROR_SESION
     mockDataResponse.codigoRespuesta = Constant.COD_ERROR_SESION;
     // Asigamos los valores a nuestro formGroup
     component.crearUserForm.patchValue(mockValues);
     // Espiamos nuestra funcion salir()
     const spySalir = spyOn(component, 'salir').and.callThrough();
     // Llamamos nuestra funcion crearUsuario(values)
     component.crearUsuario(mockValues);
     // Verificamos que llame nuestra funcion salir()
     expect(spySalir).toHaveBeenCalled();

    // Ahora verificamos el caso que codigoRespuesta cumpla con la funcion validarRespuesta()
     mockDataResponse.codigoRespuesta = '99999';
     // Asigamos los valores a nuestro formGroup
     component.crearUserForm.patchValue(mockValues);
     // Espiamos nuestra funcion validarRespuesta()
     const spyValidarRespuesta = spyOn(component, 'validarRespuesta').and.callThrough();
     // Llamamos nuestra funcion crearUsuario(values)
     component.crearUsuario(mockValues);
     // Verificamos que llame nuestro Toast
     expect(spyToastFake).toHaveBeenCalled();
     // Validamos que llame nuestro espia spyValidarRespuesta de la funcion validarRespuesta();
     expect(spyValidarRespuesta).toHaveBeenCalled();
     // Verificamos que los valores seteados a nuestro formControl coincidan
     expect(component.crearUserForm.getRawValue().fecNac).toEqual(mockValues.fecNac);
     expect(component.crearUserForm.getRawValue().rol).toEqual(mockValues.rol);


     // Ahora verificamos el caso donde no nos devuelva un mensaje mapeado anteriormente
     mockDataResponse.codigoRespuesta = 'Ninguno de los Anteriores';
     // Asigamos los valores a nuestro formGroup
     component.crearUserForm.patchValue(mockValues);
     // Llamamos nuestra funcion crearUsuario(values)
     component.crearUsuario(mockValues);
     // Verificamos que llame nuestro Toast
     expect(spyToastFake).toHaveBeenCalled();
     // Verificamos que llame nuestra funcion salir()
     expect(spySalir).toHaveBeenCalled();


     // Ahora verificamos el caso que nuestro servicio nos retorne un error
     const spyServiceError = spyOn(servicio, 'serviceCrearUsuario').and.returnValue(throwError('Error serviceCrearUsuario!'));
     // Asigamos los valores a nuestro formGroup
     component.crearUserForm.patchValue(mockValues);
     // Llamamos nuestra funcion crearUsuario(values)
     component.crearUsuario(mockValues);
     // Verificamos que llame nuestro Toast
     expect(spyToastFake).toHaveBeenCalled();
     // Verificamos que llame nuestra funcion salir()
     expect(spySalir).toHaveBeenCalled();

  });












});
