import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '', component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'gestion-usuarios' },
        // eslint-disable-next-line max-len
            { path: 'verificacionBiometrica', loadChildren: () => import('./verificacion/verificacion.module').then(m => m.VerificacionModule) },
            // eslint-disable-next-line max-len
            { path: 'gestion-usuarios', loadChildren: () => import('./gestion-usuarios/gestion-usuarios.module').then(m => m.GestionUsuariosModule)},
            // eslint-disable-next-line max-len
            { path: 'consultar-validaciones', loadChildren: () => import('./consultar-validaciones/consultar-validaciones.module').then(m => m.ConsultarValidacionesModule) },
        ]
    }

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
