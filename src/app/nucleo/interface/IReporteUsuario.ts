export interface IReporteUsuario {
    listaUsuarios: Object;
    tipoReporte: string;
    f_usuario: String;
    f_nombre: String;
    f_apellidos: String;
    f_estado: String;

}
