export interface IReporteValidaciones {
    listaValidaciones: Object;
    tipoReporte: string;
    f_usuario: String;
    f_cantidad: String;
    f_fDesde: String;
    f_fHasta: String;

}
