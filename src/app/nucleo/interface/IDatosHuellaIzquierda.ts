export interface IDatosHuellaIzquierda {
    huellaI?: string;
    huellaCompaCardI?: string;
    huellaWSQI?: string;
    huellaTemplateI?: string;
    nistI?: string;
    huellavivaI?: string;
    imagenNistI?: String;
    imagenhuellavivaI?: String;
}
