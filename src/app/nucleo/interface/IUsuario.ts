export interface IUsuario {
    user?: string;
    password?: string;
    newPassword?: string;
    oldPassword?: string;
    confirmedPassword?: string;
}
