import { IlistaUsuario } from './IlistaUsuario';

export interface IlistaUsuarios {
    cRespuesta: string;
    listaUser: IlistaUsuario[];
    menRespuesta: string;

}
