export interface IDatosHuellaDerecha {
    huellaD?: string;
    huellaCompaCardD?: string;
    huellaWSQD?: string;
    huellaTemplateD?: string;
    nistD?: string;
    huellavivaD?: string;
    imagenNistD?: String;
    imagenhuellavivaD?: String;
}
