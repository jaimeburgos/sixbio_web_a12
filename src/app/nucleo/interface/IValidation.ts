export interface IValidation {
    empty?: boolean;
    errorMinLength?: boolean;
    errorMaxLength?: boolean;
    numeroDocumentoPersona?: string;
    dniValidador?: string;
}
