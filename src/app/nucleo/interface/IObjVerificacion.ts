export interface IObjVerificacion {
    dniValidador?: String;
    dniConsultor?: String;
    tipoCaptura?: number;
}
