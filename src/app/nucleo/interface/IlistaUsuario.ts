export interface IlistaUsuario {
    apMaterno: string;
    apPaterno: string;
    audFecCreac: string;
    audFecModif: string;
    audUsuCreac: string;
    audUsuModif: string;
    correo: string;
    estado: string;
    fecNac: string;
    nombre: string;
    numDoc: string;
    rol: string;
    telef: string;
    tipoDoc: string;
    usuario: string;
    usuarioLogueado: any;
}
