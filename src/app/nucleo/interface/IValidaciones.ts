export interface IValidaciones {
    apellidoMaterno?: String;
      apellidoPaterno?: String;
      codigoRespuesta?: String;
      fechaCaducidad?: String;
      fechaEmision?: String;
      fechaNacimiento?: String;
      fechaValidacion?: String;
      idValidacion?: String;
      latitudLocalizacion?: String;
      longitudLocalizacion?: String;
      mensajeRespuesta?: String;
      nombres?: String;
      numeroDocumento?: String;
      sexo?: String;
      sistemaLocalizacion?: String;
      tipoDocumento?: String;
      usuario?: String;
      origenRespuesta?: String;
      codigoTransaccion?: String;
      horaProceso?: String;
      tipoVerificacion?: String;
}
