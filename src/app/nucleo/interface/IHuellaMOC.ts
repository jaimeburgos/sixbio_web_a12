export interface IHuellaMOC {
    dniValidador?: String;
    codigoHuella?: string;
    huella?: string;
    nist?: number;

}
