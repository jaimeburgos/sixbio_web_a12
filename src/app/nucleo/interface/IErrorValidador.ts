export interface IErrorValidador {
    banderaError?: boolean;
    mensajeError?: String;
    tipoInput?: String;
}
