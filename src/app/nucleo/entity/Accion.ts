
export class Accion {

    private _descripcion: string;
    private _codigo: string;
    private _grupo: string;

    get descripcion() {
        return this._descripcion;
    }

    set descripcion(value) {
        this._descripcion = value;
    }

    get codigo() {
        return this._codigo;
    }

    set codigo(value) {
        this._codigo = value;
    }

    get grupo() {
        return this._grupo;
    }

    set grupo(value) {
        this._grupo = value;
    }













}