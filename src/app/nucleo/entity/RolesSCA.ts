export class RolesSCA {

    private _rol: string;

    private _permisos: Array<string>;

    constructor(){
        this._permisos =  new Array<string>();
    }

    get rol() {
        return this._rol;
    }

    set rol(value) {
        this._rol = value;
    }


    get permisos() {
        return this._permisos;
    }

    set permisos(value) {
        this._permisos = value;
    }

}