
import { Accion } from './Accion';
import { RolesSCA } from './RolesSCA';
import { ServicioVerificacion } from './ServicioVerificacion';


export class DatosSesion {

    private _ningunaHuella: string;

    private _huellaVivaAmarillo: string;

    private _noHuellaViva: string;

    private _nistRojo: string;

    private _umbralHuellaViva: string;

    private _nistAmarillo: string;

    private _huellaVivaRojo: string;

    private _validarSixser: boolean;

    private _serviciosVerificacion: ServicioVerificacion;

    private _acciones: Array<Accion>;

    private _idSesion: string;

    private _validarHuellaViva: boolean;

    private _validarDniValidador: boolean;

    private _visualizar: string;

    private _sixbioVersion: string;

    private _usuario: string;

    private _noHuellaCapturada: string;

    private _validarIpCliente: boolean;

    private _rolesSCA: Array<RolesSCA>;

    private _codigoRespuesta: string;

    private _mensajeRespuesta: string;

    get ningunaHuella() {
        return this._ningunaHuella;
    }

    set ningunaHuella(value) {
        this._ningunaHuella = value;
    }


    get huellaVivaAmarillo() {
        return this._huellaVivaAmarillo;
    }

    set huellaVivaAmarillo(value) {
        this._huellaVivaAmarillo = value;
    }

    get noHuellaViva() {
        return this._noHuellaViva;
    }

    set noHuellaViva(value) {
        this._noHuellaViva = value;
    }

    get nistRojo() {
        return this._nistRojo;
    }

    set nistRojo(value) {
        this._nistRojo = value;
    }

    get umbralHuellaViva() {
        return this._umbralHuellaViva;
    }

    set umbralHuellaViva(value) {
        this._umbralHuellaViva = value;
    }

    get nistAmarillo() {
        return this._nistAmarillo;
    }

    set nistAmarillo(value) {
        this._nistAmarillo = value;
    }

    get huellaVivaRojo() {
        return this._huellaVivaRojo;
    }

    set huellaVivaRojo(value) {
        this._huellaVivaRojo = value;
    }

    get validarSixser() {
        return this._validarSixser;
    }

    set validarSixser(value) {
        this._validarSixser = value;
    }

    get serviciosVerificacion() {
        return this._serviciosVerificacion;
    }

    set serviciosVerificacion(value) {
        this._serviciosVerificacion = value;
    }

    get acciones() {
        return this._acciones;
    }

    set acciones(value) {
        this._acciones = value;
    }

    get idSesion() {
        return this._idSesion;
    }

    set idSesion(value) {
        this._idSesion = value;
    }

    get validarHuellaViva() {
        return this._validarHuellaViva;
    }

    set validarHuellaViva(value) {
        this._validarHuellaViva = value;
    }

    get validarDniValidador() {
        return this._validarDniValidador;
    }

    set validarDniValidador(value) {
        this._validarDniValidador = value;
    }

    get visualizar() {
        return this._visualizar;
    }

    set visualizar(value) {
        this._visualizar = value;
    }
    get sixbioVersion() {
        return this._sixbioVersion;
    }

    set sixbioVersion(value) {
        this._sixbioVersion = value;
    }
    get usuario() {
        return this._usuario;
    }

    set usuario(value) {
        this._usuario = value;
    }

    get noHuellaCapturada() {
        return this._noHuellaCapturada;
    }

    set noHuellaCapturada(value) {
        this._noHuellaCapturada = value;
    }

    get validarIpCliente() {
        return this._validarIpCliente;
    }

    set validarIpCliente(value) {
        this._validarIpCliente = value;
    }

    get rolesSCA() {
        return this._rolesSCA;
    }

    set rolesSCA(value) {
        this._rolesSCA = value;
    }

    get codigoRespuesta() {
        return this._codigoRespuesta;
    }

    set codigoRespuesta(value) {
        this._codigoRespuesta = value;
    }
    get mensajeRespuesta() {
        return this._mensajeRespuesta;
    }

    set mensajeRespuesta(value) {
        this._mensajeRespuesta = value;
    }


}
