import { TipoServicioVerificacion } from './TipoServicioVerificacion';

export class ServicioVerificacion {

    private _default: string;

    private _tiposServicioVerificacion: Array<TipoServicioVerificacion>;


    get default() {
        return this._default;
    }

    set default(value) {
        this._default = value;
    }


    get tiposServicioVerificacion() {
        return this._tiposServicioVerificacion;
    }

    set tiposServicioVerificacion(value) {
        this._tiposServicioVerificacion = value;
    } 

}