import { HttpHandler } from '@angular/common/http';
import { Constant } from '../constante/Constant';

export class Utilitario {
    constante: Constant;
    constructor() {
        this.constante = new Constant();
    }
    public getUsuario() {
    }
    public getValidador() {
    }


    public getFechaTransaccion(): string {
        const today: Date = new Date();
        const yyyy = today.getFullYear();
        const mm = today.getMonth() + 1;
        let monthStr: string = mm + '';
        if (mm < 10) {
            monthStr = '0' + mm;
        }
        const dd = today.getDate();
        let dayStr: string = dd + '';
        if (dd < 10) {
            dayStr = '0' + dd;
        }
        const fecha: string = yyyy + monthStr + dayStr;
        return fecha;
    }

    public getHoraTransaccion(): string {
        const today: Date = new Date();
        const HH = today.getHours();
        let hourStr: string = HH + '';
        if (HH < 10) {
            hourStr = '0' + HH;
        }
        const MM = today.getMinutes();
        let MinuteStr: string = MM + '';
        if (MM < 10) {
            MinuteStr = '0' + MM;
        }
        const SS = today.getSeconds();
        let secondStr: string = SS + '';
        if (SS < 10) {
            secondStr = '0' + SS;
        }
        const mmm = today.getMilliseconds();
        let miliStr: string = mmm + '';
        if (mmm < 100 && mmm >= 10) {
            miliStr = '0' + mmm;
        } else if (mmm < 10) {
            miliStr = '00' + mmm;
        }
        const horaTransaccion = hourStr + MinuteStr + secondStr + miliStr;
        return horaTransaccion;
    }


    public generarInicializarDispositivo(): string {
        const parametrosControl =
                'fechaTransaccion=' + this.getFechaTransaccion() +
                '&horaTransaccion=' + this.getHoraTransaccion() +
                '&tipoEstacion=' + this.getTipoEstacion() +
                '&sistemaOperativoEstacion=' + this.getSistemaOperativoEstacion() +
                '&tipoIdentificacionEstacion=' + this.getTipoIdentificacionEstacion() +
                '&codigoIdentificacionEstacion=' + this.getCodigoIdentificacionEstacion() +
                '&modeloEstacion=' + this.getModeloEstacion() +
                '&numeroTransaccion=' + this.getNumeroTransaccion() +
                '&modeloDispositivo=' + this.getModeloDispositivo();
        return parametrosControl;
    }
    public generarIniciarCaptura(): string {
        const parametrosControl =
            'fechaTransaccion=' +
            this.getFechaTransaccion() +
            '&horaTransaccion=' +
            this.getHoraTransaccion() +
            '&tipoEstacion=' +
            this.getTipoEstacion() +
            '&sistemaOperativoEstacion=' +
            this.getSistemaOperativoEstacion() +
            '&tipoIdentificacionEstacion=' +
            this.getTipoIdentificacionEstacion() +
            '&codigoIdentificacionEstacion=' +
            this.getCodigoIdentificacionEstacion() +
            '&modeloEstacion=' +
            this.getModeloEstacion() +
            '&numeroTransaccion=' +
            this.getNumeroTransaccion() +
            '&tipoDispositivo=' +
            this.getTipoDispositivo() +
            '&tipoIdentificacionDispositivo=' +
            this.getTipoIdentificacionDispositivo() +
            '&codigoIdentificacionDispositivo=' +
            this.getCodigoIdentificacionDispositivo() +
            '&marcaDispositivo=' +
            this.getMarcaDispositivo() +
            '&timeout=' +
            Constant.tiempoEspera;
        return parametrosControl;
    }

    public generarLiberarDispositivo() {
        const parametrosControl =
                'fechaTransaccion=' + this.getFechaTransaccion() +
                '&horaTransaccion=' + this.getHoraTransaccion() +
                '&tipoEstacion=' + this.getTipoEstacion() +
                '&sistemaOperativoEstacion=' + this.getSistemaOperativoEstacion() +
                '&tipoIdentificacionEstacion=' + this.getTipoIdentificacionEstacion() +
                '&codigoIdentificacionEstacion=' + this.getCodigoIdentificacionEstacion() +
                '&modeloEstacion=' + this.getModeloEstacion() +
                '&numeroTransaccion=' + this.getNumeroTransaccion();
        return parametrosControl;
    }

    public executeServiceVerificacionMOC() {

    }
    public getTipoEstacion() {
        return '02';
    }
    public getSistemaOperativoEstacion() {
        return '00';
    }
    public getTipoIdentificacionEstacion() {
        return '02';
    }
    public getCodigoIdentificacionEstacion() {
        return '5454584544548448';
    }
    public getModeloEstacion() {
        return 'Intel i7';
    }
    public getNumeroTransaccion() {
        return '23536578134326';
    }
    public getModeloDispositivo() {
        return 'FG_ZF1';
    }

    public getTipoIdentificacionDispositivo() {
        return '02';
    }
    public getCodigoIdentificacionDispositivo() {
        return '10029811';
    }
    public getMarcaDispositivo() {
        return 'DERMALOG';
    }
    public getTipoDispositivo() {
        return '01';
    }
    public getTipoDocumentoPersona() {
        return '01';
    }
    public generarJsonVerificacionMOC(dni, codigoHuella, huella, nist) {
        const parametros =
                'fechaTransaccion=' + this.getFechaTransaccion() +
                '&horaTransaccion=' + this.getHoraTransaccion() +
                '&tipoEstacion=' + this.getTipoEstacion() +
                '&sistemaOperativoEstacion=' + this.getSistemaOperativoEstacion() +
                '&tipoIdentificacionEstacion=' + this.getTipoIdentificacionEstacion() +
                '&codigoIdentificacionEstacion=' + this.getCodigoIdentificacionEstacion() +
                '&modeloEstacion=' + this.getModeloEstacion() +
                '&numeroTransaccion=' + this.getNumeroTransaccion() +
                '&tipoDocumentoPersona=' + this.getTipoDocumentoPersona() +
                '&numeroDocumentoPersona=' + dni +
                '&codigoHuellaTemplateCompaCard=' + codigoHuella +
                '&huellaTemplateCompaCard=' + encodeURIComponent(huella) +
                '&calidadNISTMatchOnCard=' + nist;
        return parametros;

    }


    // eslint-disable-next-line max-len
    public generarJsonVerificacionBiometricaRequest(varCodigoTransaccion, varDocumentoPersona , varTipoCaptura , varHuellaDerechaImagen, varHuellaDerechaTemplate, varCalidadHuellaDerecha, varPorcentajeHuellaVivaDerecha, varHuellaIzquierdaImagen, varHuellaIzquierdaTemplate, varCalidadHuellaIzquierda, varPorcentajeHuellaVivaIzquierda) {


        const objResponse = {
            'numeroDocumentoPersona': varDocumentoPersona,
            'codigoTransaccion': varCodigoTransaccion,
            'huellasBiometricas': []
        };

        const huellaDerecha = {
            'calidadImagen': varCalidadHuellaDerecha,
            'formatoHuella': this.constante.HUELLA.DERECHA.FORMATO,
            'huellaImagen': varHuellaDerechaImagen,
            'huellaTemplate': varHuellaDerechaTemplate,
            'idHuellas': this.constante.HUELLA.DERECHA.CODIGO,
            'porcentajeHuellaViva' : varPorcentajeHuellaVivaDerecha
        };

        const huellaIzquierda = {
            'calidadImagen': varCalidadHuellaIzquierda,
            'formatoHuella': this.constante.HUELLA.IZQUIERDA.FORMATO,
            'huellaImagen': varHuellaIzquierdaImagen,
            'huellaTemplate': varHuellaIzquierdaTemplate,
            'idHuellas': this.constante.HUELLA.IZQUIERDA.CODIGO,
            'porcentajeHuellaViva' : varPorcentajeHuellaVivaIzquierda
        };
        switch (varTipoCaptura) {
            case 0:
                objResponse.huellasBiometricas.push(huellaDerecha);
                break;
            case 2:
                objResponse.huellasBiometricas.push(huellaDerecha);
            case 1:
                objResponse.huellasBiometricas.push(huellaIzquierda);
                break;
        }

        return objResponse;
    }
    encodeMensaje(mensajeString) {
        let mensajeOut = mensajeString;
        mensajeOut = mensajeOut.replace(new RegExp('Ã¡', 'g'), 'á');
        mensajeOut = mensajeOut.replace(new RegExp('Ã©', 'g'), 'é');
        mensajeOut = mensajeOut.replace(new RegExp('Ã³', 'g'), 'ó');
        mensajeOut = mensajeOut.replace(new RegExp('Ãº', 'g'), 'ú');
        mensajeOut = mensajeOut.replace(new RegExp('Ã', 'g'), 'í');
        return mensajeOut;
    }
}
