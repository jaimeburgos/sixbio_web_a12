import { stringify } from 'querystring';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

export class Util {

    constructor(

        public modalService?: NgbModal
    ) { }
    showDefaultModalComponent(theComponent: any, headerText: any, bodyText: any) {

        let closeResult: string;
        const modalRef = this.modalService.open(theComponent);
        modalRef.componentInstance.message = bodyText;
        modalRef.componentInstance.title = headerText;
        /*  modalRef.result.then((result) => {
             closeResult = `${result}`;
         }, (reason) => {
             closeResult = `${this.getDismissReason(reason)}`;
         });
         return closeResult; */
        return modalRef;
    }


    public getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {

            return 'by clicking on a backdrop';
        } else {

            return `with: ${reason}`;
        }
    }


    public delay(ms: number) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
}
