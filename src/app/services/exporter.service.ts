import { Injectable } from '@angular/core';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { Title } from '@angular/platform-browser';

const EXCEL_TYPE =
    'application/vnd.openxmlfornats-officedocument.spreadsheetml.sheet; charset=UTF-8';
const EXEL_EXT = '.xlsx';
@Injectable()
export class ExporterService {

    workbook: XLSX.WorkBook;

    constructor() { }

    exportAExcel(json: any[], excelFileName: String): void {
        const worksheet: XLSX.Sheet = XLSX.utils.json_to_sheet(json);
        this.workbook = {
            Sheets: {
                'data': worksheet

            },
            SheetNames: ['data'],
            Props: {Title: 'REPORTE DE USUARIOS'}
        };
        this.workbook.Props.Title = 'REPORTE DE USUARIOS';
        const excelBuffer: any = XLSX.write(this.workbook, { bookType: 'xlsx', type: 'array' });
        // Llamar al metodo: buffer y fileName
        this.saveAsExcel(excelBuffer, excelFileName);

    }
    private saveAsExcel(buffer: any, fileName: String): void {
        const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
        FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXEL_EXT);
    }
}
