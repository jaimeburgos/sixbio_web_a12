import { waitForAsync, ComponentFixture, TestBed } from '@angular/core/testing';
import { NotFoundComponent } from './not-found.component';

describe('NotFoundComponent', () => {
  let component: NotFoundComponent;
  let fixture: ComponentFixture<NotFoundComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ NotFoundComponent ],
      imports : [],
      providers: []
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  fit('Componente creado correctamente', () => {
    expect(component).toBeTruthy();
  });

  fit('Test 00 ', () => {
    const a = true;
    expect(a).toBeTruthy();
    });

  it('Test 01 ', () => {
    expect(component).toBeTruthy();
  });

});
