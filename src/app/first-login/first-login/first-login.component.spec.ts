import { async, ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule , HttpTestingController} from '@angular/common/http/testing';
import { FormBuilder, FormsModule , ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { FirstLoginComponent } from './first-login.component';
import { EnvService } from 'src/app/env.service';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { Router } from '@angular/router';
import { Observable, of, throwError } from 'rxjs';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { BsComponentModule } from 'src/app/layout/bs-component/bs-component.module';
import { NgbActiveModal, NgbDropdownModule, NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from 'src/app/layout/bs-component/components';
import { ModalModule } from 'ngx-bootstrap/modal';


// 42728713

class FakeRouter {
  navigate(params) {

  }
}

fdescribe('FirstLoginComponent', () => {
  let component: FirstLoginComponent;
  let fixture: ComponentFixture<FirstLoginComponent>;
  let servicio: ServicioService;

  let modal: ModalComponent;
  let fixtureModal: ComponentFixture<ModalComponent>;


  // Declaracion de constantes de caracteres permitidos y no permitidos para el Password
  const P_Permitidos = [
    'ññññññññññ', 'ÑÑÑÑÑÑÑ', ':::::::::', '............', ',,,,,,,,,,,', ';;;;;;;;;;;;', '--------------',
  '______________', '((((((((((', '))))))))))', '@@@@@@@@@', '/////////'
  ];

  const P_No_Permitidos = [
    'O☺☻♥♠○◘♣•', '[[[[]]]]', '{{{{{{}}}}}}}', '          ', '???????¿¿¿¿¿¿', '´´´´´´´´',
    '============', '<<<<<>>>>>', '**********', '++++++++++', '$$$$$$$$', '""""""""""""', '^^^^^^^^^^^^'
  ];

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ FirstLoginComponent ],
      imports: [
        CommonModule,
        NgbDropdownModule,
        NgbModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule, ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        BsComponentModule,
        TranslateModule.forRoot(),
        BrowserAnimationsModule,
        ModalModule.forRoot(),
      ], providers: [
        TranslateService, EnvService, ServicioService, FormBuilder ,
        {provide: Router , useClass: FakeRouter},
        NgbActiveModal,
        NgbModal,
        ReactiveFormsModule, {provide: ToastrService, useClass: ToastrService}],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FirstLoginComponent);
    fixtureModal = TestBed.createComponent(ModalComponent);
    servicio = TestBed.inject(ServicioService);
    component = fixture.componentInstance;
    modal = fixtureModal.componentInstance;
    fixture.detectChanges();
    fixtureModal.detectChanges();
    // Habilitamos en jasmine el re espiar las funciones , caso contrario tendriamos un error
    jasmine.getEnv().allowRespy(true);
  });

  it('First-login componente creado correctamente', () => {
    expect(component).toBeTruthy();
  });

    // Validar restricciones del Formulario ChangeForm
  it('OldPassword es obligatorio en ChangeForm', () => {
    // Obtenemos en una constante el ControlForm que deseamos evaluar
    const oldPassword = component.changeForm.get('old');
    // Le asignamos un valor
    oldPassword.setValue('');
    // La expectativa de la prueba de ser falso ya que no permite vacio
    expect(oldPassword.valid).toBeFalsy();
  });

  it('NewPassword es obligatorio en ChangeForm', () => {
    // Obtenemos en una constante el ControlForm que deseamos evaluar
    const newPassword = component.changeForm.get('new');
    // Le asignamos un valor
    newPassword.setValue('');
    // La expectativa de la prueba de ser falso ya que no permite vacio
    expect(newPassword.valid).toBeFalsy();
  });

  it('ConfirmPassword es obligatorio en ChangeForm', () => {
    // Obtenemos en una constante el ControlForm que deseamos evaluar
    const confirmPassword = component.changeForm.get('confirm');
    // Le asignamos un valor
    confirmPassword.setValue('');
    // La expectativa de la prueba de ser falso ya que no permite vacio
    expect(confirmPassword.valid).toBeFalsy();
  });

  it('NewPassword con caracteres permitidos y no permitidos', () => {
    const newPassword = component.changeForm.get('new');
    // Bucle para caracteres permitidos en usuario ChangeForm
      for ( let j = 0; j <= P_Permitidos.length - 1; j++) {
        newPassword.setValue(P_Permitidos[j]);
        expect(newPassword.valid).toBe(true);
        // Cuando sea verdadero newPassword.valid! no se cumple la condicion de verificacion
        if ( !newPassword.valid) {
          console.log('Fallo en el caracter ' + P_Permitidos[j] );
        }
      }

      // Bucle para caracteres no permitidos en Usuario ChangeForm
      for ( let i = 0 ; i <= P_No_Permitidos.length - 1 ; i++ ) {
        newPassword.setValue(P_No_Permitidos[i]);
        expect(newPassword.valid).toBe(false);
        // Cuando sea verdadero no se cumple la condicion de verificacion
        if (newPassword.valid) {
          console.log('Fallo en el caracter ' + P_No_Permitidos[i] );
        }
      }
  });

  it('ConfirmPassword con caracteres permitidos y no permitidos', () => {
    const confirm = component.changeForm.get('confirm');
      for ( let j = 0; j <= P_Permitidos.length - 1; j++) {
        confirm.setValue(P_Permitidos[j]);
        expect(confirm.valid).toBe(true);
        if ( !confirm.valid) {
          console.log('Fallo en el caracter ' + P_Permitidos[j] );
        }
      }
      for ( let i = 0 ; i <= P_No_Permitidos.length - 1 ; i++ ) {
        confirm.setValue(P_No_Permitidos[i]);
        expect(confirm.valid).toBe(false);
        if (confirm.valid) {
          console.log('Fallo en el caracter ' + P_No_Permitidos[i] );
        }
      }
  });

  it('OldPassword con caracteres permitidos y no permitidos', () => {
    const old = component.changeForm.get('old');
      for ( let j = 0; j <= P_Permitidos.length - 1; j++) {
        old.setValue(P_Permitidos[j]);
        expect(old.valid).toBe(true);
        if ( !old.valid) {
          console.log('Fallo en el caracter Permitido' + P_Permitidos[j] );
        }
      }
      for ( let i = 0 ; i <= P_No_Permitidos.length - 1 ; i++ ) {
        old.setValue(P_No_Permitidos[i]);
        expect(old.valid).toBe(false);
        if (old.valid) {
          console.log('Fallo en el caracter NO Permitido ' + P_No_Permitidos[i] );
        }
      }
  });

  it('NewPassword Longitud Maxima es 16 caracteres en ChangePassword', () => {
    const pass = component.changeForm.get('new');
    pass.setValue('1234567890123456');
    expect(pass.valid).toBeTruthy();
    pass.setValue('12345678901234567');
    expect(pass.valid).toBeFalsy();
  });

  it('OldPassword Longitud Maxima es 16 caracteres en ChangePassword', () => {
    const pass = component.changeForm.get('old');
    pass.setValue('1234567890123456');
    expect(pass.valid).toBeTruthy();
    pass.setValue('12345678901234567');
    expect(pass.valid).toBeFalsy();
  });


  it('ConfirmPassword Longitud Maxima es 16 caracteres en ChangePassword', () => {
    const pass = component.changeForm.get('confirm');
    pass.setValue('1234567890123456');
    expect(pass.valid).toBeTruthy();
    pass.setValue('12345678901234567');
    expect(pass.valid).toBeFalsy();
  });


  it('NewPassword Longitud Minima es 6 caracteres en ChangePassword', () => {
    const pass = component.changeForm.get('new');
    pass.setValue('12345');
    expect(pass.valid).toBeFalsy();
    pass.setValue('123456');
    expect(pass.valid).toBeTruthy();
  });

  it('OldPassword Longitud Minima es 6 caracteres en ChangePassword', () => {
    const pass = component.changeForm.get('old');
    pass.setValue('12345');
    expect(pass.valid).toBeFalsy();
    pass.setValue('123456');
    expect(pass.valid).toBeTruthy();
  });

  it('ConfirmPassword Longitud Minima es 6 caracteres en ChangePassword', () => {
    const pass = component.changeForm.get('confirm');
    pass.setValue('12345');
    expect(pass.valid).toBeFalsy();
    pass.setValue('123456');
    expect(pass.valid).toBeTruthy();
  });


  it ('Verificamos en el ngOnInit se llame al servicio "serviceConsultDataChangePassword" si es el primer logueo', () => {
    // Asignamos un valor a isFirstLogin que es leido por la funcion ngOnInit para luego recuperarlo y pasar la validacion
    localStorage.setItem('isFirstLogin', 'true');
    //   localStorage.getItem('isFirstLogin');
    // Comprobamos con el spyOn y and.callThrough() , la llamada al servicio "serviceConsultDataChangePassword"
    const espia = spyOn(servicio, 'serviceConsultDataChangePassword').and.callThrough();
    // Posteriormente llamamos la funcion del componente donde se ejecuta este servicio en este caso es el ngOnInit
    component.ngOnInit();
    // verificamos con un expect y toHaveBeenCalled si fue llamado una vez nuestro servicio en el componente
    expect(espia).toHaveBeenCalled();


    const mockDataResponse = {
      codigoRespuesta: Constant.COD_OPERACION_SATISFACTORIA_SERVICE_CONSULT_DATA_CHANGE_PASSWORD
    };

    const spy = spyOn(servicio, 'serviceConsultDataChangePassword').and.callFake( () => {
      return of (mockDataResponse);
    });

    component.ngOnInit();
    expect(spy).toHaveBeenCalled();

    const mockDataResponse2 = {
      codigoRespuesta: 'Diferente a los esperado correctamente'
    };

    const spy2 = spyOn(component, 'redirigirLogin').and.callThrough();

    const spy3 = spyOn(servicio, 'serviceConsultDataChangePassword').and.callFake( () => {
      return of (mockDataResponse2);
    });


    component.ngOnInit();
    expect(spy3).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();

  });


  it ('Verificamos en el singoff se llame al servicio "serviceSignOff"', () => {
    const vacio: Object = {  };
    // setDataSignOff nos debe devolver un arreglo vacio
    const test_setDataSignOff = component.setDataSignOff();
    expect(test_setDataSignOff).toEqual(vacio);
    // dataRequestSingOff debe ser vacio
    expect(component.dataRequestSingOff).toEqual(vacio);
    // espiamos a servicioService.serviceSignOff y lo llamamos con and.callThrough()
    const espia = spyOn(servicio, 'serviceSignOff').and.callThrough();
    // Ejecutamos el metodo que llama al servicioSignOff en el componente
    component.singoff();
    // Verificamos con el espia si se llamo al servicio
    expect(espia).toHaveBeenCalled();
  });


  it ('Verificamos en singoff el comportamiento del suscribe al servicio', () => {
    const vacio: Object = {  };
    // setDataSignOff nos debe devolver un arreglo vacio
    const test_setDataSignOff = component.setDataSignOff();
    expect(test_setDataSignOff).toEqual(vacio);
    // dataRequestSingOff debe ser vacio
    expect(component.dataRequestSingOff).toEqual(vacio);
    // espiamos a servicioService.serviceSignOff y lo llamamos con and.callThrough()
    const espia = spyOn(servicio, 'serviceSignOff').and.callThrough();
    // Ejecutamos el metodo que llama al servicioSignOff en el componente
    component.singoff();
    // Verificamos con el espia si se llamo al servicio
    expect(espia).toHaveBeenCalled();

    const mockDataResponse = {
      codigoRespuesta : Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    };
    const spy = spyOn(servicio, 'serviceSignOff').and.callFake( () => {
      return of (mockDataResponse);
     });
    const spy2 = spyOn(component, 'redirigirLogin').and.callThrough();
    component.singoff();
    expect(spy).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();

    // Caso Else
    mockDataResponse.codigoRespuesta = 'Else';
    component.singoff();
    expect(spy).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();

    // Caso de Error
    const testError = 'Error Test';
    // Forzamos el error en el servicio que llamamos en la funcion singoff
    spyOn(servicio, 'serviceSignOff').and.returnValue(throwError(testError));

    component.singoff();
    // expect(error).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();

  });







  it('Verificamos el funcionamiento esperado de la funcion salir()', () => {
    const vacio: Object = {  };
    // Asignamos valor al localStorage changePasswordPorCaducar para probar condicion de la funcion salir()
    localStorage.setItem('changePasswordPorCaducar', 'true');
    // verificamos que sea true
    expect(localStorage.getItem('changePasswordPorCaducar')).toBeTruthy();
    // Espiamos la funcion singoff del componente para verificar que sea llamado por salir()
    const spy1 = spyOn(component, 'singoff').and.callThrough();
    // Llamamos la funcion salir()
    component.salir();
    expect(spy1).toHaveBeenCalled();
     // Si se cumple la segunda condicion nos debe redireccionar a /login
     localStorage.setItem('isFirstLogin', 'true');
     localStorage.setItem('changePassword', 'false');
     // injectamos nuestro FakeRouter
     const router = TestBed.inject(Router);
     // Espiamos que llame a navigate router
     const spy2 = spyOn(router, 'navigate');
     component.salir();
     // verificamos que efectivamente sea llamando
     expect(spy2).toHaveBeenCalled();

    // Si se cumple la tercera condicion
    localStorage.setItem('changePasswordPorCaducar', null);
    localStorage.setItem('isFirstLogin', null);
    localStorage.setItem('changePassword', 'true');

    // Espiamos que llame a navigate router
    const spy3 = spyOn(router, 'navigate');
     component.salir();
    // verificamos que efectivamente sea llamando
    expect(spy3).toHaveBeenCalled();

    // en caso que no se cumpla ninguna de las 3 condiciones
    localStorage.setItem('changePasswordPorCaducar', null);
    localStorage.setItem('changePassword', null);
    localStorage.setItem('isFirstLogin', null);
    const spy4 = spyOn(component, 'singoff').and.callThrough();
    component.salir();
    expect(spy4).toHaveBeenCalled();
  });

  it('Verificamos funcion cancel()', () => {
    // Espiamos la funcion salir
    const spy =  spyOn(component, 'salir').and.callThrough();
    // Llamos la funcion cancel
    component.cancel();
    // verificamos que la funcion salir() fuera llamada dentro de cancel()
    expect(spy).toHaveBeenCalled();
  });

  it('Verificamos funcion rediriguirLogin()', () => {
    const router = TestBed.inject(Router);
    const spy = spyOn(router, 'navigate');
    component.redirigirLogin();
    expect(spy).toHaveBeenCalled();
  });


  it('Verificamos funcion redirigirCambio()', () => {
    const router = TestBed.inject(Router);
    const spy = spyOn(router, 'navigate');
    component.redirigirCambio();
    expect(spy).toHaveBeenCalled();
  });

  it('Verificamos funcion limpiarCampos()', () => {
    component.limpiarCampos();
    expect(component.entidad.confirmedPassword).toBeNull();
    expect(component.entidad.oldPassword).toBeNull();
    expect(component.entidad.newPassword).toBeNull();
  });



  it('Verificamos funcion confirmacionContrasenia()', () => {
    const values = {
      new : 'TestPass',
      confirm : 'TestPass'
    };
    expect(component.confirmacionContrasenia(values)).toBeTruthy();
    values.new = 'DiferentPassword';
    expect(component.confirmacionContrasenia(values)).toBeFalsy();
  });


  it('Verificamos funcion validarRespuesta(c)', () => {
   let c = '1004';
   expect(component.validarRespuesta(c)).toBeTruthy();
   c = '00000';
   expect(component.validarRespuesta(c)).toBeTruthy();
   c = '00001';
   expect(component.validarRespuesta(c)).toBeTruthy();
   c = '1009';
   expect(component.validarRespuesta(c)).toBeTruthy();
   c = '1002';
   expect(component.validarRespuesta(c)).toBeTruthy();
   c = '1003';
   expect(component.validarRespuesta(c)).toBeTruthy();
   c = '1006';
   expect(component.validarRespuesta(c)).toBeTruthy();
   c = '0000';
   expect(component.validarRespuesta(c)).toBeTruthy();
   c = 'N.A.';
   expect(component.validarRespuesta(c)).toBeFalsy();
  });


  it('verificamos la funcion setDataChangePassword()', () => {
    // Recordar agregar librerias JS en assets/aes en TEST en angular.json , caso contrario marcara error de dependencia
    const oldPassword = 'TestingPassword';
    const newPassword = 'TestingPassword';
    const spy = spyOn(component, 'encrypted').and.callThrough();
    const test = component.setDataChangePassword(oldPassword, newPassword);
    expect(test.claveAntigua).toEqual(component.encrypted(oldPassword.trim()));
    expect(test.claveNueva).toEqual(component.encrypted(newPassword.trim()));
    // Verificamos que la funcion encrypted se ha lanzando cuatro veces
    expect(spy).toHaveBeenCalledTimes(4);
  });


  it('Verificamos funcion Confirm()', () => {
    component.confirm();
    expect(component.aceptar).toBeTruthy();
  });

  it('Verificamos funcion decline()', () => {
    component.decline();
    expect(component.aceptar).toBeFalsy();
  });

  it('Verificamos funcion showModal()', () => {
    // Espiamos al modal para verificar la funcion show
    const spyModal = spyOn(component.modal, 'show').and.callThrough();
    component.showModal();
    // Verificamos que fuera llamado la funcion show del modal
    expect(spyModal).toHaveBeenCalled();
    // Verificamos que aceptar sea false
    expect(component.aceptar).toBeFalsy();
  });


  it('Verificamos funcion evento()', () => {
    // seteamos true para entrar en el condicional If
    component.aceptar = true;
    // Espiamos para verificar si llama efectivamente al servicio suscrito
    const spy = spyOn(servicio, 'serviceChangePassword').and.callThrough();
    // Llamamos la funcion evento() de nuestro componenete
    component.evento();
    // Verificamos el espia si fue llamado por la funcion evento()
    expect(spy).toHaveBeenCalled();

    // Mockeamos el dataResponse de nuestro servicio serviceChangePassword
    const dataResponseMock = {
      codigoRespuesta : Constant.COD_OPERACION_SATISFACTORIA_CAMBIO_CLAVE
    };
    // Espiamos el componente y servicio que son llamados en la condicional del cambio de clave correcto
    const spySingOff = spyOn(component, 'singoff').and.callThrough();
    const spyMock =   spyOn(servicio, 'serviceChangePassword').and.callFake( () => {
      return of (dataResponseMock);
     });
    // Llamamos la funcion evento del componente
    component.aceptar = true;
    component.evento();
    // Verificamos si nuestros espias han sido llamados
    expect(spySingOff).toHaveBeenCalled();
    expect(spyMock).toHaveBeenCalled();

    // La condicion evaluada anteriormente tambien se cumple para cambio clave a demanda
    dataResponseMock.codigoRespuesta = Constant.COD_OPERACION_SATISFACTORIA_CAMBIO_CLAVE_DEMANDA;
    // Llamamos nuestra funcion evento() y verificamos que nuestro espia SingOff anteriormente se llamado
    component.aceptar = true;
    component.evento();
    expect(spySingOff).toHaveBeenCalled();

    // Verificamos el caso que la respuesta sea Constant.COD_ERROR_SESION
    dataResponseMock.codigoRespuesta = Constant.COD_ERROR_SESION;
    // Llamamos nuestra funcion evento() y verificamos que nuestro espia SingOff anteriormente se llamado
    component.aceptar = true;
    component.evento();
    expect(spySingOff).toHaveBeenCalled();

    // En caso contrario no se cumplan las condiciones anteriores mockeamos una respuesta segun validarRespuesta(c)
    dataResponseMock.codigoRespuesta = '1009';
    const spyValidarRespuesta = spyOn(component, 'validarRespuesta').and.callThrough();
    // Llamamos nuestra funcion evento() y verificamos que nuestro espia SingOff anteriormente se llamado
    component.aceptar = true;
    component.evento();
    expect(spyValidarRespuesta).toHaveBeenCalled();

    // En caso contrario no se cumpla lo anterior , mockeamos una respuesta no valida para la funcion validarRespuesta(c)
    dataResponseMock.codigoRespuesta = 'Esto es diferente';
      // Llamamos nuestra funcion evento() y verificamos que nuestro espia SingOff anteriormente se llamado
      component.aceptar = true;
      component.evento();
      expect(spySingOff).toHaveBeenCalled();


     // Ahora verificamos en caso de una excepcion de error con el servicio
     const spyError = spyOn(servicio, 'serviceChangePassword').and.returnValue(throwError('Error Test'));
     // Lllamamos a la funcion evento del componente
     component.aceptar = true;
     component.evento();
     // verificamos que entramos en el error
     expect(spyError).toHaveBeenCalled();
     // Verificamos que se llamo la funcion singoff
     expect(spySingOff).toHaveBeenCalled();

  });

  it('verificamos la funcion change(values)', () => {
    // Mockeamos los parametros de entra de la funcion change , lo cuales se reciben desde el changeForm
    const mockValues = {
      confirm : 'Identico123',
      new :  'Identico123',
      old : 'Antiguo123'
    };
    // Creamos nuestro espia para verificar la funcion del componente es llamada mas adelante
    const spyConfirmacion = spyOn(component, 'confirmacionContrasenia').and.callThrough();
    // Llamamos nuestra funcion change(values);
    component.change(mockValues);
    // Verificamos que nuestro espia fuera llamado
    expect(spyConfirmacion).toHaveBeenCalled();
    // Verificamos que la funcion confirmacionContrasenia nos retorne TRUE
    expect(component.confirmacionContrasenia(mockValues)).toBeTruthy();

    // Para verificar else necesitamos que New y Confirm sea diferentes
    mockValues.confirm = 'Diferente de  New';
    const spyConfirmacion2 = spyOn(component, 'confirmacionContrasenia').and.callThrough();

    // Llamamos nuestra funcion change(values);
    component.change(mockValues);

    // Verificamos que nuestro espia fuera llamado
    expect(spyConfirmacion2).toHaveBeenCalled();
    // Verificamos que la funcion confirmacionContrasenia nos retorne FALSE
    expect(component.confirmacionContrasenia(mockValues)).toBeFalsy();

  });







/* BLOQUEADO CHANGE(VALUES) , ModalComponent no se puede asignar valor que

  it('verificamos la funcion change(values)', () => {
    // Mockeamos los parametros de entra de la funcion change , lo cuales se reciben desde el changeForm
    const mockValues = {
      confirm : 'Identico123',
      new :  'Identico123',
      old : 'Antiguo123'
    };
    // Creamos un espia para verificar mas adelante si la funcion es llamada
    const spyConfirmacionContrasenia = spyOn(component, 'confirmacionContrasenia').and.callThrough();
    // Espiamos la funcion singoff para verificar que sea llamada mas adelante
    const sypSingOff = spyOn(component, 'singoff').and.callThrough();

    const mockDataResponse = {
      codigoRespuesta : Constant.COD_OPERACION_SATISFACTORIA_CAMBIO_CLAVE,
    };

    spyOn(servicio, 'serviceChangePassword').and.returnValue(mockDataResponse);



    component.util.showDefaultModalComponent(ModalComponent, '', 'Ud. esta seguro de realizar Cambio clave').result.then(
      (closeResult) => closeResult = 'Confirm');
      modal.closeResult = 'Confirm';

    component.change(mockValues);


    expect(spyConfirmacionContrasenia).toHaveBeenCalled();
    expect(sypSingOff).toHaveBeenCalled();
  });

*/





});
