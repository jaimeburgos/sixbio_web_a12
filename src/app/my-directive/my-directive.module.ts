import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ButtonDirective } from './directive/button/button.directive';
import { InputDirective } from './directive/input/input.directive';

@NgModule({
  declarations: [
    ButtonDirective,
    InputDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ButtonDirective,
    InputDirective
  ]
})
export class MyDirectiveModule { }
