import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appButton]'
})
export class ButtonDirective {

  constructor(private btn: ElementRef) { }

  @HostListener('click')
  onClick() {
    this.disableButton();
  }

  disableButton() {
    this.btn.nativeElement.disabled = true;
  }

  enableButton() {
    this.btn.nativeElement.disabled = false;
  }

}
