import { Directive, ElementRef, Renderer2, OnInit } from '@angular/core';

@Directive({
  selector: '[appInput]'
})
export class InputDirective implements OnInit {

  constructor(
    private inp: ElementRef,
    private renderer: Renderer2) { }

  ngOnInit() {
    console.log("init appInput");
  }

  ocultarInput() {
    console.log("ocultarInput");
    this.renderer.setStyle(this.inp.nativeElement, "display", "none");
  }


  mostrarInput() {

  }

}
