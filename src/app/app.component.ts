import { Component, OnInit } from '@angular/core';
import { ServicioService } from './servicio/servicio.service';
import { Router } from '@angular/router';
import { IUsuario } from './nucleo/interface/IUsuario';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    data: Object = {};
    constructor(private servicioService: ServicioService, private router: Router) {

    }
    ngOnInit() {
        window.addEventListener('beforeunload', () => {
            this.servicioService.serviceEndCapturaDactilar(this.data).subscribe((dataR: any) => {});
            if (performance.navigation.type !== performance.navigation.TYPE_RELOAD) {
                this.servicioService.serviceSignOff(this.data).subscribe((dataResponse: any) => {});
                localStorage.clear();
            }
         }, false);
    }


}
