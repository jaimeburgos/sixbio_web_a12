import { Injectable } from '@angular/core';
import { ServicioApiService } from './servicio-api.service';
import { VerificacionComponent } from '../layout/verificacion/verificacion.component';
import { IDataValidacion } from '../nucleo/interface/IDataValidacion';


@Injectable({
  providedIn: 'root'
})

export class ServicioService {
  verificacionComponent: VerificacionComponent;
  constructor(private servicioApiService: ServicioApiService) {
  }

// Grupo de Servicios

  serviceSignOn(data: Object) {
    return this.servicioApiService.serviceSignOn(data);
  }

  serviceConsultDataChangePassword(data: Object) {
    return this.servicioApiService.serviceConsultDataChangePassword(data);
  }

// Servicios Agente Biometrico
  serviceInicializarCaptura(data: Object) {
    return this.servicioApiService.serviceIniciarCaptura(data);
  }
  serviceCapturaDerecha(data: Object) {
    return this.servicioApiService.serviceCapturaHuellaDerecha(data);
  }
  serviceCapturaIzquierda(data: Object) {
    return this.servicioApiService.serviceCapturaHuellaIzquierda(data);
  }
  serviceCapturaAmbasHuellas(data: Object) {
    return this.servicioApiService.serviceCapturaAmbasHuellas(data);
  }
  serviceInicializarVerificacionMOC(data: string) {
    return this.servicioApiService.serviceIniciarVerificacionMOC(data);
  }
  serviceEndCapturaDactilar(data: Object) {
    return this.servicioApiService.serviceEndCapturaDactilar(data);
  }

// Servicios SIX/BIO

  serviceSignOff(data: Object) {
    return this.servicioApiService.serviceSignOff(data);
  }

  serviceVerificacionBiometrica(data: Object) {
    return this.servicioApiService.serviceVerificacionBiometrica(data);
  }

  serviceConsultaMejorHuella(data: Object) {
    return this.servicioApiService.serviceConsultaMejorHuella(data);
  }

  serviceVerificacioMOC(data: string) {
    return this.servicioApiService.serviceIniciarVerificacionMOC(data);
  }

  serviceConsultarValidaciones(data: IDataValidacion) {
    return this.servicioApiService.serviceConsultarValidaciones(data);
  }

// Servicios SIX/SER

  serviceObtenerFoto(data: Object) {
    return this.servicioApiService.serviceObtenerFoto(data);
  }

  serviceObtenerConsolidado(data: Object) {
    return this.servicioApiService.serviceObtenerConsolidado(data);
  }

  serviceObtenerDatos(data: Object) {
    return this.servicioApiService.serviceObtenerDatos(data);
  }

// Servicios SCA

  serviceChangePassword(data: Object) {
    return this.servicioApiService.serviceChangePassword(data);
  }

  serviceCrearUsuario(data: Object) {
    return this.servicioApiService.serviceCrearUsuario(data);
  }

  serviceEliminarUsuario(data: Object) {
    return this.servicioApiService.serviceEliminarUsuario(data);
  }

  serviceEditarUsuario(data: Object) {
    return this.servicioApiService.serviceEditarUsuario(data);
  }

  serviceResetearUsuario(data: Object) {
    return this.servicioApiService.serviceResetearUsuario(data);
  }

  serviceListarUsuario(data: Object) {
    return this.servicioApiService.serviceListarUsuario(data);
  }

  serviceListarRolesAgrupador(data: Object) {
    return this.servicioApiService.serviceListarRolesAgrupador(data);
  }

  serviceGenerarReporte(data: Object) {
    return this.servicioApiService.serviceGenerarReporte(data);
  }

  serviceGenerarReporteValidaciones(data: Object) {
    return this.servicioApiService.serviceGenerarReporteValidaciones(data);
  }
}
