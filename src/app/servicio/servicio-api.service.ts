import { Injectable } from '@angular/core';
import { HttpClient, HttpClientJsonpModule } from '@angular/common/http';
import { Utilitario } from '../nucleo/util/Utilitario';
import { Constant } from '../nucleo/constante/Constant';
import { Observable } from 'rxjs';
import { VerificarmocComponent } from '../layout/verificacion/components/verificarmoc/verificarmoc.component';
import { IHuellaMOC } from '../nucleo/interface/IHuellaMOC';
import { EnvService } from '../env.service';
import { IDataValidacion } from '../nucleo/interface/IDataValidacion';
@Injectable({
  providedIn: 'root'
})
export class ServicioApiService {
  constante: Constant;
  options: any;
  utilitario: Utilitario;
/*   verificarmocComponent: VerificarmocComponent; */
  constructor(private httpClient: HttpClient, private env: EnvService) {
    this.utilitario = new Utilitario();
    this.constante = new Constant();
/*     this.verificarmocComponent = new VerificarmocComponent(); */
   }
  // Grupo de Servicios

 /*  serviceSignOn(data: Object) {
    return this.httpClient.post(`http://${this.env.servicio}:${this.env.puerto}/${this.env.app}/service/signon`, data);
  }

  serviceConsultDataChangePassword(data: Object) {
    return this.httpClient.post(`http://${this.env.servicio}:${this.env.puerto}/${this.env.app}/service/consultardatoscambioclave`, data);
  }

  // Servicios SIX/SER

  serviceObtenerFoto(data: Object) {
    return this.httpClient.post(`http://${this.env.servicio}:${this.env.puerto}/${this.env.app}/sixser/obtenerfoto`, data);
  }

  serviceObtenerConsolidado(data: Object) {
      return this.httpClient.post(`http://${this.env.servicio}:${this.env.puerto}/${this.env.app}/sixser/obtenerconsolidado`, data);
  }

  serviceObtenerDatos(data: Object) {
      return this.httpClient.post(`http://${this.env.servicio}:${this.env.puerto}/${this.env.app}/sixser/obtenerdatos`, data);
  }

  // Servicios SIX/BIO

  serviceSignOff(data: Object) {
    return this.httpClient.post(`http://${this.env.servicio}:${this.env.puerto}/${this.env.app}/sixbio/signoff`, data);
  }
  serviceVerificacionBiometrica(data: Object) {
      return this.httpClient.post(`http://${this.env.servicio}:${this.env.puerto}/${this.env.app}/sixbio/verificacionbiometrica`, data);
  }

  serviceConsultaMejorHuella(data: Object) {
      return this.httpClient.post(`http://${this.env.servicio}:${this.env.puerto}/${this.env.app}/sixbio/consultamejorhuella`, data);
  }
  serviceConsultarValidaciones(data: IDataValidacion) {
    return this.httpClient.post(`http://${this.env.servicio}:${this.env.puerto}/${this.env.app}/sixbio/consultavalidaciones`, data);
  }

 // Servicio INICIAR CAPTURA
 serviceIniciarCaptura(url, callback = 'callback') {
  // tslint:disable-next-line:max-line-length
  return this.httpClient.jsonp(this.constante.CAPTURA_DACTILAR.INICIALIZAR_DISPOSITIVO.URL_SERVICES + this.utilitario.generarInicializarDispositivo(), callback);
}

serviceCapturaHuellaDerecha(url, callback = 'callback') {
  // tslint:disable-next-line:max-line-length
  return this.httpClient.jsonp(this.constante.CAPTURA_DACTILAR.INICIAR_CAPTURA.URL_SERVICES + this.utilitario.generarIniciarCaptura(), callback);
}
serviceCapturaHuellaIzquierda(url, callback = 'callback') {
  // tslint:disable-next-line:max-line-length
  return this.httpClient.jsonp(this.constante.CAPTURA_DACTILAR.INICIAR_CAPTURA.URL_SERVICES + this.utilitario.generarIniciarCaptura(), callback);
}
serviceCapturaAmbasHuellas(url, callback = 'callback') {
  // tslint:disable-next-line:max-line-length
  return this.httpClient.jsonp(this.constante.CAPTURA_DACTILAR.INICIAR_CAPTURA.URL_SERVICES + this.utilitario.generarIniciarCaptura(), callback);
}
serviceIniciarVerificacionMOC(data: string) {
  // tslint:disable-next-line:max-line-length
  return this.httpClient.jsonp(this.constante.CAPTURA_DACTILAR.VERIFICACION_MOC.URL_SERVICES + data, 'callback');
}
serviceVerificacionMOC(datosMOC: IHuellaMOC) {
  // tslint:disable-next-line:max-line-length
  return this.httpClient.jsonp(this.constante.CAPTURA_DACTILAR.VERIFICACION_MOC.URL_SERVICES + this.utilitario.generarJsonVerificacionMOC(datosMOC.dniValidador, datosMOC.codigoHuella, datosMOC.huella, datosMOC.nist), 'callback');
}
serviceEndCapturaDactilar(url, callback = 'callback') {
  // tslint:disable-next-line:max-line-length
  return this.httpClient.jsonp(this.constante.CAPTURA_DACTILAR.LIBERAR_DISPOSITIVO.URL_SERVICES + this.utilitario.generarLiberarDispositivo(), callback);
}

  // Servicios SCA

  serviceChangePassword(data: Object) {
    return this.httpClient.post(`http://${this.env.servicio}:${this.env.puerto}/${this.env.app}/sixsca/cambiarclave`, data);
  }

  serviceCrearUsuario(data: Object) {
    return this.httpClient.post(`http://${this.env.servicio}:${this.env.puerto}/${this.env.app}/sixsca/crearusuario`, data);
  }

  serviceEliminarUsuario(data: Object) {
    return this.httpClient.post(`http://${this.env.servicio}:${this.env.puerto}/${this.env.app}/sixsca/eliminarusuario`, data);
  }

  serviceEditarUsuario(data: Object) {
    return this.httpClient.post(`http://${this.env.servicio}:${this.env.puerto}/${this.env.app}/sixsca/modificarusuario`, data);
  }

  serviceResetearUsuario(data: Object) {
    return this.httpClient.post(`http://${this.env.servicio}:${this.env.puerto}/${this.env.app}/sixsca/resetearusuario`, data);
  }

  serviceListarUsuario(data: Object) {
    return this.httpClient.post(`http://${this.env.servicio}:${this.env.puerto}/${this.env.app}/sixsca/listarusuarios`, data);
  }

  serviceListarRolesAgrupador(data: Object) {
    return this.httpClient.post(`http://${this.env.servicio}:${this.env.puerto}/${this.env.app}/sixsca/listarrolesagrupador`, data);
  }

  serviceGenerarReporte(data: Object) {
    this.options = {
        observe: 'body',
        responseType: 'blob' as 'json'
      };
    // tslint:disable-next-line:max-line-length
    return this.httpClient.post(`http://${this.env.servicio}:${this.env.puerto}/${this.env.app}/service/reporteusuarios`, data, this.options);
  }

  serviceGenerarReporteValidaciones(data: Object) {
    this.options = {
        observe: 'body',
        responseType: 'blob' as 'json'
      };
    // tslint:disable-next-line:max-line-length
    return this.httpClient.post(`http://${this.env.servicio}:${this.env.puerto}/${this.env.app}/service/reportevalidaciones`, data, this.options);
  }
 */
// =========================
//          PRUEBA
// =========================

serviceSignOn(data: Object) {
    return this.httpClient.post('/SIXBIO-webcore-Nova/service/signon', data);
  }

  serviceConsultDataChangePassword(data: Object) {
    return this.httpClient.post('/SIXBIO-webcore-Nova/service/consultardatoscambioclave', data);
  }

  // Servicios SIX/SER

  serviceObtenerFoto(data: Object) {
    return this.httpClient.post('/SIXBIO-webcoresixser/obtenerfoto', data);
  }

  serviceObtenerConsolidado(data: Object) {
      return this.httpClient.post('/SIXBIO-webcore-Nova/sixser/obtenerconsolidado', data);
  }

  serviceObtenerDatos(data: Object) {
      return this.httpClient.post('/SIXBIO-webcore-Nova/sixser/obtenerdatos', data);
  }

  // Servicios SIX/BIO

  serviceSignOff(data: Object) {
    return this.httpClient.post('/SIXBIO-webcore-Nova/sixbio/signoff', data);
  }
  serviceVerificacionBiometrica(data: Object) {
      return this.httpClient.post('/SIXBIO-webcore-Nova/sixbio/verificacionbiometrica', data);
  }

  serviceConsultaMejorHuella(data: Object) {
      return this.httpClient.post('/SIXBIO-webcore-Nova/sixbio/consultamejorhuella', data);
  }
  serviceConsultarValidaciones(data: IDataValidacion) {
    return this.httpClient.post('/SIXBIO-webcore-Nova/sixbio/consultavalidaciones', data);
  }

 // Servicio INICIAR CAPTURA
 serviceIniciarCaptura(url, callback = 'callback') {
  // tslint:disable-next-line:max-line-length
  console.log(this.constante.CAPTURA_DACTILAR.INICIALIZAR_DISPOSITIVO.URL_SERVICES + this.utilitario.generarInicializarDispositivo());
  return this.httpClient.jsonp(this.constante.CAPTURA_DACTILAR.INICIALIZAR_DISPOSITIVO.URL_SERVICES + this.utilitario.generarInicializarDispositivo(), callback);
}


serviceCapturaHuellaDerecha(url, callback = 'callback') {
  // tslint:disable-next-line:max-line-length
  return this.httpClient.jsonp(this.constante.CAPTURA_DACTILAR.INICIAR_CAPTURA.URL_SERVICES + this.utilitario.generarIniciarCaptura(), callback);
}
serviceCapturaHuellaIzquierda(url, callback = 'callback') {
  // tslint:disable-next-line:max-line-length
  return this.httpClient.jsonp(this.constante.CAPTURA_DACTILAR.INICIAR_CAPTURA.URL_SERVICES + this.utilitario.generarIniciarCaptura(), callback);
}
serviceCapturaAmbasHuellas(url, callback = 'callback') {
  // tslint:disable-next-line:max-line-length
  return this.httpClient.jsonp(this.constante.CAPTURA_DACTILAR.INICIAR_CAPTURA.URL_SERVICES + this.utilitario.generarIniciarCaptura(), callback);
}
serviceIniciarVerificacionMOC(data: string) {
  // tslint:disable-next-line:max-line-length
  return this.httpClient.jsonp(this.constante.CAPTURA_DACTILAR.VERIFICACION_MOC.URL_SERVICES + data, 'callback');
}
serviceVerificacionMOC(datosMOC: IHuellaMOC) {
  // tslint:disable-next-line:max-line-length
  return this.httpClient.jsonp(this.constante.CAPTURA_DACTILAR.VERIFICACION_MOC.URL_SERVICES + this.utilitario.generarJsonVerificacionMOC(datosMOC.dniValidador, datosMOC.codigoHuella, datosMOC.huella, datosMOC.nist), 'callback');
}
serviceEndCapturaDactilar(url, callback = 'callback') {
  // tslint:disable-next-line:max-line-length
  return this.httpClient.jsonp(this.constante.CAPTURA_DACTILAR.LIBERAR_DISPOSITIVO.URL_SERVICES + this.utilitario.generarLiberarDispositivo(), callback);
}

  // Servicios SCA

  serviceChangePassword(data: Object) {
    return this.httpClient.post('/SIXBIO-webcore-Nova/sixsca/cambiarclave', data);
  }

  serviceCrearUsuario(data: Object) {
    return this.httpClient.post('/SIXBIO-webcore-Nova/sixsca/crearusuario', data);
  }

  serviceEliminarUsuario(data: Object) {
    return this.httpClient.post('SIXBIO-webcore-Nova/sixsca/eliminarusuario', data);
  }

  serviceEditarUsuario(data: Object) {
    return this.httpClient.post('/SIXBIO-webcore-Nova/sixsca/modificarusuario', data);
  }

  serviceResetearUsuario(data: Object) {
    return this.httpClient.post('/SIXBIO-webcore-Nova/sixsca/resetearusuario', data);
  }

  serviceListarUsuario(data: Object) {
    return this.httpClient.post('/SIXBIO-webcore-Nova/sixsca/listarusuarios', data);
  }

  serviceListarRolesAgrupador(data: Object) {
    return this.httpClient.post('/SIXBIO-webcore-Nova/sixsca/listarrolesagrupador', data);
  }

  serviceGenerarReporte(data: Object) {
    this.options = {
        observe: 'body',
        responseType: 'blob' as 'json'
      };
    // tslint:disable-next-line:max-line-length
    return this.httpClient.post('/SIXBIO-webcore-Nova/service/reporteusuarios', data, this.options);
  }

  serviceGenerarReporteValidaciones(data: Object) {
    this.options = {
        observe: 'body',
        responseType: 'blob' as 'json'
      };
    // tslint:disable-next-line:max-line-length
    return this.httpClient.post('/SIXBIO-webcore-Nova/service/reportevalidaciones', data, this.options);
  }


}
