export class EnvService {

  // The values that are defined here are the default values that can
  // be overridden by env.js

  // API url
  public apiUrl = '';

  // Whether or not to enable debug mode
  public enableDebug = true;
  public servicio = '26.196.225.129';
  public puerto = '8081';
  public app = 'SIXBIO-webcore';

  constructor() {
  }

}
