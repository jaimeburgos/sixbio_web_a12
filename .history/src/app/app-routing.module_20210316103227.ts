import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FirstLoginComponent } from './first-login/first-login/first-login.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard , FirstLoginGuard } from './shared';


const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'prefix'},
    { path: 'home', loadChildren: () => import('./layout/layout.module').then(m => m.LayoutModule), canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent},
    // tslint:disable-next-line:max-line-length
    { path: 'firstLogin', component: FirstLoginComponent, canActivate: [FirstLoginGuard]},
    { path: 'error', loadChildren: () => import('./server-error/server-error.module').then(m => m.ServerErrorModule) },
    { path: 'access-denied', loadChildren: () => import('./access-denied/access-denied.module').then(m => m.AccessDeniedModule) },
    { path: 'not-found', loadChildren: () => import('./not-found/not-found.module').then(m => m.NotFoundModule) },
    { path: '**', redirectTo: 'not-found' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
