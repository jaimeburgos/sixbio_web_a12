import { ElementRef } from '@angular/core';
import {  ComponentFixture, TestBed } from '@angular/core/testing';
import { ButtonDirective } from './button.directive';

fdescribe('AccessDeniedComponent', () => {
    let component: ButtonDirective;
    let fixture: ComponentFixture<ButtonDirective>;
    let btn: ElementRef;
  beforeEach(() => {
    TestBed.configureTestingModule({
        declarations : [ButtonDirective],
        providers : [ElementRef]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonDirective);
    btn = TestBed.inject(ElementRef);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Verificamos la funcion disableButton', () => {
    // Llamamos la funcion disableButton()
    component.disableButton();
    // Verificamos el valor de btn.nativeElement.disabled
    console.log(' El valor btn ' + btn.nativeElement.disabled);
  });
});
