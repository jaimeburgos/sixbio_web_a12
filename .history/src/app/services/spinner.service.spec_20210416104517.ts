import { TestBed } from '@angular/core/testing';
import { NgxSpinnerService } from 'ngx-spinner';

import { SpinnerService } from './spinner.service';

fdescribe('SpinnerService', () => {
  let service: SpinnerService;
  let spinner: NgxSpinnerService;
  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SpinnerService);
    spinner = TestBed.inject(NgxSpinnerService);
  });

  it('SpinnerService creado correctamente', () => {
    expect(service).toBeTruthy();
  });

  it('Verificamos la funcion llamarSpinner', () => {
    // Espiamos nuestra funcion show() de spinner( NgxSpinnerService) [Libreria Externa]
    const spyShow = spyOn(spinner, 'show');
    // Llamamos nuestra funcion llamarSpinner
    service.llamarSpinner();
    // Verificamos que fuera llamada nuestra funcion show de spinner
    expect(spyShow).toHaveBeenCalled();
  });

  it('Verificamos la funcion detenerSpinner', () => {
    // Espiamos nuestra funcion hide() de spinner( NgxSpinnerService) [Libreria Externa]
    const spyHide = spyOn(spinner, 'hide');
    // Llamamos nuestra funcion llamarSpinner
    service.detenerSpinner();
    // Verificamos que fuera llamada nuestra funcion hide de spinner
    expect(spyHide).toHaveBeenCalled();
  });




});
