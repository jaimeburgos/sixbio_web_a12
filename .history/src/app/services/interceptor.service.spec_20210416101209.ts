import { TestBed } from '@angular/core/testing';

import { InterceptorService } from './interceptor.service';

fdescribe('InterceptorService', () => {
  let service: InterceptorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InterceptorService);
  });

  it('InterceptorService creado correctamente', () => {
    expect(service).toBeTruthy();
  });
});
