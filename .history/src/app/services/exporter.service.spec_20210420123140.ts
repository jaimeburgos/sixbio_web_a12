import { componentFactoryName } from '@angular/compiler';
import { TestBed } from '@angular/core/testing';
import * as FileSaver from 'file-saver';
import { ServicioApiService } from '../servicio/servicio-api.service';
import { ExporterService } from './exporter.service';

fdescribe('ExporterService', () => {

  let service: ExporterService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers : [ExporterService]
    });
    service = TestBed.inject(ExporterService);
  });

  it('ExporterService creado correctamente', () => {
    expect(service).toBeTruthy();
  });

  it('Verificamos la funcion exportAExcel()', () => {
    // Mockeamos valores de entrada para nuestra funcion
    const mockJson = ['Testing'];
    const mockExcelFileName = 'Testing_File_Name';
    // Espiamos nuestra funcion saveAsExcel , agregar <any> delante de spyOn en caso de ser funcion privada
    const spySaveAsExcel = spyOn<any>(service, 'saveAsExcel');
    // Llamamos nuestra funcion  exportAExcel
    service.exportAExcel(mockJson, mockExcelFileName);
    // Verificamos que llame a nuestro espia
    expect(spySaveAsExcel).toHaveBeenCalled();

  });

  xit('Verificamos nuestra funcion saveAsExcel()', () => {
    // Mockeamos los valores de entrada de nuestra funcion
    const mockbuffer = ['Testing'];
    const mockFileName = 'Testing Name';
    // Espiamos a FileSaver y su funcion saveAs
    const spySaveAs = spyOn(FileSaver, 'saveAs');
    // Llamamos nuestra funcion y le pasamos lso valores de entrada mockeados
    service['saveAsExcel'](mockbuffer, mockFileName);    // Verificamos que llame nuestro espia
    expect(spySaveAs).toHaveBeenCalled();

  });
/*
const EXCEL_TYPE =
    'application/vnd.openxmlfornats-officedocument.spreadsheetml.sheet; charset=UTF-8';
const EXEL_EXT = '.xlsx';
@Injectable()
export class ExporterService {

    workbook: XLSX.WorkBook;

    constructor() { }

    exportAExcel(json: any[], excelFileName: String): void {
        const worksheet: XLSX.Sheet = XLSX.utils.json_to_sheet(json);
        this.workbook = {
            Sheets: {
                'data': worksheet

            },
            SheetNames: ['data'],
            Props: {Title: 'REPORTE DE USUARIOS'}
        };
        this.workbook.Props.Title = 'REPORTE DE USUARIOS';
        const excelBuffer: any = XLSX.write(this.workbook, { bookType: 'xlsx', type: 'array' });
        // Llamar al metodo: buffer y fileName
        this.saveAsExcel(excelBuffer, excelFileName);

    }
    private saveAsExcel(buffer: any, fileName: String): void {
        const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
        FileSaver.saveAs(data, fileName + '_export_' + new Date().getTime() + EXEL_EXT);
    }

*/



});
