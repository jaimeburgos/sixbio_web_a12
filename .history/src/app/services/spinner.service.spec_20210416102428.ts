import { TestBed } from '@angular/core/testing';

import { SpinnerService } from './spinner.service';

fdescribe('SpinnerService', () => {
  let service: SpinnerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SpinnerService);
  });

  it('SpinnerService creado correctamente', () => {
    expect(service).toBeTruthy();
  });
});
