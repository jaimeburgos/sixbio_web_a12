export class EnvService {

  // The values that are defined here are the default values that can
  // be overridden by env.js

  // API url
  public apiUrl = '';

  // Whether or not to enable debug mode
  public enableDebug = true;
  public servicio = 'localhost';
  public puerto = '8180';
  public app = 'SIXBIO-webcore-Nova';

  constructor() {
  }

}
