import { Component, OnInit, ViewChild } from '@angular/core';
import { IUsuario } from '../../nucleo/interface/IUsuario';
import { Router } from '@angular/router';
import { ServicioService } from '../../servicio/servicio.service';
import { routerTransition } from '../../router.animations';
import { Constant } from './../../nucleo/constante/Constant';
import { Util } from '../../nucleo/util/Util';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from 'src/app/layout/bs-component/components';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { AbstractControlDirective, FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { BsModalService, BsModalRef, ModalDirective } from 'ngx-bootstrap/modal';
import { TemplateRef } from '@angular/core';

declare const AesUtil: any;
@Component({
    selector: 'app-first-login',
    templateUrl: './first-login.component.html',
    styleUrls: ['./first-login.component.scss'],
    animations: [routerTransition()]
})
export class FirstLoginComponent implements OnInit {


    @ViewChild(ToastComponent, { static: true }) toast: ToastComponent;
    @ViewChild(ModalDirective, { static: false }) modal: ModalDirective;

    dataRequest: Object = {};
    dataRequestSingOff: Object = {};

    entidad: IUsuario = {
        user: '',
        newPassword: '',
        oldPassword: '',
        confirmedPassword: ''
    };
    util: Util;

    changeForm: FormGroup;
    aceptar: Boolean;

    showModal() {
        this.modal.show();
    }

    constructor(public router: Router,  public servicioService: ServicioService,  private modalService: NgbModal, private fb: FormBuilder) {
            this.util = new Util(modalService);
    }


    ngOnInit() {

        this.changeForm =  this.fb.group({
            old:   new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(6),
                Validators.maxLength(16),
                Validators.pattern('[a-zA-Z0-9-_@\.\:\.\,\;\(\)\/\u00f1\u00d1]+')
            ])),
            new:   new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(6),
                Validators.maxLength(16),
                Validators.pattern('[a-zA-Z0-9-_@\.\:\.\,\;\(\)\/\u00f1\u00d1]+')
            ])),
            confirm:   new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(6),
                Validators.maxLength(16),
                Validators.pattern('[a-zA-Z0-9-_@\.\:\.\,\;\(\)\/\u00f1\u00d1]+')
            ]))
          });

        if (localStorage.getItem('isFirstLogin') === 'true') {

            this.servicioService.serviceConsultDataChangePassword(this.dataRequest).subscribe((dataResponse: any) => {
                if (dataResponse.codigoRespuesta !== Constant.COD_OPERACION_SATISFACTORIA_SERVICE_CONSULT_DATA_CHANGE_PASSWORD) {
                    console.log('Diferente del correcto inicio');
                    localStorage.clear();
                    console.log('Diferente del correcto antes del toast');
                    this.toast.addToast({ title: 'Error', msg: dataResponse.codigoRespuesta, timeOut: 5000, type: 'error' });
                    console.log('Despues del toast');
                    this.redirigirLogin();
                    console.log('Diferente del correcto fin');
                }
            });
        }

    }

    setDataSignOff() {
        return {
        };
    }
    cancel() {
        this.salir();
    }

    salir() {
        if (localStorage.getItem('changePasswordPorCaducar') === 'true') {
          localStorage.clear();
            this.singoff();
        }
        if (localStorage.getItem('isFirstLogin') === 'true' && localStorage.getItem('changePassword') !== 'true') {
          localStorage.clear();
          this.router.navigate(['/login']);
        }
        if (localStorage.getItem('changePassword') === 'true') {
          localStorage.setItem('changePassword', 'false');
          this.router.navigate(['/home']);
        } else {
            localStorage.clear();
            this.singoff();
        }
    }

    singoff() {
        this.dataRequestSingOff = this.setDataSignOff();
        this.servicioService.serviceSignOff(this.dataRequestSingOff).subscribe((dataResponse: any) => {

            if (dataResponse.codigoRespuesta === Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO) {
                localStorage.clear();
                this.redirigirLogin();
            } else {
                console.log('Entre en else de la funcion singoff()');
                localStorage.clear();
                this.redirigirLogin();

            }
        }
            , error => {
                console.log('Caso de error singoff() TEST');
                localStorage.clear();
                this.redirigirLogin();
            });
    }

    confirm() {
        this.aceptar = true;
        this.modal.hide();
    }
    decline() {
        this.aceptar = false;
        this.modal.hide();
    }

    evento() {
        console.log('Desparecio modal');
        console.log('Valor de aceptar es ' + this.aceptar);
        if ( this.aceptar) {
            this.dataRequest = this.setDataChangePassword(this.changeForm.getRawValue().old, this.changeForm.getRawValue().new);
            this.servicioService.serviceChangePassword(this.dataRequest).subscribe((dataResponse: any) => {
                if ((dataResponse.codigoRespuesta === Constant.COD_OPERACION_SATISFACTORIA_CAMBIO_CLAVE)
                    || (dataResponse.codigoRespuesta === Constant.COD_OPERACION_SATISFACTORIA_CAMBIO_CLAVE_DEMANDA)) {
                    this.toast.addToast({ title: 'Success', msg: dataResponse.codigoRespuesta, timeOut: 5000, type: 'success' });
                    localStorage.clear();
                    this.singoff();
                } else if (dataResponse.codigoRespuesta === Constant.COD_ERROR_SESION) {
                    this.toast.addToast({ title: 'Error', msg: dataResponse.codigoRespuesta, timeOut: 5000, type: 'error' });
                    localStorage.clear();
                    this.singoff();
                } else if (this.validarRespuesta(dataResponse.codigoRespuesta)) {
                    this.toast.addToast({ title: 'Error', msg: dataResponse.codigoRespuesta, timeOut: 5000, type: 'error' });
                } else {
                    this.toast.addToast({ title: 'Error', msg: 'Error de conectividad', timeOut: 5000, type: 'error' });
                    setTimeout(() => {localStorage.clear(); this.singoff(); }, 500);
                }

            }
                , error => {
                    this.toast.addToast({ title: 'Error', msg: 'Error de conectividad', timeOut: 5000, type: 'error' });
                    localStorage.clear();
                    this.singoff();
                });
        }
    }

    change(values) {
        console.log(values);
        if (this.confirmacionContrasenia(values)) {
            this.showModal();
        } else {
            this.toast.addToast({ title: 'Error',
             msg: Constant.MENSAJE_ERROR_VALIDACION_CONFIRMACION_CLAVE, timeOut: 5000, type: 'error' });
        }
    }







    setDataChangePassword(oldPassword: String, newPassword: String) {
        return {
            claveAntigua: this.encrypted(oldPassword.trim()),
            claveNueva: this.encrypted(newPassword.trim())
        };
    }

    redirigirLogin() {
        this.router.navigate(['/login']);
    }

    redirigirCambio() {
        this.router.navigate(['/firstLogin']);
    }

    encrypted(password: String) {
        const iv = 'F27D5C9927726BCEFE7510B1BDD3D137';
        const salt = '3FF2EC019C627B945225DEBAD71A01B6985FE84C95A70EB132882F88C0A59A55';
        const keySize = 128;
        const iterationCount = 10000;
        const passPhrase = 'sixbiowebaclient';
        const aesUtil = new AesUtil(keySize, iterationCount);
        const encrypt = aesUtil.encrypt(salt, iv, passPhrase, password);
        return encrypt;
    }

    limpiarCampos() {
        this.entidad.confirmedPassword = null;
        this.entidad.oldPassword = null;
        this.entidad.newPassword = null;
    }

    confirmacionContrasenia(values): boolean {
        if (values.new === values.confirm) {
            return true;
        }
        return false;
    }
    validarRespuesta(c) {
        // tslint:disable-next-line:max-line-length
        if (c === '1004' || c === '00000' || c === '00001' || c === '1009' || c === '1002' || c === '1003' || c === '0000' || c === '1006') {
            return true;
        } else {
            return false;
        }
    }
}
