import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';

import { FirstLoginRoutingModule } from './first-login-routing.module';
import { FirstLoginComponent } from './first-login/first-login.component';
import { BsComponentModule } from '../layout/bs-component/bs-component.module';

@NgModule({
  declarations: [FirstLoginComponent],
  imports: [
    CommonModule,
    FirstLoginRoutingModule,
    FormsModule,
    TranslateModule,
    BsComponentModule
  ]
})
export class FirstLoginModule { }
