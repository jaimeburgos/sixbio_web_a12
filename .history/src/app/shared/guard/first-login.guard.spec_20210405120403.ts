import { TestBed, async, inject } from '@angular/core/testing';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Subject } from 'rxjs';
import { FirstLoginGuard } from './first-login.guard';

class FakeRouter {
  navigate(params) {
  }
}

class FakeActivatedRoute {
  // params: Observable<any> = Observable.empty();
  private subject = new Subject();

  push( valor ) {
    this.subject.next( valor );
  }
  get params() {
    return this.subject.asObservable();
  }
}

fdescribe('AuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      providers: [
        FirstLoginGuard,
        {provide: Router , useClass: FakeRouter},
        {provide: ActivatedRoute, useClass: FakeActivatedRoute },
      ]
    });
  });

  it('AuthGuard Creado Correctamente',
  inject([FirstLoginGuard], (guard: FirstLoginGuard) => {
    expect(guard).toBeTruthy();
  }));

  it('Verificamos funcionamiento AuthGuard', inject([FirstLoginGuard], (guard: FirstLoginGuard) => {
    // Setemos en nuestro local.storage 'isFirstLogin' con TRUE
    localStorage.setItem('isFirstLogin', 'true');
    // Llamamos nuestra funcion canActivate de FirstLoginGuard y verificamos que retorne TRUE
    expect(guard.canActivate()).toBeTruthy();
    // Ahora verificamos el caso contrario
    // Donde no hay nada en el local storage
    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate').and.callThrough();
    localStorage.clear();
    // Verificamos que nos retorne FALSE el guard dado que no hay ninguno valor asignado a isFirstLogin , ni existe
    expect(guard.canActivate()).toBeFalsy();
    // Verificamos que llave nuestro Router.navigate
    expect(spyRouter).toHaveBeenCalled();
  }));



});
