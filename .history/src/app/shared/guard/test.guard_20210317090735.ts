import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router) {}

    canActivatetest() {
        console.log('localStorage isLoggedin : [' + localStorage.getItem('isLoggedin') + ']');
        if (localStorage.getItem('isLoggedin')) {

            console.log('AUTHGUARD TRUE ');
            return true;
        }

        console.log('AUTHGUARD FALSE ');
        this.router.navigate(['/login']);
        return false;
    }
}
