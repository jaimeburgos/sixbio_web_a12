import { TestBed, async, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AuthGuard } from './auth.guard';

fdescribe('AuthGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      providers: [AuthGuard]
    });
  });

  it('AuthGuard Creado Correctamente',
  inject([AuthGuard], (guard: AuthGuard) => {
    expect(guard).toBeTruthy();
  }));

  it('Verificamos funcionamiento AuthGuard', inject([AuthGuard], (guard: AuthGuard) => {
    // Setemos en nuestro local.storage 'isLoggedin' con TRUE
    localStorage.setItem('isLoggedin', 'true');
    // Llamamos nuestra funcion canActivate de AuthGuard y verificamos que retorne TRUE
    expect(guard.canActivate()).toBeTruthy();

    // Ahora verificamos el caso contrario
    // Donde no hay nada en el local storage
    localStorage.clear();
    expect(guard.canActivate()).toBeFalsy();


  }));



});
