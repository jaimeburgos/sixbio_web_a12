import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class TestGuard implements CanActivate {
    constructor(private router: Router) {}

    canActivate() {
            console.log('RolUsuario de TestGuard' + localStorage.getItem('RolUsuario'));
        if
        ( true
            // localStorage.getItem('RolUsuario') === 'EMP_ADM'
        ) {
            console.log('TestGuard TRUE ');
            return true;
        }
        console.log('TestGuard FALSE ');
        this.router.navigate(['/login']);
        return false;
    }
}
