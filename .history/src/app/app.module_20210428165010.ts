import { CommonModule, LocationStrategy, HashLocationStrategy, PathLocationStrategy } from '@angular/common';
import { HttpClientModule, HttpClientJsonpModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LanguageTranslationModule } from './shared/modules/language-translation/language-translation.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthGuard, FirstLoginGuard } from './shared';
// import { BsComponentModule } from './layout/bs-component/bs-component.module';
import { EnvServiceProvider } from './env.service.provider';
import { FooterComponent } from './layout/components/footer/footer.component';
// DatePicker
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TranslateModule, TranslatePipe } from '@ngx-translate/core';
import { InterceptorService } from './services/interceptor.service';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { defineLocale, esLocale } from 'ngx-bootstrap/chronos';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { FirstLoginComponent } from './first-login/first-login/first-login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { AccessDeniedComponent } from './access-denied/access-denied.component';
import { BsComponentModule } from './layout/bs-component/bs-component.module';
import { ServerErrorComponent } from './server-error/server-error.component';
import { HeaderComponent } from './layout/components/header/header.component';
import { SidebarComponent } from './layout/components/sidebar/sidebar.component';
import { LayoutComponent } from './layout/layout.component';
import { NgbDropdownModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxSpinnerModule } from 'ngx-spinner';


defineLocale('es', esLocale);
@NgModule({
  declarations: [
        AppComponent,
        LoginComponent,
        FirstLoginComponent,
        NotFoundComponent,
        AccessDeniedComponent,
        ServerErrorComponent,
    ],
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        LanguageTranslationModule,
        AppRoutingModule,
        BsComponentModule,
        HttpClientJsonpModule,
        TranslateModule,
        FormsModule,
        NgbDropdownModule,
        NgbModule,
        ReactiveFormsModule,
        BsDatepickerModule.forRoot(),
        ModalModule.forRoot(),
        NgxSpinnerModule,
    ],
    // tslint:disable-next-line:max-line-length
    providers: [
      AuthGuard,
      FirstLoginGuard,
      TranslatePipe ,
      EnvServiceProvider,
      { provide: LocationStrategy, useClass: HashLocationStrategy },
      {provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true}
    ],
    bootstrap: [AppComponent],
    schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class AppModule {}
