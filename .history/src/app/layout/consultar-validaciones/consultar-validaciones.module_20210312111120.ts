import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsultarValidacionesRoutingModule } from './consultar-validaciones-routing.module';
import { ConsultarValidacionesComponent } from './consultar-validaciones.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { DetalleValidacionComponent } from './components/detalle-validacion/detalle-validacion.component';
import { TranslateModule } from '@ngx-translate/core';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
import { DatePipe } from '@angular/common';
defineLocale('es', esLocale);

@NgModule({
  declarations: [ConsultarValidacionesComponent, DetalleValidacionComponent],
  imports: [
    CommonModule,
    ConsultarValidacionesRoutingModule,
    NgxPaginationModule,
    BsComponentModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot()
  ],
  providers: [DatePipe]

})
export class ConsultarValidacionesModule { }
