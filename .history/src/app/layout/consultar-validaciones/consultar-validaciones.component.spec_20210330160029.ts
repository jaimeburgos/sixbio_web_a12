import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import * as FileSaver from 'file-saver';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { async, of } from 'rxjs';
import { EnvService } from 'src/app/env.service';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { ToastComponent } from '../bs-component/components/toast/toast.component';
import { ConsultarValidacionesComponent } from './consultar-validaciones.component';

fdescribe('ConsultarValidacionesComponent', () => {
  let component: ConsultarValidacionesComponent;
  let fixture: ComponentFixture<ConsultarValidacionesComponent>;
  // Instanciamos los servicios que utilizaremos
  let translateService: TranslateService;
  let servicio: ServicioService;
  beforeEach((() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultarValidacionesComponent, ToastComponent ],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService ,
        {provide: ToastrService, useClass: ToastrService}],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultarValidacionesComponent);
    translateService = TestBed.inject(TranslateService);
    // Injectamos nuestro servicio con TestBed.inject
    servicio = TestBed.inject(ServicioService);
    component = fixture.componentInstance;
    fixture.detectChanges();
    // Mockeamos valor en localstorage de usuarioLogueado
    localStorage.setItem('usuarioLogueado', 'jburgos6');
  });

  it('consultar validaciones component creado correctamente', () => {
    expect(component).toBeTruthy();
  });


  // Validar restricciones del consultarValidacionesForm
  it('filterUsuario es obligatorio en consultarValidacionesForm', () => {
    // Obtenemos en una constante el ControlForm que deseamos evaluar
    const filterUsuario = component.consultarValidacionesForm.get('filterUsuario');
    // Le asignamos un valor
    filterUsuario.setValue('');
    // La expectativa de la prueba de ser falso ya que no permite vacio
    expect(filterUsuario.valid).toBeFalsy();
  });


  it('filterfechaHasta es obligatorio en consultarValidacionesForm', () => {
    const filterfechaHasta = component.consultarValidacionesForm.get('filterfechaHasta');
    filterfechaHasta.setValue('');
    expect(filterfechaHasta.valid).toBeFalsy();
    filterfechaHasta.setValue(null);
    expect(filterfechaHasta.valid).toBeFalsy();
  });

  it('filterfechaDesde es obligatorio en consultarValidacionesForm', () => {
    const filterfechaDesde = component.consultarValidacionesForm.get('filterfechaDesde');
    filterfechaDesde.setValue('');
    expect(filterfechaDesde.valid).toBeFalsy();
    filterfechaDesde.setValue(null);
    expect(filterfechaDesde.valid).toBeFalsy();
  });

  it('cantidad es obligatorio en consultarValidacionesForm', () => {
    const cantidad = component.consultarValidacionesForm.get('cantidad');
    cantidad.setValue('');
    expect(cantidad.valid).toBeFalsy();
    cantidad.setValue(null);
    expect(cantidad.valid).toBeFalsy();
  });


  it('Campo "cantidad" puede tener maximo 4 caracteres consultarValidacionesForm', () => {
    const cantidad = component.consultarValidacionesForm.get('cantidad');
    cantidad.setValue('1234');
    expect(cantidad.valid).toBeTruthy();
    cantidad.setValue('12345');
    expect(cantidad.valid).toBeFalsy();
  });


  it('Campo "cantidad" verificamos que solo acepte valores numericos', () => {
    const cantidad = component.consultarValidacionesForm.get('cantidad');
    cantidad.setValue('1234');
    expect(cantidad.valid).toBeTruthy();
    cantidad.setValue('abc');
    expect(cantidad.valid).toBeFalsy();
    cantidad.setValue('""');
    expect(cantidad.valid).toBeFalsy();
    cantidad.setValue('++--');
    expect(cantidad.valid).toBeFalsy();
    cantidad.setValue('    ');
    expect(cantidad.valid).toBeFalsy();
  });


  it('Se carga usuarioLogeado desde localStorage en filterUsuario del FormControl', () => {
    component.ngOnInit();
   const filterUsuario = component.consultarValidacionesForm.getRawValue().filterUsuario;
   // verificamos que el filterUsuario sea igual al valor recuperado del localStorage
    expect(filterUsuario).toBe(localStorage.getItem('usuarioLogueado'));
    // verificamos que mantenga la misma longitud
    expect(filterUsuario.length).toBe(8);
    // verificamos que el usuario cumpla los requisitos de ser mayor o igual 5 caracteres
    expect(filterUsuario.length).toBeGreaterThanOrEqual(5);
  });


  it('Verificamos la funcion ngOnInit()', () => {
    // Espiamos la funcion recuperarConsultaValidaciones
    const spyrecuperarConsultaValidaciones = spyOn(component, 'recuperarConsultaValidaciones').and.callThrough();
    const spylistarUsuarios = spyOn(component, 'listarUsuarios').and.callThrough();
    const spyfilterRadioButtonCantidad = spyOn(component, 'filterRadioButtonCantidad').and.callThrough();
    const spyfilterRadioButtonFechas = spyOn(component, 'filterRadioButtonFechas').and.callThrough();
    // Llamamos nuestra funcion
    component.ngOnInit();
    // Verficamos los valores esperados
    expect(component.listaValidaciones ).toEqual([]);
    expect(component.sizePagination  ).toEqual('10');
    expect(component.p  ).toEqual(1);
    expect(component.listaUsuariosSixbio  ).toEqual([]);
    expect(component.listaUsuarios  ).toEqual([]);
    // Verificamos que llame nuestros espias
    expect(spyrecuperarConsultaValidaciones).toHaveBeenCalled();
    expect(spylistarUsuarios).toHaveBeenCalled();
    expect(spyfilterRadioButtonCantidad).toHaveBeenCalled();
    expect(spyfilterRadioButtonFechas).toHaveBeenCalled();
    // Verificamos el valor asignado a filterUsuario , que es el usuario logeado que se Mockeamos
    const filterUsuario = component.consultarValidacionesForm.getRawValue().filterUsuario;
    expect(filterUsuario).toEqual('jburgos6');
  });

  it('Verificamos funcion onChangeForm()', () => {
    // Para ello seteamos un valor para probar la validacion
    component.listaValidaciones.length = 10;
    // Llamamos nuestra funcion
    component.onChangeForm();
    // Verificamos los valores
    expect(component.listaValidaciones).toEqual([]);
    expect(component.listaValidacionesExportar ).toEqual([]);
    expect(component.listarValidacionesOK ).toBeFalsy();
  });


  it('Verificamos funcion onChangeCantidad()', () => {
    // Espiamos la funcion isExcelDisabled();
    const spyIsExcel = spyOn(component, 'isExcelDisabled').and.callThrough();
    // Espiamos la funcion isPDFDisabled
    const spyIsPDF = spyOn(component, 'isPDFDisabled').and.callThrough();
    // Seteamos un valor para probar la condicion de la funcion onChangeCantidad
    component.listaValidaciones.length = 10;
    // Llamamos nuestra funcion
    component.onChangeCantidad();
    // Verificamos los valores
    expect(component.listaValidaciones).toEqual([]);
    expect(component.listaValidacionesExportar ).toEqual([]);
    expect(component.listarValidacionesOK ).toBeFalsy();
    // Verificamos que llame nuestros espias
    expect(spyIsPDF).toHaveBeenCalled();
    expect(spyIsExcel).toHaveBeenCalled();
  });

  it('Verificamos funcion onChangeFecha()', () => {
    // Espiamos la funcion isExcelDisabled();
    const spyIsExcel = spyOn(component, 'isExcelDisabled').and.callThrough();
    // Espiamos la funcion isPDFDisabled
    const spyIsPDF = spyOn(component, 'isPDFDisabled').and.callThrough();
    // Seteamos un valor para probar la condicion de la funcion onChangeCantidad
    component.listaValidaciones.length = 10;
    // Llamamos nuestra funcion
    component.onChangeFecha();
    // Verificamos los valores
    expect(component.listaValidaciones).toEqual([]);
    expect(component.listaValidacionesExportar ).toEqual([]);
    expect(component.listarValidacionesOK ).toBeFalsy();
    // Verificamos que llame nuestros espias
    expect(spyIsPDF).toHaveBeenCalled();
    expect(spyIsExcel).toHaveBeenCalled();
  });

  it('Verificamos la funcion isExcelDisabled()', () => {
    // Seteamos una propiedad que genera invalid en el Formulario Reactivo
    component.consultarValidacionesForm.controls.cantidad.setValue('');
    // Llamamos nuestra funcion
    component.isExcelDisabled();
    // Verificamos valores
    expect(component.isClicExcel ).toBeTruthy();
    // Verificamos cuando formulario es valid , para ello seteamos valores validos
    component.consultarValidacionesForm.controls.cantidad.setValue('111');
    component.consultarValidacionesForm.controls.filterUsuario.setValue('abenito');
    // Llamamos nuestra funcion
    component.isExcelDisabled();
    // Verificamos valores
    expect(component.isClicExcel ).toBeFalsy();
  });


  it('Verificamos la funcion isPDFDisabled()', () => {
    // Seteamos una propiedad que genera invalid en el Formulario Reactivo
    component.consultarValidacionesForm.controls.cantidad.setValue('');
    // Llamamos nuestra funcion
    component.isPDFDisabled();
    // Verificamos valores
    expect(component.isClicPDF ).toBeTruthy();
    // Verificamos cuando formulario es valid , para ello seteamos valores validos
    component.consultarValidacionesForm.controls.cantidad.setValue('111');
    component.consultarValidacionesForm.controls.filterUsuario.setValue('abenito');
    // Llamamos nuestra funcion
    component.isPDFDisabled();
    // Verificamos valores
    expect(component.isClicPDF ).toBeFalsy();
  });

  it('Verificamos la funcion exportarExcel', () => {
    // Espiamos nuestro metodo setTipoReporte
    const spySetTipoReporte = spyOn(component, 'setTipoReporte').and.callThrough();
    // Espiamos nuestro metodo setTipoReporte
    const spybuscarValidacionesExportar = spyOn(component, 'buscarValidacionesExportar').and.callThrough();
    // Espiamos nuestro metodo setFechaActual
    const spysetFechaActual = spyOn(component, 'setFechaActual').and.callThrough();
    // Llamamos nuestra funcion
    component.exportarExcel();
    // Verificamos que llame nuestros espias
    expect(spybuscarValidacionesExportar).toHaveBeenCalled();
    expect(spysetFechaActual).toHaveBeenCalled();
    expect(spySetTipoReporte).toHaveBeenCalled();
    // Verificamos los valores de isClicPDF isClicExcel
    expect(component.isClicExcel).toBeTruthy();
    expect(component.isClicPDF).toBeTruthy();
  });

  it('Verificamos la funcion exportarPDF', () => {
    // Espiamos nuestro metodo setTipoReporte
    const spySetTipoReporte = spyOn(component, 'setTipoReporte').and.callThrough();
    // Espiamos nuestro metodo setTipoReporte
    const spybuscarValidacionesExportar = spyOn(component, 'buscarValidacionesExportar').and.callThrough();
    // Espiamos nuestro metodo setFechaActual
    const spysetFechaActual = spyOn(component, 'setFechaActual').and.callThrough();
    // Llamamos nuestra funcion
    component.exportarPDF();
    // Verificamos que llame nuestros espias
    expect(spybuscarValidacionesExportar).toHaveBeenCalled();
    expect(spysetFechaActual).toHaveBeenCalled();
    expect(spySetTipoReporte).toHaveBeenCalled();
    // Verificamos los valores de isClicPDF isClicExcel
    expect(component.isClicExcel).toBeTruthy();
    expect(component.isClicPDF).toBeTruthy();
  });

  it('Verificamos la funcion recuperarConsultaValidaciones()', () => {
    // Espiamos la funcion validarRegresoDetalleValidacion
    const spy = spyOn(component, 'validarRegresoDetalleValidacion').and.callThrough();
    // Llamamos nuestra funcion
    component.recuperarConsultaValidaciones();
    // Verificamos que llame nuestro espia
    expect(spy).toHaveBeenCalled();
  });


  it('Verificamos la funcion validarRegresoDetalleValidacion()', () => {
    // Probamos la primera condicion cuando regresarDetalleVal del LocalStorage sea TRUE, recodar que es una cadena
    localStorage.setItem('regresarDetalleVal', 'true');
    // Espiamos nuestra funcion recuperarDatosFiltrosPropiedades
    const spyRecuperarDatosFiltroPropiedades = spyOn(component, 'recuperarDatosFiltrosPropiedades').and.callThrough();
    // Espiamos buscarValidaciones
    const spyBuscarValidaciones = spyOn(component, 'buscarValidaciones').and.callThrough();
    // Llamamos nuestra funcion
    component.validarRegresoDetalleValidacion();
    // Verificamos que llamen nuestros espias
    expect(spyRecuperarDatosFiltroPropiedades).toHaveBeenCalled();
    expect(spyBuscarValidaciones).toHaveBeenCalled();
    // Verificamos los valores de isClicPDF y isClicExcel
    expect(component.isClicPDF).toBeFalsy();
    expect(component.isClicExcel).toBeFalsy();
    // Verificamos valor que se seta a regresarDetalleVal
    expect(localStorage.getItem('regresarDetalleVal')).toEqual('false');



    // Ahora verificamos el caso contrario para ello le asignamos FALSE a regresarDetalleVal , recodar que es una cadena
    localStorage.clear();
    localStorage.setItem('regresarDetalleVal', 'false');
    // Llamamos nuestra funcion
    component.validarRegresoDetalleValidacion();
    // Ahora verificamos los valores
    expect(localStorage.getItem('campoFechaDesde')).toEqual('false');
    expect(localStorage.getItem('campoFechaHasta')).toEqual('false');
    expect(localStorage.getItem('campoCantidad')).toEqual('true');
    expect(localStorage.getItem('ischeckCantidad')).toEqual('true');
    expect(localStorage.getItem('ischeckFechas')).toEqual('false');
    expect(localStorage.getItem('regresarDetalleVal')).toEqual('false');
  });


  it('Verificamos funcion guardarPropiedades()', () => {
    // Seteamos datos para probar la funcion
    const p = 2;
    const sizePagination = '10';
    component.p = p;
    component.sizePagination = sizePagination;
    // Llamamos nuestra funcion
    component.guardarPropiedades();
    // Verificamos los valores asignados al LocalStorage
    expect(localStorage.getItem('numeroPagina')).toEqual(p.toString());
    expect(localStorage.getItem('tamanioPagina')).toEqual(sizePagination);
  });

  it('Verificamos la funcion recuperarDatosFiltrosPropiedades()', async() => {
    // Seteamos valores en los Constants
    const FDesde = '"2014-05-15';
    const FHasta = '"2021-03-25"';
    const Usuario = 'jburgos6';
    const Cantidad = '';
    Constant.FILTRO_FDESDE = FDesde;
    Constant.FILTRO_FHASTA = FHasta;
    Constant.FILTRO_USUARIO = Usuario;
    Constant.FILTRO_CANTIDAD = Cantidad;
    // Seteamos valores en el localStorage
    const tamanioPagina = '10';
    const numeroPagina = '2';
    localStorage.clear();
    localStorage.setItem('tamanioPagina', '10');
    localStorage.setItem('numeropagina', '2');
    // Fakeamos las funcion que usen datePipe para evitar errores
    spyOn(component, 'setDataFechaBusqueda').and.callFake(() => {'Nada'; });
    spyOn(component, 'setDataFechaBusquedaExportar').and.callFake(() => {'Nada'; });
    // Llamamos nuestra funcion
    component.recuperarDatosFiltrosPropiedades();
    // Ahora verificamos los valores seteamos
    expect(component.sizePagination).toEqual(tamanioPagina);
    expect(component.numeroPaginas).toEqual(2);
    // Ahora verificamos las asignaciones a nuestro Formulario
    expect(component.consultarValidacionesForm.getRawValue().filterfechaDesde).toEqual(FDesde);
    expect(component.consultarValidacionesForm.getRawValue().filterfechaHasta).toEqual(FHasta);
    expect(component.consultarValidacionesForm.getRawValue().filterUsuario).toEqual(Usuario);
    expect(component.consultarValidacionesForm.getRawValue().cantidad).toEqual(Cantidad);
  });

  it('Verificamos la funcion exportar(extension:string)', () => {
    // Espiamos nuestra funcion setdataRequestReporte
    const spySetDataRequestReporte = spyOn(component, 'setdataRequestReporte').and.callThrough();
    // Espiamos nuestro servicio serviceGenerarReporteValidaciones
    const spyService = spyOn(servicio, 'serviceGenerarReporteValidaciones').and.callThrough();
    // Llamamos nuestra funcion , le pasamos una extension
    component.exportar('PDF');
    // Verificamos que nuestro servicio fuera llamado
    expect(spyService).toHaveBeenCalled();
    // Verificamos que nuestra funcion setdataRequestReporte fuera llamada
    expect(spySetDataRequestReporte).toHaveBeenCalled();

    // Ahora verificamos las condiciones de respuesta del servicio
    // Mockeamos un DataResponse
    const mockDataResponse = 'Esto es una respuesta';
    // Fakeamos nuestro servicio para que nos devuelva el mockDataResponse
     const spyServiceFake = spyOn(servicio, 'serviceGenerarReporteValidaciones').and.callFake(() => {
       return of(mockDataResponse);
     });
    //  Espiamos la funcion SaveAs
    const spySaveAs = spyOn(FileSaver, 'saveAs').and.stub();
    // Llamamos nuestra funcion , le pasamos una extension
    component.exportar('PDF');
    // Verificamos que llame a SaveAs
    expect(spySaveAs).toHaveBeenCalled();



  });




});
