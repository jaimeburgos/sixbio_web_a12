import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import * as FileSaver from 'file-saver';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { async, of, throwError } from 'rxjs';
import { EnvService } from 'src/app/env.service';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { ToastComponent } from '../bs-component/components/toast/toast.component';
import { ConsultarValidacionesComponent } from './consultar-validaciones.component';

fdescribe('ConsultarValidacionesComponent', () => {
  let component: ConsultarValidacionesComponent;
  let fixture: ComponentFixture<ConsultarValidacionesComponent>;
  // Instanciamos los servicios que utilizaremos
  let translateService: TranslateService;
  let servicio: ServicioService;
  beforeEach((() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultarValidacionesComponent, ToastComponent ],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService ,
        {provide: ToastrService, useClass: ToastrService}],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultarValidacionesComponent);
    translateService = TestBed.inject(TranslateService);
    // Injectamos nuestro servicio con TestBed.inject
    servicio = TestBed.inject(ServicioService);
    component = fixture.componentInstance;
    fixture.detectChanges();
    // Mockeamos valor en localstorage de usuarioLogueado
    localStorage.setItem('usuarioLogueado', 'jburgos6');
  });

  it('consultar validaciones component creado correctamente', () => {
    expect(component).toBeTruthy();
  });


  // Validar restricciones del consultarValidacionesForm
  it('filterUsuario es obligatorio en consultarValidacionesForm', () => {
    // Obtenemos en una constante el ControlForm que deseamos evaluar
    const filterUsuario = component.consultarValidacionesForm.get('filterUsuario');
    // Le asignamos un valor
    filterUsuario.setValue('');
    // La expectativa de la prueba de ser falso ya que no permite vacio
    expect(filterUsuario.valid).toBeFalsy();
  });


  it('filterfechaHasta es obligatorio en consultarValidacionesForm', () => {
    const filterfechaHasta = component.consultarValidacionesForm.get('filterfechaHasta');
    filterfechaHasta.setValue('');
    expect(filterfechaHasta.valid).toBeFalsy();
    filterfechaHasta.setValue(null);
    expect(filterfechaHasta.valid).toBeFalsy();
  });

  it('filterfechaDesde es obligatorio en consultarValidacionesForm', () => {
    const filterfechaDesde = component.consultarValidacionesForm.get('filterfechaDesde');
    filterfechaDesde.setValue('');
    expect(filterfechaDesde.valid).toBeFalsy();
    filterfechaDesde.setValue(null);
    expect(filterfechaDesde.valid).toBeFalsy();
  });

  it('cantidad es obligatorio en consultarValidacionesForm', () => {
    const cantidad = component.consultarValidacionesForm.get('cantidad');
    cantidad.setValue('');
    expect(cantidad.valid).toBeFalsy();
    cantidad.setValue(null);
    expect(cantidad.valid).toBeFalsy();
  });


  it('Campo "cantidad" puede tener maximo 4 caracteres consultarValidacionesForm', () => {
    const cantidad = component.consultarValidacionesForm.get('cantidad');
    cantidad.setValue('1234');
    expect(cantidad.valid).toBeTruthy();
    cantidad.setValue('12345');
    expect(cantidad.valid).toBeFalsy();
  });


  it('Campo "cantidad" verificamos que solo acepte valores numericos', () => {
    const cantidad = component.consultarValidacionesForm.get('cantidad');
    cantidad.setValue('1234');
    expect(cantidad.valid).toBeTruthy();
    cantidad.setValue('abc');
    expect(cantidad.valid).toBeFalsy();
    cantidad.setValue('""');
    expect(cantidad.valid).toBeFalsy();
    cantidad.setValue('++--');
    expect(cantidad.valid).toBeFalsy();
    cantidad.setValue('    ');
    expect(cantidad.valid).toBeFalsy();
  });


  it('Se carga usuarioLogeado desde localStorage en filterUsuario del FormControl', () => {
    component.ngOnInit();
   const filterUsuario = component.consultarValidacionesForm.getRawValue().filterUsuario;
   // verificamos que el filterUsuario sea igual al valor recuperado del localStorage
    expect(filterUsuario).toBe(localStorage.getItem('usuarioLogueado'));
    // verificamos que mantenga la misma longitud
    expect(filterUsuario.length).toBe(8);
    // verificamos que el usuario cumpla los requisitos de ser mayor o igual 5 caracteres
    expect(filterUsuario.length).toBeGreaterThanOrEqual(5);
  });


  it('Verificamos la funcion ngOnInit()', () => {
    // Espiamos la funcion recuperarConsultaValidaciones
    const spyrecuperarConsultaValidaciones = spyOn(component, 'recuperarConsultaValidaciones').and.callThrough();
    const spylistarUsuarios = spyOn(component, 'listarUsuarios').and.callThrough();
    const spyfilterRadioButtonCantidad = spyOn(component, 'filterRadioButtonCantidad').and.callThrough();
    const spyfilterRadioButtonFechas = spyOn(component, 'filterRadioButtonFechas').and.callThrough();
    // Llamamos nuestra funcion
    component.ngOnInit();
    // Verficamos los valores esperados
    expect(component.listaValidaciones ).toEqual([]);
    expect(component.sizePagination  ).toEqual('10');
    expect(component.p  ).toEqual(1);
    expect(component.listaUsuariosSixbio  ).toEqual([]);
    expect(component.listaUsuarios  ).toEqual([]);
    // Verificamos que llame nuestros espias
    expect(spyrecuperarConsultaValidaciones).toHaveBeenCalled();
    expect(spylistarUsuarios).toHaveBeenCalled();
    expect(spyfilterRadioButtonCantidad).toHaveBeenCalled();
    expect(spyfilterRadioButtonFechas).toHaveBeenCalled();
    // Verificamos el valor asignado a filterUsuario , que es el usuario logeado que se Mockeamos
    const filterUsuario = component.consultarValidacionesForm.getRawValue().filterUsuario;
    expect(filterUsuario).toEqual('jburgos6');
  });

  it('Verificamos funcion onChangeForm()', () => {
    // Para ello seteamos un valor para probar la validacion
    component.listaValidaciones.length = 10;
    // Llamamos nuestra funcion
    component.onChangeForm();
    // Verificamos los valores
    expect(component.listaValidaciones).toEqual([]);
    expect(component.listaValidacionesExportar ).toEqual([]);
    expect(component.listarValidacionesOK ).toBeFalsy();
  });


  it('Verificamos funcion onChangeCantidad()', () => {
    // Espiamos la funcion isExcelDisabled();
    const spyIsExcel = spyOn(component, 'isExcelDisabled').and.callThrough();
    // Espiamos la funcion isPDFDisabled
    const spyIsPDF = spyOn(component, 'isPDFDisabled').and.callThrough();
    // Seteamos un valor para probar la condicion de la funcion onChangeCantidad
    component.listaValidaciones.length = 10;
    // Llamamos nuestra funcion
    component.onChangeCantidad();
    // Verificamos los valores
    expect(component.listaValidaciones).toEqual([]);
    expect(component.listaValidacionesExportar ).toEqual([]);
    expect(component.listarValidacionesOK ).toBeFalsy();
    // Verificamos que llame nuestros espias
    expect(spyIsPDF).toHaveBeenCalled();
    expect(spyIsExcel).toHaveBeenCalled();
  });

  it('Verificamos funcion onChangeFecha()', () => {
    // Espiamos la funcion isExcelDisabled();
    const spyIsExcel = spyOn(component, 'isExcelDisabled').and.callThrough();
    // Espiamos la funcion isPDFDisabled
    const spyIsPDF = spyOn(component, 'isPDFDisabled').and.callThrough();
    // Seteamos un valor para probar la condicion de la funcion onChangeCantidad
    component.listaValidaciones.length = 10;
    // Llamamos nuestra funcion
    component.onChangeFecha();
    // Verificamos los valores
    expect(component.listaValidaciones).toEqual([]);
    expect(component.listaValidacionesExportar ).toEqual([]);
    expect(component.listarValidacionesOK ).toBeFalsy();
    // Verificamos que llame nuestros espias
    expect(spyIsPDF).toHaveBeenCalled();
    expect(spyIsExcel).toHaveBeenCalled();
  });

  it('Verificamos la funcion isExcelDisabled()', () => {
    // Seteamos una propiedad que genera invalid en el Formulario Reactivo
    component.consultarValidacionesForm.controls.cantidad.setValue('');
    // Llamamos nuestra funcion
    component.isExcelDisabled();
    // Verificamos valores
    expect(component.isClicExcel ).toBeTruthy();
    // Verificamos cuando formulario es valid , para ello seteamos valores validos
    component.consultarValidacionesForm.controls.cantidad.setValue('111');
    component.consultarValidacionesForm.controls.filterUsuario.setValue('abenito');
    // Llamamos nuestra funcion
    component.isExcelDisabled();
    // Verificamos valores
    expect(component.isClicExcel ).toBeFalsy();
  });


  it('Verificamos la funcion isPDFDisabled()', () => {
    // Seteamos una propiedad que genera invalid en el Formulario Reactivo
    component.consultarValidacionesForm.controls.cantidad.setValue('');
    // Llamamos nuestra funcion
    component.isPDFDisabled();
    // Verificamos valores
    expect(component.isClicPDF ).toBeTruthy();
    // Verificamos cuando formulario es valid , para ello seteamos valores validos
    component.consultarValidacionesForm.controls.cantidad.setValue('111');
    component.consultarValidacionesForm.controls.filterUsuario.setValue('abenito');
    // Llamamos nuestra funcion
    component.isPDFDisabled();
    // Verificamos valores
    expect(component.isClicPDF ).toBeFalsy();
  });

  it('Verificamos la funcion exportarExcel', () => {
    // Espiamos nuestro metodo setTipoReporte
    const spySetTipoReporte = spyOn(component, 'setTipoReporte').and.callThrough();
    // Espiamos nuestro metodo setTipoReporte
    const spybuscarValidacionesExportar = spyOn(component, 'buscarValidacionesExportar').and.callThrough();
    // Espiamos nuestro metodo setFechaActual
    const spysetFechaActual = spyOn(component, 'setFechaActual').and.callThrough();
    // Llamamos nuestra funcion
    component.exportarExcel();
    // Verificamos que llame nuestros espias
    expect(spybuscarValidacionesExportar).toHaveBeenCalled();
    expect(spysetFechaActual).toHaveBeenCalled();
    expect(spySetTipoReporte).toHaveBeenCalled();
    // Verificamos los valores de isClicPDF isClicExcel
    expect(component.isClicExcel).toBeTruthy();
    expect(component.isClicPDF).toBeTruthy();
  });

  it('Verificamos la funcion exportarPDF', () => {
    // Espiamos nuestro metodo setTipoReporte
    const spySetTipoReporte = spyOn(component, 'setTipoReporte').and.callThrough();
    // Espiamos nuestro metodo setTipoReporte
    const spybuscarValidacionesExportar = spyOn(component, 'buscarValidacionesExportar').and.callThrough();
    // Espiamos nuestro metodo setFechaActual
    const spysetFechaActual = spyOn(component, 'setFechaActual').and.callThrough();
    // Llamamos nuestra funcion
    component.exportarPDF();
    // Verificamos que llame nuestros espias
    expect(spybuscarValidacionesExportar).toHaveBeenCalled();
    expect(spysetFechaActual).toHaveBeenCalled();
    expect(spySetTipoReporte).toHaveBeenCalled();
    // Verificamos los valores de isClicPDF isClicExcel
    expect(component.isClicExcel).toBeTruthy();
    expect(component.isClicPDF).toBeTruthy();
  });

  it('Verificamos la funcion recuperarConsultaValidaciones()', () => {
    // Espiamos la funcion validarRegresoDetalleValidacion
    const spy = spyOn(component, 'validarRegresoDetalleValidacion').and.callThrough();
    // Llamamos nuestra funcion
    component.recuperarConsultaValidaciones();
    // Verificamos que llame nuestro espia
    expect(spy).toHaveBeenCalled();
  });


  it('Verificamos la funcion validarRegresoDetalleValidacion()', () => {
    // Probamos la primera condicion cuando regresarDetalleVal del LocalStorage sea TRUE, recodar que es una cadena
    localStorage.setItem('regresarDetalleVal', 'true');
    // Espiamos nuestra funcion recuperarDatosFiltrosPropiedades
    const spyRecuperarDatosFiltroPropiedades = spyOn(component, 'recuperarDatosFiltrosPropiedades').and.callThrough();
    // Espiamos buscarValidaciones
    const spyBuscarValidaciones = spyOn(component, 'buscarValidaciones').and.callThrough();
    // Llamamos nuestra funcion
    component.validarRegresoDetalleValidacion();
    // Verificamos que llamen nuestros espias
    expect(spyRecuperarDatosFiltroPropiedades).toHaveBeenCalled();
    expect(spyBuscarValidaciones).toHaveBeenCalled();
    // Verificamos los valores de isClicPDF y isClicExcel
    expect(component.isClicPDF).toBeFalsy();
    expect(component.isClicExcel).toBeFalsy();
    // Verificamos valor que se seta a regresarDetalleVal
    expect(localStorage.getItem('regresarDetalleVal')).toEqual('false');



    // Ahora verificamos el caso contrario para ello le asignamos FALSE a regresarDetalleVal , recodar que es una cadena
    localStorage.clear();
    localStorage.setItem('regresarDetalleVal', 'false');
    // Llamamos nuestra funcion
    component.validarRegresoDetalleValidacion();
    // Ahora verificamos los valores
    expect(localStorage.getItem('campoFechaDesde')).toEqual('false');
    expect(localStorage.getItem('campoFechaHasta')).toEqual('false');
    expect(localStorage.getItem('campoCantidad')).toEqual('true');
    expect(localStorage.getItem('ischeckCantidad')).toEqual('true');
    expect(localStorage.getItem('ischeckFechas')).toEqual('false');
    expect(localStorage.getItem('regresarDetalleVal')).toEqual('false');
  });


  it('Verificamos funcion guardarPropiedades()', () => {
    // Seteamos datos para probar la funcion
    const p = 2;
    const sizePagination = '10';
    component.p = p;
    component.sizePagination = sizePagination;
    // Llamamos nuestra funcion
    component.guardarPropiedades();
    // Verificamos los valores asignados al LocalStorage
    expect(localStorage.getItem('numeroPagina')).toEqual(p.toString());
    expect(localStorage.getItem('tamanioPagina')).toEqual(sizePagination);
  });

  it('Verificamos la funcion recuperarDatosFiltrosPropiedades()', async() => {
    // Seteamos valores en los Constants
    const FDesde = '"2014-05-15';
    const FHasta = '"2021-03-25"';
    const Usuario = 'jburgos6';
    const Cantidad = '';
    Constant.FILTRO_FDESDE = FDesde;
    Constant.FILTRO_FHASTA = FHasta;
    Constant.FILTRO_USUARIO = Usuario;
    Constant.FILTRO_CANTIDAD = Cantidad;
    // Seteamos valores en el localStorage
    const tamanioPagina = '10';
    const numeroPagina = '2';
    localStorage.clear();
    localStorage.setItem('tamanioPagina', '10');
    localStorage.setItem('numeroPagina', '2');
    // Fakeamos las funcion que usen datePipe para evitar errores
    spyOn(component, 'setDataFechaBusqueda').and.callFake(() => {'Nada'; });
    spyOn(component, 'setDataFechaBusquedaExportar').and.callFake(() => {'Nada'; });
    // Llamamos nuestra funcion
    component.recuperarDatosFiltrosPropiedades();
    // Ahora verificamos los valores seteamos
    expect(component.sizePagination).toEqual(tamanioPagina);
    expect(component.p).toEqual(2);
    // Ahora verificamos las asignaciones a nuestro Formulario
    expect(component.consultarValidacionesForm.getRawValue().filterfechaDesde).toEqual(FDesde);
    expect(component.consultarValidacionesForm.getRawValue().filterfechaHasta).toEqual(FHasta);
    expect(component.consultarValidacionesForm.getRawValue().filterUsuario).toEqual(Usuario);
    expect(component.consultarValidacionesForm.getRawValue().cantidad).toEqual(Cantidad);
  });

  it('Verificamos la funcion exportar(extension:string)', () => {
    // Espiamos nuestra funcion setdataRequestReporte
    const spySetDataRequestReporte = spyOn(component, 'setdataRequestReporte').and.callThrough();
    // Espiamos nuestro servicio serviceGenerarReporteValidaciones
    const spyService = spyOn(servicio, 'serviceGenerarReporteValidaciones').and.callThrough();
    // Llamamos nuestra funcion , le pasamos una extension
    component.exportar('PDF');
    // Verificamos que nuestro servicio fuera llamado
    expect(spyService).toHaveBeenCalled();
    // Verificamos que nuestra funcion setdataRequestReporte fuera llamada
    expect(spySetDataRequestReporte).toHaveBeenCalled();

    // Ahora verificamos las condiciones de respuesta del servicio
    // Mockeamos un DataResponse
    const mockDataResponse = 'Esto es una respuesta';
    // Fakeamos nuestro servicio para que nos devuelva el mockDataResponse
     const spyServiceFake = spyOn(servicio, 'serviceGenerarReporteValidaciones').and.callFake(() => {
       return of(mockDataResponse);
     });
    //  Espiamos la funcion SaveAs
    const spySaveAs = spyOn(FileSaver, 'saveAs').and.stub();
    // Llamamos nuestra funcion , le pasamos una extension
    component.exportar('PDF');
    // Verificamos que llame a SaveAs
    expect(spySaveAs).toHaveBeenCalled();

    // Ahora verificamos el caso de ERROR del servicio
    const spyServiceError = spyOn(servicio, 'serviceGenerarReporteValidaciones').and.returnValue(throwError('Error de serviceGenerarReporteValidaciones'));
    // Fakeamos nuestro Toast , caso contrario nos dara error por la libreria translate
    const spyToastFake = spyOn(component.toast, 'addToast').and.callFake(() => {'Nada'; });
    // Llamamos nuestra funcion , le pasamos una extension
    component.exportar('PDF');
    // Verificamos que llame a nuestro servicio ERROR
    expect(spyServiceError).toHaveBeenCalled();
    // Verificamos que llame a nuestro Toast FAKE
    expect(spyToastFake).toHaveBeenCalled();
  });

  it('Verificamos la funcion mensajeEspera', () => {
    // Fakeamos nuestro Toast , caso contrario nos dara error por la libreria translate
    const spyToastFake = spyOn(component.toast, 'addToast').and.callFake(() => {'Nada'; });
    // Llamamos nuestra funcion , le pasamos una extension
    component.mensajeEspera();
    // Verificamos que llame a nuestro Toast FAKE
    expect(spyToastFake).toHaveBeenCalled();
  });

  it('Verificamos nuestra funcion buscarValidacionesExportar()', () => {
    // Mockeamos data para nuestro formulario consultarValidacionesForm
    const filterUsuario = 'jburgos6';
    const cantidad = '123';
    const vacio = '';
    component.consultarValidacionesForm.controls.filterUsuario.setValue(filterUsuario);
    component.consultarValidacionesForm.controls.cantidad.setValue(cantidad);
    component.consultarValidacionesForm.controls.filterfechaDesde.setValue(vacio);
    component.consultarValidacionesForm.controls.filterfechaHasta.setValue(vacio);
    // Espiamos nuestra funcion setDataFechaBusqueda
    const spysetDataFechaBusqueda = spyOn(component, 'setDataFechaBusqueda').and.callThrough();
    // Espiamos nuestra funcion mensajeEspera
    const spymensajeEspera = spyOn(component, 'mensajeEspera').and.callThrough();
    // Espiamos nuestro servicio
    const spyService = spyOn(servicio, 'serviceConsultarValidaciones').and.callThrough();
    // Llamamos nuestra funcion , le pasamos parametros
    component.buscarValidacionesExportar('', '', 'reporteValidaciones' + '.xls');
    // Verificamos los valores de listaValidacionesExportar debe asignarse [];
    expect(component.listaValidacionesExportar).toEqual([]);
    // Verificamos que la data asignada del Formulario sea la misma que se asigne al objeto dataRequest
    expect(component.dataRequest.usuario).toEqual(filterUsuario);
    expect(component.dataRequest.cantidad).toEqual(cantidad);
    expect(component.dataRequest.tamanioPagina).toEqual('');
    expect(component.dataRequest.pagina).toEqual('');
    // Verificamos que se llame la funcion setDataFechaBusqueda que espiamos
    expect(spysetDataFechaBusqueda).toHaveBeenCalled();
    // Verificamos que se llame la funcion mensajeEspera que espiamos
    expect(spymensajeEspera).toHaveBeenCalled();
    // Verificamos que nuestro servicio fuera llamado
    expect(spyService).toHaveBeenCalled();

    // Ahora verificamos las respuesta para el caso del servico
    // Mockearemos un DataResponse
    // Se probara el primer caso donde codigoRespuesta sea Constant.COD_OPERACION_SATISFACTORIA_CONSULTA_VALIDACIONES_SIXBIO
    const mockDataResponse = {
      'numeroOrden': '7GKQJLAHHCVF6RD6',
      'horaTransaccion': '045210020',
      'institucionSolicitanteServicio': '000000',
      'horaProceso': '045055633',
      'codigoIdentificacionDispositivo': '01',
      'sistemaOperativoEstacion': 'WINDOWS',
      'versionAplicativo': '03.00.00',
      'latitudLocalizacion': null,
      'numeroIdentificadorCliente': '42728713',
      'modeloDispositivo': 'ZF1',
      'tipoIdentificacionDispositivo': '01',
      'reservado': 'R',
      'codigoSubGrupo': null,
      'tipoDispositivo': '01',
      'fechaTransaccion': '20210330',
      'numeroProceso': '108980704229522',
      'marcaDispositivo': 'DERMALOG',
      'codigoGrupo': null,
      'tipoEstacion': '00',
      'mensajeRespuesta': 'OPERACIÓN REALIZADA SATISFACTORIAMENTE',
      'institucionCanalizaServicio': '000000',
      'puntoVentaSolicitanteServicio': '00',
      'fechaProceso': '20210330',
      'origenRespuesta': '02',
      'numeroTransaccion': '7252209434616016',
      'institucionProveedorServicio': '300000',
      'codigoProcesadorAdquirente': null,
      'codigoTransaccion': '500000',
      'validacionesBiometricas': [
          {
              'apellidoMaterno': 'CARRASCO',
              'apellidoPaterno': 'BAJALQUI',
              'codigoRespuesta': null,
              'codigoTransaccion': '301000',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '03/03/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '8740580460025774',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': null,
              'nombres': 'EDGAR GUSTAVO',
              'numeroDocumento': '88888888',
              'origenRespuesta': 'INVALIDO',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'DNI',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'CARRASCO',
              'apellidoPaterno': 'BAJALQUI',
              'codigoRespuesta': null,
              'codigoTransaccion': '301000',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '03/03/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '1435356861058524',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': null,
              'nombres': 'EDGAR GUSTAVO',
              'numeroDocumento': '88888888',
              'origenRespuesta': 'INVALIDO',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'DNI',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'CARRASCO',
              'apellidoPaterno': 'BAJALQUI',
              'codigoRespuesta': null,
              'codigoTransaccion': '301000',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '03/03/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '0631463202555101',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': null,
              'nombres': 'EDGAR GUSTAVO',
              'numeroDocumento': '88888888',
              'origenRespuesta': 'INVALIDO',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'DNI',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'CARRASCO',
              'apellidoPaterno': 'BAJALQUI',
              'codigoRespuesta': null,
              'codigoTransaccion': '301000',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '03/03/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '0441148302662070',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': null,
              'nombres': 'EDGAR GUSTAVO',
              'numeroDocumento': '88888888',
              'origenRespuesta': 'INVALIDO',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'DNI',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'CARRASCO',
              'apellidoPaterno': 'BAJALQUI',
              'codigoRespuesta': null,
              'codigoTransaccion': '301000',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '13/02/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '6304808127482730',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': null,
              'nombres': 'EDGAR GUSTAVO',
              'numeroDocumento': '88888888',
              'origenRespuesta': 'INVALIDO',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'DNI',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'CARRASCO',
              'apellidoPaterno': 'BAJALQUI',
              'codigoRespuesta': null,
              'codigoTransaccion': '301000',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '10/02/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '0824278678231766',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': null,
              'nombres': 'EDGAR GUSTAVO',
              'numeroDocumento': '88888888',
              'origenRespuesta': 'INVALIDO',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'DNI',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'MAMANI',
              'apellidoPaterno': 'SMITH',
              'codigoRespuesta': null,
              'codigoTransaccion': '310000',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '10/02/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '8856425848271707',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': null,
              'nombres': 'GERRY CHERRY TERRY',
              'numeroDocumento': '7777777777777',
              'origenRespuesta': 'INVALIDO',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'PASAPORTE',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'MAMANI',
              'apellidoPaterno': 'SMITH',
              'codigoRespuesta': '00000',
              'codigoTransaccion': '310000',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '10/02/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '4523838724174847',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': 'SI CORRESPONDE',
              'nombres': 'GERRY CHERRY TERRY',
              'numeroDocumento': '7777777777777',
              'origenRespuesta': 'NOVATRONIC',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'PASAPORTE',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'MAMANI',
              'apellidoPaterno': 'SMITH',
              'codigoRespuesta': '00000',
              'codigoTransaccion': '310000',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '10/02/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '1850878025236177',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': 'SI CORRESPONDE',
              'nombres': 'GERRY CHERRY TERRY',
              'numeroDocumento': '7777777777777',
              'origenRespuesta': 'NOVATRONIC',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'PASAPORTE',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'CARRASCO',
              'apellidoPaterno': 'BAJALQUI',
              'codigoRespuesta': '00000',
              'codigoTransaccion': '301001',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '10/02/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '1973226176994878',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': 'SI CORRESPONDE',
              'nombres': 'EDGAR GUSTAVO',
              'numeroDocumento': '88888888',
              'origenRespuesta': 'NOVATRONIC',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'DNI',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'CARRASCO',
              'apellidoPaterno': 'BAJALQUI',
              'codigoRespuesta': '10000',
              'codigoTransaccion': '301001',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '10/02/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '2318016121671603',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': 'NO CORRESPONDE',
              'nombres': 'EDGAR GUSTAVO',
              'numeroDocumento': '88888888',
              'origenRespuesta': 'NOVATRONIC',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'DNI',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'CARRASCO',
              'apellidoPaterno': 'BAJALQUI',
              'codigoRespuesta': '00000',
              'codigoTransaccion': '301001',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '10/02/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '2453856751465534',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': 'SI CORRESPONDE',
              'nombres': 'EDGAR GUSTAVO',
              'numeroDocumento': '88888888',
              'origenRespuesta': 'NOVATRONIC',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'DNI',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'MAMANI',
              'apellidoPaterno': 'SMITH',
              'codigoRespuesta': '00000',
              'codigoTransaccion': '310000',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '10/02/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '3158671148180673',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': 'SI CORRESPONDE',
              'nombres': 'GERRY CHERRY TERRY',
              'numeroDocumento': '7777777777777',
              'origenRespuesta': 'NOVATRONIC',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'PASAPORTE',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          }
      ],
      'autenticacionMensaje': '1216386664685317',
      'tipoFormato': '0002',
      'paginas': '1',
      'codigoAdquirente': null,
      'idSesion': '000000202103301640430160022',
      'tipoIdentificadorCliente': '01',
      'longitudLocalizacion': null,
      'codigoAutorizador': '189513252949914',
      'codigoSistemaLocalizacion': null,
      'tipoMensaje': '0210',
      'tipoRespuesta': '00',
      'modeloEstacion': 'NOVA-MRIOS',
      'tipoIdentificacionEstacion': '01',
      'codigoAplicativo': '00',
      'codigoRespuesta': '00000',
      'agenteSolicitanteServicio': '0',
      'codigoIdentificacionEstacion': '00'
    };
    // Fakeamos la respuesta de nuestro servicio para que nos retorne el mockDataResponse
    const spyServiceFake = spyOn(servicio, 'serviceConsultarValidaciones').and.callFake(() => {
      return of(mockDataResponse);
    });
    // Espiamos la funcion setCamposGrillaExportar
    const spysetCampoGrillaExportar = spyOn(component, 'setCamposGrillaExportar').and.callThrough();
    // Espiamos la funcion exportar
    const spyexportar = spyOn(component, 'exportar').and.callThrough();
    // Llamamos nuestra funcion , le pasamos parametros
    component.buscarValidacionesExportar('', '', 'reporteValidaciones' + '.xls');
    // Verificamos que que llame la funcion setCamposGrillaExportar
    expect(spysetCampoGrillaExportar).toHaveBeenCalled();
    // Verificamos que llame la funcion spyexportar
    expect(spyexportar).toHaveBeenCalled();
    // Verificamos que llame nuestro servicio Fake
    expect(spyServiceFake).toHaveBeenCalled();
    //  Verificamos los valores de isClicPDF y isClicExcel
    expect(component.isClicPDF).toBeFalsy();
    expect(component.isClicExcel).toBeFalsy();
    // Verificamos que los valores asignado a listaValidacionesExportar sean las del mockDataResponse.validacionesBiometricas
    expect(component.listaValidacionesExportar).toEqual(mockDataResponse.validacionesBiometricas);

/*
                        this.listaValidacionesExportar = result.validacionesBiometricas;
                        this.setCamposGrillaExportar();
                        this.exportar(extension);
                        this.isClicPDF = false;
                        this.isClicExcel = false;

*/
  });



});
