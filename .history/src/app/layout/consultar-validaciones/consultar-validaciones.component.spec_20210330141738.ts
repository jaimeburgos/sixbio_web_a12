import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { EnvService } from 'src/app/env.service';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { ToastComponent } from '../bs-component/components/toast/toast.component';
import { ConsultarValidacionesComponent } from './consultar-validaciones.component';

fdescribe('ConsultarValidacionesComponent', () => {
  let component: ConsultarValidacionesComponent;
  let fixture: ComponentFixture<ConsultarValidacionesComponent>;
  let translateService: TranslateService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultarValidacionesComponent, ToastComponent ],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService ,
        {provide: ToastrService, useClass: ToastrService}],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultarValidacionesComponent);
    translateService = TestBed.inject(TranslateService);
    component = fixture.componentInstance;
    fixture.detectChanges();
    // Mockeamos valor en localstorage de usuarioLogueado
    localStorage.setItem('usuarioLogueado', 'jburgos6');
  });

  it('consultar validaciones component creado correctamente', () => {
    expect(component).toBeTruthy();
  });


  // Validar restricciones del consultarValidacionesForm
  it('filterUsuario es obligatorio en consultarValidacionesForm', () => {
    // Obtenemos en una constante el ControlForm que deseamos evaluar
    const filterUsuario = component.consultarValidacionesForm.get('filterUsuario');
    // Le asignamos un valor
    filterUsuario.setValue('');
    // La expectativa de la prueba de ser falso ya que no permite vacio
    expect(filterUsuario.valid).toBeFalsy();
  });


  it('filterfechaHasta es obligatorio en consultarValidacionesForm', () => {
    const filterfechaHasta = component.consultarValidacionesForm.get('filterfechaHasta');
    filterfechaHasta.setValue('');
    expect(filterfechaHasta.valid).toBeFalsy();
    filterfechaHasta.setValue(null);
    expect(filterfechaHasta.valid).toBeFalsy();
  });

  it('filterfechaDesde es obligatorio en consultarValidacionesForm', () => {
    const filterfechaDesde = component.consultarValidacionesForm.get('filterfechaDesde');
    filterfechaDesde.setValue('');
    expect(filterfechaDesde.valid).toBeFalsy();
    filterfechaDesde.setValue(null);
    expect(filterfechaDesde.valid).toBeFalsy();
  });

  it('cantidad es obligatorio en consultarValidacionesForm', () => {
    const cantidad = component.consultarValidacionesForm.get('cantidad');
    cantidad.setValue('');
    expect(cantidad.valid).toBeFalsy();
    cantidad.setValue(null);
    expect(cantidad.valid).toBeFalsy();
  });


  it('Campo "cantidad" puede tener maximo 4 caracteres consultarValidacionesForm', () => {
    const cantidad = component.consultarValidacionesForm.get('cantidad');
    cantidad.setValue('1234');
    expect(cantidad.valid).toBeTruthy();
    cantidad.setValue('12345');
    expect(cantidad.valid).toBeFalsy();
  });


  it('Campo "cantidad" verificamos que solo acepte valores numericos', () => {
    const cantidad = component.consultarValidacionesForm.get('cantidad');
    cantidad.setValue('1234');
    expect(cantidad.valid).toBeTruthy();
    cantidad.setValue('abc');
    expect(cantidad.valid).toBeFalsy();
    cantidad.setValue('""');
    expect(cantidad.valid).toBeFalsy();
    cantidad.setValue('++--');
    expect(cantidad.valid).toBeFalsy();
    cantidad.setValue('    ');
    expect(cantidad.valid).toBeFalsy();
  });


  it('Se carga usuarioLogeado desde localStorage en filterUsuario del FormControl', () => {
   const filterUsuario = component.consultarValidacionesForm.getRawValue().filterUsuario;
   // verificamos que el filterUsuario sea igual al valor recuperado del localStorage
    expect(filterUsuario).toBe(localStorage.getItem('usuarioLogueado'));
    // verificamos que mantenga la misma longitud
    expect(filterUsuario.length).toBe(8);
    // verificamos que el usuario cumpla los requisitos de ser mayor o igual 5 caracteres
    expect(filterUsuario.length).toBeGreaterThanOrEqual(5);
  });


  it('Verificamos funcion onChangeForm()', () => {
    // Para ello seteamos un valor para probar la validacion
    component.listaValidaciones.length = 10;
    // Llamamos nuestra funcion
    component.onChangeForm();
    // Verificamos los valores
    expect(component.listaValidaciones).toEqual([]);
    expect(component.listaValidacionesExportar ).toEqual([]);
    expect(component.listarValidacionesOK ).toBeFalsy();

  });


  it('Verificamos funcion onChangeCantidad()', () => {
    // Espiamos la funcion isExcelDisabled();
    const spyIsExcel = spyOn(component, 'isExcelDisabled').and.callThrough();
    // Espiamos la funcion isPDFDisabled
    const spyIsPDF = spyOn(component, 'isPDFDisabled').and.callThrough();
    // Seteamos un valor para probar la condicion de la funcion onChangeCantidad
    component.listaValidaciones.length = 10;
    // Llamamos nuestra funcion
    component.onChangeCantidad();
    // Verificamos los valores
    expect(component.listaValidaciones).toEqual([]);
    expect(component.listaValidacionesExportar ).toEqual([]);
    expect(component.listarValidacionesOK ).toBeFalsy();
  });

  it('Verificamos funcion onChangeFecha()', () => {
    // Espiamos la funcion isExcelDisabled();
    const spyIsExcel = spyOn(component, 'isExcelDisabled');
    // Espiamos la funcion isPDFDisabled
    const spyIsPDF = spyOn(component, 'isPDFDisabled').and.callThrough();
    // Seteamos un valor para probar la condicion de la funcion onChangeCantidad
    component.listaValidaciones.length = 10;
    // Llamamos nuestra funcion
    component.onChangeFecha();
    // Verificamos los valores
    expect(component.listaValidaciones).toEqual([]);
    expect(component.listaValidacionesExportar ).toEqual([]);
    expect(component.listarValidacionesOK ).toBeFalsy();
    // Verificamos que llame nuestros espias
    expect(spyIsPDF).toHaveBeenCalled();
    expect(spyIsExcel).toHaveBeenCalled();
  });

  it('Verificamos la funcion isExcelDisabled()', () => {
    // Seteamos una propiedad que genera invalid en el Formulario Reactivo
    component.consultarValidacionesForm.controls.cantidad.setValue('');
    // Llamamos nuestra funcion
    component.isExcelDisabled();
    // Verificamos valores
    expect(component.isClicExcel ).toBeTruthy();
    // Verificamos cuando formulario es valid , para ello seteamos valores validos
    component.consultarValidacionesForm.controls.cantidad.setValue('111');
    component.consultarValidacionesForm.controls.filterUsuario.setValue('abenito');
    // Llamamos nuestra funcion
    component.isExcelDisabled();
    // Verificamos valores
    expect(component.isClicExcel ).toBeFalsy();

  });




});
