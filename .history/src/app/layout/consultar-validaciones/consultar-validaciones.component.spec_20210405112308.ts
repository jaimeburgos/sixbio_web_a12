import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import * as FileSaver from 'file-saver';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { async, of, Subject, throwError } from 'rxjs';
import { EnvService } from 'src/app/env.service';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { ToastComponent } from '../bs-component/components/toast/toast.component';
import { ConsultarValidacionesComponent } from './consultar-validaciones.component';

class FakeRouter {
  navigate(params) {

  }
}

class FakeActivatedRoute {
  // params: Observable<any> = Observable.empty();
  private subject = new Subject();

  push( valor ) {
    this.subject.next( valor );
  }
  get params() {
    return this.subject.asObservable();
  }
}

fdescribe('ConsultarValidacionesComponent', () => {
  let component: ConsultarValidacionesComponent;
  let fixture: ComponentFixture<ConsultarValidacionesComponent>;
  // Instanciamos los servicios que utilizaremos
  let translateService: TranslateService;
  let servicio: ServicioService;
  beforeEach((() => {
    TestBed.configureTestingModule({
      declarations: [ ConsultarValidacionesComponent, ToastComponent ],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService ,
        {provide: Router , useClass: FakeRouter},
        {provide: ToastrService, useClass: ToastrService},
        { provide: ActivatedRoute, useClass: FakeActivatedRoute }
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultarValidacionesComponent);
    translateService = TestBed.inject(TranslateService);
    // Injectamos nuestro servicio con TestBed.inject
    servicio = TestBed.inject(ServicioService);
    component = fixture.componentInstance;
    fixture.detectChanges();
    // Mockeamos valor en localstorage de usuarioLogueado
    localStorage.setItem('usuarioLogueado', 'jburgos6');
  });

  it('consultar validaciones component creado correctamente', () => {
    expect(component).toBeTruthy();
  });


  // Validar restricciones del consultarValidacionesForm
  it('filterUsuario es obligatorio en consultarValidacionesForm', () => {
    // Obtenemos en una constante el ControlForm que deseamos evaluar
    const filterUsuario = component.consultarValidacionesForm.get('filterUsuario');
    // Le asignamos un valor
    filterUsuario.setValue('');
    // La expectativa de la prueba de ser falso ya que no permite vacio
    expect(filterUsuario.valid).toBeFalsy();
  });


  it('filterfechaHasta es obligatorio en consultarValidacionesForm', () => {
    const filterfechaHasta = component.consultarValidacionesForm.get('filterfechaHasta');
    filterfechaHasta.setValue('');
    expect(filterfechaHasta.valid).toBeFalsy();
    filterfechaHasta.setValue(null);
    expect(filterfechaHasta.valid).toBeFalsy();
  });

  it('filterfechaDesde es obligatorio en consultarValidacionesForm', () => {
    const filterfechaDesde = component.consultarValidacionesForm.get('filterfechaDesde');
    filterfechaDesde.setValue('');
    expect(filterfechaDesde.valid).toBeFalsy();
    filterfechaDesde.setValue(null);
    expect(filterfechaDesde.valid).toBeFalsy();
  });

  it('cantidad es obligatorio en consultarValidacionesForm', () => {
    const cantidad = component.consultarValidacionesForm.get('cantidad');
    cantidad.setValue('');
    expect(cantidad.valid).toBeFalsy();
    cantidad.setValue(null);
    expect(cantidad.valid).toBeFalsy();
  });


  it('Campo "cantidad" puede tener maximo 4 caracteres consultarValidacionesForm', () => {
    const cantidad = component.consultarValidacionesForm.get('cantidad');
    cantidad.setValue('1234');
    expect(cantidad.valid).toBeTruthy();
    cantidad.setValue('12345');
    expect(cantidad.valid).toBeFalsy();
  });


  it('Campo "cantidad" verificamos que solo acepte valores numericos', () => {
    const cantidad = component.consultarValidacionesForm.get('cantidad');
    cantidad.setValue('1234');
    expect(cantidad.valid).toBeTruthy();
    cantidad.setValue('abc');
    expect(cantidad.valid).toBeFalsy();
    cantidad.setValue('""');
    expect(cantidad.valid).toBeFalsy();
    cantidad.setValue('++--');
    expect(cantidad.valid).toBeFalsy();
    cantidad.setValue('    ');
    expect(cantidad.valid).toBeFalsy();
  });


  it('Se carga usuarioLogeado desde localStorage en filterUsuario del FormControl', () => {
    component.ngOnInit();
   const filterUsuario = component.consultarValidacionesForm.getRawValue().filterUsuario;
   // verificamos que el filterUsuario sea igual al valor recuperado del localStorage
    expect(filterUsuario).toBe(localStorage.getItem('usuarioLogueado'));
    // verificamos que mantenga la misma longitud
    expect(filterUsuario.length).toBe(8);
    // verificamos que el usuario cumpla los requisitos de ser mayor o igual 5 caracteres
    expect(filterUsuario.length).toBeGreaterThanOrEqual(5);
  });


  it('Verificamos la funcion ngOnInit()', () => {
    // Espiamos la funcion recuperarConsultaValidaciones
    const spyrecuperarConsultaValidaciones = spyOn(component, 'recuperarConsultaValidaciones').and.callThrough();
    const spylistarUsuarios = spyOn(component, 'listarUsuarios').and.callThrough();
    const spyfilterRadioButtonCantidad = spyOn(component, 'filterRadioButtonCantidad').and.callThrough();
    const spyfilterRadioButtonFechas = spyOn(component, 'filterRadioButtonFechas').and.callThrough();
    // Llamamos nuestra funcion
    component.ngOnInit();
    // Verficamos los valores esperados
    expect(component.listaValidaciones ).toEqual([]);
    expect(component.sizePagination  ).toEqual('10');
    expect(component.p  ).toEqual(1);
    expect(component.listaUsuariosSixbio  ).toEqual([]);
    expect(component.listaUsuarios  ).toEqual([]);
    // Verificamos que llame nuestros espias
    expect(spyrecuperarConsultaValidaciones).toHaveBeenCalled();
    expect(spylistarUsuarios).toHaveBeenCalled();
    expect(spyfilterRadioButtonCantidad).toHaveBeenCalled();
    expect(spyfilterRadioButtonFechas).toHaveBeenCalled();
    // Verificamos el valor asignado a filterUsuario , que es el usuario logeado que se Mockeamos
    const filterUsuario = component.consultarValidacionesForm.getRawValue().filterUsuario;
    expect(filterUsuario).toEqual('jburgos6');
  });

  it('Verificamos funcion onChangeForm()', () => {
    // Para ello seteamos un valor para probar la validacion
    component.listaValidaciones.length = 10;
    // Llamamos nuestra funcion
    component.onChangeForm();
    // Verificamos los valores
    expect(component.listaValidaciones).toEqual([]);
    expect(component.listaValidacionesExportar ).toEqual([]);
    expect(component.listarValidacionesOK ).toBeFalsy();
  });


  it('Verificamos funcion onChangeCantidad()', () => {
    // Espiamos la funcion isExcelDisabled();
    const spyIsExcel = spyOn(component, 'isExcelDisabled').and.callThrough();
    // Espiamos la funcion isPDFDisabled
    const spyIsPDF = spyOn(component, 'isPDFDisabled').and.callThrough();
    // Seteamos un valor para probar la condicion de la funcion onChangeCantidad
    component.listaValidaciones.length = 10;
    // Llamamos nuestra funcion
    component.onChangeCantidad();
    // Verificamos los valores
    expect(component.listaValidaciones).toEqual([]);
    expect(component.listaValidacionesExportar ).toEqual([]);
    expect(component.listarValidacionesOK ).toBeFalsy();
    // Verificamos que llame nuestros espias
    expect(spyIsPDF).toHaveBeenCalled();
    expect(spyIsExcel).toHaveBeenCalled();
  });

  it('Verificamos funcion onChangeFecha()', () => {
    // Espiamos la funcion isExcelDisabled();
    const spyIsExcel = spyOn(component, 'isExcelDisabled').and.callThrough();
    // Espiamos la funcion isPDFDisabled
    const spyIsPDF = spyOn(component, 'isPDFDisabled').and.callThrough();
    // Seteamos un valor para probar la condicion de la funcion onChangeCantidad
    component.listaValidaciones.length = 10;
    // Llamamos nuestra funcion
    component.onChangeFecha();
    // Verificamos los valores
    expect(component.listaValidaciones).toEqual([]);
    expect(component.listaValidacionesExportar ).toEqual([]);
    expect(component.listarValidacionesOK ).toBeFalsy();
    // Verificamos que llame nuestros espias
    expect(spyIsPDF).toHaveBeenCalled();
    expect(spyIsExcel).toHaveBeenCalled();
  });

  it('Verificamos la funcion isExcelDisabled()', () => {
    // Seteamos una propiedad que genera invalid en el Formulario Reactivo
    component.consultarValidacionesForm.controls.cantidad.setValue('');
    // Llamamos nuestra funcion
    component.isExcelDisabled();
    // Verificamos valores
    expect(component.isClicExcel ).toBeTruthy();
    // Verificamos cuando formulario es valid , para ello seteamos valores validos
    component.consultarValidacionesForm.controls.cantidad.setValue('111');
    component.consultarValidacionesForm.controls.filterUsuario.setValue('abenito');
    component.consultarValidacionesForm.controls.filterfechaDesde.setValue('01/01/2000');
    component.consultarValidacionesForm.controls.filterfechaHasta.setValue('25/02/2024');
    // Llamamos nuestra funcion
    component.isExcelDisabled();
    // Verificamos valores
    expect(component.isClicExcel ).toBeFalsy();
  });


  it('Verificamos la funcion isPDFDisabled()', () => {
    // Seteamos una propiedad que genera invalid en el Formulario Reactivo
    component.consultarValidacionesForm.controls.cantidad.setValue('');
    // Llamamos nuestra funcion
    component.isPDFDisabled();
    // Verificamos valores
    expect(component.isClicPDF ).toBeTruthy();
    // Verificamos cuando formulario es valid , para ello seteamos valores validos
    component.consultarValidacionesForm.controls.cantidad.setValue('111');
    component.consultarValidacionesForm.controls.filterUsuario.setValue('abenito');
    component.consultarValidacionesForm.controls.filterfechaDesde.setValue('01/01/2000');
    component.consultarValidacionesForm.controls.filterfechaHasta.setValue('25/02/2024');
    // Llamamos nuestra funcion
    component.isPDFDisabled();
    // Verificamos valores
    expect(component.isClicPDF ).toBeFalsy();
  });

  it('Verificamos la funcion exportarExcel', () => {
    // Espiamos nuestro metodo setTipoReporte
    const spySetTipoReporte = spyOn(component, 'setTipoReporte').and.callThrough();
    // Espiamos nuestro metodo setTipoReporte
    const spybuscarValidacionesExportar = spyOn(component, 'buscarValidacionesExportar').and.callThrough();
    // Espiamos nuestro metodo setFechaActual
    const spysetFechaActual = spyOn(component, 'setFechaActual').and.callThrough();
    // Llamamos nuestra funcion
    component.exportarExcel();
    // Verificamos que llame nuestros espias
    expect(spybuscarValidacionesExportar).toHaveBeenCalled();
    expect(spysetFechaActual).toHaveBeenCalled();
    expect(spySetTipoReporte).toHaveBeenCalled();
    // Verificamos los valores de isClicPDF isClicExcel
    expect(component.isClicExcel).toBeTruthy();
    expect(component.isClicPDF).toBeTruthy();
  });

  it('Verificamos la funcion exportarPDF', () => {
    // Espiamos nuestro metodo setTipoReporte
    const spySetTipoReporte = spyOn(component, 'setTipoReporte').and.callThrough();
    // Espiamos nuestro metodo setTipoReporte
    const spybuscarValidacionesExportar = spyOn(component, 'buscarValidacionesExportar').and.callThrough();
    // Espiamos nuestro metodo setFechaActual
    const spysetFechaActual = spyOn(component, 'setFechaActual').and.callThrough();
    // Llamamos nuestra funcion
    component.exportarPDF();
    // Verificamos que llame nuestros espias
    expect(spybuscarValidacionesExportar).toHaveBeenCalled();
    expect(spysetFechaActual).toHaveBeenCalled();
    expect(spySetTipoReporte).toHaveBeenCalled();
    // Verificamos los valores de isClicPDF isClicExcel
    expect(component.isClicExcel).toBeTruthy();
    expect(component.isClicPDF).toBeTruthy();
  });

  it('Verificamos la funcion recuperarConsultaValidaciones()', () => {
    // Espiamos la funcion validarRegresoDetalleValidacion
    const spy = spyOn(component, 'validarRegresoDetalleValidacion').and.callThrough();
    // Llamamos nuestra funcion
    component.recuperarConsultaValidaciones();
    // Verificamos que llame nuestro espia
    expect(spy).toHaveBeenCalled();
  });


  it('Verificamos la funcion validarRegresoDetalleValidacion()', () => {
    // Probamos la primera condicion cuando regresarDetalleVal del LocalStorage sea TRUE, recodar que es una cadena
    localStorage.setItem('regresarDetalleVal', 'true');
    // Espiamos nuestra funcion recuperarDatosFiltrosPropiedades
    const spyRecuperarDatosFiltroPropiedades = spyOn(component, 'recuperarDatosFiltrosPropiedades').and.callThrough();
    // Espiamos buscarValidaciones
    const spyBuscarValidaciones = spyOn(component, 'buscarValidaciones').and.callThrough();
    // Llamamos nuestra funcion
    component.validarRegresoDetalleValidacion();
    // Verificamos que llamen nuestros espias
    expect(spyRecuperarDatosFiltroPropiedades).toHaveBeenCalled();
    expect(spyBuscarValidaciones).toHaveBeenCalled();
    // Verificamos los valores de isClicPDF y isClicExcel
    expect(component.isClicPDF).toBeFalsy();
    expect(component.isClicExcel).toBeFalsy();
    // Verificamos valor que se seta a regresarDetalleVal
    expect(localStorage.getItem('regresarDetalleVal')).toEqual('false');



    // Ahora verificamos el caso contrario para ello le asignamos FALSE a regresarDetalleVal , recodar que es una cadena
    localStorage.clear();
    localStorage.setItem('regresarDetalleVal', 'false');
    // Llamamos nuestra funcion
    component.validarRegresoDetalleValidacion();
    // Ahora verificamos los valores
    expect(localStorage.getItem('campoFechaDesde')).toEqual('false');
    expect(localStorage.getItem('campoFechaHasta')).toEqual('false');
    expect(localStorage.getItem('campoCantidad')).toEqual('true');
    expect(localStorage.getItem('ischeckCantidad')).toEqual('true');
    expect(localStorage.getItem('ischeckFechas')).toEqual('false');
    expect(localStorage.getItem('regresarDetalleVal')).toEqual('false');
  });


  it('Verificamos funcion guardarPropiedades()', () => {
    // Seteamos datos para probar la funcion
    const p = 2;
    const sizePagination = '10';
    component.p = p;
    component.sizePagination = sizePagination;
    // Llamamos nuestra funcion
    component.guardarPropiedades();
    // Verificamos los valores asignados al LocalStorage
    expect(localStorage.getItem('numeroPagina')).toEqual(p.toString());
    expect(localStorage.getItem('tamanioPagina')).toEqual(sizePagination);
  });

  it('Verificamos la funcion recuperarDatosFiltrosPropiedades()', async() => {
    // Seteamos valores en los Constants
    const FDesde = '"2014-05-15';
    const FHasta = '"2021-03-25"';
    const Usuario = 'jburgos6';
    const Cantidad = '';
    Constant.FILTRO_FDESDE = FDesde;
    Constant.FILTRO_FHASTA = FHasta;
    Constant.FILTRO_USUARIO = Usuario;
    Constant.FILTRO_CANTIDAD = Cantidad;
    // Seteamos valores en el localStorage
    const tamanioPagina = '10';
    const numeroPagina = '2';
    localStorage.clear();
    localStorage.setItem('tamanioPagina', '10');
    localStorage.setItem('numeroPagina', '2');
    // Fakeamos las funcion que usen datePipe para evitar errores
    spyOn(component, 'setDataFechaBusqueda').and.callFake(() => {'Nada'; });
    spyOn(component, 'setDataFechaBusquedaExportar').and.callFake(() => {'Nada'; });
    // Llamamos nuestra funcion
    component.recuperarDatosFiltrosPropiedades();
    // Ahora verificamos los valores seteamos
    expect(component.sizePagination).toEqual(tamanioPagina);
    expect(component.p).toEqual(2);
    // Ahora verificamos las asignaciones a nuestro Formulario
    expect(component.consultarValidacionesForm.getRawValue().filterfechaDesde).toEqual(FDesde);
    expect(component.consultarValidacionesForm.getRawValue().filterfechaHasta).toEqual(FHasta);
    expect(component.consultarValidacionesForm.getRawValue().filterUsuario).toEqual(Usuario);
    expect(component.consultarValidacionesForm.getRawValue().cantidad).toEqual(Cantidad);
  });

  it('Verificamos la funcion exportar(extension:string)', () => {
    // Espiamos nuestra funcion setdataRequestReporte
    const spySetDataRequestReporte = spyOn(component, 'setdataRequestReporte').and.callThrough();
    // Espiamos nuestro servicio serviceGenerarReporteValidaciones
    const spyService = spyOn(servicio, 'serviceGenerarReporteValidaciones').and.callThrough();
    // Llamamos nuestra funcion , le pasamos una extension
    component.exportar('PDF');
    // Verificamos que nuestro servicio fuera llamado
    expect(spyService).toHaveBeenCalled();
    // Verificamos que nuestra funcion setdataRequestReporte fuera llamada
    expect(spySetDataRequestReporte).toHaveBeenCalled();

    // Ahora verificamos las condiciones de respuesta del servicio
    // Mockeamos un DataResponse
    const mockDataResponse = 'Esto es una respuesta';
    // Fakeamos nuestro servicio para que nos devuelva el mockDataResponse
     const spyServiceFake = spyOn(servicio, 'serviceGenerarReporteValidaciones').and.callFake(() => {
       return of(mockDataResponse);
     });
    //  Espiamos la funcion SaveAs
    const spySaveAs = spyOn(FileSaver, 'saveAs').and.stub();
    // Llamamos nuestra funcion , le pasamos una extension
    component.exportar('PDF');
    // Verificamos que llame a SaveAs
    expect(spySaveAs).toHaveBeenCalled();

    // Ahora verificamos el caso de ERROR del servicio
    const spyServiceError = spyOn(servicio, 'serviceGenerarReporteValidaciones').and.returnValue(throwError('Error de serviceGenerarReporteValidaciones'));
    // Fakeamos nuestro Toast , caso contrario nos dara error por la libreria translate
    const spyToastFake = spyOn(component.toast, 'addToast').and.callFake(() => {'Nada'; });
    // Llamamos nuestra funcion , le pasamos una extension
    component.exportar('PDF');
    // Verificamos que llame a nuestro servicio ERROR
    expect(spyServiceError).toHaveBeenCalled();
    // Verificamos que llame a nuestro Toast FAKE
    expect(spyToastFake).toHaveBeenCalled();
  });

  it('Verificamos la funcion mensajeEspera', () => {
    // Fakeamos nuestro Toast , caso contrario nos dara error por la libreria translate
    const spyToastFake = spyOn(component.toast, 'addToast').and.callFake(() => {'Nada'; });
    // Llamamos nuestra funcion , le pasamos una extension
    component.mensajeEspera();
    // Verificamos que llame a nuestro Toast FAKE
    expect(spyToastFake).toHaveBeenCalled();
  });

  it('Verificamos nuestra funcion buscarValidacionesExportar()', () => {
    // Mockeamos data para nuestro formulario consultarValidacionesForm
    const filterUsuario = 'jburgos6';
    const cantidad = '123';
    const vacio = '';
    component.consultarValidacionesForm.controls.filterUsuario.setValue(filterUsuario);
    component.consultarValidacionesForm.controls.cantidad.setValue(cantidad);
    component.consultarValidacionesForm.controls.filterfechaDesde.setValue(vacio);
    component.consultarValidacionesForm.controls.filterfechaHasta.setValue(vacio);
    // Espiamos nuestra funcion setDataFechaBusqueda
    const spysetDataFechaBusqueda = spyOn(component, 'setDataFechaBusqueda').and.callThrough();
    // Espiamos nuestra funcion mensajeEspera
    const spymensajeEspera = spyOn(component, 'mensajeEspera').and.callThrough();
    // Espiamos nuestro servicio
    const spyService = spyOn(servicio, 'serviceConsultarValidaciones').and.callThrough();
    // Llamamos nuestra funcion , le pasamos parametros
    component.buscarValidacionesExportar('', '', 'reporteValidaciones' + '.xls');
    // Verificamos los valores de listaValidacionesExportar debe asignarse [];
    expect(component.listaValidacionesExportar).toEqual([]);
    // Verificamos que la data asignada del Formulario sea la misma que se asigne al objeto dataRequest
    expect(component.dataRequest.usuario).toEqual(filterUsuario);
    expect(component.dataRequest.cantidad).toEqual(cantidad);
    expect(component.dataRequest.tamanioPagina).toEqual('');
    expect(component.dataRequest.pagina).toEqual('');
    // Verificamos que se llame la funcion setDataFechaBusqueda que espiamos
    expect(spysetDataFechaBusqueda).toHaveBeenCalled();
    // Verificamos que se llame la funcion mensajeEspera que espiamos
    expect(spymensajeEspera).toHaveBeenCalled();
    // Verificamos que nuestro servicio fuera llamado
    expect(spyService).toHaveBeenCalled();

    // Ahora verificamos las respuesta para el caso del servico
    // Mockearemos un DataResponse
    // Se probara el primer caso donde codigoRespuesta sea Constant.COD_OPERACION_SATISFACTORIA_CONSULTA_VALIDACIONES_SIXBIO
    const mockDataResponse = {
      'numeroOrden': '7GKQJLAHHCVF6RD6',
      'horaTransaccion': '045210020',
      'institucionSolicitanteServicio': '000000',
      'horaProceso': '045055633',
      'codigoIdentificacionDispositivo': '01',
      'sistemaOperativoEstacion': 'WINDOWS',
      'versionAplicativo': '03.00.00',
      'latitudLocalizacion': null,
      'numeroIdentificadorCliente': '42728713',
      'modeloDispositivo': 'ZF1',
      'tipoIdentificacionDispositivo': '01',
      'reservado': 'R',
      'codigoSubGrupo': null,
      'tipoDispositivo': '01',
      'fechaTransaccion': '20210330',
      'numeroProceso': '108980704229522',
      'marcaDispositivo': 'DERMALOG',
      'codigoGrupo': null,
      'tipoEstacion': '00',
      'mensajeRespuesta': 'OPERACIÓN REALIZADA SATISFACTORIAMENTE',
      'institucionCanalizaServicio': '000000',
      'puntoVentaSolicitanteServicio': '00',
      'fechaProceso': '20210330',
      'origenRespuesta': '02',
      'numeroTransaccion': '7252209434616016',
      'institucionProveedorServicio': '300000',
      'codigoProcesadorAdquirente': null,
      'codigoTransaccion': '500000',
      'validacionesBiometricas': [
          {
              'apellidoMaterno': 'CARRASCO',
              'apellidoPaterno': 'BAJALQUI',
              'codigoRespuesta': null,
              'codigoTransaccion': '301000',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '03/03/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '8740580460025774',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': null,
              'nombres': 'EDGAR GUSTAVO',
              'numeroDocumento': '88888888',
              'origenRespuesta': 'INVALIDO',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'DNI',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'CARRASCO',
              'apellidoPaterno': 'BAJALQUI',
              'codigoRespuesta': null,
              'codigoTransaccion': '301000',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '03/03/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '1435356861058524',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': null,
              'nombres': 'EDGAR GUSTAVO',
              'numeroDocumento': '88888888',
              'origenRespuesta': 'INVALIDO',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'DNI',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'CARRASCO',
              'apellidoPaterno': 'BAJALQUI',
              'codigoRespuesta': null,
              'codigoTransaccion': '301000',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '03/03/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '0631463202555101',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': null,
              'nombres': 'EDGAR GUSTAVO',
              'numeroDocumento': '88888888',
              'origenRespuesta': 'INVALIDO',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'DNI',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'CARRASCO',
              'apellidoPaterno': 'BAJALQUI',
              'codigoRespuesta': null,
              'codigoTransaccion': '301000',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '03/03/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '0441148302662070',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': null,
              'nombres': 'EDGAR GUSTAVO',
              'numeroDocumento': '88888888',
              'origenRespuesta': 'INVALIDO',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'DNI',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'CARRASCO',
              'apellidoPaterno': 'BAJALQUI',
              'codigoRespuesta': null,
              'codigoTransaccion': '301000',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '13/02/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '6304808127482730',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': null,
              'nombres': 'EDGAR GUSTAVO',
              'numeroDocumento': '88888888',
              'origenRespuesta': 'INVALIDO',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'DNI',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'CARRASCO',
              'apellidoPaterno': 'BAJALQUI',
              'codigoRespuesta': null,
              'codigoTransaccion': '301000',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '10/02/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '0824278678231766',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': null,
              'nombres': 'EDGAR GUSTAVO',
              'numeroDocumento': '88888888',
              'origenRespuesta': 'INVALIDO',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'DNI',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'MAMANI',
              'apellidoPaterno': 'SMITH',
              'codigoRespuesta': null,
              'codigoTransaccion': '310000',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '10/02/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '8856425848271707',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': null,
              'nombres': 'GERRY CHERRY TERRY',
              'numeroDocumento': '7777777777777',
              'origenRespuesta': 'INVALIDO',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'PASAPORTE',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'MAMANI',
              'apellidoPaterno': 'SMITH',
              'codigoRespuesta': '00000',
              'codigoTransaccion': '310000',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '10/02/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '4523838724174847',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': 'SI CORRESPONDE',
              'nombres': 'GERRY CHERRY TERRY',
              'numeroDocumento': '7777777777777',
              'origenRespuesta': 'NOVATRONIC',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'PASAPORTE',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'MAMANI',
              'apellidoPaterno': 'SMITH',
              'codigoRespuesta': '00000',
              'codigoTransaccion': '310000',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '10/02/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '1850878025236177',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': 'SI CORRESPONDE',
              'nombres': 'GERRY CHERRY TERRY',
              'numeroDocumento': '7777777777777',
              'origenRespuesta': 'NOVATRONIC',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'PASAPORTE',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'CARRASCO',
              'apellidoPaterno': 'BAJALQUI',
              'codigoRespuesta': '00000',
              'codigoTransaccion': '301001',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '10/02/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '1973226176994878',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': 'SI CORRESPONDE',
              'nombres': 'EDGAR GUSTAVO',
              'numeroDocumento': '88888888',
              'origenRespuesta': 'NOVATRONIC',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'DNI',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'CARRASCO',
              'apellidoPaterno': 'BAJALQUI',
              'codigoRespuesta': '10000',
              'codigoTransaccion': '301001',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '10/02/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '2318016121671603',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': 'NO CORRESPONDE',
              'nombres': 'EDGAR GUSTAVO',
              'numeroDocumento': '88888888',
              'origenRespuesta': 'NOVATRONIC',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'DNI',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'CARRASCO',
              'apellidoPaterno': 'BAJALQUI',
              'codigoRespuesta': '00000',
              'codigoTransaccion': '301001',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '10/02/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '2453856751465534',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': 'SI CORRESPONDE',
              'nombres': 'EDGAR GUSTAVO',
              'numeroDocumento': '88888888',
              'origenRespuesta': 'NOVATRONIC',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'DNI',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          },
          {
              'apellidoMaterno': 'MAMANI',
              'apellidoPaterno': 'SMITH',
              'codigoRespuesta': '00000',
              'codigoTransaccion': '310000',
              'fechaCaducidad': '19-12-2022',
              'fechaEmision': '01-02-2016',
              'fechaNacimiento': '29-12-1991',
              'fechaValidacion': '10/02/2020',
              'horaProceso': '00:00:00',
              'idValidacion': '3158671148180673',
              'latitudLocalizacion': '-77.010710',
              'longitudLocalizacion': '-12.107029',
              'mensajeRespuesta': 'SI CORRESPONDE',
              'nombres': 'GERRY CHERRY TERRY',
              'numeroDocumento': '7777777777777',
              'origenRespuesta': 'NOVATRONIC',
              'sexo': 'MASCULINO',
              'sistemaLocalizacion': 'PSISTGEO001',
              'tipoDocumento': 'PASAPORTE',
              'usuario': 'abenito',
              'tipoVerificacion': 'T33'
          }
      ],
      'autenticacionMensaje': '1216386664685317',
      'tipoFormato': '0002',
      'paginas': '1',
      'codigoAdquirente': null,
      'idSesion': '000000202103301640430160022',
      'tipoIdentificadorCliente': '01',
      'longitudLocalizacion': null,
      'codigoAutorizador': '189513252949914',
      'codigoSistemaLocalizacion': null,
      'tipoMensaje': '0210',
      'tipoRespuesta': '00',
      'modeloEstacion': 'NOVA-MRIOS',
      'tipoIdentificacionEstacion': '01',
      'codigoAplicativo': '00',
      'codigoRespuesta': '00000',
      'agenteSolicitanteServicio': '0',
      'codigoIdentificacionEstacion': '00'
    };
    // Fakeamos la respuesta de nuestro servicio para que nos retorne el mockDataResponse
    const spyServiceFake = spyOn(servicio, 'serviceConsultarValidaciones').and.callFake(() => {
      return of(mockDataResponse);
    });
    // Espiamos la funcion setCamposGrillaExportar
    const spysetCampoGrillaExportar = spyOn(component, 'setCamposGrillaExportar').and.callThrough();
    // Espiamos la funcion exportar
    const spyexportar = spyOn(component, 'exportar').and.callThrough();
    // Llamamos nuestra funcion , le pasamos parametros
    component.buscarValidacionesExportar('', '', 'reporteValidaciones' + '.xls');
    // Verificamos que que llame la funcion setCamposGrillaExportar
    expect(spysetCampoGrillaExportar).toHaveBeenCalled();
    // Verificamos que llame la funcion spyexportar
    expect(spyexportar).toHaveBeenCalled();
    // Verificamos que llame nuestro servicio Fake
    expect(spyServiceFake).toHaveBeenCalled();
    //  Verificamos los valores de isClicPDF y isClicExcel
    expect(component.isClicPDF).toBeFalsy();
    expect(component.isClicExcel).toBeFalsy();
    // Verificamos que los valores asignado a listaValidacionesExportar sean las del mockDataResponse.validacionesBiometricas
    expect(component.listaValidacionesExportar).toEqual(mockDataResponse.validacionesBiometricas);

    // Ahora verificamos para el caso Constant.COD_ERROR_SESION
    mockDataResponse.codigoRespuesta = Constant.COD_ERROR_SESION;
    // Espiamos nuestra funcion salir
    const spySalir = spyOn(component, 'salir').and.callThrough();
    // Llamamos nuestra funcion , le pasamos parametros
    component.buscarValidacionesExportar('', '', 'reporteValidaciones' + '.xls');
    // Verificamos que llame nuestra  funcion salir();
    expect(spySalir).toHaveBeenCalled();

    // Ahora verificamos para el caso nos de un codigoRespuesta disferente a los mapeados
    mockDataResponse.codigoRespuesta = 'Ninguno de los Anteriores';
    // Fakeamos nuestro Toast , caso contrario nos dara error por la libreria translate
    const spyToastFake = spyOn(component.toast, 'addToast').and.callFake(() => { 'Nada'; });
    // Llamamos nuestra funcion , le pasamos parametros
    component.buscarValidacionesExportar('', '', 'reporteValidaciones' + '.xls');
    // Verificamos que llame a nuestro Toast FAKE
    expect(spyToastFake).toHaveBeenCalled();
    //  Verificamos los valores de isClicPDF y isClicExcel
    expect(component.isClicPDF).toBeFalsy();
    expect(component.isClicExcel).toBeFalsy();

    // Ahora verificamos el caso de que tenga un Error con nuestro servicio
    const spyServicioError = spyOn(servicio, 'serviceConsultarValidaciones').and.returnValue(throwError('Error serviceGenerarReporteValidaciones'));
    // Llamamos nuestra funcion , le pasamos parametros
    component.buscarValidacionesExportar('', '', 'reporteValidaciones' + '.xls');
    // Verificamos que se llame nuestro servicio Error
    expect(spyServicioError).toHaveBeenCalled();
    // Verificamos que llame nuestra  funcion salir();
    expect(spySalir).toHaveBeenCalled();
  });



  it('Verificamos funcion listarUsuarios()', () => {
    // Espiamos nuestro servicio serviceListarUsuario
    const spyService = spyOn(servicio, 'serviceListarUsuario').and.callThrough();
    // Llamamos nuestra funcion listarUsuarios
    component.listarUsuarios();
    // Verificamos nuestro espia del servicio fuera llamado
    expect(spyService).toHaveBeenCalled();


    // Ahora mockeamos un mockDataResponse
    const mockDataResponse = {
      'listarUsuario': {
        'codigoRespuesta': '0000',
        'mensajeRespuesta': 'OPERACION REALIZADA SATISFACTORIAMENTE',
        'listUsuarios': [
          {
            'apPaterno': 'Palomino',
            'apMaterno': 'Astupuma',
            'nombre': 'Katherine',
            'tipoDoc': 'DNI',
            'numDoc': '45896328',
            'telef': '',
            'correo': 'kpalomino@novatronic.com',
            'fecNac': '03/07/1990',
            'usuario': 'kpalomino',
            'estado': 'INHABILITADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '13/05/2020',
            'audFecCreac': '17/07/2019',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Nuflo',
            'apMaterno': 'Gamarra',
            'nombre': 'Isaac',
            'tipoDoc': 'DNI',
            'numDoc': '75328398',
            'telef': '',
            'correo': 'inuflo@novatronic.com',
            'fecNac': '21/06/1997',
            'usuario': 'FDOperador',
            'estado': 'INHABILITADO',
            'rol': 'Operadorfirmdig',
            'usuarioLogueado': null,
            'audFecModif': '14/01/2020',
            'audFecCreac': '27/03/2019',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Rivas',
            'apMaterno': 'Estrada',
            'nombre': 'Manuel',
            'tipoDoc': 'DNI',
            'numDoc': '41836380',
            'telef': '992783250',
            'correo': 'empresamanuel@gmail.com',
            'fecNac': '08/11/2019',
            'usuario': 'mrivas2',
            'estado': 'HABILITADO',
            'rol': 'rolagrupadorconsultor',
            'usuarioLogueado': null,
            'audFecModif': '04/03/2020',
            'audFecCreac': '04/03/2020',
            'audUsuModif': 'mrivas2',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'JoelPRueba22',
            'apMaterno': 'JoelPRueba22fwefwfwfwf',
            'nombre': 'JoelPRueba2255',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '01/07/2020',
            'usuario': 'JoelPRueba22',
            'estado': 'RESETEADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '29/03/2021',
            'audFecCreac': '13/07/2020',
            'audUsuModif': 'JoelPRueba22',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'efeefefef3555555555555555555',
            'apMaterno': 'efeefefef3444444444444444444',
            'nombre': 'efeefefef3444444444444444444444444444',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '05/09/2020',
            'usuario': 'efeefefef3',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '03/09/2020',
            'audFecCreac': '03/09/2020',
            'audUsuModif': 'jdelosrios2',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'Bajalqui',
            'apMaterno': 'Carrasco',
            'nombre': 'Edgar Gustavo',
            'tipoDoc': 'DNI',
            'numDoc': '12345678',
            'telef': '965773387',
            'correo': 'edgar.bajalqui@gmail.com',
            'fecNac': '29/12/1989',
            'usuario': 'bioadmin',
            'estado': 'IMPORTADO',
            'rol': 'RolAgrpSIXBIO',
            'usuarioLogueado': null,
            'audFecModif': '22/07/2019',
            'audFecCreac': '10/04/2019',
            'audUsuModif': '',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Benito',
            'apMaterno': 'Torres',
            'nombre': 'Fazio44',
            'tipoDoc': 'DNI',
            'numDoc': '70652784',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '30/01/1995',
            'usuario': 'abenito4',
            'estado': 'HABILITADO',
            'rol': 'AGRUPADOR_ADM',
            'usuarioLogueado': null,
            'audFecModif': '24/02/2021',
            'audFecCreac': '09/06/2020',
            'audUsuModif': 'abenito4',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'De los rios5',
            'apMaterno': 'Guimack',
            'nombre': 'Joel Prueba Edicion7',
            'tipoDoc': 'DNI',
            'numDoc': '10126886',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '20/05/1994',
            'usuario': 'jdelosrios2',
            'estado': 'INHABILITADO',
            'rol': 'ROL_ADM',
            'usuarioLogueado': null,
            'audFecModif': '24/10/2020',
            'audFecCreac': '23/01/2020',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'scaCaidoLevantado',
            'apMaterno': 'scaCaidoLevantado',
            'nombre': 'scaCaidoLevantado',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '01/08/2020',
            'usuario': 'scaCaidoLevantado',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '10/08/2020',
            'audUsuModif': '',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'Villegas',
            'apMaterno': 'Diaz',
            'nombre': 'Juan Diego',
            'tipoDoc': 'DNI',
            'numDoc': '12842823',
            'telef': '',
            'correo': 'jvillegas@novatronic.com',
            'fecNac': '02/02/2021',
            'usuario': 'jvillegas3',
            'estado': 'INHABILITADO',
            'rol': 'AGRUPADOR_PROCESADOR',
            'usuarioLogueado': null,
            'audFecModif': '12/03/2021',
            'audFecCreac': '04/02/2021',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'jvillegas'
          },
          {
            'apPaterno': 'Benito5',
            'apMaterno': 'Torres5',
            'nombre': 'Fazio5',
            'tipoDoc': 'DNI',
            'numDoc': '85474584',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '01/02/2021',
            'usuario': 'fbenito5',
            'estado': 'HABILITADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '16/03/2021',
            'audFecCreac': '22/02/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'abenito5'
          },
          {
            'apPaterno': 'khjkhjkhjkhj',
            'apMaterno': 'khjkhjk',
            'nombre': 'hkjhkhj',
            'tipoDoc': 'DNI',
            'numDoc': '99999999',
            'telef': '546546546',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '18/03/2021',
            'usuario': 'jburgos5',
            'estado': 'HABILITADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '30/03/2021',
            'audFecCreac': '05/03/2021',
            'audUsuModif': 'jburgos5',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'Martinez',
            'apMaterno': 'Linares',
            'nombre': 'Marco',
            'tipoDoc': 'DNI',
            'numDoc': '42424242',
            'telef': '999999999',
            'correo': 'mmartinez_prov@novatronic.com',
            'fecNac': '13/06/2019',
            'usuario': 'mmartinez',
            'estado': 'INHABILITADO',
            'rol': 'rol_adm_sms',
            'usuarioLogueado': null,
            'audFecModif': '14/01/2020',
            'audFecCreac': '05/06/2019',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Nuflo',
            'apMaterno': 'Gamarra',
            'nombre': 'isaac',
            'tipoDoc': 'DNI',
            'numDoc': '75328398',
            'telef': '',
            'correo': 'inuflo@novatronic.com',
            'fecNac': '21/06/1997',
            'usuario': 'inuflo1',
            'estado': 'INHABILITADO',
            'rol': 'rol_adm_sms',
            'usuarioLogueado': null,
            'audFecModif': '14/01/2020',
            'audFecCreac': '15/11/2019',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Chunga',
            'apMaterno': 'Chapilliquen',
            'nombre': 'Jose',
            'tipoDoc': 'DNI',
            'numDoc': '71487247',
            'telef': '999999999',
            'correo': 'djimenez@novatronic.com',
            'fecNac': '01/11/2019',
            'usuario': 'jchunga',
            'estado': 'INHABILITADO',
            'rol': 'stiAdmin',
            'usuarioLogueado': null,
            'audFecModif': '16/12/2020',
            'audFecCreac': '26/11/2019',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Nuflo',
            'apMaterno': 'Gamarra',
            'nombre': 'Isaac',
            'tipoDoc': 'DNI',
            'numDoc': '75328398',
            'telef': '',
            'correo': 'inuflo@novatronic.com',
            'fecNac': '21/06/1997',
            'usuario': 'FDProcesador',
            'estado': 'INHABILITADO',
            'rol': 'adminFDig',
            'usuarioLogueado': null,
            'audFecModif': '14/01/2020',
            'audFecCreac': '27/03/2019',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Alcala',
            'apMaterno': 'Marcos',
            'nombre': 'Giancarlo',
            'tipoDoc': 'DNI',
            'numDoc': '42728713',
            'telef': '',
            'correo': 'evaldivia@novatronic.com',
            'fecNac': '03/10/1984',
            'usuario': 'galcala',
            'estado': 'HABILITADO',
            'rol': 'ROL_ADM',
            'usuarioLogueado': null,
            'audFecModif': '30/12/2020',
            'audFecCreac': '15/07/2019',
            'audUsuModif': 'galcala',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Alcala',
            'apMaterno': 'Marcos',
            'nombre': 'Giancarlo',
            'tipoDoc': 'DNI',
            'numDoc': '12345678',
            'telef': '',
            'correo': 'galcala@novatronic.com',
            'fecNac': '01/01/2000',
            'usuario': 'adminBio',
            'estado': 'INHABILITADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '21/01/2020',
            'audFecCreac': '15/07/2019',
            'audUsuModif': 'evaldivia',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Salazar',
            'apMaterno': 'Llactahuaman',
            'nombre': 'Gerry',
            'tipoDoc': 'DNI',
            'numDoc': '73383637',
            'telef': '',
            'correo': 'gsalazar@novatronic.com',
            'fecNac': '01/01/2000',
            'usuario': 'gsalazar',
            'estado': 'INHABILITADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '06/04/2020',
            'audFecCreac': '15/07/2019',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'DIONISIO',
            'apMaterno': 'TRIVENIO',
            'nombre': 'CHRISTIAN',
            'tipoDoc': 'DNI',
            'numDoc': '98546521',
            'telef': '950565189',
            'correo': 'cdionisio@novatronic.com',
            'fecNac': '29/11/1994',
            'usuario': '74854354',
            'estado': 'IMPORTADO',
            'rol': 'stiAdmin',
            'usuarioLogueado': null,
            'audFecModif': '13/12/2019',
            'audFecCreac': '13/12/2019',
            'audUsuModif': 'admin',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Benito',
            'apMaterno': 'Torres',
            'nombre': 'Fazio',
            'tipoDoc': 'DNI',
            'numDoc': '70652784',
            'telef': '333333333',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '30/01/1995',
            'usuario': 'abenito',
            'estado': 'HABILITADO',
            'rol': 'sixbioweb_adm',
            'usuarioLogueado': null,
            'audFecModif': '30/03/2021',
            'audFecCreac': '28/01/2020',
            'audUsuModif': 'abenito',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Mendoza',
            'apMaterno': 'Rios',
            'nombre': 'Miguel',
            'tipoDoc': 'DNI',
            'numDoc': '88888888',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '04/01/2020',
            'usuario': 'mrioss',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '30/01/2020',
            'audUsuModif': '',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Jimenez',
            'apMaterno': 'Huaman',
            'nombre': 'Dennys',
            'tipoDoc': 'DNI',
            'numDoc': '12354678',
            'telef': '12345678',
            'correo': 'djimenez@novatronic.com',
            'fecNac': '10/02/2020',
            'usuario': 'djimenezSTI',
            'estado': 'RESETEADO',
            'rol': 'stiAdmin',
            'usuarioLogueado': null,
            'audFecModif': '05/03/2020',
            'audFecCreac': '17/02/2020',
            'audUsuModif': 'admin',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'fbenito6',
            'apMaterno': 'fbenito6',
            'nombre': 'fbenito6',
            'tipoDoc': 'DNI',
            'numDoc': '85748574',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '01/02/2021',
            'usuario': 'fbenito6',
            'estado': 'HABILITADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '15/03/2021',
            'audFecCreac': '23/02/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'fbenito5'
          },
          {
            'apPaterno': 'Rivas',
            'apMaterno': 'Estrada',
            'nombre': 'Manuel',
            'tipoDoc': 'DNI',
            'numDoc': '41836380',
            'telef': '992783250',
            'correo': 'empresamanuel@gmail.com',
            'fecNac': '07/02/1983',
            'usuario': 'mrivas',
            'estado': 'INHABILITADO',
            'rol': 'rolagrupadorconsultor',
            'usuarioLogueado': null,
            'audFecModif': '04/04/2020',
            'audFecCreac': '28/02/2020',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Valdivia',
            'apMaterno': 'Noa',
            'nombre': 'Eric',
            'tipoDoc': 'DNI',
            'numDoc': '72890317',
            'telef': '999999999',
            'correo': 'evaldivia@novatronic.com',
            'fecNac': '25/09/1992',
            'usuario': 'evaldivia2',
            'estado': 'RESETEADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '25/05/2020',
            'audFecCreac': '14/05/2020',
            'audUsuModif': 'evaldivia2',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'abenito9',
            'apMaterno': 'abenito9',
            'nombre': 'abenito9',
            'tipoDoc': 'DNI',
            'numDoc': '77777777',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '02/02/2021',
            'usuario': 'abenito9',
            'estado': 'HABILITADO',
            'rol': 'ROL_ADM',
            'usuarioLogueado': null,
            'audFecModif': '25/02/2021',
            'audFecCreac': '25/02/2021',
            'audUsuModif': 'abenito9',
            'audUsuCreac': 'abenito5'
          },
          {
            'apPaterno': 'Alcala',
            'apMaterno': 'Marcos',
            'nombre': 'Giancarlo',
            'tipoDoc': 'DNI',
            'numDoc': '42728713',
            'telef': '',
            'correo': 'galcala@novatronic.com',
            'fecNac': '14/06/1996',
            'usuario': 'usuarioPrueba',
            'estado': 'INHABILITADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '31/07/2020',
            'audFecCreac': '30/06/2020',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'Benito',
            'apMaterno': 'Torres',
            'nombre': 'Fazio',
            'tipoDoc': 'DNI',
            'numDoc': '70652784',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '14/03/1996',
            'usuario': 'abenitoprueba4',
            'estado': 'RESETEADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '10/07/2020',
            'audFecCreac': '10/07/2020',
            'audUsuModif': 'abenitoprueba4',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'abenito',
            'apMaterno': 'abenito',
            'nombre': 'abenito',
            'tipoDoc': 'DNI',
            'numDoc': '78452524',
            'telef': '',
            'correo': 'abenito@novatronic.com',
            'fecNac': '08/07/2020',
            'usuario': 'Ar',
            'estado': 'RESETEADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '17/07/2020',
            'audFecCreac': '16/07/2020',
            'audUsuModif': 'admin',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'abenito',
            'apMaterno': 'abenito',
            'nombre': 'abenito',
            'tipoDoc': 'DNI',
            'numDoc': '87548748',
            'telef': '',
            'correo': 'abenito@novatronic.com',
            'fecNac': '07/07/2020',
            'usuario': 'Aman',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '16/07/2020',
            'audUsuModif': '',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'abenito',
            'apMaterno': 'abenito',
            'nombre': 'abenito',
            'tipoDoc': 'DNI',
            'numDoc': '74785748',
            'telef': '',
            'correo': 'abenito@novatronic.com',
            'fecNac': '07/07/2020',
            'usuario': 'ds',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '17/07/2020',
            'audUsuModif': '',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'abenito',
            'apMaterno': 'abenito',
            'nombre': 'abenito',
            'tipoDoc': 'DNI',
            'numDoc': '78574857',
            'telef': '',
            'correo': 'abenito@novatronic.com',
            'fecNac': '07/07/2020',
            'usuario': 't12',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '17/07/2020',
            'audUsuModif': '',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'abenito',
            'apMaterno': 'abenito',
            'nombre': 'abenito',
            'tipoDoc': 'DNI',
            'numDoc': '85748748',
            'telef': '',
            'correo': 'abenito@novatronic.com',
            'fecNac': '07/07/2020',
            'usuario': 'testbenito',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '21/07/2020',
            'audUsuModif': '',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'abenito',
            'apMaterno': 'abenito',
            'nombre': 'abenito',
            'tipoDoc': 'DNI',
            'numDoc': '85748574',
            'telef': '474857487',
            'correo': 'abenito@novatronic.com',
            'fecNac': '15/07/2020',
            'usuario': '(@g85',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '23/07/2020',
            'audUsuModif': '',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'abenito3',
            'apMaterno': 'abenito3',
            'nombre': 'abenito3',
            'tipoDoc': 'DNI',
            'numDoc': '74857487',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '02/11/2020',
            'usuario': 'abenito3',
            'estado': 'CREADO',
            'rol': 'RolAdminBioWeb',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '10/11/2020',
            'audUsuModif': '',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'wfwfwf',
            'apMaterno': 'wfwfwfwf',
            'nombre': 'wfwfwfw',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '01/08/2020',
            'usuario': 'wfwfwfwf',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '03/09/2020',
            'audFecCreac': '28/08/2020',
            'audUsuModif': 'jdelosrios2',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'abenito1',
            'apMaterno': 'abenito1',
            'nombre': 'abenito1',
            'tipoDoc': 'DNI',
            'numDoc': '85748574',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '03/11/2020',
            'usuario': 'abenito1',
            'estado': 'RESETEADO',
            'rol': 'ROL_ADM',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '10/11/2020',
            'audUsuModif': 'admin',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'joelPrueba3266',
            'apMaterno': 'joelPrueba32',
            'nombre': 'joelPrueba32',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '920805153',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '12/09/2020',
            'usuario': 'joelPrueba32',
            'estado': 'RESETEADO',
            'rol': 'ROL_ADM',
            'usuarioLogueado': null,
            'audFecModif': '03/09/2020',
            'audFecCreac': '31/08/2020',
            'audUsuModif': 'jdelosrios2',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'jvillegas2',
            'apMaterno': 'jvillegas2',
            'nombre': 'jvillegas2',
            'tipoDoc': 'DNI',
            'numDoc': '44326766',
            'telef': '',
            'correo': 'jvillegas@novatronic.com',
            'fecNac': '05/11/1999',
            'usuario': 'jvillegas2',
            'estado': 'HABILITADO',
            'rol': 'ROL_ADM',
            'usuarioLogueado': null,
            'audFecModif': '31/03/2021',
            'audFecCreac': '26/11/2020',
            'audUsuModif': 'jvillegas2',
            'audUsuCreac': 'jvillegas'
          },
          {
            'apPaterno': 'Burgos',
            'apMaterno': 'Tejada',
            'nombre': 'Jaime',
            'tipoDoc': 'DNI',
            'numDoc': '12345678',
            'telef': '',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '18/01/2021',
            'usuario': 'jburgos',
            'estado': 'INHABILITADO',
            'rol': 'AGRUPADOR_ADM',
            'usuarioLogueado': null,
            'audFecModif': '08/03/2021',
            'audFecCreac': '18/01/2021',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'joeprueba99',
            'apMaterno': 'joeprueba33',
            'nombre': 'joeprueba33',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '999999999',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '02/08/2020',
            'usuario': 'joeprueba34',
            'estado': 'CREADO',
            'rol': 'ROL_ADM',
            'usuarioLogueado': null,
            'audFecModif': '03/09/2020',
            'audFecCreac': '31/08/2020',
            'audUsuModif': 'jdelosrios2',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'joelprueba34',
            'apMaterno': 'joelprueba34www',
            'nombre': 'joelprueba34',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '01/08/2020',
            'usuario': 'joelprueba34',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '31/08/2020',
            'audFecCreac': '31/08/2020',
            'audUsuModif': 'admin',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'joelprueba35',
            'apMaterno': 'joelprueba35',
            'nombre': 'joelprueba35',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '01/08/2020',
            'usuario': 'joelprueba35',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '31/08/2020',
            'audUsuModif': '',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'joelprueba36',
            'apMaterno': 'joelprueba36',
            'nombre': 'joelprueba36',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '07/08/2020',
            'usuario': 'joelprueba36',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '31/08/2020',
            'audUsuModif': '',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'Alcala',
            'apMaterno': 'Marcos',
            'nombre': 'Giancarlo',
            'tipoDoc': 'DNI',
            'numDoc': '44444444',
            'telef': '997021999',
            'correo': 'galcala@novatronic.com',
            'fecNac': '03/05/1987',
            'usuario': 'galcala9',
            'estado': 'INHABILITADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '01/08/2020',
            'audFecCreac': '06/05/2020',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'Valdivia',
            'apMaterno': 'Noa',
            'nombre': 'Eric',
            'tipoDoc': 'DNI',
            'numDoc': '72890317',
            'telef': '',
            'correo': 'evaldivia@novatronic.com',
            'fecNac': '25/09/1992',
            'usuario': 'evaldivia',
            'estado': 'INHABILITADO',
            'rol': 'ROL_ADM',
            'usuarioLogueado': null,
            'audFecModif': '28/02/2021',
            'audFecCreac': '18/05/2020',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'Benito',
            'apMaterno': 'Torres',
            'nombre': 'Fazio',
            'tipoDoc': 'DNI',
            'numDoc': '70652784',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '12/07/1995',
            'usuario': 'abenito5',
            'estado': 'HABILITADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '11/03/2021',
            'audFecCreac': '24/07/2020',
            'audUsuModif': 'abenito5',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'bancognb',
            'apMaterno': 'bancognb',
            'nombre': 'bancognb',
            'tipoDoc': 'DNI',
            'numDoc': '12345678',
            'telef': '',
            'correo': 'jchunga@novatronic.com',
            'fecNac': '01/02/2050',
            'usuario': 'adminsti',
            'estado': 'IMPORTADO',
            'rol': 'adminSTICliente',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '13/11/2020',
            'audUsuModif': '',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'palomino',
            'apMaterno': 'chahua',
            'nombre': 'solange',
            'tipoDoc': 'DNI',
            'numDoc': '74883732',
            'telef': '967770360',
            'correo': 'spalomino@novatronic.com',
            'fecNac': '23/07/1997',
            'usuario': 'solange',
            'estado': 'INHABILITADO',
            'rol': 'rol_adm_sms',
            'usuarioLogueado': null,
            'audFecModif': '26/01/2021',
            'audFecCreac': '19/10/2020',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'test',
            'apMaterno': 'test',
            'nombre': 'Test',
            'tipoDoc': 'RUC',
            'numDoc': '99999999999',
            'telef': '111111111',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '15/03/2003',
            'usuario': 'jburgos3',
            'estado': 'HABILITADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '18/03/2021',
            'audFecCreac': '15/03/2021',
            'audUsuModif': 'jburgos3',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'ffffffffffffffffffffffffffffffffffffffffffffffffff',
            'apMaterno': 'ffffffffffffffffffffffffffffffffffffffffffffffffff',
            'nombre': 'ffffffffffffffffffffffffffffffffffffffffffffffffff',
            'tipoDoc': 'RUC',
            'numDoc': '99999999999',
            'telef': '111111111',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '13/03/2003',
            'usuario': 'TestUnitffffffffffff',
            'estado': 'RESETEADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '29/03/2021',
            'audFecCreac': '15/03/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'TestingUnit',
            'apMaterno': 'Unit',
            'nombre': 'Testing',
            'tipoDoc': 'DNI',
            'numDoc': '55555555',
            'telef': '',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '01/03/2003',
            'usuario': 'TestingUnitTest',
            'estado': 'CREADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '29/03/2021',
            'audFecCreac': '29/03/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'DFGFDGFD',
            'apMaterno': 'GFDGDFGFD',
            'nombre': 'SDFSDFSD',
            'tipoDoc': 'RUC',
            'numDoc': '99999999999',
            'telef': '',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '03/03/2003',
            'usuario': 'NewTest',
            'estado': 'CREADO',
            'rol': 'sixbioweb_adm',
            'usuarioLogueado': null,
            'audFecModif': '29/03/2021',
            'audFecCreac': '29/03/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'Date',
            'apMaterno': 'Date',
            'nombre': 'Date',
            'tipoDoc': 'DNI',
            'numDoc': '99999999',
            'telef': '333333333',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '29/03/2003',
            'usuario': 'TestNewDate',
            'estado': 'CREADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '29/03/2021',
            'audFecCreac': '29/03/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'ccc',
            'apMaterno': 'cccc',
            'nombre': 'sdfsd',
            'tipoDoc': 'DNI',
            'numDoc': '99999999',
            'telef': '',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '06/03/2003',
            'usuario': 'TestFormsdfsd',
            'estado': 'CREADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '29/03/2021',
            'audFecCreac': '29/03/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'dsfds',
            'apMaterno': 'dsfdsf',
            'nombre': 'sdfsd',
            'tipoDoc': 'DNI',
            'numDoc': '99999999',
            'telef': '',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '29/03/2003',
            'usuario': 'Testinsdf',
            'estado': 'CREADO',
            'rol': 'sixbioweb_adm',
            'usuarioLogueado': null,
            'audFecModif': '29/03/2021',
            'audFecCreac': '29/03/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'Benito',
            'apMaterno': 'Torres',
            'nombre': 'Fazio',
            'tipoDoc': 'DNI',
            'numDoc': '70652784',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '30/01/1995',
            'usuario': 'fbenito2',
            'estado': 'INHABILITADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '25/01/2021',
            'audFecCreac': '25/05/2020',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'Benito',
            'apMaterno': 'Torres',
            'nombre': 'Fazio',
            'tipoDoc': 'DNI',
            'numDoc': '70652784',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '09/05/1995',
            'usuario': 'fbenito3',
            'estado': 'HABILITADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '25/05/2020',
            'audFecCreac': '25/05/2020',
            'audUsuModif': 'fbenito3',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'Meza',
            'apMaterno': '0000000',
            'nombre': 'Gustavo',
            'tipoDoc': 'DNI',
            'numDoc': '12345678',
            'telef': '',
            'correo': 'gmeza@novatronic.com',
            'fecNac': '06/03/2021',
            'usuario': 'gmeza5',
            'estado': 'CREADO',
            'rol': 'sixbioweb_adm',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '19/03/2021',
            'audUsuModif': '',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Meza',
            'apMaterno': '0000000',
            'nombre': 'Gustavo',
            'tipoDoc': 'DNI',
            'numDoc': '99999999',
            'telef': '',
            'correo': 'gmeza@novatronic.com',
            'fecNac': '03/02/2003',
            'usuario': 'gmeza3',
            'estado': 'CREADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '19/03/2021',
            'audUsuModif': '',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'asdasdasdasd',
            'apMaterno': 'asdasdas',
            'nombre': 'rasdasdasd',
            'tipoDoc': 'DNI',
            'numDoc': '12345678',
            'telef': '232432432',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '07/03/2021',
            'usuario': 'Angular9',
            'estado': 'RESETEADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '25/03/2021',
            'audFecCreac': '10/03/2021',
            'audUsuModif': 'Angular9',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'sdafsdfsdf',
            'apMaterno': 'sdfsdfsdfsd',
            'nombre': 'asdfsdfgsd',
            'tipoDoc': 'RUC',
            'numDoc': '99999999999',
            'telef': '234444444',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '19/03/2003',
            'usuario': 'Primerlogin',
            'estado': 'RESETEADO',
            'rol': 'sixbioweb_adm',
            'usuarioLogueado': null,
            'audFecModif': '29/03/2021',
            'audFecCreac': '23/03/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'sdfsdfds',
            'apMaterno': '3432',
            'nombre': 'saf',
            'tipoDoc': 'RUC',
            'numDoc': '99999999999',
            'telef': '111111111',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '03/05/2003',
            'usuario': 'fdgdfsdxxdsfds',
            'estado': 'RESETEADO',
            'rol': 'sixbioweb_adm',
            'usuarioLogueado': null,
            'audFecModif': '29/03/2021',
            'audFecCreac': '25/03/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'Benito',
            'apMaterno': 'Torres',
            'nombre': 'Fazio',
            'tipoDoc': 'DNI',
            'numDoc': '70652784',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '30/01/1995',
            'usuario': 'fbenito',
            'estado': 'INHABILITADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '28/02/2021',
            'audFecCreac': '19/05/2020',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'villegas',
            'apMaterno': 'diaz',
            'nombre': 'juan diego',
            'tipoDoc': 'DNI',
            'numDoc': '88888888',
            'telef': '',
            'correo': 'jvillegas@novatronic.com',
            'fecNac': '01/09/2020',
            'usuario': 'jvillegas',
            'estado': 'HABILITADO',
            'rol': 'AGRUPADOR_ADM',
            'usuarioLogueado': null,
            'audFecModif': '23/03/2021',
            'audFecCreac': '15/09/2020',
            'audUsuModif': 'jvillegas',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'stefantu',
            'apMaterno': 'stefantu',
            'nombre': 'stefantu',
            'tipoDoc': 'DNI',
            'numDoc': '71957130',
            'telef': '941886550',
            'correo': 'ssolari@novatronic.com',
            'fecNac': '18/08/1984',
            'usuario': 'stefantu',
            'estado': 'INHABILITADO',
            'rol': 'rol_adm_sms',
            'usuarioLogueado': null,
            'audFecModif': '02/12/2020',
            'audFecCreac': '16/10/2020',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Burgos',
            'apMaterno': 'Tejada',
            'nombre': 'Jaime',
            'tipoDoc': 'DNI',
            'numDoc': '12345678',
            'telef': '',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '25/02/2021',
            'usuario': 'jburgos6',
            'estado': 'HABILITADO',
            'rol': 'sixbioweb_adm',
            'usuarioLogueado': null,
            'audFecModif': '30/03/2021',
            'audFecCreac': '23/02/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Six',
            'apMaterno': 'Bio',
            'nombre': 'Administrador',
            'tipoDoc': 'DNI',
            'numDoc': '88888888',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '23/04/2020',
            'usuario': 'administradorBio',
            'estado': 'INHABILITADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '14/06/2020',
            'audFecCreac': '23/04/2020',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'fbenitoprocesador',
            'apMaterno': 'fbenitoprocesador',
            'nombre': 'fbenitoprocesador',
            'tipoDoc': 'DNI',
            'numDoc': '82547414',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '03/02/2021',
            'usuario': 'fbenitoproc',
            'estado': 'INHABILITADO',
            'rol': 'AGRUPADOR_OPERADOR',
            'usuarioLogueado': null,
            'audFecModif': '06/03/2021',
            'audFecCreac': '02/02/2021',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Benito',
            'apMaterno': 'Torres',
            'nombre': 'Fazio',
            'tipoDoc': 'DNI',
            'numDoc': '70652784',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '04/09/2019',
            'usuario': 'abenitoprueba',
            'estado': 'RESETEADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '10/07/2020',
            'audFecCreac': '10/07/2020',
            'audUsuModif': 'abenitoprueba',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'wfwfwfw',
            'apMaterno': 'wfwfwfwf',
            'nombre': 'wfwfwfwf',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '04/09/2020',
            'usuario': 'wwfwfwf',
            'estado': 'CREADO',
            'rol': 'ROL_ADM',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '03/09/2020',
            'audUsuModif': '',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'joelprueba223',
            'apMaterno': 'joelprueba223',
            'nombre': 'joelprueba223',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '02/09/2020',
            'usuario': 'joelprueba223',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '03/09/2020',
            'audUsuModif': '',
            'audUsuCreac': 'jdelosrios2'
          }
        ]
      },
      'codigoRespuesta': Constant.COD_OPERACION_SATISFACTORIA_SCA,
      'mensajeRespuesta': 'OPERACION REALIZADA SATISFACTORIAMENTE'
    };
    // Verificamos la primera condicion donde Constant.COD_OPERACION_SATISFACTORIA_SCA es codigoRespuesta
    // Crearemos un servicio Fake que nos devolvera el mockDataResponse , se usara callFake
    const spyServiceFake = spyOn(servicio, 'serviceListarUsuario').and.callFake(() => {
      return of(mockDataResponse);
    });
    // Espiamos nuestra funcion discriminarUsuarioSixbio();
    const spyDiscriminar = spyOn(component, 'discriminarUsuarioSixbio').and.callThrough();
    // Llamamos nuestra funcion listarUsuarios
    component.listarUsuarios();
    // Verificamos que fuera llamado nuestro servicio Fake
    expect(spyServiceFake).toHaveBeenCalled();
    // Verificamos nuestra funcion discriminarUsuarioSixbio
    expect(spyDiscriminar).toHaveBeenCalled();
    // Verificamos que sea igual a la data entrada por MockDataResponse
    expect(component.listaUsuarios).toEqual(mockDataResponse.listarUsuario.listUsuarios);

    // Ahora verificamos el caso codigoRespuesta sea Constant.COD_ERROR_SESION
    mockDataResponse.codigoRespuesta = Constant.COD_ERROR_SESION;
    const spySalir = spyOn(component, 'salir').and.callThrough();
    // Llamamos nuestra funcion listarUsuarios
    component.listarUsuarios();
    // Verificamos que llame nuestra funcion salir();
    expect(spySalir).toHaveBeenCalled();


    // Ahora verificamos el caso donde el codigoRespuesta no sea ninguno de los Anteriores
    mockDataResponse.codigoRespuesta = 'Ninguno de los Anteriores';
    // Fakeamos el comportamiento de nuestro Toast , en caso contrario dara error por la libreria Translate
    const spyToastFake = spyOn(component.toast, 'addToast').and.callThrough();
    // Llamamos nuestra funcion listarUsuarios
    component.listarUsuarios();

    // Verificamos que nuestro Toast Fake sea llamado

    expect(spyToastFake).toHaveBeenCalled();

    // Ahora verificamos para el caso que nuestro servicio un error
    // Ahora ello usaremos returnValue y throwError
    // Creamos nuestro espia que retorna ERROR
    const spyServiceError = spyOn(servicio, 'serviceListarUsuario').and.returnValue(throwError('Esto es un error de serviceListarUsuario'));
    // Llamamos nuestra funcion listarUsuarios
    component.listarUsuarios();

    // Verificamos que llame nuestro servicio error
    expect(spyServiceError).toHaveBeenCalled();
    // Verificamos que llame nuestra funcion salir();
    expect(spySalir).toHaveBeenCalled();
  });


  it('Verificamos la funcion discriminarUsuarioSixbio()', () => {
    // Mockeamos los valores para nuestra constante   Constant.ROLES_AGRUPADORES_VALOR
    Constant.ROLES_AGRUPADORES_VALOR = ['sixbioweb_adm', 'sixbioweb_ver'];
    // Mockeamos valores para listarUsuarios
    component.listaUsuarios = [
          {
            'apPaterno': 'Palomino',
            'apMaterno': 'Astupuma',
            'nombre': 'Katherine',
            'tipoDoc': 'DNI',
            'numDoc': '45896328',
            'telef': '',
            'correo': 'kpalomino@novatronic.com',
            'fecNac': '03/07/1990',
            'usuario': 'kpalomino',
            'estado': 'INHABILITADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '13/05/2020',
            'audFecCreac': '17/07/2019',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Nuflo',
            'apMaterno': 'Gamarra',
            'nombre': 'Isaac',
            'tipoDoc': 'DNI',
            'numDoc': '75328398',
            'telef': '',
            'correo': 'inuflo@novatronic.com',
            'fecNac': '21/06/1997',
            'usuario': 'FDOperador',
            'estado': 'INHABILITADO',
            'rol': 'Operadorfirmdig',
            'usuarioLogueado': null,
            'audFecModif': '14/01/2020',
            'audFecCreac': '27/03/2019',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Rivas',
            'apMaterno': 'Estrada',
            'nombre': 'Manuel',
            'tipoDoc': 'DNI',
            'numDoc': '41836380',
            'telef': '992783250',
            'correo': 'empresamanuel@gmail.com',
            'fecNac': '08/11/2019',
            'usuario': 'mrivas2',
            'estado': 'HABILITADO',
            'rol': 'rolagrupadorconsultor',
            'usuarioLogueado': null,
            'audFecModif': '04/03/2020',
            'audFecCreac': '04/03/2020',
            'audUsuModif': 'mrivas2',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'JoelPRueba22',
            'apMaterno': 'JoelPRueba22fwefwfwfwf',
            'nombre': 'JoelPRueba2255',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '01/07/2020',
            'usuario': 'JoelPRueba22',
            'estado': 'RESETEADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '29/03/2021',
            'audFecCreac': '13/07/2020',
            'audUsuModif': 'JoelPRueba22',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'efeefefef3555555555555555555',
            'apMaterno': 'efeefefef3444444444444444444',
            'nombre': 'efeefefef3444444444444444444444444444',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '05/09/2020',
            'usuario': 'efeefefef3',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '03/09/2020',
            'audFecCreac': '03/09/2020',
            'audUsuModif': 'jdelosrios2',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'Bajalqui',
            'apMaterno': 'Carrasco',
            'nombre': 'Edgar Gustavo',
            'tipoDoc': 'DNI',
            'numDoc': '12345678',
            'telef': '965773387',
            'correo': 'edgar.bajalqui@gmail.com',
            'fecNac': '29/12/1989',
            'usuario': 'bioadmin',
            'estado': 'IMPORTADO',
            'rol': 'RolAgrpSIXBIO',
            'usuarioLogueado': null,
            'audFecModif': '22/07/2019',
            'audFecCreac': '10/04/2019',
            'audUsuModif': '',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Benito',
            'apMaterno': 'Torres',
            'nombre': 'Fazio44',
            'tipoDoc': 'DNI',
            'numDoc': '70652784',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '30/01/1995',
            'usuario': 'abenito4',
            'estado': 'HABILITADO',
            'rol': 'AGRUPADOR_ADM',
            'usuarioLogueado': null,
            'audFecModif': '24/02/2021',
            'audFecCreac': '09/06/2020',
            'audUsuModif': 'abenito4',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'De los rios5',
            'apMaterno': 'Guimack',
            'nombre': 'Joel Prueba Edicion7',
            'tipoDoc': 'DNI',
            'numDoc': '10126886',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '20/05/1994',
            'usuario': 'jdelosrios2',
            'estado': 'INHABILITADO',
            'rol': 'ROL_ADM',
            'usuarioLogueado': null,
            'audFecModif': '24/10/2020',
            'audFecCreac': '23/01/2020',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'scaCaidoLevantado',
            'apMaterno': 'scaCaidoLevantado',
            'nombre': 'scaCaidoLevantado',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '01/08/2020',
            'usuario': 'scaCaidoLevantado',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '10/08/2020',
            'audUsuModif': '',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'Villegas',
            'apMaterno': 'Diaz',
            'nombre': 'Juan Diego',
            'tipoDoc': 'DNI',
            'numDoc': '12842823',
            'telef': '',
            'correo': 'jvillegas@novatronic.com',
            'fecNac': '02/02/2021',
            'usuario': 'jvillegas3',
            'estado': 'INHABILITADO',
            'rol': 'AGRUPADOR_PROCESADOR',
            'usuarioLogueado': null,
            'audFecModif': '12/03/2021',
            'audFecCreac': '04/02/2021',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'jvillegas'
          },
          {
            'apPaterno': 'Benito5',
            'apMaterno': 'Torres5',
            'nombre': 'Fazio5',
            'tipoDoc': 'DNI',
            'numDoc': '85474584',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '01/02/2021',
            'usuario': 'fbenito5',
            'estado': 'HABILITADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '16/03/2021',
            'audFecCreac': '22/02/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'abenito5'
          },
          {
            'apPaterno': 'khjkhjkhjkhj',
            'apMaterno': 'khjkhjk',
            'nombre': 'hkjhkhj',
            'tipoDoc': 'DNI',
            'numDoc': '99999999',
            'telef': '546546546',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '18/03/2021',
            'usuario': 'jburgos5',
            'estado': 'HABILITADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '30/03/2021',
            'audFecCreac': '05/03/2021',
            'audUsuModif': 'jburgos5',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'Martinez',
            'apMaterno': 'Linares',
            'nombre': 'Marco',
            'tipoDoc': 'DNI',
            'numDoc': '42424242',
            'telef': '999999999',
            'correo': 'mmartinez_prov@novatronic.com',
            'fecNac': '13/06/2019',
            'usuario': 'mmartinez',
            'estado': 'INHABILITADO',
            'rol': 'rol_adm_sms',
            'usuarioLogueado': null,
            'audFecModif': '14/01/2020',
            'audFecCreac': '05/06/2019',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Nuflo',
            'apMaterno': 'Gamarra',
            'nombre': 'isaac',
            'tipoDoc': 'DNI',
            'numDoc': '75328398',
            'telef': '',
            'correo': 'inuflo@novatronic.com',
            'fecNac': '21/06/1997',
            'usuario': 'inuflo1',
            'estado': 'INHABILITADO',
            'rol': 'rol_adm_sms',
            'usuarioLogueado': null,
            'audFecModif': '14/01/2020',
            'audFecCreac': '15/11/2019',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Chunga',
            'apMaterno': 'Chapilliquen',
            'nombre': 'Jose',
            'tipoDoc': 'DNI',
            'numDoc': '71487247',
            'telef': '999999999',
            'correo': 'djimenez@novatronic.com',
            'fecNac': '01/11/2019',
            'usuario': 'jchunga',
            'estado': 'INHABILITADO',
            'rol': 'stiAdmin',
            'usuarioLogueado': null,
            'audFecModif': '16/12/2020',
            'audFecCreac': '26/11/2019',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Nuflo',
            'apMaterno': 'Gamarra',
            'nombre': 'Isaac',
            'tipoDoc': 'DNI',
            'numDoc': '75328398',
            'telef': '',
            'correo': 'inuflo@novatronic.com',
            'fecNac': '21/06/1997',
            'usuario': 'FDProcesador',
            'estado': 'INHABILITADO',
            'rol': 'adminFDig',
            'usuarioLogueado': null,
            'audFecModif': '14/01/2020',
            'audFecCreac': '27/03/2019',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Alcala',
            'apMaterno': 'Marcos',
            'nombre': 'Giancarlo',
            'tipoDoc': 'DNI',
            'numDoc': '42728713',
            'telef': '',
            'correo': 'evaldivia@novatronic.com',
            'fecNac': '03/10/1984',
            'usuario': 'galcala',
            'estado': 'HABILITADO',
            'rol': 'ROL_ADM',
            'usuarioLogueado': null,
            'audFecModif': '30/12/2020',
            'audFecCreac': '15/07/2019',
            'audUsuModif': 'galcala',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Alcala',
            'apMaterno': 'Marcos',
            'nombre': 'Giancarlo',
            'tipoDoc': 'DNI',
            'numDoc': '12345678',
            'telef': '',
            'correo': 'galcala@novatronic.com',
            'fecNac': '01/01/2000',
            'usuario': 'adminBio',
            'estado': 'INHABILITADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '21/01/2020',
            'audFecCreac': '15/07/2019',
            'audUsuModif': 'evaldivia',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Salazar',
            'apMaterno': 'Llactahuaman',
            'nombre': 'Gerry',
            'tipoDoc': 'DNI',
            'numDoc': '73383637',
            'telef': '',
            'correo': 'gsalazar@novatronic.com',
            'fecNac': '01/01/2000',
            'usuario': 'gsalazar',
            'estado': 'INHABILITADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '06/04/2020',
            'audFecCreac': '15/07/2019',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'DIONISIO',
            'apMaterno': 'TRIVENIO',
            'nombre': 'CHRISTIAN',
            'tipoDoc': 'DNI',
            'numDoc': '98546521',
            'telef': '950565189',
            'correo': 'cdionisio@novatronic.com',
            'fecNac': '29/11/1994',
            'usuario': '74854354',
            'estado': 'IMPORTADO',
            'rol': 'stiAdmin',
            'usuarioLogueado': null,
            'audFecModif': '13/12/2019',
            'audFecCreac': '13/12/2019',
            'audUsuModif': 'admin',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Benito',
            'apMaterno': 'Torres',
            'nombre': 'Fazio',
            'tipoDoc': 'DNI',
            'numDoc': '70652784',
            'telef': '333333333',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '30/01/1995',
            'usuario': 'abenito',
            'estado': 'HABILITADO',
            'rol': 'sixbioweb_adm',
            'usuarioLogueado': null,
            'audFecModif': '30/03/2021',
            'audFecCreac': '28/01/2020',
            'audUsuModif': 'abenito',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Mendoza',
            'apMaterno': 'Rios',
            'nombre': 'Miguel',
            'tipoDoc': 'DNI',
            'numDoc': '88888888',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '04/01/2020',
            'usuario': 'mrioss',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '30/01/2020',
            'audUsuModif': '',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Jimenez',
            'apMaterno': 'Huaman',
            'nombre': 'Dennys',
            'tipoDoc': 'DNI',
            'numDoc': '12354678',
            'telef': '12345678',
            'correo': 'djimenez@novatronic.com',
            'fecNac': '10/02/2020',
            'usuario': 'djimenezSTI',
            'estado': 'RESETEADO',
            'rol': 'stiAdmin',
            'usuarioLogueado': null,
            'audFecModif': '05/03/2020',
            'audFecCreac': '17/02/2020',
            'audUsuModif': 'admin',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'fbenito6',
            'apMaterno': 'fbenito6',
            'nombre': 'fbenito6',
            'tipoDoc': 'DNI',
            'numDoc': '85748574',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '01/02/2021',
            'usuario': 'fbenito6',
            'estado': 'HABILITADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '15/03/2021',
            'audFecCreac': '23/02/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'fbenito5'
          },
          {
            'apPaterno': 'Rivas',
            'apMaterno': 'Estrada',
            'nombre': 'Manuel',
            'tipoDoc': 'DNI',
            'numDoc': '41836380',
            'telef': '992783250',
            'correo': 'empresamanuel@gmail.com',
            'fecNac': '07/02/1983',
            'usuario': 'mrivas',
            'estado': 'INHABILITADO',
            'rol': 'rolagrupadorconsultor',
            'usuarioLogueado': null,
            'audFecModif': '04/04/2020',
            'audFecCreac': '28/02/2020',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Valdivia',
            'apMaterno': 'Noa',
            'nombre': 'Eric',
            'tipoDoc': 'DNI',
            'numDoc': '72890317',
            'telef': '999999999',
            'correo': 'evaldivia@novatronic.com',
            'fecNac': '25/09/1992',
            'usuario': 'evaldivia2',
            'estado': 'RESETEADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '25/05/2020',
            'audFecCreac': '14/05/2020',
            'audUsuModif': 'evaldivia2',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'abenito9',
            'apMaterno': 'abenito9',
            'nombre': 'abenito9',
            'tipoDoc': 'DNI',
            'numDoc': '77777777',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '02/02/2021',
            'usuario': 'abenito9',
            'estado': 'HABILITADO',
            'rol': 'ROL_ADM',
            'usuarioLogueado': null,
            'audFecModif': '25/02/2021',
            'audFecCreac': '25/02/2021',
            'audUsuModif': 'abenito9',
            'audUsuCreac': 'abenito5'
          },
          {
            'apPaterno': 'Alcala',
            'apMaterno': 'Marcos',
            'nombre': 'Giancarlo',
            'tipoDoc': 'DNI',
            'numDoc': '42728713',
            'telef': '',
            'correo': 'galcala@novatronic.com',
            'fecNac': '14/06/1996',
            'usuario': 'usuarioPrueba',
            'estado': 'INHABILITADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '31/07/2020',
            'audFecCreac': '30/06/2020',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'Benito',
            'apMaterno': 'Torres',
            'nombre': 'Fazio',
            'tipoDoc': 'DNI',
            'numDoc': '70652784',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '14/03/1996',
            'usuario': 'abenitoprueba4',
            'estado': 'RESETEADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '10/07/2020',
            'audFecCreac': '10/07/2020',
            'audUsuModif': 'abenitoprueba4',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'abenito',
            'apMaterno': 'abenito',
            'nombre': 'abenito',
            'tipoDoc': 'DNI',
            'numDoc': '78452524',
            'telef': '',
            'correo': 'abenito@novatronic.com',
            'fecNac': '08/07/2020',
            'usuario': 'Ar',
            'estado': 'RESETEADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '17/07/2020',
            'audFecCreac': '16/07/2020',
            'audUsuModif': 'admin',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'abenito',
            'apMaterno': 'abenito',
            'nombre': 'abenito',
            'tipoDoc': 'DNI',
            'numDoc': '87548748',
            'telef': '',
            'correo': 'abenito@novatronic.com',
            'fecNac': '07/07/2020',
            'usuario': 'Aman',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '16/07/2020',
            'audUsuModif': '',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'abenito',
            'apMaterno': 'abenito',
            'nombre': 'abenito',
            'tipoDoc': 'DNI',
            'numDoc': '74785748',
            'telef': '',
            'correo': 'abenito@novatronic.com',
            'fecNac': '07/07/2020',
            'usuario': 'ds',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '17/07/2020',
            'audUsuModif': '',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'abenito',
            'apMaterno': 'abenito',
            'nombre': 'abenito',
            'tipoDoc': 'DNI',
            'numDoc': '78574857',
            'telef': '',
            'correo': 'abenito@novatronic.com',
            'fecNac': '07/07/2020',
            'usuario': 't12',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '17/07/2020',
            'audUsuModif': '',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'abenito',
            'apMaterno': 'abenito',
            'nombre': 'abenito',
            'tipoDoc': 'DNI',
            'numDoc': '85748748',
            'telef': '',
            'correo': 'abenito@novatronic.com',
            'fecNac': '07/07/2020',
            'usuario': 'testbenito',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '21/07/2020',
            'audUsuModif': '',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'abenito',
            'apMaterno': 'abenito',
            'nombre': 'abenito',
            'tipoDoc': 'DNI',
            'numDoc': '85748574',
            'telef': '474857487',
            'correo': 'abenito@novatronic.com',
            'fecNac': '15/07/2020',
            'usuario': '(@g85',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '23/07/2020',
            'audUsuModif': '',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'abenito3',
            'apMaterno': 'abenito3',
            'nombre': 'abenito3',
            'tipoDoc': 'DNI',
            'numDoc': '74857487',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '02/11/2020',
            'usuario': 'abenito3',
            'estado': 'CREADO',
            'rol': 'RolAdminBioWeb',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '10/11/2020',
            'audUsuModif': '',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'wfwfwf',
            'apMaterno': 'wfwfwfwf',
            'nombre': 'wfwfwfw',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '01/08/2020',
            'usuario': 'wfwfwfwf',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '03/09/2020',
            'audFecCreac': '28/08/2020',
            'audUsuModif': 'jdelosrios2',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'abenito1',
            'apMaterno': 'abenito1',
            'nombre': 'abenito1',
            'tipoDoc': 'DNI',
            'numDoc': '85748574',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '03/11/2020',
            'usuario': 'abenito1',
            'estado': 'RESETEADO',
            'rol': 'ROL_ADM',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '10/11/2020',
            'audUsuModif': 'admin',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'joelPrueba3266',
            'apMaterno': 'joelPrueba32',
            'nombre': 'joelPrueba32',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '920805153',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '12/09/2020',
            'usuario': 'joelPrueba32',
            'estado': 'RESETEADO',
            'rol': 'ROL_ADM',
            'usuarioLogueado': null,
            'audFecModif': '03/09/2020',
            'audFecCreac': '31/08/2020',
            'audUsuModif': 'jdelosrios2',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'jvillegas2',
            'apMaterno': 'jvillegas2',
            'nombre': 'jvillegas2',
            'tipoDoc': 'DNI',
            'numDoc': '44326766',
            'telef': '',
            'correo': 'jvillegas@novatronic.com',
            'fecNac': '05/11/1999',
            'usuario': 'jvillegas2',
            'estado': 'HABILITADO',
            'rol': 'ROL_ADM',
            'usuarioLogueado': null,
            'audFecModif': '31/03/2021',
            'audFecCreac': '26/11/2020',
            'audUsuModif': 'jvillegas2',
            'audUsuCreac': 'jvillegas'
          },
          {
            'apPaterno': 'Burgos',
            'apMaterno': 'Tejada',
            'nombre': 'Jaime',
            'tipoDoc': 'DNI',
            'numDoc': '12345678',
            'telef': '',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '18/01/2021',
            'usuario': 'jburgos',
            'estado': 'INHABILITADO',
            'rol': 'AGRUPADOR_ADM',
            'usuarioLogueado': null,
            'audFecModif': '08/03/2021',
            'audFecCreac': '18/01/2021',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'joeprueba99',
            'apMaterno': 'joeprueba33',
            'nombre': 'joeprueba33',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '999999999',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '02/08/2020',
            'usuario': 'joeprueba34',
            'estado': 'CREADO',
            'rol': 'ROL_ADM',
            'usuarioLogueado': null,
            'audFecModif': '03/09/2020',
            'audFecCreac': '31/08/2020',
            'audUsuModif': 'jdelosrios2',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'joelprueba34',
            'apMaterno': 'joelprueba34www',
            'nombre': 'joelprueba34',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '01/08/2020',
            'usuario': 'joelprueba34',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '31/08/2020',
            'audFecCreac': '31/08/2020',
            'audUsuModif': 'admin',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'joelprueba35',
            'apMaterno': 'joelprueba35',
            'nombre': 'joelprueba35',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '01/08/2020',
            'usuario': 'joelprueba35',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '31/08/2020',
            'audUsuModif': '',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'joelprueba36',
            'apMaterno': 'joelprueba36',
            'nombre': 'joelprueba36',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '07/08/2020',
            'usuario': 'joelprueba36',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '31/08/2020',
            'audUsuModif': '',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'Alcala',
            'apMaterno': 'Marcos',
            'nombre': 'Giancarlo',
            'tipoDoc': 'DNI',
            'numDoc': '44444444',
            'telef': '997021999',
            'correo': 'galcala@novatronic.com',
            'fecNac': '03/05/1987',
            'usuario': 'galcala9',
            'estado': 'INHABILITADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '01/08/2020',
            'audFecCreac': '06/05/2020',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'Valdivia',
            'apMaterno': 'Noa',
            'nombre': 'Eric',
            'tipoDoc': 'DNI',
            'numDoc': '72890317',
            'telef': '',
            'correo': 'evaldivia@novatronic.com',
            'fecNac': '25/09/1992',
            'usuario': 'evaldivia',
            'estado': 'INHABILITADO',
            'rol': 'ROL_ADM',
            'usuarioLogueado': null,
            'audFecModif': '28/02/2021',
            'audFecCreac': '18/05/2020',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'Benito',
            'apMaterno': 'Torres',
            'nombre': 'Fazio',
            'tipoDoc': 'DNI',
            'numDoc': '70652784',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '12/07/1995',
            'usuario': 'abenito5',
            'estado': 'HABILITADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '11/03/2021',
            'audFecCreac': '24/07/2020',
            'audUsuModif': 'abenito5',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'bancognb',
            'apMaterno': 'bancognb',
            'nombre': 'bancognb',
            'tipoDoc': 'DNI',
            'numDoc': '12345678',
            'telef': '',
            'correo': 'jchunga@novatronic.com',
            'fecNac': '01/02/2050',
            'usuario': 'adminsti',
            'estado': 'IMPORTADO',
            'rol': 'adminSTICliente',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '13/11/2020',
            'audUsuModif': '',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'palomino',
            'apMaterno': 'chahua',
            'nombre': 'solange',
            'tipoDoc': 'DNI',
            'numDoc': '74883732',
            'telef': '967770360',
            'correo': 'spalomino@novatronic.com',
            'fecNac': '23/07/1997',
            'usuario': 'solange',
            'estado': 'INHABILITADO',
            'rol': 'rol_adm_sms',
            'usuarioLogueado': null,
            'audFecModif': '26/01/2021',
            'audFecCreac': '19/10/2020',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'test',
            'apMaterno': 'test',
            'nombre': 'Test',
            'tipoDoc': 'RUC',
            'numDoc': '99999999999',
            'telef': '111111111',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '15/03/2003',
            'usuario': 'jburgos3',
            'estado': 'HABILITADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '18/03/2021',
            'audFecCreac': '15/03/2021',
            'audUsuModif': 'jburgos3',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'ffffffffffffffffffffffffffffffffffffffffffffffffff',
            'apMaterno': 'ffffffffffffffffffffffffffffffffffffffffffffffffff',
            'nombre': 'ffffffffffffffffffffffffffffffffffffffffffffffffff',
            'tipoDoc': 'RUC',
            'numDoc': '99999999999',
            'telef': '111111111',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '13/03/2003',
            'usuario': 'TestUnitffffffffffff',
            'estado': 'RESETEADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '29/03/2021',
            'audFecCreac': '15/03/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'TestingUnit',
            'apMaterno': 'Unit',
            'nombre': 'Testing',
            'tipoDoc': 'DNI',
            'numDoc': '55555555',
            'telef': '',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '01/03/2003',
            'usuario': 'TestingUnitTest',
            'estado': 'CREADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '29/03/2021',
            'audFecCreac': '29/03/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'DFGFDGFD',
            'apMaterno': 'GFDGDFGFD',
            'nombre': 'SDFSDFSD',
            'tipoDoc': 'RUC',
            'numDoc': '99999999999',
            'telef': '',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '03/03/2003',
            'usuario': 'NewTest',
            'estado': 'CREADO',
            'rol': 'sixbioweb_adm',
            'usuarioLogueado': null,
            'audFecModif': '29/03/2021',
            'audFecCreac': '29/03/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'Date',
            'apMaterno': 'Date',
            'nombre': 'Date',
            'tipoDoc': 'DNI',
            'numDoc': '99999999',
            'telef': '333333333',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '29/03/2003',
            'usuario': 'TestNewDate',
            'estado': 'CREADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '29/03/2021',
            'audFecCreac': '29/03/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'ccc',
            'apMaterno': 'cccc',
            'nombre': 'sdfsd',
            'tipoDoc': 'DNI',
            'numDoc': '99999999',
            'telef': '',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '06/03/2003',
            'usuario': 'TestFormsdfsd',
            'estado': 'CREADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '29/03/2021',
            'audFecCreac': '29/03/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'dsfds',
            'apMaterno': 'dsfdsf',
            'nombre': 'sdfsd',
            'tipoDoc': 'DNI',
            'numDoc': '99999999',
            'telef': '',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '29/03/2003',
            'usuario': 'Testinsdf',
            'estado': 'CREADO',
            'rol': 'sixbioweb_adm',
            'usuarioLogueado': null,
            'audFecModif': '29/03/2021',
            'audFecCreac': '29/03/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'Benito',
            'apMaterno': 'Torres',
            'nombre': 'Fazio',
            'tipoDoc': 'DNI',
            'numDoc': '70652784',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '30/01/1995',
            'usuario': 'fbenito2',
            'estado': 'INHABILITADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '25/01/2021',
            'audFecCreac': '25/05/2020',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'Benito',
            'apMaterno': 'Torres',
            'nombre': 'Fazio',
            'tipoDoc': 'DNI',
            'numDoc': '70652784',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '09/05/1995',
            'usuario': 'fbenito3',
            'estado': 'HABILITADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '25/05/2020',
            'audFecCreac': '25/05/2020',
            'audUsuModif': 'fbenito3',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'Meza',
            'apMaterno': '0000000',
            'nombre': 'Gustavo',
            'tipoDoc': 'DNI',
            'numDoc': '12345678',
            'telef': '',
            'correo': 'gmeza@novatronic.com',
            'fecNac': '06/03/2021',
            'usuario': 'gmeza5',
            'estado': 'CREADO',
            'rol': 'sixbioweb_adm',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '19/03/2021',
            'audUsuModif': '',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Meza',
            'apMaterno': '0000000',
            'nombre': 'Gustavo',
            'tipoDoc': 'DNI',
            'numDoc': '99999999',
            'telef': '',
            'correo': 'gmeza@novatronic.com',
            'fecNac': '03/02/2003',
            'usuario': 'gmeza3',
            'estado': 'CREADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '19/03/2021',
            'audUsuModif': '',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'asdasdasdasd',
            'apMaterno': 'asdasdas',
            'nombre': 'rasdasdasd',
            'tipoDoc': 'DNI',
            'numDoc': '12345678',
            'telef': '232432432',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '07/03/2021',
            'usuario': 'Angular9',
            'estado': 'RESETEADO',
            'rol': 'sixbioweb_ver',
            'usuarioLogueado': null,
            'audFecModif': '25/03/2021',
            'audFecCreac': '10/03/2021',
            'audUsuModif': 'Angular9',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'sdafsdfsdf',
            'apMaterno': 'sdfsdfsdfsd',
            'nombre': 'asdfsdfgsd',
            'tipoDoc': 'RUC',
            'numDoc': '99999999999',
            'telef': '234444444',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '19/03/2003',
            'usuario': 'Primerlogin',
            'estado': 'RESETEADO',
            'rol': 'sixbioweb_adm',
            'usuarioLogueado': null,
            'audFecModif': '29/03/2021',
            'audFecCreac': '23/03/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'sdfsdfds',
            'apMaterno': '3432',
            'nombre': 'saf',
            'tipoDoc': 'RUC',
            'numDoc': '99999999999',
            'telef': '111111111',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '03/05/2003',
            'usuario': 'fdgdfsdxxdsfds',
            'estado': 'RESETEADO',
            'rol': 'sixbioweb_adm',
            'usuarioLogueado': null,
            'audFecModif': '29/03/2021',
            'audFecCreac': '25/03/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'jburgos6'
          },
          {
            'apPaterno': 'Benito',
            'apMaterno': 'Torres',
            'nombre': 'Fazio',
            'tipoDoc': 'DNI',
            'numDoc': '70652784',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '30/01/1995',
            'usuario': 'fbenito',
            'estado': 'INHABILITADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '28/02/2021',
            'audFecCreac': '19/05/2020',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'villegas',
            'apMaterno': 'diaz',
            'nombre': 'juan diego',
            'tipoDoc': 'DNI',
            'numDoc': '88888888',
            'telef': '',
            'correo': 'jvillegas@novatronic.com',
            'fecNac': '01/09/2020',
            'usuario': 'jvillegas',
            'estado': 'HABILITADO',
            'rol': 'AGRUPADOR_ADM',
            'usuarioLogueado': null,
            'audFecModif': '23/03/2021',
            'audFecCreac': '15/09/2020',
            'audUsuModif': 'jvillegas',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'stefantu',
            'apMaterno': 'stefantu',
            'nombre': 'stefantu',
            'tipoDoc': 'DNI',
            'numDoc': '71957130',
            'telef': '941886550',
            'correo': 'ssolari@novatronic.com',
            'fecNac': '18/08/1984',
            'usuario': 'stefantu',
            'estado': 'INHABILITADO',
            'rol': 'rol_adm_sms',
            'usuarioLogueado': null,
            'audFecModif': '02/12/2020',
            'audFecCreac': '16/10/2020',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Burgos',
            'apMaterno': 'Tejada',
            'nombre': 'Jaime',
            'tipoDoc': 'DNI',
            'numDoc': '12345678',
            'telef': '',
            'correo': 'jburgos@novatronic.com',
            'fecNac': '25/02/2021',
            'usuario': 'jburgos6',
            'estado': 'HABILITADO',
            'rol': 'sixbioweb_adm',
            'usuarioLogueado': null,
            'audFecModif': '30/03/2021',
            'audFecCreac': '23/02/2021',
            'audUsuModif': 'jburgos6',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Six',
            'apMaterno': 'Bio',
            'nombre': 'Administrador',
            'tipoDoc': 'DNI',
            'numDoc': '88888888',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '23/04/2020',
            'usuario': 'administradorBio',
            'estado': 'INHABILITADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '14/06/2020',
            'audFecCreac': '23/04/2020',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'fbenitoprocesador',
            'apMaterno': 'fbenitoprocesador',
            'nombre': 'fbenitoprocesador',
            'tipoDoc': 'DNI',
            'numDoc': '82547414',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '03/02/2021',
            'usuario': 'fbenitoproc',
            'estado': 'INHABILITADO',
            'rol': 'AGRUPADOR_OPERADOR',
            'usuarioLogueado': null,
            'audFecModif': '06/03/2021',
            'audFecCreac': '02/02/2021',
            'audUsuModif': 'ADMIN',
            'audUsuCreac': 'admin'
          },
          {
            'apPaterno': 'Benito',
            'apMaterno': 'Torres',
            'nombre': 'Fazio',
            'tipoDoc': 'DNI',
            'numDoc': '70652784',
            'telef': '',
            'correo': 'fbenito@novatronic.com',
            'fecNac': '04/09/2019',
            'usuario': 'abenitoprueba',
            'estado': 'RESETEADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '10/07/2020',
            'audFecCreac': '10/07/2020',
            'audUsuModif': 'abenitoprueba',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'wfwfwfw',
            'apMaterno': 'wfwfwfwf',
            'nombre': 'wfwfwfwf',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '04/09/2020',
            'usuario': 'wwfwfwf',
            'estado': 'CREADO',
            'rol': 'ROL_ADM',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '03/09/2020',
            'audUsuModif': '',
            'audUsuCreac': 'jdelosrios2'
          },
          {
            'apPaterno': 'joelprueba223',
            'apMaterno': 'joelprueba223',
            'nombre': 'joelprueba223',
            'tipoDoc': 'DNI',
            'numDoc': '48287359',
            'telef': '',
            'correo': 'jdelosrios@novatronic.com',
            'fecNac': '02/09/2020',
            'usuario': 'joelprueba223',
            'estado': 'CREADO',
            'rol': 'BIOCoreAgrupador',
            'usuarioLogueado': null,
            'audFecModif': '',
            'audFecCreac': '03/09/2020',
            'audUsuModif': '',
            'audUsuCreac': 'jdelosrios2'
          }
    ];
    const userTest = {
        'apPaterno': 'test',
        'apMaterno': 'test',
        'nombre': 'Test',
        'tipoDoc': 'RUC',
        'numDoc': '99999999999',
        'telef': '111111111',
        'correo': 'jburgos@novatronic.com',
        'fecNac': '15/03/2003',
        'usuario': 'jburgos3',
        'estado': 'HABILITADO',
        'rol': 'sixbioweb_ver',
        'usuarioLogueado': null,
        'audFecModif': '18/03/2021',
        'audFecCreac': '15/03/2021',
        'audUsuModif': 'jburgos3',
        'audUsuCreac': 'jburgos6'
    };
    // Llamamos nuestra funcion
    component.discriminarUsuarioSixbio();
    // Verificamos la longitud de  listaUsuariosSixbio
    // Debe poseer 18 registros segun la data Mockeada 'sixbioweb_adm', 'sixbioweb_ver'
    expect(component.listaUsuariosSixbio.length).toEqual(18);
    // Verificamos que contenga a userTest
    expect(component.listaUsuariosSixbio).toContain(userTest);
  });

  it('Verificamos la funcion setdataRequestReporte()', () => {
  // Mockeamos data para nuestro consultarValidacionesForm
  component.consultarValidacionesForm.controls.cantidad.setValue('123');
  component.consultarValidacionesForm.controls.filterUsuario.setValue('jburgos6');
  // Mockeamos data para listaValidacionesExportar
  const mockListaValidacionesExportar = [
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': null,
          'codigoTransaccion': '301000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '03/03/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '8740580460025774',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': null,
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'INVALIDO',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': null,
          'codigoTransaccion': '301000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '03/03/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '1435356861058524',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': null,
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'INVALIDO',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': null,
          'codigoTransaccion': '301000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '03/03/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '0631463202555101',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': null,
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'INVALIDO',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': null,
          'codigoTransaccion': '301000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '03/03/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '0441148302662070',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': null,
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'INVALIDO',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': null,
          'codigoTransaccion': '301000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '13/02/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '6304808127482730',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': null,
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'INVALIDO',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': null,
          'codigoTransaccion': '301000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '10/02/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '0824278678231766',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': null,
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'INVALIDO',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'MAMANI',
          'apellidoPaterno': 'SMITH',
          'codigoRespuesta': null,
          'codigoTransaccion': '310000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '10/02/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '8856425848271707',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': null,
          'nombres': 'GERRY CHERRY TERRY',
          'numeroDocumento': '7777777777777',
          'origenRespuesta': 'INVALIDO',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'PASAPORTE',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'MAMANI',
          'apellidoPaterno': 'SMITH',
          'codigoRespuesta': '00000',
          'codigoTransaccion': '310000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '10/02/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '4523838724174847',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': 'SI CORRESPONDE',
          'nombres': 'GERRY CHERRY TERRY',
          'numeroDocumento': '7777777777777',
          'origenRespuesta': 'NOVATRONIC',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'PASAPORTE',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'MAMANI',
          'apellidoPaterno': 'SMITH',
          'codigoRespuesta': '00000',
          'codigoTransaccion': '310000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '10/02/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '1850878025236177',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': 'SI CORRESPONDE',
          'nombres': 'GERRY CHERRY TERRY',
          'numeroDocumento': '7777777777777',
          'origenRespuesta': 'NOVATRONIC',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'PASAPORTE',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': '00000',
          'codigoTransaccion': '301001',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '10/02/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '1973226176994878',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': 'SI CORRESPONDE',
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'NOVATRONIC',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': '10000',
          'codigoTransaccion': '301001',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '10/02/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '2318016121671603',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': 'NO CORRESPONDE',
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'NOVATRONIC',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': '00000',
          'codigoTransaccion': '301001',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '10/02/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '2453856751465534',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': 'SI CORRESPONDE',
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'NOVATRONIC',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'MAMANI',
          'apellidoPaterno': 'SMITH',
          'codigoRespuesta': '00000',
          'codigoTransaccion': '310000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '10/02/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '3158671148180673',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': 'SI CORRESPONDE',
          'nombres': 'GERRY CHERRY TERRY',
          'numeroDocumento': '7777777777777',
          'origenRespuesta': 'NOVATRONIC',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'PASAPORTE',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      }
   ];
  component.listaValidacionesExportar = mockListaValidacionesExportar;
  // Espiamos nuestra funcion setDataFechaBusquedaExportar
  const spySetDataRequestReporte = spyOn(component, 'setDataFechaBusquedaExportar').and.callThrough();
  // Llamamos nuestra funcion
  component.setdataRequestReporte();
  // verificamos que se llame nuestra funcion setDataFechaBusquedaExportar
  expect(spySetDataRequestReporte).toHaveBeenCalled();
  // Verificamos las asignaciones que realiza nuestra funcion
  expect(component.dataRequestReporte.listaValidaciones).toEqual(mockListaValidacionesExportar);
  expect(component.dataRequestReporte.f_usuario ).toEqual('jburgos6');
  expect(component.dataRequestReporte.f_cantidad ).toEqual('123');
  });

  it('Verificamos la funcion setTipoReporte()', () => {
    // Llamamos nuestra funcion y le pasamos sus parametros
    component.setTipoReporte('excel');
    // Verificamos que la asignacion a this.dataRequestReporte.tipoReporte
    expect(component.dataRequestReporte.tipoReporte).toEqual('excel');
  });

  it('Verficamos la funcion setCamposGrilla()', () => {
    // Mockeamos data para listaValidaciones
    const mockListaValidaciones = [
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': null,
          'codigoTransaccion': '301000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '03/03/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '0631463202555101',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': null,
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'INVALIDO',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': null,
          'codigoTransaccion': '301000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '03/03/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '8740580460025774',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': null,
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'INVALIDO',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': null,
          'codigoTransaccion': '301000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '03/03/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '1435356861058524',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': null,
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'INVALIDO',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': null,
          'codigoTransaccion': '301000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '03/03/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '0441148302662070',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': null,
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'INVALIDO',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': null,
          'codigoTransaccion': '301000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '13/02/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '6304808127482730',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': null,
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'INVALIDO',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'MAMANI',
          'apellidoPaterno': 'SMITH',
          'codigoRespuesta': '00000',
          'codigoTransaccion': '310000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '10/02/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '3158671148180673',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': 'SI CORRESPONDE',
          'nombres': 'GERRY CHERRY TERRY',
          'numeroDocumento': '7777777777777',
          'origenRespuesta': 'NOVATRONIC',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'PASAPORTE',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'MAMANI',
          'apellidoPaterno': 'SMITH',
          'codigoRespuesta': '00000',
          'codigoTransaccion': '310000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '10/02/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '4523838724174847',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': 'SI CORRESPONDE',
          'nombres': 'GERRY CHERRY TERRY',
          'numeroDocumento': '7777777777777',
          'origenRespuesta': 'NOVATRONIC',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'PASAPORTE',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'MAMANI',
          'apellidoPaterno': 'SMITH',
          'codigoRespuesta': '00000',
          'codigoTransaccion': '310000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '10/02/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '1850878025236177',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': 'SI CORRESPONDE',
          'nombres': 'GERRY CHERRY TERRY',
          'numeroDocumento': '7777777777777',
          'origenRespuesta': 'NOVATRONIC',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'PASAPORTE',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': '00000',
          'codigoTransaccion': '301001',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '10/02/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '2453856751465534',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': 'SI CORRESPONDE',
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'NOVATRONIC',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': '10000',
          'codigoTransaccion': '301001',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '10/02/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '2318016121671603',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': 'NO CORRESPONDE',
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'NOVATRONIC',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      }
    ];
    component.listaValidaciones = mockListaValidaciones;
    // Espiamos la funcion setTipoVerificacion
    const spysetTipoVerificacion = spyOn(component, 'setTipoVerificacion').and.callThrough();
    const spysetMensajeRespuesta = spyOn(component, 'setMensajeRespuesta').and.callThrough();
    const spysetOrigenRespuesta = spyOn(component, 'setOrigenRespuesta').and.callThrough();
    // Llamamos nuestra funcion
    component.setCamposGrilla();
    // Verificamos que llame nuestras funciones espiadas
    expect(spysetTipoVerificacion).toHaveBeenCalled();
    expect(spysetMensajeRespuesta).toHaveBeenCalled();
    expect(spysetOrigenRespuesta).toHaveBeenCalled();

  });


  it('Verficamos la funcion setCamposGrillaExportar()', () => {
    // Mockeamos data para listaValidacionesExportar
    const mockListaValidacionesExportar = [
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': null,
          'codigoTransaccion': '301000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '03/03/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '0631463202555101',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': null,
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'INVALIDO',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': null,
          'codigoTransaccion': '301000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '03/03/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '8740580460025774',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': null,
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'INVALIDO',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': null,
          'codigoTransaccion': '301000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '03/03/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '1435356861058524',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': null,
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'INVALIDO',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': null,
          'codigoTransaccion': '301000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '03/03/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '0441148302662070',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': null,
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'INVALIDO',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': null,
          'codigoTransaccion': '301000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '13/02/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '6304808127482730',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': null,
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'INVALIDO',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'MAMANI',
          'apellidoPaterno': 'SMITH',
          'codigoRespuesta': '00000',
          'codigoTransaccion': '310000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '10/02/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '3158671148180673',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': 'SI CORRESPONDE',
          'nombres': 'GERRY CHERRY TERRY',
          'numeroDocumento': '7777777777777',
          'origenRespuesta': 'NOVATRONIC',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'PASAPORTE',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'MAMANI',
          'apellidoPaterno': 'SMITH',
          'codigoRespuesta': '00000',
          'codigoTransaccion': '310000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '10/02/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '4523838724174847',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': 'SI CORRESPONDE',
          'nombres': 'GERRY CHERRY TERRY',
          'numeroDocumento': '7777777777777',
          'origenRespuesta': 'NOVATRONIC',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'PASAPORTE',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'MAMANI',
          'apellidoPaterno': 'SMITH',
          'codigoRespuesta': '00000',
          'codigoTransaccion': '310000',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '10/02/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '1850878025236177',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': 'SI CORRESPONDE',
          'nombres': 'GERRY CHERRY TERRY',
          'numeroDocumento': '7777777777777',
          'origenRespuesta': 'NOVATRONIC',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'PASAPORTE',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': '00000',
          'codigoTransaccion': '301001',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '10/02/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '2453856751465534',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': 'SI CORRESPONDE',
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'NOVATRONIC',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      },
      {
          'apellidoMaterno': 'CARRASCO',
          'apellidoPaterno': 'BAJALQUI',
          'codigoRespuesta': '10000',
          'codigoTransaccion': '301001',
          'fechaCaducidad': '19-12-2022',
          'fechaEmision': '01-02-2016',
          'fechaNacimiento': '29-12-1991',
          'fechaValidacion': '10/02/2020',
          'horaProceso': '00:00:00',
          'idValidacion': '2318016121671603',
          'latitudLocalizacion': '-77.010710',
          'longitudLocalizacion': '-12.107029',
          'mensajeRespuesta': 'NO CORRESPONDE',
          'nombres': 'EDGAR GUSTAVO',
          'numeroDocumento': '88888888',
          'origenRespuesta': 'NOVATRONIC',
          'sexo': 'MASCULINO',
          'sistemaLocalizacion': 'PSISTGEO001',
          'tipoDocumento': 'DNI',
          'usuario': 'abenito',
          'tipoVerificacion': 'T33'
      }
    ];
    component.listaValidacionesExportar = mockListaValidacionesExportar;
    // Espiamos la funcion setTipoVerificacion
    const spysetTipoVerificacion = spyOn(component, 'setTipoVerificacion').and.callThrough();
    const spysetMensajeRespuesta = spyOn(component, 'setMensajeRespuesta').and.callThrough();
    const spysetOrigenRespuesta = spyOn(component, 'setOrigenRespuesta').and.callThrough();
    // Llamamos nuestra funcion
    component.setCamposGrillaExportar();
    // Verificamos que llame nuestras funciones espiadas
    expect(spysetTipoVerificacion).toHaveBeenCalled();
    expect(spysetMensajeRespuesta).toHaveBeenCalled();
    expect(spysetOrigenRespuesta).toHaveBeenCalled();
  });


  it('Verificamos la funcion setTipoVerificacion(validacion)', () => {
    // Esta funcion evalua con un Switch los valores de validacion.codigoTransaccion
    // Probaremos la primera condicion cuando validacion.codigoTransaccion sea Constant.VERI_DACTILAR_T33_MINUCIAS
    const mockValidacion = {
      codigoTransaccion: Constant.VERI_DACTILAR_T33_MINUCIAS,
      tipoVerificacion: ''
    };
    // Llamamos nuestra funcion ;
    component.setTipoVerificacion(mockValidacion);
    // Verificamos que asigne a tipoVerificacion Constant.T33
    expect(mockValidacion.tipoVerificacion).toEqual(Constant.T33);

    // Verficamos la siguiente condicion cuando codigoTransaccion sea Constant.VERI_DACTILAR_T33_WSQ
    mockValidacion.codigoTransaccion = Constant.VERI_DACTILAR_T33_WSQ;
    // Llamamos nuestra funcion ;
    component.setTipoVerificacion(mockValidacion);
    // Verificamos que asigne a tipoVerificacion Constant.T33
    expect(mockValidacion.tipoVerificacion).toEqual(Constant.T33);

    // Verficamos la siguiente condicion cuando codigoTransaccion sea Constant.VERI_DACTILAR_T35_WSQ
    mockValidacion.codigoTransaccion = Constant.VERI_DACTILAR_T35_WSQ;
    // Llamamos nuestra funcion ;
    component.setTipoVerificacion(mockValidacion);
    // Verificamos que asigne a tipoVerificacion Constant.T35
    expect(mockValidacion.tipoVerificacion).toEqual(Constant.T35);

    // Verficamos la siguiente condicion cuando codigoTransaccion sea Constant.VERI_EXTRANJERO
    mockValidacion.codigoTransaccion = Constant.VERI_EXTRANJERO;
    // Llamamos nuestra funcion ;
    component.setTipoVerificacion(mockValidacion);
    // Verificamos que asigne a tipoVerificacion Constant.T33
    expect(mockValidacion.tipoVerificacion).toEqual(Constant.T33);

    // Verficamos la siguiente condicion cuando codigoTransaccion sea Constant.VERI_FACIAL
    mockValidacion.codigoTransaccion = Constant.VERI_FACIAL;
    // Llamamos nuestra funcion ;
    component.setTipoVerificacion(mockValidacion);
    // Verificamos que asigne a tipoVerificacion Constant.T37
    expect(mockValidacion.tipoVerificacion).toEqual(Constant.T37);

    // Verficamos la siguiente condicion cuando codigoTransaccion NO sea niguno de los Anteriores
    mockValidacion.codigoTransaccion = 'Anything';
    // Llamamos nuestra funcion ;
    component.setTipoVerificacion(mockValidacion);
    // Verificamos que asigne a tipoVerificacion Constant.T33
    expect(mockValidacion.tipoVerificacion).toEqual(Constant.T33);
  });


  it('Verificamos la funcion setMensajeRespuesta(validacion)', () => {
    // Esta funcion evalua con un Switch los valores de validacion.codigoRespuesta
    // Probaremos la primera condicion cuando validacion.codigoRespuesta sea Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    const mockValidacion = {
      codigoRespuesta: Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO,
      mensajeRespuesta: '',
      fechaValidacion: '10/02/2020 00:00:00'
    };
    // Llamamos nuestra funcion ;
    component.setMensajeRespuesta(mockValidacion);
    // Verificamos que asigne a mensajeRespuesta Constant.SI_CORRESPONDE
    expect(mockValidacion.mensajeRespuesta).toEqual(Constant.SI_CORRESPONDE);

    // Ahora probamos cuando validacion.codigoRespuesta sea Constant.COD_OPERACION_HUELLAS_NO_COINCIDEN
    mockValidacion.codigoRespuesta = Constant.COD_OPERACION_HUELLAS_NO_COINCIDEN;
    // Llamamos nuestra funcion ;
    component.setMensajeRespuesta(mockValidacion);
    // Verificamos que asigne a mensajeRespuesta Constant.NO_CORRESPONDE
    expect(mockValidacion.mensajeRespuesta).toEqual(Constant.NO_CORRESPONDE);

    // Ahora probamos cuando validacion.codigoRespuesta sea Constant.COD_OPERACION_SATISFACTORIA_RENIEC
    mockValidacion.codigoRespuesta = Constant.COD_OPERACION_SATISFACTORIA_RENIEC;
    // Llamamos nuestra funcion ;
    component.setMensajeRespuesta(mockValidacion);
    // Verificamos que asigne a mensajeRespuesta Constant.SI_CORRESPONDE
    expect(mockValidacion.mensajeRespuesta).toEqual(Constant.SI_CORRESPONDE);

    // Ahora probamos cuando validacion.codigoRespuesta sea Constant.COD_OPERACION_NO_CORRESPONDE_RENIEC
    mockValidacion.codigoRespuesta = Constant.COD_OPERACION_NO_CORRESPONDE_RENIEC;
    // Llamamos nuestra funcion ;
    component.setMensajeRespuesta(mockValidacion);
    // Verificamos que asigne a mensajeRespuesta Constant.NO_CORRESPONDE
    expect(mockValidacion.mensajeRespuesta).toEqual(Constant.NO_CORRESPONDE);

    // Ahora probamos cuando validacion.codigoRespuesta sea Constant.COD_OPERACION_SATISFACTORIA_RENIEC_FACIAL
    mockValidacion.codigoRespuesta = Constant.COD_OPERACION_SATISFACTORIA_RENIEC_FACIAL;
    // Llamamos nuestra funcion ;
    component.setMensajeRespuesta(mockValidacion);
    // Verificamos que asigne a mensajeRespuesta Constant.SI_CORRESPONDE
    expect(mockValidacion.mensajeRespuesta).toEqual(Constant.SI_CORRESPONDE);

    // Ahora probamos cuando validacion.codigoRespuesta sea Constant.COD_OPERACION_NO_CORRESPONDE_RENIEC_FACIAL
    mockValidacion.codigoRespuesta = Constant.COD_OPERACION_NO_CORRESPONDE_RENIEC_FACIAL;
    // Llamamos nuestra funcion ;
    component.setMensajeRespuesta(mockValidacion);
    // Verificamos que asigne a mensajeRespuesta Constant.NO_CORRESPONDE
    expect(mockValidacion.mensajeRespuesta).toEqual(Constant.NO_CORRESPONDE);


    // Para cualquiera de los casos verificamos el valor que se obtener al hacer un substring(0,10)
    expect(mockValidacion.fechaValidacion).toEqual(mockValidacion.fechaValidacion.substring(0, 10));
  });

  it('Verificamos la funcion setOrigenRespuesta(validacion: IValidaciones)', () => {
        // Esta funcion evalua con un Switch los valores de validacion.origenRespuesta
    // Probaremos la primera condicion cuando validacion.origenRespuesta sea Constant.COD_ORIGEN_RENIEC
    const mockValidacion = {
      origenRespuesta: Constant.COD_ORIGEN_RENIEC
    };
    // Llamamos nuestra funcion y le pasamos el mockValidacion
    component.setOrigenRespuesta(mockValidacion);
    // Verificamos que asigne a Constant.DESC_ORIGEN_RENIEC a validacion.origenRespuesta
    expect(mockValidacion.origenRespuesta).toEqual(Constant.DESC_ORIGEN_RENIEC);

    // Ahora validamos para el caso COD_ORIGEN_NOVATRONIC
    mockValidacion.origenRespuesta = Constant.COD_ORIGEN_NOVATRONIC;
    // Llamamos nuestra funcion y le pasamos el mockValidacion
    component.setOrigenRespuesta(mockValidacion);
    // Verificamos que asigne a Constant.DESC_ORIGEN_NOVATRONIC a validacion.origenRespuesta
    expect(mockValidacion.origenRespuesta).toEqual(Constant.DESC_ORIGEN_NOVATRONIC);

    // Ahora validamos para el caso COD_ORIGEN_MIGRACIONES
    mockValidacion.origenRespuesta = Constant.COD_ORIGEN_MIGRACIONES;
    // Llamamos nuestra funcion y le pasamos el mockValidacion
    component.setOrigenRespuesta(mockValidacion);
    // Verificamos que asigne a Constant.DESC_ORIGEN_MIGRACIONES a validacion.origenRespuesta
    expect(mockValidacion.origenRespuesta).toEqual(Constant.DESC_ORIGEN_MIGRACIONES);

    // Ahora validamos para el caso COD_ORIGEN_SCA
    mockValidacion.origenRespuesta = Constant.COD_ORIGEN_SCA;
    // Llamamos nuestra funcion y le pasamos el mockValidacion
    component.setOrigenRespuesta(mockValidacion);
    // Verificamos que asigne a Constant.DESC_ORIGEN_SCA a validacion.origenRespuesta
    expect(mockValidacion.origenRespuesta).toEqual(Constant.DESC_ORIGEN_SCA);

    // Ahora validamos para el caso default
    mockValidacion.origenRespuesta = 'Anything';
    // Llamamos nuestra funcion y le pasamos el mockValidacion
    component.setOrigenRespuesta(mockValidacion);
    // Verificamos que asigne a Constant.DESC_ORIGEN_SCA a validacion.origenRespuesta
     expect(mockValidacion.origenRespuesta).toEqual(Constant.DESC_ORIGEN_DEFAULT);

  });


  it('Verificamos la funcion activarCantidad()', () => {
    // Llamamos nuestra funcion activarCantidad();
    component.activarCantidad();
    // Verificamos los valores que sea en el localStorage
    expect(localStorage.getItem('ischeckCantidad')).toEqual('true');
    expect(localStorage.getItem('ischeckFechas')).toEqual('false');
    expect(localStorage.getItem('campoFechaDesde')).toEqual('false');
    expect(localStorage.getItem('campoFechaHasta')).toEqual('false');
    // Verificamos el valor asignado a isFechasDisable
    expect(component.isFechasDisable).toBeTruthy();
    // Verificamos los valores asignados a filterfechaDesde y filterfechaHasta de consultarValidacionesForm
    expect(component.consultarValidacionesForm.getRawValue().filterfechaDesde).toEqual('' );
    expect(component.consultarValidacionesForm.getRawValue().filterfechaHasta).toEqual('' );
    // Verificamos los valores de las listas
    expect(component.listaValidaciones).toEqual([]);
    expect(component.listaValidacionesExportar).toEqual([]);
    expect(component.listarValidacionesOK).toBeFalsy();
    // Verificamos que active el campo Cantidad

    // Nos suscribimos a statusChange de FormControl de nuestro formGroup consultaValidacionesForm , este caso el control filterfechaDesde
    // Verificamos que sea DISABLED
    // Fuente :https://angular.io/api/forms/AbstractControl#statusChanges
    component.consultarValidacionesForm.controls.filterfechaDesde.statusChanges.subscribe( (status) => {
        expect(status).toEqual('DISABLED'); // status will be "VALID", "INVALID", "PENDING" or "DISABLED"
    });
    // Verificamos para filterfechaHasta
    component.consultarValidacionesForm.controls.filterfechaHasta.statusChanges.subscribe( (status) => {
      expect(status).toEqual('DISABLED'); // status will be "VALID", "INVALID", "PENDING" or "DISABLED"
    });
    // Verificamos para cantidad
    component.consultarValidacionesForm.controls.cantidad.statusChanges.subscribe( (status) => {
      // Se encontrara invalido debido que se setea ''
      expect(status).toEqual('INVALID'); // status will be "VALID", "INVALID", "PENDING" or "DISABLED"
      // Verificamos que cantidad tenga vacio
      expect(component.consultarValidacionesForm.getRawValue().cantidad).toEqual('' || undefined);
    });
  });


  it('Verificamos la funcion filterRadioButtonCantidad()', () => {
    // Seteamos ischeckCantidad como 'true' , en localStorage para probar la condicion de la funcion
    localStorage.setItem('ischeckCantidad', 'true');
    // Verificamos que nos retorne true la funcion
    expect(component.filterRadioButtonCantidad()).toBeTruthy();
    // Nos suscribimos a statusChange de FormControl de nuestro formGroup consultaValidacionesForm , este caso el control filterfechaDesde
    // Verificamos que sea DISABLED
    // Fuente :https://angular.io/api/forms/AbstractControl#statusChanges
    component.consultarValidacionesForm.controls.filterfechaDesde.statusChanges.subscribe((status) => {
      expect(status).toEqual('DISABLED'); // status will be "VALID", "INVALID", "PENDING" or "DISABLED"
    });
    // Verificamos para filterfechaHasta
    component.consultarValidacionesForm.controls.filterfechaHasta.statusChanges.subscribe((status) => {
      expect(status).toEqual('DISABLED'); // status will be "VALID", "INVALID", "PENDING" or "DISABLED"
    });
    // Verificamos para cantidad
    component.consultarValidacionesForm.controls.cantidad.statusChanges.subscribe((status) => {
      // Se encontrara invalido debido que se setea ''
      expect(status).toEqual('INVALID'); // status will be "VALID", "INVALID", "PENDING" or "DISABLED"
        // Verificamos que cantidad tenga vacio
    expect(component.consultarValidacionesForm.getRawValue().cantidad).toEqual('' || undefined);
    });

    // Verificamos isFechasDisable sea false
    expect(component.isFechasDisable).toBeTruthy();

    // Ahora verificamos el caso contrario donde no hubiera ischeckCantidad o fuera false
    localStorage.clear();
    expect(component.filterRadioButtonCantidad()).toBeFalsy();
    localStorage.setItem('ischeckCantidad', 'false');
    expect(component.filterRadioButtonCantidad()).toBeFalsy();
  });


  it('Verificamos la funcion activarFechas()', () => {
    // Llamamos nuestra funcion
    component.activarFechas();
    // Verificamos el localStorage , recordar que se guardan los datos como un string
    expect(localStorage.getItem('ischeckFechas')).toEqual('true');
    expect(localStorage.getItem('campoFechaDesde')).toEqual('true');
    expect(localStorage.getItem('campoFechaHasta')).toEqual('true');
    expect(localStorage.getItem('ischeckCantidad')).toEqual('false');
    // Verificamos el valor de isFechasDisable
    expect(component.isFechasDisable).toBeFalsy();
    // Verificamos el valor de las listas
    expect(component.listaValidaciones).toEqual([]);
    expect(component.listaValidacionesExportar).toEqual([]);
    expect(component.listarValidacionesOK).toBeFalsy();

    // Nos suscribimos a statusChange de FormControl de nuestro formGroup consultaValidacionesForm
    // Verificamos que sea DISABLED
    // Fuente :https://angular.io/api/forms/AbstractControl#statusChanges
    // filterfechaDesde y filterfechaHasta seran INVALID ya que no poseeran ningun dato pero estaran habilitados
    component.consultarValidacionesForm.controls.filterfechaDesde.statusChanges.subscribe((status) => {
      expect(status).toEqual('INVALID'); // status will be "VALID", "INVALID", "PENDING" or "DISABLED"
      expect(component.consultarValidacionesForm.getRawValue().filterfechaDesde).toEqual('' || undefined);
    });
    // Verificamos para filterfechaHasta
    component.consultarValidacionesForm.controls.filterfechaHasta.statusChanges.subscribe((status) => {
      expect(status).toEqual('INVALID'); // status will be "VALID", "INVALID", "PENDING" or "DISABLED"
      expect(component.consultarValidacionesForm.getRawValue().filterfechaHasta).toEqual('' || undefined);

    });
    // Verificamos para cantidad
    component.consultarValidacionesForm.controls.cantidad.statusChanges.subscribe((status) => {
      expect(status).toEqual('DISABLED'); // status will be "VALID", "INVALID", "PENDING" or "DISABLED"
    });

  });

  it('Verificamos la funcion filterRadioButtonFechas()', () => {
    // Para probar la primera condicion de nuestra funcion seteamos  ischeckFechas 'true'
    localStorage.setItem('ischeckFechas', 'true');


    // Llamamos nuestra funcion filterRadioButtonFechas() y verificamos que retorne true
    expect(component.filterRadioButtonFechas()).toBeTruthy();
    // verificamos que isFechasDisable sea FALSE
    expect(component.isFechasDisable).toBeFalsy();
    // Nos suscribimos a statusChange de FormControl de nuestro formGroup consultaValidacionesForm
    // Verificamos que sea DISABLED el campo CANTIDAD
    // Fuente :https://angular.io/api/forms/AbstractControl#statusChanges
    // Verificamos para cantidad
    component.consultarValidacionesForm.controls.cantidad.statusChanges.subscribe((status) => {
      expect(status).toEqual('DISABLED'); // status will be "VALID", "INVALID", "PENDING" or "DISABLED"
    });
    // filterfechaDesde y filterfechaHasta seran INVALID ya que no poseeran ningun dato pero estaran habilitados
    component.consultarValidacionesForm.controls.filterfechaDesde.statusChanges.subscribe((status) => {
      expect(status).toEqual('INVALID'); // status will be "VALID", "INVALID", "PENDING" or "DISABLED"
      expect(component.consultarValidacionesForm.getRawValue().filterfechaDesde).toEqual('' || undefined);
    });
    // Verificamos para filterfechaHasta
    component.consultarValidacionesForm.controls.filterfechaHasta.statusChanges.subscribe((status) => {
      expect(status).toEqual('INVALID'); // status will be "VALID", "INVALID", "PENDING" or "DISABLED"
      expect(component.consultarValidacionesForm.getRawValue().filterfechaHasta).toEqual('' || undefined);
    });

    // Ahora verificamos el caso contrario donde no hubiera ischeckCantidad o fuera false
    localStorage.clear();
    expect(component.filterRadioButtonCantidad()).toBeFalsy();
    localStorage.setItem('ischeckFechas', 'false');
    expect(component.filterRadioButtonCantidad()).toBeFalsy();
  });


  it('Verificamos la funcion valorDelRolUsuario()', () => {
    // Para probar la funcion asigamos el valor 'EMP_ADM' a rolUsuario
    component.rolUsuario = 'EMP_ADM';
    // asigamos a usuarioLogueado el valor del localStorage usuarioLogueado
    component.usuarioLogueado = localStorage.getItem('usuarioLogueado');
    // Verificamos que nos retorne TRUE la funcion valorDelRolUsuario
    expect(component.valorDelRolUsuario()).toBeTruthy();
    // Ahora verificamos para el caso contrario que sea diferente de EMP_AMD
    component.rolUsuario = 'AnyThing';
    // Verificamos que nos retorne FALSE la funcion valorDelRolUsuario
    expect(component.valorDelRolUsuario()).toBeFalsy();
    // Verificamos que se asigne el valor al formControl(filterUsuario)
    // Del usuarioLogeado que se seteo inicialmente en localStorage ( Revisar BeforeEach)
    expect(component.consultarValidacionesForm.getRawValue().filterUsuario).toEqual(localStorage.getItem('usuarioLogueado'));
  });


  it('Verificamos la funcion setDataFechaBusqueda(fechaDesde,fechaHasta)', () => {
    // Llamamos nuestra funcion setDataFechaBusqueda(fechaDesde,fechaHasta)
    // Probamos el primer caso que sean ''
    component.setDataFechaBusqueda('', '');
    // Verificamos los valores asignados a dataRequest.fechaDesde y dataRequest.fechaHasta , SEAN Constant.CAMPO_VACIO
    expect(component.dataRequest.fechaDesde).toEqual(Constant.CAMPO_VACIO);
    expect(component.dataRequest.fechaHasta).toEqual(Constant.CAMPO_VACIO);

    // Ahora verficamos para el sea undefined
    component.setDataFechaBusqueda(Constant.CAMPO_UNDEFINED, Constant.CAMPO_UNDEFINED);
    // Verificamos los valores asignados a dataRequest.fechaDesde y dataRequest.fechaHasta , SEAN Constant.CAMPO_UNDEFINED
    expect(component.dataRequest.fechaDesde).toEqual(Constant.CAMPO_VACIO);
    expect(component.dataRequest.fechaHasta).toEqual(Constant.CAMPO_VACIO);

    // Ahora verficamos para el sea CAMPO_NULO
    component.setDataFechaBusqueda(Constant.CAMPO_NULO, Constant.CAMPO_NULO);
    // Verificamos los valores asignados a dataRequest.fechaDesde y dataRequest.fechaHasta , SEAN Constant.CAMPO_UNDEFINED
    expect(component.dataRequest.fechaDesde).toEqual(Constant.CAMPO_VACIO);
    expect(component.dataRequest.fechaHasta).toEqual(Constant.CAMPO_VACIO);


    // Ahora verificamos el caso cuando tenga una fecha valida
    const mockData = {
      'filterfechaHasta': new Date('2021-03-13T21:32:52.000Z'),
      'filterUsuario': 'abenito',
      'filterfechaDesde': new Date('2014-09-15T21:32:52.000Z'),
      'cantidad': ''
    };
    // Llamamos nuestra funcion
    component.setDataFechaBusqueda(mockData.filterfechaDesde, mockData.filterfechaHasta);
        // Verificamos los valores asignados a dataRequest.fechaDesde y dataRequest.fechaHasta , 15092014 y 13032021 respectivamente
        expect(component.dataRequest.fechaDesde).toEqual('15092014');
        expect(component.dataRequest.fechaHasta).toEqual('13032021');
  });


  it('Verificamos la funcion setDataFechaBusquedaExportar(fechaDesde,fechaHasta)', () => {
    // Llamamos nuestra funcion setDataFechaBusquedaExportar(fechaDesde,fechaHasta)
    // Probamos el primer caso que sean ''
    component.setDataFechaBusquedaExportar('', '');
    // Verificamos los valores asignados a dataRequestReporte.f_fDesde y dataRequest.f_fHasta , SEAN Constant.CAMPO_VACIO
    expect(component.dataRequestReporte.f_fDesde).toEqual(Constant.CAMPO_VACIO);
    expect(component.dataRequestReporte.f_fHasta).toEqual(Constant.CAMPO_VACIO);

    // Ahora verficamos para el sea undefined
    component.setDataFechaBusquedaExportar(Constant.CAMPO_UNDEFINED, Constant.CAMPO_UNDEFINED);
    // Verificamos los valores asignados a dataRequestReporte.f_fDesde y dataRequest.f_fHasta , SEAN Constant.CAMPO_UNDEFINED
    expect(component.dataRequestReporte.f_fDesde).toEqual(Constant.CAMPO_VACIO);
    expect(component.dataRequestReporte.f_fHasta).toEqual(Constant.CAMPO_VACIO);

    // Ahora verficamos para el sea CAMPO_NULO
    component.setDataFechaBusquedaExportar(Constant.CAMPO_NULO, Constant.CAMPO_NULO);
    // Verificamos los valores asignados a dataRequestReporte.f_fDesde y dataRequest.f_fHasta , SEAN Constant.CAMPO_UNDEFINED
    expect(component.dataRequestReporte.f_fDesde).toEqual(Constant.CAMPO_VACIO);
    expect(component.dataRequestReporte.f_fHasta).toEqual(Constant.CAMPO_VACIO);


    // Ahora verificamos el caso cuando tenga una fecha valida
    const mockData = {
      'filterfechaHasta': new Date('2021-03-13T21:32:52.000Z'),
      'filterUsuario': 'abenito',
      'filterfechaDesde': new Date('2014-09-15T21:32:52.000Z'),
      'cantidad': ''
    };
    // Llamamos nuestra funcion
    component.setDataFechaBusquedaExportar(mockData.filterfechaDesde, mockData.filterfechaHasta);
    // Verificamos los valores asignados a dataRequestReporte.f_fDesde y dataRequestReporte.f_fHasta , 15092014 y 13032021 respectivamente
        expect(component.dataRequestReporte.f_fDesde).toEqual('15/09/2014');
        expect(component.dataRequestReporte.f_fHasta).toEqual('13/03/2021');
  });



  it('Verificamos el funcionamiento esperado de la funcion salir()', () => {
    const vacio: Object = {  };
    component.dataRequest = vacio;
    // Creamos un espia para el servicio
    const spyService = spyOn(servicio, 'serviceSignOff').and.callThrough();
    // Llamamos nuestra funcion salir() desde el componente
    component.salir();
    // Verificamos que nuestro servicio fuera llamado
    expect(spyService).toHaveBeenCalled();

    // Mockeamos dataResponse para probar la validaciones , el primer caso
    const mockDataResponse = {
      codigoRespuesta : Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    };
    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate').and.callThrough();

    // Fakeamos una llamada al servicio para que nos de el mock de lo que deseamos
    const spyServiceFake = spyOn(servicio, 'serviceSignOff').and.callFake(() => {
      return of (mockDataResponse);
    });

    component.salir();

    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();
    // Verificamos que nuestro servicio Fake fuera llamado
    expect(spyServiceFake).toHaveBeenCalled();


    // Comprobamos la siguiente condicion cuando es diferente de COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    mockDataResponse.codigoRespuesta = 'Diferente de correcto';

    // Volvemos a llamar a la funcion
    component.salir();

    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();


    // Ahora verificamos en caso de un error al llamar al servicio
    const spyError = spyOn(servicio, 'serviceSignOff').and.returnValue(throwError('Esto es un error'));
    // Volvemos a llamar a la funcion
    component.salir();
    // Verificamos que fuera llamado con error nuestro servicio
    expect(spyError).toHaveBeenCalled();
    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();
  });

  it('Verificamos la funcion buscarValidaciones()', () => {
    // Espiamos nuestro servicio serviceConsultarValidaciones
    const spyService = spyOn(servicio, 'serviceConsultarValidaciones').and.callThrough();
    // Espiamos nuestra funcion setDataFechaBusqueda
    const spysetDataFechaBusqueda = spyOn(component, 'setDataFechaBusqueda').and.callThrough();

    const mockCantidad = '123';
    // Asigamos el valor a nuestro formControl Cantidad
    component.consultarValidacionesForm.controls.cantidad.setValue(mockCantidad);
    const mockUsuario = 'abenito';
    // Asigamos el valor a nuestro formControl filterUsuario
    component.consultarValidacionesForm.controls.filterUsuario.setValue(mockUsuario);
    // Mockeamos valores la pagina y tamaño de pagina (mockSize)
    const   mockPagina = 1;
    const mockSize = '10';
    component.sizePagination = mockSize;
       // Llamamos nuestra funcion
    component.buscarValidaciones(mockPagina);

    // Verificamos los valores de listarValidacionesOK y esperandoValidaciones
    expect(component.listarValidacionesOK).toBeFalsy();
    expect(component.esperandoValidaciones).toBeTruthy();
    // Verificamos que el valor de numeroPaginas sea 0
    expect(component.numeroPaginas).toBe(0);
    // Verificamos que nuestros espias fueran llamados
    expect(spysetDataFechaBusqueda).toHaveBeenCalled();
    expect(spyService).toHaveBeenCalled();
    // Verificamos la pagina
    expect(component.dataRequest.pagina.toString()).toEqual(mockPagina.toString());
    expect(component.p).toEqual(mockPagina);
    // Verificamos los valores asignados al dataRequest
    expect(component.dataRequest.cantidad).toEqual(mockCantidad);
    expect(component.dataRequest.usuario).toEqual(mockUsuario);


    // Ahora mockeamos un dataResponse que nos devolvera nuestro servicio
    // Probaremos la primera condicion cuando codigoRespuesta sea Constant.COD_OPERACION_SATISFACTORIA_CONSULTA_VALIDACIONES_SIXBIO
    // Mockeamos data como pagina y validacionesBiometricas para verificar las asignaciones
    const mockDataResponse = {
      codigoRespuesta : Constant.COD_OPERACION_SATISFACTORIA_CONSULTA_VALIDACIONES_SIXBIO,
      paginas: '2',
      validacionesBiometricas: [
        {
            'apellidoMaterno': 'CARRASCO',
            'apellidoPaterno': 'BAJALQUI',
            'codigoRespuesta': null,
            'codigoTransaccion': '301000',
            'fechaCaducidad': '19-12-2022',
            'fechaEmision': '01-02-2016',
            'fechaNacimiento': '29-12-1991',
            'fechaValidacion': '03/03/2020',
            'horaProceso': '00:00:00',
            'idValidacion': '1435356861058524',
            'latitudLocalizacion': '-77.010710',
            'longitudLocalizacion': '-12.107029',
            'mensajeRespuesta': null,
            'nombres': 'EDGAR GUSTAVO',
            'numeroDocumento': '88888888',
            'origenRespuesta': 'INVALIDO',
            'sexo': 'MASCULINO',
            'sistemaLocalizacion': 'PSISTGEO001',
            'tipoDocumento': 'DNI',
            'usuario': 'abenito',
            'tipoVerificacion': 'T33'
        },
        {
            'apellidoMaterno': 'CARRASCO',
            'apellidoPaterno': 'BAJALQUI',
            'codigoRespuesta': null,
            'codigoTransaccion': '301000',
            'fechaCaducidad': '19-12-2022',
            'fechaEmision': '01-02-2016',
            'fechaNacimiento': '29-12-1991',
            'fechaValidacion': '03/03/2020',
            'horaProceso': '00:00:00',
            'idValidacion': '8740580460025774',
            'latitudLocalizacion': '-77.010710',
            'longitudLocalizacion': '-12.107029',
            'mensajeRespuesta': null,
            'nombres': 'EDGAR GUSTAVO',
            'numeroDocumento': '88888888',
            'origenRespuesta': 'INVALIDO',
            'sexo': 'MASCULINO',
            'sistemaLocalizacion': 'PSISTGEO001',
            'tipoDocumento': 'DNI',
            'usuario': 'abenito',
            'tipoVerificacion': 'T33'
        },
        {
            'apellidoMaterno': 'CARRASCO',
            'apellidoPaterno': 'BAJALQUI',
            'codigoRespuesta': null,
            'codigoTransaccion': '301000',
            'fechaCaducidad': '19-12-2022',
            'fechaEmision': '01-02-2016',
            'fechaNacimiento': '29-12-1991',
            'fechaValidacion': '03/03/2020',
            'horaProceso': '00:00:00',
            'idValidacion': '0441148302662070',
            'latitudLocalizacion': '-77.010710',
            'longitudLocalizacion': '-12.107029',
            'mensajeRespuesta': null,
            'nombres': 'EDGAR GUSTAVO',
            'numeroDocumento': '88888888',
            'origenRespuesta': 'INVALIDO',
            'sexo': 'MASCULINO',
            'sistemaLocalizacion': 'PSISTGEO001',
            'tipoDocumento': 'DNI',
            'usuario': 'abenito',
            'tipoVerificacion': 'T33'
        },
        {
            'apellidoMaterno': 'CARRASCO',
            'apellidoPaterno': 'BAJALQUI',
            'codigoRespuesta': null,
            'codigoTransaccion': '301000',
            'fechaCaducidad': '19-12-2022',
            'fechaEmision': '01-02-2016',
            'fechaNacimiento': '29-12-1991',
            'fechaValidacion': '03/03/2020',
            'horaProceso': '00:00:00',
            'idValidacion': '0631463202555101',
            'latitudLocalizacion': '-77.010710',
            'longitudLocalizacion': '-12.107029',
            'mensajeRespuesta': null,
            'nombres': 'EDGAR GUSTAVO',
            'numeroDocumento': '88888888',
            'origenRespuesta': 'INVALIDO',
            'sexo': 'MASCULINO',
            'sistemaLocalizacion': 'PSISTGEO001',
            'tipoDocumento': 'DNI',
            'usuario': 'abenito',
            'tipoVerificacion': 'T33'
        },
        {
            'apellidoMaterno': 'CARRASCO',
            'apellidoPaterno': 'BAJALQUI',
            'codigoRespuesta': null,
            'codigoTransaccion': '301000',
            'fechaCaducidad': '19-12-2022',
            'fechaEmision': '01-02-2016',
            'fechaNacimiento': '29-12-1991',
            'fechaValidacion': '13/02/2020',
            'horaProceso': '00:00:00',
            'idValidacion': '6304808127482730',
            'latitudLocalizacion': '-77.010710',
            'longitudLocalizacion': '-12.107029',
            'mensajeRespuesta': null,
            'nombres': 'EDGAR GUSTAVO',
            'numeroDocumento': '88888888',
            'origenRespuesta': 'INVALIDO',
            'sexo': 'MASCULINO',
            'sistemaLocalizacion': 'PSISTGEO001',
            'tipoDocumento': 'DNI',
            'usuario': 'abenito',
            'tipoVerificacion': 'T33'
        },
        {
            'apellidoMaterno': 'CARRASCO',
            'apellidoPaterno': 'BAJALQUI',
            'codigoRespuesta': '00000',
            'codigoTransaccion': '301001',
            'fechaCaducidad': '19-12-2022',
            'fechaEmision': '01-02-2016',
            'fechaNacimiento': '29-12-1991',
            'fechaValidacion': '10/02/2020',
            'horaProceso': '00:00:00',
            'idValidacion': '2453856751465534',
            'latitudLocalizacion': '-77.010710',
            'longitudLocalizacion': '-12.107029',
            'mensajeRespuesta': 'SI CORRESPONDE',
            'nombres': 'EDGAR GUSTAVO',
            'numeroDocumento': '88888888',
            'origenRespuesta': 'NOVATRONIC',
            'sexo': 'MASCULINO',
            'sistemaLocalizacion': 'PSISTGEO001',
            'tipoDocumento': 'DNI',
            'usuario': 'abenito',
            'tipoVerificacion': 'T33'
        },
        {
            'apellidoMaterno': 'MAMANI',
            'apellidoPaterno': 'SMITH',
            'codigoRespuesta': '00000',
            'codigoTransaccion': '310000',
            'fechaCaducidad': '19-12-2022',
            'fechaEmision': '01-02-2016',
            'fechaNacimiento': '29-12-1991',
            'fechaValidacion': '10/02/2020',
            'horaProceso': '00:00:00',
            'idValidacion': '3158671148180673',
            'latitudLocalizacion': '-77.010710',
            'longitudLocalizacion': '-12.107029',
            'mensajeRespuesta': 'SI CORRESPONDE',
            'nombres': 'GERRY CHERRY TERRY',
            'numeroDocumento': '7777777777777',
            'origenRespuesta': 'NOVATRONIC',
            'sexo': 'MASCULINO',
            'sistemaLocalizacion': 'PSISTGEO001',
            'tipoDocumento': 'PASAPORTE',
            'usuario': 'abenito',
            'tipoVerificacion': 'T33'
        },
        {
            'apellidoMaterno': 'CARRASCO',
            'apellidoPaterno': 'BAJALQUI',
            'codigoRespuesta': '00000',
            'codigoTransaccion': '301001',
            'fechaCaducidad': '19-12-2022',
            'fechaEmision': '01-02-2016',
            'fechaNacimiento': '29-12-1991',
            'fechaValidacion': '10/02/2020',
            'horaProceso': '00:00:00',
            'idValidacion': '1973226176994878',
            'latitudLocalizacion': '-77.010710',
            'longitudLocalizacion': '-12.107029',
            'mensajeRespuesta': 'SI CORRESPONDE',
            'nombres': 'EDGAR GUSTAVO',
            'numeroDocumento': '88888888',
            'origenRespuesta': 'NOVATRONIC',
            'sexo': 'MASCULINO',
            'sistemaLocalizacion': 'PSISTGEO001',
            'tipoDocumento': 'DNI',
            'usuario': 'abenito',
            'tipoVerificacion': 'T33'
        },
        {
            'apellidoMaterno': 'MAMANI',
            'apellidoPaterno': 'SMITH',
            'codigoRespuesta': '00000',
            'codigoTransaccion': '310000',
            'fechaCaducidad': '19-12-2022',
            'fechaEmision': '01-02-2016',
            'fechaNacimiento': '29-12-1991',
            'fechaValidacion': '10/02/2020',
            'horaProceso': '00:00:00',
            'idValidacion': '1850878025236177',
            'latitudLocalizacion': '-77.010710',
            'longitudLocalizacion': '-12.107029',
            'mensajeRespuesta': 'SI CORRESPONDE',
            'nombres': 'GERRY CHERRY TERRY',
            'numeroDocumento': '7777777777777',
            'origenRespuesta': 'NOVATRONIC',
            'sexo': 'MASCULINO',
            'sistemaLocalizacion': 'PSISTGEO001',
            'tipoDocumento': 'PASAPORTE',
            'usuario': 'abenito',
            'tipoVerificacion': 'T33'
        },
        {
            'apellidoMaterno': 'MAMANI',
            'apellidoPaterno': 'SMITH',
            'codigoRespuesta': '00000',
            'codigoTransaccion': '310000',
            'fechaCaducidad': '19-12-2022',
            'fechaEmision': '01-02-2016',
            'fechaNacimiento': '29-12-1991',
            'fechaValidacion': '10/02/2020',
            'horaProceso': '00:00:00',
            'idValidacion': '4523838724174847',
            'latitudLocalizacion': '-77.010710',
            'longitudLocalizacion': '-12.107029',
            'mensajeRespuesta': 'SI CORRESPONDE',
            'nombres': 'GERRY CHERRY TERRY',
            'numeroDocumento': '7777777777777',
            'origenRespuesta': 'NOVATRONIC',
            'sexo': 'MASCULINO',
            'sistemaLocalizacion': 'PSISTGEO001',
            'tipoDocumento': 'PASAPORTE',
            'usuario': 'abenito',
            'tipoVerificacion': 'T33'
        }
       ]
    };
    // Este valor seria la expect que tendremos para el numero de pagina el cual verificaremos
    const esperoNumeroPaginas = Number(mockDataResponse.paginas) * Number(mockSize);
    // Fakeamos la respuesta de nuestro servicio con callFake
    const spyServiceFake = spyOn(servicio, 'serviceConsultarValidaciones').and.callFake(() => {
      return of(mockDataResponse);
    });
    // Espiamos nuestra funcion setCamposGrilla
    const spysetCamposGrilla = spyOn(component, 'setCamposGrilla').and.callThrough();
    // Llamamos nuestra funcion
    component.buscarValidaciones(mockPagina);
    // Verificamos que llame nuestro servicio FAKE
    expect(spyServiceFake).toHaveBeenCalled();
    // Verificamos que esperandoValidaciones sea false
    expect(component.esperandoValidaciones).toBeFalsy();
    // Verificamos que llame nuestra funcion setCamposGrilla
    expect(spysetCamposGrilla).toHaveBeenCalled();
    // Verificamos el valor de listarValidacionesOK sea TRUE
    expect(component.listarValidacionesOK).toBeTruthy();
    // verificamos que numero paginas sea esperoNumeroPaginas
    expect(component.numeroPaginas).toBe(esperoNumeroPaginas);
    // Verificamos que listaValidaciones sea igual mockDataResponse.validacionesBiometricas
    expect(component.listaValidaciones).toEqual(mockDataResponse.validacionesBiometricas);

    // Ahora verificamos para el caso codigoRespuesta sea Constant.COD_ERROR_SESION
    mockDataResponse.codigoRespuesta = Constant.COD_ERROR_SESION;
    // Espiamos nuestra funcion salir()
    const spySalir = spyOn(component, 'salir').and.callThrough();
    // Llamamos nuestra funcion
    component.buscarValidaciones(mockPagina);
    // Verificamos que hubiera llamado nuestra funcion salir()
    expect(spySalir).toHaveBeenCalled();

    // Ahora verificamos para el caso que que el codigoRespuesta no sea ninguno de los de anteriores , es decir no este definido
    mockDataResponse.codigoRespuesta = 'Ninguno de los Anteriores o no definido';
    // Creame un comportamiento Fake para nuestro toast caso contrarios nos dara un error por la libreria translate
    const spyToastFake = spyOn(component.toast, 'addToast').and.callFake(() => { 'Nada'; });
    // Llamamos nuestra funcion
    component.buscarValidaciones(mockPagina);
    // Ahora el valor de esperandoValidaciones debe ser FALSE
    expect(component.esperandoValidaciones).toBeFalsy();
    // Verificamos que llame nuestro spyToastFake
    expect(spyToastFake).toHaveBeenCalled();


    // Ahora verificamos el caso donde nuestro servicio nos retorne un ERROR
    // Para ello forzaremos a que nos de un error con returnValue(thrownError('Mensaje'))
    const spyServiceError = spyOn(servicio, 'serviceConsultarValidaciones').and.returnValue(throwError('Error serviceConsultarValidaciones !'));
    // Llamamos nuestra funcion
    component.buscarValidaciones(mockPagina);
    // Ahora el valor de esperandoValidaciones debe ser FALSE
    expect(component.esperandoValidaciones).toBeFalsy();
    // Verificamos que llame nuestro spyToastFake
    expect(spyToastFake).toHaveBeenCalled();
    // Verificamos que hubiera llamado nuestra funcion salir()
    expect(spySalir).toHaveBeenCalled();
  });




  it('Verificamos la funcion cambiosGrilla(valor)', () => {
    // Mockeamos data para nuestro formulario y verificamos que se asigne correctamente al dataRequest
      component.consultarValidacionesForm.controls.filterUsuario.setValue('jburgos6');
      component.consultarValidacionesForm.controls.cantidad.setValue('123');
      component.consultarValidacionesForm.controls.filterfechaDesde.setValue('');
      component.consultarValidacionesForm.controls.filterfechaHasta.setValue('');
    // Mockeamos un valor sizePagination
    const mockSizePagination = '10';
    component.sizePagination = mockSizePagination;
    // Mockeamos valor
    const mockValor = 5;
    // Espiamos nuestro servicio para verificamos posteriormente si este fue llamado
    const spyService = spyOn(servicio, 'serviceConsultarValidaciones').and.callThrough();

    // Probaremos el primer caso donde listarValidacionesOK sea FALSE
    component.listarValidacionesOK = false;
    // Llamamos nuestra funcion cambiosGrilla(valor)
    component.cambiosGrilla(mockValor);
    // Verificamos el valor de esperandoValidaciones debe ser FALSE
    expect(component.esperandoValidaciones).toBeFalsy();
    // Verificamos que se asigne la data mockeada correctamente a dataRequest
    expect(component.dataRequest.usuario).toEqual('jburgos6');
    expect(component.dataRequest.cantidad).toEqual('123');
    expect(component.dataRequest.fechaDesde).toEqual('');
    expect(component.dataRequest.fechaHasta).toEqual('');
    // Para verificamos el caso de valor y pagina lo convertimos a String dado que es un Number para poder comparar
    expect(component.dataRequest.pagina.toString()).toEqual(mockValor.toString());
    // Verificamos para nuestra variable p
    expect(component.p).toEqual(mockValor);


    // Verificamos que fuera NO llamado nuestro servicio serviceConsultarValidaciones
    // para ello usaremos la funcion not delante de toHaveBeenCalled()
    expect(spyService).not.toHaveBeenCalled();


    // Probaremos el primer caso donde listarValidacionesOK sea TRUE
    component.listarValidacionesOK = true;
    // Llamamos nuestra funcion cambiosGrilla(valor)
    component.cambiosGrilla(mockValor);

    // Verificamos que fuera SI llamado nuestro servicio serviceConsultarValidaciones
    expect(spyService).toHaveBeenCalled();

    // Ahora mockeamos codigoRespuesta para verificar las condiciones
    // Probaremos inicialmente con codigoRespuesta sea Constant.COD_OPERACION_SATISFACTORIA_CONSULTA_VALIDACIONES_SIXBIO
    const mockDataResponse = {
      codigoRespuesta : Constant.COD_OPERACION_SATISFACTORIA_CONSULTA_VALIDACIONES_SIXBIO,
      paginas: '2',
      validacionesBiometricas : [
        {
            'apellidoMaterno': 'CARRASCO',
            'apellidoPaterno': 'BAJALQUI',
            'codigoRespuesta': null,
            'codigoTransaccion': '301000',
            'fechaCaducidad': '19-12-2022',
            'fechaEmision': '01-02-2016',
            'fechaNacimiento': '29-12-1991',
            'fechaValidacion': '10/02/2020',
            'horaProceso': '00:00:00',
            'idValidacion': '0824278678231766',
            'latitudLocalizacion': '-77.010710',
            'longitudLocalizacion': '-12.107029',
            'mensajeRespuesta': null,
            'nombres': 'EDGAR GUSTAVO',
            'numeroDocumento': '88888888',
            'origenRespuesta': 'INVALIDO',
            'sexo': 'MASCULINO',
            'sistemaLocalizacion': 'PSISTGEO001',
            'tipoDocumento': 'DNI',
            'usuario': 'abenito',
            'tipoVerificacion': 'T33'
        },
        {
            'apellidoMaterno': 'MAMANI',
            'apellidoPaterno': 'SMITH',
            'codigoRespuesta': null,
            'codigoTransaccion': '310000',
            'fechaCaducidad': '19-12-2022',
            'fechaEmision': '01-02-2016',
            'fechaNacimiento': '29-12-1991',
            'fechaValidacion': '10/02/2020',
            'horaProceso': '00:00:00',
            'idValidacion': '8856425848271707',
            'latitudLocalizacion': '-77.010710',
            'longitudLocalizacion': '-12.107029',
            'mensajeRespuesta': null,
            'nombres': 'GERRY CHERRY TERRY',
            'numeroDocumento': '7777777777777',
            'origenRespuesta': 'INVALIDO',
            'sexo': 'MASCULINO',
            'sistemaLocalizacion': 'PSISTGEO001',
            'tipoDocumento': 'PASAPORTE',
            'usuario': 'abenito',
            'tipoVerificacion': 'T33'
        },
        {
            'apellidoMaterno': 'CARRASCO',
            'apellidoPaterno': 'BAJALQUI',
            'codigoRespuesta': '10000',
            'codigoTransaccion': '301001',
            'fechaCaducidad': '19-12-2022',
            'fechaEmision': '01-02-2016',
            'fechaNacimiento': '29-12-1991',
            'fechaValidacion': '10/02/2020',
            'horaProceso': '00:00:00',
            'idValidacion': '2318016121671603',
            'latitudLocalizacion': '-77.010710',
            'longitudLocalizacion': '-12.107029',
            'mensajeRespuesta': 'NO CORRESPONDE',
            'nombres': 'EDGAR GUSTAVO',
            'numeroDocumento': '88888888',
            'origenRespuesta': 'NOVATRONIC',
            'sexo': 'MASCULINO',
            'sistemaLocalizacion': 'PSISTGEO001',
            'tipoDocumento': 'DNI',
            'usuario': 'abenito',
            'tipoVerificacion': 'T33'
        }
      ]
    };
    // Ahora hacemos que nuestro servicio nos devuelva nuestro mockDataResponse
    const spyServiceFake = spyOn(servicio, 'serviceConsultarValidaciones').and.callFake(() => {
      return of(mockDataResponse);
    });
    // Espiamos nuestra funcion setCamposGrilla
    const spysetCamposGrilla = spyOn(component, 'setCamposGrilla').and.callThrough();
    // asignamos TRUE a listarValidacionesOK para poder validar nuestro caso
    component.listarValidacionesOK = true;
    // Llamamos nuestra funcion cambiosGrilla(valor)
    component.cambiosGrilla(mockValor);
    // Verificamos que llame nuestro servicio Fake
    expect(spyServiceFake).toHaveBeenCalled();
    // Verificamos que llame nuestra funcion setCamposGrilla();
    expect(spysetCamposGrilla).toHaveBeenCalled();
    // Verificamos que el valor de numeroPaginas sea mockDataResponse.paginas* Number(mockSizePagination)
    // Los volvemos string para poder comparar su igualdad
    const numeroPaginasExpect = (Number(mockDataResponse.paginas) * Number(mockSizePagination)).toString();
    expect(component.numeroPaginas.toString()).toEqual(numeroPaginasExpect);
    // Verificamos que listaValidaciones se asigne a mockDataResponse.validacionesBiometricas
    expect(component.listaValidaciones).toEqual(mockDataResponse.validacionesBiometricas);
    // Verificamos listaValidacionesOk sea TRUE
    expect(component.listarValidacionesOK).toBeTruthy();



    // Ahora probaremos para el caso el codigoRespuesta sea Constant.COD_ERROR_SESION
    mockDataResponse.codigoRespuesta = Constant.COD_ERROR_SESION;
    // Espiamos nuestra funcion salir()
    const spySalir = spyOn(component, 'salir').and.callThrough();
    // asignamos TRUE a listarValidacionesOK para poder validar nuestro caso
    component.listarValidacionesOK = true;
    // Llamamos nuestra funcion cambiosGrilla(valor)
    component.cambiosGrilla(mockValor);
    // Verificamos que se llame nuestra funcion salir()
    expect(spySalir).toHaveBeenCalled();


    // Ahora verificamos el caso que no nos devuelva ninguno codigoRespuesta asignado
    mockDataResponse.codigoRespuesta = 'Ninguno de los anteriores o no determinado';
    // Fakeamos nuestro comportamiento de la funcion toast.addToast para poder verificar si llama
    const spyToastFake = spyOn(component.toast, 'addToast').and.callThrough();
    // asignamos TRUE a listarValidacionesOK para poder validar nuestro caso
    component.listarValidacionesOK = true;
    // Llamamos nuestra funcion cambiosGrilla(valor)
    component.cambiosGrilla(mockValor);
    // Verificamos que se asigne FALSE a listarValidacionesOK
    expect(component.listarValidacionesOK).toBeFalsy();
    // Verificamos que llame nuestro toastFake
    expect(spyToastFake).toHaveBeenCalled();


  });
  /*
     cambiosGrilla(valor) {
            this.esperandoValidaciones = false;
            this.dataRequest.usuario = this.consultarValidacionesForm.getRawValue().filterUsuario;
            this.dataRequest.cantidad = this.consultarValidacionesForm.getRawValue().cantidad;
            this.setDataFechaBusqueda(this.consultarValidacionesForm.getRawValue().filterfechaDesde,
                 this.consultarValidacionesForm.getRawValue().filterfechaHasta);
            this.dataRequest.tamanioPagina = this.sizePagination;
            this.dataRequest.pagina = valor;
            this.p = valor;

            if (this.listarValidacionesOK) {
              //  this.spinnerService.llamarSpinner();
                this.servicioService.serviceConsultarValidaciones(this.dataRequest).subscribe(
                    (result: any) => {
                        if (Constant.COD_OPERACION_SATISFACTORIA_CONSULTA_VALIDACIONES_SIXBIO === result.codigoRespuesta) {
                            this.numeroPaginas = result.paginas * Number(this.sizePagination);
                            this.listaValidaciones = result.validacionesBiometricas;
                            this.setCamposGrilla();
                            this.listarValidacionesOK = true;
                        } else if (result.codigoRespuesta === Constant.COD_ERROR_SESION) {
                            this.salir();
                        return;
                        } else {
                            this.toast.addToast({ title: 'Error', msg: result.mensajeRespuesta, timeOut: 5000, type: 'error' });
                            this.listarValidacionesOK = false;
                        }

                    }, error => {
                        this.toast.addToast({ title: 'Error', msg: Constant.MENSAJE_ERROR_CONECTIVIDAD, timeOut: 5000, type: 'error' });
                        console.log(error);
                        setTimeout(() => {this.salir(); }, 500);
                    }
                );
            }
    }





  */


});

