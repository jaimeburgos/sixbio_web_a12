import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConsultarValidacionesComponent } from './consultar-validaciones.component';
import { DetalleValidacionComponent } from './components';

const routes: Routes = [
    {path: '',
    children: [
        {path: '', component: ConsultarValidacionesComponent},
        {path: 'detalle-validacion', component: DetalleValidacionComponent}
    ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsultarValidacionesRoutingModule { }
