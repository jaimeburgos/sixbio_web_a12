import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsultarValidacionesRoutingModule } from './consultar-validaciones-routing.module';
import { ConsultarValidacionesComponent } from './consultar-validaciones.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { DetalleValidacionComponent } from './components/detalle-validacion/detalle-validacion.component';
import { TranslateModule } from '@ngx-translate/core';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
import { DatePipe } from '@angular/common';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { NgxSpinnerModule } from 'ngx-spinner';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorService } from 'src/app/services/interceptor.service';


defineLocale('es', esLocale);

@NgModule({
  declarations: [ConsultarValidacionesComponent, DetalleValidacionComponent],
  imports: [
    CommonModule,
    ConsultarValidacionesRoutingModule,
    NgxPaginationModule,
    BsComponentModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    NgxSpinnerModule
  ],
  providers: [
    DatePipe,
    {provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true}
   ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
})
export class ConsultarValidacionesModule { }
