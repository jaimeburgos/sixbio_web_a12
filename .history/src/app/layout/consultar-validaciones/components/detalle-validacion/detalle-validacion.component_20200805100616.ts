import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { IValidaciones } from 'src/app/nucleo/interface/IValidaciones';
import { Constant } from 'src/app/nucleo/constante/Constant';

@Component({
  selector: 'app-detalle-validacion',
  templateUrl: './detalle-validacion.component.html',
  styleUrls: ['./detalle-validacion.component.scss']
})
export class DetalleValidacionComponent implements OnInit {

    constructor(private router: Router, private rutaActual: ActivatedRoute) {

    }
    footer = Constant.FOOTER_NOVATRONIC;
    validacion: IValidaciones;
    ngOnInit() {

        this.validacion = history.state;
    }

    cancelar() {
        localStorage.setItem('regresarDetalleVal', 'true');
         this.router.navigate(['consultar-validaciones/']);
    }

}
