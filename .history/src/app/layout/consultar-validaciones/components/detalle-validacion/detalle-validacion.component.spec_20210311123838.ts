import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { EnvService } from 'src/app/env.service';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { ServicioService } from 'src/app/servicio/servicio.service';

import { DetalleValidacionComponent } from './detalle-validacion.component';

describe('DetalleValidacionComponent', () => {
  let component: DetalleValidacionComponent;
  let fixture: ComponentFixture<DetalleValidacionComponent>;
  let translateService: TranslateService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleValidacionComponent, ToastComponent ] ,
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService ,
        {provide: ToastrService, useClass: ToastrService}],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleValidacionComponent);
    translateService = TestBed.inject(TranslateService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
