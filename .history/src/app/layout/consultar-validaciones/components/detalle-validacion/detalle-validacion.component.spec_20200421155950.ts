import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleValidacionComponent } from './detalle-validacion.component';

describe('DetalleValidacionComponent', () => {
  let component: DetalleValidacionComponent;
  let fixture: ComponentFixture<DetalleValidacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleValidacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleValidacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
