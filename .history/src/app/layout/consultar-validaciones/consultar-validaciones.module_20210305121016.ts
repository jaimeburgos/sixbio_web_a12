import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConsultarValidacionesRoutingModule } from './consultar-validaciones-routing.module';
import { ConsultarValidacionesComponent } from './consultar-validaciones.component';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { DetalleValidacionComponent } from './components/detalle-validacion/detalle-validacion.component';
import { TranslateModule } from '@ngx-translate/core';


@NgModule({
  declarations: [ConsultarValidacionesComponent, DetalleValidacionComponent],
  imports: [
    CommonModule,
    ConsultarValidacionesRoutingModule,
    NgxPaginationModule,
    NgxMyDatePickerModule.forRoot(),
    BsComponentModule,
    TranslateModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class ConsultarValidacionesModule { }
