import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConsultarValidacionesRoutingModule } from './consultar-validaciones-routing.module';
import { ConsultarValidacionesComponent } from './consultar-validaciones.component';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { DetalleValidacionComponent } from './components/detalle-validacion/detalle-validacion.component';
import { NgxSpinnerModule } from 'ngx-spinner';

@NgModule({
  declarations: [ConsultarValidacionesComponent, DetalleValidacionComponent],
  imports: [
    CommonModule,
    ConsultarValidacionesRoutingModule,
    FormsModule,
    NgxPaginationModule,
    NgxMyDatePickerModule.forRoot(),
    BsComponentModule,
    NgxSpinnerModule
  ]
})
export class ConsultarValidacionesModule { }
