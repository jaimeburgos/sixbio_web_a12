import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { ToastComponent } from '../bs-component/components/toast/toast.component';
import { IDataValidacion } from 'src/app/nucleo/interface/IDataValidacion';
import { IlistaUsuario } from 'src/app/nucleo/interface/IlistaUsuario';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { IValidaciones } from 'src/app/nucleo/interface/IValidaciones';
import { Router, ActivatedRoute } from '@angular/router';
import { IReporteValidaciones } from 'src/app/nucleo/interface/IReporteValidaciones';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { saveAs } from 'file-saver';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { listLocales } from 'ngx-bootstrap/chronos';
import { DatePipe } from '@angular/common';
import { SpinnerService } from 'src/app/services/spinner.service';


@Component({
    selector: 'app-consultar-validaciones',
    templateUrl: './consultar-validaciones.component.html',
    styleUrls: ['./consultar-validaciones.component.scss'],
    animations: [routerTransition()]
})
export class ConsultarValidacionesComponent implements OnInit {

    constructor(private servicioService: ServicioService, private router: Router, private rutaActual: ActivatedRoute ,
        private spinnerService: SpinnerService, private datePipe: DatePipe, private fb: FormBuilder,
        private localeService: BsLocaleService) {
        this.localeService.use(this.locale);
        this.maxDate = new Date();
        this.maxDate.setDate(this.maxDate.getDate());
    }
    locale = 'es';
    locales = listLocales();
    @ViewChild(ToastComponent, { static: true }) toast: ToastComponent;
    footer = Constant.FOOTER_NOVATRONIC;
    cambioPagina = true;
    detalleValidacionFiltros = history.state;
    isFiltroVacio = false;
    sizePagination = '';
    rolUsuario = localStorage.getItem('RolUsuario');
    p = 1;
    numeroPaginas = 0;
    filterUsuario = null;
    usuarioLogueado = localStorage.getItem('usuarioLogueado');
    esperandoValidaciones = false;
    listarValidacionesOK = false;
    isFechasDisable = true;
    fechaReporte = '';
    listaValidaciones: IValidaciones[] = [];
    listaValidacionesExportar: IValidaciones[] = [];
    listaUsuarios: IlistaUsuario[] = [];
    listaUsuariosSixbio: IlistaUsuario[] = [];
    dataRequest: IDataValidacion = {
        usuario: '',
        fechaDesde: '',
        fechaHasta: '',
        cantidad: '',
        pagina: '1',
        tamanioPagina: '10',
    };

    dataRequestReporte: IReporteValidaciones = {
        listaValidaciones: [],
        tipoReporte: ' ',
        f_usuario: ' ',
        f_cantidad: ' ',
        f_fDesde: ' ',
        f_fHasta: ' ',
    };
    maxDate: Date;

    consultarValidacionesForm: FormGroup;
    data: Object = {};

    isClicPDF = true ;
    isClicExcel = true;

    ngOnInit() {
        this.listaValidaciones = [];
        this.sizePagination = '10';
        this.p = 1;
        this.numeroPaginas = 0;
        this.listaUsuariosSixbio = [];
        this.listaUsuarios = [];
        this.consultarValidacionesForm =  this.fb.group({
            filterfechaHasta: new FormControl( '', Validators.compose([
                Validators.required,
            ])),
            filterUsuario: new FormControl('' , Validators.compose([
                Validators.required,
            ])),
            filterfechaDesde: new FormControl('', Validators.compose([
                Validators.required,
             ])),
            cantidad: new FormControl('', Validators.compose([
                 Validators.required,
                 Validators.maxLength(4),
                 Validators.pattern('[0-9]+'),
            ]))
        });
        this.consultarValidacionesForm.controls.filterUsuario.setValue(localStorage.getItem('usuarioLogueado'));
        this.recuperarConsultaValidaciones();
        this.listarUsuarios();
        this.filterRadioButtonCantidad();
        this.filterRadioButtonFechas();
    }

    onChangeForm() {
    if (this.listaValidaciones.length > 0) {
            this.listaValidaciones = [];
            this.listaValidacionesExportar = [];
            this.listarValidacionesOK = false;
        }
    }

    onChangeCantidad() {
        this.isExcelDisabled();
        this.isPDFDisabled();
        if (this.listaValidaciones.length > 0) {
            this.listaValidaciones = [];
            this.listaValidacionesExportar = [];
            this.listarValidacionesOK = false;
        }
    }
    onChangeFecha() {
        this.isExcelDisabled();
        this.isPDFDisabled();
        if (this.listaValidaciones.length > 0) {
            this.listaValidaciones = [];
            this.listaValidacionesExportar = [];
            this.listarValidacionesOK = false;
        }
    }

    isExcelDisabled() {

        if (this.consultarValidacionesForm.invalid) {
            this.isClicExcel = true;
        } else  {
            this.isClicExcel = false;
        }
    }

    isPDFDisabled() {
        if (this.consultarValidacionesForm.invalid) {
            this.isClicPDF = true;
        } else {
            this.isClicPDF = false;
        }
    }

    exportarExcel() {
        this.isClicExcel = true;
        this.isClicPDF = true;
        this.setTipoReporte('excel');
        this.buscarValidacionesExportar('', '', 'reporteValidaciones' + this.setFechaActual() + '.xls');
    }

    exportarPDF() {
        this.isClicExcel = true;
        this.isClicPDF = true;
        this.setTipoReporte('pdf');
        this.buscarValidacionesExportar('', '', 'reporteValidaciones' + this.setFechaActual() + '.pdf');
    }


    buscarValidaciones(pagina) {
            this.listarValidacionesOK = false;
            this.esperandoValidaciones = true;
            this.numeroPaginas = 0;

            this.setDataFechaBusqueda(this.consultarValidacionesForm.getRawValue().filterfechaDesde,
            this.consultarValidacionesForm.getRawValue().filterfechaHasta);
            this.dataRequest.tamanioPagina = this.sizePagination;
            this.dataRequest.pagina = pagina;
            this.p = pagina;
            this.dataRequest.cantidad = this.consultarValidacionesForm.getRawValue().cantidad;
            this.dataRequest.usuario = this.consultarValidacionesForm.getRawValue().filterUsuario;
            this.servicioService.serviceConsultarValidaciones(this.dataRequest).subscribe(
                (result: any) => {
                    if (Constant.COD_OPERACION_SATISFACTORIA_CONSULTA_VALIDACIONES_SIXBIO === result.codigoRespuesta) {
                        this.esperandoValidaciones = false;
                        this.numeroPaginas = result.paginas * Number(this.sizePagination);
                        this.listaValidaciones = result.validacionesBiometricas;
                        this.setCamposGrilla();
                        this.listarValidacionesOK = true;
                    } else if (result.codigoRespuesta === Constant.COD_ERROR_SESION) {
                        this.salir();
                        return;
                        // redirect inicio
                    } else {
                        this.toast.addToast({ title: 'Error', msg: result.mensajeRespuesta, timeOut: 5000, type: 'error' });
                        this.esperandoValidaciones = false;
                    }

                }, error => {
                    this.toast.addToast({ title: 'Error', msg: Constant.MENSAJE_ERROR_CONECTIVIDAD, timeOut: 5000, type: 'error' });
                    this.esperandoValidaciones = false;
                    setTimeout(() => {this.salir(); }, 500);
                }
            );
    }


    cambiosGrilla(valor) {
            this.esperandoValidaciones = false;
            this.dataRequest.usuario = this.consultarValidacionesForm.getRawValue().filterUsuario;
            this.dataRequest.cantidad = this.consultarValidacionesForm.getRawValue().cantidad;
            this.setDataFechaBusqueda(this.consultarValidacionesForm.getRawValue().filterfechaDesde,
                 this.consultarValidacionesForm.getRawValue().filterfechaHasta);
            this.dataRequest.tamanioPagina = this.sizePagination;
            this.dataRequest.pagina = valor;
            this.p = valor;

            if (this.listarValidacionesOK) {
              //  this.spinnerService.llamarSpinner();
                this.servicioService.serviceConsultarValidaciones(this.dataRequest).subscribe(
                    (result: any) => {
                        if (Constant.COD_OPERACION_SATISFACTORIA_CONSULTA_VALIDACIONES_SIXBIO === result.codigoRespuesta) {
                            this.numeroPaginas = result.paginas * Number(this.sizePagination);
                            this.listaValidaciones = result.validacionesBiometricas;
                            this.setCamposGrilla();
                            this.listarValidacionesOK = true;
                        } else if (result.codigoRespuesta === Constant.COD_ERROR_SESION) {
                            this.salir();
                        return;
                        } else {
                            this.toast.addToast({ title: 'Error', msg: result.mensajeRespuesta, timeOut: 5000, type: 'error' });
                            this.listarValidacionesOK = false;
                        }

                    }, error => {
                        this.toast.addToast({ title: 'Error', msg: Constant.MENSAJE_ERROR_CONECTIVIDAD, timeOut: 5000, type: 'error' });
                        setTimeout(() => {this.salir(); }, 500);
                    }
                );
            }
    }
    setRequest(value) {
        localStorage.setItem('tipoVerificacion', value);
    }

    detalleValidacion(validacion: IValidaciones) {
        Constant.FILTRO_FDESDE = this.consultarValidacionesForm.getRawValue().filterfechaDesde;
        Constant.FILTRO_FHASTA = this.consultarValidacionesForm.getRawValue().filterfechaHasta;
        Constant.FILTRO_CANTIDAD = this.consultarValidacionesForm.getRawValue().cantidad;
        Constant.FILTRO_USUARIO = this.consultarValidacionesForm.getRawValue().filterUsuario;
        this.guardarPropiedades();
        this.router.navigate(['detalle-validacion'], { state: validacion, relativeTo: this.rutaActual });
    }

    recuperarConsultaValidaciones() {
        this.validarRegresoDetalleValidacion();
    }

    validarRegresoDetalleValidacion() {
        if (localStorage.getItem('regresarDetalleVal') === 'true') {
            this.recuperarDatosFiltrosPropiedades();
            this.buscarValidaciones(this.p);
            this.isClicExcel = false;
            this.isClicPDF = false;
        } else {
            localStorage.setItem('campoFechaDesde', 'false');
            localStorage.setItem('campoFechaHasta', 'false');
            localStorage.setItem('campoCantidad', 'true');
            localStorage.setItem('ischeckCantidad', 'true');
            localStorage.setItem('ischeckFechas', 'false');
        }
        localStorage.setItem('regresarDetalleVal', 'false');

    }

    guardarPropiedades() {
        localStorage.setItem('numeroPagina', this.p.toString());
        localStorage.setItem('tamanioPagina', this.sizePagination);
    }

    recuperarDatosFiltrosPropiedades() {
        this.consultarValidacionesForm.controls.filterfechaDesde.setValue(Constant.FILTRO_FDESDE);
        this.consultarValidacionesForm.controls.filterfechaHasta.setValue(Constant.FILTRO_FHASTA);
        this.consultarValidacionesForm.controls.filterUsuario.setValue(Constant.FILTRO_USUARIO);
        this.consultarValidacionesForm.controls.cantidad.setValue(Constant.FILTRO_CANTIDAD);

        this.sizePagination =  localStorage.getItem('tamanioPagina');
        this.p = Number(localStorage.getItem('numeroPagina'));
    }


    exportar(extension: string) {
        this.setdataRequestReporte();
        this.servicioService.serviceGenerarReporteValidaciones(this.dataRequestReporte).subscribe(
            (result: any) => {
                saveAs(result, extension);
            }, error => {
                this.toast.addToast({ title: 'Error', msg: Constant.MENSAJE_ERROR_CONECTIVIDAD, timeOut: 5000, type: 'error' });
            }
        );
    }

    buscarValidacionesExportar(pagina, tamanioPagina, extension) {
            this.listaValidacionesExportar = [];
            this.dataRequest.usuario = this.consultarValidacionesForm.getRawValue().filterUsuario;
            this.dataRequest.cantidad = this.consultarValidacionesForm.getRawValue().cantidad;
            this.setDataFechaBusqueda(this.consultarValidacionesForm.getRawValue().filterfechaDesde,
            this.consultarValidacionesForm.getRawValue().filterfechaHasta);
            this.dataRequest.tamanioPagina = tamanioPagina;
            this.dataRequest.pagina = pagina;
            this.mensajeEspera();

            this.servicioService.serviceConsultarValidaciones(this.dataRequest).subscribe(
                (result: any) => {
                    if (Constant.COD_OPERACION_SATISFACTORIA_CONSULTA_VALIDACIONES_SIXBIO === result.codigoRespuesta) {
                        this.listaValidacionesExportar = result.validacionesBiometricas;
                        this.setCamposGrillaExportar();
                        this.exportar(extension);
                        this.isClicPDF = false;
                        this.isClicExcel = false;
                    } else if (result.codigoRespuesta === Constant.COD_ERROR_SESION) {
                        this.salir();
                        return;
                    } else {
                        this.toast.addToast({ title: 'Error', msg: result.mensajeRespuesta, timeOut: 5000, type: 'error' });
                        this.isClicPDF = false;
                        this.isClicExcel = false;
                    }

                }, error => {
                    this.toast.addToast({ title: 'Error', msg: Constant.MENSAJE_ERROR_CONECTIVIDAD, timeOut: 5000, type: 'error' });
                    setTimeout(() => {this.salir(); }, 500);
                }
            );
    }

    mensajeEspera() {
        this.toast.addToast({ title: 'Info', msg: Constant.MENSAJE_ESPERA_REPORTE, timeOut: 5000, type: 'info' });
    }

    listarUsuarios() {
        this.servicioService.serviceListarUsuario(this.data).subscribe(
            (result: any) => {
                if (Constant.COD_OPERACION_SATISFACTORIA_SCA === result.codigoRespuesta) {
                this.listaUsuarios = result.listarUsuario.listUsuarios;
                this.discriminarUsuarioSixbio();
                } else if (result.codigoRespuesta === Constant.COD_ERROR_SESION) {
                    this.salir();
                        return;
                } else {
                    this.toast.addToast({ title: 'Error', msg: result.mensajeRespuesta, timeOut: 5000, type: 'error' });
                }
            }, error => {
                // modal de error
                this.toast.addToast({ title: 'Error', msg: Constant.MENSAJE_ERROR_CONECTIVIDAD, timeOut: 5000, type: 'error' });
                setTimeout(() => {this.salir(); }, 500);
            }
            );
    }

    discriminarUsuarioSixbio() {
        this.listaUsuarios.forEach(element => {
            const existe = Constant.ROLES_AGRUPADORES_VALOR.includes(element.rol);
            if (existe) {
                this.listaUsuariosSixbio.push(element);
            }
        });
    }

    setdataRequestReporte() {
        this.dataRequestReporte.listaValidaciones = this.listaValidacionesExportar;
        this.dataRequestReporte.f_usuario = this.consultarValidacionesForm.getRawValue().filterUsuario;
        this.dataRequestReporte.f_cantidad = this.consultarValidacionesForm.getRawValue().cantidad;
        this.setDataFechaBusquedaExportar(this.consultarValidacionesForm.getRawValue().filterfechaDesde,
        this.consultarValidacionesForm.getRawValue().filterfechaHasta);
    }

    setTipoReporte(tipoReport: string) {
        this.dataRequestReporte.tipoReporte = tipoReport;
    }

    setCamposGrilla() {
        this.listaValidaciones.forEach(validacion => {
            this.setTipoVerificacion(validacion);
            this.setMensajeRespuesta(validacion);
            this.setOrigenRespuesta(validacion);
        });
    }

    setCamposGrillaExportar() {
        this.listaValidacionesExportar.forEach(validacion => {
            this.setTipoVerificacion(validacion);
            this.setMensajeRespuesta(validacion);
            this.setOrigenRespuesta(validacion);
        });

    }

    setTipoVerificacion(validacion: IValidaciones) {
        switch (validacion.codigoTransaccion) {
            case Constant.VERI_DACTILAR_T33_MINUCIAS: {
                validacion.tipoVerificacion = Constant.T33;
                break;
            }
            case Constant.VERI_DACTILAR_T33_WSQ: {
                validacion.tipoVerificacion = Constant.T33;
                break;
            }
            case Constant.VERI_DACTILAR_T35_WSQ: {
                validacion.tipoVerificacion = Constant.T35;
                break;
            }
            case Constant.VERI_EXTRANJERO: {
                validacion.tipoVerificacion = Constant.T33;
                break;
            }
            case Constant.VERI_FACIAL: {
                validacion.tipoVerificacion = Constant.T37;
                break;
            }
            default: {
                validacion.tipoVerificacion = Constant.T33;
                break;
            }
        }

    }

    setMensajeRespuesta(validacion: IValidaciones) {
        switch (validacion.codigoRespuesta) {
            case Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO: {
                validacion.mensajeRespuesta = Constant.SI_CORRESPONDE;
                break;
            }
            case Constant.COD_OPERACION_HUELLAS_NO_COINCIDEN: {
                validacion.mensajeRespuesta = Constant.NO_CORRESPONDE;
                break;
            }
            case Constant.COD_OPERACION_SATISFACTORIA_RENIEC: {
                validacion.mensajeRespuesta = Constant.SI_CORRESPONDE;
                break;
            }

            case Constant.COD_OPERACION_NO_CORRESPONDE_RENIEC: {
                validacion.mensajeRespuesta = Constant.NO_CORRESPONDE;
                break;
            }
            case Constant.COD_OPERACION_SATISFACTORIA_RENIEC_FACIAL: {
                validacion.mensajeRespuesta = Constant.SI_CORRESPONDE;
                break;
            }
            case Constant.COD_OPERACION_NO_CORRESPONDE_RENIEC_FACIAL: {
                validacion.mensajeRespuesta = Constant.NO_CORRESPONDE;
                break;
            }
        }
        validacion.fechaValidacion = validacion.fechaValidacion.substring(0, 10);

    }

    setOrigenRespuesta(validacion: IValidaciones) {
        switch (validacion.origenRespuesta) {
            case Constant.COD_ORIGEN_RENIEC: {
                validacion.origenRespuesta = Constant.DESC_ORIGEN_RENIEC;
                break;
            }
            case Constant.COD_ORIGEN_NOVATRONIC: {
                validacion.origenRespuesta = Constant.DESC_ORIGEN_NOVATRONIC;
                break;
            }
            case Constant.COD_ORIGEN_MIGRACIONES: {
                validacion.origenRespuesta = Constant.DESC_ORIGEN_MIGRACIONES;
                break;
            }
            case Constant.COD_ORIGEN_SCA: {
                validacion.origenRespuesta = Constant.DESC_ORIGEN_SCA;
                break;
            }
            default: {
                validacion.origenRespuesta = Constant.DESC_ORIGEN_DEFAULT;
                break;
            }
        }
    }

    setFechaActual(): string {
        const fecha = new Date();
        const mes = '' + <string><any>(fecha.getMonth() + 1);
        const dia = '' + <string><any>fecha.getDate();
        const hora = '' + <string><any>fecha.getHours();
        const minuto = '' + <string><any>fecha.getMinutes();
        const segundo = '' + <string><any>fecha.getSeconds();
        this.fechaReporte = <string><any>fecha.getFullYear() + '' + mes.padStart(2, '0') + '' + dia.padStart(2, '0');
        // eslint-disable-next-line max-len
        this.fechaReporte = this.fechaReporte + hora.padStart(2, '0') + '' + minuto.padStart(2, '0') + '' + segundo.padStart(2, '0');
        return this.fechaReporte;
    }

    activarCantidad() {
        localStorage.setItem('ischeckCantidad', 'true');
        localStorage.setItem('ischeckFechas', 'false');
        localStorage.setItem('campoFechaDesde', 'false');
        localStorage.setItem('campoFechaHasta', 'false');
        this.isFechasDisable = true;

        this.consultarValidacionesForm.controls.filterfechaDesde.reset();
        this.consultarValidacionesForm.controls.filterfechaHasta.reset();
        this.consultarValidacionesForm.controls.filterfechaDesde.setValue(Constant.CAMPO_VACIO);
        this.consultarValidacionesForm.controls.filterfechaHasta.setValue(Constant.CAMPO_VACIO);


        this.listaValidaciones =  [];
        this.listaValidacionesExportar =  [];
        this.listarValidacionesOK = false;
        this.consultarValidacionesForm.controls.filterfechaDesde.disable();
        this.consultarValidacionesForm.controls.filterfechaHasta.disable();
        this.consultarValidacionesForm.controls.cantidad.enable();
    }

    filterRadioButtonCantidad(): boolean {
        if (localStorage.getItem('ischeckCantidad') === 'true') {
            this.consultarValidacionesForm.controls.filterfechaDesde.disable();
            this.consultarValidacionesForm.controls.filterfechaHasta.disable();
            this.consultarValidacionesForm.controls.cantidad.enable();
            this.isFechasDisable = true;
            return true;
        }
        return false;
    }

    activarFechas() {
        localStorage.setItem('ischeckFechas', 'true');
        localStorage.setItem('campoFechaDesde', 'true');
        localStorage.setItem('campoFechaHasta', 'true');
        localStorage.setItem('ischeckCantidad', 'false');
        this.isFechasDisable = false;

        this.consultarValidacionesForm.controls.cantidad.reset();
        this.consultarValidacionesForm.controls.cantidad.setValue(Constant.CAMPO_VACIO);

        this.listaValidaciones =  [];
        this.listaValidacionesExportar =  [];
        this.listarValidacionesOK = false;
        this.consultarValidacionesForm.controls.cantidad.disable();
        this.consultarValidacionesForm.controls.filterfechaDesde.enable();
        this.consultarValidacionesForm.controls.filterfechaHasta.enable();
    }



    filterRadioButtonFechas(): boolean {
        if (localStorage.getItem('ischeckFechas') === 'true') {
            this.consultarValidacionesForm.controls.cantidad.disable();
            this.consultarValidacionesForm.controls.filterfechaDesde.enable();
            this.consultarValidacionesForm.controls.filterfechaHasta.enable();
            this.isFechasDisable = false;
            return true;
        }
        return false;
    }

    valorDelRolUsuario() {
      if (this.rolUsuario === 'EMP_ADM') {
          return true;
       } else {
        this.consultarValidacionesForm.controls.filterUsuario.setValue(this.usuarioLogueado);
            return false;
        }
    }

    setDataFechaBusqueda(fechaDesde: any, fechaHasta: any) {
       if (fechaDesde === Constant.CAMPO_NULO || fechaDesde === Constant.CAMPO_UNDEFINED || fechaDesde === '') {
           this.dataRequest.fechaDesde = Constant.CAMPO_VACIO;
        } else {
            this.dataRequest.fechaDesde = this.datePipe.transform(fechaDesde, 'ddMMyyyy');
       }
        if (fechaHasta === Constant.CAMPO_NULO || fechaHasta === Constant.CAMPO_UNDEFINED || fechaHasta === '') {
            this.dataRequest.fechaHasta = Constant.CAMPO_VACIO;
        } else {
            this.dataRequest.fechaHasta = this.datePipe.transform(fechaHasta, 'ddMMyyyy');
        }
    }

    setDataFechaBusquedaExportar(fechaDesde: any, fechaHasta: any) {
        if (fechaDesde === Constant.CAMPO_NULO || fechaDesde === Constant.CAMPO_UNDEFINED || fechaDesde === '') {
            this.dataRequestReporte.f_fDesde = Constant.CAMPO_VACIO;

        } else {
            this.dataRequestReporte.f_fDesde = this.datePipe.transform(fechaDesde, 'dd/MM/yyyy');
        }
        if (fechaHasta === Constant.CAMPO_NULO || fechaHasta === Constant.CAMPO_UNDEFINED || fechaDesde === '') {
            this.dataRequestReporte.f_fHasta = Constant.CAMPO_VACIO;
        } else {

            this.dataRequestReporte.f_fHasta = this.datePipe.transform(fechaHasta, 'dd/MM/yyyy');
        }
    }

    salir() {
        this.servicioService.serviceSignOff(this.dataRequest).subscribe((dataResponse: any) => {

            if (dataResponse.codigoRespuesta === Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO) {
                localStorage.clear();
                this.router.navigate(['/login']);
            } else {
                localStorage.clear();
                this.router.navigate(['/login']);
            }
        }
            , error => {
                localStorage.clear();
                this.router.navigate(['/login']);
            });
    }
}
