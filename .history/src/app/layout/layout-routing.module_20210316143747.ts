import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
    {
        path: '',
      component: LayoutComponent,
        children: [
            // tslint:disable-next-line:max-line-length
        {path: 'verificacionBiometrica', loadChildren: () => import('./verificacion/verificacion.module').then(m => m.VerificacionModule)},
            // tslint:disable-next-line:max-line-length
            { path: 'gestion-usuarios', loadChildren: () => import('./gestion-usuarios/gestion-usuarios.module').then(m => m.GestionUsuariosModule) },
            // tslint:disable-next-line:max-line-length
            { path: 'consultar-validaciones', loadChildren: () => import('./consultar-validaciones/consultar-validaciones.module').then(m => m.ConsultarValidacionesModule) },
        ]
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule {}
