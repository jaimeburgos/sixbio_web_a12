import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IUsuario } from '../nucleo/interface/IUsuario';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

    collapedSideBar: boolean;

    entidad: IUsuario;
    constructor(   public router: Router) {}

    ngOnInit() {
        console.log(this.router.url);
    }

    receiveCollapsed($event) {
        this.collapedSideBar = $event;
    }
}
