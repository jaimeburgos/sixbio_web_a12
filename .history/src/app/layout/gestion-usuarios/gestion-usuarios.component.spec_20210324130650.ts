import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { EnvService } from 'src/app/env.service';
import { ExporterService } from 'src/app/services/exporter.service';
import { ServicioService } from 'src/app/servicio/servicio.service';

import { GestionUsuariosComponent } from './gestion-usuarios.component';
class FakeRouter {
  navigate(params) {

  }
}
fdescribe('GestionUsuariosComponent', () => {
  let component: GestionUsuariosComponent;
  let fixture: ComponentFixture<GestionUsuariosComponent>;
  let translateService: TranslateService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionUsuariosComponent ],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
        RouterTestingModule,
        ModalModule.forRoot(),
      ],
      providers: [
        ExporterService,
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService ,
        {provide: Router , useClass: FakeRouter},
        {provide: ToastrService, useClass: ToastrService}],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionUsuariosComponent);
    translateService = TestBed.inject(TranslateService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Componente Gestion-Usuario creado correctamente', () => {
    expect(component).toBeTruthy();
  });

  /*
    filterUsuario : new FormControl('', Validators.compose([ Validators.maxLength(20) ])),
    filterApellido : new FormControl('', Validators.compose([Validators.maxLength(50)  ])),
    filterNombre : new FormControl('', Validators.compose([ Validators.maxLength(50) ])),
  */
  it('Campo "filterUsuario" de gestionarUsuarioForm longitud maxima es 20', () => {
    const filterUsuario = component.gestionarUsuarioForm.get('filterUsuario');
    filterUsuario.setValue('12345678901234567890');
    expect(filterUsuario.valid).toBeTruthy();
    filterUsuario.setValue('123456789012345678901');
    expect(filterUsuario.valid).toBeFalsy();
  });

  it('Campo "filterApellido" de gestionarUsuarioForm longitud maxima es 50', () => {
    const filterApellido = component.gestionarUsuarioForm.get('filterApellido');
    filterApellido.setValue('12345678901234567890123456789012345678901234567890');
    expect(filterApellido.valid).toBeTruthy();
    filterApellido.setValue('123456789012345678901234567890123456789012345678901');
    expect(filterApellido.valid).toBeFalsy();
  });


  it('Campo "filterNombre" de gestionarUsuarioForm longitud maxima es 50', () => {
    const filterNombre = component.gestionarUsuarioForm.get('filterNombre');
    filterNombre.setValue('12345678901234567890123456789012345678901234567890');
    expect(filterNombre.valid).toBeTruthy();
    filterNombre.setValue('123456789012345678901234567890123456789012345678901');
    expect(filterNombre.valid).toBeFalsy();
  });


  it('filterUsuario con caracteres permitidos y no permitidos en gestionarUsuarioForm', () => {
    const filterUsuario = component.gestionarUsuarioForm.get('filterUsuario');
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b'];
    const U_No_Permitidos =
    ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
    '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
    '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]', '          '];
    // Bucle para caracteres permitidos en usuario gestionarUsuarioForm
    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      filterUsuario.setValue(U_Permitidos[j]);
      expect(filterUsuario.valid).toBe(true);
      // Cuando sea verdadero !filterUsuario.valid no se cumple la condicion de verificacion
      if ( !filterUsuario.valid) {
        console.log('Fallo en el caracter Permitido gestionarUsuarioForm ' + U_Permitidos[j] );
      }
    }
    // Bucle para caracteres no permitidos en Usuario gestionarUsuarioForm
    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      filterUsuario.setValue(U_No_Permitidos[i]);
      expect(filterUsuario.valid).toBe(false);
      // Cuando sea verdadero no se cumple la condicion de verificacion
      if (filterUsuario.valid) {
        console.log('Fallo en el caracter No Permitido  gestionarUsuarioForm' + U_No_Permitidos[i] );
      }
    }
  });

  it('filterApellido con caracteres permitidos y no permitidos en gestionarUsuarioForm', () => {
    const filterApellido = component.gestionarUsuarioForm.get('filterApellido');
    const U_Permitidos = ['aaaaaaaa', 'ccccb bbbb', 'c123bbb4b', 'rrr33 444 44'];
    const U_No_Permitidos =
    ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
    '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
    '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]', 'ñññññññ'];

    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      filterApellido.setValue(U_Permitidos[j]);
      expect(filterApellido.valid).toBe(true);
      if ( !filterApellido.valid) {
        console.log('Fallo en el caracter Permitido gestionarUsuarioForm ' + U_Permitidos[j] );
      }
    }

    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      filterApellido.setValue(U_No_Permitidos[i]);
      expect(filterApellido.valid).toBe(false);
      if (filterApellido.valid) {
        console.log('Fallo en el caracter No Permitido  gestionarUsuarioForm' + U_No_Permitidos[i] );
      }
    }
  });

  it('filterNombre con caracteres permitidos y no permitidos en gestionarUsuarioForm', () => {
    const filterNombre = component.gestionarUsuarioForm.get('filterNombre');
    const U_Permitidos = ['aaaaaaaa', 'ccccb bbbb', 'c123bbb4b', 'rrr33 444 44'];
    const U_No_Permitidos =
    ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
    '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
    '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]', 'ñññññññ'];

    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      filterNombre.setValue(U_Permitidos[j]);
      expect(filterNombre.valid).toBe(true);
      if ( !filterNombre.valid) {
        console.log('Fallo en el caracter Permitido gestionarUsuarioForm ' + U_Permitidos[j] );
      }
    }

    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      filterNombre.setValue(U_No_Permitidos[i]);
      expect(filterNombre.valid).toBe(false);
      if (filterNombre.valid) {
        console.log('Fallo en el caracter No Permitido  gestionarUsuarioForm' + U_No_Permitidos[i] );
      }
    }
  });

  it('Verificamos el funcionamiento esperado de la funcion salir()', () => {
    const vacio: Object = {  };
    // Asignamos valor al localStorage changePasswordPorCaducar para probar condicion de la funcion salir()
    localStorage.setItem('changePasswordPorCaducar', 'true');
    // verificamos que sea true
    expect(localStorage.getItem('changePasswordPorCaducar')).toBeTruthy();
    // Llamamos la funcion salir()
    component.salir();
     // Si se cumple la segunda condicion nos debe redireccionar a /login
     localStorage.setItem('isFirstLogin', 'true');
     localStorage.setItem('changePassword', 'false');
     // injectamos nuestro FakeRouter
     const router = TestBed.inject(Router);
     // Espiamos que llame a navigate router
     const spy2 = spyOn(router, 'navigate');
     component.salir();
     // verificamos que efectivamente sea llamando
     expect(spy2).toHaveBeenCalled();

    // Si se cumple la tercera condicion
    localStorage.setItem('changePasswordPorCaducar', null);
    localStorage.setItem('isFirstLogin', null);
    localStorage.setItem('changePassword', 'true');

    // Espiamos que llame a navigate router
    const spy3 = spyOn(router, 'navigate');
     component.salir();
    // verificamos que efectivamente sea llamando
    expect(spy3).toHaveBeenCalled();

    // en caso que no se cumpla ninguna de las 3 condiciones
    localStorage.setItem('changePasswordPorCaducar', null);
    localStorage.setItem('changePassword', null);
    localStorage.setItem('isFirstLogin', null);

    component.salir();

  });






});
