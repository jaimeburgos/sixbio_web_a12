import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, Injectable, Injector, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, getTestBed, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MissingTranslationHandler, MissingTranslationHandlerParams, TranslateLoader, TranslateModule, TranslatePipe, TranslateService, TranslateStore } from '@ngx-translate/core';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { Observable, of, Subject, throwError } from 'rxjs';
import { EnvService } from 'src/app/env.service';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { ExporterService } from 'src/app/services/exporter.service';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { BsComponentModule } from '../bs-component/bs-component.module';
import * as FileSaver from 'file-saver';


import { GestionUsuariosComponent } from './gestion-usuarios.component';
import { translate } from '@angular/localize/src/utils';
import { defineLocale, esLocale } from 'ngx-bootstrap/chronos';
import { HttpLoaderFactory } from 'src/app/shared/modules/language-translation/language-translation.module';
import { HttpClient } from '@angular/common/http';
import { analyzeNgModules } from '@angular/compiler';

class FakeRouter {
  navigate(params) {

  }
}

class FakeActivatedRoute {
  // params: Observable<any> = Observable.empty();
  private subject = new Subject();

  push( valor ) {
    this.subject.next( valor );
  }
  get params() {
    return this.subject.asObservable();
  }
}


fdescribe('GestionUsuariosComponent', () => {
  let component: GestionUsuariosComponent;
  let fixture: ComponentFixture<GestionUsuariosComponent>;
  let servicio: ServicioService;


  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionUsuariosComponent ],
      imports: [
        CommonModule,
        BsComponentModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        BsDatepickerModule.forRoot(),
        RouterTestingModule,
        ModalModule.forRoot(),
        TranslateModule.forRoot()
      ],
      providers: [
        ExporterService,
        BsLocaleService,
        TranslateService,
        DatePipe,
        EnvService,
        ServicioService ,
        {provide: Router , useClass: FakeRouter},
        {provide: ToastrService, useClass: ToastrService},
        { provide: ActivatedRoute, useClass: FakeActivatedRoute },
      ],

      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionUsuariosComponent);
    servicio = TestBed.inject(ServicioService);



    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it('Componente Gestion-Usuario creado correctamente', () => {

   expect(component).toBeTruthy();
  } );

  it('Campo "filterUsuario" de gestionarUsuarioForm longitud maxima es 20', () => {
    const filterUsuario = component.gestionarUsuarioForm.get('filterUsuario');
    filterUsuario.setValue('12345678901234567890');
    expect(filterUsuario.valid).toBeTruthy();
    filterUsuario.setValue('123456789012345678901');
    expect(filterUsuario.valid).toBeFalsy();
  });

  it('Campo "filterApellido" de gestionarUsuarioForm longitud maxima es 50', () => {
    const filterApellido = component.gestionarUsuarioForm.get('filterApellido');
    filterApellido.setValue('12345678901234567890123456789012345678901234567890');
    expect(filterApellido.valid).toBeTruthy();
    filterApellido.setValue('123456789012345678901234567890123456789012345678901');
    expect(filterApellido.valid).toBeFalsy();
  });


  it('Campo "filterNombre" de gestionarUsuarioForm longitud maxima es 50', () => {
    const filterNombre = component.gestionarUsuarioForm.get('filterNombre');
    filterNombre.setValue('12345678901234567890123456789012345678901234567890');
    expect(filterNombre.valid).toBeTruthy();
    filterNombre.setValue('123456789012345678901234567890123456789012345678901');
    expect(filterNombre.valid).toBeFalsy();
  });


  it('filterUsuario con caracteres permitidos y no permitidos en gestionarUsuarioForm', () => {
    const filterUsuario = component.gestionarUsuarioForm.get('filterUsuario');
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b'];
    const U_No_Permitidos =
    ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
    '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
    '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]', '          '];
    // Bucle para caracteres permitidos en usuario gestionarUsuarioForm
    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      filterUsuario.setValue(U_Permitidos[j]);
      expect(filterUsuario.valid).toBe(true);
      // Cuando sea verdadero !filterUsuario.valid no se cumple la condicion de verificacion
      if ( !filterUsuario.valid) {
        console.log('Fallo en el caracter Permitido gestionarUsuarioForm ' + U_Permitidos[j] );
      }
    }
    // Bucle para caracteres no permitidos en Usuario gestionarUsuarioForm
    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      filterUsuario.setValue(U_No_Permitidos[i]);
      expect(filterUsuario.valid).toBe(false);
      // Cuando sea verdadero no se cumple la condicion de verificacion
      if (filterUsuario.valid) {
        console.log('Fallo en el caracter No Permitido  gestionarUsuarioForm' + U_No_Permitidos[i] );
      }
    }
  });

  it('filterApellido con caracteres permitidos y no permitidos en gestionarUsuarioForm', () => {
    const filterApellido = component.gestionarUsuarioForm.get('filterApellido');
    const U_Permitidos = ['aaaaaaaa', 'ccccb bbbb', 'c123bbb4b', 'rrr33 444 44'];
    const U_No_Permitidos =
    ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
    '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
    '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]', 'ñññññññ'];

    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      filterApellido.setValue(U_Permitidos[j]);
      expect(filterApellido.valid).toBe(true);
      if ( !filterApellido.valid) {
        console.log('Fallo en el caracter Permitido gestionarUsuarioForm ' + U_Permitidos[j] );
      }
    }

    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      filterApellido.setValue(U_No_Permitidos[i]);
      expect(filterApellido.valid).toBe(false);
      if (filterApellido.valid) {
        console.log('Fallo en el caracter No Permitido  gestionarUsuarioForm' + U_No_Permitidos[i] );
      }
    }
  });

  it('filterNombre con caracteres permitidos y no permitidos en gestionarUsuarioForm', () => {
    const filterNombre = component.gestionarUsuarioForm.get('filterNombre');
    const U_Permitidos = ['aaaaaaaa', 'ccccb bbbb', 'c123bbb4b', 'rrr33 444 44'];
    const U_No_Permitidos =
    ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
    '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
    '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]', 'ñññññññ'];

    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      filterNombre.setValue(U_Permitidos[j]);
      expect(filterNombre.valid).toBe(true);
      if ( !filterNombre.valid) {
        console.log('Fallo en el caracter Permitido gestionarUsuarioForm ' + U_Permitidos[j] );
      }
    }

    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      filterNombre.setValue(U_No_Permitidos[i]);
      expect(filterNombre.valid).toBe(false);
      if (filterNombre.valid) {
        console.log('Fallo en el caracter No Permitido  gestionarUsuarioForm' + U_No_Permitidos[i] );
      }
    }
  });

  it('Verificamos el funcionamiento esperado de la funcion salir()', () => {
    const vacio: Object = {  };
    component.dataRequest = vacio;
    // Creamos un espia para el servicio
    const spyService = spyOn(servicio, 'serviceSignOff').and.callThrough();
    // Llamamos nuestra funcion salir() desde el componente
    component.salir();
    // Verificamos que nuestro servicio fuera llamado
    expect(spyService).toHaveBeenCalled();

    // Mockeamos dataResponse para probar la validaciones , el primer caso
    const mockDataResponse = {
      codigoRespuesta : Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    };
    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate').and.callThrough();

    // Fakeamos una llamada al servicio para que nos de el mock de lo que deseamos
    const spyServiceFake = spyOn(servicio, 'serviceSignOff').and.callFake(() => {
      return of (mockDataResponse);
    });

    component.salir();

    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();
    // Verificamos que nuestro servicio Fake fuera llamado
    expect(spyServiceFake).toHaveBeenCalled();


    // Comprobamos la siguiente condicion cuando es diferente de COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    mockDataResponse.codigoRespuesta = 'Diferente de correcto';

    // Volvemos a llamar a la funcion
    component.salir();

    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();


    // Ahora verificamos en caso de un error al llamar al servicio
    const spyError = spyOn(servicio, 'serviceSignOff').and.returnValue(throwError('Esto es un error'));
    // Volvemos a llamar a la funcion
    component.salir();
    // Verificamos que fuera llamado con error nuestro servicio
    expect(spyError).toHaveBeenCalled();
    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();

  });

  it('Verificamos funcion decline()', () => {
    // Espiamos al modal para verificar la funcion hide
    const spyModal = spyOn(component.modal, 'hide').and.callThrough();
    // Llamamos a la funcion del component
    component.decline();
    // Verificamos que fuera llamado la funcion hide del modal y que aceptar sea FALSE
    expect(component.aceptar).toBeFalsy();
    expect(spyModal).toHaveBeenCalled();
  });


  it('Verificamos funcion confirm()', () => {
    // Espiamos al modal para verificar la funcion hide
    const spyModal = spyOn(component.modal, 'hide').and.callThrough();
    // Llamamos a la funcion del component
    component.confirm();
    // Verificamos que fuera llamado la funcion hide del modal y que aceptar sea TRUE
    expect(component.aceptar).toBeTruthy();
    expect(spyModal).toHaveBeenCalled();
  });

  it('Verieficamos funcion showModal()', () => {
    // Espiamos al modal para verificar la funcion show
    const spyModal = spyOn(component.modal, 'show').and.callThrough();
    component.showModal();
    // Verificamos que fuera llamado la funcion show del modal
    expect(spyModal).toHaveBeenCalled();
  });


  it('Verificamos funcion crearUsuario()', () => {
    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate');
    // Llamamos la funcion desde el component
    component.crearUsuario();
    expect(spyRouter).toHaveBeenCalled();
  });

  it('Verificamos funcion editarUsuario()', () => {
    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate').and.callThrough();
    // Mockeamos el objeto usuario que recibe la funcion editarUsuario
    const mockUsuario = {
      apPaterno: 'Benito5',
      apMaterno: 'Torres5',
      nombre: 'Fazio5',
      tipoDoc: 'DNI',
      numDoc: '85474584',
      telef: '',
      correo: 'fbenito@novatronic.com',
      fecNac: '01/02/2021',
      usuario: 'fbenito5',
      estado: 'HABILITADO',
      rol: 'sixbioweb_ver',
      audFecModif: '05/03/2021',
      audFecCreac: '22/02/2021',
      audUsuModif: 'jburgos6',
      audUsuCreac: 'abenito5',
      navigationId: 3
    };
    // Llamamos nuestra función
    component.editarUsuario(mockUsuario);

    // Verificamos que llame a Router y su funcion navigate
    expect(spyRouter).toHaveBeenCalled();
    // Verificamos que entidad pase el mismo mockUsuario
    expect(component.entidad).toEqual(mockUsuario);

  });

  it('Validamos funcion resetearContrasenia(usuario: String, correo: string) ', () => {
    // Mockeamos la data que recibira la funcion
    const mockData = {
      usuarioAResetear : 'Testing123',
      correo: 'Pruebas123@prueba.com'
    };
    // Espiamos la funcion showModal
    const spyModal = spyOn(component , 'showModal').and.callThrough();
    // Llamamos la funcion  resetearContrasenia
    component.resetearContrasenia(mockData.usuarioAResetear, mockData.correo);
    // Verificamos el mensaje que se asigna a la variable messageModal
    expect(component.messageModal).toEqual('Do you want to reset the user password?');
    // Verificamos que fuera llamada la funcion showModal
    expect(spyModal).toHaveBeenCalled();
    // Verificamos el seteo de datos al objeto dataRequestResetear
    expect(component.dataRequestResetear).toEqual(mockData);
  });

  it('Verificamos funcion detalleUsuario()', () => {
    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate').and.callThrough();
    // Mockeamos el objeto usuario que recibe la funcion editarUsuario
    const mockUsuario = {
      apPaterno: 'Benito5',
      apMaterno: 'Torres5',
      nombre: 'Fazio5',
      tipoDoc: 'DNI',
      numDoc: '85474584',
      telef: '',
      correo: 'fbenito@novatronic.com',
      fecNac: '01/02/2021',
      usuario: 'fbenito5',
      estado: 'HABILITADO',
      rol: 'sixbioweb_ver',
      audFecModif: '05/03/2021',
      audFecCreac: '22/02/2021',
      audUsuModif: 'jburgos6',
      audUsuCreac: 'abenito5',
      navigationId: 3
    };
    // Llamamos nuestra función
    component.detalleUsuario(mockUsuario);

    // Verificamos que llame a Router y su funcion navigate
    expect(spyRouter).toHaveBeenCalled();
    // Verificamos que entidad pase el mismo mockUsuario
    expect(component.entidad).toEqual(mockUsuario);
  });



  it('Verificar funcion limpiarDatosServicioEliminarUsuario()', () => {
      // Espiamos la funcion listarUsuarios()
      const spy = spyOn(component, 'listarUsuarios').and.callThrough();
      component.limpiarDatosServicioEliminarUsuario();

      // Verificamos que llame la funcion listarUsuarios()
      expect(spy).toHaveBeenCalled();
      // Verificamos los valores de la variables
      expect(component.dataRequestEliminarUsuario.usuariosAEliminar).toEqual([]);
      expect(component.usuariosPorEliminar).toEqual([]);
      expect(component.listaUsuariosSixbio).toEqual([]);
      expect(component.usuariosCheckeados).toEqual(new Set());
    });


  it('Verificamos listarUsuarios()', () => {
      // Espiamos el servicio serviceListarUsuario
      const spyService = spyOn(servicio, 'serviceListarUsuario').and.callThrough();
      // Llamamos la funcion
      component.listarUsuarios();

      // Verificamos valor de la variable listarUsuarioOk
      expect(component.listarUsuarioOk ).toBeFalsy();
      // Verificamos que llamen al servicio
      expect(spyService).toHaveBeenCalled();

      // Mockeamos un DataResponse para verificar las condiciones de las respuesta
      // Verificamos la primera condicion COD_OPERACION_SATISFACTORIA_SCA

    const mockDataResponse = {
      listarUsuario: {
          codigoRespuesta: '0000',
          mensajeRespuesta: 'OPERACION REALIZADA SATISFACTORIAMENTE',
          listUsuarios: [
                  {
                      'apPaterno': 'Palomino',
                      'apMaterno': 'Astupuma',
                      'nombre': 'Katherine',
                      'tipoDoc': 'DNI',
                      'numDoc': '45896328',
                      'telef': '',
                      'correo': 'kpalomino@novatronic.com',
                      'fecNac': '03/07/1990',
                      'usuario': 'kpalomino',
                      'estado': 'INHABILITADO',
                      'rol': 'BIOCoreAgrupador',
                      'usuarioLogueado': null,
                      'audFecModif': '13/05/2020',
                      'audFecCreac': '17/07/2019',
                      'audUsuModif': 'ADMIN',
                      'audUsuCreac': 'admin'
                  },
                  {
                      'apPaterno': 'Chunga',
                      'apMaterno': 'Chapilliquen',
                      'nombre': 'Jose',
                      'tipoDoc': 'DNI',
                      'numDoc': '71487247',
                      'telef': '999999999',
                      'correo': 'djimenez@novatronic.com',
                      'fecNac': '01/11/2019',
                      'usuario': 'jchunga',
                      'estado': 'INHABILITADO',
                      'rol': 'stiAdmin',
                      'usuarioLogueado': null,
                      'audFecModif': '16/12/2020',
                      'audFecCreac': '26/11/2019',
                      'audUsuModif': 'ADMIN',
                      'audUsuCreac': 'admin'
                  },
                  {
                      'apPaterno': 'Alcala',
                      'apMaterno': 'Marcos',
                      'nombre': 'Giancarlo',
                      'tipoDoc': 'DNI',
                      'numDoc': '42728713',
                      'telef': '',
                      'correo': 'evaldivia@novatronic.com',
                      'fecNac': '03/10/1984',
                      'usuario': 'galcala',
                      'estado': 'HABILITADO',
                      'rol': 'ROL_ADM',
                      'usuarioLogueado': null,
                      'audFecModif': '30/12/2020',
                      'audFecCreac': '15/07/2019',
                      'audUsuModif': 'galcala',
                      'audUsuCreac': 'admin'
                  }]
          },
      codigoRespuesta: '0000',
      mensajeRespuesta: 'OPERACION REALIZADA SATISFACTORIAMENTE'
      };
      // con CallFake retornamos nuestra data previamente Mockeada
      const spyServiceFake = spyOn(servicio, 'serviceListarUsuario').and.callFake(() => {
          return of (mockDataResponse);
      });
      // Espiamos la funcion discriminarUsuarioSixbio de nuestro componente
      const spydiscriminarUsuarioSixbio = spyOn(component, 'discriminarUsuarioSixbio').and.callThrough();
      // Espiamos si llama nuestro TOAST
      const spyToast = spyOn(component.toast, 'addToast').and.callThrough();
      // Llamamos nuestra funcion desde el componente
      component.listarUsuarios();
      // Verificamos que se llame nuestro servicio Fake
      expect(spyServiceFake).toHaveBeenCalled();
      // Verificamos que se llame la funcion discriminarUsuarioSixbio
      expect(spydiscriminarUsuarioSixbio).toHaveBeenCalled();
      // Verificamos que la data recibida por listaUsuarios  sea igual al mockDataResponse.listarUsuario.listUsuarios
      console.log('Verificamos TEST listarUsuarios');
      console.log(component.listaUsuarios );
      // Verificamos que valor de listarUsuarioOk sea TRUE
      expect(component.listarUsuarioOk ).toBeTruthy();
      console.log('Test pruebas component listarUsuarioOk es ' + component.listarUsuarioOk);

      // Probamos en caso nos devuelva una respuestsa Constant.COD_ERROR_SESION
      mockDataResponse.codigoRespuesta = Constant.COD_ERROR_SESION;
      // espiamos nuestra funcion salir()
      const spySalir = spyOn(component, 'salir').and.callThrough();
      // Llamamos nuestra funcion
      component.listarUsuarios();
      // Verificamos que se llame nuestra funcion salir()
      expect(spySalir).toHaveBeenCalled();


      // Probamos el caso donde no coincida con ningun mensaje enviado
      mockDataResponse.codigoRespuesta = 'No hay coincidencia';

      // Llamamos nuestra funcion listarUsuarios()
      component.listarUsuarios();
      // Verificamos el valor de listarUsuarioOk si ahora es TRUE
      expect(component.listarUsuarioOk).toBeTruthy();
      // Verificamos si fue llamado nuestro Toast
      expect(spyToast).toHaveBeenCalled();

      // Probabamos el caso que nos retorne un ERROR el servicio
      const spyError = spyOn(servicio, 'serviceListarUsuario').and.returnValue(throwError('Esto es un Error!!'));
      // Llamamos nuestra funcion listarUsuarios()
      component.listarUsuarios();
      // Verificamos si fue llamado nuestro Servicio Error
      expect(spyError).toHaveBeenCalled();
      // Verificamos si fue llamado nuestro Toast
      expect(spyToast).toHaveBeenCalled();
      // Verificamos que se llame nuestra funcion salir()
      expect(spySalir).toHaveBeenCalled();
      // Verificamos el valor de listarUsuarioOk si ahora es TRUE
      expect(component.listarUsuarioOk).toBeTruthy();
    });


  it('Verificamos funcion discriminarUsuarioSixbio()', () => {
    // Mockeamos una lista de los usuarios a recibir
    const mockListUsuarios = [
        {
              'apPaterno': 'Benito5',
              'apMaterno': 'Torres5',
              'nombre': 'Fazio5',
              'tipoDoc': 'DNI',
              'numDoc': '85474584',
              'telef': '',
              'correo': 'fbenito@novatronic.com',
              'fecNac': '01/02/2021',
              'usuario': 'fbenito5',
              'estado': 'HABILITADO',
              'rol': 'sixbioweb_ver',
              'usuarioLogueado': null,
              'audFecModif': '16/03/2021',
              'audFecCreac': '22/02/2021',
              'audUsuModif': 'jburgos6',
              'audUsuCreac': 'abenito5'
          },
          {
              'apPaterno': 'khjkhjkhjkhj',
              'apMaterno': 'khjkhjk',
              'nombre': 'hkjhkhj',
              'tipoDoc': 'DNI',
              'numDoc': '99999999',
              'telef': '546546546',
              'correo': 'jburgos@novatronic.com',
              'fecNac': '18/03/2021',
              'usuario': 'jburgos5',
              'estado': 'RESETEADO',
              'rol': 'sixbioweb_ver',
              'usuarioLogueado': null,
              'audFecModif': '24/03/2021',
              'audFecCreac': '05/03/2021',
              'audUsuModif': 'jburgos5',
              'audUsuCreac': 'jburgos6'
          },
          {
              'apPaterno': 'Benito',
              'apMaterno': 'Torres',
              'nombre': 'Fazio',
              'tipoDoc': 'DNI',
              'numDoc': '70652784',
              'telef': '333333333',
              'correo': 'fbenito@novatronic.com',
              'fecNac': '30/01/1995',
              'usuario': 'abenito',
              'estado': 'HABILITADO',
              'rol': 'sixbioweb_adm',
              'usuarioLogueado': null,
              'audFecModif': '17/03/2021',
              'audFecCreac': '28/01/2020',
              'audUsuModif': 'abenito',
              'audUsuCreac': 'admin'
          },
          {
              'apPaterno': 'fbenito6',
              'apMaterno': 'fbenito6',
              'nombre': 'fbenito6',
              'tipoDoc': 'DNI',
              'numDoc': '85748574',
              'telef': '',
              'correo': 'fbenito@novatronic.com',
              'fecNac': '01/02/2021',
              'usuario': 'fbenito6',
              'estado': 'HABILITADO',
              'rol': 'sixbioweb_ver',
              'usuarioLogueado': null,
              'audFecModif': '15/03/2021',
              'audFecCreac': '23/02/2021',
              'audUsuModif': 'jburgos6',
              'audUsuCreac': 'fbenito5'
          },
          {
              'apPaterno': '345345345',
              'apMaterno': '34534534',
              'nombre': '45345',
              'tipoDoc': 'DNI',
              'numDoc': '12345678',
              'telef': '',
              'correo': 'jburgos@novatronic.com',
              'fecNac': '19/03/2021',
              'usuario': 'GrillaTest',
              'estado': 'CREADO',
              'rol': 'sixbioweb_adm',
              'usuarioLogueado': null,
              'audFecModif': '',
              'audFecCreac': '24/03/2021',
              'audUsuModif': '',
              'audUsuCreac': 'jburgos6'
          },
          {
              'apPaterno': 'fsdfds',
              'apMaterno': 'fsdfsd',
              'nombre': 'sdfdsfsd',
              'tipoDoc': 'DNI',
              'numDoc': '99999999',
              'telef': '',
              'correo': 'jburgos@novatronic.com',
              'fecNac': '04/03/2003',
              'usuario': 'TestFormdd',
              'estado': 'CREADO',
              'rol': 'sixbioweb_ver',
              'usuarioLogueado': null,
              'audFecModif': '',
              'audFecCreac': '24/03/2021',
              'audUsuModif': '',
              'audUsuCreac': 'jburgos6'
          },
          {
              'apPaterno': 'Benito',
              'apMaterno': 'Torres',
              'nombre': 'Fazio',
              'tipoDoc': 'DNI',
              'numDoc': '70652784',
              'telef': '',
              'correo': 'fbenito@novatronic.com',
              'fecNac': '12/07/1995',
              'usuario': 'abenito5',
              'estado': 'HABILITADO',
              'rol': 'sixbioweb_ver',
              'usuarioLogueado': null,
              'audFecModif': '11/03/2021',
              'audFecCreac': '24/07/2020',
              'audUsuModif': 'abenito5',
              'audUsuCreac': 'admin'
          },
          {
              'apPaterno': 'sdfsdf',
              'apMaterno': 'sdfsd',
              'nombre': 'asdfsdf',
              'tipoDoc': 'DNI',
              'numDoc': '99999999',
              'telef': '',
              'correo': 'jburgos@novatronic.com',
              'fecNac': '12/03/2003',
              'usuario': 'testingemail',
              'estado': 'INHABILITADO',
              'rol': 'sixbioweb_adm',
              'usuarioLogueado': null,
              'audFecModif': '15/03/2021',
              'audFecCreac': '15/03/2021',
              'audUsuModif': 'jburgos6',
              'audUsuCreac': 'jburgos6'
          },
          {
              'apPaterno': 'test',
              'apMaterno': 'test',
              'nombre': 'Test',
              'tipoDoc': 'RUC',
              'numDoc': '99999999999',
              'telef': '111111111',
              'correo': 'jburgos@novatronic.com',
              'fecNac': '15/03/2003',
              'usuario': 'jburgos3',
              'estado': 'HABILITADO',
              'rol': 'sixbioweb_ver',
              'usuarioLogueado': null,
              'audFecModif': '18/03/2021',
              'audFecCreac': '15/03/2021',
              'audUsuModif': 'jburgos3',
              'audUsuCreac': 'jburgos6'
          },
          {
              'apPaterno': 'ffffffffffffffffffffffffffffffffffffffffffffffffff',
              'apMaterno': 'ffffffffffffffffffffffffffffffffffffffffffffffffff',
              'nombre': 'ffffffffffffffffffffffffffffffffffffffffffffffffff',
              'tipoDoc': 'RUC',
              'numDoc': '99999999999',
              'telef': '111111111',
              'correo': 'jburgos@novatronic.com',
              'fecNac': '13/03/2003',
              'usuario': 'TestUnitffffffffffff',
              'estado': 'RESETEADO',
              'rol': 'sixbioweb_ver',
              'usuarioLogueado': null,
              'audFecModif': '15/03/2021',
              'audFecCreac': '15/03/2021',
              'audUsuModif': 'TestUnitffffffffffff',
              'audUsuCreac': 'jburgos6'
          },
          {
              'apPaterno': 'Meza',
              'apMaterno': '0000000',
              'nombre': 'Gustavo',
              'tipoDoc': 'DNI',
              'numDoc': '12345678',
              'telef': '',
              'correo': 'gmeza@novatronic.com',
              'fecNac': '06/03/2021',
              'usuario': 'gmeza5',
              'estado': 'CREADO',
              'rol': 'sixbioweb_adm',
              'usuarioLogueado': null,
              'audFecModif': '',
              'audFecCreac': '19/03/2021',
              'audUsuModif': '',
              'audUsuCreac': 'admin'
          },
          {
              'apPaterno': 'Meza',
              'apMaterno': '0000000',
              'nombre': 'Gustavo',
              'tipoDoc': 'DNI',
              'numDoc': '99999999',
              'telef': '',
              'correo': 'gmeza@novatronic.com',
              'fecNac': '03/02/2003',
              'usuario': 'gmeza3',
              'estado': 'CREADO',
              'rol': 'sixbioweb_ver',
              'usuarioLogueado': null,
              'audFecModif': '',
              'audFecCreac': '19/03/2021',
              'audUsuModif': '',
              'audUsuCreac': 'jburgos6'
          },
          {
              'apPaterno': 'sdfdsfds',
              'apMaterno': 'fdsfdsfds',
              'nombre': 'sdfsdfffffffffffffffffffffffffffffffffffffffffffff',
              'tipoDoc': 'RUC',
              'numDoc': '99999999999',
              'telef': '111111111',
              'correo': 'jburgos@novatronic.com',
              'fecNac': '06/03/2003',
              'usuario': 'login123',
              'estado': 'HABILITADO',
              'rol': 'sixbioweb_ver',
              'usuarioLogueado': null,
              'audFecModif': '19/03/2021',
              'audFecCreac': '19/03/2021',
              'audUsuModif': 'login123',
              'audUsuCreac': 'jburgos6'
          },
          {
              'apPaterno': 'vdfgfdgfdgfdg',
              'apMaterno': 'fdgfdgfdg',
              'nombre': 'prueba 2',
              'tipoDoc': 'RUC',
              'numDoc': '99999999999',
              'telef': '243243243',
              'correo': 'jburgos@novatronic.com',
              'fecNac': '01/03/2021',
              'usuario': 'j12345jhsdsds',
              'estado': 'CREADO',
              'rol': 'sixbioweb_ver',
              'usuarioLogueado': null,
              'audFecModif': '',
              'audFecCreac': '10/03/2021',
              'audUsuModif': '',
              'audUsuCreac': 'jburgos6'
          },
          {
              'apPaterno': '44444444444444444444444444444444444444444444444444',
              'apMaterno': '44444444444444444444444444444444444444444444444444',
              'nombre': 'prueba 2',
              'tipoDoc': 'DNI',
              'numDoc': '44444444',
              'telef': '444444444',
              'correo': 'jburgos@novatronic.com',
              'fecNac': '01/03/2021',
              'usuario': 'aaasdsx43testttttttt',
              'estado': 'CREADO',
              'rol': 'sixbioweb_ver',
              'usuarioLogueado': null,
              'audFecModif': '',
              'audFecCreac': '10/03/2021',
              'audUsuModif': '',
              'audUsuCreac': 'jburgos6'
          },
          {
              'apPaterno': 'asdasdasdasd',
              'apMaterno': 'asdasdas',
              'nombre': 'rasdasdasd',
              'tipoDoc': 'DNI',
              'numDoc': '12345678',
              'telef': '232432432',
              'correo': 'jburgos@novatronic.com',
              'fecNac': '07/03/2021',
              'usuario': 'Angular9',
              'estado': 'RESETEADO',
              'rol': 'sixbioweb_ver',
              'usuarioLogueado': null,
              'audFecModif': '25/03/2021',
              'audFecCreac': '10/03/2021',
              'audUsuModif': 'Angular9',
              'audUsuCreac': 'jburgos6'
          },
          {
              'apPaterno': 'sdafsdfsdf',
              'apMaterno': 'sdfsdfsdfsd',
              'nombre': 'asdfsdfgsd',
              'tipoDoc': 'RUC',
              'numDoc': '99999999999',
              'telef': '234444444',
              'correo': 'jburgos@novatronic.com',
              'fecNac': '19/03/2003',
              'usuario': 'Primerlogin',
              'estado': 'CREADO',
              'rol': 'sixbioweb_adm',
              'usuarioLogueado': null,
              'audFecModif': '',
              'audFecCreac': '23/03/2021',
              'audUsuModif': '',
              'audUsuCreac': 'jburgos6'
          },
          {
              'apPaterno': 'Burgos',
              'apMaterno': 'Tejada',
              'nombre': 'Jaime',
              'tipoDoc': 'DNI',
              'numDoc': '12345678',
              'telef': '',
              'correo': 'jburgos@novatronic.com',
              'fecNac': '25/02/2021',
              'usuario': 'jburgos6',
              'estado': 'HABILITADO',
              'rol': 'sixbioweb_adm',
              'usuarioLogueado': null,
              'audFecModif': '22/03/2021',
              'audFecCreac': '23/02/2021',
              'audUsuModif': 'jburgos6',
              'audUsuCreac': 'admin'
          },
          {
              'apPaterno': '4  44',
              'apMaterno': '44  444',
              'nombre': '44  444',
              'tipoDoc': 'DNI',
              'numDoc': '12345678',
              'telef': '111111111',
              'correo': 'jburgos@novatronic.com',
              'fecNac': '05/03/2021',
              'usuario': 'jburgos1',
              'estado': 'CREADO',
              'rol': 'sixbioweb_adm',
              'usuarioLogueado': null,
              'audFecModif': '17/03/2021',
              'audFecCreac': '16/03/2021',
              'audUsuModif': 'jburgos6',
              'audUsuCreac': 'jburgos3'
          },
          {
              'apPaterno': 'sdfsdfsd',
              'apMaterno': 'fsdfsdfsd',
              'nombre': 'dsfsd',
              'tipoDoc': 'RUC',
              'numDoc': '99999999999',
              'telef': '',
              'correo': 'jburgos@novatronic.com',
              'fecNac': '18/02/2003',
              'usuario': 'TestFormaadsdsdfgggg',
              'estado': 'CREADO',
              'rol': 'sixbioweb_adm',
              'usuarioLogueado': null,
              'audFecModif': '',
              'audFecCreac': '17/03/2021',
              'audUsuModif': '',
              'audUsuCreac': 'jburgos6'
          }];
    // Mockeamos los ROLES_AGRUPADORES_VALOR
    Constant.ROLES_AGRUPADORES_VALOR = ['sixbioweb_adm', 'sixbioweb_ver'];
    // Asiganamos nuestro mockListUsuarios
    component.listaUsuarios = mockListUsuarios;
    // Llamamos nuestra funcion discriminarUsuarioSixbio()
    component.discriminarUsuarioSixbio();
    // Mockeamos un usuario valido de las lista para verificar posteriormente que sea contenido
    const userValido =         {
      'apPaterno': 'Benito5',
      'apMaterno': 'Torres5',
      'nombre': 'Fazio5',
      'tipoDoc': 'DNI',
      'numDoc': '85474584',
      'telef': '',
      'correo': 'fbenito@novatronic.com',
      'fecNac': '01/02/2021',
      'usuario': 'fbenito5',
      'estado': 'HABILITADO',
      'rol': 'sixbioweb_ver',
      'usuarioLogueado': null,
      'audFecModif': '16/03/2021',
      'audFecCreac': '22/02/2021',
      'audUsuModif': 'jburgos6',
      'audUsuCreac': 'abenito5'
     };
    console.log('Valor desde Pruebas de LISTA_USUARIOSSIXBIO ES');
    console.log(component.listaUsuariosSixbio);
    // Verificiamos que se contenga la lista discriminaba por la funcion al que cumplen con la condicion
    expect(component.listaUsuariosSixbio).toContain(userValido);
  });


  it('Verificamos funncion mensajeEspera()', () => {
    // Espiamos nuestro Toast
    const spyToast = spyOn(component.toast, 'addToast').and.callThrough();
    // Llamamos la funcion mensajeEspera()
    component.mensajeEspera();
    // Verificamos si fue llamado nuestro Toast
    expect(spyToast).toHaveBeenCalled();
  });

  it('Verificamos funcion  exportarExcel()', () => {
    // Espiamos setTipoReporte del componente
    const spyTipoReporte = spyOn(component, 'setTipoReporte').and.callThrough();
    // Espiamos la funcion exportar del componente
    const spyExportar = spyOn(component, 'exportar').and.callThrough();
    //  Espaimos la funcion setFechaActual del componente
    const spyFechaActual = spyOn(component, 'setFechaActual').and.callThrough();
    // Llamamos nuestra funcion exportarExcel()
    component.exportarExcel();
    // Verificamos que nuestra funcion Exportar fuera llamado
    expect(spyExportar).toHaveBeenCalled();
    // Verificamos que nuestra funcion setFechaActual fuera llamada
    expect(spyFechaActual).toHaveBeenCalled();
    // Verificamos que sea llamado setTipoReporte
    expect(spyTipoReporte).toHaveBeenCalled();

  });

  it('Verificamos funcion setTipoReporte()', () => {
    // Llamamos la funcion setTipoReporte()
    component.setTipoReporte('Test');
    // Verificamos la asignacion a DataRequest
    expect(component.dataRequestReporte.tipoReporte).toEqual('Test');
  });

  it('Verificamos funcion exportarPDF()', () => {
    // Espiamos setTipoReporte del componente
    const spyTipoReporte = spyOn(component, 'setTipoReporte').and.callThrough();
    // Espiamos la funcion exportar del componente
    const spyExportar = spyOn(component, 'exportar').and.callThrough();
    //  Espaimos la funcion setFechaActual del componente
    const spyFechaActual = spyOn(component, 'setFechaActual').and.callThrough();
    // Llamamos nuestra funcion exportarExcel()
    component.exportarPDF();
    // Verificamos que nuestra funcion Exportar fuera llamado
    expect(spyExportar).toHaveBeenCalled();
    // Verificamos que nuestra funcion setFechaActual fuera llamada
    expect(spyFechaActual).toHaveBeenCalled();
    // Verificamos que sea llamado setTipoReporte
    expect(spyTipoReporte).toHaveBeenCalled();
  });

  it('Verificamos funcion exportar()', () => {
    // Espiamos nuestra funcion setdataRequestReporte()
    const spySetDataRequestReporte = spyOn(component, 'setdataRequestReporte').and.callThrough();
    // Espiamos la llamada de nuestra funcion mensajeEspera()
    const spyMensajeEspera = spyOn(component, 'mensajeEspera').and.callThrough();
    // Espiamos nuestro servicio serviceGenerarReporte
    const spyService = spyOn(servicio, 'serviceGenerarReporte').and.callThrough();
    // Mockeamos dataRequestReporte
    const mockDataRequestReporte = {
      listaUsuarios: Constant.LISTA_USUARIOS_FILTRADO,
        tipoReporte: 'excel',
        f_usuario: 'TestUser123',
        f_nombre: 'NameUser',
        f_apellidos: 'LastNameUser',
        f_estado: 'TestStatus',
    };
    // Probamos el caso donde  Constant.LISTA_USUARIOS_FILTRADO.length >0
    let mockListaUsuariosFiltrados = [
      {
          'apPaterno': 'Benito5',
          'apMaterno': 'Torres5',
          'nombre': 'Fazio5',
          'tipoDoc': 'DNI',
          'numDoc': '85474584',
          'telef': '',
          'correo': 'fbenito@novatronic.com',
          'fecNac': '01/02/2021',
          'usuario': 'fbenito5',
          'estado': 'HABILITADO',
          'rol': 'sixbioweb_ver',
          'usuarioLogueado': null,
          'audFecModif': '16/03/2021',
          'audFecCreac': '22/02/2021',
          'audUsuModif': 'jburgos6',
          'audUsuCreac': 'abenito5'
      },
      {
          'apPaterno': 'khjkhjkhjkhj',
          'apMaterno': 'khjkhjk',
          'nombre': 'hkjhkhj',
          'tipoDoc': 'DNI',
          'numDoc': '99999999',
          'telef': '546546546',
          'correo': 'jburgos@novatronic.com',
          'fecNac': '18/03/2021',
          'usuario': 'jburgos5',
          'estado': 'RESETEADO',
          'rol': 'sixbioweb_ver',
          'usuarioLogueado': null,
          'audFecModif': '24/03/2021',
          'audFecCreac': '05/03/2021',
          'audUsuModif': 'jburgos5',
          'audUsuCreac': 'jburgos6'
      },
      {
          'apPaterno': 'Benito',
          'apMaterno': 'Torres',
          'nombre': 'Fazio',
          'tipoDoc': 'DNI',
          'numDoc': '70652784',
          'telef': '333333333',
          'correo': 'fbenito@novatronic.com',
          'fecNac': '30/01/1995',
          'usuario': 'abenito',
          'estado': 'HABILITADO',
          'rol': 'sixbioweb_adm',
          'usuarioLogueado': null,
          'audFecModif': '17/03/2021',
          'audFecCreac': '28/01/2020',
          'audUsuModif': 'abenito',
          'audUsuCreac': 'admin'
      },
      {
          'apPaterno': 'fbenito6',
          'apMaterno': 'fbenito6',
          'nombre': 'fbenito6',
          'tipoDoc': 'DNI',
          'numDoc': '85748574',
          'telef': '',
          'correo': 'fbenito@novatronic.com',
          'fecNac': '01/02/2021',
          'usuario': 'fbenito6',
          'estado': 'HABILITADO',
          'rol': 'sixbioweb_ver',
          'usuarioLogueado': null,
          'audFecModif': '15/03/2021',
          'audFecCreac': '23/02/2021',
          'audUsuModif': 'jburgos6',
          'audUsuCreac': 'fbenito5'
      },
      {
          'apPaterno': '345345345',
          'apMaterno': '34534534',
          'nombre': '45345',
          'tipoDoc': 'DNI',
          'numDoc': '12345678',
          'telef': '',
          'correo': 'jburgos@novatronic.com',
          'fecNac': '19/03/2021',
          'usuario': 'GrillaTest',
          'estado': 'CREADO',
          'rol': 'sixbioweb_adm',
          'usuarioLogueado': null,
          'audFecModif': '',
          'audFecCreac': '24/03/2021',
          'audUsuModif': '',
          'audUsuCreac': 'jburgos6'
      },
      {
          'apPaterno': 'fsdfds',
          'apMaterno': 'fsdfsd',
          'nombre': 'sdfdsfsd',
          'tipoDoc': 'DNI',
          'numDoc': '99999999',
          'telef': '',
          'correo': 'jburgos@novatronic.com',
          'fecNac': '04/03/2003',
          'usuario': 'TestFormdd',
          'estado': 'CREADO',
          'rol': 'sixbioweb_ver',
          'usuarioLogueado': null,
          'audFecModif': '',
          'audFecCreac': '24/03/2021',
          'audUsuModif': '',
          'audUsuCreac': 'jburgos6'
      },
      {
          'apPaterno': 'Benito',
          'apMaterno': 'Torres',
          'nombre': 'Fazio',
          'tipoDoc': 'DNI',
          'numDoc': '70652784',
          'telef': '',
          'correo': 'fbenito@novatronic.com',
          'fecNac': '12/07/1995',
          'usuario': 'abenito5',
          'estado': 'HABILITADO',
          'rol': 'sixbioweb_ver',
          'usuarioLogueado': null,
          'audFecModif': '11/03/2021',
          'audFecCreac': '24/07/2020',
          'audUsuModif': 'abenito5',
          'audUsuCreac': 'admin'
      },
      {
          'apPaterno': 'sdfsdf',
          'apMaterno': 'sdfsd',
          'nombre': 'asdfsdf',
          'tipoDoc': 'DNI',
          'numDoc': '99999999',
          'telef': '',
          'correo': 'jburgos@novatronic.com',
          'fecNac': '12/03/2003',
          'usuario': 'testingemail',
          'estado': 'INHABILITADO',
          'rol': 'sixbioweb_adm',
          'usuarioLogueado': null,
          'audFecModif': '15/03/2021',
          'audFecCreac': '15/03/2021',
          'audUsuModif': 'jburgos6',
          'audUsuCreac': 'jburgos6'
      },
      {
          'apPaterno': 'test',
          'apMaterno': 'test',
          'nombre': 'Test',
          'tipoDoc': 'RUC',
          'numDoc': '99999999999',
          'telef': '111111111',
          'correo': 'jburgos@novatronic.com',
          'fecNac': '15/03/2003',
          'usuario': 'jburgos3',
          'estado': 'HABILITADO',
          'rol': 'sixbioweb_ver',
          'usuarioLogueado': null,
          'audFecModif': '18/03/2021',
          'audFecCreac': '15/03/2021',
          'audUsuModif': 'jburgos3',
          'audUsuCreac': 'jburgos6'
      },
      {
          'apPaterno': 'ffffffffffffffffffffffffffffffffffffffffffffffffff',
          'apMaterno': 'ffffffffffffffffffffffffffffffffffffffffffffffffff',
          'nombre': 'ffffffffffffffffffffffffffffffffffffffffffffffffff',
          'tipoDoc': 'RUC',
          'numDoc': '99999999999',
          'telef': '111111111',
          'correo': 'jburgos@novatronic.com',
          'fecNac': '13/03/2003',
          'usuario': 'TestUnitffffffffffff',
          'estado': 'RESETEADO',
          'rol': 'sixbioweb_ver',
          'usuarioLogueado': null,
          'audFecModif': '15/03/2021',
          'audFecCreac': '15/03/2021',
          'audUsuModif': 'TestUnitffffffffffff',
          'audUsuCreac': 'jburgos6'
      },
      {
          'apPaterno': 'Meza',
          'apMaterno': '0000000',
          'nombre': 'Gustavo',
          'tipoDoc': 'DNI',
          'numDoc': '12345678',
          'telef': '',
          'correo': 'gmeza@novatronic.com',
          'fecNac': '06/03/2021',
          'usuario': 'gmeza5',
          'estado': 'CREADO',
          'rol': 'sixbioweb_adm',
          'usuarioLogueado': null,
          'audFecModif': '',
          'audFecCreac': '19/03/2021',
          'audUsuModif': '',
          'audUsuCreac': 'admin'
      },
      {
          'apPaterno': 'Meza',
          'apMaterno': '0000000',
          'nombre': 'Gustavo',
          'tipoDoc': 'DNI',
          'numDoc': '99999999',
          'telef': '',
          'correo': 'gmeza@novatronic.com',
          'fecNac': '03/02/2003',
          'usuario': 'gmeza3',
          'estado': 'CREADO',
          'rol': 'sixbioweb_ver',
          'usuarioLogueado': null,
          'audFecModif': '',
          'audFecCreac': '19/03/2021',
          'audUsuModif': '',
          'audUsuCreac': 'jburgos6'
      },
      {
          'apPaterno': 'sdfdsfds',
          'apMaterno': 'fdsfdsfds',
          'nombre': 'sdfsdfffffffffffffffffffffffffffffffffffffffffffff',
          'tipoDoc': 'RUC',
          'numDoc': '99999999999',
          'telef': '111111111',
          'correo': 'jburgos@novatronic.com',
          'fecNac': '06/03/2003',
          'usuario': 'login123',
          'estado': 'HABILITADO',
          'rol': 'sixbioweb_ver',
          'usuarioLogueado': null,
          'audFecModif': '19/03/2021',
          'audFecCreac': '19/03/2021',
          'audUsuModif': 'login123',
          'audUsuCreac': 'jburgos6'
      },
      {
          'apPaterno': 'vdfgfdgfdgfdg',
          'apMaterno': 'fdgfdgfdg',
          'nombre': 'prueba 2',
          'tipoDoc': 'RUC',
          'numDoc': '99999999999',
          'telef': '243243243',
          'correo': 'jburgos@novatronic.com',
          'fecNac': '01/03/2021',
          'usuario': 'j12345jhsdsds',
          'estado': 'CREADO',
          'rol': 'sixbioweb_ver',
          'usuarioLogueado': null,
          'audFecModif': '',
          'audFecCreac': '10/03/2021',
          'audUsuModif': '',
          'audUsuCreac': 'jburgos6'
      },
      {
          'apPaterno': '44444444444444444444444444444444444444444444444444',
          'apMaterno': '44444444444444444444444444444444444444444444444444',
          'nombre': 'prueba 2',
          'tipoDoc': 'DNI',
          'numDoc': '44444444',
          'telef': '444444444',
          'correo': 'jburgos@novatronic.com',
          'fecNac': '01/03/2021',
          'usuario': 'aaasdsx43testttttttt',
          'estado': 'CREADO',
          'rol': 'sixbioweb_ver',
          'usuarioLogueado': null,
          'audFecModif': '',
          'audFecCreac': '10/03/2021',
          'audUsuModif': '',
          'audUsuCreac': 'jburgos6'
      },
      {
          'apPaterno': 'asdasdasdasd',
          'apMaterno': 'asdasdas',
          'nombre': 'rasdasdasd',
          'tipoDoc': 'DNI',
          'numDoc': '12345678',
          'telef': '232432432',
          'correo': 'jburgos@novatronic.com',
          'fecNac': '07/03/2021',
          'usuario': 'Angular9',
          'estado': 'RESETEADO',
          'rol': 'sixbioweb_ver',
          'usuarioLogueado': null,
          'audFecModif': '25/03/2021',
          'audFecCreac': '10/03/2021',
          'audUsuModif': 'Angular9',
          'audUsuCreac': 'jburgos6'
      },
      {
          'apPaterno': 'sdafsdfsdf',
          'apMaterno': 'sdfsdfsdfsd',
          'nombre': 'asdfsdfgsd',
          'tipoDoc': 'RUC',
          'numDoc': '99999999999',
          'telef': '234444444',
          'correo': 'jburgos@novatronic.com',
          'fecNac': '19/03/2003',
          'usuario': 'Primerlogin',
          'estado': 'CREADO',
          'rol': 'sixbioweb_adm',
          'usuarioLogueado': null,
          'audFecModif': '',
          'audFecCreac': '23/03/2021',
          'audUsuModif': '',
          'audUsuCreac': 'jburgos6'
      },
      {
          'apPaterno': 'Burgos',
          'apMaterno': 'Tejada',
          'nombre': 'Jaime',
          'tipoDoc': 'DNI',
          'numDoc': '12345678',
          'telef': '',
          'correo': 'jburgos@novatronic.com',
          'fecNac': '25/02/2021',
          'usuario': 'jburgos6',
          'estado': 'HABILITADO',
          'rol': 'sixbioweb_adm',
          'usuarioLogueado': null,
          'audFecModif': '22/03/2021',
          'audFecCreac': '23/02/2021',
          'audUsuModif': 'jburgos6',
          'audUsuCreac': 'admin'
      },
      {
          'apPaterno': '4  44',
          'apMaterno': '44  444',
          'nombre': '44  444',
          'tipoDoc': 'DNI',
          'numDoc': '12345678',
          'telef': '111111111',
          'correo': 'jburgos@novatronic.com',
          'fecNac': '05/03/2021',
          'usuario': 'jburgos1',
          'estado': 'CREADO',
          'rol': 'sixbioweb_adm',
          'usuarioLogueado': null,
          'audFecModif': '17/03/2021',
          'audFecCreac': '16/03/2021',
          'audUsuModif': 'jburgos6',
          'audUsuCreac': 'jburgos3'
      },
      {
          'apPaterno': 'sdfsdfsd',
          'apMaterno': 'fsdfsdfsd',
          'nombre': 'dsfsd',
          'tipoDoc': 'RUC',
          'numDoc': '99999999999',
          'telef': '',
          'correo': 'jburgos@novatronic.com',
          'fecNac': '18/02/2003',
          'usuario': 'TestFormaadsdsdfgggg',
          'estado': 'CREADO',
          'rol': 'sixbioweb_adm',
          'usuarioLogueado': null,
          'audFecModif': '',
          'audFecCreac': '17/03/2021',
          'audUsuModif': '',
          'audUsuCreac': 'jburgos6'
      }
     ];
    // Asignamos nuestra data Mockeada a nuestra constante LISTA_USUARIO_FILTRADO
    Constant.LISTA_USUARIOS_FILTRADO = mockListaUsuariosFiltrados;
    // asignamos nuestro mockDataRequestReporte al dataRequestReporte
    component.dataRequestReporte = mockDataRequestReporte;
    // Llamamos nuestra funcion exportar() y le pasamos sus parametros de prueba
    component.exportar('reporteUsuario TEST' + '.xls');


    // Verificamos que fuera llamada nuestra funcion setdataRequestReporte
    expect(spySetDataRequestReporte).toHaveBeenCalled();
    // Verificamos que fuera llamada nuestra funcion mensajeEspera
    expect(spyMensajeEspera).toHaveBeenCalled();
    // Verificamos que fuera llamado nuestro servicio serviceGenerarReporte
    expect(spyService).toHaveBeenCalled();

    // Probamos el caso donde Probamos el caso donde  Constant.LISTA_USUARIOS_FILTRADO.length = 0
    mockListaUsuariosFiltrados = [];
    // Espiamos nuestro Toast
    const spyToast = spyOn(component.toast, 'addToast').and.callThrough();
    // Llamamos nuestra funcion exportar() y le pasamos sus parametros de prueba
    component.exportar('reporteUsuario TEST' + '.xls');
    // verificamos que llame nuestro toast
    expect(spyToast).toHaveBeenCalled();






    // Probamos el primer caso donde nuestro servicio nos responda Constant.COD_ERROR_SESION
    // Espiamos nuestra funcion salir()
    const spySalir = spyOn(component, 'salir').and.callThrough();
    // Mockeamos nuestro DataResponse
    const mockDataResponse = {
      codigoRespuesta : Constant.COD_ERROR_SESION
    };
    const spyFake = spyOn(servicio, 'serviceGenerarReporte').and.callFake(() => {
      return of (mockDataResponse);
    });
    // Llamamos nuestra funcion exportar() y le pasamos sus parametros de prueba
    component.exportar('reporteUsuario TEST' + '.xls');
    // Verificamos que llame nuestra funcion salir()
    expect(spySalir).toHaveBeenCalled();
    // Verificamos que llame nuestro servicio Fake
    expect(spyFake).toHaveBeenCalled();

    // Probamos el caso donde guarda el Reporte , es decir no responde error alguno
    //  Espiamos la funcion SaveAs
    const spySaveAs = spyOn(FileSaver, 'saveAs').and.stub();
    mockDataResponse.codigoRespuesta = '';
    // Llamamos nuestra funcion exportar() y le pasamos sus parametros de prueba
    component.exportar('reporteUsuario TEST' + '.xls');
    // Verificamos que fuera llamado
    expect(spySaveAs).toHaveBeenCalled();

    // Verificamos el caso donde nuestro servicio nos responda un Error
    const spyError = spyOn(servicio, 'serviceGenerarReporte').and.returnValue(throwError('Esto un Error!!'));
    // Llamamos nuestra funcion exportar() y le pasamos sus parametros de prueba
    component.exportar('reporteUsuario TEST' + '.xls');
    // Verificamos que llame nuestro servicio con error
    expect(spyError).toHaveBeenCalled();
    // Verificamos que llame nuestra funcion salir()
    expect(spySalir).toHaveBeenCalled();
    // verificamos que llame nuestro toast
    expect(spyToast).toHaveBeenCalled();
  });

  it('Verificamos funcion eliminarUsuarios()', () => {
    // Mockeamos nuestro objeto new Set()
    let mockTest = new Set([]);
    // asignamos los valores de los usuarios a userValues
    const userValues = [
          {
            apMaterno: 'Torres5',
            apPaterno: 'Benito5',
            audFecCreac: '22/02/2021',
            audFecModif: '16/03/2021',
            audUsuCreac: 'abenito5',
            audUsuModif: 'jburgos6',
            correo: 'fbenito@novatronic.com',
            estado: 'HABILITADO',
            fecNac: '01/02/2021',
            nombre: 'Fazio5',
            rol: 'sixbioweb_ver',
            telef: '',
            tipoDoc: 'DNI',
            usuario: 'fbenito5',
            usuarioLogueado: null,
          },
          {
            apMaterno: 'khjkhjk',
            apPaterno: 'khjkhjkhjkhj',
            audFecCreac: '05/03/2021',
            audFecModif: '24/03/2021',
            audUsuCreac: 'jburgos6',
            audUsuModif: 'jburgos5',
            correo: 'jburgos@novatronic.com',
            estado: 'RESETEADO',
            fecNac: '18/03/2021',
            nombre: 'hkjhkhj',
            numDoc: '99999999',
            rol: 'sixbioweb_ver',
            telef: '546546546',
            tipoDoc: 'DNI',
            usuario: 'jburgos5',
            usuarioLogueado: null,
        },
        {
          apMaterno: 'asdasdas',
          apPaterno: 'asdasdasdasd',
          audFecCreac: '10/03/2021',
          audFecModif: '25/03/2021',
          audUsuCreac: 'jburgos6',
          audUsuModif: 'Angular9',
          correo: 'jburgos@novatronic.com',
          estado: 'RESETEADO',
          fecNac: '07/03/2021',
          nombre: 'rasdasdasd',
          numDoc: '12345678',
          rol: 'sixbioweb_ver',
          telef: '232432432',
          tipoDoc: 'DNI',
          usuario: 'Angular9',
          usuarioLogueado: null,
        },
        {
          apMaterno: '44  444',
          apPaterno: '4  44',
          audFecCreac: '16/03/2021',
          audFecModif: '17/03/2021',
          audUsuCreac: 'jburgos3',
          audUsuModif: 'jburgos6',
          correo: 'jburgos@novatronic.com',
          estado: 'CREADO',
          fecNac: '05/03/2021',
          nombre: '44  444',
          numDoc: '12345678',
          rol: 'sixbioweb_adm',
          telef: '111111111',
          tipoDoc: 'DNI',
          usuario: 'jburgos1',
          usuarioLogueado: null,
    }];
    // agregamos nuestros usuarios al mockTest
    for ( let i = 0 ; i < userValues.length - 1 ; i++) {
        mockTest.add(userValues[i]);
    }
    // Lo asignamos a nuestra variable usuarioCheckeados
    component.usuariosCheckeados = mockTest;
    // Espiamos la llamada a nuestro modal
    const spyShowModal = spyOn(component, 'showModal').and.callThrough();

    // Validaremos la primera condicion donde es this.usuariosCheckeados.size > 0
    // Llamamos nuestra funcion
    component.eliminarUsuarios();
    // Verificamos que tengan el mismo tamaño
    expect(component.usuariosCheckeados.size).toEqual(mockTest.size);
    // Verificamos que sea usuarioCheckeados.size >0
    expect(component.usuariosCheckeados.size).toBeGreaterThan(0);
    // Verificamos el mensaje del modal
    expect(component.messageModal).toEqual('You are sure to perform the removal');
    // Verificamos la llamada a nuestro Modal
    expect(spyShowModal).toHaveBeenCalled();

    // Verificamos el caso donde usuariosCheckeados sea 0
    mockTest = new Set([]);
    // Lo asignamos a nuestra variable usuarioCheckeados
    component.usuariosCheckeados = mockTest;
    // Espiamos la llamada a nuestro Toast
    const spyToast = spyOn(component.toast, 'addToast').and.callThrough();
    // Llamamos nuestra funcion
    component.eliminarUsuarios();
    // Verificamos que nuestro sea llamado
    expect(spyToast).toHaveBeenCalled();
  });

  it('Verificamos funcion setDataRequestEliminarUsuario()', () => {
    // Mockeamos nuestro objeto new Set()
    const mockTest = new Set([]);
    // asignamos los valores de los usuarios a userValues
    const userValues = [
          {
            apMaterno: 'Torres5',
            apPaterno: 'Benito5',
            audFecCreac: '22/02/2021',
            audFecModif: '16/03/2021',
            audUsuCreac: 'abenito5',
            audUsuModif: 'jburgos6',
            correo: 'fbenito@novatronic.com',
            estado: 'HABILITADO',
            fecNac: '01/02/2021',
            nombre: 'Fazio5',
            rol: 'sixbioweb_ver',
            telef: '',
            tipoDoc: 'DNI',
            usuario: 'fbenito5',
            usuarioLogueado: null,
          },
          {
            apMaterno: 'khjkhjk',
            apPaterno: 'khjkhjkhjkhj',
            audFecCreac: '05/03/2021',
            audFecModif: '24/03/2021',
            audUsuCreac: 'jburgos6',
            audUsuModif: 'jburgos5',
            correo: 'jburgos@novatronic.com',
            estado: 'RESETEADO',
            fecNac: '18/03/2021',
            nombre: 'hkjhkhj',
            numDoc: '99999999',
            rol: 'sixbioweb_ver',
            telef: '546546546',
            tipoDoc: 'DNI',
            usuario: 'jburgos5',
            usuarioLogueado: null,
          },
          {
          apMaterno: 'asdasdas',
          apPaterno: 'asdasdasdasd',
          audFecCreac: '10/03/2021',
          audFecModif: '25/03/2021',
          audUsuCreac: 'jburgos6',
          audUsuModif: 'Angular9',
          correo: 'jburgos@novatronic.com',
          estado: 'RESETEADO',
          fecNac: '07/03/2021',
          nombre: 'rasdasdasd',
          numDoc: '12345678',
          rol: 'sixbioweb_ver',
          telef: '232432432',
          tipoDoc: 'DNI',
          usuario: 'Angular9',
          usuarioLogueado: null,
          },
          {
          apMaterno: '44  444',
          apPaterno: '4  44',
          audFecCreac: '16/03/2021',
          audFecModif: '17/03/2021',
          audUsuCreac: 'jburgos3',
          audUsuModif: 'jburgos6',
          correo: 'jburgos@novatronic.com',
          estado: 'CREADO',
          fecNac: '05/03/2021',
          nombre: '44  444',
          numDoc: '12345678',
          rol: 'sixbioweb_adm',
          telef: '111111111',
          tipoDoc: 'DNI',
          usuario: 'jburgos1',
          usuarioLogueado: null,
    }];

    // agregamos nuestros usuarios al mockTest
    for ( let i = 0 ; i < userValues.length - 1 ; i++) {
        mockTest.add(userValues[i]);
    }
    // Lo asignamos a nuestra variable usuarioCheckeados
    component.usuariosCheckeados = mockTest;
    component.setDataRequestEliminarUsuario();
    let testing: any;  testing = component.usuariosPorEliminar;
    // Verificamos que usuariosPorEliminar contenga los usuarios que le pasamos
    for ( let i = 0 ; i < userValues.length - 1 ; i++) {
      expect(component.usuariosPorEliminar).toContain(userValues[i]['usuario']);
    }
    // Verificamos que dataRequestEliminarUsuario.usuariosAEliminar contenga los usuarios que le pasamos
    for ( let i = 0 ; i < userValues.length - 1 ; i++) {
      expect(component.dataRequestEliminarUsuario.usuariosAEliminar).toContain(userValues[i]['usuario']);
    }
  });

  it('Verificamos funcion llamadaServicioEliminar()', () => {
    // Mockeamos nuestro objeto new Set()
    const mockTest = new Set([]);
    // asignamos los valores de los usuarios a userValues
    const userValues = [
              {
                apMaterno: 'Torres5',
                apPaterno: 'Benito5',
                audFecCreac: '22/02/2021',
                audFecModif: '16/03/2021',
                audUsuCreac: 'abenito5',
                audUsuModif: 'jburgos6',
                correo: 'fbenito@novatronic.com',
                estado: 'HABILITADO',
                fecNac: '01/02/2021',
                nombre: 'Fazio5',
                rol: 'sixbioweb_ver',
                telef: '',
                tipoDoc: 'DNI',
                usuario: 'fbenito5',
                usuarioLogueado: null,
              },
              {
                apMaterno: 'khjkhjk',
                apPaterno: 'khjkhjkhjkhj',
                audFecCreac: '05/03/2021',
                audFecModif: '24/03/2021',
                audUsuCreac: 'jburgos6',
                audUsuModif: 'jburgos5',
                correo: 'jburgos@novatronic.com',
                estado: 'RESETEADO',
                fecNac: '18/03/2021',
                nombre: 'hkjhkhj',
                numDoc: '99999999',
                rol: 'sixbioweb_ver',
                telef: '546546546',
                tipoDoc: 'DNI',
                usuario: 'jburgos5',
                usuarioLogueado: null,
              },
              {
              apMaterno: 'asdasdas',
              apPaterno: 'asdasdasdasd',
              audFecCreac: '10/03/2021',
              audFecModif: '25/03/2021',
              audUsuCreac: 'jburgos6',
              audUsuModif: 'Angular9',
              correo: 'jburgos@novatronic.com',
              estado: 'RESETEADO',
              fecNac: '07/03/2021',
              nombre: 'rasdasdasd',
              numDoc: '12345678',
              rol: 'sixbioweb_ver',
              telef: '232432432',
              tipoDoc: 'DNI',
              usuario: 'Angular9',
              usuarioLogueado: null,
              },
              {
              apMaterno: '44  444',
              apPaterno: '4  44',
              audFecCreac: '16/03/2021',
              audFecModif: '17/03/2021',
              audUsuCreac: 'jburgos3',
              audUsuModif: 'jburgos6',
              correo: 'jburgos@novatronic.com',
              estado: 'CREADO',
              fecNac: '05/03/2021',
              nombre: '44  444',
              numDoc: '12345678',
              rol: 'sixbioweb_adm',
              telef: '111111111',
              tipoDoc: 'DNI',
              usuario: 'jburgos1',
              usuarioLogueado: null,
        }];
    // agregamos nuestros usuarios al mockTest
    for ( let i = 0 ; i < userValues.length - 1 ; i++) {
      mockTest.add(userValues[i]);
    }
    // Lo asignamos a nuestra variable usuarioCheckeados
    component.usuariosCheckeados = mockTest;
    // Espiamos nuestra funcion setDataRequestEliminarUsuario
    const spysetDataRequestEliminarUsuario = spyOn(component, 'setDataRequestEliminarUsuario').and.callThrough();
    // Espiamos nuestro servicio
    const spyserviceEliminarUsuario = spyOn(servicio, 'serviceEliminarUsuario').and.callThrough();

    // Llamamos nuestra funcion
    component.llamadaServicioEliminar();

    // Verificamos que llame nuestro servicio serviceEliminarUsuario
    expect(spyserviceEliminarUsuario).toHaveBeenCalled();
    // Verificamos que llame nuestra funcion setDataRequestEliminarUsuario()
    expect(spysetDataRequestEliminarUsuario).toHaveBeenCalled();


    // Ahora mockeamos el codigo de respuesta Constant.COD_OPERACION_SATISFACTORIA_SCA , para la primera condicion
    const mockDataResponseSer = {
      codigoRespuesta :  Constant.COD_OPERACION_SATISFACTORIA_SCA,
      usuariosEliminados : ''
    };
    // Creamos un servicio Fake de nuestro serviceEliminarUsuario
    const spyFake = spyOn(servicio, 'serviceEliminarUsuario').and.callFake(() => {
      return of (mockDataResponseSer);
    });
    // Espiamos la llamada de limpiarDatosServicioEliminarUsuario
    const spylimpiarDatosServicioEliminarUsuario = spyOn(component, 'limpiarDatosServicioEliminarUsuario').and.callThrough();
    // Fakeo la llamada del Toast ya que nos da un error en caso contrario con libreria Translate
    const spyToastFake = spyOn(component.toast, 'addToast').and.callFake(() => ' nada');
    // Llamamos nuestra funcion
    component.llamadaServicioEliminar();
    // Verificamos que llame nuestra ffuncion limpiarDatosServicioEliminarUsuario del component
    expect(spylimpiarDatosServicioEliminarUsuario).toHaveBeenCalled();
    // Verificamos la llamada de ToastFake
    expect(spyToastFake);


    // Probamos la siguiente caso cuando codigoRespuesta es Constant.COD_ERROR_ELIMINAR_USUARIO
     mockDataResponseSer.codigoRespuesta = Constant.COD_ERROR_ELIMINAR_USUARIO;
     // Probamos el caso donde no hay ningun usuario a eliminar y usuariosEliminados es vacio
     mockDataResponseSer.usuariosEliminados = '';
    // Llamamos nuestra funcion
    component.llamadaServicioEliminar();
     // Verificamos que llame nuestra ffuncion limpiarDatosServicioEliminarUsuario del component
     expect(spylimpiarDatosServicioEliminarUsuario).toHaveBeenCalled();
     // Verificamos la llamada de nuestro toastFake
     expect(spyToastFake).toHaveBeenCalled();

    // Ahora probamos el siguiente caso cuando es diferente vacio
    mockDataResponseSer.usuariosEliminados = 'Algo Diferente';
    // Llamamos nuestra funcion
    component.llamadaServicioEliminar();
    // Verificamos que llame nuestra ffuncion limpiarDatosServicioEliminarUsuario del component
    expect(spylimpiarDatosServicioEliminarUsuario).toHaveBeenCalled();
    // Verificamos la llamada de nuestro toastFake
    expect(spyToastFake).toHaveBeenCalled();

    // Ahora probamos el caso que el servicio nos responda con Constant.COD_ERROR_SESION
    mockDataResponseSer.codigoRespuesta = Constant.COD_ERROR_SESION;
    // Creamos nuestro espia para la funcion salir()
    const spySalir = spyOn(component, 'salir').and.callThrough();
    // Llamamos nuestra funcion
    component.llamadaServicioEliminar();
    // Verificamos que llama nuestra funcion salir()
    expect(spySalir).toHaveBeenCalled();

    // Ahora probamos el caso donde no nos de ninguna respuesta mapeada
    mockDataResponseSer.codigoRespuesta = 'Ninguno de los Anteriores';
    // Llamamos nuestra funcion
    component.llamadaServicioEliminar();
    // Verificamos que llame nuestra ffuncion limpiarDatosServicioEliminarUsuario del component
    expect(spylimpiarDatosServicioEliminarUsuario).toHaveBeenCalled();
    // Verificamos la llamada de nuestro toastFake
    expect(spyToastFake).toHaveBeenCalled();

    // Ahora probamos el caso que el Servicio nos retorne un ERROR
    const spyError = spyOn(servicio, 'serviceEliminarUsuario').and.returnValue(throwError('Esto es un Error de serviceEliminarUsuario'));
    // Llamamos nuestra funcion
    component.llamadaServicioEliminar();
    // Verificamos que llame nuestro espia del error del Servicio serviceEliminarUsuario
    expect(spyError).toHaveBeenCalled();
    // Verificamos que llame nuestra ffuncion limpiarDatosServicioEliminarUsuario del component
    expect(spylimpiarDatosServicioEliminarUsuario).toHaveBeenCalled();
    // Verificamos la llamada de nuestro toastFake
    expect(spyToastFake).toHaveBeenCalled();
    // Verificamos que llama nuestra funcion salir()
    expect(spySalir).toHaveBeenCalled();

  });

  it('Verificamos la funcion evento()', () => {
    // Probamos el primer caso del Switch
    component.messageModal = 'You are sure to perform the removal';
    // El caso donde aceptar = TRUE
    component.aceptar = true;
    // Espiamos nuestra funcion del component llamadaServicioEliminar();
    const spyLlamadaServicioEliminar = spyOn(component, 'llamadaServicioEliminar').and.callThrough();
    // Llamamos nuestra funcion evento()
    component.evento();
    // Verificamos el espia que fuera llamado
    expect(spyLlamadaServicioEliminar).toHaveBeenCalled();




  });


});
