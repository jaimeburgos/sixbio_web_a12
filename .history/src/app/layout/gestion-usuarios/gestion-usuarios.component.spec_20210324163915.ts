import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { of, Subject, throwError } from 'rxjs';
import { EnvService } from 'src/app/env.service';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { ExporterService } from 'src/app/services/exporter.service';
import { ServicioService } from 'src/app/servicio/servicio.service';

import { GestionUsuariosComponent } from './gestion-usuarios.component';
class FakeRouter {
  navigate(params) {

  }
}

class FakeActivatedRoute {
  // params: Observable<any> = Observable.empty();
  private subject = new Subject();

  push( valor ) {
    this.subject.next( valor );
  }
  get params() {
    return this.subject.asObservable();
  }
}


fdescribe('GestionUsuariosComponent', () => {
  let component: GestionUsuariosComponent;
  let fixture: ComponentFixture<GestionUsuariosComponent>;
  let servicio: ServicioService;
  let translateService: TranslateService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionUsuariosComponent ],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
        RouterTestingModule,
        ModalModule.forRoot(),
      ],
      providers: [
        ExporterService,
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService ,
        {provide: Router , useClass: FakeRouter},
        {provide: ToastrService, useClass: ToastrService},
        { provide: ActivatedRoute, useClass: FakeActivatedRoute },
      ],

      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionUsuariosComponent);
    servicio = TestBed.inject(ServicioService);
    translateService = TestBed.inject(TranslateService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Componente Gestion-Usuario creado correctamente', () => {
    expect(component).toBeTruthy();
  });

  /*
    filterUsuario : new FormControl('', Validators.compose([ Validators.maxLength(20) ])),
    filterApellido : new FormControl('', Validators.compose([Validators.maxLength(50)  ])),
    filterNombre : new FormControl('', Validators.compose([ Validators.maxLength(50) ])),
  */
  it('Campo "filterUsuario" de gestionarUsuarioForm longitud maxima es 20', () => {
    const filterUsuario = component.gestionarUsuarioForm.get('filterUsuario');
    filterUsuario.setValue('12345678901234567890');
    expect(filterUsuario.valid).toBeTruthy();
    filterUsuario.setValue('123456789012345678901');
    expect(filterUsuario.valid).toBeFalsy();
  });

  it('Campo "filterApellido" de gestionarUsuarioForm longitud maxima es 50', () => {
    const filterApellido = component.gestionarUsuarioForm.get('filterApellido');
    filterApellido.setValue('12345678901234567890123456789012345678901234567890');
    expect(filterApellido.valid).toBeTruthy();
    filterApellido.setValue('123456789012345678901234567890123456789012345678901');
    expect(filterApellido.valid).toBeFalsy();
  });


  it('Campo "filterNombre" de gestionarUsuarioForm longitud maxima es 50', () => {
    const filterNombre = component.gestionarUsuarioForm.get('filterNombre');
    filterNombre.setValue('12345678901234567890123456789012345678901234567890');
    expect(filterNombre.valid).toBeTruthy();
    filterNombre.setValue('123456789012345678901234567890123456789012345678901');
    expect(filterNombre.valid).toBeFalsy();
  });


  it('filterUsuario con caracteres permitidos y no permitidos en gestionarUsuarioForm', () => {
    const filterUsuario = component.gestionarUsuarioForm.get('filterUsuario');
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b'];
    const U_No_Permitidos =
    ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
    '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
    '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]', '          '];
    // Bucle para caracteres permitidos en usuario gestionarUsuarioForm
    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      filterUsuario.setValue(U_Permitidos[j]);
      expect(filterUsuario.valid).toBe(true);
      // Cuando sea verdadero !filterUsuario.valid no se cumple la condicion de verificacion
      if ( !filterUsuario.valid) {
        console.log('Fallo en el caracter Permitido gestionarUsuarioForm ' + U_Permitidos[j] );
      }
    }
    // Bucle para caracteres no permitidos en Usuario gestionarUsuarioForm
    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      filterUsuario.setValue(U_No_Permitidos[i]);
      expect(filterUsuario.valid).toBe(false);
      // Cuando sea verdadero no se cumple la condicion de verificacion
      if (filterUsuario.valid) {
        console.log('Fallo en el caracter No Permitido  gestionarUsuarioForm' + U_No_Permitidos[i] );
      }
    }
  });

  it('filterApellido con caracteres permitidos y no permitidos en gestionarUsuarioForm', () => {
    const filterApellido = component.gestionarUsuarioForm.get('filterApellido');
    const U_Permitidos = ['aaaaaaaa', 'ccccb bbbb', 'c123bbb4b', 'rrr33 444 44'];
    const U_No_Permitidos =
    ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
    '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
    '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]', 'ñññññññ'];

    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      filterApellido.setValue(U_Permitidos[j]);
      expect(filterApellido.valid).toBe(true);
      if ( !filterApellido.valid) {
        console.log('Fallo en el caracter Permitido gestionarUsuarioForm ' + U_Permitidos[j] );
      }
    }

    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      filterApellido.setValue(U_No_Permitidos[i]);
      expect(filterApellido.valid).toBe(false);
      if (filterApellido.valid) {
        console.log('Fallo en el caracter No Permitido  gestionarUsuarioForm' + U_No_Permitidos[i] );
      }
    }
  });

  it('filterNombre con caracteres permitidos y no permitidos en gestionarUsuarioForm', () => {
    const filterNombre = component.gestionarUsuarioForm.get('filterNombre');
    const U_Permitidos = ['aaaaaaaa', 'ccccb bbbb', 'c123bbb4b', 'rrr33 444 44'];
    const U_No_Permitidos =
    ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
    '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
    '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]', 'ñññññññ'];

    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      filterNombre.setValue(U_Permitidos[j]);
      expect(filterNombre.valid).toBe(true);
      if ( !filterNombre.valid) {
        console.log('Fallo en el caracter Permitido gestionarUsuarioForm ' + U_Permitidos[j] );
      }
    }

    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      filterNombre.setValue(U_No_Permitidos[i]);
      expect(filterNombre.valid).toBe(false);
      if (filterNombre.valid) {
        console.log('Fallo en el caracter No Permitido  gestionarUsuarioForm' + U_No_Permitidos[i] );
      }
    }
  });

  it('Verificamos el funcionamiento esperado de la funcion salir()', () => {
    const vacio: Object = {  };
    component.dataRequest = vacio;
    // Creamos un espia para el servicio
    const spyService = spyOn(servicio, 'serviceSignOff').and.callThrough();
    // Llamamos nuestra funcion salir() desde el componente
    component.salir();
    // Verificamos que nuestro servicio fuera llamado
    expect(spyService).toHaveBeenCalled();

    // Mockeamos dataResponse para probar la validaciones , el primer caso
    const mockDataResponse = {
      codigoRespuesta : Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    };
    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate').and.callThrough();

    // Fakeamos una llamada al servicio para que nos de el mock de lo que deseamos
    const spyServiceFake = spyOn(servicio, 'serviceSignOff').and.callFake(() => {
      return of (mockDataResponse);
    });

    component.salir();

    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();
    // Verificamos que nuestro servicio Fake fuera llamado
    expect(spyServiceFake).toHaveBeenCalled();


    // Comprobamos la siguiente condicion cuando es diferente de COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    mockDataResponse.codigoRespuesta = 'Diferente de correcto';

    // Volvemos a llamar a la funcion
    component.salir();

    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();


    // Ahora verificamos en caso de un error al llamar al servicio
    const spyError = spyOn(servicio, 'serviceSignOff').and.returnValue(throwError('Esto es un error'));
    // Volvemos a llamar a la funcion
    component.salir();
    // Verificamos que fuera llamado con error nuestro servicio
    expect(spyError).toHaveBeenCalled();
    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();

  });

  it('Verificamos funcion decline()', () => {
    // Espiamos al modal para verificar la funcion hide
    const spyModal = spyOn(component.modal, 'hide').and.callThrough();
    // Llamamos a la funcion del component
    component.decline();
    // Verificamos que fuera llamado la funcion hide del modal y que aceptar sea FALSE
    expect(component.aceptar).toBeFalsy();
    expect(spyModal).toHaveBeenCalled();
  });


  it('Verificamos funcion confirm()', () => {
    // Espiamos al modal para verificar la funcion hide
    const spyModal = spyOn(component.modal, 'hide').and.callThrough();
    // Llamamos a la funcion del component
    component.confirm();
    // Verificamos que fuera llamado la funcion hide del modal y que aceptar sea TRUE
    expect(component.aceptar).toBeTruthy();
    expect(spyModal).toHaveBeenCalled();
  });

  it('Verieficamos funcion showModal()', () => {
    // Espiamos al modal para verificar la funcion show
    const spyModal = spyOn(component.modal, 'show').and.callThrough();
    component.showModal();
    // Verificamos que fuera llamado la funcion show del modal
    expect(spyModal).toHaveBeenCalled();
  });


  it('Verificamos funcion crearUsuario()', () => {
    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate');
    // Llamamos la funcion desde el component
    component.crearUsuario();
    expect(spyRouter).toHaveBeenCalled();
  });

  it('Verificamos funcion editarUsuario()', () => {
    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate').and.callThrough();
    // Mockeamos el objeto usuario que recibe la funcion editarUsuario
    const mockUsuario = {
      apPaterno: 'Benito5',
      apMaterno: 'Torres5',
      nombre: 'Fazio5',
      tipoDoc: 'DNI',
      numDoc: '85474584',
      telef: '',
      correo: 'fbenito@novatronic.com',
      fecNac: '01/02/2021',
      usuario: 'fbenito5',
      estado: 'HABILITADO',
      rol: 'sixbioweb_ver',
      audFecModif: '05/03/2021',
      audFecCreac: '22/02/2021',
      audUsuModif: 'jburgos6',
      audUsuCreac: 'abenito5',
      navigationId: 3
    };
    // Llamamos nuestra función
    component.editarUsuario(mockUsuario);

    // Verificamos que llame a Router y su funcion navigate
    expect(spyRouter).toHaveBeenCalled();
    // Verificamos que entidad pase el mismo mockUsuario
    expect(component.entidad).toEqual(mockUsuario);

  });

  it('Validamos funcion resetearContrasenia(usuario: String, correo: string) ', () => {
    // Mockeamos la data que recibira la funcion
    const mockData = {
      usuarioAResetear : 'Testing123',
      correo: 'Pruebas123@prueba.com'
    };
    // Espiamos la funcion showModal
    const spyModal = spyOn(component , 'showModal').and.callThrough();
    // Llamamos la funcion  resetearContrasenia
    component.resetearContrasenia(mockData.usuarioAResetear, mockData.correo);
    // Verificamos el mensaje que se asigna a la variable messageModal
    expect(component.messageModal).toEqual('Do you want to reset the user password?');
    // Verificamos que fuera llamada la funcion showModal
    expect(spyModal).toHaveBeenCalled();
    // Verificamos el seteo de datos al objeto dataRequestResetear
    expect(component.dataRequestResetear).toEqual(mockData);
  });

  it('Verificamos funcion detalleUsuario()', () => {
    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate').and.callThrough();
    // Mockeamos el objeto usuario que recibe la funcion editarUsuario
    const mockUsuario = {
      apPaterno: 'Benito5',
      apMaterno: 'Torres5',
      nombre: 'Fazio5',
      tipoDoc: 'DNI',
      numDoc: '85474584',
      telef: '',
      correo: 'fbenito@novatronic.com',
      fecNac: '01/02/2021',
      usuario: 'fbenito5',
      estado: 'HABILITADO',
      rol: 'sixbioweb_ver',
      audFecModif: '05/03/2021',
      audFecCreac: '22/02/2021',
      audUsuModif: 'jburgos6',
      audUsuCreac: 'abenito5',
      navigationId: 3
    };
    // Llamamos nuestra función
    component.detalleUsuario(mockUsuario);

    // Verificamos que llame a Router y su funcion navigate
    expect(spyRouter).toHaveBeenCalled();
    // Verificamos que entidad pase el mismo mockUsuario
    expect(component.entidad).toEqual(mockUsuario);
  });


/*
  limpiarDatosServicioEliminarUsuario() {
    this.usuariosCheckeados.clear();
    this.usuariosPorEliminar = [];
    this.dataRequestEliminarUsuario.usuariosAEliminar = [];
    this.listaUsuariosSixbio = [];
    this.listarUsuarios();
    }
*/
    it('Verificar funcion limpiarDatosServicioEliminarUsuario()', () => {
      // Espiamos la funcion listarUsuarios()
      const spy = spyOn(component, 'listarUsuarios').and.callThrough();
      component.limpiarDatosServicioEliminarUsuario();

      // Verificamos que llame la funcion listarUsuarios()
      expect(spy).toHaveBeenCalled();
      // Verificamos los valores de la variables
      expect(component.dataRequestEliminarUsuario.usuariosAEliminar).toEqual([]);
      expect(component.usuariosPorEliminar).toEqual([]);
      expect(component.listaUsuariosSixbio).toEqual([]);
      expect(component.usuariosCheckeados).toEqual(new Set());
    });


    it('Verificamos listarUsuarios()', () => {
      // Espiamos el servicio serviceListarUsuario
      const spyService = spyOn(servicio, 'serviceListarUsuario').and.callThrough();

      // Llamamos la funcion
      component.listarUsuarios();

      // Verificamos valor de la variable listarUsuarioOk
      expect(component.listarUsuarioOk ).toBeFalsy();
      // Verificamos que llamen al servicio
      expect(spyService).toHaveBeenCalled();

      // Mockeamos un DataResponse para verificar las condiciones de las respuesta
      // Verificamos la primera condicion COD_OPERACION_SATISFACTORIA_SCA
    /*  const mockDataResponse = {
        codigoRespuesta: '0000',
        listarUsuario: {
          codigoRespuesta: '0000',
          listUsuarios: {
            0: {
            apMaterno: 'Astupuma',
            apPaterno: 'Palomino',
            audFecCreac: '17/07/2019',
            audFecModif: '13/05/2020',
            audUsuCreac: 'admin',
            audUsuModif: 'ADMIN',
            correo: 'kpalomino@novatronic.com',
            estado: 'INHABILITADO',
            fecNac: '03/07/1990',
            nombre: 'Katherine',
            numDoc: '45896328',
            rol: 'BIOCoreAgrupador',
            telef: '',
            tipoDoc: 'DNI',
            usuario: 'kpalomino',
            usuarioLogueado: null}
          },
          mensajeRespuesta: 'OPERACION REALIZADA SATISFACTORIAMENTE'
        },
        mensajeRespuesta: 'OPERACION REALIZADA SATISFACTORIAMENTE',
      };*/

const mockDataResponse = {

    'listarUsuario': {
        'codigoRespuesta': '0000',
        'mensajeRespuesta': 'OPERACION REALIZADA SATISFACTORIAMENTE',
        'listUsuarios': [
            {
                'apPaterno': 'Palomino',
                'apMaterno': 'Astupuma',
                'nombre': 'Katherine',
                'tipoDoc': 'DNI',
                'numDoc': '45896328',
                'telef': '',
                'correo': 'kpalomino@novatronic.com',
                'fecNac': '03/07/1990',
                'usuario': 'kpalomino',
                'estado': 'INHABILITADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '13/05/2020',
                'audFecCreac': '17/07/2019',
                'audUsuModif': 'ADMIN',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'Nuflo',
                'apMaterno': 'Gamarra',
                'nombre': 'Isaac',
                'tipoDoc': 'DNI',
                'numDoc': '75328398',
                'telef': '',
                'correo': 'inuflo@novatronic.com',
                'fecNac': '21/06/1997',
                'usuario': 'FDOperador',
                'estado': 'INHABILITADO',
                'rol': 'Operadorfirmdig',
                'usuarioLogueado': null,
                'audFecModif': '14/01/2020',
                'audFecCreac': '27/03/2019',
                'audUsuModif': 'ADMIN',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'Rivas',
                'apMaterno': 'Estrada',
                'nombre': 'Manuel',
                'tipoDoc': 'DNI',
                'numDoc': '41836380',
                'telef': '992783250',
                'correo': 'empresamanuel@gmail.com',
                'fecNac': '08/11/2019',
                'usuario': 'mrivas2',
                'estado': 'HABILITADO',
                'rol': 'rolagrupadorconsultor',
                'usuarioLogueado': null,
                'audFecModif': '04/03/2020',
                'audFecCreac': '04/03/2020',
                'audUsuModif': 'mrivas2',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'JoelPRueba22',
                'apMaterno': 'JoelPRueba22fwefwfwfwf',
                'nombre': 'JoelPRueba2255',
                'tipoDoc': 'DNI',
                'numDoc': '48287359',
                'telef': '',
                'correo': 'jdelosrios@novatronic.com',
                'fecNac': '01/07/2020',
                'usuario': 'JoelPRueba22',
                'estado': 'RESETEADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '03/09/2020',
                'audFecCreac': '13/07/2020',
                'audUsuModif': 'JoelPRueba22',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'efeefefef3555555555555555555',
                'apMaterno': 'efeefefef3444444444444444444',
                'nombre': 'efeefefef3444444444444444444444444444',
                'tipoDoc': 'DNI',
                'numDoc': '48287359',
                'telef': '',
                'correo': 'jdelosrios@novatronic.com',
                'fecNac': '05/09/2020',
                'usuario': 'efeefefef3',
                'estado': 'CREADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '03/09/2020',
                'audFecCreac': '03/09/2020',
                'audUsuModif': 'jdelosrios2',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'Bajalqui',
                'apMaterno': 'Carrasco',
                'nombre': 'Edgar Gustavo',
                'tipoDoc': 'DNI',
                'numDoc': '12345678',
                'telef': '965773387',
                'correo': 'edgar.bajalqui@gmail.com',
                'fecNac': '29/12/1989',
                'usuario': 'bioadmin',
                'estado': 'IMPORTADO',
                'rol': 'RolAgrpSIXBIO',
                'usuarioLogueado': null,
                'audFecModif': '22/07/2019',
                'audFecCreac': '10/04/2019',
                'audUsuModif': '',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'Benito',
                'apMaterno': 'Torres',
                'nombre': 'Fazio44',
                'tipoDoc': 'DNI',
                'numDoc': '70652784',
                'telef': '',
                'correo': 'fbenito@novatronic.com',
                'fecNac': '30/01/1995',
                'usuario': 'abenito4',
                'estado': 'HABILITADO',
                'rol': 'AGRUPADOR_ADM',
                'usuarioLogueado': null,
                'audFecModif': '24/02/2021',
                'audFecCreac': '09/06/2020',
                'audUsuModif': 'abenito4',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'De los rios5',
                'apMaterno': 'Guimack',
                'nombre': 'Joel Prueba Edicion7',
                'tipoDoc': 'DNI',
                'numDoc': '10126886',
                'telef': '',
                'correo': 'jdelosrios@novatronic.com',
                'fecNac': '20/05/1994',
                'usuario': 'jdelosrios2',
                'estado': 'INHABILITADO',
                'rol': 'ROL_ADM',
                'usuarioLogueado': null,
                'audFecModif': '24/10/2020',
                'audFecCreac': '23/01/2020',
                'audUsuModif': 'ADMIN',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'scaCaidoLevantado',
                'apMaterno': 'scaCaidoLevantado',
                'nombre': 'scaCaidoLevantado',
                'tipoDoc': 'DNI',
                'numDoc': '48287359',
                'telef': '',
                'correo': 'jdelosrios@novatronic.com',
                'fecNac': '01/08/2020',
                'usuario': 'scaCaidoLevantado',
                'estado': 'CREADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '10/08/2020',
                'audUsuModif': '',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'Villegas',
                'apMaterno': 'Diaz',
                'nombre': 'Juan Diego',
                'tipoDoc': 'DNI',
                'numDoc': '12842823',
                'telef': '',
                'correo': 'jvillegas@novatronic.com',
                'fecNac': '02/02/2021',
                'usuario': 'jvillegas3',
                'estado': 'INHABILITADO',
                'rol': 'AGRUPADOR_PROCESADOR',
                'usuarioLogueado': null,
                'audFecModif': '12/03/2021',
                'audFecCreac': '04/02/2021',
                'audUsuModif': 'ADMIN',
                'audUsuCreac': 'jvillegas'
            },
            {
                'apPaterno': 'Benito5',
                'apMaterno': 'Torres5',
                'nombre': 'Fazio5',
                'tipoDoc': 'DNI',
                'numDoc': '85474584',
                'telef': '',
                'correo': 'fbenito@novatronic.com',
                'fecNac': '01/02/2021',
                'usuario': 'fbenito5',
                'estado': 'HABILITADO',
                'rol': 'sixbioweb_ver',
                'usuarioLogueado': null,
                'audFecModif': '16/03/2021',
                'audFecCreac': '22/02/2021',
                'audUsuModif': 'jburgos6',
                'audUsuCreac': 'abenito5'
            },
            {
                'apPaterno': 'khjkhjkhjkhj',
                'apMaterno': 'khjkhjk',
                'nombre': 'hkjhkhj',
                'tipoDoc': 'DNI',
                'numDoc': '99999999',
                'telef': '546546546',
                'correo': 'jburgos@novatronic.com',
                'fecNac': '18/03/2021',
                'usuario': 'jburgos5',
                'estado': 'RESETEADO',
                'rol': 'sixbioweb_ver',
                'usuarioLogueado': null,
                'audFecModif': '24/03/2021',
                'audFecCreac': '05/03/2021',
                'audUsuModif': 'jburgos5',
                'audUsuCreac': 'jburgos6'
            },
            {
                'apPaterno': 'Martinez',
                'apMaterno': 'Linares',
                'nombre': 'Marco',
                'tipoDoc': 'DNI',
                'numDoc': '42424242',
                'telef': '999999999',
                'correo': 'mmartinez_prov@novatronic.com',
                'fecNac': '13/06/2019',
                'usuario': 'mmartinez',
                'estado': 'INHABILITADO',
                'rol': 'rol_adm_sms',
                'usuarioLogueado': null,
                'audFecModif': '14/01/2020',
                'audFecCreac': '05/06/2019',
                'audUsuModif': 'ADMIN',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'Nuflo',
                'apMaterno': 'Gamarra',
                'nombre': 'isaac',
                'tipoDoc': 'DNI',
                'numDoc': '75328398',
                'telef': '',
                'correo': 'inuflo@novatronic.com',
                'fecNac': '21/06/1997',
                'usuario': 'inuflo1',
                'estado': 'INHABILITADO',
                'rol': 'rol_adm_sms',
                'usuarioLogueado': null,
                'audFecModif': '14/01/2020',
                'audFecCreac': '15/11/2019',
                'audUsuModif': 'ADMIN',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'Chunga',
                'apMaterno': 'Chapilliquen',
                'nombre': 'Jose',
                'tipoDoc': 'DNI',
                'numDoc': '71487247',
                'telef': '999999999',
                'correo': 'djimenez@novatronic.com',
                'fecNac': '01/11/2019',
                'usuario': 'jchunga',
                'estado': 'INHABILITADO',
                'rol': 'stiAdmin',
                'usuarioLogueado': null,
                'audFecModif': '16/12/2020',
                'audFecCreac': '26/11/2019',
                'audUsuModif': 'ADMIN',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'Nuflo',
                'apMaterno': 'Gamarra',
                'nombre': 'Isaac',
                'tipoDoc': 'DNI',
                'numDoc': '75328398',
                'telef': '',
                'correo': 'inuflo@novatronic.com',
                'fecNac': '21/06/1997',
                'usuario': 'FDProcesador',
                'estado': 'INHABILITADO',
                'rol': 'adminFDig',
                'usuarioLogueado': null,
                'audFecModif': '14/01/2020',
                'audFecCreac': '27/03/2019',
                'audUsuModif': 'ADMIN',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'Alcala',
                'apMaterno': 'Marcos',
                'nombre': 'Giancarlo',
                'tipoDoc': 'DNI',
                'numDoc': '42728713',
                'telef': '',
                'correo': 'evaldivia@novatronic.com',
                'fecNac': '03/10/1984',
                'usuario': 'galcala',
                'estado': 'HABILITADO',
                'rol': 'ROL_ADM',
                'usuarioLogueado': null,
                'audFecModif': '30/12/2020',
                'audFecCreac': '15/07/2019',
                'audUsuModif': 'galcala',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'Alcala',
                'apMaterno': 'Marcos',
                'nombre': 'Giancarlo',
                'tipoDoc': 'DNI',
                'numDoc': '12345678',
                'telef': '',
                'correo': 'galcala@novatronic.com',
                'fecNac': '01/01/2000',
                'usuario': 'adminBio',
                'estado': 'INHABILITADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '21/01/2020',
                'audFecCreac': '15/07/2019',
                'audUsuModif': 'evaldivia',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'Salazar',
                'apMaterno': 'Llactahuaman',
                'nombre': 'Gerry',
                'tipoDoc': 'DNI',
                'numDoc': '73383637',
                'telef': '',
                'correo': 'gsalazar@novatronic.com',
                'fecNac': '01/01/2000',
                'usuario': 'gsalazar',
                'estado': 'INHABILITADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '06/04/2020',
                'audFecCreac': '15/07/2019',
                'audUsuModif': 'ADMIN',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'DIONISIO',
                'apMaterno': 'TRIVENIO',
                'nombre': 'CHRISTIAN',
                'tipoDoc': 'DNI',
                'numDoc': '98546521',
                'telef': '950565189',
                'correo': 'cdionisio@novatronic.com',
                'fecNac': '29/11/1994',
                'usuario': '74854354',
                'estado': 'IMPORTADO',
                'rol': 'stiAdmin',
                'usuarioLogueado': null,
                'audFecModif': '13/12/2019',
                'audFecCreac': '13/12/2019',
                'audUsuModif': 'admin',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'Benito',
                'apMaterno': 'Torres',
                'nombre': 'Fazio',
                'tipoDoc': 'DNI',
                'numDoc': '70652784',
                'telef': '333333333',
                'correo': 'fbenito@novatronic.com',
                'fecNac': '30/01/1995',
                'usuario': 'abenito',
                'estado': 'HABILITADO',
                'rol': 'sixbioweb_adm',
                'usuarioLogueado': null,
                'audFecModif': '17/03/2021',
                'audFecCreac': '28/01/2020',
                'audUsuModif': 'abenito',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'Mendoza',
                'apMaterno': 'Rios',
                'nombre': 'Miguel',
                'tipoDoc': 'DNI',
                'numDoc': '88888888',
                'telef': '',
                'correo': 'jdelosrios@novatronic.com',
                'fecNac': '04/01/2020',
                'usuario': 'mrioss',
                'estado': 'CREADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '30/01/2020',
                'audUsuModif': '',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'Jimenez',
                'apMaterno': 'Huaman',
                'nombre': 'Dennys',
                'tipoDoc': 'DNI',
                'numDoc': '12354678',
                'telef': '12345678',
                'correo': 'djimenez@novatronic.com',
                'fecNac': '10/02/2020',
                'usuario': 'djimenezSTI',
                'estado': 'RESETEADO',
                'rol': 'stiAdmin',
                'usuarioLogueado': null,
                'audFecModif': '05/03/2020',
                'audFecCreac': '17/02/2020',
                'audUsuModif': 'admin',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'fbenito6',
                'apMaterno': 'fbenito6',
                'nombre': 'fbenito6',
                'tipoDoc': 'DNI',
                'numDoc': '85748574',
                'telef': '',
                'correo': 'fbenito@novatronic.com',
                'fecNac': '01/02/2021',
                'usuario': 'fbenito6',
                'estado': 'HABILITADO',
                'rol': 'sixbioweb_ver',
                'usuarioLogueado': null,
                'audFecModif': '15/03/2021',
                'audFecCreac': '23/02/2021',
                'audUsuModif': 'jburgos6',
                'audUsuCreac': 'fbenito5'
            },
            {
                'apPaterno': 'Rivas',
                'apMaterno': 'Estrada',
                'nombre': 'Manuel',
                'tipoDoc': 'DNI',
                'numDoc': '41836380',
                'telef': '992783250',
                'correo': 'empresamanuel@gmail.com',
                'fecNac': '07/02/1983',
                'usuario': 'mrivas',
                'estado': 'INHABILITADO',
                'rol': 'rolagrupadorconsultor',
                'usuarioLogueado': null,
                'audFecModif': '04/04/2020',
                'audFecCreac': '28/02/2020',
                'audUsuModif': 'ADMIN',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'Valdivia',
                'apMaterno': 'Noa',
                'nombre': 'Eric',
                'tipoDoc': 'DNI',
                'numDoc': '72890317',
                'telef': '999999999',
                'correo': 'evaldivia@novatronic.com',
                'fecNac': '25/09/1992',
                'usuario': 'evaldivia2',
                'estado': 'RESETEADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '25/05/2020',
                'audFecCreac': '14/05/2020',
                'audUsuModif': 'evaldivia2',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'abenito9',
                'apMaterno': 'abenito9',
                'nombre': 'abenito9',
                'tipoDoc': 'DNI',
                'numDoc': '77777777',
                'telef': '',
                'correo': 'fbenito@novatronic.com',
                'fecNac': '02/02/2021',
                'usuario': 'abenito9',
                'estado': 'HABILITADO',
                'rol': 'ROL_ADM',
                'usuarioLogueado': null,
                'audFecModif': '25/02/2021',
                'audFecCreac': '25/02/2021',
                'audUsuModif': 'abenito9',
                'audUsuCreac': 'abenito5'
            },
            {
                'apPaterno': 'Alcala',
                'apMaterno': 'Marcos',
                'nombre': 'Giancarlo',
                'tipoDoc': 'DNI',
                'numDoc': '42728713',
                'telef': '',
                'correo': 'galcala@novatronic.com',
                'fecNac': '14/06/1996',
                'usuario': 'usuarioPrueba',
                'estado': 'INHABILITADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '31/07/2020',
                'audFecCreac': '30/06/2020',
                'audUsuModif': 'ADMIN',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'Benito',
                'apMaterno': 'Torres',
                'nombre': 'Fazio',
                'tipoDoc': 'DNI',
                'numDoc': '70652784',
                'telef': '',
                'correo': 'fbenito@novatronic.com',
                'fecNac': '14/03/1996',
                'usuario': 'abenitoprueba4',
                'estado': 'RESETEADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '10/07/2020',
                'audFecCreac': '10/07/2020',
                'audUsuModif': 'abenitoprueba4',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'abenito',
                'apMaterno': 'abenito',
                'nombre': 'abenito',
                'tipoDoc': 'DNI',
                'numDoc': '78452524',
                'telef': '',
                'correo': 'abenito@novatronic.com',
                'fecNac': '08/07/2020',
                'usuario': 'Ar',
                'estado': 'RESETEADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '17/07/2020',
                'audFecCreac': '16/07/2020',
                'audUsuModif': 'admin',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'abenito',
                'apMaterno': 'abenito',
                'nombre': 'abenito',
                'tipoDoc': 'DNI',
                'numDoc': '87548748',
                'telef': '',
                'correo': 'abenito@novatronic.com',
                'fecNac': '07/07/2020',
                'usuario': 'Aman',
                'estado': 'CREADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '16/07/2020',
                'audUsuModif': '',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'abenito',
                'apMaterno': 'abenito',
                'nombre': 'abenito',
                'tipoDoc': 'DNI',
                'numDoc': '74785748',
                'telef': '',
                'correo': 'abenito@novatronic.com',
                'fecNac': '07/07/2020',
                'usuario': 'ds',
                'estado': 'CREADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '17/07/2020',
                'audUsuModif': '',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'abenito',
                'apMaterno': 'abenito',
                'nombre': 'abenito',
                'tipoDoc': 'DNI',
                'numDoc': '78574857',
                'telef': '',
                'correo': 'abenito@novatronic.com',
                'fecNac': '07/07/2020',
                'usuario': 't12',
                'estado': 'CREADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '17/07/2020',
                'audUsuModif': '',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'abenito',
                'apMaterno': 'abenito',
                'nombre': 'abenito',
                'tipoDoc': 'DNI',
                'numDoc': '85748748',
                'telef': '',
                'correo': 'abenito@novatronic.com',
                'fecNac': '07/07/2020',
                'usuario': 'testbenito',
                'estado': 'CREADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '21/07/2020',
                'audUsuModif': '',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'abenito',
                'apMaterno': 'abenito',
                'nombre': 'abenito',
                'tipoDoc': 'DNI',
                'numDoc': '85748574',
                'telef': '474857487',
                'correo': 'abenito@novatronic.com',
                'fecNac': '15/07/2020',
                'usuario': '(@g85',
                'estado': 'CREADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '23/07/2020',
                'audUsuModif': '',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'abenito3',
                'apMaterno': 'abenito3',
                'nombre': 'abenito3',
                'tipoDoc': 'DNI',
                'numDoc': '74857487',
                'telef': '',
                'correo': 'fbenito@novatronic.com',
                'fecNac': '02/11/2020',
                'usuario': 'abenito3',
                'estado': 'CREADO',
                'rol': 'RolAdminBioWeb',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '10/11/2020',
                'audUsuModif': '',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'wfwfwf',
                'apMaterno': 'wfwfwfwf',
                'nombre': 'wfwfwfw',
                'tipoDoc': 'DNI',
                'numDoc': '48287359',
                'telef': '',
                'correo': 'jdelosrios@novatronic.com',
                'fecNac': '01/08/2020',
                'usuario': 'wfwfwfwf',
                'estado': 'CREADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '03/09/2020',
                'audFecCreac': '28/08/2020',
                'audUsuModif': 'jdelosrios2',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'abenito1',
                'apMaterno': 'abenito1',
                'nombre': 'abenito1',
                'tipoDoc': 'DNI',
                'numDoc': '85748574',
                'telef': '',
                'correo': 'fbenito@novatronic.com',
                'fecNac': '03/11/2020',
                'usuario': 'abenito1',
                'estado': 'RESETEADO',
                'rol': 'ROL_ADM',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '10/11/2020',
                'audUsuModif': 'admin',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'joelPrueba3266',
                'apMaterno': 'joelPrueba32',
                'nombre': 'joelPrueba32',
                'tipoDoc': 'DNI',
                'numDoc': '48287359',
                'telef': '920805153',
                'correo': 'jdelosrios@novatronic.com',
                'fecNac': '12/09/2020',
                'usuario': 'joelPrueba32',
                'estado': 'RESETEADO',
                'rol': 'ROL_ADM',
                'usuarioLogueado': null,
                'audFecModif': '03/09/2020',
                'audFecCreac': '31/08/2020',
                'audUsuModif': 'jdelosrios2',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'jvillegas2',
                'apMaterno': 'jvillegas2',
                'nombre': 'jvillegas2',
                'tipoDoc': 'DNI',
                'numDoc': '44326766',
                'telef': '',
                'correo': 'jvillegas@novatronic.com',
                'fecNac': '05/11/1999',
                'usuario': 'jvillegas2',
                'estado': 'HABILITADO',
                'rol': 'ROL_ADM',
                'usuarioLogueado': null,
                'audFecModif': '16/03/2021',
                'audFecCreac': '26/11/2020',
                'audUsuModif': 'admin',
                'audUsuCreac': 'jvillegas'
            },
            {
                'apPaterno': 'Burgos',
                'apMaterno': 'Tejada',
                'nombre': 'Jaime',
                'tipoDoc': 'DNI',
                'numDoc': '12345678',
                'telef': '',
                'correo': 'jburgos@novatronic.com',
                'fecNac': '18/01/2021',
                'usuario': 'jburgos',
                'estado': 'INHABILITADO',
                'rol': 'AGRUPADOR_ADM',
                'usuarioLogueado': null,
                'audFecModif': '08/03/2021',
                'audFecCreac': '18/01/2021',
                'audUsuModif': 'ADMIN',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'joeprueba99',
                'apMaterno': 'joeprueba33',
                'nombre': 'joeprueba33',
                'tipoDoc': 'DNI',
                'numDoc': '48287359',
                'telef': '999999999',
                'correo': 'jdelosrios@novatronic.com',
                'fecNac': '02/08/2020',
                'usuario': 'joeprueba34',
                'estado': 'CREADO',
                'rol': 'ROL_ADM',
                'usuarioLogueado': null,
                'audFecModif': '03/09/2020',
                'audFecCreac': '31/08/2020',
                'audUsuModif': 'jdelosrios2',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'joelprueba34',
                'apMaterno': 'joelprueba34www',
                'nombre': 'joelprueba34',
                'tipoDoc': 'DNI',
                'numDoc': '48287359',
                'telef': '',
                'correo': 'jdelosrios@novatronic.com',
                'fecNac': '01/08/2020',
                'usuario': 'joelprueba34',
                'estado': 'CREADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '31/08/2020',
                'audFecCreac': '31/08/2020',
                'audUsuModif': 'admin',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'joelprueba35',
                'apMaterno': 'joelprueba35',
                'nombre': 'joelprueba35',
                'tipoDoc': 'DNI',
                'numDoc': '48287359',
                'telef': '',
                'correo': 'jdelosrios@novatronic.com',
                'fecNac': '01/08/2020',
                'usuario': 'joelprueba35',
                'estado': 'CREADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '31/08/2020',
                'audUsuModif': '',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'joelprueba36',
                'apMaterno': 'joelprueba36',
                'nombre': 'joelprueba36',
                'tipoDoc': 'DNI',
                'numDoc': '48287359',
                'telef': '',
                'correo': 'jdelosrios@novatronic.com',
                'fecNac': '07/08/2020',
                'usuario': 'joelprueba36',
                'estado': 'CREADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '31/08/2020',
                'audUsuModif': '',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': '345345345',
                'apMaterno': '34534534',
                'nombre': '45345',
                'tipoDoc': 'DNI',
                'numDoc': '12345678',
                'telef': '',
                'correo': 'jburgos@novatronic.com',
                'fecNac': '19/03/2021',
                'usuario': 'GrillaTest',
                'estado': 'CREADO',
                'rol': 'sixbioweb_adm',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '24/03/2021',
                'audUsuModif': '',
                'audUsuCreac': 'jburgos6'
            },
            {
                'apPaterno': 'fsdfds',
                'apMaterno': 'fsdfsd',
                'nombre': 'sdfdsfsd',
                'tipoDoc': 'DNI',
                'numDoc': '99999999',
                'telef': '',
                'correo': 'jburgos@novatronic.com',
                'fecNac': '04/03/2003',
                'usuario': 'TestFormdd',
                'estado': 'CREADO',
                'rol': 'sixbioweb_ver',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '24/03/2021',
                'audUsuModif': '',
                'audUsuCreac': 'jburgos6'
            },
            {
                'apPaterno': 'Alcala',
                'apMaterno': 'Marcos',
                'nombre': 'Giancarlo',
                'tipoDoc': 'DNI',
                'numDoc': '44444444',
                'telef': '997021999',
                'correo': 'galcala@novatronic.com',
                'fecNac': '03/05/1987',
                'usuario': 'galcala9',
                'estado': 'INHABILITADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '01/08/2020',
                'audFecCreac': '06/05/2020',
                'audUsuModif': 'ADMIN',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'Valdivia',
                'apMaterno': 'Noa',
                'nombre': 'Eric',
                'tipoDoc': 'DNI',
                'numDoc': '72890317',
                'telef': '',
                'correo': 'evaldivia@novatronic.com',
                'fecNac': '25/09/1992',
                'usuario': 'evaldivia',
                'estado': 'INHABILITADO',
                'rol': 'ROL_ADM',
                'usuarioLogueado': null,
                'audFecModif': '28/02/2021',
                'audFecCreac': '18/05/2020',
                'audUsuModif': 'ADMIN',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'Benito',
                'apMaterno': 'Torres',
                'nombre': 'Fazio',
                'tipoDoc': 'DNI',
                'numDoc': '70652784',
                'telef': '',
                'correo': 'fbenito@novatronic.com',
                'fecNac': '12/07/1995',
                'usuario': 'abenito5',
                'estado': 'HABILITADO',
                'rol': 'sixbioweb_ver',
                'usuarioLogueado': null,
                'audFecModif': '11/03/2021',
                'audFecCreac': '24/07/2020',
                'audUsuModif': 'abenito5',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'bancognb',
                'apMaterno': 'bancognb',
                'nombre': 'bancognb',
                'tipoDoc': 'DNI',
                'numDoc': '12345678',
                'telef': '',
                'correo': 'jchunga@novatronic.com',
                'fecNac': '01/02/2050',
                'usuario': 'adminsti',
                'estado': 'IMPORTADO',
                'rol': 'adminSTICliente',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '13/11/2020',
                'audUsuModif': '',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'palomino',
                'apMaterno': 'chahua',
                'nombre': 'solange',
                'tipoDoc': 'DNI',
                'numDoc': '74883732',
                'telef': '967770360',
                'correo': 'spalomino@novatronic.com',
                'fecNac': '23/07/1997',
                'usuario': 'solange',
                'estado': 'INHABILITADO',
                'rol': 'rol_adm_sms',
                'usuarioLogueado': null,
                'audFecModif': '26/01/2021',
                'audFecCreac': '19/10/2020',
                'audUsuModif': 'ADMIN',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'sdfsdf',
                'apMaterno': 'sdfsd',
                'nombre': 'asdfsdf',
                'tipoDoc': 'DNI',
                'numDoc': '99999999',
                'telef': '',
                'correo': 'jburgos@novatronic.com',
                'fecNac': '12/03/2003',
                'usuario': 'testingemail',
                'estado': 'INHABILITADO',
                'rol': 'sixbioweb_adm',
                'usuarioLogueado': null,
                'audFecModif': '15/03/2021',
                'audFecCreac': '15/03/2021',
                'audUsuModif': 'jburgos6',
                'audUsuCreac': 'jburgos6'
            },
            {
                'apPaterno': 'test',
                'apMaterno': 'test',
                'nombre': 'Test',
                'tipoDoc': 'RUC',
                'numDoc': '99999999999',
                'telef': '111111111',
                'correo': 'jburgos@novatronic.com',
                'fecNac': '15/03/2003',
                'usuario': 'jburgos3',
                'estado': 'HABILITADO',
                'rol': 'sixbioweb_ver',
                'usuarioLogueado': null,
                'audFecModif': '18/03/2021',
                'audFecCreac': '15/03/2021',
                'audUsuModif': 'jburgos3',
                'audUsuCreac': 'jburgos6'
            },
            {
                'apPaterno': 'ffffffffffffffffffffffffffffffffffffffffffffffffff',
                'apMaterno': 'ffffffffffffffffffffffffffffffffffffffffffffffffff',
                'nombre': 'ffffffffffffffffffffffffffffffffffffffffffffffffff',
                'tipoDoc': 'RUC',
                'numDoc': '99999999999',
                'telef': '111111111',
                'correo': 'jburgos@novatronic.com',
                'fecNac': '13/03/2003',
                'usuario': 'TestUnitffffffffffff',
                'estado': 'RESETEADO',
                'rol': 'sixbioweb_ver',
                'usuarioLogueado': null,
                'audFecModif': '15/03/2021',
                'audFecCreac': '15/03/2021',
                'audUsuModif': 'TestUnitffffffffffff',
                'audUsuCreac': 'jburgos6'
            },
            {
                'apPaterno': 'Benito',
                'apMaterno': 'Torres',
                'nombre': 'Fazio',
                'tipoDoc': 'DNI',
                'numDoc': '70652784',
                'telef': '',
                'correo': 'fbenito@novatronic.com',
                'fecNac': '30/01/1995',
                'usuario': 'fbenito2',
                'estado': 'INHABILITADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '25/01/2021',
                'audFecCreac': '25/05/2020',
                'audUsuModif': 'ADMIN',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'Benito',
                'apMaterno': 'Torres',
                'nombre': 'Fazio',
                'tipoDoc': 'DNI',
                'numDoc': '70652784',
                'telef': '',
                'correo': 'fbenito@novatronic.com',
                'fecNac': '09/05/1995',
                'usuario': 'fbenito3',
                'estado': 'HABILITADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '25/05/2020',
                'audFecCreac': '25/05/2020',
                'audUsuModif': 'fbenito3',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'Meza',
                'apMaterno': '0000000',
                'nombre': 'Gustavo',
                'tipoDoc': 'DNI',
                'numDoc': '12345678',
                'telef': '',
                'correo': 'gmeza@novatronic.com',
                'fecNac': '06/03/2021',
                'usuario': 'gmeza5',
                'estado': 'CREADO',
                'rol': 'sixbioweb_adm',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '19/03/2021',
                'audUsuModif': '',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'Meza',
                'apMaterno': '0000000',
                'nombre': 'Gustavo',
                'tipoDoc': 'DNI',
                'numDoc': '99999999',
                'telef': '',
                'correo': 'gmeza@novatronic.com',
                'fecNac': '03/02/2003',
                'usuario': 'gmeza3',
                'estado': 'CREADO',
                'rol': 'sixbioweb_ver',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '19/03/2021',
                'audUsuModif': '',
                'audUsuCreac': 'jburgos6'
            },
            {
                'apPaterno': 'sdfdsfds',
                'apMaterno': 'fdsfdsfds',
                'nombre': 'sdfsdfffffffffffffffffffffffffffffffffffffffffffff',
                'tipoDoc': 'RUC',
                'numDoc': '99999999999',
                'telef': '111111111',
                'correo': 'jburgos@novatronic.com',
                'fecNac': '06/03/2003',
                'usuario': 'login123',
                'estado': 'HABILITADO',
                'rol': 'sixbioweb_ver',
                'usuarioLogueado': null,
                'audFecModif': '19/03/2021',
                'audFecCreac': '19/03/2021',
                'audUsuModif': 'login123',
                'audUsuCreac': 'jburgos6'
            },
            {
                'apPaterno': 'vdfgfdgfdgfdg',
                'apMaterno': 'fdgfdgfdg',
                'nombre': 'prueba 2',
                'tipoDoc': 'RUC',
                'numDoc': '99999999999',
                'telef': '243243243',
                'correo': 'jburgos@novatronic.com',
                'fecNac': '01/03/2021',
                'usuario': 'j12345jhsdsds',
                'estado': 'CREADO',
                'rol': 'sixbioweb_ver',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '10/03/2021',
                'audUsuModif': '',
                'audUsuCreac': 'jburgos6'
            },
            {
                'apPaterno': '44444444444444444444444444444444444444444444444444',
                'apMaterno': '44444444444444444444444444444444444444444444444444',
                'nombre': 'prueba 2',
                'tipoDoc': 'DNI',
                'numDoc': '44444444',
                'telef': '444444444',
                'correo': 'jburgos@novatronic.com',
                'fecNac': '01/03/2021',
                'usuario': 'aaasdsx43testttttttt',
                'estado': 'CREADO',
                'rol': 'sixbioweb_ver',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '10/03/2021',
                'audUsuModif': '',
                'audUsuCreac': 'jburgos6'
            },
            {
                'apPaterno': 'asdasdasdasd',
                'apMaterno': 'asdasdas',
                'nombre': 'rasdasdasd',
                'tipoDoc': 'DNI',
                'numDoc': '12345678',
                'telef': '232432432',
                'correo': 'jburgos@novatronic.com',
                'fecNac': '07/03/2021',
                'usuario': 'Angular9',
                'estado': 'CREADO',
                'rol': 'sixbioweb_ver',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '10/03/2021',
                'audUsuModif': '',
                'audUsuCreac': 'jburgos6'
            },
            {
                'apPaterno': 'sdafsdfsdf',
                'apMaterno': 'sdfsdfsdfsd',
                'nombre': 'asdfsdfgsd',
                'tipoDoc': 'RUC',
                'numDoc': '99999999999',
                'telef': '234444444',
                'correo': 'jburgos@novatronic.com',
                'fecNac': '19/03/2003',
                'usuario': 'Primerlogin',
                'estado': 'CREADO',
                'rol': 'sixbioweb_adm',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '23/03/2021',
                'audUsuModif': '',
                'audUsuCreac': 'jburgos6'
            },
            {
                'apPaterno': 'Benito',
                'apMaterno': 'Torres',
                'nombre': 'Fazio',
                'tipoDoc': 'DNI',
                'numDoc': '70652784',
                'telef': '',
                'correo': 'fbenito@novatronic.com',
                'fecNac': '30/01/1995',
                'usuario': 'fbenito',
                'estado': 'INHABILITADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '28/02/2021',
                'audFecCreac': '19/05/2020',
                'audUsuModif': 'ADMIN',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'villegas',
                'apMaterno': 'diaz',
                'nombre': 'juan diego',
                'tipoDoc': 'DNI',
                'numDoc': '88888888',
                'telef': '',
                'correo': 'jvillegas@novatronic.com',
                'fecNac': '01/09/2020',
                'usuario': 'jvillegas',
                'estado': 'HABILITADO',
                'rol': 'AGRUPADOR_ADM',
                'usuarioLogueado': null,
                'audFecModif': '23/03/2021',
                'audFecCreac': '15/09/2020',
                'audUsuModif': 'jvillegas',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'stefantu',
                'apMaterno': 'stefantu',
                'nombre': 'stefantu',
                'tipoDoc': 'DNI',
                'numDoc': '71957130',
                'telef': '941886550',
                'correo': 'ssolari@novatronic.com',
                'fecNac': '18/08/1984',
                'usuario': 'stefantu',
                'estado': 'INHABILITADO',
                'rol': 'rol_adm_sms',
                'usuarioLogueado': null,
                'audFecModif': '02/12/2020',
                'audFecCreac': '16/10/2020',
                'audUsuModif': 'ADMIN',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'Burgos',
                'apMaterno': 'Tejada',
                'nombre': 'Jaime',
                'tipoDoc': 'DNI',
                'numDoc': '12345678',
                'telef': '',
                'correo': 'jburgos@novatronic.com',
                'fecNac': '25/02/2021',
                'usuario': 'jburgos6',
                'estado': 'HABILITADO',
                'rol': 'sixbioweb_adm',
                'usuarioLogueado': null,
                'audFecModif': '22/03/2021',
                'audFecCreac': '23/02/2021',
                'audUsuModif': 'jburgos6',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': '4  44',
                'apMaterno': '44  444',
                'nombre': '44  444',
                'tipoDoc': 'DNI',
                'numDoc': '12345678',
                'telef': '111111111',
                'correo': 'jburgos@novatronic.com',
                'fecNac': '05/03/2021',
                'usuario': 'jburgos1',
                'estado': 'CREADO',
                'rol': 'sixbioweb_adm',
                'usuarioLogueado': null,
                'audFecModif': '17/03/2021',
                'audFecCreac': '16/03/2021',
                'audUsuModif': 'jburgos6',
                'audUsuCreac': 'jburgos3'
            },
            {
                'apPaterno': 'Six',
                'apMaterno': 'Bio',
                'nombre': 'Administrador',
                'tipoDoc': 'DNI',
                'numDoc': '88888888',
                'telef': '',
                'correo': 'jdelosrios@novatronic.com',
                'fecNac': '23/04/2020',
                'usuario': 'administradorBio',
                'estado': 'INHABILITADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '14/06/2020',
                'audFecCreac': '23/04/2020',
                'audUsuModif': 'ADMIN',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'fbenitoprocesador',
                'apMaterno': 'fbenitoprocesador',
                'nombre': 'fbenitoprocesador',
                'tipoDoc': 'DNI',
                'numDoc': '82547414',
                'telef': '',
                'correo': 'fbenito@novatronic.com',
                'fecNac': '03/02/2021',
                'usuario': 'fbenitoproc',
                'estado': 'INHABILITADO',
                'rol': 'AGRUPADOR_OPERADOR',
                'usuarioLogueado': null,
                'audFecModif': '06/03/2021',
                'audFecCreac': '02/02/2021',
                'audUsuModif': 'ADMIN',
                'audUsuCreac': 'admin'
            },
            {
                'apPaterno': 'Benito',
                'apMaterno': 'Torres',
                'nombre': 'Fazio',
                'tipoDoc': 'DNI',
                'numDoc': '70652784',
                'telef': '',
                'correo': 'fbenito@novatronic.com',
                'fecNac': '04/09/2019',
                'usuario': 'abenitoprueba',
                'estado': 'RESETEADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '10/07/2020',
                'audFecCreac': '10/07/2020',
                'audUsuModif': 'abenitoprueba',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'wfwfwfw',
                'apMaterno': 'wfwfwfwf',
                'nombre': 'wfwfwfwf',
                'tipoDoc': 'DNI',
                'numDoc': '48287359',
                'telef': '',
                'correo': 'jdelosrios@novatronic.com',
                'fecNac': '04/09/2020',
                'usuario': 'wwfwfwf',
                'estado': 'CREADO',
                'rol': 'ROL_ADM',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '03/09/2020',
                'audUsuModif': '',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'joelprueba223',
                'apMaterno': 'joelprueba223',
                'nombre': 'joelprueba223',
                'tipoDoc': 'DNI',
                'numDoc': '48287359',
                'telef': '',
                'correo': 'jdelosrios@novatronic.com',
                'fecNac': '02/09/2020',
                'usuario': 'joelprueba223',
                'estado': 'CREADO',
                'rol': 'BIOCoreAgrupador',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '03/09/2020',
                'audUsuModif': '',
                'audUsuCreac': 'jdelosrios2'
            },
            {
                'apPaterno': 'sdfsdfsd',
                'apMaterno': 'fsdfsdfsd',
                'nombre': 'dsfsd',
                'tipoDoc': 'RUC',
                'numDoc': '99999999999',
                'telef': '',
                'correo': 'jburgos@novatronic.com',
                'fecNac': '18/02/2003',
                'usuario': 'TestFormaadsdsdfgggg',
                'estado': 'CREADO',
                'rol': 'sixbioweb_adm',
                'usuarioLogueado': null,
                'audFecModif': '',
                'audFecCreac': '17/03/2021',
                'audUsuModif': '',
                'audUsuCreac': 'jburgos6'
            }
        ]
    },
    'codigoRespuesta': '0000',
    'mensajeRespuesta': 'OPERACION REALIZADA SATISFACTORIAMENTE'
};








      // con CallFake retornamos nuestra data previamente Mockeada
      const spyServiceFake = spyOn(servicio, 'serviceListarUsuario').and.callFake(() => {
          return of (mockDataResponse);
      });
      // Espiamos la funcion discriminarUsuarioSixbio de nuestro componente
  //    const spydiscriminarUsuarioSixbio = spyOn(component, 'discriminarUsuarioSixbio').and.callThrough();

      // Llamamos nuestra funcion desde el componente
      component.listarUsuarios();
      // Verificamos que se llame nuestro servicio Fake
      expect(spyServiceFake).toHaveBeenCalled();


      // Verificamos que se llame la funcion discriminarUsuarioSixbio
     // expect(spydiscriminarUsuarioSixbio).toHaveBeenCalled();
      // Verificamos que la data recibida por listaUsuarios  sea igual al mockDataResponse.listarUsuario.listUsuarios
      console.log('Verificamos TEST listarUsuarios');
      console.log(component.listaUsuarios );
          // Verificamos que valor de listarUsuarioOk sea TRUE
      expect(component.listarUsuarioOk ).toBe(false);
      console.log('Test pruebas component listarUsuarioOk es ' + component.listarUsuarioOk);
    });









});
