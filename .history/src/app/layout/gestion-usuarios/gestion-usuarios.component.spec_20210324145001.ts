import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { of, Subject, throwError } from 'rxjs';
import { EnvService } from 'src/app/env.service';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { ExporterService } from 'src/app/services/exporter.service';
import { ServicioService } from 'src/app/servicio/servicio.service';

import { GestionUsuariosComponent } from './gestion-usuarios.component';
class FakeRouter {
  navigate(params) {

  }
}

class FakeActivatedRoute {
  // params: Observable<any> = Observable.empty();
  private subject = new Subject();

  push( valor ) {
    this.subject.next( valor );
  }
  get params() {
    return this.subject.asObservable();
  }
}


fdescribe('GestionUsuariosComponent', () => {
  let component: GestionUsuariosComponent;
  let fixture: ComponentFixture<GestionUsuariosComponent>;
  let servicio: ServicioService;
  let translateService: TranslateService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionUsuariosComponent ],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
        RouterTestingModule,
        ModalModule.forRoot(),
      ],
      providers: [
        ExporterService,
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService ,
        {provide: Router , useClass: FakeRouter},
        {provide: ToastrService, useClass: ToastrService},
        { provide: ActivatedRoute, useClass: FakeActivatedRoute },
      ],

      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionUsuariosComponent);
    servicio = TestBed.inject(ServicioService);
    translateService = TestBed.inject(TranslateService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Componente Gestion-Usuario creado correctamente', () => {
    expect(component).toBeTruthy();
  });

  /*
    filterUsuario : new FormControl('', Validators.compose([ Validators.maxLength(20) ])),
    filterApellido : new FormControl('', Validators.compose([Validators.maxLength(50)  ])),
    filterNombre : new FormControl('', Validators.compose([ Validators.maxLength(50) ])),
  */
  it('Campo "filterUsuario" de gestionarUsuarioForm longitud maxima es 20', () => {
    const filterUsuario = component.gestionarUsuarioForm.get('filterUsuario');
    filterUsuario.setValue('12345678901234567890');
    expect(filterUsuario.valid).toBeTruthy();
    filterUsuario.setValue('123456789012345678901');
    expect(filterUsuario.valid).toBeFalsy();
  });

  it('Campo "filterApellido" de gestionarUsuarioForm longitud maxima es 50', () => {
    const filterApellido = component.gestionarUsuarioForm.get('filterApellido');
    filterApellido.setValue('12345678901234567890123456789012345678901234567890');
    expect(filterApellido.valid).toBeTruthy();
    filterApellido.setValue('123456789012345678901234567890123456789012345678901');
    expect(filterApellido.valid).toBeFalsy();
  });


  it('Campo "filterNombre" de gestionarUsuarioForm longitud maxima es 50', () => {
    const filterNombre = component.gestionarUsuarioForm.get('filterNombre');
    filterNombre.setValue('12345678901234567890123456789012345678901234567890');
    expect(filterNombre.valid).toBeTruthy();
    filterNombre.setValue('123456789012345678901234567890123456789012345678901');
    expect(filterNombre.valid).toBeFalsy();
  });


  it('filterUsuario con caracteres permitidos y no permitidos en gestionarUsuarioForm', () => {
    const filterUsuario = component.gestionarUsuarioForm.get('filterUsuario');
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b'];
    const U_No_Permitidos =
    ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
    '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
    '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]', '          '];
    // Bucle para caracteres permitidos en usuario gestionarUsuarioForm
    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      filterUsuario.setValue(U_Permitidos[j]);
      expect(filterUsuario.valid).toBe(true);
      // Cuando sea verdadero !filterUsuario.valid no se cumple la condicion de verificacion
      if ( !filterUsuario.valid) {
        console.log('Fallo en el caracter Permitido gestionarUsuarioForm ' + U_Permitidos[j] );
      }
    }
    // Bucle para caracteres no permitidos en Usuario gestionarUsuarioForm
    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      filterUsuario.setValue(U_No_Permitidos[i]);
      expect(filterUsuario.valid).toBe(false);
      // Cuando sea verdadero no se cumple la condicion de verificacion
      if (filterUsuario.valid) {
        console.log('Fallo en el caracter No Permitido  gestionarUsuarioForm' + U_No_Permitidos[i] );
      }
    }
  });

  it('filterApellido con caracteres permitidos y no permitidos en gestionarUsuarioForm', () => {
    const filterApellido = component.gestionarUsuarioForm.get('filterApellido');
    const U_Permitidos = ['aaaaaaaa', 'ccccb bbbb', 'c123bbb4b', 'rrr33 444 44'];
    const U_No_Permitidos =
    ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
    '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
    '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]', 'ñññññññ'];

    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      filterApellido.setValue(U_Permitidos[j]);
      expect(filterApellido.valid).toBe(true);
      if ( !filterApellido.valid) {
        console.log('Fallo en el caracter Permitido gestionarUsuarioForm ' + U_Permitidos[j] );
      }
    }

    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      filterApellido.setValue(U_No_Permitidos[i]);
      expect(filterApellido.valid).toBe(false);
      if (filterApellido.valid) {
        console.log('Fallo en el caracter No Permitido  gestionarUsuarioForm' + U_No_Permitidos[i] );
      }
    }
  });

  it('filterNombre con caracteres permitidos y no permitidos en gestionarUsuarioForm', () => {
    const filterNombre = component.gestionarUsuarioForm.get('filterNombre');
    const U_Permitidos = ['aaaaaaaa', 'ccccb bbbb', 'c123bbb4b', 'rrr33 444 44'];
    const U_No_Permitidos =
    ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
    '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
    '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]', 'ñññññññ'];

    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      filterNombre.setValue(U_Permitidos[j]);
      expect(filterNombre.valid).toBe(true);
      if ( !filterNombre.valid) {
        console.log('Fallo en el caracter Permitido gestionarUsuarioForm ' + U_Permitidos[j] );
      }
    }

    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      filterNombre.setValue(U_No_Permitidos[i]);
      expect(filterNombre.valid).toBe(false);
      if (filterNombre.valid) {
        console.log('Fallo en el caracter No Permitido  gestionarUsuarioForm' + U_No_Permitidos[i] );
      }
    }
  });

  it('Verificamos el funcionamiento esperado de la funcion salir()', () => {
    const vacio: Object = {  };
    component.dataRequest = vacio;
    // Creamos un espia para el servicio
    const spyService = spyOn(servicio, 'serviceSignOff').and.callThrough();
    // Llamamos nuestra funcion salir() desde el componente
    component.salir();
    // Verificamos que nuestro servicio fuera llamado
    expect(spyService).toHaveBeenCalled();

    // Mockeamos dataResponse para probar la validaciones , el primer caso
    const mockDataResponse = {
      codigoRespuesta : Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    };
    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate');

    // Fakeamos una llamada al servicio para que nos de el mock de lo que deseamos
    const spyServiceFake = spyOn(servicio, 'serviceSignOff').and.callFake(() => {
      return of (mockDataResponse);
    });

    component.salir();

    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();
    // Verificamos que nuestro servicio Fake fuera llamado
    expect(spyServiceFake).toHaveBeenCalled();


    // Comprobamos la siguiente condicion cuando es diferente de COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    mockDataResponse.codigoRespuesta = 'Diferente de correcto';

    // Volvemos a llamar a la funcion
    component.salir();

    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();


    // Ahora verificamos en caso de un error al llamar al servicio
    const spyError = spyOn(servicio, 'serviceSignOff').and.returnValue(throwError('Esto es un error'));
    // Volvemos a llamar a la funcion
    component.salir();
    // Verificamos que fuera llamado con error nuestro servicio
    expect(spyError).toHaveBeenCalled();
    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();

  });

  it('Verificamos funcion decline()', () => {
    // Espiamos al modal para verificar la funcion hide
    const spyModal = spyOn(component.modal, 'hide');
    // Llamamos a la funcion del component
    component.decline();
    // Verificamos que fuera llamado la funcion hide del modal y que aceptar sea FALSE
    expect(component.aceptar).toBeFalsy();
    expect(spyModal).toHaveBeenCalled();
  });


  it('Verificamos funcion confirm()', () => {
    // Espiamos al modal para verificar la funcion hide
    const spyModal = spyOn(component.modal, 'hide');
    // Llamamos a la funcion del component
    component.confirm();
    // Verificamos que fuera llamado la funcion hide del modal y que aceptar sea TRUE
    expect(component.aceptar).toBeTruthy();
    expect(spyModal).toHaveBeenCalled();
  });

  it('Verieficamos funcion showModal()', () => {
    // Espiamos al modal para verificar la funcion show
    const spyModal = spyOn(component.modal, 'show');
    component.showModal();
    // Verificamos que fuera llamado la funcion show del modal
    expect(spyModal).toHaveBeenCalled();
  });


  it('Verificamos funcion crearUsuario()', () => {
    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate');
    // Llamamos la funcion desde el component
    component.crearUsuario();
    expect(spyRouter).toHaveBeenCalled();
  });

  it('Verificamos funcion editarUsuario()', () => {
    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate');
    // Mockeamos el objeto usuario que recibe la funcion editarUsuario
    const mockUsuario = {
      apMaterno: 'Tejada',
      apPaterno: 'Burgos',
      audFecCreac: '23/02/2021',
      audFecModif: '22/03/2021',
      audUsuCreac: 'admin',
      audUsuModif: 'jburgos6',
      correo: 'jburgos@novatronic.com',
      estado: 'HABILITADO',
      fecNac: '25/02/2021',
      nombre: 'Jaime',
      numDoc: '12345678',
      rol: 'sixbioweb_adm',
      telef: '',
      tipoDoc: 'DNI',
      usuario: 'jburgos6',
      usuarioLogueado: null,
    };
    // Llamamos nuestra función
    component.editarUsuario(mockUsuario);

    // Verificamos que llame a Router y su funcion navigate
    expect(spyRouter).toHaveBeenCalled();
    // Verificamos que el state pasado a editar-usuario sea el mismo
    expect(mockUsuario).toEqual(history.state);

  });



});
