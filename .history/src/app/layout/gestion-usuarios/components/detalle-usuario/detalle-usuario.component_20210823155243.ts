import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ActivatedRouteSnapshot } from '@angular/router';
import { IUsuarioSCA } from 'src/app/nucleo/interface/IUsuarioSCA';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { SpinnerService } from 'src/app/services/spinner.service';
import { ServicioService } from 'src/app/servicio/servicio.service';
@Component({
    selector: 'app-detalle-usuario',
    templateUrl: './detalle-usuario.component.html',
    styleUrls: ['./detalle-usuario.component.scss']
})
export class DetalleUsuarioComponent implements OnInit {



    RolesAgrupadorValor = [];
    RolesAgrupadorClave = [];
    indiceRolAgrupador = 0;
    footer = Constant.FOOTER_NOVATRONIC;
    usuario: IUsuarioSCA;

    constructor(private router: Router, private rutaActual: ActivatedRoute, private servicioService: ServicioService,
        private spinnerService: SpinnerService) {
            if (localStorage.getItem('RolUsuario') === 'EMP_VER') {
                this.singoff();
            }
            if (history.state.usuario === undefined && localStorage.getItem('RolUsuario') === 'EMP_ADM') {
                this.router.navigate(['home']);
            }
    }
    ngOnInit() {
        if (localStorage.getItem('RolUsuario') === 'EMP_VER') {
            this.singoff();
        }
        if (history.state.usuario === undefined && localStorage.getItem('RolUsuario') === 'EMP_ADM') {
            this.singoff();
        }
        this.usuario = history.state;
        this.RolesAgrupadorValor = Constant.ROLES_AGRUPADORES_VALOR;
        this.RolesAgrupadorClave = Constant.ROLES_AGRUPADORES_CLAVE;
        this.indiceRolAgrupador = this.RolesAgrupadorValor.indexOf(this.usuario.rol);
    }

    cancelar() {
        this.router.navigate(['gestion-usuarios/']);
    }

    singoff() {
        const dataRequestSingOff = {};
         this.servicioService.serviceSignOff(dataRequestSingOff).subscribe((dataResponse: any) => {

             if (dataResponse.codigoRespuesta === Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO) {
                 localStorage.clear();
                 this.router.navigate(['login']);
             } else {
                 localStorage.clear();
                 this.router.navigate(['login']);

             }
         }
             , error => {
                 localStorage.clear();
                 this.router.navigate(['login']);
             });
     }




}
