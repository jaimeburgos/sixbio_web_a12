import { async, ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { HttpClientTestingModule , HttpTestingController} from '@angular/common/http/testing';
import { DetalleUsuarioComponent } from './detalle-usuario.component';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { EnvService } from 'src/app/env.service';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';

fdescribe('DetalleUsuarioComponent', () => {
  let component: DetalleUsuarioComponent;
  let fixture: ComponentFixture<DetalleUsuarioComponent>;
  let translateService: TranslateService;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ DetalleUsuarioComponent, ToastComponent ],
      imports: [
        CommonModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ], providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService ,
        {provide: ToastrService, useClass: ToastrService}],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    component.usuario = {
      apPaterno: 'Benito5',
      apMaterno: 'Torres5',
      nombre: 'Fazio5',
      tipoDoc: 'DNI',
      numDoc: '85474584',
      telef: '',
      correo: 'fbenito@novatronic.com',
      fecNac: '01/02/2021',
      usuario: 'fbenito5',
      estado: 'HABILITADO',
      rol: 'sixbioweb_ver',
      audFecModif: '05/03/2021',
      audFecCreac: '22/02/2021',
      audUsuModif: 'jburgos6',
      audUsuCreac: 'abenito5',
      navigationId: 3
  };
    translateService = TestBed.inject(TranslateService);
    fixture = TestBed.createComponent(DetalleUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.usuario.rol = 'sixbioweb_adm';
  });

  it('Component detalle-usuario creado correctamente', () => {
    expect(component).toBeTruthy();
  });

});
