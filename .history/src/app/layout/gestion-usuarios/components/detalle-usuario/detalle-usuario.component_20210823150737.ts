import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ActivatedRouteSnapshot } from '@angular/router';
import { IUsuarioSCA } from 'src/app/nucleo/interface/IUsuarioSCA';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { SpinnerService } from 'src/app/services/spinner.service';
@Component({
    selector: 'app-detalle-usuario',
    templateUrl: './detalle-usuario.component.html',
    styleUrls: ['./detalle-usuario.component.scss']
})
export class DetalleUsuarioComponent implements OnInit {



    RolesAgrupadorValor = [];
    RolesAgrupadorClave = [];
    indiceRolAgrupador = 0;
    footer = Constant.FOOTER_NOVATRONIC;
    usuario: IUsuarioSCA;

    constructor(private router: Router, private rutaActual: ActivatedRoute,
        private spinnerService: SpinnerService) {
            if (localStorage.getItem('RolUsuario') === 'EMP_VER') {
                this.router.navigate(['login']);
            }
            if (history.state.usuario === undefined && localStorage.getItem('RolUsuario') === 'EMP_ADM') {
                this.router.navigate(['home']);
            }
    }
    ngOnInit() {
        if (localStorage.getItem('RolUsuario') === 'EMP_VER') {
            this.router.navigate(['login']);
        }
        if (history.state.usuario === undefined && localStorage.getItem('RolUsuario') === 'EMP_ADM') {
            this.router.navigate(['home']);
        }
        this.usuario = history.state;
        this.RolesAgrupadorValor = Constant.ROLES_AGRUPADORES_VALOR;
        this.RolesAgrupadorClave = Constant.ROLES_AGRUPADORES_CLAVE;
        this.indiceRolAgrupador = this.RolesAgrupadorValor.indexOf(this.usuario.rol);
    }

    cancelar() {
        this.router.navigate(['gestion-usuarios/']);
    }

}
