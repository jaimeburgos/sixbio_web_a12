import { async, ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CrearUsuarioComponent } from './crear-usuario.component';
import { CommonModule, DatePipe } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing'; // Recordar usar RouterTesting NO usar Router
import { NgxPaginationModule } from 'ngx-pagination';
import { HttpClientTestingModule} from '@angular/common/http/testing';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { EnvService } from 'src/app/env.service';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { TranslateModule, TranslateService } from '@ngx-translate/core'; // Faltaba importar Translate Service
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { Constant } from 'src/app/nucleo/constante/Constant';

fdescribe('CrearUsuarioComponent', () => {
  let component: CrearUsuarioComponent;
  let fixture: ComponentFixture<CrearUsuarioComponent>;
  let translateService: TranslateService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearUsuarioComponent , ToastComponent],
      imports: [
        CommonModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService ,
        {provide: ToastrService, useClass: ToastrService}],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    translateService = TestBed.inject(TranslateService);
    fixture = TestBed.createComponent(CrearUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Componente CrearUsuario creado correctamente', () => {
    expect(component).toBeTruthy();
  });

  it('usuario es obligatorio en crearUserForm', () => {
    const usuario = component.crearUserForm.get('usuario');
    usuario.setValue('');
    expect(usuario.valid).toBeFalsy();
    usuario.setValue(null);
    expect(usuario.valid).toBeFalsy();
  });

  it('apPaterno es obligatorio en crearUserForm', () => {
    const apPaterno = component.crearUserForm.get('apPaterno');
    apPaterno.setValue('');
    expect(apPaterno.valid).toBeFalsy();
    apPaterno.setValue(null);
    expect(apPaterno.valid).toBeFalsy();
  });

  it('nombre es obligatorio en crearUserForm', () => {
    const nombre = component.crearUserForm.get('nombre');
    nombre.setValue('');
    expect(nombre.valid).toBeFalsy();
    nombre.setValue(null);
    expect(nombre.valid).toBeFalsy();
  });

  it('apMaterno es obligatorio en crearUserForm', () => {
    const apMaterno = component.crearUserForm.get('apMaterno');
    apMaterno.setValue('');
    expect(apMaterno.valid).toBeFalsy();
    apMaterno.setValue(null);
    expect(apMaterno.valid).toBeFalsy();
  });

  it('tipoDoc es obligatorio en crearUserForm', () => {
    const tipoDoc = component.crearUserForm.get('tipoDoc');
    tipoDoc.setValue('');
    expect(tipoDoc.valid).toBeFalsy();
    tipoDoc.setValue(null);
    expect(tipoDoc.valid).toBeFalsy();
  });

  it('numDoc es obligatorio en crearUserForm', () => {
    const numDoc = component.crearUserForm.get('tipoDoc');
    numDoc.setValue('');
    expect(numDoc.valid).toBeFalsy();
    numDoc.setValue(null);
    expect(numDoc.valid).toBeFalsy();
  });

  it('correo es obligatorio en crearUserForm', () => {
    const correo = component.crearUserForm.get('correo');
    correo.setValue('');
    expect(correo.valid).toBeFalsy();
    correo.setValue(null);
    expect(correo.valid).toBeFalsy();
  });

  it('rol es obligatorio en crearUserForm', () => {
    const rol = component.crearUserForm.get('rol');
    rol.setValue('');
    expect(rol.valid).toBeFalsy();
    rol.setValue(null);
    expect(rol.valid).toBeFalsy();
  });

  it('Campo "usuario" puede tener maximo 20 caracteres crearUserForm', () => {
    const usuario = component.crearUserForm.get('usuario');
    usuario.setValue('12345678901234567890');
    expect(usuario.valid).toBeTruthy();
    usuario.setValue('123456789012345678901');
    expect(usuario.valid).toBeFalsy();
  });

  it('Campo "usuario" debe tener minimo 5 caracteres crearUserForm', () => {
    const usuario = component.crearUserForm.get('usuario');
    usuario.setValue('12345');
    expect(usuario.valid).toBeTruthy();
    usuario.setValue('1234');
    expect(usuario.valid).toBeFalsy();
  });

  it('Campo "nombre" puede tener maximo 50 caracteres crearUserForm', () => {
    const nombre = component.crearUserForm.get('nombre');
    nombre.setValue('12345678901234567890123456789012345678901234567890');
    expect(nombre.valid).toBeTruthy();
    nombre.setValue('123456789012345678901234567890123456789012345678901');
    expect(nombre.valid).toBeFalsy();
  });

  it('Campo "nombre" puede tener minimo 1 caracter crearUserForm', () => {
    const nombre = component.crearUserForm.get('nombre');
    nombre.setValue('1');
    expect(nombre.valid).toBeTruthy();
    nombre.setValue('');
    expect(nombre.valid).toBeFalsy();
  });

  it('Campo "apPaterno" puede tener maximo 50 caracteres crearUserForm', () => {
    const apPaterno = component.crearUserForm.get('apPaterno');
    apPaterno.setValue('12345678901234567890123456789012345678901234567890');
    expect(apPaterno.valid).toBeTruthy();
    apPaterno.setValue('123456789012345678901234567890123456789012345678901');
    expect(apPaterno.valid).toBeFalsy();
  });

  it('Campo "apPaterno" puede tener minimo 1 caracter crearUserForm', () => {
    const apPaterno = component.crearUserForm.get('apPaterno');
    apPaterno.setValue('1');
    expect(apPaterno.valid).toBeTruthy();
    apPaterno.setValue('');
    expect(apPaterno.valid).toBeFalsy();
  });

  it('Campo "apMaterno" puede tener maximo 50 caracteres crearUserForm', () => {
    const apMaterno = component.crearUserForm.get('apMaterno');
    apMaterno.setValue('12345678901234567890123456789012345678901234567890');
    expect(apMaterno.valid).toBeTruthy();
    apMaterno.setValue('123456789012345678901234567890123456789012345678901');
    expect(apMaterno.valid).toBeFalsy();
  });

  it('Campo "apMaterno" puede tener minimo 1 caracter crearUserForm', () => {
    const apMaterno = component.crearUserForm.get('apMaterno');
    apMaterno.setValue('1');
    expect(apMaterno.valid).toBeTruthy();
    apMaterno.setValue('');
    expect(apMaterno.valid).toBeFalsy();
  });

  it('Verificar Longitud Maxima y Minima segun tipo Documento al Numero Documento', () => {
    const tipoDoc = component.crearUserForm.get('tipoDoc');
    const numDoc = component.crearUserForm.get('numDoc');
    tipoDoc.setValue(Constant.TIPO_DOC_DNI);
    component.onChangeTipoDoc();
    expect(component.valueMaxLenght).toBe(8);
    expect(component.valueMinLenght).toBe(8);
    numDoc.setValue('12345678');
    expect(numDoc.valid).toBeTruthy();
    numDoc.setValue('123456789');
    expect(numDoc.valid).toBeFalsy();
    numDoc.setValue('1234567');
    expect(numDoc.valid).toBeFalsy();
    tipoDoc.setValue(Constant.TIPO_DOC_PASAPORTE);
    component.onChangeTipoDoc();
    expect(component.valueMaxLenght).toBe(12);
    expect(component.valueMinLenght).toBe(12);
    numDoc.setValue('123456789012');
    expect(numDoc.valid).toBeTruthy();
    numDoc.setValue('12345678901');
    expect(numDoc.valid).toBeFalsy();
    numDoc.setValue('1234567890123');
    expect(numDoc.valid).toBeFalsy();
    tipoDoc.setValue(Constant.TIPO_DOC_RUC);
    component.onChangeTipoDoc();
    expect(component.valueMaxLenght).toBe(11);
    expect(component.valueMinLenght).toBe(11);
    numDoc.setValue('12345678901');
    expect(numDoc.valid).toBeTruthy();
    numDoc.setValue('123456789012');
    expect(numDoc.valid).toBeFalsy();
    numDoc.setValue('1234567890');
    expect(numDoc.valid).toBeFalsy();
  });


    // Usuario solo permite letras y numeros en crearUserForm
  it('Usuario con caracteres especiales en FormLogin', () => {
    const user = component.crearUserForm.get('usuario');
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b'];
    const U_No_Permitidos =
      ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
      '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
      '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]', '          '];

      // Bucle para caracteres permitidos en usuario crearUserForm
    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
        user.setValue(U_Permitidos[j]);
        expect(user.valid).toBe(true);
        // Cuando sea verdadero user.valid! no se cumple la condicion de verificacion
        if ( !user.valid) {
          console.log('Fallo en el caracter Permitido ' + U_Permitidos[j] );
        }
    }
      // Bucle para caracteres no permitidos en Usuario crearUserForm
    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
        user.setValue(U_No_Permitidos[i]);
        expect(user.valid).toBe(false);
        // Cuando sea verdadero no se cumple la condicion de verificacion
        if (user.valid) {
          console.log('Fallo en el caracter No Permitido ' + U_No_Permitidos[i] );
        }
      }
  });

  it('nombre con caracteres especiales en crearUserForm', () => {
    const nombre = component.crearUserForm.get('nombre');
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b', 'c123 bbb 4b', ' sdfds sdfsd'];
    const U_No_Permitidos =
      ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
      '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
      '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]'];
    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      nombre.setValue(U_Permitidos[j]);
        expect(nombre.valid).toBe(true);
        if ( !nombre.valid) {
          console.log('Fallo en el caracter Permitido ' + U_Permitidos[j] );
        }
    }
    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      nombre.setValue(U_No_Permitidos[i]);
        expect(nombre.valid).toBe(false);
        if (nombre.valid) {
          console.log('Fallo en el caracter No Permitido ' + U_No_Permitidos[i] );
        }
      }
  });

  it('apMaterno con caracteres especiales en crearUserForm', () => {
    const apMaterno = component.crearUserForm.get('apMaterno');
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b', 'c123 bbb 4b', ' sdfds sdfsd'];
    const U_No_Permitidos =
      ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
      '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
      '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]'];
    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      apMaterno.setValue(U_Permitidos[j]);
        expect(apMaterno.valid).toBe(true);
        if ( !apMaterno.valid) {
          console.log('Fallo en el caracter Permitido ' + U_Permitidos[j] );
        }
    }
    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      apMaterno.setValue(U_No_Permitidos[i]);
        expect(apMaterno.valid).toBe(false);
        if (apMaterno.valid) {
          console.log('Fallo en el caracter No Permitido ' + U_No_Permitidos[i] );
        }
      }
  });

  it('apMaterno con caracteres especiales en crearUserForm', () => {
    const apPaterno = component.crearUserForm.get('apPaterno');
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b', 'c123 bbb 4b', ' sdfds sdfsd'];
    const U_No_Permitidos =
      ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
      '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
      '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]'];
    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      apPaterno.setValue(U_Permitidos[j]);
        expect(apPaterno.valid).toBe(true);
        if ( !apPaterno.valid) {
          console.log('Fallo en el caracter Permitido ' + U_Permitidos[j] );
        }
    }
    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      apPaterno.setValue(U_No_Permitidos[i]);
        expect(apPaterno.valid).toBe(false);
        if (apPaterno.valid) {
          console.log('Fallo en el caracter No Permitido ' + U_No_Permitidos[i] );
        }
      }
  });







  /*  it('correo es cada maxima de caracteres es 100 en crearUserForm', () => {

      const correo = component.crearUserForm.get('correo');
      correo.setValue('1234567890123456789012345678901234567890123456789012345678901234567890123456789012@test.com');
      expect(correo.valid).toBeTruthy();
      correo.setValue('testing_2-@test.com');
      expect(correo.valid).toBeTruthy();
      correo.setValue('1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901@test23.com');
      expect(correo.valid).toBeFalsy();
    });*/









});
