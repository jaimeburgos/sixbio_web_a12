import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { FormBuilder, FormGroup, MinLengthValidator } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { listLocales } from 'ngx-bootstrap/chronos';
import { DatePipe } from '@angular/common';
import { SpinnerService } from 'src/app/services/spinner.service';
@Component({
    selector: 'app-crear-usuario',
    templateUrl: './crear-usuario.component.html',
    styleUrls: ['./crear-usuario.component.scss']
})
export class CrearUsuarioComponent implements OnInit {

    @ViewChild(ToastComponent, { static: true }) toast: ToastComponent;
    locale = 'es';
    locales = listLocales();
    RolesAgrupadorValor = [];
    RolesAgrupadorClave = [];
    footer = Constant.FOOTER_NOVATRONIC;
    valueMinLenght: number;
    valueMaxLenght: number;

    maxDate: Date;
    constructor(private router: Router, private rutaActual: ActivatedRoute, private servicioService: ServicioService,
        private spinnerService: SpinnerService,
        private datePipe: DatePipe, private localeService: BsLocaleService, private fb: FormBuilder) {
        if (localStorage.getItem('RolUsuario') === 'EMP_VER') {
            this.router.navigate(['home/verificacionBiometrica']);
        }
        this.localeService.use(this.locale);
        this.maxDate = new Date();
        this.maxDate.setFullYear(this.maxDate.getFullYear() - 18);
        this.maxDate.setDate(this.maxDate.getDate());

    }

    fechaNacimiento: any;
    indiceRolAgrupador = 0;
    crearUserForm: FormGroup;

    ngOnInit() {
        if (localStorage.getItem('RolUsuario') === 'EMP_VER') {
            this.router.navigate(['home/verificacionBiometrica']);
        }
        this.RolesAgrupadorValor = Constant.ROLES_AGRUPADORES_VALOR;
        this.RolesAgrupadorClave = Constant.ROLES_AGRUPADORES_CLAVE;
        this.crearUserForm = this.fb.group({
            usuario: new FormControl('', Validators.compose([
                Validators.minLength(5),
                Validators.maxLength(20),
                Validators.pattern('[A-Za-z0-9]+')
            ])),
            apPaterno: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(1),
                Validators.maxLength(50),
                Validators.pattern('[A-Za-z0-9 ]+')
            ])),
            nombre: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(1),
                Validators.maxLength(50),
                Validators.pattern('[A-Za-z0-9 ]+')
            ])),
            apMaterno: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(1),
                Validators.maxLength(50),
                Validators.pattern('[A-Za-z0-9 ]+')
            ])),
            fecNac: new FormControl('', Validators.compose([
                Validators.required,
            ])),
            numDoc: new FormControl({ value: '', disabled: true }, Validators.compose([
                Validators.required,
                Validators.minLength(this.valueMinLenght),
                Validators.maxLength(this.valueMaxLenght),
                //   Validators.pattern('[0-9]+')
            ])),
            tipoDoc: new FormControl(0, Validators.compose([
                Validators.required
            ])),
            correo: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(1),
                Validators.maxLength(47),
                // Validators.email,
                Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$'),
                // SCA soporta 85
            ])),
            telef: new FormControl('', Validators.compose([
                Validators.minLength(9),
                Validators.maxLength(9),
                Validators.pattern('[0-9]+')
            ])),
            rol: new FormControl('', Validators.compose([
                Validators.required,
            ]))
        });
        this.limpiar();
    }

    cancelar() {
        this.router.navigate(['gestion-usuarios/']);
    }

    crearUsuario(values) {
        //  this.setDataRequestCrearUsuario(values.fecNac);
        this.setDataRequestCrearUsuario(this.crearUserForm.getRawValue().fecNac);
        // Se usa this.crearUserForm.getRawValue() en lugar de values dado la conversion de datos hacia el backend
        this.servicioService.serviceCrearUsuario(this.crearUserForm.getRawValue()).subscribe(
            (result: any) => {
                if (Constant.COD_OPERACION_SATISFACTORIA_SCA === result.codigoRespuesta) {
                    this.toast.addToast({
                        title: 'Info', msg: 'El Usuario: ' +
                            values.usuario + ' fue creado satisfactoriamente', timeOut: 5000, type: 'info'
                    });
                    this.limpiar();
                } else if (result.codigoRespuesta === Constant.COD_ERROR_SESION) {
                    this.salir();
                    return;
                    // redirect inicio
                } else if (this.validarRespuesta(result.codigoRespuesta)) {
                    this.toast.addToast({ title: 'Error', msg: result.mensajeRespuesta, timeOut: 5000, type: 'error' });
                    this.crearUserForm.controls.fecNac.setValue(values.fecNac);
                    this.crearUserForm.controls.rol.setValue(values.rol);
                } else {
                    this.toast.addToast({ title: 'Error', msg: Constant.MENSAJE_ERROR_CONECTIVIDAD, timeOut: 5000, type: 'error' });
                    setTimeout(() => { this.salir(); }, 500);
                }
            }, error => {
                this.toast.addToast({ title: 'Error', msg: Constant.MENSAJE_ERROR_CONECTIVIDAD, timeOut: 5000, type: 'error' });
                setTimeout(() => { this.salir(); }, 500);
            }
        );
    }

    limpiar() {
        this.crearUserForm.reset();
        this.crearUserForm.controls['rol'].setValue(0);
        this.crearUserForm.controls['tipoDoc'].setValue('');
        this.crearUserForm.controls['telef'].setValue('');
        this.crearUserForm.controls.numDoc.disable();
    }

    onChangeTipoDoc() {
        switch (this.crearUserForm.getRawValue().tipoDoc) {
            case Constant.TIPO_DOC_DNI: {
                this.crearUserForm.controls.numDoc.setValue('');
                this.crearUserForm.controls.numDoc.enable();
                this.valueMaxLenght = 8;
                this.valueMinLenght = 8;
                this.crearUserForm.controls.numDoc.setValidators([
                    Validators.required,
                    Validators.minLength(this.valueMinLenght),
                    Validators.maxLength(this.valueMaxLenght)
                ]);
                break;
            }
            case Constant.TIPO_DOC_PASAPORTE: {
                this.crearUserForm.controls.numDoc.setValue('');
                this.crearUserForm.controls.numDoc.enable();
                this.valueMaxLenght = 12;
                this.valueMinLenght = 12;
                this.crearUserForm.controls.numDoc.setValidators([
                    Validators.required,
                    Validators.minLength(this.valueMinLenght),
                    Validators.maxLength(this.valueMaxLenght)
                ]);
                break;
            }
            case Constant.TIPO_DOC_RUC: {
                this.crearUserForm.controls.numDoc.setValue('');
                this.crearUserForm.controls.numDoc.enable();
                this.valueMaxLenght = 11;
                this.valueMinLenght = 11;
                this.crearUserForm.controls.numDoc.setValidators([
                    Validators.required,
                    Validators.minLength(this.valueMinLenght),
                    Validators.maxLength(this.valueMaxLenght)
                ]);
                break;
            }
            default: {
                this.crearUserForm.controls.numDoc.setValue('');
                this.crearUserForm.controls.numDoc.disable();
                break;
            }
        }
    }

    onChangeVacioNombre() {
        if (this.crearUserForm.getRawValue().nombre.trim() === '') {
            this.crearUserForm.controls['nombre'].setValue('');
        }
    }

    onChangeVacioMaterno() {
        if (this.crearUserForm.getRawValue().apMaterno.trim() === '') {
            this.crearUserForm.controls['apMaterno'].setValue('');
        }
    }

    onChangeVacioPaterno() {
        if (this.crearUserForm.getRawValue().apPaterno.trim() === '') {
            this.crearUserForm.controls.apPaterno.setValue('');
        }
    }

    setDataRequestCrearUsuario(fechaNacimiento: any) {
        this.crearUserForm.controls['rol'].setValue(this.RolesAgrupadorValor[this.crearUserForm.getRawValue().rol]);
        this.crearUserForm.controls['nombre'].setValue(this.crearUserForm.getRawValue().nombre.trim());
        if (fechaNacimiento !== null) {
            this.crearUserForm.controls['fecNac'].setValue(this.datePipe.transform(fechaNacimiento, 'ddMMyyyy'));
        }
    }

    validarRespuesta(c) {
        if (c === '99999' || c === '3001' || c === '9999') {
            return true;
        } else {
            return false;
        }
    }

    salir() {
        const vacio = {};
        // this.servicioService.serviceSignOff(this.crearUserForm.getRawValue()).subscribe((dataResponse: any) => {
        this.servicioService.serviceSignOff(vacio).subscribe((dataResponse: any) => {
            if (dataResponse.codigoRespuesta === Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO) {
                localStorage.clear();
                this.router.navigate(['/login']);
            } else {
                localStorage.clear();
                this.router.navigate(['/login']);
            }
        }
            , error => {
                localStorage.clear();
                this.router.navigate(['/login']);
            });
    }

}
