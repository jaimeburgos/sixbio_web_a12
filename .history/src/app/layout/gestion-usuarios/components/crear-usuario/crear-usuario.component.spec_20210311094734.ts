import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { CrearUsuarioComponent } from './crear-usuario.component';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { CommonModule } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing'; // Recordar usar RouterTesting NO usar Router
import { NgxPaginationModule } from 'ngx-pagination';
import { HttpClientTestingModule} from '@angular/common/http/testing';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';

  import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { EnvService } from 'src/app/env.service';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { TranslateModule, TranslateService } from '@ngx-translate/core'; // Faltaba importar Translate Service
fdescribe('CrearUsuarioComponent', () => {
  let component: CrearUsuarioComponent;
  let fixture: ComponentFixture<CrearUsuarioComponent>;
  let translateService: TranslateService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearUsuarioComponent , ToastComponent],
      imports: [   CommonModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),

      ],
      // tslint:disable-next-line:max-line-length
      providers: [TranslateService, EnvService, ServicioService , {provide: ToastrService, useClass: ToastrService}],
        schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    translateService = TestBed.get(TranslateService);

    fixture = TestBed.createComponent(CrearUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Componente CrearUsuario creado correctamente', () => {
    expect(component).toBeTruthy();
  });
});
