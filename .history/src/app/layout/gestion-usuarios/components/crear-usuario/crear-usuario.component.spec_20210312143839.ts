import { async, ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CrearUsuarioComponent } from './crear-usuario.component';
import { CommonModule, DatePipe } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing'; // Recordar usar RouterTesting NO usar Router
import { NgxPaginationModule } from 'ngx-pagination';
import { HttpClientTestingModule} from '@angular/common/http/testing';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { EnvService } from 'src/app/env.service';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { TranslateModule, TranslateService } from '@ngx-translate/core'; // Faltaba importar Translate Service
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';

fdescribe('CrearUsuarioComponent', () => {
  let component: CrearUsuarioComponent;
  let fixture: ComponentFixture<CrearUsuarioComponent>;
  let translateService: TranslateService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearUsuarioComponent , ToastComponent],
      imports: [
        CommonModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService ,
        {provide: ToastrService, useClass: ToastrService}],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    translateService = TestBed.inject(TranslateService);
    fixture = TestBed.createComponent(CrearUsuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Componente CrearUsuario creado correctamente', () => {
    expect(component).toBeTruthy();
  });

  it('usuario es obligatorio en crearUserForm', () => {
    const usuario = component.crearUserForm.get('usuario');
    usuario.setValue('');
    expect(usuario.valid).toBeFalsy();
    usuario.setValue(null);
    expect(usuario.valid).toBeFalsy();
  });

  it('apPaterno es obligatorio en crearUserForm', () => {
    const apPaterno = component.crearUserForm.get('apPaterno');
    apPaterno.setValue('');
    expect(apPaterno.valid).toBeFalsy();
    apPaterno.setValue(null);
    expect(apPaterno.valid).toBeFalsy();
  });

  it('nombre es obligatorio en crearUserForm', () => {
    const nombre = component.crearUserForm.get('nombre');
    nombre.setValue('');
    expect(nombre.valid).toBeFalsy();
    nombre.setValue(null);
    expect(nombre.valid).toBeFalsy();
  });

  it('apMaterno es obligatorio en crearUserForm', () => {
    const apMaterno = component.crearUserForm.get('apMaterno');
    apMaterno.setValue('');
    expect(apMaterno.valid).toBeFalsy();
    apMaterno.setValue(null);
    expect(apMaterno.valid).toBeFalsy();
  });

  it('tipoDoc es obligatorio en crearUserForm', () => {
    const tipoDoc = component.crearUserForm.get('tipoDoc');
    tipoDoc.setValue('');
    expect(tipoDoc.valid).toBeFalsy();
    tipoDoc.setValue(null);
    expect(tipoDoc.valid).toBeFalsy();
  });

  it('numDoc es obligatorio en crearUserForm', () => {
    const numDoc = component.crearUserForm.get('tipoDoc');
    numDoc.setValue('');
    expect(numDoc.valid).toBeFalsy();
    numDoc.setValue(null);
    expect(numDoc.valid).toBeFalsy();
  });

  it('correo es obligatorio en crearUserForm', () => {
    const correo = component.crearUserForm.get('correo');
    correo.setValue('');
    expect(correo.valid).toBeFalsy();
    correo.setValue(null);
    expect(correo.valid).toBeFalsy();
  });


  it('rol es obligatorio en crearUserForm', () => {
    const rol = component.crearUserForm.get('rol');
    rol.setValue('');
    expect(rol.valid).toBeFalsy();
    rol.setValue(null);
    expect(rol.valid).toBeFalsy();
  });

  it('Campo "usuario" puede tener maximo 20 caracteres consultarValidacionesForm', () => {
    const usuario = component.crearUserForm.get('usuario');
    usuario.setValue('12345678901234567890');
    expect(usuario.valid).toBeTruthy();
    usuario.setValue('123456789012345678901');
    expect(usuario.valid).toBeFalsy();
  });

  it('Campo "usuario" debe tener minimo 5 caracteres consultarValidacionesForm', () => {
    const usuario = component.crearUserForm.get('usuario');
    usuario.setValue('12345');
    expect(usuario.valid).toBeTruthy();
    usuario.setValue('1234');
    expect(usuario.valid).toBeFalsy();
  });



  it('Campo "nombre" puede tener maximo 50 caracteres consultarValidacionesForm', () => {
    const nombre = component.crearUserForm.get('nombre');
    nombre.setValue('12345678901234567890123456789012345678901234567890');
    expect(nombre.valid).toBeTruthy();
    nombre.setValue('123456789012345678901234567890123456789012345678901');
    expect(nombre.valid).toBeFalsy();
  });

  it('Campo "apPaterno" puede tener maximo 50 caracteres consultarValidacionesForm', () => {
    const apPaterno = component.crearUserForm.get('apPaterno');
    apPaterno.setValue('12345678901234567890123456789012345678901234567890');
    expect(apPaterno.valid).toBeTruthy();
    apPaterno.setValue('123456789012345678901234567890123456789012345678901');
    expect(apPaterno.valid).toBeFalsy();
  });

  it('Campo "apMaterno" puede tener maximo 50 caracteres consultarValidacionesForm', () => {
    const apMaterno = component.crearUserForm.get('apMaterno');
    apMaterno.setValue('12345678901234567890123456789012345678901234567890');
    expect(apMaterno.valid).toBeTruthy();
    apMaterno.setValue('123456789012345678901234567890123456789012345678901');
    expect(apMaterno.valid).toBeFalsy();
  });



});
