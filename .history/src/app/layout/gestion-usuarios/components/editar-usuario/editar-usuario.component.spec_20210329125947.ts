import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { of, Subject, throwError } from 'rxjs';
import { EnvService } from 'src/app/env.service';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { EditarUsuarioComponent } from './editar-usuario.component';


class FakeRouter {
  navigate(params) {
  }
}

class FakeActivatedRoute {
  // params: Observable<any> = Observable.empty();
  private subject = new Subject();
  push( valor ) {
    this.subject.next( valor );
  }
  get params() {
    return this.subject.asObservable();
  }
}



fdescribe('EditarUsuarioComponent', () => {
  let component: EditarUsuarioComponent;
  let fixture: ComponentFixture<EditarUsuarioComponent>;
  // Instanciamos nuestros servicio que usaremos
  let translateService: TranslateService;
  let servicio: ServicioService;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarUsuarioComponent, ToastComponent ],
      imports: [
        CommonModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService ,
        {provide: Router , useClass: FakeRouter},
        {provide: ToastrService, useClass: ToastrService},
        { provide: ActivatedRoute, useClass: FakeActivatedRoute },
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
  // Mockeamos un history.state ya que se requiere para inicializar y cargar datos del componente
    const data = {
      apPaterno: 'Benito5',
      apMaterno: 'Torres5',
      nombre: 'Fazio5',
      tipoDoc: 'DNI',
      numDoc: '85474584',
      telef: '',
      correo: 'fbenito@novatronic.com',
      fecNac: '01/02/2021',
      usuario: 'fbenito5',
      estado: 'HABILITADO',
      rol: 'sixbioweb_ver',
      audFecModif: '05/03/2021',
      audFecCreac: '22/02/2021',
      audUsuModif: 'jburgos6',
      audUsuCreac: 'abenito5',
      navigationId: 3
    };
    // Seteamos la data mockeada a nuestro history.state
    window.history.pushState({data}, '', '');
    fixture = TestBed.createComponent(EditarUsuarioComponent);
    // Injectamos con TestBed los servicios a usar
    translateService = TestBed.inject(TranslateService);
    servicio = TestBed.inject(ServicioService);
    component = fixture.componentInstance;
    fixture.detectChanges();
    // Asiganmos los datos mockeados a nuestro editarUserForm
    component.editarUserForm.patchValue(history.state);
  });

  it('componente editar-usuario creado correctamente', () => {
    expect(component).toBeTruthy();
  });

  it('Componente CrearUsuario creado correctamente', () => {
    expect(component).toBeTruthy();
  });

  it('usuario es obligatorio en editarUserForm', () => {
    const usuario = component.editarUserForm.get('usuario');
    usuario.setValue('');
    expect(usuario.valid).toBeFalsy();
    usuario.setValue(null);
    expect(usuario.valid).toBeFalsy();
  });

  it('apPaterno es obligatorio en editarUserForm', () => {
    const apPaterno = component.editarUserForm.get('apPaterno');
    apPaterno.setValue('');
    expect(apPaterno.valid).toBeFalsy();
    apPaterno.setValue(null);
    expect(apPaterno.valid).toBeFalsy();
  });

  it('nombre es obligatorio en editarUserForm', () => {
    const nombre = component.editarUserForm.get('nombre');
    nombre.setValue('');
    expect(nombre.valid).toBeFalsy();
    nombre.setValue(null);
    expect(nombre.valid).toBeFalsy();
  });

  it('apMaterno es obligatorio en editarUserForm', () => {
    const apMaterno = component.editarUserForm.get('apMaterno');
    apMaterno.setValue('');
    expect(apMaterno.valid).toBeFalsy();
    apMaterno.setValue(null);
    expect(apMaterno.valid).toBeFalsy();
  });

  it('apMaterno es obligatorio en editarUserForm', () => {
    const fecNac = component.editarUserForm.get('fecNac');
    fecNac.setValue('');
    expect(fecNac.valid).toBeFalsy();
    fecNac.setValue(null);
    expect(fecNac.valid).toBeFalsy();
  });

  it('tipoDoc es obligatorio en editarUserForm', () => {
    const tipoDoc = component.editarUserForm.get('tipoDoc');
    tipoDoc.setValue('');
    expect(tipoDoc.valid).toBeFalsy();
    tipoDoc.setValue(null);
    expect(tipoDoc.valid).toBeFalsy();
  });

  it('numDoc es obligatorio en editarUserForm', () => {
    const numDoc = component.editarUserForm.get('tipoDoc');
    numDoc.setValue('');
    expect(numDoc.valid).toBeFalsy();
    numDoc.setValue(null);
    expect(numDoc.valid).toBeFalsy();
  });

  it('correo es obligatorio en editarUserForm', () => {
    const correo = component.editarUserForm.get('correo');
    correo.setValue('');
    expect(correo.valid).toBeFalsy();
    correo.setValue(null);
    expect(correo.valid).toBeFalsy();
  });

  it('rol es obligatorio en editarUserForm', () => {
    const rol = component.editarUserForm.get('rol');
    rol.setValue('');
    expect(rol.valid).toBeFalsy();
    rol.setValue(null);
    expect(rol.valid).toBeFalsy();
  });

  it('Campo "usuario" puede tener maximo 20 caracteres editarUserForm', () => {
    const usuario = component.editarUserForm.get('usuario');
    usuario.setValue('12345678901234567890');
    expect(usuario.valid).toBeTruthy();
    usuario.setValue('123456789012345678901');
    expect(usuario.valid).toBeFalsy();
  });

  it('Campo "usuario" debe tener minimo 5 caracteres editarUserForm', () => {
    const usuario = component.editarUserForm.get('usuario');
    usuario.setValue('12345');
    expect(usuario.valid).toBeTruthy();
    usuario.setValue('1234');
    expect(usuario.valid).toBeFalsy();
  });

  it('Campo "nombre" puede tener maximo 50 caracteres editarUserForm', () => {
    const nombre = component.editarUserForm.get('nombre');
    nombre.setValue('12345678901234567890123456789012345678901234567890');
    expect(nombre.valid).toBeTruthy();
    nombre.setValue('123456789012345678901234567890123456789012345678901');
    expect(nombre.valid).toBeFalsy();
  });

  it('Campo "nombre" puede tener minimo 1 caracter editarUserForm', () => {
    const nombre = component.editarUserForm.get('nombre');
    nombre.setValue('1');
    expect(nombre.valid).toBeTruthy();
    nombre.setValue('');
    expect(nombre.valid).toBeFalsy();
  });

  it('Campo "apPaterno" puede tener maximo 50 caracteres editarUserForm', () => {
    const apPaterno = component.editarUserForm.get('apPaterno');
    apPaterno.setValue('12345678901234567890123456789012345678901234567890');
    expect(apPaterno.valid).toBeTruthy();
    apPaterno.setValue('123456789012345678901234567890123456789012345678901');
    expect(apPaterno.valid).toBeFalsy();
  });

  it('Campo "apPaterno" puede tener minimo 1 caracter editarUserForm', () => {
    const apPaterno = component.editarUserForm.get('apPaterno');
    apPaterno.setValue('1');
    expect(apPaterno.valid).toBeTruthy();
    apPaterno.setValue('');
    expect(apPaterno.valid).toBeFalsy();
  });

  it('Campo "apMaterno" puede tener maximo 50 caracteres editarUserForm', () => {
    const apMaterno = component.editarUserForm.get('apMaterno');
    apMaterno.setValue('12345678901234567890123456789012345678901234567890');
    expect(apMaterno.valid).toBeTruthy();
    apMaterno.setValue('123456789012345678901234567890123456789012345678901');
    expect(apMaterno.valid).toBeFalsy();
  });

  it('Campo "apMaterno" puede tener minimo 1 caracter editarUserForm', () => {
    const apMaterno = component.editarUserForm.get('apMaterno');
    apMaterno.setValue('1');
    expect(apMaterno.valid).toBeTruthy();
    apMaterno.setValue('');
    expect(apMaterno.valid).toBeFalsy();
  });

  // Usuario solo permite letras y numeros en editarUserForm
  it('Usuario con caracteres especiales en editarUserForm', () => {
    const user = component.editarUserForm.get('usuario');
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b'];
    const U_No_Permitidos =
      ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
      '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
      '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]', '          '];

      // Bucle para caracteres permitidos en usuario editarUserForm
    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
        user.setValue(U_Permitidos[j]);
        expect(user.valid).toBe(true);
        // Cuando sea verdadero user.valid! no se cumple la condicion de verificacion
        if ( !user.valid) {
          console.log('Fallo en el caracter Permitido ' + U_Permitidos[j] );
        }
    }
      // Bucle para caracteres no permitidos en Usuario editarUserForm
    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
        user.setValue(U_No_Permitidos[i]);
        expect(user.valid).toBe(false);
        // Cuando sea verdadero no se cumple la condicion de verificacion
        if (user.valid) {
          console.log('Fallo en el caracter No Permitido ' + U_No_Permitidos[i] );
        }
      }
  });

  it('nombre con caracteres especiales en editarUserForm', () => {
    const nombre = component.editarUserForm.get('nombre');
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b', 'c123 bbb 4b', ' sdfds sdfsd'];
    const U_No_Permitidos =
      ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
      '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
      '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]'];
    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      nombre.setValue(U_Permitidos[j]);
        expect(nombre.valid).toBe(true);
        if ( !nombre.valid) {
          console.log('Fallo en el caracter Permitido ' + U_Permitidos[j] );
        }
    }
    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      nombre.setValue(U_No_Permitidos[i]);
        expect(nombre.valid).toBe(false);
        if (nombre.valid) {
          console.log('Fallo en el caracter No Permitido ' + U_No_Permitidos[i] );
        }
      }
  });

  it('apMaterno con caracteres especiales en editarUserForm', () => {
    const apMaterno = component.editarUserForm.get('apMaterno');
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b', 'c123 bbb 4b', ' sdfds sdfsd'];
    const U_No_Permitidos =
      ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
      '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
      '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]'];
    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      apMaterno.setValue(U_Permitidos[j]);
        expect(apMaterno.valid).toBe(true);
        if ( !apMaterno.valid) {
          console.log('Fallo en el caracter Permitido ' + U_Permitidos[j] );
        }
    }
    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      apMaterno.setValue(U_No_Permitidos[i]);
        expect(apMaterno.valid).toBe(false);
        if (apMaterno.valid) {
          console.log('Fallo en el caracter No Permitido ' + U_No_Permitidos[i] );
        }
      }
  });

  it('apPaterno con caracteres especiales en editarUserForm', () => {
    const apPaterno = component.editarUserForm.get('apPaterno');
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b', 'c123 bbb 4b', ' sdfds sdfsd'];
    const U_No_Permitidos =
      ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
      '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
      '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]'];
    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      apPaterno.setValue(U_Permitidos[j]);
        expect(apPaterno.valid).toBe(true);
        if ( !apPaterno.valid) {
          console.log('Fallo en el caracter Permitido ' + U_Permitidos[j] );
        }
    }
    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      apPaterno.setValue(U_No_Permitidos[i]);
        expect(apPaterno.valid).toBe(false);
        if (apPaterno.valid) {
          console.log('Fallo en el caracter No Permitido ' + U_No_Permitidos[i] );
        }
      }
  });

  /*  it('correo es cada maxima de caracteres es 100 en editarUserForm', () => {
      const correo = component.editarUserForm.get('correo');
      correo.setValue('1234567890123456789012345678901234567890123456789012345678901234567890123456789012@test.com');
      expect(correo.valid).toBeTruthy();
      correo.setValue('testing_2-@test.com');
      expect(correo.valid).toBeTruthy();
      correo.setValue('1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901@test23.com');
      expect(correo.valid).toBeFalsy();
  });*/

  it('Verificamos funcion ngOnInit', () => {
    // Espiamos nuestra funcion asignacionEditarForm();
    const spyAsignacion = spyOn(component, 'asignacionEditarForm').and.callThrough();
    // Llamamos nuestra funcion ngOnInit();
    component.ngOnInit();
    // Verificamos que los valores asignados sean los correctos
    expect(component.RolesAgrupadorValor).toEqual(Constant.ROLES_AGRUPADORES_VALOR);
    expect(component.RolesAgrupadorClave ).toEqual(Constant.ROLES_AGRUPADORES_CLAVE);
    // Verificamos que fuera llamada nuestro espia spyAsignacion
    expect(spyAsignacion).toHaveBeenCalled();
    // Verificamos indiceRolAgrupador
    expect(component.indiceRolAgrupador).toEqual(-1);
    // Verificamos que la asignacion de fecehaNacInicial sea igual editarUserForm que fue previamente seteado con el history.state
    expect(component.fechaNacInicial).toEqual(component.editarUserForm.getRawValue().fecNac);
  });

  it('Verificamos funcion onChangeVacioNombre()', () => {
    // Seteamos un valor a nuestro formControl nombre
    // El cual sera solo espacios
    component.editarUserForm.controls.nombre.setValue('          ');
    // Ahora llamamos nuestra funcion onChangeVacioNombre
    component.onChangeVacioNombre();
    // Verificamos el valor final en nuestri editarUserForm nombre
    expect(component.editarUserForm.getRawValue().nombre).toEqual('');
    // Verificamos que no se cumple la validacion del formulario
    expect(component.editarUserForm.controls.nombre.valid).toBeFalsy();
  });

  it('Verificamos funcion onChangeVacioMaterno()', () => {
    // Seteamos un valor a nuestro formControl apMaterno
    // El cual sera solo espacios
    component.editarUserForm.controls.apMaterno.setValue('          ');
    // Ahora llamamos nuestra funcion onChangeVacioMaterno
    component.onChangeVacioMaterno();
    // Verificamos el valor final en nuestri editarUserForm apMaterno
    expect(component.editarUserForm.getRawValue().apMaterno).toEqual('');
    // Verificamos que no se cumple la validacion del formulario
    expect(component.editarUserForm.controls.apMaterno.valid).toBeFalsy();
  });

  it('Verificamos funcion onChangeVacioPaterno()', () => {
    // Seteamos un valor a nuestro formControl apPaterno
    // El cual sera solo espacios
    component.editarUserForm.controls.apPaterno.setValue('          ');
    // Ahora llamamos nuestra funcion onChangeVacioPaterno
    component.onChangeVacioPaterno();
    // Verificamos el valor final en nuestri editarUserForm apPaterno
    expect(component.editarUserForm.getRawValue().apPaterno).toEqual('');
    // Verificamos que no se cumple la validacion del formulario
    expect(component.editarUserForm.controls.apPaterno.valid).toBeFalsy();
  });

  it('Verificamos la funcion cancelar()', () => {
    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate').and.callThrough();
    // Llamamos nuestra funcion
    component.cancelar();
    // Verificamos que fuera llamado Router navigate
    expect(spyRouter).toHaveBeenCalled();
  });

  it('Verificamos la funcion salir()', () => {
    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate').and.callThrough();
    // Espiamos nuestro servicio
    const spyService = spyOn(servicio, 'serviceSignOff').and.callThrough();
    // Llamamos nuestra funcion salir
    component.salir();
    // Verificamos que fuera llamada nuestro servicio
    expect(spyService).toHaveBeenCalled();

    // Ahora mockeamos un dataResponse del servicio
    // Evaluamos el primer caso donde codigoRespuesta = Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    const mockDataResponse = {
      codigoRespuesta : Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    };

    // Fakeamos un retorno de nuestro servicio
    const spyServiceFake = spyOn(servicio, 'serviceSignOff').and.callFake(() => {
      return of (mockDataResponse);
    });
    // Llamamos nuestra funcion salir
    component.salir();
    // Verificamos llame nuestro servicio
    expect(spyServiceFake).toHaveBeenCalled();
    // Verificamos que fuera llamado Router navigate
    expect(spyRouter).toHaveBeenCalled();

    // Ahora probamos el caso contrario donde no nos responda Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    mockDataResponse.codigoRespuesta = 'Diferente del Anterior';
    // Llamamos nuestra funcion salir
    component.salir();
    // Verificamos llame nuestro servicio
    expect(spyServiceFake).toHaveBeenCalled();
    // Verificamos que fuera llamado Router navigate
    expect(spyRouter).toHaveBeenCalled();

    // Ahora verificamos en caso nos retorne un error nuestro servicio
    const spyServiceError = spyOn(servicio, 'serviceSignOff').and.returnValue(throwError('Esto un error SignOff'));
    // Llamamos nuestra funcion salir
    component.salir();
    // Verificamos llame nuestro servicio Error
    expect(spyServiceError).toHaveBeenCalled();
    // Verificamos que fuera llamado Router navigate
    expect(spyRouter).toHaveBeenCalled();
  });


  it('Verificamos la funcion validarRespuesta', () => {
    // Asigamos un valor que nos devuelve TRUE
    let mockRespuesta = '99999';

    // Verificamos que sea true la respuesta
    expect( component.validarRespuesta(mockRespuesta)).toBeTruthy();
    // Ahora validamos de manera analoga con el resto de valores
    mockRespuesta = '3001';
    // Verificamos que sea true la respuesta
    expect( component.validarRespuesta(mockRespuesta)).toBeTruthy();
    mockRespuesta = '9999';
    // Verificamos que sea true la respuesta
    expect( component.validarRespuesta(mockRespuesta)).toBeTruthy();

    // Ahora verificamos cuando nos retorna FALSE cuando no es ninguno de los anteriores
    mockRespuesta = 'Ninguno de los Anteiores';
    expect( component.validarRespuesta(mockRespuesta)).toBeFalsy();
  });

  it('Verificamos funcion cambiarFormatoFechaNacimiento()', () => {
    // Mockeamos una fecha
    const mockDate = '01/01/2000';
    // Llamamos nuestra funcion y le pasamos la fecha mockeada
    component.cambiarFormatoFechaNacimiento(mockDate);
    // Ahora verificamos que quite los " / "
    expect(component.editarUserForm.getRawValue().fecNac).toEqual('01012000');
  });

  it('Verificamos la funcion setearDatosRequest()', () => {

  });






});
