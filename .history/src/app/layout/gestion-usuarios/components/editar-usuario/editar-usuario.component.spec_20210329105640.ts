import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { EnvService } from 'src/app/env.service';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { ServicioService } from 'src/app/servicio/servicio.service';

import { EditarUsuarioComponent } from './editar-usuario.component';

fdescribe('EditarUsuarioComponent', () => {
  let component: EditarUsuarioComponent;
  let fixture: ComponentFixture<EditarUsuarioComponent>;
  let translateService: TranslateService;
  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarUsuarioComponent, ToastComponent ],
      imports: [
        CommonModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService ,
        {provide: ToastrService, useClass: ToastrService}],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarUsuarioComponent);
    translateService = TestBed.inject(TranslateService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('componente editar-usuario creado correctamente', () => {
    expect(component).toBeTruthy();
  });



  it('Componente CrearUsuario creado correctamente', () => {
    expect(component).toBeTruthy();
  });

  it('usuario es obligatorio en editarUserForm', () => {
    const usuario = component.editarUserForm.get('usuario');
    usuario.setValue('');
    expect(usuario.valid).toBeFalsy();
    usuario.setValue(null);
    expect(usuario.valid).toBeFalsy();
  });

  it('apPaterno es obligatorio en editarUserForm', () => {
    const apPaterno = component.editarUserForm.get('apPaterno');
    apPaterno.setValue('');
    expect(apPaterno.valid).toBeFalsy();
    apPaterno.setValue(null);
    expect(apPaterno.valid).toBeFalsy();
  });

  it('nombre es obligatorio en editarUserForm', () => {
    const nombre = component.editarUserForm.get('nombre');
    nombre.setValue('');
    expect(nombre.valid).toBeFalsy();
    nombre.setValue(null);
    expect(nombre.valid).toBeFalsy();
  });

  it('apMaterno es obligatorio en editarUserForm', () => {
    const apMaterno = component.editarUserForm.get('apMaterno');
    apMaterno.setValue('');
    expect(apMaterno.valid).toBeFalsy();
    apMaterno.setValue(null);
    expect(apMaterno.valid).toBeFalsy();
  });

  it('apMaterno es obligatorio en editarUserForm', () => {
    const fecNac = component.editarUserForm.get('fecNac');
    fecNac.setValue('');
    expect(fecNac.valid).toBeFalsy();
    fecNac.setValue(null);
    expect(fecNac.valid).toBeFalsy();
  });

  it('tipoDoc es obligatorio en editarUserForm', () => {
    const tipoDoc = component.editarUserForm.get('tipoDoc');
    tipoDoc.setValue('');
    expect(tipoDoc.valid).toBeFalsy();
    tipoDoc.setValue(null);
    expect(tipoDoc.valid).toBeFalsy();
  });

  it('numDoc es obligatorio en editarUserForm', () => {
    const numDoc = component.editarUserForm.get('tipoDoc');
    numDoc.setValue('');
    expect(numDoc.valid).toBeFalsy();
    numDoc.setValue(null);
    expect(numDoc.valid).toBeFalsy();
  });

  it('correo es obligatorio en editarUserForm', () => {
    const correo = component.editarUserForm.get('correo');
    correo.setValue('');
    expect(correo.valid).toBeFalsy();
    correo.setValue(null);
    expect(correo.valid).toBeFalsy();
  });

  it('rol es obligatorio en editarUserForm', () => {
    const rol = component.editarUserForm.get('rol');
    rol.setValue('');
    expect(rol.valid).toBeFalsy();
    rol.setValue(null);
    expect(rol.valid).toBeFalsy();
  });

  it('Campo "usuario" puede tener maximo 20 caracteres editarUserForm', () => {
    const usuario = component.editarUserForm.get('usuario');
    usuario.setValue('12345678901234567890');
    expect(usuario.valid).toBeTruthy();
    usuario.setValue('123456789012345678901');
    expect(usuario.valid).toBeFalsy();
  });

  it('Campo "usuario" debe tener minimo 5 caracteres editarUserForm', () => {
    const usuario = component.editarUserForm.get('usuario');
    usuario.setValue('12345');
    expect(usuario.valid).toBeTruthy();
    usuario.setValue('1234');
    expect(usuario.valid).toBeFalsy();
  });

  it('Campo "nombre" puede tener maximo 50 caracteres editarUserForm', () => {
    const nombre = component.editarUserForm.get('nombre');
    nombre.setValue('12345678901234567890123456789012345678901234567890');
    expect(nombre.valid).toBeTruthy();
    nombre.setValue('123456789012345678901234567890123456789012345678901');
    expect(nombre.valid).toBeFalsy();
  });

  it('Campo "nombre" puede tener minimo 1 caracter editarUserForm', () => {
    const nombre = component.editarUserForm.get('nombre');
    nombre.setValue('1');
    expect(nombre.valid).toBeTruthy();
    nombre.setValue('');
    expect(nombre.valid).toBeFalsy();
  });

  it('Campo "apPaterno" puede tener maximo 50 caracteres editarUserForm', () => {
    const apPaterno = component.editarUserForm.get('apPaterno');
    apPaterno.setValue('12345678901234567890123456789012345678901234567890');
    expect(apPaterno.valid).toBeTruthy();
    apPaterno.setValue('123456789012345678901234567890123456789012345678901');
    expect(apPaterno.valid).toBeFalsy();
  });

  it('Campo "apPaterno" puede tener minimo 1 caracter editarUserForm', () => {
    const apPaterno = component.editarUserForm.get('apPaterno');
    apPaterno.setValue('1');
    expect(apPaterno.valid).toBeTruthy();
    apPaterno.setValue('');
    expect(apPaterno.valid).toBeFalsy();
  });

  it('Campo "apMaterno" puede tener maximo 50 caracteres editarUserForm', () => {
    const apMaterno = component.editarUserForm.get('apMaterno');
    apMaterno.setValue('12345678901234567890123456789012345678901234567890');
    expect(apMaterno.valid).toBeTruthy();
    apMaterno.setValue('123456789012345678901234567890123456789012345678901');
    expect(apMaterno.valid).toBeFalsy();
  });

  it('Campo "apMaterno" puede tener minimo 1 caracter editarUserForm', () => {
    const apMaterno = component.editarUserForm.get('apMaterno');
    apMaterno.setValue('1');
    expect(apMaterno.valid).toBeTruthy();
    apMaterno.setValue('');
    expect(apMaterno.valid).toBeFalsy();
  });

  // Usuario solo permite letras y numeros en editarUserForm
  it('Usuario con caracteres especiales en editarUserForm', () => {
    const user = component.editarUserForm.get('usuario');
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b'];
    const U_No_Permitidos =
      ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
      '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
      '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]', '          '];

      // Bucle para caracteres permitidos en usuario editarUserForm
    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
        user.setValue(U_Permitidos[j]);
        expect(user.valid).toBe(true);
        // Cuando sea verdadero user.valid! no se cumple la condicion de verificacion
        if ( !user.valid) {
          console.log('Fallo en el caracter Permitido ' + U_Permitidos[j] );
        }
    }
      // Bucle para caracteres no permitidos en Usuario editarUserForm
    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
        user.setValue(U_No_Permitidos[i]);
        expect(user.valid).toBe(false);
        // Cuando sea verdadero no se cumple la condicion de verificacion
        if (user.valid) {
          console.log('Fallo en el caracter No Permitido ' + U_No_Permitidos[i] );
        }
      }
  });

  it('nombre con caracteres especiales en editarUserForm', () => {
    const nombre = component.editarUserForm.get('nombre');
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b', 'c123 bbb 4b', ' sdfds sdfsd'];
    const U_No_Permitidos =
      ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
      '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
      '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]'];
    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      nombre.setValue(U_Permitidos[j]);
        expect(nombre.valid).toBe(true);
        if ( !nombre.valid) {
          console.log('Fallo en el caracter Permitido ' + U_Permitidos[j] );
        }
    }
    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      nombre.setValue(U_No_Permitidos[i]);
        expect(nombre.valid).toBe(false);
        if (nombre.valid) {
          console.log('Fallo en el caracter No Permitido ' + U_No_Permitidos[i] );
        }
      }
  });

  it('apMaterno con caracteres especiales en editarUserForm', () => {
    const apMaterno = component.editarUserForm.get('apMaterno');
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b', 'c123 bbb 4b', ' sdfds sdfsd'];
    const U_No_Permitidos =
      ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
      '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
      '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]'];
    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      apMaterno.setValue(U_Permitidos[j]);
        expect(apMaterno.valid).toBe(true);
        if ( !apMaterno.valid) {
          console.log('Fallo en el caracter Permitido ' + U_Permitidos[j] );
        }
    }
    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      apMaterno.setValue(U_No_Permitidos[i]);
        expect(apMaterno.valid).toBe(false);
        if (apMaterno.valid) {
          console.log('Fallo en el caracter No Permitido ' + U_No_Permitidos[i] );
        }
      }
  });

  it('apPaterno con caracteres especiales en editarUserForm', () => {
    const apPaterno = component.editarUserForm.get('apPaterno');
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b', 'c123 bbb 4b', ' sdfds sdfsd'];
    const U_No_Permitidos =
      ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
      '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
      '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]'];
    for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
      apPaterno.setValue(U_Permitidos[j]);
        expect(apPaterno.valid).toBe(true);
        if ( !apPaterno.valid) {
          console.log('Fallo en el caracter Permitido ' + U_Permitidos[j] );
        }
    }
    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      apPaterno.setValue(U_No_Permitidos[i]);
        expect(apPaterno.valid).toBe(false);
        if (apPaterno.valid) {
          console.log('Fallo en el caracter No Permitido ' + U_No_Permitidos[i] );
        }
      }
  });

  /*  it('correo es cada maxima de caracteres es 100 en editarUserForm', () => {

      const correo = component.editarUserForm.get('correo');
      correo.setValue('1234567890123456789012345678901234567890123456789012345678901234567890123456789012@test.com');
      expect(correo.valid).toBeTruthy();
      correo.setValue('testing_2-@test.com');
      expect(correo.valid).toBeTruthy();
      correo.setValue('1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901@test23.com');
      expect(correo.valid).toBeFalsy();
    });*/






});
