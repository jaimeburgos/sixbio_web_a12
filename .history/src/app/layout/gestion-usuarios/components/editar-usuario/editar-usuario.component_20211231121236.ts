import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { Util } from 'src/app/nucleo/util/Util';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { SpinnerService } from 'src/app/services/spinner.service';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'app-editar-usuario',
    templateUrl: './editar-usuario.component.html',
    styleUrls: ['./editar-usuario.component.scss']
})
export class EditarUsuarioComponent implements OnInit {
    @ViewChild(ToastComponent, { static: true }) toast: ToastComponent;

    RolesAgrupadorValor = [];
    RolesAgrupadorClave = [];
    indiceRolAgrupador = 0;
    fechaNacInicial: any;
    valueMinLenght: number;
    valueMaxLenght: number;
    util: Util;

    editarUserForm: FormGroup;
    constructor(private router: Router, private rutaActual: ActivatedRoute, private servicioService: ServicioService,
        private datePipe: DatePipe, private spinnerService: SpinnerService, private fb: FormBuilder) {

        if (localStorage.getItem('RolUsuario') === 'EMP_VER') {
            this.singoff();
        }

        if (history.state.usuario === undefined && localStorage.getItem('RolUsuario') === 'EMP_ADM') {
            this.router.navigate(['home']);
        }

    }

    ngOnInit() {
        if (localStorage.getItem('RolUsuario') === 'EMP_VER') {
            this.router.navigate(['login']);
        }
        if (history.state.usuario === undefined && localStorage.getItem('RolUsuario') === 'EMP_ADM') {
            this.router.navigate(['home']);
        }
        this.RolesAgrupadorValor = Constant.ROLES_AGRUPADORES_VALOR;
        this.RolesAgrupadorClave = Constant.ROLES_AGRUPADORES_CLAVE;
        this.editarUserForm = this.fb.group({
            usuario: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(5),
                Validators.maxLength(20),
                Validators.pattern('[A-Za-z0-9]+')
            ])),
            apPaterno: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(1),
                Validators.maxLength(50),
                Validators.pattern('[A-Za-z0-9 ]+')
            ])),
            nombre: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(1),
                Validators.maxLength(50),
                Validators.pattern('[A-Za-z0-9 ]+')
            ])),
            apMaterno: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(1),
                Validators.maxLength(50),
                Validators.pattern('[A-Za-z0-9 ]+')
            ])),
            fecNac: new FormControl('', Validators.compose([
                Validators.required,
            ])),
            numDoc: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(this.valueMinLenght),
                Validators.maxLength(this.valueMaxLenght),
               // Validators.pattern('[0-9]+')
            ])),
            tipoDoc: new FormControl({ value: 0, disabled: true }, Validators.compose([])),
            correo: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(1),
                Validators.maxLength(100),
                Validators.email
            ])),
            estado: new FormControl(0, Validators.compose([
                Validators.required
            ])),
            telef: new FormControl('', Validators.compose([
                Validators.minLength(9),
                Validators.maxLength(9),
                Validators.pattern('[0-9]+')
            ])),
            rol: new FormControl(0, Validators.compose([Validators.required])),
            audFecCreac: new FormControl('', Validators.compose([])),
            audFecModif: new FormControl('', Validators.compose([])),
            audUsuCreac: new FormControl('', Validators.compose([])),
            audUsuModif: new FormControl('', Validators.compose([])),
            navigationId: new FormControl(0, Validators.compose([])),
        });
        this.asignacionEditarForm();
        this.indiceRolAgrupador = this.RolesAgrupadorValor.indexOf(this.editarUserForm.getRawValue().rol);

        this.fechaNacInicial = this.editarUserForm.getRawValue().fecNac;

    }
    singoff() {
        const dataRequestSingOff = {};
        this.servicioService.serviceSignOff(dataRequestSingOff).subscribe((dataResponse: any) => {

            if (dataResponse.codigoRespuesta === Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO) {
                localStorage.clear();
                this.router.navigate(['login']);
            } else {
                localStorage.clear();
                this.router.navigate(['login']);

            }
        }
            , error => {
                localStorage.clear();
                this.router.navigate(['login']);
            });
    }


    onChangeVacioNombre() {
        if (this.editarUserForm.getRawValue().nombre.trim() === '') {
            this.editarUserForm.controls.nombre.setValue('');
        }
    }

    onChangeVacioMaterno() {
        if (this.editarUserForm.getRawValue().apMaterno.trim() === '') {
            this.editarUserForm.controls.apMaterno.setValue('');
        }
    }

    onChangeVacioPaterno() {
        if (this.editarUserForm.getRawValue().apPaterno.trim() === '') {
            this.editarUserForm.controls.apPaterno.setValue('');
        }
    }


    cancelar() {
        this.router.navigate(['gestion-usuarios/']);
    }

    editarUsuario(values) {
        this.setearDatosRequest();
        this.cambiarFormatoFechaNacimiento(this.editarUserForm.getRawValue().fecNac);
        this.servicioService.serviceEditarUsuario(this.editarUserForm.getRawValue()).subscribe(
            (result: any) => {
                if (Constant.COD_OPERACION_SATISFACTORIA_SCA === result.codigoRespuesta) {
                    this.toast.addToast({ title: 'Info', msg: Constant.COD_OPERACION_SATISFACTORIA_SCA, timeOut: 5000, type: 'info' });
                    this.editarUserForm.controls.rol.setValue(values.rol);
                    this.editarUserForm.controls.fecNac.setValue(values.fecNac);
                    //    this.editarUserForm.controls.fecNac.setValue(this.fechaNacInicial);
                } else if (result.codigoRespuesta === Constant.COD_ERROR_SESION) {
                    this.salir();
                    return;
                } else if (this.validarRespuesta(result.codigoRespuesta)) {
                    this.editarUserForm.controls.rol.setValue(values.rol);
                    this.editarUserForm.controls.fecNac.setValue(values.fecNac);
                    //   this.editarUserForm.controls.fecNac.setValue(this.fechaNacInicial);
                    this.toast.addToast({ title: 'Error', msg: result.mensajeRespuesta, timeOut: 5000, type: 'error' });
                } else {
                    this.toast.addToast({ title: 'Error', msg: Constant.MENSAJE_ERROR_CONECTIVIDAD, timeOut: 5000, type: 'error' });
                    setTimeout(() => { this.salir(); }, 500);
                }
            }, error => {
                this.toast.addToast({ title: 'Error', msg: Constant.MENSAJE_ERROR_CONECTIVIDAD, timeOut: 5000, type: 'error' });
                setTimeout(() => { this.salir(); }, 500);
            }
        );
    }

    cambiarFormatoFechaNacimiento(fechaNacimiento) {
        console.log('Fecha Nacimiento anterior');
        console.log(fechaNacimiento);
        this.editarUserForm.controls.fecNac.setValue(fechaNacimiento.replace(/\//g, ''));
    }

    asignacionEditarForm() {
        this.editarUserForm.patchValue(history.state);

        this.editarUserForm.controls['rol'].setValue(this.RolesAgrupadorValor.indexOf(this.editarUserForm.getRawValue().rol));
    }

    setearDatosRequest() {
        this.editarUserForm.controls['rol'].setValue(this.RolesAgrupadorValor[this.editarUserForm.getRawValue().rol]);
        this.editarUserForm.controls['nombre'].setValue(this.editarUserForm.getRawValue().nombre.trim());
        this.editarUserForm.controls['apPaterno'].setValue(this.editarUserForm.getRawValue().apPaterno.trim());
        this.editarUserForm.controls['apMaterno'].setValue(this.editarUserForm.getRawValue().apMaterno.trim());
    }

    validarRespuesta(c) {
        if (c === '99999' || c === '9999' || c === '3001') {
            return true;
        } else {
            return false;
        }
    }
    salir() {
        const vacio = {};
        this.servicioService.serviceSignOff(vacio).subscribe((dataResponse: any) => {
            if (dataResponse.codigoRespuesta === Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO) {
                localStorage.clear();
                this.router.navigate(['/login']);
            } else {
                localStorage.clear();
                this.router.navigate(['/login']);
            }
        }
            , error => {
                localStorage.clear();
                this.router.navigate(['/login']);
            });
    }
}
