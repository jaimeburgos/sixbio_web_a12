import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { routerTransition } from '../../router.animations';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { IUsuarioSCA } from 'src/app/nucleo/interface/IUsuarioSCA';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { NgxPaginationModule } from 'ngx-pagination';
import { ExporterService } from 'src/app/services/exporter.service';
import { ModalComponent } from 'src/app/layout/bs-component/components';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Util } from 'src/app/nucleo/util/Util';
import { saveAs } from 'file-saver';
import { IReporteUsuario } from 'src/app/nucleo/interface/IReporteUsuario';
import { FormGroup } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { SpinnerService } from 'src/app/services/spinner.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-gestion-usuarios',
    templateUrl: './gestion-usuarios.component.html',
    styleUrls: ['./gestion-usuarios.component.scss'],
    animations: [routerTransition()]
})
export class GestionUsuariosComponent implements OnInit {
    @ViewChild(ToastComponent, { static: true }) toast: ToastComponent;
    @ViewChild(ModalDirective, { static: false }) modal: ModalDirective;

    @Input()
    pageSize: number;
    p = 1;
    ultimoUsuario: number;
    Roles = [];
    idUSuariosEliminados: String = '';
    banderaUsuarioEliminados = 1;
    listaUsuarios: IUsuarioSCA[] = [];
    listaUsuariosSixbio: IUsuarioSCA[] = [];

    usuariosCheckeados = new Set();
    footer = Constant.FOOTER_NOVATRONIC;
    listarUsuarioOk = false;
    dataRequest: Object = {};

    dataRequestEliminarUsuario: IUsuarioSCA = {
        usuariosAEliminar: []
    };

    dataRequestResetear: IUsuarioSCA = {
        usuarioAResetear: '',
        correo: ''
    };

    dataRequestReporte: IReporteUsuario = {
        listaUsuarios: Constant.LISTA_USUARIOS_FILTRADO,
        tipoReporte: ' ',
        f_usuario: ' ',
        f_nombre: ' ',
        f_apellidos: ' ',
        f_estado: ' ',

    };
    gestionarUsuarioForm: FormGroup;

    usuariosPorEliminar = [];
    entidad: Object = {};
    util: Util;
    filterUsuario = '';
    filterNombre = '';
    filterApellido = '';
    filterEstado = '';
    fechaReporte = '';
    header = '';
    body_table_header = '';
    body_table_body = '';
    messageModal: string;
    aceptar = false;

    // eslint-disable-next-line max-len
    constructor(private router: Router, private rutaActual: ActivatedRoute, private servicioService: ServicioService, private spinnerService: SpinnerService,
        private excelService: ExporterService, private modalService: NgbModal, private fb: FormBuilder, public translate: TranslateService) {
        translate.addLangs(['en', 'es']);
        translate.setDefaultLang('es');
        this.util = new Util(this.modalService);
        if (localStorage.getItem('RolUsuario') === 'EMP_VER') {
            this.singoff();
        }
    }

    ngOnInit() {
        if (localStorage.getItem('RolUsuario') === 'EMP_VER') {
            this.singoff();
        }
        this.listaUsuarios = [];
        this.listaUsuariosSixbio = [];
        this.listarRolesAgrupador();
        this.gestionarUsuarioForm = this.fb.group({
            filterUsuario: new FormControl('', Validators.compose([
                Validators.maxLength(20),
                Validators.pattern('[A-Za-z0-9]+')
            ])),
            filterApellido: new FormControl('', Validators.compose([
                Validators.maxLength(50),
                Validators.pattern('[A-Za-z0-9 ]+')
            ])),
            filterNombre: new FormControl('', Validators.compose([
                Validators.maxLength(50),
                Validators.pattern('[A-Za-z0-9 ]+')
            ])),
            filterEstado: new FormControl('', Validators.compose([])),
        });
    }

    onChangeForm() {
        this.p = 1;
    }
    singoff() {
        const dataRequestSingOff = {};
        this.servicioService.serviceSignOff(dataRequestSingOff).subscribe((dataResponse: any) => {

            if (dataResponse.codigoRespuesta === Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO) {
                localStorage.clear();
                this.router.navigate(['login']);
            } else {
                localStorage.clear();
                this.router.navigate(['login']);

            }
        }
            , error => {
                localStorage.clear();
                this.router.navigate(['login']);
            });
    }

    evento() {
        switch (this.messageModal) {
            case 'You are sure to perform the removal':
                if (this.aceptar) {
                    this.llamadaServicioEliminar();
                } else {
                    return;
                }
                break;
            case 'Do you want to reset the user password?':
                if (this.aceptar) {
                    this.llamadaServicioResetear();
                } else {
                    return;
                }
                break;
        }
    }

    decline() {
        this.aceptar = false;
        this.modal.hide();
    }

    confirm() {
        this.aceptar = true;
        this.modal.hide();
    }

    showModal() {
        this.aceptar = false;
        this.modal.show();
    }

    editarUsuario(usuario: Object) {
        this.entidad = usuario;
        console.log('Mi entidad es :');
        console.log(this.entidad);
        this.router.navigate(['editar-usuario'], { state: this.entidad, relativeTo: this.rutaActual });
    }

    crearUsuario() {
        this.router.navigate(['crear-usuario'], { relativeTo: this.rutaActual });
    }

    detalleUsuario(usuario: Object) {
        this.entidad = usuario;
        this.router.navigate(['detalle-usuario'], { state: this.entidad, relativeTo: this.rutaActual });
    }

    listarUsuarios() {
        this.listarUsuarioOk = false;
        this.servicioService.serviceListarUsuario(this.dataRequest).subscribe((result: any) => {
            if (Constant.COD_OPERACION_SATISFACTORIA_SCA === result.codigoRespuesta) {
                this.listaUsuarios = result.listarUsuario.listUsuarios;
                this.discriminarUsuarioSixbio();
                this.listarUsuarioOk = true;
            } else if (result.codigoRespuesta === Constant.COD_ERROR_SESION) {
                this.salir();
                return;
                // redirect inicio
            } else {
                this.toast.addToast({ title: 'Error', msg: result.mensajeRespuesta, timeOut: 5000, type: 'error' });
                this.listarUsuarioOk = true;
            }
        }, error => {
            // modal de error
            this.toast.addToast({ title: 'Error', msg: Constant.MENSAJE_ERROR_CONECTIVIDAD, timeOut: 5000, type: 'error' });
            this.listarUsuarioOk = true;
            setTimeout(() => { this.salir(); }, 500);
        }
        );

    }

    discriminarUsuarioSixbio() {
        this.listaUsuarios.forEach(element => {
            const existe = Constant.ROLES_AGRUPADORES_VALOR.includes(element.rol);
            if (existe) {
                this.listaUsuariosSixbio.push(element);
            }
        });
    }

    listarRolesAgrupador() {
        this.servicioService.serviceListarRolesAgrupador(this.dataRequest).subscribe(
            (result: any) => {
                if (Constant.COD_OPERACION_SATISFACTORIA_SCA === result.codigoRespuesta) {
                    Constant.ROLES_AGRUPADORES_VALOR = result.listarRolesAgrupadorValor;
                    Constant.ROLES_AGRUPADORES_CLAVE = result.listarRolesAgrupadorClave;
                    this.listarUsuarios();
                } else if (result.codigoRespuesta === Constant.COD_ERROR_SESION) {
                    this.salir();
                    return;
                    // redirect inicio
                } else {
                    this.toast.addToast({ title: 'Error', msg: result.mensajeRespuesta, timeOut: 5000, type: 'error' });
                    setTimeout(() => { this.salir(); }, 500);
                }
            }, error => {
                // modal de error
                this.toast.addToast({ title: 'Error', msg: Constant.MENSAJE_ERROR_CONECTIVIDAD, timeOut: 5000, type: 'error' });
                setTimeout(() => { this.salir(); }, 500);
            }
        );
    }

    resetearContrasenia(usuario: String, correo: string) {
        this.dataRequestResetear.usuarioAResetear = usuario;
        this.dataRequestResetear.correo = correo;
        this.messageModal = 'Do you want to reset the user password?';
        this.showModal();
    }

    llamadaServicioResetear() {
        //  this.setDataRequestResetear(this.dataRequestResetear.usuario, this.dataRequestResetear.correo);
        this.servicioService.serviceResetearUsuario(this.dataRequestResetear).subscribe(
            (result: any) => {
                if (Constant.COD_OPERACION_SATISFACTORIA_SCA === result.codigoRespuesta) {
                    this.toast.addToast({ title: 'Info', msg: Constant.COD_OPERACION_SATISFACTORIA_SCA, timeOut: 5000, type: 'info' });
                    this.listaUsuariosSixbio = [];
                    this.listarUsuarios();
                } else if (result.codigoRespuesta === Constant.COD_ERROR_SESION) {
                    this.salir();
                    return;
                    // redirect inicio
                } else {
                    this.toast.addToast({ title: 'Error', msg: result.mensajeRespuesta, timeOut: 5000, type: 'error' });
                    setTimeout(() => { this.salir(); }, 500);
                }
            }, error => {
                // modal de error
                this.toast.addToast({ title: 'Error', msg: Constant.MENSAJE_ERROR_CONECTIVIDAD, timeOut: 5000, type: 'error' });
                setTimeout(() => { this.salir(); }, 500);
            }
        );
    }

    /*  setDataRequestResetear(usuario: String, correo: string) {
           this.dataRequestResetear.usuarioAResetear = usuario;
           this.dataRequestResetear.correo = correo;
       }
   */
    onChange(usuario: IUsuarioSCA, isChekecd: boolean) {
        if (isChekecd) {
            this.usuariosCheckeados.add(usuario);
        } else {
            this.usuariosCheckeados.delete(usuario);
        }
    }

    eliminarUsuarios() {
        if (this.usuariosCheckeados.size > 0) {
            this.messageModal = 'You are sure to perform the removal';
            this.showModal();
            /*    this.util.showDefaultModalComponent(ModalComponent, '', 'You are sure to perform the removal').result.then((closeResult) => {
                    if (closeResult !== 'Confirm') {
                      return;
                    }
                    this.llamadaServicioEliminar();
                },
                (reason) => {
                  this.util.getDismissReason(reason);
                }
              );
            */
        } else {
            // alert seleccione usuario(s) a eliminar
            this.toast.addToast({ title: 'Error', msg: 'Select user(s) to be deleted', timeOut: 5000, type: 'error' });
        }
    }

    llamadaServicioEliminar() {
        this.setDataRequestEliminarUsuario();
        this.servicioService.serviceEliminarUsuario(this.dataRequestEliminarUsuario).subscribe(
            (result: any) => {
                if (Constant.COD_OPERACION_SATISFACTORIA_SCA === result.codigoRespuesta) {
                    this.limpiarDatosServicioEliminarUsuario();
                    this.toast.addToast({ title: 'Usuario(s) eliminado(s)', msg: result.usuariosEliminados, timeOut: 5000, type: 'info' });
                    this.p = 1;
                } else if (Constant.COD_ERROR_ELIMINAR_USUARIO === result.codigoRespuesta) {
                    if (result.usuariosEliminados === '') {
                        this.limpiarDatosServicioEliminarUsuario();
                        this.toast.addToast({ title: 'Error', msg: Constant.MENSAJE_ERROR_DE_ELIMINACION, timeOut: 5000, type: 'error' });
                    } else {
                        this.limpiarDatosServicioEliminarUsuario();
                        // eslint-disable-next-line max-len
                        this.toast.addToast({
                            title: 'Error', msg: Constant.MENSAJE_ERROR_DE_ELIMINACION_MULTIPLE +
                                result.usuariosEliminados + ' satisfactoriamente', timeOut: 5000, type: 'error'
                        });
                    }
                } else if (result.codigoRespuesta === Constant.COD_ERROR_SESION) {
                    this.salir();
                    return;
                    // redirect inicio
                } else {
                    this.limpiarDatosServicioEliminarUsuario();
                    this.toast.addToast({ title: 'Error', msg: result.mensajeRespuesta, timeOut: 5000, type: 'error' });
                }
            }, error => {
                this.limpiarDatosServicioEliminarUsuario();
                this.toast.addToast({ title: 'Error', msg: Constant.MENSAJE_ERROR_CONECTIVIDAD, timeOut: 5000, type: 'error' });
                setTimeout(() => { this.salir(); }, 500);
            }
        );

    }

    setDataRequestEliminarUsuario() {
        this.usuariosCheckeados.forEach(usuarioSeleccionado => {
            this.usuariosPorEliminar.push((usuarioSeleccionado as IUsuarioSCA).usuario);
        });
        this.dataRequestEliminarUsuario.usuariosAEliminar = this.usuariosPorEliminar;
    }

    limpiarDatosServicioEliminarUsuario() {
        this.usuariosCheckeados.clear();
        this.usuariosPorEliminar = [];
        this.listaUsuariosSixbio = [];
        this.dataRequestEliminarUsuario.usuariosAEliminar = [];
        this.listarUsuarios();
    }

    exportarExcel() {
        this.setTipoReporte('excel');
        this.exportar('reporteUsuario' + this.setFechaActual() + '.xls');
    }

    exportarPDF() {
        this.setTipoReporte('pdf');
        this.exportar('reporteUsuario' + this.setFechaActual() + '.pdf');
    }
    exportar(extension: string) {
        this.setdataRequestReporte();
        if (Constant.LISTA_USUARIOS_FILTRADO.length !== 0) {
            this.mensajeEspera();
            this.servicioService.serviceGenerarReporte(this.dataRequestReporte).subscribe(
                (result: any) => {
                    if (result.codigoRespuesta === Constant.COD_ERROR_SESION) {
                        this.salir();
                        return;
                    }
                    saveAs(result, extension);
                }, error => {
                    this.toast.addToast({ title: 'Error', msg: Constant.MENSAJE_ERROR_CONECTIVIDAD, timeOut: 1000, type: 'error' });
                    setTimeout(() => { this.salir(); }, 500);
                }
            );
        } else {
            this.toast.addToast({ title: 'Error', msg: Constant.SIN_DATOS_REPORTE, timeOut: 5000, type: 'error' });
        }

    }
    mensajeEspera() {
        this.toast.addToast({ title: 'Info', msg: Constant.MENSAJE_ESPERA_REPORTE, timeOut: 5000, type: 'info' });
    }

    setdataRequestReporte() {
        this.dataRequestReporte.listaUsuarios = Constant.LISTA_USUARIOS_FILTRADO;
        this.dataRequestReporte.f_usuario = this.gestionarUsuarioForm.getRawValue().filterUsuario;
        this.dataRequestReporte.f_nombre = this.gestionarUsuarioForm.getRawValue().filterNombre;
        this.dataRequestReporte.f_apellidos = this.gestionarUsuarioForm.getRawValue().filterApellido;
        this.dataRequestReporte.f_estado = this.gestionarUsuarioForm.getRawValue().filterEstado;
    }

    setTipoReporte(tipoReport: string) {
        this.dataRequestReporte.tipoReporte = tipoReport;
    }

    setFechaActual(): string {
        const fecha = new Date();
        const mes = '' + <string><any>(fecha.getMonth() + 1);
        const dia = '' + <string><any>fecha.getDate();
        const hora = '' + <string><any>fecha.getHours();
        const minuto = '' + <string><any>fecha.getMinutes();
        const segundo = '' + <string><any>fecha.getSeconds();
        this.fechaReporte = <string><any>fecha.getFullYear() + '' + mes.padStart(2, '0') + '' + dia.padStart(2, '0');
        // eslint-disable-next-line max-len
        this.fechaReporte = this.fechaReporte + hora.padStart(2, '0') + '' + minuto.padStart(2, '0') + '' + segundo.padStart(2, '0');
        return this.fechaReporte;
    }

    salir() {
        this.servicioService.serviceSignOff(this.dataRequest).subscribe((dataResponse: any) => {

            if (dataResponse.codigoRespuesta === Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO) {
                localStorage.clear();
                this.router.navigate(['/login']);
            } else {

                localStorage.clear();
                this.router.navigate(['/login']);

            }
        }
            , error => {
                localStorage.clear();
                this.router.navigate(['/login']);
            });
    }
}
