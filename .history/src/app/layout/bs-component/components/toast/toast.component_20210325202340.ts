import { Component, OnInit, Input, ViewChild, ViewEncapsulation } from '@angular/core';
import { ToastrService, ToastContainerDirective, GlobalConfig } from 'ngx-toastr';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-toast',
  templateUrl: './toast.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./toast.component.scss']
})
export class ToastComponent implements OnInit {

  options: GlobalConfig;
  @ViewChild(ToastContainerDirective, { static: true }) toastContainer: ToastContainerDirective;

  constructor(private toastr: ToastrService, private translateService: TranslateService) {
    this.toastr.overlayContainer = this.toastContainer;
    this.options = this.toastr.toastrConfig;

  }

  ngOnInit() {
  }

  addToast(optionsToast: any) {
    this.toastr.clear();
    this.options.titleClass = optionsToast.title;
    this.translateService.get(optionsToast.msg).subscribe(value => { this.options.messageClass = value; optionsToast.msg = value; });
    this.options.timeOut = optionsToast.timeOut;
    this.options.closeButton = true;
    this.options.enableHtml = true;
    this.options.progressBar = true;
    this.options.positionClass = 'toast-top-right';
/*
    switch (optionsToast.type) {
      case 'info': this.toastr.info(optionsToast.msg, optionsToast.title, this.options); break;
      case 'success': this.toastr.success(optionsToast.msg, optionsToast.title, this.options); break;
      case 'error': this.toastr.error(optionsToast.msg, optionsToast.title, this.options); break;
      case 'warning': this.toastr.warning(optionsToast.msg, optionsToast.title, this.options); break;
    }*/
  }

}






