import { Component, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})
export class ModalComponent {
    closeResult: string;
    @Input() title;
    @Input() message;
    constructor(public activeModal: NgbActiveModal) { }
/*
    open(content): string {
        this.modalService.open(content).result.then((result) => {
            this.closeResult = `${result}`;
            console.log("DENTRO DE CLOSED WITH:" + this.closeResult);
        }, (reason) => {
            this.closeResult = `${this.getDismissReason(reason)}`;
            console.log("DENTRO DE REASON:" + this.closeResult);
        });
        return this.closeResult;
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {

            console.log("DENTRO DE ESC");
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            console.log("DENTRO DE BACKDROP CLICK");

            return 'by clicking on a backdrop';
        } else {

            console.log("DENTRO DE ELSE");
            return `with: ${reason}`;
        }
    } */
}
