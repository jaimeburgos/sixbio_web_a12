import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { BsComponentRoutingModule } from './bs-component-routing.module';
import { BsComponentComponent } from './bs-component.component';
import {
    AlertComponent,
    ModalComponent,
    PaginationComponent,
} from './components';
import { PageHeaderModule } from '../../shared';
import { ToastComponent } from './components/toast/toast.component';
import { ToastrModule, ToastContainerModule } from 'ngx-toastr';

@NgModule({
    imports: [
        CommonModule,
        BsComponentRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NgbModule,
        PageHeaderModule,
        TranslateModule,
        ToastrModule.forRoot(),
        ToastContainerModule
    ],
    entryComponents: [ModalComponent],
    declarations: [
        BsComponentComponent,
        AlertComponent,
        ModalComponent,
        PaginationComponent,
        TooltipComponent,
        ToastComponent
    ],
    exports: [
        ToastrModule,
        ToastContainerModule,
        ToastComponent
    ]
})
export class BsComponentModule {}
