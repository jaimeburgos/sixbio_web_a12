import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { EnvService } from '../env.service';
import { ServicioService } from '../servicio/servicio.service';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { LayoutComponent } from './layout.component';
import { LayoutModule } from './layout.module';

class FakeRouter {
  navigate(params) {

  }
}

class FakeActivatedRoute {
  // params: Observable<any> = Observable.empty();
  private subject = new Subject();

  push( valor ) {
    this.subject.next( valor );
  }
  get params() {
    return this.subject.asObservable();
  }
}

fdescribe('LayoutComponent', () => {
  let component: LayoutComponent;
  let fixture: ComponentFixture<LayoutComponent>;

  beforeEach(
    waitForAsync(() => {
      TestBed.configureTestingModule({
        declarations: [ LayoutComponent, SidebarComponent, HeaderComponent, FooterComponent
        ],
      imports: [
          CommonModule,
          BrowserAnimationsModule,
          HttpClientTestingModule,
          RouterTestingModule,
          FormsModule,
          ReactiveFormsModule,
          NgxPaginationModule,
          ToastrModule.forRoot(),
          TranslateModule.forRoot(),
          BsDatepickerModule.forRoot(),
        ],
        providers: [
          TranslateService,
          BsLocaleService,
          DatePipe,
          EnvService,
          ServicioService ,
          {provide: Router , useClass: FakeRouter},
          {provide: ToastrService, useClass: ToastrService},
          { provide: ActivatedRoute, useClass: FakeActivatedRoute }],
        schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
      }).compileComponents();
    })
  );

  beforeEach(() => {
    const testObject = {
      _ningunaHuella: '0',
      _huellaVivaAmarillo: '0',
      _noHuellaViva: '0',
      _nistRojo: '0',
      _umbralHuellaViva: '50.0',
      _nistAmarillo: '0',
      _huellaVivaRojo: '0',
      _validarSixser: false,
      _serviciosVerificacion: {default: '301001', tipos:
      [{descripcion: 'T33-ANSI', codigo: '301001'},
      {descripcion: 'T33-ANSI', codigo: '301001'},
      {descripcion: 'T35-WSQ', codigo: '301100'}],
      tiposServicioVerificacion: [{_descripcion: 'T33-ANSI', _codigo: '301001'},
      {_descripcion: 'T33-ANSI', _codigo: '301001'}, {_descripcion: 'T35-WSQ', '_codigo': '301100'}]},
      _acciones: [{_descripcion: 'Mejor Huella', _codigo: 'sixbio'}, {_descripcion: 'Capturar', _codigo: 'sixbio'},
      {_descripcion: 'Verificar', _codigo: 'sixbio'}, {_descripcion: 'VerificarMOCard', _codigo: 'sixbio'},
      {_descripcion: 'Foto', _codigo: 'sixser'}, {_descripcion: 'Datos', _codigo: 'sixser'}],
      _idSesion: '000000202103111120504380242',
      _validarHuellaViva: true,
      _validarDniValidador: true,
      _visualizar: true,
      _sixbioVersion: '1.6',
      _usuario: 'jburgos6',
      _noHuellaCapturada: '0',
      _validarIpCliente: true,
      _rolesSCA: [{_permisos: [], _rol: 'EMP_ADM'}],
      _codigoRespuesta: '00000',
      _mensajeRespuesta: 'OPERACION REALIZADA SATISFACTORIAMENTE'};
    localStorage.setItem('datosSesion', JSON.stringify(testObject) );
    localStorage.setItem('RolUsuario', 'EMP_ADM');
    fixture = TestBed.createComponent(LayoutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Componente Layout creado correctamente', () => {
    // Fakeamos el evento recieveCollapsed para evitar errores
    const fakeEvent = spyOn(component, 'receiveCollapsed').and.callFake(() => {'Nada'; });
    expect(component).toBeTruthy();
  });
});
