import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import * as FileSaver from 'file-saver';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { async, of, Subject, throwError } from 'rxjs';
import { EnvService } from 'src/app/env.service';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { ToastComponent } from '../bs-component/components/toast/toast.component';
import { VerificacionComponent } from './verificacion.component';

class FakeRouter {
  navigate(params) {

  }
}

class FakeActivatedRoute {
  // params: Observable<any> = Observable.empty();
  private subject = new Subject();

  push( valor ) {
    this.subject.next( valor );
  }
  get params() {
    return this.subject.asObservable();
  }
}

fdescribe('VerificacionComponent', () => {
  let component: VerificacionComponent;
  let fixture: ComponentFixture<VerificacionComponent>;
  // Instanciamos los servicios que utilizaremos
  let translateService: TranslateService;
  let servicio: ServicioService;
  beforeEach((() => {
    TestBed.configureTestingModule({
      declarations: [ VerificacionComponent, ToastComponent ],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService ,
        {provide: Router , useClass: FakeRouter},
        {provide: ToastrService, useClass: ToastrService},
        { provide: ActivatedRoute, useClass: FakeActivatedRoute }
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificacionComponent);
    translateService = TestBed.inject(TranslateService);
    // Injectamos nuestro servicio con TestBed.inject
    servicio = TestBed.inject(ServicioService);
    component = fixture.componentInstance;
    fixture.detectChanges();
    // Mockeamos valor en localstorage de usuarioLogueado
  //  localStorage.setItem('usuarioLogueado', 'jburgos6');
  });

  it('VerificacionComponent creado correctamente', () => {
    expect(component).toBeTruthy();
  });

});
