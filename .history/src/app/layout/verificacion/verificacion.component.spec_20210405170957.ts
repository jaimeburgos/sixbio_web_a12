import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import * as FileSaver from 'file-saver';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { async, of, Subject, throwError } from 'rxjs';
import { EnvService } from 'src/app/env.service';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { ToastComponent } from '../bs-component/components/toast/toast.component';
import { VerificacionComponent } from './verificacion.component';

class FakeRouter {
    navigate(params) {

    }
}

class FakeActivatedRoute {
    // params: Observable<any> = Observable.empty();
    private subject = new Subject();

    push(valor) {
        this.subject.next(valor);
    }
    get params() {
        return this.subject.asObservable();
    }
}

fdescribe('VerificacionComponent', () => {
    let component: VerificacionComponent;
    let fixture: ComponentFixture<VerificacionComponent>;
    // Instanciamos los servicios que utilizaremos
    let translateService: TranslateService;
    let servicio: ServicioService;
    beforeEach((() => {
        TestBed.configureTestingModule({
            declarations: [VerificacionComponent, ToastComponent],
            imports: [
                CommonModule,
                BrowserAnimationsModule,
                HttpClientTestingModule,
                RouterTestingModule,
                FormsModule,
                ReactiveFormsModule,
                NgxPaginationModule,
                ToastrModule.forRoot(),
                TranslateModule.forRoot(),
                BsDatepickerModule.forRoot(),
            ],
            providers: [
                TranslateService,
                BsLocaleService,
                DatePipe,
                EnvService,
                ServicioService,
                { provide: Router, useClass: FakeRouter },
                { provide: ToastrService, useClass: ToastrService },
                { provide: ActivatedRoute, useClass: FakeActivatedRoute }
            ],
            schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(VerificacionComponent);
        translateService = TestBed.inject(TranslateService);
        // Injectamos nuestro servicio con TestBed.inject
        servicio = TestBed.inject(ServicioService);
        component = fixture.componentInstance;
        fixture.detectChanges();
        // Mockeamos valor en localstorage de datosSesion
        const mockDatosSesion = {
            '_ningunaHuella': '0',
            '_huellaVivaAmarillo': '0',
            '_noHuellaViva': '0',
            '_nistRojo': '0',
            '_umbralHuellaViva': '50.0',
            '_nistAmarillo': '0',
            '_huellaVivaRojo': '0',
            '_validarSixser': false,
            '_serviciosVerificacion': {
                'default': '301001',
                'tipos': [
                    {
                        'descripcion': 'T33-ANSI',
                        'codigo': '301001'
                    },
                    {
                        'descripcion': 'T33-ANSI',
                        'codigo': '301001'
                    },
                    {
                        'descripcion': 'T35-WSQ',
                        'codigo': '301100'
                    }
                ],
                'tiposServicioVerificacion': [
                    {
                        '_descripcion': 'T33-ANSI',
                        '_codigo': '301001'
                    },
                    {
                        '_descripcion': 'T33-ANSI',
                        '_codigo': '301001'
                    },
                    {
                        '_descripcion': 'T35-WSQ',
                        '_codigo': '301100'
                    }
                ]
            },
            '_acciones': [
                {
                    '_descripcion': 'Mejor Huella',
                    '_codigo': 'sixbio'
                },
                {
                    '_descripcion': 'Capturar',
                    '_codigo': 'sixbio'
                },
                {
                    '_descripcion': 'Verificar',
                    '_codigo': 'sixbio'
                },
                {
                    '_descripcion': 'VerificarMOCard',
                    '_codigo': 'sixbio'
                },
                {
                    '_descripcion': 'Foto',
                    '_codigo': 'sixser'
                },
                {
                    '_descripcion': 'Datos',
                    '_codigo': 'sixser'
                }
            ],
            '_idSesion': '000000202104051226028320041',
            '_validarHuellaViva': true,
            '_validarDniValidador': true,
            '_visualizar': 'true',
            '_sixbioVersion': '1.6',
            '_usuario': 'jburgos5',
            '_noHuellaCapturada': '0',
            '_validarIpCliente': true,
            '_rolesSCA': [
                {
                    '_permisos': [],
                    '_rol': 'EMP_VER'
                },
                {
                    '_permisos': [],
                    '_rol': 'VAL'
                }
            ],
            '_codigoRespuesta': '00000',
            '_mensajeRespuesta': 'OPERACION REALIZADA SATISFACTORIAMENTE'
        };

        localStorage.setItem('datosSesion', JSON.stringify(mockDatosSesion));

    });

    it('VerificacionComponent creado correctamente', () => {
        expect(component).toBeTruthy();
    });

    it('Verificamos la funcion ngOnInit()', () => {
        // Espiamos nuestra funcion setIsDniValidatorValid
        const spysetIsDniValidatorValid = spyOn(component, 'setIsDniValidatorValid').and.callThrough();
        // Inyectamos Router que fue previamente fakeado
        const router = TestBed.inject(Router);
        // Espiamos que llame a navigate router
        const spyRouter = spyOn(router, 'navigate').and.callThrough();
        // Llamamos nuestra funcion ngOnInit ()
        component.ngOnInit();
        // Verificamos el valor de dniValidadorLS debe ser TRUE
        expect(component.dniValidadorLS).toBeTruthy();
        // Verificamos que el valor seteado al item isWithDniValidator sea 'true'
        expect(localStorage.getItem('isWithDniValidator')).toEqual('true');
        // Verificamos que fuera llamada nuestra funcion setIsDniValidatorValid
        expect(spysetIsDniValidatorValid).toHaveBeenCalled();
        // Verificamos que llame nuestra funcion navigate de router
        expect(spyRouter).toHaveBeenCalled();
    });


    it('Verificamos la funcion setIsDniValidatorValid()', () => {
        // Verificamos el primer caso , dado que mockeamos inicialmente data para el item isWithDniValidator del localStorage sera true
        // Llamamos nuestra funcion
        component.setIsDniValidatorValid();
        // Verificamos el valor de isDniValidator  sea TRUE
        expect(component.isDniValidator).toBeTruthy();
        // Ahora verificamos en caso no se cumpla la primera condicion
        localStorage.setItem('isWithDniValidator', 'Cualquier Cosa');
        // Llamamos nuestra funcion
        component.setIsDniValidatorValid();
        // Verificamos el valor de isDniValidator  sea FALSE
        expect(component.isDniValidator).toBeFalsy();
    });

    it('Verificamos la funcion verificatorConDniValidator()', () => {
        // Le damos valor true a siDniValidador para verificar la primera condicion
        component.siDniValidador = true;
        // Llamamos nuestra funcion
        component.verificatorConDniValidator();
        // Verificamos el valor de que nos retorna la funcion , para este caso es TRUE
        expect(component.verificatorConDniValidator()).toBeTruthy();
        // Ahora verificamos el caso donde sea FALSE
        component.siDniValidador = false;
        // Llamamos nuestra funcion
        component.verificatorConDniValidator();
        // Verificamos el valor de que nos retorna la funcion , para este caso es FALSE
        expect(component.verificatorConDniValidator()).toBeFalsy();
    });

    it('Verificamos nuestra funcion verificatorConSixSer()', () => {
        // Asigamos 'true' a siSixSer para probar la primera condicion
        component.siSixSer = 'true';
        // Llamamos nuestra funcion verificatorConSixSer
        component.verificatorConSixSer();
        // Verificamos el valor de que nos retorna nuestra funcion sea TRUE
        expect(component.verificatorConSixSer()).toBeTruthy();
        // Ahora verificamos el caso que sea diferente de 'true' siSixSer
        component.siSixSer = 'Cualquier cosa';
        // Llamamos nuestra funcion verificatorConSixSer
        component.verificatorConSixSer();
        // Verificamos el valor de que nos retorna nuestra funcion sea FALSE
        expect(component.verificatorConSixSer()).toBeFalsy();

    });

    it('Verificamos la funcion changeOfHtmlByTypCapture()', () => {
        // Probamos las condiciones del switch
        // para el primer caso asigamos a this.entidad.tipoCaptura el valor 0
        component.entidad.tipoCaptura = 0;
        // Llamamos nuestra funcion
        component.changeOfHtmlByTypCapture();
        // Verificamos el valor de que se asigna a isValidDedoDerecho y a isValidDedoIzquierdo
        expect(component.isValidDedoDerecho).toBeTruthy();
        expect(component.isValidDedoIzquierdo).toBeFalsy();

        // Ahora verificamos para el valor de 1
        component.entidad.tipoCaptura = 1;
        // Llamamos nuestra funcion
        component.changeOfHtmlByTypCapture();
        // Verificamos el valor de que se asigna a isValidDedoDerecho y a isValidDedoIzquierdo
        expect(component.isValidDedoDerecho).toBeFalsy();
        expect(component.isValidDedoIzquierdo).toBeTruthy();

        // Ahora verificamos para el valor de 2
        component.entidad.tipoCaptura = 2;
        // Llamamos nuestra funcion
        component.changeOfHtmlByTypCapture();
        // Verificamos el valor de que se asigna a isValidDedoDerecho y a isValidDedoIzquierdo
        expect(component.isValidDedoDerecho).toBeTruthy();
        expect(component.isValidDedoIzquierdo).toBeTruthy();


        // Ahora verificamos para el caso por default del switch , es decir no sea 0 , 1 o 2
        component.entidad.tipoCaptura = 3;
        // Llamamos nuestra funcion
        component.changeOfHtmlByTypCapture();
        // No hay ninguna asignacion para este caso no se realiza accion alguna
    });

    it('Verificamos la funcio nmostrarFoto(foto)', () => {
        const mockFoto = 'Test_Foto';
        // Llamamos nuestra funcion y le pasamos el dato mockeado
        component.mostrarFoto(mockFoto);
        // Verificamos que se asigne correctamente el dato mockFoto a la variable this.foto del componente
        expect(component.foto).toEqual(mockFoto);
    });


    it('Verificamos la funcion mostrarHuellaDerecha(datos: IDatosHuellaDerecha)', () => {
        // Mockeamos la data que le pasaremos a nuestra funcion
        const mockDatos = {
            huellaD : 'Test_HuellaD',
            nistD : 'Test_nistDerecha',
            huellavivaD : 'Test_huellaVivaDerecha',
            imagenNistD : 'Test_imagenNistDerecha',
            imagenhuellavivaD : 'Test_imagenHuellaVivaDerecha',

        };
        // Llamamos nuestra funcion y le pasamos mockDatos
        component.mostrarHuellaDerecha(mockDatos);
        // Verificamos que los valores se asignen correctamente a nuestras variables
        expect(component.huellaDerecha).toEqual(mockDatos.huellaD);
        expect(component.nistDerecha).toEqual(mockDatos.nistD);
        expect(component.huellaVivaDerecha).toEqual(mockDatos.huellavivaD);
        expect(component.imagenNistDerecha).toEqual(mockDatos.imagenNistD);
        expect(component.imagenHuellaVivaDerecha).toEqual(mockDatos.imagenhuellavivaD);

    });

    it('Verificamos la funcion mostrarHuellaIzquierda(datos: IDatosHuellaIzquierda)', () => {
        // Mockeamos la data que le pasaremos a nuestra funcion
        const mockDatos = {
            huellaI : 'Test_huellaIzquierda',
            nistI : 'Test_nistIzquierda',
            huellavivaI : 'Test_huellaVivaIzquierda',
            imagenNistI : 'Test_imagenNistIzquierda',
            imagenhuellavivaI : 'Test_imagenHuellaVivaIzquierda'

        };
        // Llamamos nuestra funcion y le pasamos mockDatos
        component.mostrarHuellaIzquierda(mockDatos);
        // Verificamos que los valores se asignen correctamente a nuestras variables
        expect(component.huellaIzquierda).toEqual(mockDatos.huellaI);
        expect(component.nistIzquierda).toEqual(mockDatos.nistI);
        expect(component.huellaVivaIzquierda).toEqual(mockDatos.huellavivaI);
        expect(component.imagenNistIzquierda).toEqual(mockDatos.imagenNistI);
        expect(component.imagenHuellaVivaIzquierda).toEqual(mockDatos.imagenhuellavivaI);

    });

    it('Verificamos nuestra funcion  mostrarMensajeError(error: IErrorValidador)', () => {
        // Mockeamos el valor de Error que se le pasa a nuestra funcion , son valores de prueba cualquiera
        const mockError = {
            mensajeError: 'Test mensajeError',
            banderaError: true,
            tipoInput: ''
        };
        // Verificaremos para la primera condicion cuando tipoInput sea dniValidador
        mockError.tipoInput = 'dniValidador';
        // Llamamos nuestra funcion
        component.mostrarMensajeError(mockError);
        // Verificamos que los valores asignados sean los correctos
        expect(component.messageValidatorByDniValidator).toEqual(mockError.mensajeError);
        expect(component.isDniValidatorValid).toBeTruthy(mockError.banderaError);

        // Verificamos la segunda condicion cuando tipoInput sea dniConsultor
        mockError.tipoInput = 'dniConsultor';
        // Llamamos nuestra funcion
        component.mostrarMensajeError(mockError);
        // Verificamos que los valores asignados sean los correctos
        expect(component.messageValidatorByDniConsultor).toEqual(mockError.mensajeError);
        expect(component.isDniConsultorValid).toBeTruthy(mockError.banderaError);

        // Verificamos el caso que ninguno de los casos anteriores se cumplan
        mockError.tipoInput = 'Ninguno del os anteriores';
        // Llamamos nuestra funcion
        component.mostrarMensajeError(mockError);
        // Verificamos que los valores asignados sean los correctos
        expect(component.messageValidatorByDniValidator).toEqual(mockError.mensajeError);
        expect(component.isDniValidatorValid).toBeTruthy(mockError.banderaError);
        expect(component.messageValidatorByDniConsultor).toEqual(mockError.mensajeError);
        expect(component.isDniConsultorValid).toBeTruthy(mockError.banderaError);

    });


    it('Verificamos la funcion respuestaServicio()', () => {
        // Mockeamos un valor mockRespuesta para pasarlo a nuestra funcion
        const mockRespuesta = {
            codigoRespuesta: '',
            mensajeRespuesta: ''
        };
        // Asigamos los valores codigoRespuesta Constant.CODIGO_VACIO y mensajeRespuesta Constant.MENSAJE_VACIO
        // Para validar la primera condicion
        mockRespuesta.codigoRespuesta = Constant.CODIGO_VACIO;
        mockRespuesta.mensajeRespuesta = Constant.MENSAJE_VACIO;
        // Espiamos nuestra funcion cleanMensajes
        const spycleanMensajes = spyOn(component, 'cleanMensajes').and.callThrough();
        // Llamamos nuestra funcion y le pasamos mockRespuesta
        component.respuestaServicio(mockRespuesta);
        // Verificamos nuestra funcion clearMensajes fuera llamada
        expect(spycleanMensajes).toHaveBeenCalled();


        // Ahora verificamos para el caso donde mensajeRespuesta sea cleanFront
        mockRespuesta.mensajeRespuesta = 'cleanFront';
        // Espiamos nuestra funcion clearDatosConsolidado
        const spyclearDatosConsolidado = spyOn(component, 'clearDatosConsolidado').and.callThrough();
        // Llamamos nuestra funcion y le pasamos mockRespuesta
        component.respuestaServicio(mockRespuesta);
        // Verificamos los valores que se asignen correctamente
        expect(component.messageValidatorByDniValidator).toEqual('');
        expect(component.codigoRespuestaServicio).toEqual('');
        expect(component.isValidMessageServices).toBeFalsy();
        expect(component.isDniConsultorValid).toBeFalsy();
        // Verificamos nuestra funcion clearMensajes fuera llamada
        expect(spycleanMensajes).toHaveBeenCalled();
        // Verificamos nuestra funcion clearDatosConsolidado fuera llamada
        expect(spyclearDatosConsolidado).toHaveBeenCalled();


        // Ahora verificamos la condicion cuando codigoRespuesta sea Constant.CODIGO_VACIO y mensajeRespuesta !== Constant.MENSAJE_VACIO
        mockRespuesta.codigoRespuesta = Constant.CODIGO_VACIO;
        mockRespuesta.mensajeRespuesta = 'Cualquier cosa que es diferente de mensaje vacio';
        // Llamamos nuestra funcion y le pasamos mockRespuesta
        component.respuestaServicio(mockRespuesta);
        // Verificamos la asignacion de los valores a las variables
        expect(component.mensageRespuestaServicio).toEqual(mockRespuesta.mensajeRespuesta);
        expect(component.isValidMessageServices).toBeTruthy();

        // Verificamos para el caso codigoRespuesta sea Constant.CODIGO_OPERACION_ERROR
        mockRespuesta.codigoRespuesta = Constant.CODIGO_OPERACION_ERROR;
        // Espiamos nuestra funcion cleanImagen()
        const spyclearImagen = spyOn(component, 'cleanImagen').and.callThrough();
        // Espiamos nuestra funcion cleanFirma()
        const spycleanFirma = spyOn(component, 'cleanFirma').and.callThrough();
        // Llamamos nuestra funcion y le pasamos mockRespuesta
        component.respuestaServicio(mockRespuesta);
        // Verificamos que los valores sean asignadas a las variablex
        expect(component.codigoRespuestaServicio).toEqual(mockRespuesta.codigoRespuesta);
        expect(component.mensageRespuestaServicio).toEqual(mockRespuesta.mensajeRespuesta);
        expect(component.isValidMessageServices).toBeTruthy();
        // Verificamos nuestra funcion clearDatosConsolidado sea llamada
        expect(spyclearDatosConsolidado).toHaveBeenCalled();
        // Verificamos nuestra funcion clearImagen sea llamada
        expect(spyclearImagen).toHaveBeenCalled();
        // Verificamos nuestra funcion cleanFirma sea llamada
        expect(spycleanFirma).toHaveBeenCalled();

        // Ahora verificamos para el caso que no se den ninguna de las condiciones anteriores
        mockRespuesta.codigoRespuesta = ' Ninguno de los anteriores';
        mockRespuesta.mensajeRespuesta = 'Ninguno de los anteriores';
        // Llamamos nuestra funcion y le pasamos mockRespuesta
        component.respuestaServicio(mockRespuesta);
        // Verificamos que los valores sean asignadas a nuestras variables
        expect(component.codigoRespuestaServicio).toEqual(mockRespuesta.codigoRespuesta);
        expect(component.mensageRespuestaServicio).toEqual(mockRespuesta.mensajeRespuesta);
        expect(component.isValidMessageServices).toBeTruthy();
    });


    it('Verificamos nuestra funcion cambiarTituloDeTipoHuella()', () => {
        // Mockeamos el valor data que le pasaremos a nuestra funcion
        const mockData = {
            descripcionTipoHuellaDerecha : 'descripcion huella derecha TEST',
            descripcionTipoHuellaIzquierda: 'descripcion huella izquierda TEST',
        };
        // Llamamos nuestra funcion y le pasamos mockData
        component.cambiarTituloDeTipoHuella(mockData);
        // Verificamos que los valores sean asignados
        expect(component.tituloDeTipoHuellaDerecha).toEqual(mockData.descripcionTipoHuellaDerecha);
        expect(component.tituloDeTipoHuellaIzquierdo).toEqual(mockData.descripcionTipoHuellaIzquierda);


    });

    it('Verificamos la funcion mostrarDatosConsolidado(datos : IResponseDatosConsolidado)', () => {
        // Mockeamos los datos para pasarle a nuestra funcion
        const mockData = {
            apellidos: 'Test Apellidos',
            apellidoPaterno: 'Test Apellido Parterno',
            fechaNacimiento: '01/01/2000',
            constanciaVotacionDesc: 'Test constancia',
            nombreMadre: 'Nombre Madre Test',
            codigoUbigeoProvDomicilio: '150392',
            direccion: 'Direccion Test',
            direccionDomicilio: 'direccionDomicilio TEST',
            estatura: 'Estatura TEST',
            constanciaVotacionCodigo: 'constanciaVotacionCodigo TEST',
            localidadDomicilio: 'LOCALIDAD Domicilio TEST',
            provinciaNacimiento: 'Test provinciaNacimiento',
            apellidoMaterno: 'TEST apellidoMaterno',
            codigoUbigeoDistNacimiento: 'TEST codigoUbigeoDistNacimiento',
            nombres: 'TEST nombres',
            restricciones: 'TEST restricciones',
            caducidadDescripcion: 'TEST caducidadDescripcion',
            codigoUbigeoDistDomicilio: 'TEST codigoUbigeoDistDomicilio',
            codigoUbigeoProvNacimiento: 'TEST codigoUbigeoProvNacimiento',
            nombrePadre: 'TEST nombrePadre',
            codigoUbigeoLocalidadNacimiento: 'TEST codigoUbigeoLocalidadNacimiento',
            numeroLibro: 'TEST numeroLibro',
            localidad: 'TEST localidad',
            distritoNacimiento: 'TEST distritoNacimiento',
            ubigeoVotacion: 'TEST ubigeoVotacion',
            dni: 'TEST dni',
            codigoUbigeoLocalidadDomicilio: 'TEST codigoUbigeoLocalidadDomicilio',
            departamentoDomicilio: 'TEST departamentoDomicilio',
            grupoVotacion: 'TEST grupoVotacion',
            provinciaDomicilio: 'TEST provinciaDomicilio',
            restriccionesDesc: 'TEST restriccionesDesc',
            caducidadCodigo: 'TEST caducidadCodigo',
            anioEstudio: 'TEST anioEstudio',
            departamentoNacimiento: 'TEST departamentoNacimiento',
            numeroDocSustentarioIdentidad: 'TEST numeroDocSustentarioIdentidad',
            estadoCivilDescripcion: 'TEST estadoCivilDescripcion',
            fechaInscripcion: 'TEST fechaInscripcion',
            codigoUbigeoDeptoDomicilio: 'TEST codigoUbigeoDeptoDomicilio',
            estadoCivilCodigo: 'TEST estadoCivilCodigo',
            fechaExpedicion: 'TEST fechaExpedicion',
            tipoDocSustentarioIdentidad: 'TEST tipoDocSustentarioIdentidad',
            distritoDomicilio: 'TEST distritoDomicilio',
            sexoCodigo: 'TEST sexoCodigo',
            codigoUbigeoDeptoNacimiento: 'TEST codigoUbigeoDeptoNacimiento',
            sexoDescripcion: 'TEST sexoDescripcion',
            foto: 'TEST foto',
            firma: 'TEST firma',
        };
        // Espiamos nuestra funcion mostrarFoto
        const spymostrarFoto = spyOn(component, 'mostrarFoto').and.callThrough();
        // Espiamos nuestra funcion mostrarFirma
        const spymostrarFirma = spyOn(component, 'mostrarFirma').and.callThrough();
        // Espiamos nuestra funcion mostrarDatosPersona
        const spymostrarDatosPersonao = spyOn(component, 'mostrarDatosPersona').and.callThrough();
        // Llamamos nuestra funcion y le pasamos los datos mockeados
        component.mostrarDatosConsolidado(mockData);

        // Ahora verificamos que nuestras funciones sean llamadas
        expect(spymostrarFoto).toHaveBeenCalled();
        expect(spymostrarFirma).toHaveBeenCalled();
        expect(spymostrarDatosPersonao).toHaveBeenCalled();
    });

    it('Verificamos la funcion mostrarFirma', () => {
        // mockeamos el valor de firma para pasarlo a nuestra funcion
        const mockFirma = 'Test Firma';
        // Llamamos nuestra funcion
        component.mostrarFirma(mockFirma);
        // Verificamos que nuestra variable sea asignada
        expect(component.firma).toEqual(mockFirma);
    });

    it('Verificamos la funcion mostrarDatosPersona', () => {
        // mockeamos datos que se le pasara a nuestra funcion
        const mockDatos = {
            apellidos: 'Test Apellidos',
            apellidoPaterno: 'Test Apellido Parterno',
            fechaNacimiento: '01/01/2000',
            constanciaVotacionDesc: 'Test constancia',
            nombreMadre: 'Nombre Madre Test',
            codigoUbigeoProvDomicilio: '150392',
            direccion: 'Direccion Test',
            direccionDomicilio: 'direccionDomicilio TEST',
            estatura: 'Estatura TEST',
            constanciaVotacionCodigo: 'constanciaVotacionCodigo TEST',
            localidadDomicilio: 'LOCALIDAD Domicilio TEST',
            provinciaNacimiento: 'Test provinciaNacimiento',
            apellidoMaterno: 'TEST apellidoMaterno',
            codigoUbigeoDistNacimiento: 'TEST codigoUbigeoDistNacimiento',
            nombres: 'TEST nombres',
            restricciones: 'TEST restricciones',
            caducidadDescripcion: 'TEST caducidadDescripcion',
            codigoUbigeoDistDomicilio: 'TEST codigoUbigeoDistDomicilio',
            codigoUbigeoProvNacimiento: 'TEST codigoUbigeoProvNacimiento',
            nombrePadre: 'TEST nombrePadre',
            codigoUbigeoLocalidadNacimiento: 'TEST codigoUbigeoLocalidadNacimiento',
            numeroLibro: 'TEST numeroLibro',
            localidad: 'TEST localidad',
            distritoNacimiento: 'TEST distritoNacimiento',
            ubigeoVotacion: 'TEST ubigeoVotacion',
            dni: 'TEST dni',
            codigoUbigeoLocalidadDomicilio: 'TEST codigoUbigeoLocalidadDomicilio',
            departamentoDomicilio: 'TEST departamentoDomicilio',
            grupoVotacion: 'TEST grupoVotacion',
            provinciaDomicilio: 'TEST provinciaDomicilio',
            restriccionesDesc: 'TEST restriccionesDesc',
            caducidadCodigo: 'TEST caducidadCodigo',
            anioEstudio: 'TEST anioEstudio',
            departamentoNacimiento: 'TEST departamentoNacimiento',
            numeroDocSustentarioIdentidad: 'TEST numeroDocSustentarioIdentidad',
            estadoCivilDescripcion: 'TEST estadoCivilDescripcion',
            fechaInscripcion: 'TEST fechaInscripcion',
            codigoUbigeoDeptoDomicilio: 'TEST codigoUbigeoDeptoDomicilio',
            estadoCivilCodigo: 'TEST estadoCivilCodigo',
            fechaExpedicion: 'TEST fechaExpedicion',
            tipoDocSustentarioIdentidad: 'TEST tipoDocSustentarioIdentidad',
            distritoDomicilio: 'TEST distritoDomicilio',
            sexoCodigo: 'TEST sexoCodigo',
            codigoUbigeoDeptoNacimiento: 'TEST codigoUbigeoDeptoNacimiento',
            sexoDescripcion: 'TEST sexoDescripcion',
            foto: 'TEST foto',
            firma: 'TEST firma',
        };
        // Llamamos nuestra funcion mostrarDatosPersona
        component.mostrarDatosPersona(mockDatos);
        // Verificamos que nuestra variable se le asignen los datos como se esperaba
        expect(component.datosConsolidado).toEqual(mockDatos);
    });


    it('Verificamos la funcion cleanMensajes()', () => {
        // Espiamos nuestra funcion clearDatosConsolidado
        const spyclearDatosConsolidado = spyOn(component, 'clearDatosConsolidado').and.callThrough();
        // Espiamos nuestra funcioncleanImagen
        const spyclearImagen = spyOn(component, 'cleanImagen').and.callThrough();
        // Espiamos nuestra funcion cleanFirma
        const spycleanFirma = spyOn(component, 'cleanFirma').and.callThrough();
        // Llamamos nuestra funcion
        component.cleanMensajes();
        // Verificamos los valores que asigna nuestra funcion
        expect(component.codigoRespuestaServicio).toEqual('');
        expect(component.mensageRespuestaServicio).toEqual('');
        expect(component.isValidMessageServices).toBeFalsy();
        // Verificamos que llame nuestra funcion clearDatosConsolidado()
        expect(spyclearDatosConsolidado).toHaveBeenCalled();
        // Verificamos que llame nuestra funcion cleanImagen()
        expect(spyclearImagen).toHaveBeenCalled();
        // Verificamos que llame nuestra funcion cleanFirma()
        expect(spycleanFirma).toHaveBeenCalled();
    });

    it('Verificamos nuestra funcion limpiarTodo()', () => {
        // Espiamos nuestras funciones desHabilitarBotones
        const spydesHabilitarBotones = spyOn(component, 'desHabilitarBotones').and.callThrough();
        // Espiamos nuestra funcion clearDatosConsolidado
        const spyclearDatosConsolidado = spyOn(component, 'clearDatosConsolidado').and.callThrough();
        // Espiamos nuestra funcion clearDatosConsolidado
        const spycleanImagen = spyOn(component, 'cleanImagen').and.callThrough();
        // Espiamos nuestra funcion cleanFirma
        const spycleanFirma = spyOn(component, 'cleanFirma').and.callThrough();
        // Espiamos nuestra funcion cleanHuella
        const spycleanHuella = spyOn(component, 'cleanHuella').and.callThrough();
        // Esiamos nuestro servicio
        const spyServicio = spyOn(servicio, 'serviceEndCapturaDactilar').and.callThrough();
        // Llamamos nuestra funcion
        component.limpiarTodo();
        // Verificamos que dataResponseMejorHuella no exista y por ello sea NULL
        expect(localStorage.getItem('dataResponseMejorHuella')).toBeNull();
        // Verificamos la asignacion de valores de nuestras variables
        expect(component.codigoRespuestaServicio).toEqual('');
        expect(component.mensageRespuestaServicio).toEqual('');
        expect(component.messageValidatorByDniValidator).toEqual('');
        expect(component.isValidMessageServices).toBeFalsy();
        expect(component.isDniConsultorValid).toBeFalsy();
        // Verificamos que llamen nuestros espias de las funciones
        expect(spydesHabilitarBotones).toHaveBeenCalled();
        expect(spyclearDatosConsolidado).toHaveBeenCalled();
        expect(spycleanImagen).toHaveBeenCalled();
        expect(spycleanFirma).toHaveBeenCalled();
        expect(spycleanHuella).toHaveBeenCalled();
        // Verificamos que llame nuestro servicio serviceEndCapturaDactilar
        expect(spyServicio).toHaveBeenCalled();

    });


    it('Verificamos la funcion limpiarDatos()', () => {
        // Espiamos nuestra funcion clearDatosConsolidado
        const spyclearDatosConsolidado = spyOn(component, 'clearDatosConsolidado').and.callThrough();
        // Llamamos nuestra funcion limpiarDatos();
        component.limpiarDatos();
        // Verificamos los valores que asigna nuestra funcion();
        expect(component.codigoRespuestaServicio).toEqual('');
        expect(component.mensageRespuestaServicio).toEqual('');
        expect(component.messageValidatorByDniValidator).toEqual('');
        expect(component.isValidMessageServices).toBeFalsy();
        expect(component.isDniConsultorValid).toBeFalsy();
        // Verificamos que llame nuestra funcion
        expect(spyclearDatosConsolidado).toHaveBeenCalled();
    });





});
