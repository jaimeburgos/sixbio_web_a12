import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { VerificacionRoutingModule } from './verificacion-routing.module';
import { VerificacionComponent } from './verificacion.component';
// tslint:disable-next-line:max-line-length
import { FotoComponent,  DatosComponent, MejorHuellaComponent, CapturarComponent, VerificarComponent, VerificarmocComponent} from './components';
import { BsComponentModule } from '../bs-component/bs-component.module';
import { MyDirectiveModule } from '../../my-directive/my-directive.module';

@NgModule({
  declarations: [
    VerificacionComponent,
    FotoComponent,
    DatosComponent,
    MejorHuellaComponent,
    CapturarComponent,
    VerificarComponent,
    VerificarmocComponent

  ],
  imports: [
    CommonModule,
    VerificacionRoutingModule,
    FormsModule,
    BsComponentModule,
    MyDirectiveModule,
    ReactiveFormsModule
  ]
})
export class VerificacionModule { }
