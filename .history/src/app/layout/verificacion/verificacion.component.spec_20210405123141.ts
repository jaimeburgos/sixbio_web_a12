import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Variable } from '@angular/compiler/src/render3/r3_ast';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import * as FileSaver from 'file-saver';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { async, of, Subject, throwError } from 'rxjs';
import { EnvService } from 'src/app/env.service';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { ToastComponent } from '../bs-component/components/toast/toast.component';
import { VerificacionComponent } from './verificacion.component';

class FakeRouter {
  navigate(params) {

  }
}

class FakeActivatedRoute {
  // params: Observable<any> = Observable.empty();
  private subject = new Subject();

  push( valor ) {
    this.subject.next( valor );
  }
  get params() {
    return this.subject.asObservable();
  }
}

fdescribe('VerificacionComponent', () => {
  let component: VerificacionComponent;
  let fixture: ComponentFixture<VerificacionComponent>;
  // Instanciamos los servicios que utilizaremos
  let translateService: TranslateService;
  let servicio: ServicioService;
  beforeEach((() => {
    TestBed.configureTestingModule({
      declarations: [ VerificacionComponent, ToastComponent ],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService ,
        {provide: Router , useClass: FakeRouter},
        {provide: ToastrService, useClass: ToastrService},
        { provide: ActivatedRoute, useClass: FakeActivatedRoute }
      ],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificacionComponent);
    translateService = TestBed.inject(TranslateService);
    // Injectamos nuestro servicio con TestBed.inject
    servicio = TestBed.inject(ServicioService);
    component = fixture.componentInstance;
    fixture.detectChanges();
    // Mockeamos valor en localstorage de datosSesion
    const mockDatosSesion = {
        '_ningunaHuella': '0',
        '_huellaVivaAmarillo': '0',
        '_noHuellaViva': '0',
        '_nistRojo': '0',
        '_umbralHuellaViva': '50.0',
        '_nistAmarillo': '0',
        '_huellaVivaRojo': '0',
        '_validarSixser': false,
        '_serviciosVerificacion': {
            'default': '301001',
            'tipos': [
                {
                    'descripcion': 'T33-ANSI',
                    'codigo': '301001'
                },
                {
                    'descripcion': 'T33-ANSI',
                    'codigo': '301001'
                },
                {
                    'descripcion': 'T35-WSQ',
                    'codigo': '301100'
                }
            ],
            'tiposServicioVerificacion': [
                {
                    '_descripcion': 'T33-ANSI',
                    '_codigo': '301001'
                },
                {
                    '_descripcion': 'T33-ANSI',
                    '_codigo': '301001'
                },
                {
                    '_descripcion': 'T35-WSQ',
                    '_codigo': '301100'
                }
            ]
        },
        '_acciones': [
            {
                '_descripcion': 'Mejor Huella',
                '_codigo': 'sixbio'
            },
            {
                '_descripcion': 'Capturar',
                '_codigo': 'sixbio'
            },
            {
                '_descripcion': 'Verificar',
                '_codigo': 'sixbio'
            },
            {
                '_descripcion': 'VerificarMOCard',
                '_codigo': 'sixbio'
            },
            {
                '_descripcion': 'Foto',
                '_codigo': 'sixser'
            },
            {
                '_descripcion': 'Datos',
                '_codigo': 'sixser'
            }
        ],
        '_idSesion': '000000202104051226028320041',
        '_validarHuellaViva': true,
        '_validarDniValidador': true,
        '_visualizar': 'true',
        '_sixbioVersion': '1.6',
        '_usuario': 'jburgos5',
        '_noHuellaCapturada': '0',
        '_validarIpCliente': true,
        '_rolesSCA': [
            {
                '_permisos': [],
                '_rol': 'EMP_VER'
            },
            {
                '_permisos': [],
                '_rol': 'VAL'
            }
        ],
        '_codigoRespuesta': '00000',
        '_mensajeRespuesta': 'OPERACION REALIZADA SATISFACTORIAMENTE'
    };

    localStorage.setItem('datosSesion', JSON.stringify(mockDatosSesion));

  });

  it('VerificacionComponent creado correctamente', () => {
    expect(component).toBeTruthy();
  });











});
