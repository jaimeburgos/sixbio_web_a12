import { Component, OnInit, Output, Input, EventEmitter, ViewChild } from '@angular/core';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { IErrorValidador } from 'src/app/nucleo/interface/IErrorValidador';
import { Constant } from '../../../../nucleo/constante/Constant';
import { ButtonDirective } from 'src/app/my-directive/directive/button/button.directive';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { IRespuestaServicio } from 'src/app/nucleo/interface/IRespuestaServicio';
import { Router } from '@angular/router';


@Component({
    selector: 'app-mejor-huella',
    templateUrl: './mejor-huella.component.html',
    styleUrls: ['./mejor-huella.component.scss']
})
export class MejorHuellaComponent implements OnInit {
    constructor(private servicioService: ServicioService,
        private router: Router) { }

    @ViewChild(ButtonDirective, { static: true })
    buttonDirective = null;

    @ViewChild(ToastComponent, { static: true })
    toast: ToastComponent;

    @Input()
    dniConsultor;

    @Input()
    dniValidador;
    @Output()
    propagarRespuestaServicio = new EventEmitter<IRespuestaServicio>();
    @Output()
    executeFuctionCleanDatos = new EventEmitter();

    @Output()
    executeFuctionLimpiarTodo = new EventEmitter();

    @Output()
    executeFuctionCleanImagen = new EventEmitter();

    @Output()
    executeFuctionCleanHuella = new EventEmitter();

    @Output()
    propagarMensajeErrorInput = new EventEmitter<IErrorValidador>();

    mensajeErrorInput: IErrorValidador = {
        banderaError: false,
        mensajeError: '',
        tipoInput: ''
    };

    @Output()
    propagarMensajeRespuestaServicio = new EventEmitter<IRespuestaServicio>();

    mensajeRespuestaServicio: IRespuestaServicio = {
        codigoRespuesta: '',
        mensajeRespuesta: ''
    };

    @Output()
    propagarTituloDeTipoCapturaHuella = new EventEmitter<any>();

    tituloDeTipoCapturaHuella: any = {
        descripcionTipoHuellaDerecha: '',
        descripcionTipoHuellaIzquierda: ''
    };

    dataRequest: any;

    dataResponse: any;
    seHabilita = 'true';
    habilitacion = 1;
    respuestaServicio: IRespuestaServicio = {
        codigoRespuesta: '',
        mensajeRespuesta: ''
    };
    ngOnInit() {
    }

    obtenerMejorHuella() {
        this.habilitacion = 0;
        this.setSendRespuesta(null, 'cleanFront');
        this.executeFuctionLimpiarTodo.emit();
        this.executeFuctionCleanDatos.emit();
        this.executeFuctionCleanImagen.emit();
        this.executeFuctionCleanHuella.emit();

        if (this.isValidInputs()) {
            this.emitirMensajeRespuestaServicio(Constant.CODIGO_VACIO, Constant.MENSAJE_VACIO);
            this.emitirValidacionInput(false, '', '');
            this.emitirMensajeRespuestaServicio('', Constant.MENSAJE_ESPERA_OBTENER_MEJOR_HUELLA);

            this.dataRequest = this.setDataRequest();

            setTimeout(() => {
                this.executeServiceConsultaMejorHuella(this.dataRequest);
            }, 100);

        } else {
            this.emitirMensajeRespuestaServicio(Constant.CODIGO_VACIO, Constant.MENSAJE_VACIO);
            this.buttonDirective.enableButton();
            return;
        }

    }

    executeServiceConsultaMejorHuella(dataRequest: any) {

        this.servicioService.serviceConsultaMejorHuella(dataRequest).subscribe(
            (result: any) => {
                if (result.codigoRespuesta === Constant.COD_ERROR_SESION) {
                    localStorage.clear();
                    this.salir();
                        return;
                        // redirect inicio
                }
                this.endConsultaMejorHuella(result);
                this.buttonDirective.enableButton();
            }, error => {
                this.emitirMensajeRespuestaServicio('', Constant.MENSAJE_VACIO);
                this.buttonDirective.enableButton();
                this.toast.addToast({ title: 'Error', msg: Constant.MENSAJE_ERROR_CONECTIVIDAD, type: 'error' });
                setTimeout(() => {this.salir(); }, 500);
            }
        );

    }

    endConsultaMejorHuella(result: any) {


        if (result.codigoRespuesta === Constant.COD_OPERACION_SATISFACTORIA_SERVICE_CONSULT_MEJOR_HUELLA) {

            if (result.codigoHuellaDerecha === '0101' || result.codigoHuellaDerecha === '0102' || result.codigoHuellaDerecha === '0103' || result.codigoHuellaDerecha === '0104' || result.codigoHuellaDerecha === '0105') {

                this.emitirMensajeRespuestaServicio('', 'CONSULTA DE MEJOR HUELLA REALIZADA : ' + this.encodeMensaje(result.descripcionHuellaDerecha) + ', ' + this.encodeMensaje(result.descripcionHuellaIzquierda));
                this.setDatosMejorHuella(this.dniConsultor, result.codigoHuellaDerecha, result.descripcionHuellaDerecha, result.codigoHuellaIzquierda, result.descripcionHuellaIzquierda);
                this.modificarTituloDeTipoCapturaHuella(this.dataResponse.descripcionHuellaDerecha, this.dataResponse.descripcionHuellaIzquierda);
                this.habilitacion = 1;
            } else {
                this.emitirMensajeRespuestaServicio('', 'CONSULTA DE MEJOR HUELLA REALIZADA : No encontrado');
                this.habilitacion = 1;
            }

            // setValorHTML(IDHTML.DATOS.CODIGORESPUESTA, result.codigoRespuesta);
            this.buttonDirective.enableButton();

        } else if (result.codigoRespuesta === '15000' || result.codigoRespuesta === '20001') {
            this.emitirMensajeRespuestaServicio('', this.encodeMensaje(result.mensajeRespuesta));
        } else {
            this.emitirMensajeRespuestaServicio('', '');
            this.toast.addToast({ title: 'Error', msg: 'Error de conectividad', timeOut: 5000, type: 'error' });
            setTimeout(() => {this.salir(); }, 500);
        }

    }

    modificarTituloDeTipoCapturaHuella(descripcionHuellaDerecha: any, descripcionHuellaIzquierda: any) {
        this.setTituloDeTipoCapturaHuella(descripcionHuellaDerecha, descripcionHuellaIzquierda);
        this.propagarTituloDeTipoCapturaHuella.emit(this.tituloDeTipoCapturaHuella);
    }

    setTituloDeTipoCapturaHuella(descripcionHuellaDerecha: any, descripcionHuellaIzquierda: any) {
        this.tituloDeTipoCapturaHuella.descripcionTipoHuellaDerecha = descripcionHuellaDerecha;
        this.tituloDeTipoCapturaHuella.descripcionTipoHuellaIzquierda = descripcionHuellaIzquierda;
    }

    // eslint-disable-next-line max-len
    setDatosMejorHuella(dni: any, codigoHuellaDerecha: any, descripcionHuellaDerecha: any, codigoHuellaIzquierda: any, descripcionHuellaIzquierda: any) {
        this.dataResponse = {
            dni: dni,
            codigoHuellaDerecha: codigoHuellaDerecha,
            descripcionHuellaDerecha: descripcionHuellaDerecha,
            codigoHuellaIzquierda: codigoHuellaIzquierda,
            descripcionHuellaIzquierda: descripcionHuellaIzquierda
        };
        localStorage.setItem('dataResponseMejorHuella', JSON.stringify(this.dataResponse));
    }

    encodeMensaje(mensajeString: String) {
        let mensajeOut = mensajeString;
        mensajeOut = mensajeOut.replace('Menique', 'Meñique');
        mensajeOut = mensajeOut.replace(new RegExp('Ã¡', 'g'), 'á');
        mensajeOut = mensajeOut.replace(new RegExp('Ã©', 'g'), 'é');
        mensajeOut = mensajeOut.replace(new RegExp('Ã³', 'g'), 'ó');
        mensajeOut = mensajeOut.replace(new RegExp('Ãº', 'g'), 'ú');
        mensajeOut = mensajeOut.replace(new RegExp('Ã', 'g'), 'í');
        return mensajeOut;
    }

    setDataRequest() {
        if (this.requiereDniValidador()) {
            return {
                'numeroDocumentoPersona': this.dniConsultor,
                /* 'numeroIdentificadorCliente': this.dniValidador, */
            };
        } else {
            return {
                'numeroDocumentoPersona': this.dniConsultor,
            };
        }
    }

    isValidInputs(): boolean {

        // Validacion del input dniValidador
        if (this.requiereDniValidador()) {
            if (this.isEmpty(this.dniValidador)) {
                this.emitirValidacionInput(true, Constant.MENSAJE_ERROR_DNI_VALIDADOR_VACIO, 'dniValidador');
                return false;
            } else if (this.isInvalidRegexInputDni(this.dniValidador)) {
                this.emitirValidacionInput(true, Constant.MENSAJE_ERROR_DNI_VALIDADOR_INVALIDO, 'dniValidador');
                return false;
            }
        }

        // Validacion del input dniConsultor
        if (this.isEmpty(this.dniConsultor)) {
            this.emitirValidacionInput(true, Constant.MENSAJE_ERROR_DNI_CONSULTOR_VACIO, 'dniConsultor');
            return false;
        } else if (this.isInvalidRegexInputDni(this.dniConsultor)) {
            this.emitirValidacionInput(true, Constant.MENSAJE_ERROR_DNI_CONSULTOR_INVALIDO, 'dniConsultor');
            return false;
        }

        return true;

    }

    emitirValidacionInput(bandera: boolean, mensaje: string, tipoInput: string) {
        this.setMensajeErrorInput(bandera, mensaje, tipoInput);
        this.propagarMensajeErrorInput.emit(this.mensajeErrorInput);
    }

    setMensajeErrorInput(bandera: boolean, msgError: String, tipoInput: String) {
        this.mensajeErrorInput.banderaError = bandera;
        this.mensajeErrorInput.mensajeError = msgError;
        this.mensajeErrorInput.tipoInput = tipoInput;
    }

    emitirMensajeRespuestaServicio(codigo: String, mensaje: String) {
        this.setMensajeRespuestaServicio(codigo, mensaje);
        this.propagarMensajeRespuestaServicio.emit(this.mensajeRespuestaServicio);
    }

    setMensajeRespuestaServicio(codigo: String, mensaje: String) {
        this.mensajeRespuestaServicio.codigoRespuesta = codigo;
        this.mensajeRespuestaServicio.mensajeRespuesta = mensaje;
    }

    isEmpty(param: any): boolean {
        const dni: string = param;
        if (dni.length === 0) {
            return true;
        }
        return false;
    }

    isInvalidRegexInputDni(param: any): boolean {
        const dni: string = param;
        const regexpNumber = new RegExp('^[+0-9]{8}$');
        if (!regexpNumber.test(dni)) {
            return true;
        }
        return false;
    }

    requiereDniValidador(): boolean {
        if (localStorage.getItem('isWithDniValidator') !== null && localStorage.getItem('isWithDniValidator') === 'true') {
            return true;
        } else {
            return false;
        }
    }
    habilitarBoton() {
        if (this.habilitacion = 1) {
            return true;

        } else {
            return false;
        }
    }
    setSendRespuesta(codigoR, mensajeR) {
        this.setRespuestaServicio(codigoR, mensajeR);
        this.propagarRespuestaServicio.emit(this.respuestaServicio);
}
    setRespuestaServicio(codigo: string, mensaje: string) {
        this.respuestaServicio.codigoRespuesta = '';
        this.respuestaServicio.mensajeRespuesta = mensaje;
    }

    salir() {
        this.servicioService.serviceSignOff(this.dataRequest).subscribe((dataResponse: any) => {

            if (dataResponse.codigoRespuesta === Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO) {
                localStorage.clear();
                this.router.navigate(['/login']);
            } else {

                localStorage.clear();
                this.router.navigate(['/login']);

            }
        }
            , error => {
                localStorage.clear();
                this.router.navigate(['/login']);
            });
    }
}
