import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { of, Subject, throwError } from 'rxjs';
import { EnvService } from 'src/app/env.service';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { HuellaService } from '../../serviciosDeUI/huella.service';
import { DatosComponent } from './datos.component';

fdescribe('DatosComponent', () => {
  let component: DatosComponent;
  let fixture: ComponentFixture<DatosComponent>;
  // Instanciamos los servicios que utilizaremos
  let translateService: TranslateService;
  let servicio: ServicioService;
  let huellaService: HuellaService;
  class FakeRouter {
    navigate(params) {

    }
  }

  class FakeActivatedRoute {
    // params: Observable<any> = Observable.empty();
    private subject = new Subject();

    push(valor) {
      this.subject.next(valor);
    }
    get params() {
      return this.subject.asObservable();
    }
  }
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DatosComponent, ToastComponent],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService,
        HuellaService,
        { provide: Router, useClass: FakeRouter },
        { provide: ToastrService, useClass: ToastrService },
        { provide: ActivatedRoute, useClass: FakeActivatedRoute }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosComponent);
    // Injectamos nuestro servicio con TestBed.inject
    servicio = TestBed.inject(ServicioService);
    // Injectamos nuestro servicio de huella con TestBed.inject
    huellaService = TestBed.inject(HuellaService);
    component = fixture.componentInstance;
     // Mockeamos valor en localstorage de datosSesion
  /*   const mockDatosSesion = {
      '_ningunaHuella': '0',
      '_huellaVivaAmarillo': '0',
      '_noHuellaViva': '0',
      '_nistRojo': '0',
      '_umbralHuellaViva': '50.0',
      '_nistAmarillo': '0',
      '_huellaVivaRojo': '0',
      '_validarSixser': false,
      '_serviciosVerificacion': {
          'default': '301001',
          'tipos': [
              {
                  'descripcion': 'T33-ANSI',
                  'codigo': '301001'
              },
              {
                  'descripcion': 'T33-ANSI',
                  'codigo': '301001'
              },
              {
                  'descripcion': 'T35-WSQ',
                  'codigo': '301100'
              }
          ],
          'tiposServicioVerificacion': [
              {
                  '_descripcion': 'T33-ANSI',
                  '_codigo': '301001'
              },
              {
                  '_descripcion': 'T33-ANSI',
                  '_codigo': '301001'
              },
              {
                  '_descripcion': 'T35-WSQ',
                  '_codigo': '301100'
              }
          ]
      },
      '_acciones': [
          {
              '_descripcion': 'Mejor Huella',
              '_codigo': 'sixbio'
          },
          {
              '_descripcion': 'Capturar',
              '_codigo': 'sixbio'
          },
          {
              '_descripcion': 'Verificar',
              '_codigo': 'sixbio'
          },
          {
              '_descripcion': 'VerificarMOCard',
              '_codigo': 'sixbio'
          },
          {
              '_descripcion': 'Foto',
              '_codigo': 'sixser'
          },
          {
              '_descripcion': 'Datos',
              '_codigo': 'sixser'
          }
      ],
      '_idSesion': '000000202104051226028320041',
      '_validarHuellaViva': true,
      '_validarDniValidador': true,
      '_visualizar': 'true',
      '_sixbioVersion': '1.6',
      '_usuario': 'jburgos5',
      '_noHuellaCapturada': '0',
      '_validarIpCliente': true,
      '_rolesSCA': [
          {
              '_permisos': [],
              '_rol': 'EMP_VER'
          },
          {
              '_permisos': [],
              '_rol': 'VAL'
          }
      ],
      '_codigoRespuesta': '00000',
      '_mensajeRespuesta': 'OPERACION REALIZADA SATISFACTORIAMENTE'
  };*/
    fixture.detectChanges();
    // Habilitamos en jasmine el re espiar las funciones , caso contrario tendriamos un error
    jasmine.getEnv().allowRespy(true);
  //  localStorage.setItem('datosSesion', JSON.stringify(mockDatosSesion));
  });

  it('DatosComponent creado correctamente', () => {
    expect(component).toBeTruthy();
  });

  it('Verificamos la funcion clearAll()', () => {
    // Espiamos nuestra funcion clearDatosConsolidado()
    const spyclearDatosConsolidado = spyOn(component, 'clearDatosConsolidado').and.callThrough();
    // Espiamos nuestra funcion cleanFirma()
    const spycleanFirma = spyOn(component, 'cleanFirma').and.callThrough();
    // Espiamos nuestra funcion cleanImagen()
    const spycleanImagen = spyOn(component, 'cleanImagen').and.callThrough();
    // Llamamos nuestra funcion clearAll()
    component.clearAll();
    // Verificamos que nuestros espias fueran llamados
    expect(spyclearDatosConsolidado).toHaveBeenCalled();
    expect(spycleanFirma).toHaveBeenCalled();
    expect(spycleanImagen).toHaveBeenCalled();
  });

  it('Verificamos la funcion salir()', () => {
    // Espiamos nuestro servicio serviceSignOff
    const spyServicios = spyOn(servicio, 'serviceSignOff').and.callThrough();
    // Llamamos nuestra funcion salir()
    component.salir();
    // Verificamos que fuera llamado nuestro servicio
    expect(spyServicios).toHaveBeenCalled();
    component.dataRequest = {};
    // Ahora verificamos la primera condicion cuando codigoRespuesta sea Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    const mockDataResponse = {
      codigoRespuesta: Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO,
    };
    // Injectamos nuestro router previamente fakeado para espiar la funcion navigate
    const router = TestBed.inject(Router);
    // Espiamos la funcion navigate de router
    const spyRouterFake = spyOn(router, 'navigate').and.callThrough();
    // Fakeamos localStorage clear caso contrario tendremos un error por la dependencia entre datosComponent y verificacionComponent
    spyOn(localStorage, 'clear').and.callFake(() => { 'No hago nada'; });
    // Ahora hacemos que nuestro servicio nos devuelva con un callFake el dataResponse previamente mockeado
    const spyServicioFake = spyOn(servicio, 'serviceSignOff').and.callFake(() => {
      return of(mockDataResponse);
    });
    // Llamamos nuestra funcion salir()
    component.salir();
    // Verificamos que fuera llamado nuestro servicio FAKE
    expect(spyServicioFake).toHaveBeenCalled();
    // Verificamos que fuera llamado navigate de  Router Fake
    expect(spyRouterFake).toHaveBeenCalled();

    // Ahora verificamos para el caso que codigoRespuesta sea diferente de  Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    mockDataResponse.codigoRespuesta = 'Esto es diferente';
    // Llamamos nuestra funcion salir()
    component.salir();
    // Verificamos que fuera llamado nuestro servicio FAKE
    expect(spyServicioFake).toHaveBeenCalled();
    // Verificamos que fuera llamado navigate de  Router Fake
    expect(spyRouterFake).toHaveBeenCalled();

    // Ahora forzamos a nuestro servicio nos retorne un Error
    const spyServiceError = spyOn(servicio, 'serviceSignOff').and.returnValue(throwError('Error serviceSignOff'));
    // Llamamos nuestra funcion salir()
    component.salir();
    // Verificamos que fuera llamado navigate de  Router Fake
    expect(spyRouterFake).toHaveBeenCalled();
    // Verificamos que llame nuestro servicio ERROR
    expect(spyServiceError).toHaveBeenCalled();
  });


  it('Verificamos la funcion clearDatosConsolidado()', () => {
    // Llamamos nuestra funcion
    component.clearDatosConsolidado();
    // Verificamos los valores asignados a nuestras variables
    expect(component.datosConsolidado.apellidos).toEqual('');
    expect(component.datosConsolidado.fechaNacimiento).toEqual('');
    expect(component.datosConsolidado.constanciaVotacionDesc).toEqual('');
    expect(component.datosConsolidado.nombreMadre).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoProvDomicilio).toEqual('');
    expect(component.datosConsolidado.direccion).toEqual('');
    expect(component.datosConsolidado.estatura).toEqual('');
    expect(component.datosConsolidado.constanciaVotacionCodigo).toEqual('');
    expect(component.datosConsolidado.localidadDomicilio).toEqual('');
    expect(component.datosConsolidado.provinciaNacimiento).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoDistNacimiento).toEqual('');
    expect(component.datosConsolidado.nombres).toEqual('');
    expect(component.datosConsolidado.restricciones).toEqual('');
    expect(component.datosConsolidado.caducidadDescripcion).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoDistDomicilio).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoProvNacimiento).toEqual('');
    expect(component.datosConsolidado.nombrePadre).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoLocalidadNacimiento).toEqual('');
    expect(component.datosConsolidado.numeroLibro).toEqual('');
    expect(component.datosConsolidado.localidad).toEqual('');
    expect(component.datosConsolidado.distritoNacimiento).toEqual('');
    expect(component.datosConsolidado.ubigeoVotacion).toEqual('');
    expect(component.datosConsolidado.dni).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoLocalidadDomicilio).toEqual('');
    expect(component.datosConsolidado.departamentoDomicilio).toEqual('');
    expect(component.datosConsolidado.grupoVotacion).toEqual('');
    expect(component.datosConsolidado.provinciaDomicilio).toEqual('');
    expect(component.datosConsolidado.restriccionesDesc).toEqual('');
    expect(component.datosConsolidado.caducidadCodigo).toEqual('');
    expect(component.datosConsolidado.anioEstudio).toEqual('');
    expect(component.datosConsolidado.departamentoNacimiento).toEqual('');
    expect(component.datosConsolidado.numeroDocSustentarioIdentidad).toEqual('');
    expect(component.datosConsolidado.estadoCivilDescripcion).toEqual('');
    expect(component.datosConsolidado.fechaInscripcion).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoDeptoDomicilio).toEqual('');
    expect(component.datosConsolidado.estadoCivilCodigo).toEqual('');
    expect(component.datosConsolidado.fechaExpedicion).toEqual('');
    expect(component.datosConsolidado.tipoDocSustentarioIdentidad).toEqual('');
    expect(component.datosConsolidado.sexoCodigo).toEqual('');
    expect(component.datosConsolidado.sexoDescripcion).toEqual('');
    expect(component.datosConsolidado.foto).toEqual('assets/images/perfil.png');
    expect(component.datosConsolidado.firma).toEqual('assets/images/firma.jpg');
  });

  it('Verificamos la funcion habilitarBotonSi()', () => {
    // Llamamos nuestra funcion
    component.habilitarBotonSi();
    // Verificamos que se setee el valor esperado en localStorage
    expect(localStorage.getItem('botonDatos')).toEqual('true');
  });

  it('Verificamos la funcion desHabilitarBoton()', () => {
    // Llamamos nuestra funcion
    component.desHabilitarBoton();
    // Verificamos que se setee el valor esperado en localStorage
    expect(localStorage.getItem('botonDatos')).toEqual('false');
  });

  it('Verificamos nuestra funcion formatearFecha', () => {
    // Mockeamos un valor para pasarle a la funcion
    const mockFecha = '01 02 2000';
    // Llamamos nuestra funcion
    // Verificamos que que nos retorna la funcion
    expect(component.formatearFecha(mockFecha)).toEqual('01-02-2000');
  });

  it('Verificamos la funcion habilitarBoton()', () => {
    // Seteamos el valor al item botonDatos de 'true'
    localStorage.setItem('botonDatos', 'true');
    // Verificamos que que nos retorne true nuestra funcion
    expect(component.habilitarBoton()).toBeTruthy();
    // Ahora para el caso de contrario que sea diferente de true
    localStorage.setItem('botonDatos', 'Cualquier cosa diferente de true');
    // Verificamos que que nos retorne false nuestra funcion
    expect(component.habilitarBoton()).toBeFalsy();
  });


  it('Verificamos la funcion setDataRequestDatos(dniConsultor:string)', () => {
    // Mockeamos un valor el parametro de entrada de la funcion
    const mockDniConsultor = '12345678';
    const mockTest = {
      numeroDocumentoPersona : mockDniConsultor
    };
    // verificamos que nos retorne el objeto deseado
    expect(component.setDataRequestDatos(mockDniConsultor)).toEqual(mockTest);
  });


  it('Verificamos la funcion setErrorValidador(bandera: boolean, msgError: string, tipoInput: string)', () => {
    // Mockeamos valores para los parametros de entrada de la funcion
    const mockBandera = true;
    const mockMsgError = 'Error_Test';
    const mockTipoInput = 'Type_Test';
    // Le pasamos los valores a nuestra funcion
    component.setErrorValidador(mockBandera, mockMsgError, mockTipoInput);
    // Verificamos la asignacion a nuestras variables
    expect(component.errorValidador.banderaError).toBe(mockBandera);
    expect(component.errorValidador.mensajeError).toEqual(mockMsgError);
    expect(component.errorValidador.tipoInput).toEqual(mockTipoInput);
  });

  it('Verificamos nuestra funcion setRespuestaServicio()', () => {
    // Mockeamos los valores entrada de la funcion
    const mockCodigo = 'Test_code';
    const mockMensaje = 'Test_mensaje';
    // Llamamos nuestra funcion
    component.setRespuestaServicio(mockCodigo, mockMensaje);
    // Verificamos la asignacion a nuestras variables
    expect(component.respuestaServicio.codigoRespuesta).toEqual(mockCodigo);
    expect(component.respuestaServicio.mensajeRespuesta).toEqual(mockMensaje);
  });

  it('Verificamos la funcion setDatosResponseWithObjtDatosConsolidado(response:any)', () => {
    // Mockeamos el valor de entrada
    const mockResponse = {
      datoPersona : {
        apellidoPaterno: 'Test_apellidoPaterno',
        apellidoMaterno : 'Test_apellidoMaterno',
        fechaNacimiento : '01 02 2000',
        constanciaVotacionDesc : 'Test_constanciaVotacionDesc',
        nombreMadre : 'Test_nombreMadre',
        codigoUbigeoProvDomicilio : 'Test_codigoUbigeoProvDomicilio',
        direccionDomicilio : 'Test_direccionDomicilio',
        distritoDomicilio : 'Test_distritoDomicilio',
        estatura : 'Test_estatura',
        constanciaVotacionCodigo : 'Test_constanciaVotacionCodigo',
        localidadDomicilio : 'Test_localidadDomicilio',
        provinciaNacimiento : 'Test_provinciaNacimiento',
        codigoUbigeoDistNacimiento : 'Test_codigoUbigeoDistNacimiento',
        nombres : 'Test_nombres',
        restricciones : 'Porque sigo programando esto :"v',
        caducidadDescripcion : 'Test_caducidadDescripcion',
        codigoUbigeoDistDomicilio : 'Test_codigoUbigeoDistDomicilio',
        codigoUbigeoProvNacimiento : 'Test_codigoUbigeoProvNacimiento',
        nombrePadre : 'Test_nombrePadre',
        codigoUbigeoLocalidadNacimiento : 'Test_codigoUbigeoLocalidadNacimiento',
        numeroLibro : 'Test_numeroLibro',
        localidad: 'Test_localidad',
        distritoNacimiento : 'Test_distritoNacimiento',
        ubigeoVotacion: 'test_ubigeoVotacion',
        dni: '12345678',
        codigoUbigeoLocalidadDomicilio : 'test_codigoUbigeoLocalidadDomicilio',
        departamentoDomicilio : 'Test_departamentoDomicilio',
        grupoVotacion : 'Test_grupoVotacion',
        provinciaDomicilio : 'Test_provinciaDomicilio',
        restriccionesDesc : 'Test_restriccionesDesc',
        caducidadCodigo : 'Test_caducidadCodigo',
        anioEstudio : 'Test_anioEstudio',
        departamentoNacimiento : 'Test_departamentoNacimiento',
        numeroDocSustentarioIdentidad : 'TESTnumeroDocSustentarioIdentidad',
        estadoCivilDescripcion : 'TESTestadoCivilDescripcion',
        fechaInscripcion : '02 02 6666',
        codigoUbigeoDeptoDomicilio : 'tEST _codigoUbigeoDeptoDomicilio',
        estadoCivilCodigo: 'Test_estadoCivilCodigo',
        fechaExpedicion : '06 06 6666',
        tipoDocSustentarioIdentidad : 'Test_tipoDocSustentarioIdentidad',
        sexoCodigo : 'Test_sexoCodigo',
        codigoUbigeoDeptoNacimiento : 'Test_codigoUbigeoDeptoNacimiento',
        sexoDescripcion : 'Test_sexoDescripcion',
      },
      foto : 'Test_foto',
      firma : 'Test_firma',
    };
    // Le pasamos  los valores mockeados a nuestra funcion
    component.setDatosResponseWithObjtDatosConsolidado(mockResponse);
    // Verificamos la asignacion de valores a nuestro objeto datosConsolidado
    const mockDatoConsolidadoApellidos = mockResponse.datoPersona.apellidoPaterno + ' ' + mockResponse.datoPersona.apellidoMaterno;
    expect(component.datosConsolidado.apellidos).toEqual(mockDatoConsolidadoApellidos);



  });

/*    setDatosResponseWithObjtDatosConsolidado(response: any): void {
        this.datosConsolidado.apellidos = response.datoPersona.apellidoPaterno + ' ' + response.datoPersona.apellidoMaterno;
        this.datosConsolidado.fechaNacimiento = this.formatearFecha(response.datoPersona.fechaNacimiento);
        this.datosConsolidado.constanciaVotacionDesc = response.datoPersona.constanciaVotacionDesc;
        this.datosConsolidado.nombreMadre = response.datoPersona.nombreMadre;
        this.datosConsolidado.codigoUbigeoProvDomicilio = response.datoPersona.codigoUbigeoProvDomicilio;
        this.datosConsolidado.direccion = response.datoPersona.direccionDomicilio + ' / ' + response.datoPersona.distritoDomicilio;
        this.datosConsolidado.estatura = response.datoPersona.estatura;
        this.datosConsolidado.constanciaVotacionCodigo = response.datoPersona.constanciaVotacionCodigo;
        this.datosConsolidado.localidadDomicilio = response.datoPersona.localidadDomicilio;
        this.datosConsolidado.provinciaNacimiento = response.datoPersona.provinciaNacimiento;
        this.datosConsolidado.codigoUbigeoDistNacimiento = response.datoPersona.codigoUbigeoDistNacimiento;
        this.datosConsolidado.nombres = response.datoPersona.nombres;
        this.datosConsolidado.restricciones = response.datoPersona.restricciones;
        this.datosConsolidado.caducidadDescripcion = response.datoPersona.caducidadDescripcion;
        this.datosConsolidado.codigoUbigeoDistDomicilio = response.datoPersona.codigoUbigeoDistDomicilio;
        this.datosConsolidado.codigoUbigeoProvNacimiento = response.datoPersona.codigoUbigeoProvNacimiento;
        this.datosConsolidado.nombrePadre = response.datoPersona.nombrePadre;
        this.datosConsolidado.codigoUbigeoLocalidadNacimiento = response.datoPersona.codigoUbigeoLocalidadNacimiento;
        this.datosConsolidado.numeroLibro = response.datoPersona.numeroLibro;
        this.datosConsolidado.localidad = response.datoPersona.localidad;
        this.datosConsolidado.distritoNacimiento = response.datoPersona.distritoNacimiento;
        this.datosConsolidado.ubigeoVotacion = response.datoPersona.ubigeoVotacion;
        this.datosConsolidado.dni = response.datoPersona.dni;
        this.datosConsolidado.codigoUbigeoLocalidadDomicilio = response.datoPersona.codigoUbigeoLocalidadDomicilio;
        this.datosConsolidado.departamentoDomicilio = response.datoPersona.departamentoDomicilio;
        this.datosConsolidado.grupoVotacion = response.datoPersona.grupoVotacion;
        this.datosConsolidado.provinciaDomicilio = response.datoPersona.provinciaDomicilio;
        this.datosConsolidado.restriccionesDesc = response.datoPersona.restriccionesDesc;
        this.datosConsolidado.caducidadCodigo = response.datoPersona.caducidadCodigo;
        this.datosConsolidado.anioEstudio = response.datoPersona.anioEstudio;
        this.datosConsolidado.departamentoNacimiento = response.datoPersona.departamentoNacimiento;
        this.datosConsolidado.numeroDocSustentarioIdentidad = response.datoPersona.numeroDocSustentarioIdentidad;
        this.datosConsolidado.estadoCivilDescripcion = response.datoPersona.estadoCivilDescripcion;
        this.datosConsolidado.fechaInscripcion = this.formatearFecha(response.datoPersona.fechaInscripcion);
        this.datosConsolidado.codigoUbigeoDeptoDomicilio = response.datoPersona.codigoUbigeoDeptoDomicilio;
        this.datosConsolidado.estadoCivilCodigo = response.datoPersona.estadoCivilCodigo;
        this.datosConsolidado.fechaExpedicion = this.formatearFecha(response.datoPersona.fechaExpedicion);
        this.datosConsolidado.tipoDocSustentarioIdentidad = response.datoPersona.tipoDocSustentarioIdentidad;
        this.datosConsolidado.tipoDocSustentarioIdentidad = response.datoPersona.tipoDocSustentarioIdentidad;
        this.datosConsolidado.sexoCodigo = response.datoPersona.sexoCodigo;
        this.datosConsolidado.codigoUbigeoDeptoNacimiento = response.datoPersona.codigoUbigeoDeptoNacimiento;
        this.datosConsolidado.sexoDescripcion = response.datoPersona.sexoDescripcion;
        this.datosConsolidado.foto = 'data:image/png;base64,' + response.foto;
        this.datosConsolidado.firma = 'data:image/png;base64,' + response.firma;
    }
*/
});
