import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { of, Subject, throwError } from 'rxjs';
import { EnvService } from 'src/app/env.service';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { HuellaService } from '../../serviciosDeUI/huella.service';
import { DatosComponent } from './datos.component';

fdescribe('DatosComponent', () => {
  let component: DatosComponent;
  let fixture: ComponentFixture<DatosComponent>;
  // Instanciamos los servicios que utilizaremos
  let translateService: TranslateService;
  let servicio: ServicioService;
  let huellaService: HuellaService;
  class FakeRouter {
    navigate(params) {

    }
  }

  class FakeActivatedRoute {
    // params: Observable<any> = Observable.empty();
    private subject = new Subject();

    push(valor) {
      this.subject.next(valor);
    }
    get params() {
      return this.subject.asObservable();
    }
  }
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DatosComponent, ToastComponent],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService,
        HuellaService,
        { provide: Router, useClass: FakeRouter },
        { provide: ToastrService, useClass: ToastrService },
        { provide: ActivatedRoute, useClass: FakeActivatedRoute }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosComponent);
    // Injectamos nuestro servicio con TestBed.inject
    servicio = TestBed.inject(ServicioService);
    // Injectamos nuestro servicio de huella con TestBed.inject
    huellaService = TestBed.inject(HuellaService);
    component = fixture.componentInstance;
     // Mockeamos valor en localstorage de datosSesion
  /*   const mockDatosSesion = {
      '_ningunaHuella': '0',
      '_huellaVivaAmarillo': '0',
      '_noHuellaViva': '0',
      '_nistRojo': '0',
      '_umbralHuellaViva': '50.0',
      '_nistAmarillo': '0',
      '_huellaVivaRojo': '0',
      '_validarSixser': false,
      '_serviciosVerificacion': {
          'default': '301001',
          'tipos': [
              {
                  'descripcion': 'T33-ANSI',
                  'codigo': '301001'
              },
              {
                  'descripcion': 'T33-ANSI',
                  'codigo': '301001'
              },
              {
                  'descripcion': 'T35-WSQ',
                  'codigo': '301100'
              }
          ],
          'tiposServicioVerificacion': [
              {
                  '_descripcion': 'T33-ANSI',
                  '_codigo': '301001'
              },
              {
                  '_descripcion': 'T33-ANSI',
                  '_codigo': '301001'
              },
              {
                  '_descripcion': 'T35-WSQ',
                  '_codigo': '301100'
              }
          ]
      },
      '_acciones': [
          {
              '_descripcion': 'Mejor Huella',
              '_codigo': 'sixbio'
          },
          {
              '_descripcion': 'Capturar',
              '_codigo': 'sixbio'
          },
          {
              '_descripcion': 'Verificar',
              '_codigo': 'sixbio'
          },
          {
              '_descripcion': 'VerificarMOCard',
              '_codigo': 'sixbio'
          },
          {
              '_descripcion': 'Foto',
              '_codigo': 'sixser'
          },
          {
              '_descripcion': 'Datos',
              '_codigo': 'sixser'
          }
      ],
      '_idSesion': '000000202104051226028320041',
      '_validarHuellaViva': true,
      '_validarDniValidador': true,
      '_visualizar': 'true',
      '_sixbioVersion': '1.6',
      '_usuario': 'jburgos5',
      '_noHuellaCapturada': '0',
      '_validarIpCliente': true,
      '_rolesSCA': [
          {
              '_permisos': [],
              '_rol': 'EMP_VER'
          },
          {
              '_permisos': [],
              '_rol': 'VAL'
          }
      ],
      '_codigoRespuesta': '00000',
      '_mensajeRespuesta': 'OPERACION REALIZADA SATISFACTORIAMENTE'
  };*/
    fixture.detectChanges();
    // Habilitamos en jasmine el re espiar las funciones , caso contrario tendriamos un error
    jasmine.getEnv().allowRespy(true);
  //  localStorage.setItem('datosSesion', JSON.stringify(mockDatosSesion));
  });

  it('DatosComponent creado correctamente', () => {
    expect(component).toBeTruthy();
  });

  it('Verificamos la funcion clearAll()', () => {
    // Espiamos nuestra funcion clearDatosConsolidado()
    const spyclearDatosConsolidado = spyOn(component, 'clearDatosConsolidado').and.callThrough();
    // Espiamos nuestra funcion cleanFirma()
    const spycleanFirma = spyOn(component, 'cleanFirma').and.callThrough();
    // Espiamos nuestra funcion cleanImagen()
    const spycleanImagen = spyOn(component, 'cleanImagen').and.callThrough();
    // Llamamos nuestra funcion clearAll()
    component.clearAll();
    // Verificamos que nuestros espias fueran llamados
    expect(spyclearDatosConsolidado).toHaveBeenCalled();
    expect(spycleanFirma).toHaveBeenCalled();
    expect(spycleanImagen).toHaveBeenCalled();
  });

  it('Verificamos la funcion salir()', () => {
    // Espiamos nuestro servicio serviceSignOff
    const spyServicios = spyOn(servicio, 'serviceSignOff').and.callThrough();
    // Llamamos nuestra funcion salir()
    component.salir();
    // Verificamos que fuera llamado nuestro servicio
    expect(spyServicios).toHaveBeenCalled();
    component.dataRequest = {};
    // Ahora verificamos la primera condicion cuando codigoRespuesta sea Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    const mockDataResponse = {
      codigoRespuesta: Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO,
    };
    // Injectamos nuestro router previamente fakeado para espiar la funcion navigate
    const router = TestBed.inject(Router);
    // Espiamos la funcion navigate de router
    const spyRouterFake = spyOn(router, 'navigate').and.callThrough();
    // Fakeamos localStorage clear caso contrario tendremos un error por la dependencia entre datosComponent y verificacionComponent
    spyOn(localStorage, 'clear').and.callFake(() => { 'No hago nada'; });
    // Ahora hacemos que nuestro servicio nos devuelva con un callFake el dataResponse previamente mockeado
    const spyServicioFake = spyOn(servicio, 'serviceSignOff').and.callFake(() => {
      return of(mockDataResponse);
    });
    // Llamamos nuestra funcion salir()
    component.salir();
    // Verificamos que fuera llamado nuestro servicio FAKE
    expect(spyServicioFake).toHaveBeenCalled();
    // Verificamos que fuera llamado navigate de  Router Fake
    expect(spyRouterFake).toHaveBeenCalled();

    // Ahora verificamos para el caso que codigoRespuesta sea diferente de  Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    mockDataResponse.codigoRespuesta = 'Esto es diferente';
    // Llamamos nuestra funcion salir()
    component.salir();
    // Verificamos que fuera llamado nuestro servicio FAKE
    expect(spyServicioFake).toHaveBeenCalled();
    // Verificamos que fuera llamado navigate de  Router Fake
    expect(spyRouterFake).toHaveBeenCalled();

    // Ahora forzamos a nuestro servicio nos retorne un Error
    const spyServiceError = spyOn(servicio, 'serviceSignOff').and.returnValue(throwError('Error serviceSignOff'));
    // Llamamos nuestra funcion salir()
    component.salir();
    // Verificamos que fuera llamado navigate de  Router Fake
    expect(spyRouterFake).toHaveBeenCalled();
    // Verificamos que llame nuestro servicio ERROR
    expect(spyServiceError).toHaveBeenCalled();
  });


  it('Verificamos la funcion clearDatosConsolidado()', () => {
    // Llamamos nuestra funcion
    component.clearDatosConsolidado();
    // Verificamos los valores asignados a nuestras variables
    expect(component.datosConsolidado.apellidos).toEqual('');
    expect(component.datosConsolidado.fechaNacimiento).toEqual('');
    expect(component.datosConsolidado.constanciaVotacionDesc).toEqual('');
    expect(component.datosConsolidado.nombreMadre).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoProvDomicilio).toEqual('');
    expect(component.datosConsolidado.direccion).toEqual('');
    expect(component.datosConsolidado.estatura).toEqual('');
    expect(component.datosConsolidado.constanciaVotacionCodigo).toEqual('');
    expect(component.datosConsolidado.localidadDomicilio).toEqual('');
    expect(component.datosConsolidado.provinciaNacimiento).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoDistNacimiento).toEqual('');
    expect(component.datosConsolidado.nombres).toEqual('');
    expect(component.datosConsolidado.restricciones).toEqual('');
    expect(component.datosConsolidado.caducidadDescripcion).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoDistDomicilio).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoProvNacimiento).toEqual('');
    expect(component.datosConsolidado.nombrePadre).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoLocalidadNacimiento).toEqual('');
    expect(component.datosConsolidado.numeroLibro).toEqual('');
    expect(component.datosConsolidado.localidad).toEqual('');
    expect(component.datosConsolidado.distritoNacimiento).toEqual('');
    expect(component.datosConsolidado.ubigeoVotacion).toEqual('');
    expect(component.datosConsolidado.dni).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoLocalidadDomicilio).toEqual('');
    expect(component.datosConsolidado.departamentoDomicilio).toEqual('');
    expect(component.datosConsolidado.grupoVotacion).toEqual('');
    expect(component.datosConsolidado.provinciaDomicilio).toEqual('');
    expect(component.datosConsolidado.restriccionesDesc).toEqual('');
    expect(component.datosConsolidado.caducidadCodigo).toEqual('');
    expect(component.datosConsolidado.anioEstudio).toEqual('');
    expect(component.datosConsolidado.departamentoNacimiento).toEqual('');
    expect(component.datosConsolidado.numeroDocSustentarioIdentidad).toEqual('');
    expect(component.datosConsolidado.estadoCivilDescripcion).toEqual('');
    expect(component.datosConsolidado.fechaInscripcion).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoDeptoDomicilio).toEqual('');
    expect(component.datosConsolidado.estadoCivilCodigo).toEqual('');
    expect(component.datosConsolidado.fechaExpedicion).toEqual('');
    expect(component.datosConsolidado.tipoDocSustentarioIdentidad).toEqual('');
    expect(component.datosConsolidado.sexoCodigo).toEqual('');
    expect(component.datosConsolidado.sexoDescripcion).toEqual('');
    expect(component.datosConsolidado.foto).toEqual('assets/images/perfil.png');
    expect(component.datosConsolidado.firma).toEqual('assets/images/firma.jpg');
  });

  it('Verificamos la funcion habilitarBotonSi()', () => {
    // Llamamos nuestra funcion
    component.habilitarBotonSi();
    // Verificamos que se setee el valor esperado en localStorage
    expect(localStorage.getItem('botonDatos')).toEqual('true');
  });

  it('Verificamos la funcion desHabilitarBoton()', () => {
    // Llamamos nuestra funcion
    component.desHabilitarBoton();
    // Verificamos que se setee el valor esperado en localStorage
    expect(localStorage.getItem('botonDatos')).toEqual('false');
  });

  it('Verificamos nuestra funcion formatearFecha', () => {
    // Mockeamos un valor para pasarle a la funcion
    const mockFecha = '01 02 2000';
    // Llamamos nuestra funcion
    // Verificamos que que nos retorna la funcion
    expect(component.formatearFecha(mockFecha)).toEqual('01-02-2000');
  });

  it('Verificamos la funcion habilitarBoton()', () => {
    // Seteamos el valor al item botonDatos de 'true'
    localStorage.setItem('botonDatos', 'true');
    // Verificamos que que nos retorne true nuestra funcion
    expect(component.habilitarBoton()).toBeTruthy();
    // Ahora para el caso de contrario que sea diferente de true
    localStorage.setItem('botonDatos', 'Cualquier cosa diferente de true');
    // Verificamos que que nos retorne false nuestra funcion
    expect(component.habilitarBoton()).toBeFalsy();
  });


  it('Verificamos la funcion setDataRequestDatos(dniConsultor:string)', () => {
    // Mockeamos un valor el parametro de entrada de la funcion
    const mockDniConsultor = '12345678';
    const mockTest = {
      numeroDocumentoPersona : mockDniConsultor
    };
    // verificamos que nos retorne el objeto deseado
    expect(component.setDataRequestDatos(mockDniConsultor)).toEqual(mockTest);
  });


  it('Verificamos la funcion setErrorValidador(bandera: boolean, msgError: string, tipoInput: string)', () => {
    // Mockeamos valores para los parametros de entrada de la funcion
    const mockBandera = true;
    const mockMsgError = 'Error_Test';
    const mockTipoInput = 'Type_Test';
    // Le pasamos los valores a nuestra funcion
    component.setErrorValidador(mockBandera, mockMsgError, mockTipoInput);
    // Verificamos la asignacion a nuestras variables
    expect(component.errorValidador.banderaError).toBe(mockBandera);
    expect(component.errorValidador.mensajeError).toEqual(mockMsgError);
    expect(component.errorValidador.tipoInput).toEqual(mockTipoInput);
  });

  it('Verificamos nuestra funcion setRespuestaServicio()', () => {
    // Mockeamos los valores entrada de la funcion
    const mockCodigo = 'Test_code';
    const mockMensaje = 'Test_mensaje';
    // Llamamos nuestra funcion
    component.setRespuestaServicio(mockCodigo, mockMensaje);
    // Verificamos la asignacion a nuestras variables
    expect(component.respuestaServicio.codigoRespuesta).toEqual(mockCodigo);
    expect(component.respuestaServicio.mensajeRespuesta).toEqual(mockMensaje);
  });

  it('Verificamos la funcion setDatosResponseWithObjtDatosConsolidado(response:any)', () => {
    // Mockeamos el valor de entrada
    const mockResponse = {
      datoPersona: {
        apellidoPaterno: 'Test_apellidoPaterno',
        apellidoMaterno: 'Test_apellidoMaterno',
        fechaNacimiento: '01 02 2000',
        constanciaVotacionDesc: 'Test_constanciaVotacionDesc',
        nombreMadre: 'Test_nombreMadre',
        codigoUbigeoProvDomicilio: 'Test_codigoUbigeoProvDomicilio',
        direccionDomicilio: 'Test_direccionDomicilio',
        distritoDomicilio: 'Test_distritoDomicilio',
        estatura: 'Test_estatura',
        constanciaVotacionCodigo: 'Test_constanciaVotacionCodigo',
        localidadDomicilio: 'Test_localidadDomicilio',
        provinciaNacimiento: 'Test_provinciaNacimiento',
        codigoUbigeoDistNacimiento: 'Test_codigoUbigeoDistNacimiento',
        nombres: 'Test_nombres',
        restricciones: 'Porque sigo programando esto :"v',
        caducidadDescripcion: 'Test_caducidadDescripcion',
        codigoUbigeoDistDomicilio: 'Test_codigoUbigeoDistDomicilio',
        codigoUbigeoProvNacimiento: 'Test_codigoUbigeoProvNacimiento',
        nombrePadre: 'Test_nombrePadre',
        codigoUbigeoLocalidadNacimiento: 'Test_codigoUbigeoLocalidadNacimiento',
        numeroLibro: 'Test_numeroLibro',
        localidad: 'Test_localidad',
        distritoNacimiento: 'Test_distritoNacimiento',
        ubigeoVotacion: 'test_ubigeoVotacion',
        dni: '12345678',
        codigoUbigeoLocalidadDomicilio: 'test_codigoUbigeoLocalidadDomicilio',
        departamentoDomicilio: 'Test_departamentoDomicilio',
        grupoVotacion: 'Test_grupoVotacion',
        provinciaDomicilio: 'Test_provinciaDomicilio',
        restriccionesDesc: 'Test_restriccionesDesc',
        caducidadCodigo: 'Test_caducidadCodigo',
        anioEstudio: 'Test_anioEstudio',
        departamentoNacimiento: 'Test_departamentoNacimiento',
        numeroDocSustentarioIdentidad: 'TESTnumeroDocSustentarioIdentidad',
        estadoCivilDescripcion: 'TESTestadoCivilDescripcion',
        fechaInscripcion: '02 02 6666',
        codigoUbigeoDeptoDomicilio: 'tEST _codigoUbigeoDeptoDomicilio',
        estadoCivilCodigo: 'Test_estadoCivilCodigo',
        fechaExpedicion: '06 06 6666',
        tipoDocSustentarioIdentidad: 'Test_tipoDocSustentarioIdentidad',
        sexoCodigo: 'Test_sexoCodigo',
        codigoUbigeoDeptoNacimiento: 'Test_codigoUbigeoDeptoNacimiento',
        sexoDescripcion: 'Test_sexoDescripcion',
      },
      foto: 'Test_foto',
      firma: 'Test_firma',
    };
    // Espiamos nuestra funcion formatearFecha()
    const spyformatearFecha = spyOn(component, 'formatearFecha').and.callThrough();
    // Le pasamos  los valores mockeados a nuestra funcion
    component.setDatosResponseWithObjtDatosConsolidado(mockResponse);
    // Verificamos la asignacion de valores a nuestro objeto datosConsolidado
    const mockDatoConsolidadoApellidos = mockResponse.datoPersona.apellidoPaterno + ' ' + mockResponse.datoPersona.apellidoMaterno;
    expect(component.datosConsolidado.apellidos).toEqual(mockDatoConsolidadoApellidos);
    // Verificamos que se llame nuestra funcion spyformatearFecha()
    expect(spyformatearFecha).toHaveBeenCalled();
    // Verificamos el valor asignado a datosConsolidado.fechaNacimiento
    const mockDatosConsolidadoFechaNacimiento = component.formatearFecha(mockResponse.datoPersona.fechaNacimiento);
    expect(component.datosConsolidado.fechaNacimiento).toEqual(mockDatosConsolidadoFechaNacimiento);
    // Verificamos las asignaciones de las otras variables
    expect(component.datosConsolidado.constanciaVotacionDesc).toEqual(mockResponse.datoPersona.constanciaVotacionDesc);
    expect(component.datosConsolidado.nombreMadre).toEqual(mockResponse.datoPersona.nombreMadre);
    expect(component.datosConsolidado.codigoUbigeoProvDomicilio).toEqual(mockResponse.datoPersona.codigoUbigeoProvDomicilio);
    // creamos la variable mockDatosConsolidadoDireccion
    const mockDatosConsolidadoDireccion = mockResponse.datoPersona.direccionDomicilio + ' / ' + mockResponse.datoPersona.distritoDomicilio;
    expect(component.datosConsolidado.direccion).toEqual(mockDatosConsolidadoDireccion);
    // Validamos las asignaciones de las variables
    expect(component.datosConsolidado.estatura).toEqual(mockResponse.datoPersona.estatura);
    expect(component.datosConsolidado.constanciaVotacionCodigo).toEqual(mockResponse.datoPersona.constanciaVotacionCodigo);
    expect(component.datosConsolidado.localidadDomicilio).toEqual(mockResponse.datoPersona.localidadDomicilio);
    expect(component.datosConsolidado.provinciaNacimiento).toEqual(mockResponse.datoPersona.provinciaNacimiento);
    expect(component.datosConsolidado.codigoUbigeoDistNacimiento).toEqual(mockResponse.datoPersona.codigoUbigeoDistNacimiento);
    expect(component.datosConsolidado.nombres).toEqual(mockResponse.datoPersona.nombres);
    expect(component.datosConsolidado.restricciones).toEqual(mockResponse.datoPersona.restricciones);
    expect(component.datosConsolidado.caducidadDescripcion).toEqual(mockResponse.datoPersona.caducidadDescripcion);
    expect(component.datosConsolidado.codigoUbigeoDistDomicilio).toEqual(mockResponse.datoPersona.codigoUbigeoDistDomicilio);
    expect(component.datosConsolidado.codigoUbigeoProvNacimiento).toEqual(mockResponse.datoPersona.codigoUbigeoProvNacimiento);
    expect(component.datosConsolidado.nombrePadre).toEqual(mockResponse.datoPersona.nombrePadre);
    expect(component.datosConsolidado.codigoUbigeoLocalidadNacimiento).toEqual(mockResponse.datoPersona.codigoUbigeoLocalidadNacimiento);
    expect(component.datosConsolidado.numeroLibro).toEqual(mockResponse.datoPersona.numeroLibro);
    expect(component.datosConsolidado.localidad).toEqual(mockResponse.datoPersona.localidad);
    expect(component.datosConsolidado.distritoNacimiento).toEqual(mockResponse.datoPersona.distritoNacimiento);
    expect(component.datosConsolidado.ubigeoVotacion).toEqual(mockResponse.datoPersona.ubigeoVotacion);
    expect(component.datosConsolidado.dni).toEqual(mockResponse.datoPersona.dni);
    expect(component.datosConsolidado.codigoUbigeoLocalidadDomicilio).toEqual(mockResponse.datoPersona.codigoUbigeoLocalidadDomicilio);
    expect(component.datosConsolidado.departamentoDomicilio).toEqual(mockResponse.datoPersona.departamentoDomicilio);
    expect(component.datosConsolidado.grupoVotacion).toEqual(mockResponse.datoPersona.grupoVotacion);
    expect(component.datosConsolidado.provinciaDomicilio).toEqual(mockResponse.datoPersona.provinciaDomicilio);
    expect(component.datosConsolidado.restriccionesDesc).toEqual(mockResponse.datoPersona.restriccionesDesc);
    expect(component.datosConsolidado.caducidadCodigo).toEqual(mockResponse.datoPersona.caducidadCodigo);
    expect(component.datosConsolidado.anioEstudio).toEqual(mockResponse.datoPersona.anioEstudio);
    expect(component.datosConsolidado.departamentoNacimiento).toEqual(mockResponse.datoPersona.departamentoNacimiento);
    expect(component.datosConsolidado.numeroDocSustentarioIdentidad).toEqual(mockResponse.datoPersona.numeroDocSustentarioIdentidad);
    expect(component.datosConsolidado.estadoCivilDescripcion).toEqual(mockResponse.datoPersona.estadoCivilDescripcion);
    // Verificamos el valor asignado a datosConsolidado.fechaInscripcion
    const mockDatosConsolidadofechaInscripcion = component.formatearFecha(mockResponse.datoPersona.fechaInscripcion);
    expect(component.datosConsolidado.fechaInscripcion).toEqual(mockDatosConsolidadofechaInscripcion);
    // Verificamos la asignacion al resto de variables
    expect(component.datosConsolidado.codigoUbigeoDeptoDomicilio).toEqual(mockResponse.datoPersona.codigoUbigeoDeptoDomicilio);
    expect(component.datosConsolidado.estadoCivilCodigo).toEqual(mockResponse.datoPersona.estadoCivilCodigo);
    // Verificamos el valor asignado a datosConsolidado.fechaExpedicion
    const mockDatosConsolidadofechaExpedicion = component.formatearFecha(mockResponse.datoPersona.fechaExpedicion);
    expect(component.datosConsolidado.fechaExpedicion).toEqual(mockDatosConsolidadofechaExpedicion);
    expect(component.datosConsolidado.tipoDocSustentarioIdentidad).toEqual(mockResponse.datoPersona.tipoDocSustentarioIdentidad);
    expect(component.datosConsolidado.sexoCodigo).toEqual(mockResponse.datoPersona.sexoCodigo);
    expect(component.datosConsolidado.codigoUbigeoDeptoNacimiento).toEqual(mockResponse.datoPersona.codigoUbigeoDeptoNacimiento);
    expect(component.datosConsolidado.sexoDescripcion).toEqual(mockResponse.datoPersona.sexoDescripcion);
    // Verificamos las asignaciones para foto y firma
    const mockFirma = 'data:image/png;base64,' + mockResponse.firma;
    const mockFoto = 'data:image/png;base64,' + mockResponse.foto;
    expect(component.datosConsolidado.foto).toEqual(mockFoto);
    expect(component.datosConsolidado.firma).toEqual(mockFirma);
  });


  it('Verificamos nuestra funcion obtenerDatos()', () => {
    // Espiamos nuestra funcion desHabilitarBoton()
    const spydesHabilitarBoton = spyOn(component, 'desHabilitarBoton');
    // Espiamos nuestra funcion setRespuestaServicio()
    const spysetRespuestaServicio = spyOn(component, 'setRespuestaServicio');
    // Espiamos nuestra funcion setErrorValidador()
    const spysetErrorValidador = spyOn(component, 'setErrorValidador');
    // Espiamos el evento emit de propagarRespuestaServicio
    const spypropagarRespuestaServicio = spyOn(component.propagarRespuestaServicio, 'emit');
    // Espiamos el evento emit de propagarMensajeError
    const spypropagarMensajeError = spyOn(component.propagarMensajeError, 'emit');
    // Llamamos nuestra funcion
    component.dniConsultor = '';
    component.errorValidador = {
      banderaError: false,
      mensajeError: 'TEST',
      tipoInput: 'TEST'
    };

    // Llamamos nuestra funcion
    component.obtenerDatos();
    // Verificamos que llame nuestros funciones espiadas
    expect(spydesHabilitarBoton).toHaveBeenCalled();
    expect(spysetRespuestaServicio).toHaveBeenCalled();
    expect(spysetErrorValidador).toHaveBeenCalled();
    // Verificamos que se llame nuestro evento EMIT de propagarRespuestaServicio Y propagarMensajeError
    expect(spypropagarRespuestaServicio).toHaveBeenCalled();
    expect(spypropagarMensajeError).toHaveBeenCalled();

    // Seteamos el item isWithDniValidator en el localStorage para verificar la primera condicion
    localStorage.setItem('isWithDniValidator', 'true');
    // Asigamos un valor dniValidador con length igual a 0 para la primera condicion
    component.dniValidador = '';
    // Llamamos nuestra funcion
    component.obtenerDatos();
    // Verificamos que llame nuestros funciones espiadas
    expect(spydesHabilitarBoton).toHaveBeenCalledTimes(2);
    expect(spysetRespuestaServicio).toHaveBeenCalledTimes(2);
    expect(spysetErrorValidador).toHaveBeenCalledTimes(5);
     expect(spypropagarMensajeError).toHaveBeenCalledTimes(5);


    // Ahora verificamos para el caso que el dni validador NO sea valido
    component.dniValidador = 'X2Z4G678';
    // Llamamos nuestra funcion
    component.obtenerDatos();
    // Verificamos que llame nuestros funciones espiadas
    expect(spysetErrorValidador).toHaveBeenCalledTimes(7);
    expect(spydesHabilitarBoton).toHaveBeenCalledTimes(3);
    expect(spypropagarMensajeError).toHaveBeenCalledTimes(7);


    // Ahora verificamos para el caso del dniConsultor y dniValidador su expresion regular sea valida
    component.dniConsultor = '12345678';
    component.dniValidador = '12345678';
    // Espiamos nuestra funcion setDataRequestDatos()
    const spysetDataRequestDatos = spyOn(component, 'setDataRequestDatos').and.callThrough();
    // Espiamos nuesto servicio serviceObtenerConsolidado
    const spyServicio = spyOn(servicio, 'serviceObtenerConsolidado').and.callThrough();

    // Llamamos nuestra funcion
    component.obtenerDatos();
    // Verificamos que llame nuestra funcion setDataRequestDatos();
    expect(spysetDataRequestDatos).toHaveBeenCalled();
    // Verificamos que llame nuestro servicio
    expect(spyServicio).toHaveBeenCalled();
    expect(spysetRespuestaServicio).toHaveBeenCalledTimes(5);


    // Ahora verificamos nuestro servicio cuando nos devuelve un result:any
    // mockeamos el result y fakeamos nuestro servicio
    const mockResult = {

    };
    const spyServiceFake = spyOn(servicio, 'serviceObtenerConsolidado').and.callFake(() => {
      return of (mockResult);
    });
    component.errorValidador = {
      banderaError: false,
      mensajeError: 'TEST',
      tipoInput: 'TEST'
    };
    component.dniConsultor = '12345678';
    // Espiamos nuestra funcion salir clearAll
    const spyclearAll = spyOn(component, 'clearAll');
    // Llamamos nuestra funcion
    component.obtenerDatos();
    component.dniValidador = '12345678';
    // Verficamos que llame nuestro servicioFake
    expect(spyServiceFake).toHaveBeenCalled();
    // Verificamos llame nuestra funcion clearAll
    expect(spyclearAll).toHaveBeenCalled();
    // Verificamos que llame nuestra funcion desHabilitarBoton()
    expect(spydesHabilitarBoton).toHaveBeenCalled();
    // Verificamos que llame nuestro evento emit de propagarRespuestaServicio
    expect(spypropagarRespuestaServicio).toHaveBeenCalledTimes(7);

  });
/*
   obtenerDatos() {
        this.desHabilitarBoton();
        this.setRespuestaServicio(null, 'cleanFront');
        this.propagarRespuestaServicio.emit(this.respuestaServicio);
        this.setErrorValidador(false, Constant.MENSAJE_VACIO, 'dniValidador');
        this.propagarMensajeError.emit(this.errorValidador);

        if (localStorage.getItem('isWithDniValidator') !== null && localStorage.getItem('isWithDniValidator') === 'true') {
            if (this.dniValidador.length === 0) {
                this.setErrorValidador(true, Constant.MENSAJE_ERROR_DNI_VALIDADOR_VACIO, 'dniValidador');
                this.propagarMensajeError.emit(this.errorValidador);
                this.habilitarBotonSi();
                return;
            } else if (!this.regexpNumber.test(this.dniValidador)) {
                this.setErrorValidador(true, Constant.MENSAJE_ERROR_DNI_VALIDADOR_INVALIDO, 'dniValidador');
                this.propagarMensajeError.emit(this.errorValidador);
                this.habilitarBotonSi();
                return;
            }
        }
        this.setErrorValidador(false, Constant.MENSAJE_VACIO, 'dniConsultor');
        this.propagarMensajeError.emit(this.errorValidador);

        if (this.regexpNumber.test(this.dniConsultor) ) {

                this.dataRequest = this.setDataRequestDatos(this.dniConsultor);
                this.setRespuestaServicio('', Constant.MENSAJE_ESPERA_OBTENER_CONSOLIDADO);
                this.propagarRespuestaServicio.emit(this.respuestaServicio);

                this.servicioService.serviceObtenerConsolidado(this.dataRequest).subscribe(
                    (result: any) => {
                        this.desHabilitarBoton();
                        this.clearAll();
                        // tslint:disable-next-line:max-line-length
                        if (Object.entries(result.datoPersona).length !== 0 && result.codigoRespuesta === '0000' ) {
                            this.habilitarBotonSi();

                            this.setDatosResponseWithObjtDatosConsolidado(result);
                            this.propagarDatos.emit(this.datosConsolidado);

                            this.setErrorValidador(false, '', 'OK');
                            this.propagarMensajeError.emit(this.errorValidador);

                            this.setRespuestaServicio(Constant.CODIGO_OPERACION_EXITOSA, Constant.MENSAJE_OPERACION_CONSOLIDADO_EXITOSA);
                            this.propagarRespuestaServicio.emit(this.respuestaServicio);
                            this.habilitarBotonSi();

                        } if (result.codigoRespuesta === Constant.COD_ERROR_SESION) {
                            this.salir();
                            return;
                        } else {
                            this.setRespuestaServicio(Constant.CODIGO_OPERACION_ERROR, Constant.MENSAJE_OPERACION_DATOS);
                            this.propagarRespuestaServicio.emit(this.respuestaServicio);
                            this.clearAll();
                            this.habilitarBotonSi();
                        }
                    }, error => {
                        this.setRespuestaServicio('', Constant.MENSAJE_VACIO);
                        this.propagarRespuestaServicio.emit(this.respuestaServicio);
                        this.clearAll();
                        this.habilitarBotonSi();
                        this.toast.addToast({ title: 'Error', msg: Constant.MENSAJE_ERROR_CONECTIVIDAD, type: 'error' });
                        setTimeout(() => {this.salir(); }, 500);
                    }
                );

        } else if (this.dniConsultor.length === 0) {
                this.setErrorValidador(true, Constant.MENSAJE_ERROR_DNI_CONSULTOR_VACIO, 'dniConsultor');
                this.propagarMensajeError.emit(this.errorValidador);
                this.habilitarBotonSi();
        } else {
                this.setErrorValidador(true, Constant.MENSAJE_ERROR_DNI_CONSULTOR_INVALIDO, 'dniConsultor');
                this.propagarMensajeError.emit(this.errorValidador);
                this.habilitarBotonSi();
        }

    }


*/



});
