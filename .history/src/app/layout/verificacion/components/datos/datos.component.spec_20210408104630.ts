import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { of, Subject } from 'rxjs';
import { EnvService } from 'src/app/env.service';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { HuellaService } from '../../serviciosDeUI/huella.service';
import { DatosComponent } from './datos.component';

fdescribe('DatosComponent', () => {
  let component: DatosComponent;
  let fixture: ComponentFixture<DatosComponent>;
  // Instanciamos los servicios que utilizaremos
  let translateService: TranslateService;
  let servicio: ServicioService;
  let huellaService: HuellaService;
  class FakeRouter {
    navigate(params) {

    }
  }

  class FakeActivatedRoute {
    // params: Observable<any> = Observable.empty();
    private subject = new Subject();

    push(valor) {
      this.subject.next(valor);
    }
    get params() {
      return this.subject.asObservable();
    }
  }
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [DatosComponent, ToastComponent],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService,
        HuellaService,
        { provide: Router, useClass: FakeRouter },
        { provide: ToastrService, useClass: ToastrService },
        { provide: ActivatedRoute, useClass: FakeActivatedRoute }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatosComponent);
    // Injectamos nuestro servicio con TestBed.inject
    servicio = TestBed.inject(ServicioService);
    // Injectamos nuestro servicio de huella con TestBed.inject
    huellaService = TestBed.inject(HuellaService);
    component = fixture.componentInstance;
     // Mockeamos valor en localstorage de datosSesion
  /*   const mockDatosSesion = {
      '_ningunaHuella': '0',
      '_huellaVivaAmarillo': '0',
      '_noHuellaViva': '0',
      '_nistRojo': '0',
      '_umbralHuellaViva': '50.0',
      '_nistAmarillo': '0',
      '_huellaVivaRojo': '0',
      '_validarSixser': false,
      '_serviciosVerificacion': {
          'default': '301001',
          'tipos': [
              {
                  'descripcion': 'T33-ANSI',
                  'codigo': '301001'
              },
              {
                  'descripcion': 'T33-ANSI',
                  'codigo': '301001'
              },
              {
                  'descripcion': 'T35-WSQ',
                  'codigo': '301100'
              }
          ],
          'tiposServicioVerificacion': [
              {
                  '_descripcion': 'T33-ANSI',
                  '_codigo': '301001'
              },
              {
                  '_descripcion': 'T33-ANSI',
                  '_codigo': '301001'
              },
              {
                  '_descripcion': 'T35-WSQ',
                  '_codigo': '301100'
              }
          ]
      },
      '_acciones': [
          {
              '_descripcion': 'Mejor Huella',
              '_codigo': 'sixbio'
          },
          {
              '_descripcion': 'Capturar',
              '_codigo': 'sixbio'
          },
          {
              '_descripcion': 'Verificar',
              '_codigo': 'sixbio'
          },
          {
              '_descripcion': 'VerificarMOCard',
              '_codigo': 'sixbio'
          },
          {
              '_descripcion': 'Foto',
              '_codigo': 'sixser'
          },
          {
              '_descripcion': 'Datos',
              '_codigo': 'sixser'
          }
      ],
      '_idSesion': '000000202104051226028320041',
      '_validarHuellaViva': true,
      '_validarDniValidador': true,
      '_visualizar': 'true',
      '_sixbioVersion': '1.6',
      '_usuario': 'jburgos5',
      '_noHuellaCapturada': '0',
      '_validarIpCliente': true,
      '_rolesSCA': [
          {
              '_permisos': [],
              '_rol': 'EMP_VER'
          },
          {
              '_permisos': [],
              '_rol': 'VAL'
          }
      ],
      '_codigoRespuesta': '00000',
      '_mensajeRespuesta': 'OPERACION REALIZADA SATISFACTORIAMENTE'
  };*/
    fixture.detectChanges();
    // Habilitamos en jasmine el re espiar las funciones , caso contrario tendriamos un error
    jasmine.getEnv().allowRespy(true);
  //  localStorage.setItem('datosSesion', JSON.stringify(mockDatosSesion));
  });

  it('DatosComponent creado correctamente', () => {
    expect(component).toBeTruthy();
  });

  it('Verificamos la funcion clearAll()', () => {
    // Espiamos nuestra funcion clearDatosConsolidado()
    const spyclearDatosConsolidado = spyOn(component, 'clearDatosConsolidado').and.callThrough();
    // Espiamos nuestra funcion cleanFirma()
    const spycleanFirma = spyOn(component, 'cleanFirma').and.callThrough();
    // Espiamos nuestra funcion cleanImagen()
    const spycleanImagen = spyOn(component, 'cleanImagen').and.callThrough();
    // Llamamos nuestra funcion clearAll()
    component.clearAll();
    // Verificamos que nuestros espias fueran llamados
    expect(spyclearDatosConsolidado).toHaveBeenCalled();
    expect(spycleanFirma).toHaveBeenCalled();
    expect(spycleanImagen).toHaveBeenCalled();
  });

  it('Verificamos la funcion salir()', () => {
    // Espiamos nuestro servicio serviceSignOff
    const spyServicios = spyOn(servicio, 'serviceSignOff').and.callThrough();
    // Llamamos nuestra funcion salir()
    component.salir();
    // Verificamos que fuera llamado nuestro servicio
    expect(spyServicios).toHaveBeenCalled();
    component.dataRequest = {};
    // Ahora verificamos la primera condicion cuando codigoRespuesta sea Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    const mockDataResponse = {
      codigoRespuesta : Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO,
    };
    // Injectamos nuestro router previamente fakeado para espiar la funcion navigate
    const router = TestBed.inject(Router);
    // Espiamos la funcion navigate de router
    const spyRouterFake = spyOn(router, 'navigate').and.callThrough();
    spyOn(localStorage, 'clear').and.callFake(() => {'No hago nada'; });
    // Ahora hacemos que nuestro servicio nos devuelva con un callFake el dataResponse previamente mockeado
    const spyServicioFake = spyOn(servicio, 'serviceSignOff').and.callFake(() => {
      return of(mockDataResponse);
    });
        // Llamamos nuestra funcion salir()
        component.salir();
        // Verificamos que fuera llamado nuestro servicio FAKE
        expect(spyServicioFake).toHaveBeenCalled();
        // Verificamos que fuera llamado navigate de  Router Fake
        expect(spyRouterFake).toHaveBeenCalled();


  });
/*
     salir() {
        this.servicioService.serviceSignOff(this.dataRequest).subscribe((dataResponse: any) => {

            if (dataResponse.codigoRespuesta === Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO) {
                localStorage.clear();
                this.router.navigate(['/login']);
            } else {

                localStorage.clear();
                this.router.navigate(['/login']);

            }
        }
            , error => {
                localStorage.clear();
                this.router.navigate(['/login']);
            });
    }
*/
});
