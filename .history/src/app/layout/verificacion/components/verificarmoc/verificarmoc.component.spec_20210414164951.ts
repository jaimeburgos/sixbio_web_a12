import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { of, Subject, throwError } from 'rxjs';
import { EnvService } from 'src/app/env.service';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { HuellaService } from '../../serviciosDeUI/huella.service';
import { VerificarmocComponent } from './verificarmoc.component';

fdescribe('VerificarmocComponent', () => {
  let component: VerificarmocComponent;
  let fixture: ComponentFixture<VerificarmocComponent>;
  // Instanciamos los servicios que utilizaremos
  let servicio: ServicioService;
  let huellaService: HuellaService;
  class FakeRouter {
    navigate(params) {

    }
  }

  class FakeActivatedRoute {
    // params: Observable<any> = Observable.empty();
    private subject = new Subject();

    push(valor) {
      this.subject.next(valor);
    }
    get params() {
      return this.subject.asObservable();
    }
  }
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VerificarmocComponent, ToastComponent],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService,
        HuellaService,
        { provide: Router, useClass: FakeRouter },
        { provide: ToastrService, useClass: ToastrService },
        { provide: ActivatedRoute, useClass: FakeActivatedRoute }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificarmocComponent);
    // Injectamos nuestro servicio con TestBed.inject
    servicio = TestBed.inject(ServicioService);
    // Injectamos nuestro servicio de huella con TestBed.inject
    huellaService = TestBed.inject(HuellaService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('VerificarmocComponent creado correctamente', () => {
    expect(component).toBeTruthy();
  });


  it('Verificamos la funcion ngOnInit()', () => {
    // Llamamos nuestro servicio huellaDerecha Y huellaIzquierda y fakeamos su retorno
    const mockHuellaDerechaCaptura = {
      huellaCompaCardD : 'Test_huellaCompaCardD',
      nistD : 'Test_nist',
    };
    const mockHuellaIzquierda = {
      huellaCompaCardI : 'Test_huellaTemplateI',
      nistI : 'Test_nistI',
    };

    // Mockeamos los inputs dniConsultor y tipoCaptura
    component.dniConsultor = '12345678';
    component.tipoCaptura = 0;

    // Fakeamos las respuestas de los servicios de las Huellas
    const spyFakeHuellaDerecha = spyOn(huellaService.huellaDerecha, 'subscribe').and.callFake(() => {
      huellaService.huellaDerecha.next(mockHuellaDerechaCaptura);

    });
    const spyFakeHuellaIzquierda = spyOn(huellaService.huellaIzquierda, 'subscribe').and.callFake(() => {
      huellaService.huellaIzquierda.next(mockHuellaIzquierda);
    });

    // Llamamos nuestra funcion ngOnInit()
    component.ngOnInit();

    // Verificamos que se llame nuestros servicio fakeados
    expect(spyFakeHuellaIzquierda).toHaveBeenCalled();
    expect(spyFakeHuellaDerecha).toHaveBeenCalled();

    // Verificamos la asignacion de los valores a nuestras variables para HuellaDerecha
    expect(component.nistDerecha).toEqual(mockHuellaDerechaCaptura.nistD);
    expect(component.huellaDerecha).toEqual(mockHuellaDerechaCaptura.huellaCompaCardD);

    // Ahora verificamos para HuellaIzquiera
    expect(component.nistIzquierda).toEqual(mockHuellaIzquierda.nistI);
    expect(component.huellaIzquierda).toEqual(mockHuellaIzquierda.huellaCompaCardI);

    // Verificamos los valores entidad.dniConsultor y entidad.tipoCaptura
    expect(component.entidad.dniConsultor).toEqual('12345678');
    expect(component.entidad.tipoCaptura).toEqual(0);


  });


  it('Verificamos la funcion servicioVerificacionMOC()', () => {
    // Espiamos nuestra funcion setSendRespuesta
    const spysetSendRespuesta = spyOn(component, 'setSendRespuesta');
    // Espiamos nuestra funcion limpiarMensaje
    const spylimpiarMensaje = spyOn(component, 'limpiarMensaje');
    // Espiamos nuestra funcion  desHabilitarBoton
    const spydesHabilitarBoton = spyOn(component, 'desHabilitarBoton');
    // Llamamos nuestra funcion servicioVerificacionMOC
    component.servicioVerificacionMOC();
    // Verificamos que se llamen nuestros espias
    expect(spysetSendRespuesta).toHaveBeenCalled();
    expect(spylimpiarMensaje).toHaveBeenCalled();
    expect(spydesHabilitarBoton).toHaveBeenCalled();


    // Verificamos el primer caso de switch donde entidad.tipoCaptura sea 0
    component.entidad.tipoCaptura = 0;
    // Espiamos nuestra funcion inicializarVerificacionMOCDerecho
    const spyinicializarVerificacionMOCDerecho = spyOn(component, 'inicializarVerificacionMOCDerecho');
    // Llamamos nuestra funcion
    component.servicioVerificacionMOC();
    // Verificamos que llame nuestra funcion inicializarVerificacionMOCDerecho
    expect(spyinicializarVerificacionMOCDerecho).toHaveBeenCalled();


    // Verificamos el primer caso de switch donde entidad.tipoCaptura sea 1
    component.entidad.tipoCaptura = 1;
    // Espiamos nuestra funcion inicializarVerificacionMOCDerecho
    const spyinicializarVerificacionMOCIzquierdo = spyOn(component, 'inicializarVerificacionMOCIzquierdo');
    // Llamamos nuestra funcion
    component.servicioVerificacionMOC();
    // Verificamos que llame nuestra funcion inicializarVerificacionMOCDerecho
    expect(spyinicializarVerificacionMOCIzquierdo).toHaveBeenCalled();


    // Verificamos el primer caso de switch donde entidad.tipoCaptura sea 2
    component.entidad.tipoCaptura = 2;
    // Llamamos nuestra funcion
    component.servicioVerificacionMOC();
    // Verificamos que llame nuestra funcion inicializarVerificacionMOCDerecho , en este punto se llamo dos veces
    expect(spyinicializarVerificacionMOCDerecho).toHaveBeenCalledTimes(2);
  });



  it('Verificamos la funcion setRespuestaServicio(codigo: String, mensaje: String)', () => {
    // Mockeamos los valores de entrada de nuestra funcion
    const mockCodigo = 'TestCodigo';
    const mockMensaje = 'TestMensaje';
    // Llamamos nuestra funcion
    component.setRespuestaServicio(mockCodigo, mockMensaje);
    // Verificamos la asignacion de valores al objeto respuestaServicio
    expect(component.respuestaServicio.codigoRespuesta).toEqual(mockCodigo);
    expect(component.respuestaServicio.mensajeRespuesta).toEqual(mockMensaje);
  });

  it('Verificamos la funcion formatoTipoCaptura(captura)', () => {
    // Mockeamos los valores de entrada de nuestra funcion
    // Asigamos valor 0 para el primer caso
    let mockCaptura = 0;
    // Llamamos nuestra funcion y verificamos que nos retorne el valor adecuado
    expect(component.formatoTipoCaptura(mockCaptura)).toEqual('0104');

    // Asigamos valor 1 para el siguiente caso
    mockCaptura = 1;
    // Llamamos nuestra funcion y verificamos que nos retorne el valor adecuado
    expect(component.formatoTipoCaptura(mockCaptura)).toEqual('0204');


    // Asigamos valor 2 para el siguiente caso
    mockCaptura = 2;
    // Llamamos nuestra funcion y verificamos que nos retorne el valor adecuado
    expect(component.formatoTipoCaptura(mockCaptura)).toEqual('0104');

    // En caso que no se cumplan los anteriores nos retorna el valor de entrada
    mockCaptura = 9999;
    // Llamamos nuestra funcion y verificamos que nos retorne el valor de entrada
    expect(component.formatoTipoCaptura(mockCaptura)).toEqual(mockCaptura);
  });

  it('Verificamos la funcion setSendRespuesta(codigoR, mensajeR) ', () => {
    // mockeamos los valores de entrada de nuestra funcion
    const mockCodigoR = 'TestCodigoR';
    const mockMensajeR = 'TestMensajeR';
    // Espiamos nuestra funcion setRespuestaServicio
    const spysetRespuestaServicio = spyOn(component, 'setRespuestaServicio');
    // Espiamos nuestro evento emit de propagarRespuestaServicio
    const spypropagarRespuestaServicio = spyOn(component.propagarRespuestaServicio, 'emit');
    // Llamamos nuestra funcion y le pasamos los valores de entrada mockeado
    component.setSendRespuesta(mockCodigoR, mockMensajeR);
    // Verificamos que se llame nuestro espia setRespuestaServicio
    expect(spysetRespuestaServicio).toHaveBeenCalled();
    // Verificamos que que se llame nuestro espia de emit
    expect(spypropagarRespuestaServicio).toHaveBeenCalled();
  });

  it('Verificamos la funcion habilitarBoton()', () => {
    // Para probar la condicion seteamos el item botonMOC con valor 'true'
    localStorage.setItem('botonMOC', 'true');
    // Llamamos nuestra funcion y verificamos el valor que nos retorna sea true
    expect(component.habilitarBoton()).toBeTruthy();

    // Para probar la siguiente condicion seteamos cualquier valor diferente de true
    localStorage.setItem('botonMOC', 'something');
    // Llamamos nuestra funcion y verificamos el valor que nos retorna sea true
    expect(component.habilitarBoton()).toBeFalsy();
  });

  it('Verificamos la funcion desHabilitarBoton()', () => {
    // Llamamos nuestra funcion
    component.desHabilitarBoton();
    // Verificamos que setee el item botonMOC con valor false
    expect(localStorage.getItem('botonMOC')).toEqual('false');
  });

  it('Verificamos la funcion siHabilitarBoton()', () => {
    // Llamamos nuestra funcion
    component.siHabilitarBoton();
    // Verificamos que setee el item botonMOC con valor true
    expect(localStorage.getItem('botonMOC')).toEqual('true');
  });

  it('Verificamos la funcion limpiarMensaje()', () => {
    // Espiamos nuestra funcion setRespuestaServicio
    const spysetRespuestaServicio = spyOn(component, 'setRespuestaServicio');
    // Espiamos nuestro evento emit de propagarRespuestaServicio
    const spypropagarRespuestaServicio = spyOn(component.propagarRespuestaServicio, 'emit');
    // Llamamos nuestra funcion
    component.limpiarMensaje();
    // Verificamos que fuera llamada nuestra funcion setRespuestaServicio
    expect(spysetRespuestaServicio).toHaveBeenCalled();
    // Verificamos que se llame el evento emit de propagarRespuestaServicio
    expect(spypropagarRespuestaServicio).toHaveBeenCalled();
  });

  it('Verificamos la funcion validarRespuesta(c)', () => {
    // Mockeamos un valor de entrada
    // Probamos el primer valor que nos retorna true
    let test = '30906';
    expect(component.validarRespuesta(test)).toBeTruthy();
    // Analogamente lo mismo que el resto de codigos de Respuesta
    test = '30302';
    expect(component.validarRespuesta(test)).toBeTruthy();
    test = '30101';
    expect(component.validarRespuesta(test)).toBeTruthy();
    test = '10302';
    expect(component.validarRespuesta(test)).toBeTruthy();

    // En caso sea diferente de los casos anteriores nos retorna false
    test = 'Diferente de los anteriores';
    // Verificamos que nos retorne false
    expect(component.validarRespuesta(test)).toBeFalsy();
  });

  it('Verificamos la funcion setDataRequest()', () => {
    // asigamos valor 0 a tipoCaptura para probar el primer caso
    component.tipoCaptura = 0;
    // Espiamos nuestra funcion generarJsonVerificacionMOC de utilitario
    const spygenerarJsonVerificacionMOC = spyOn(component.utilitario, 'generarJsonVerificacionMOC');
    // Espiamos nuestra funcion formatoTipoCaptura
    const spyformatoTipoCaptura = spyOn(component, 'formatoTipoCaptura');
    // Llamamos nuestra funcion
    component.setDataRequest();
    // Verificamos que nuestra funcion  de utilitario fuera llamada
    expect(spygenerarJsonVerificacionMOC).toHaveBeenCalled();
    // Verificamos que nuestra funcion formatoTipoCaptura fuera llamada
    expect(spyformatoTipoCaptura).toHaveBeenCalled();

    // asigamos valor 1 a tipoCaptura para probar el siguiente caso
    component.tipoCaptura = 1;
    // Llamamos nuestra funcion
    component.setDataRequest();
    // Verificamos que nuestra funcion  de utilitario fuera llamada dos veces en este punto
    expect(spygenerarJsonVerificacionMOC).toHaveBeenCalledTimes(2);
    // Verificamos que nuestra funcion formatoTipoCaptura fuera llamada dos veces en este punto
    expect(spyformatoTipoCaptura).toHaveBeenCalledTimes(2);

    // asigamos valor 2 a tipoCaptura para probar el siguiente caso
    component.tipoCaptura = 2;
    // Llamamos nuestra funcion
    component.setDataRequest();
    // Verificamos que nuestra funcion  de utilitario fuera llamada tres veces en este punto
    expect(spygenerarJsonVerificacionMOC).toHaveBeenCalledTimes(3);
    // Verificamos que nuestra funcion formatoTipoCaptura fuera llamada tres veces en este punto
    expect(spyformatoTipoCaptura).toHaveBeenCalledTimes(3);

  });

  it('Verificamos la funcion inicializarVerificacionMOCIzquierdo()', () => {
    // Espiamos nuestra funcion setRespuestaServicio
    const spysetRespuestaServicio = spyOn(component, 'setRespuestaServicio');
    // Espiamos el evento emit de propagarRespuestaServicio
    const spypropagarRespuestaServicio = spyOn(component.propagarRespuestaServicio, 'emit');
    // Espiamos nuestro servicio serviceVerificacioMOC
    const spyService = spyOn(servicio, 'serviceVerificacioMOC').and.callThrough();
    // Llamamos nuestra funcion
    component.inicializarVerificacionMOCIzquierdo();
    // Verificamos que que llame nuestros espias
    expect(spypropagarRespuestaServicio).toHaveBeenCalled();
    expect(spyService).toHaveBeenCalled();
    expect(spysetRespuestaServicio).toHaveBeenCalled();


    // Mockeamos el dataResponse para nuestro servicio
    // Probamos la primera condicion cuando codigoRespuesta sea constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCES.CODIGO
    const mockDataResponse = {
      codigoRespuesta: component.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCES.CODIGO,
      mensajeRespuesta : ''
    };
    // Para ello usaremos and.callFake
    const spyServiceFake = spyOn(servicio, 'serviceVerificacioMOC').and.callFake(() => {
      return of(mockDataResponse);
    });
    // Espiamos nuestra funcion siHabilitarBoton
    const spysiHabilitarBoton = spyOn(component, 'siHabilitarBoton');
    // Llamamos nuestra funcion
    component.inicializarVerificacionMOCIzquierdo();
    // Verificamos que que llame nuestro servicio fake
    expect(spyServiceFake).toHaveBeenCalled();
    // Verificamos que que llame nuestros espias
    expect(spypropagarRespuestaServicio).toHaveBeenCalled();
    expect(spysiHabilitarBoton).toHaveBeenCalled();
    // Verificamos que llame propagarRespuestaServicio y el evento emit
    expect(spysetRespuestaServicio).toHaveBeenCalled();


    // Ahora verificamos para el caso que codigoRespuesta sea constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCESS_WARN.CODIGO
    mockDataResponse.codigoRespuesta = component.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCESS_WARN.CODIGO;
    // Llamamos nuestra funcion
    component.inicializarVerificacionMOCIzquierdo();
    // Verificamos que que llame nuestros espias
    expect(spysetRespuestaServicio).toHaveBeenCalled();
    expect(spysiHabilitarBoton).toHaveBeenCalled();
    // Verificamos que llame propagarRespuestaServicio y el evento emit
    expect(spypropagarRespuestaServicio).toHaveBeenCalled();

    // Probamos para el que caso que codigoRespuesta sea '60000'
    mockDataResponse.codigoRespuesta = '60000';
    // Llamamos nuestra funcion
    component.inicializarVerificacionMOCIzquierdo();
    // Verificamos que que llame nuestros espias
    expect(spysetRespuestaServicio).toHaveBeenCalled();
    expect(spysiHabilitarBoton).toHaveBeenCalled();
    // Verificamos que llame propagarRespuestaServicio y el evento emit
    expect(spypropagarRespuestaServicio).toHaveBeenCalled();

    // Probamos para el que caso que codigoRespuesta sea '50001'
    mockDataResponse.codigoRespuesta = '50001';
    // Llamamos nuestra funcion
    component.inicializarVerificacionMOCIzquierdo();
    // Verificamos que que llame nuestros espias
    expect(spysetRespuestaServicio).toHaveBeenCalled();
    expect(spysiHabilitarBoton).toHaveBeenCalled();
    // Verificamos que llame propagarRespuestaServicio y el evento emit
    expect(spypropagarRespuestaServicio).toHaveBeenCalled();

    // Probamos para el que  se evalua con validar respuesta , asigamos uno de su valores que retorna true
    mockDataResponse.codigoRespuesta = '10302';
    // Espiamos nuestra funcion validarRespuesta()
    const spyvalidarRespuesta = spyOn(component, 'validarRespuesta').and.callThrough();
    // Llamamos nuestra funcion
    component.inicializarVerificacionMOCIzquierdo();
    // Verificamos que llame nuestra funcion validarRespuesta
    expect(spyvalidarRespuesta).toHaveBeenCalled();
    // Verificamos que que llame nuestros espias
    expect(spysetRespuestaServicio).toHaveBeenCalled();
    expect(spysiHabilitarBoton).toHaveBeenCalled();
    // Verificamos que llame propagarRespuestaServicio y el evento emit
    expect(spypropagarRespuestaServicio).toHaveBeenCalled();

    // Ahora verificamos para el caso que nos retorne un codigoRespuesta mapeado anteriormente
    mockDataResponse.codigoRespuesta = 'Diferente de cualquiera';
    // Llamamos nuestra funcion
    component.inicializarVerificacionMOCIzquierdo();
    // Verificamos que que llame nuestros espias
    expect(spysetRespuestaServicio).toHaveBeenCalled();
    expect(spysiHabilitarBoton).toHaveBeenCalled();
    // Verificamos que llame propagarRespuestaServicio y el evento emit
    expect(spypropagarRespuestaServicio).toHaveBeenCalled();

    // Ahora verificamos para el caso nuestro servicio nos retorne un ERROR
    // Para ello usamos and.returnValue y throwError
    const spyServiceError = spyOn(servicio, 'serviceVerificacioMOC').and.returnValue(throwError('serviceVerificacioMOC ERROR!'));
    // Espiamos y fakeamos nuestra funcion Toast , caso contrario nos retornara un error por libreria Translate
    const spyToastFake = spyOn(component.toast, 'addToast').and.callFake(() => {'Nada'; });
    // Llamamos nuestra funcion
    component.inicializarVerificacionMOCIzquierdo();
    // Verificamos que llame nuestro servicio Error
    expect(spyServiceError).toHaveBeenCalled();
    // Verificamos que que llame nuestra funcion siHabilitarBoton
    expect(spysiHabilitarBoton).toHaveBeenCalled();
    // Verificamos que se llame nuestro ToastFake
    expect(spyToastFake).toHaveBeenCalled();
  });


  it('Verificamos la funcion inicializarVerificacionMOCDerecho()', () => {
    // Espiamos nuestra funcion setRespuestaServicio
    const spysetRespuestaServicio = spyOn(component, 'setRespuestaServicio');
    // Espiamos nuestro evento emit de propagarRespuestaServicio
    const spypropagarRespuestaServicio = spyOn(component.propagarRespuestaServicio, 'emit');
    // Espiamos nuestro servicio serviceVerificacioMOC
    const spyService = spyOn(servicio, 'serviceVerificacioMOC').and.callThrough();
    // Llamamos nuestra funcion
    component.inicializarVerificacionMOCDerecho();
    // Verificamos que llame nuestros espias
    expect(spypropagarRespuestaServicio).toHaveBeenCalled();
    expect(spysetRespuestaServicio).toHaveBeenCalled();
    expect(spyService).toHaveBeenCalled();

    // Mockeamos un dataResponse para probar las condiciones
    // Para validar la primera condicion codigoRespuesta sera constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCES.CODIGO
    const mockDataResponse = {
      codigoRespuesta  : component.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCES.CODIGO,
      mensajeRespuesta : ''
    };
    // Fakeamos el retorno de nuestro servicio con and.callFake()
    const spyServiceFake = spyOn(servicio, 'serviceVerificacioMOC').and.callFake(() => {
      return of(mockDataResponse);
    });
    // Espiamos nuestra funcion siHabilitarBoton
    const spysiHabilitarBoton = spyOn(component, 'siHabilitarBoton');
    // Llamamos nuestra funcion
    component.inicializarVerificacionMOCDerecho();
    // Verificamos que se llame nuestro servicio FAKE
    expect(spyServiceFake).toHaveBeenCalled();
    // Verificamos que que llame nuestros espias
    expect(spysiHabilitarBoton).toHaveBeenCalled();
    expect(spypropagarRespuestaServicio).toHaveBeenCalled();
    expect(spysetRespuestaServicio).toHaveBeenCalled();

    // Ahora verificamos para el caso que codigoRespuesta seaconstant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCESS_WARN.CODIGO
    mockDataResponse.codigoRespuesta = component.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCESS_WARN.CODIGO;
    // Llamamos nuestra funcion
    component.inicializarVerificacionMOCDerecho();
    // Verificamos que que llame nuestros espias
    expect(spysiHabilitarBoton).toHaveBeenCalled();
    expect(spypropagarRespuestaServicio).toHaveBeenCalled();
    expect(spysetRespuestaServicio).toHaveBeenCalled();

    // Ahora verificamos el caso que codigoRespuesta sea '60000' o '80003'
    mockDataResponse.codigoRespuesta = '60000';
    // Llamamos nuestra funcion
    component.inicializarVerificacionMOCDerecho();
    // Verificamos que que llame nuestros espias
    expect(spysiHabilitarBoton).toHaveBeenCalled();
    expect(spypropagarRespuestaServicio).toHaveBeenCalled();
    expect(spysetRespuestaServicio).toHaveBeenCalled();

    // Ahora verificamos el caso que codigoRespuesta sea '50002' o '70000' o '30101' o '30103'
    mockDataResponse.codigoRespuesta = '50002';
    // Llamamos nuestra funcion
    component.inicializarVerificacionMOCDerecho();
    // Verificamos que que llame nuestros espias
    expect(spysiHabilitarBoton).toHaveBeenCalled();
    expect(spypropagarRespuestaServicio).toHaveBeenCalled();
    expect(spysetRespuestaServicio).toHaveBeenCalled();


    // Ahora verificamos el caso que codigoRespuesta sea '50001'
    mockDataResponse.codigoRespuesta = '50001';
    // Llamamos nuestra funcion
    component.inicializarVerificacionMOCDerecho();
    // Verificamos que que llame nuestros espias
    expect(spysiHabilitarBoton).toHaveBeenCalled();
    expect(spypropagarRespuestaServicio).toHaveBeenCalled();
    expect(spysetRespuestaServicio).toHaveBeenCalled();

    // Ahora verificamos el caso que se cumpla la condicion de validarRespuesta() el codigoRespuesta puede ser '10302'
    mockDataResponse.codigoRespuesta = '10302';
    const spyvalidarRespuesta = spyOn(component, 'validarRespuesta').and.callThrough();
    // Llamamos nuestra funcion
    component.inicializarVerificacionMOCDerecho();
    // Verificamos que que llame nuestros espias
    expect(spysiHabilitarBoton).toHaveBeenCalled();
    expect(spypropagarRespuestaServicio).toHaveBeenCalled();
    expect(spysetRespuestaServicio).toHaveBeenCalled();
    expect(spyvalidarRespuesta).toHaveBeenCalled();

  });

/*
inicializarVerificacionMOCDerecho() {
  this.setRespuestaServicio('', this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.MENSAJE_ESPERA);
  this.propagarRespuestaServicio.emit(this.respuestaServicio);
  this.servicioService.serviceVerificacioMOC(this.setDataRequest()).subscribe((dataResponseVerificacionMOC: any) => {

    if (dataResponseVerificacionMOC.codigoRespuesta === this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCES.CODIGO) {

       this.setRespuestaServicio
       (this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCES.CODIGO, this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCES.MENSAJE);
      this.propagarRespuestaServicio.emit(this.respuestaServicio);
      this.siHabilitarBoton();

    } else if (dataResponseVerificacionMOC.codigoRespuesta === this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCESS_WARN.CODIGO) {

       this.setRespuestaServicio
       (this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCESS_WARN.CODIGO, dataResponseVerificacionMOC.mensajeRespuesta);
        this.propagarRespuestaServicio.emit(this.respuestaServicio);
        this.siHabilitarBoton();

    } else if (dataResponseVerificacionMOC.codigoRespuesta === '60000' || dataResponseVerificacionMOC.codigoRespuesta === '80003') {

      this.setRespuestaServicio
      (this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.ERROR.CODIGO, this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.ERROR.MENSAJE);
      this.propagarRespuestaServicio.emit(this.respuestaServicio);
      this.siHabilitarBoton();

    } else if ( dataResponseVerificacionMOC.codigoRespuesta === '50002' || dataResponseVerificacionMOC.codigoRespuesta === '70000'
    || dataResponseVerificacionMOC.codigoRespuesta === '30303' || dataResponseVerificacionMOC.codigoRespuesta === '30101') {

      this.setRespuestaServicio('', dataResponseVerificacionMOC.mensajeRespuesta);
      this.propagarRespuestaServicio.emit(this.respuestaServicio);
      this.siHabilitarBoton();

    } else if (dataResponseVerificacionMOC.codigoRespuesta === '50001') {
      this.setRespuestaServicio('', 'Documento y/o Dispositivo biométrico no soportan la operación solicitada');
      this.propagarRespuestaServicio.emit(this.respuestaServicio);
      this.siHabilitarBoton();

    } else if (this.validarRespuesta(dataResponseVerificacionMOC.codigoRespuesta)) {
      this.setRespuestaServicio(dataResponseVerificacionMOC.codigoRespuesta, dataResponseVerificacionMOC.mensajeRespuesta );
      this.propagarRespuestaServicio.emit(this.respuestaServicio);
      this.siHabilitarBoton();

    } else {
      this.setRespuestaServicio('', '');
      this.propagarRespuestaServicio.emit(this.respuestaServicio);
      this.siHabilitarBoton();
      this.toast.addToast({ title: 'Error', msg: 'Error de conectividad', timeOut: 5000, type: 'error' });
    }
  },
  error => {
  this.setRespuestaServicio('', '');
  this.siHabilitarBoton();
  this.propagarRespuestaServicio.emit(this.respuestaServicio);
  this.toast.addToast({ title: 'Error', msg: 'Error de conectividad', timeOut: 5000, type: 'error' }); this.siHabilitarBoton(); });
}



*/







});
