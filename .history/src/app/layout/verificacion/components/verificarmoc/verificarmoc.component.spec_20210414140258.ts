import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs';
import { EnvService } from 'src/app/env.service';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { HuellaService } from '../../serviciosDeUI/huella.service';
import { VerificarmocComponent } from './verificarmoc.component';

fdescribe('VerificarmocComponent', () => {
  let component: VerificarmocComponent;
  let fixture: ComponentFixture<VerificarmocComponent>;
  // Instanciamos los servicios que utilizaremos
  let servicio: ServicioService;
  let huellaService: HuellaService;
  class FakeRouter {
    navigate(params) {

    }
  }

  class FakeActivatedRoute {
    // params: Observable<any> = Observable.empty();
    private subject = new Subject();

    push(valor) {
      this.subject.next(valor);
    }
    get params() {
      return this.subject.asObservable();
    }
  }
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [VerificarmocComponent, ToastComponent],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService,
        HuellaService,
        { provide: Router, useClass: FakeRouter },
        { provide: ToastrService, useClass: ToastrService },
        { provide: ActivatedRoute, useClass: FakeActivatedRoute }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificarmocComponent);
    // Injectamos nuestro servicio con TestBed.inject
    servicio = TestBed.inject(ServicioService);
    // Injectamos nuestro servicio de huella con TestBed.inject
    huellaService = TestBed.inject(HuellaService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('VerificarmocComponent creado correctamente', () => {
    expect(component).toBeTruthy();
  });


  it('Verificamos la funcion ngOnInit()', () => {
    // Llamamos nuestro servicio huellaDerecha Y huellaIzquierda y fakeamos su retorno
    const mockHuellaDerechaCaptura = {
      huellaCompaCardD : 'Test_huellaCompaCardD',
      nistD : 'Test_nist',
    };
    const mockHuellaIzquierda = {
      huellaCompaCardI : 'Test_huellaTemplateI',
      nistI : 'Test_nistI',
    };

    // Mockeamos los inputs dniConsultor y tipoCaptura
    component.dniConsultor = '12345678';
    component.tipoCaptura = 0;

    // Fakeamos las respuestas de los servicios de las Huellas
    const spyFakeHuellaDerecha = spyOn(huellaService.huellaDerecha, 'subscribe').and.callFake(() => {
      huellaService.huellaDerecha.next(mockHuellaDerechaCaptura);

    });
    const spyFakeHuellaIzquierda = spyOn(huellaService.huellaIzquierda, 'subscribe').and.callFake(() => {
      huellaService.huellaIzquierda.next(mockHuellaIzquierda);
    });

    // Llamamos nuestra funcion ngOnInit()
    component.ngOnInit();

    // Verificamos que se llame nuestros servicio fakeados
    expect(spyFakeHuellaIzquierda).toHaveBeenCalled();
    expect(spyFakeHuellaDerecha).toHaveBeenCalled();

    // Verificamos la asignacion de los valores a nuestras variables para HuellaDerecha
    expect(component.nistDerecha).toEqual(mockHuellaDerechaCaptura.nistD);
    expect(component.huellaDerecha).toEqual(mockHuellaDerechaCaptura.huellaCompaCardD);

    // Ahora verificamos para HuellaIzquiera
    expect(component.nistIzquierda).toEqual(mockHuellaIzquierda.nistI);
    expect(component.huellaIzquierda).toEqual(mockHuellaIzquierda.huellaCompaCardI);

    // Verificamos los valores entidad.dniConsultor y entidad.tipoCaptura
    expect(component.entidad.dniConsultor).toEqual('12345678');
    expect(component.entidad.tipoCaptura).toEqual(0);


  });


  it('Verificamos la funcion servicioVerificacionMOC()', () => {
    // Espiamos nuestra funcion setSendRespuesta
    const spysetSendRespuesta = spyOn(component, 'setSendRespuesta');
    // Espiamos nuestra funcion limpiarMensaje
    const spylimpiarMensaje = spyOn(component, 'limpiarMensaje');
    // Espiamos nuestra funcion  desHabilitarBoton
    const spydesHabilitarBoton = spyOn(component, 'desHabilitarBoton');
    // Llamamos nuestra funcion servicioVerificacionMOC
    component.servicioVerificacionMOC();
    // Verificamos que se llamen nuestros espias
    expect(spysetSendRespuesta).toHaveBeenCalled();
    expect(spylimpiarMensaje).toHaveBeenCalled();
    expect(spydesHabilitarBoton).toHaveBeenCalled();


    // Verificamos el primer caso de switch donde entidad.tipoCaptura sea 0
    component.entidad.tipoCaptura = 0;
    // Espiamos nuestra funcion inicializarVerificacionMOCDerecho
    const spyinicializarVerificacionMOCDerecho = spyOn(component, 'inicializarVerificacionMOCDerecho');
    // Llamamos nuestra funcion
    component.servicioVerificacionMOC();
    // Verificamos que llame nuestra funcion inicializarVerificacionMOCDerecho
    expect(spyinicializarVerificacionMOCDerecho).toHaveBeenCalled();


    // Verificamos el primer caso de switch donde entidad.tipoCaptura sea 1
    component.entidad.tipoCaptura = 1;
    // Espiamos nuestra funcion inicializarVerificacionMOCDerecho
    const spyinicializarVerificacionMOCIzquierdo = spyOn(component, 'inicializarVerificacionMOCIzquierdo');
    // Llamamos nuestra funcion
    component.servicioVerificacionMOC();
    // Verificamos que llame nuestra funcion inicializarVerificacionMOCDerecho
    expect(spyinicializarVerificacionMOCIzquierdo).toHaveBeenCalled();



    // Verificamos el primer caso de switch donde entidad.tipoCaptura sea 2
    component.entidad.tipoCaptura = 2;
    // Llamamos nuestra funcion
    component.servicioVerificacionMOC();
    // Verificamos que llame nuestra funcion inicializarVerificacionMOCDerecho , en este punto se llamo dos veces
    expect(spyinicializarVerificacionMOCDerecho).toHaveBeenCalledTimes(2);



  });



  /*

  servicioVerificacionMOC() {
    this.setSendRespuesta(null, 'cleanFront');
    this.limpiarMensaje();
    this.desHabilitarBoton();
                switch (this.entidad.tipoCaptura) {
                    case 0:
                        this.inicializarVerificacionMOCDerecho();
                        break;
                    case 1:
                        this.inicializarVerificacionMOCIzquierdo();
                        break;
                    case 2:
                        this.inicializarVerificacionMOCDerecho();
                        break;
                    default:
                        break;
                }
}


*/



});
