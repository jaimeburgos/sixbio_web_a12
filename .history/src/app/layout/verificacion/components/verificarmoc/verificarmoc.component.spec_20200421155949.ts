import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerificarmocComponent } from './verificarmoc.component';

describe('VerificarmocComponent', () => {
  let component: VerificarmocComponent;
  let fixture: ComponentFixture<VerificarmocComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerificarmocComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerificarmocComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
