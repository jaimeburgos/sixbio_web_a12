import { Component, OnInit, Input, ViewChild, EventEmitter, Output } from '@angular/core';
import { ServicioService } from '../../../../servicio/servicio.service';
import { ServicioApiService } from '../../../../servicio/servicio-api.service';
import { ButtonDirective } from 'src/app/my-directive/directive/button/button.directive';
import { VerificacionComponent } from '../../verificacion.component';
import { IObjVerificacion } from 'src/app/nucleo/interface/IObjVerificacion';
import { IHuellaMOC } from 'src/app/nucleo/interface/IHuellaMOC';
import { IRespuestaServicio } from 'src/app/nucleo/interface/IRespuestaServicio';
import { Constant } from '../../../../nucleo/constante/Constant';
import { Utilitario } from '../../../../nucleo/util/Utilitario';
import { Router } from '@angular/router';
import { HuellaService } from '../../serviciosDeUI/huella.service';
import { IErrorValidador } from 'src/app/nucleo/interface/IErrorValidador';
import { IDatosHuellaDerecha } from 'src/app/nucleo/interface/IDatosHuellaDerecha';
import { IDatosHuellaIzquierda } from 'src/app/nucleo/interface/IDatosHuellaIzquierda';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';

@Component({
  selector: 'app-verificarmoc',
  templateUrl: './verificarmoc.component.html',
  styleUrls: ['./verificarmoc.component.scss']
})
export class VerificarmocComponent implements OnInit {
  utilitario: Utilitario;
  constant: Constant;
  verificacionComponent: VerificacionComponent;
/*   const valorRequestMOC: string; */
@ViewChild(ToastComponent, { static: true }) toast: ToastComponent;
@ViewChild(ButtonDirective, { static: true })
buttonDirective = null;
respuestaServicio: IRespuestaServicio = {
    codigoRespuesta: '',
    mensajeRespuesta: ''
};
  @Input()
  dniConsultor;

  @Input()
  tipoCaptura;

  @Output()
    propagarMensajeError = new EventEmitter<IErrorValidador>();

    @Output()
    propagarRespuestaServicio = new EventEmitter<IRespuestaServicio>();

    @Output()
    propagarMensajeRespuesta = new EventEmitter();

  huellaDerecha = '';
  huellaIzquierda = '';
  nistDerecha: String = '';
  nistIzquierda: String = '';
  huellaVivaIzquierda: String = '';
  huellaVivaDerecha: String = '';

  mensajeVacio: IRespuestaServicio = {
    codigoRespuesta: null,
    mensajeRespuesta: null
  };
  datosHuellaDerecha: IDatosHuellaDerecha = {
    huellaD: '',
    huellaCompaCardD: '',
    huellaWSQD: '',
    huellaTemplateD: '',
    nistD: '',
    huellavivaD: '',
    imagenNistD: '',
    imagenhuellavivaD: ''
};
datosHuellaIzquierda: IDatosHuellaIzquierda = {
    huellaI: '',
    huellaCompaCardI: '',
    huellaWSQI: '',
    huellaTemplateI: '',
    nistI: '',
    huellavivaI: '',
    imagenNistI: '',
    imagenhuellavivaI: ''
};
  data: Object = {};
  entidad: IObjVerificacion = {
      dniValidador: '',
      dniConsultor: '',
      tipoCaptura: 0
  };

  constructor(private servicioService: ServicioService, private router: Router, private huellaService: HuellaService) {
    this.constant = new Constant;
  }

  ngOnInit() {
    this.huellaService.huellaDerecha.subscribe(huellaDerecha => {
      this.huellaDerecha = huellaDerecha.huellaCompaCardD;
      this.nistDerecha = huellaDerecha.nistD;
    });
    this.huellaService.huellaIzquierda.subscribe(huellaIzquierda => {
      this.huellaIzquierda = huellaIzquierda.huellaCompaCardI;
      this.nistIzquierda = huellaIzquierda.nistI;
    });
    this.entidad.dniConsultor = this.dniConsultor;
    this.entidad.tipoCaptura = this.tipoCaptura;
    this.utilitario = new Utilitario();
  }

  servicioVerificacionMOC() {
    this.setSendRespuesta(null, 'cleanFront');
    this.limpiarMensaje();
    this.desHabilitarBoton();
                switch (this.entidad.tipoCaptura) {
                    case 0:
                        this.inicializarVerificacionMOCDerecho();
                        break;
                    case 1:
                        this.inicializarVerificacionMOCIzquierdo();
                        break;
                    case 2:
                        this.inicializarVerificacionMOCDerecho();
                        break;
                    default:
                        break;
                }
}

inicializarVerificacionMOCDerecho() {
  this.setRespuestaServicio('', this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.MENSAJE_ESPERA);
  this.propagarRespuestaServicio.emit(this.respuestaServicio);
  this.servicioService.serviceVerificacioMOC(this.setDataRequest()).subscribe((dataResponseVerificacionMOC: any) => {
    if (dataResponseVerificacionMOC.codigoRespuesta === this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCES.CODIGO) {
      // tslint:disable-next-line:max-line-length
      this.setRespuestaServicio(this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCES.CODIGO, this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCES.MENSAJE);
      this.propagarRespuestaServicio.emit(this.respuestaServicio);
      this.siHabilitarBoton();
    } else if (dataResponseVerificacionMOC.codigoRespuesta === this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCESS_WARN.CODIGO) {
        // tslint:disable-next-line:max-line-length
        this.setRespuestaServicio(this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCESS_WARN.CODIGO, dataResponseVerificacionMOC.mensajeRespuesta);
        this.propagarRespuestaServicio.emit(this.respuestaServicio);
        this.siHabilitarBoton();
    } else if (dataResponseVerificacionMOC.codigoRespuesta === '60000' || dataResponseVerificacionMOC.codigoRespuesta === '80003') {
      // tslint:disable-next-line:max-line-length
      this.setRespuestaServicio(this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.ERROR.CODIGO, this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.ERROR.MENSAJE);
      this.propagarRespuestaServicio.emit(this.respuestaServicio);
      this.siHabilitarBoton();
    // tslint:disable-next-line:max-line-length
    } else if ( dataResponseVerificacionMOC.codigoRespuesta === '50002' || dataResponseVerificacionMOC.codigoRespuesta === '70000' || dataResponseVerificacionMOC.codigoRespuesta === '30303' || dataResponseVerificacionMOC.codigoRespuesta === '30101') {
      // tslint:disable-next-line:max-line-length
      this.setRespuestaServicio('', dataResponseVerificacionMOC.mensajeRespuesta);
      this.propagarRespuestaServicio.emit(this.respuestaServicio);
      this.siHabilitarBoton();
    } else if (dataResponseVerificacionMOC.codigoRespuesta === '50001') {
      this.setRespuestaServicio('', 'Documento y/o Dispositivo biométrico no soportan la operación solicitada');
      this.propagarRespuestaServicio.emit(this.respuestaServicio);
      this.siHabilitarBoton();
    } else if (this.validarRespuesta(dataResponseVerificacionMOC.codigoRespuesta)) {
      this.setRespuestaServicio(dataResponseVerificacionMOC.codigoRespuesta, dataResponseVerificacionMOC.mensajeRespuesta );
      this.propagarRespuestaServicio.emit(this.respuestaServicio);
      this.siHabilitarBoton();

    } else {
      this.setRespuestaServicio('', '');
      this.propagarRespuestaServicio.emit(this.respuestaServicio);
      this.siHabilitarBoton();
      this.toast.addToast({ title: 'Error', msg: 'Error de conectividad', timeOut: 5000, type: 'error' });
    }
  }, error => {this.setRespuestaServicio('', '');
  this.siHabilitarBoton();
  this.propagarRespuestaServicio.emit(this.respuestaServicio); this.toast.addToast({ title: 'Error', msg: 'Error de conectividad', timeOut: 5000, type: 'error' }); this.siHabilitarBoton(); });
}

inicializarVerificacionMOCIzquierdo() {
  this.setRespuestaServicio('', this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.MENSAJE_ESPERA);
  this.propagarRespuestaServicio.emit(this.respuestaServicio);
  this.servicioService.serviceVerificacioMOC(this.setDataRequest()).subscribe((dataResponseVerificacionMOC: any) => {
    if (dataResponseVerificacionMOC.codigoRespuesta === this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCES.CODIGO) {
      // tslint:disable-next-line:max-line-length
      this.setRespuestaServicio(this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCES.CODIGO, this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCES.MENSAJE);
      this.propagarRespuestaServicio.emit(this.respuestaServicio);
      this.siHabilitarBoton();
    } else if (dataResponseVerificacionMOC.codigoRespuesta === this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCESS_WARN.CODIGO) {
        // tslint:disable-next-line:max-line-length
        this.setRespuestaServicio(this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.SUCCESS_WARN.CODIGO, dataResponseVerificacionMOC.mensajeRespuesta);
        this.propagarRespuestaServicio.emit(this.respuestaServicio);
        this.siHabilitarBoton();
    } else if (dataResponseVerificacionMOC.codigoRespuesta === '60000') {
      // tslint:disable-next-line:max-line-length
      this.setRespuestaServicio(this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.ERROR.CODIGO, this.constant.CAPTURA_DACTILAR.VERIFICACION_MOC.ERROR.MENSAJE);
      this.propagarRespuestaServicio.emit(this.respuestaServicio);
      this.siHabilitarBoton();

    }  else if (dataResponseVerificacionMOC.codigoRespuesta === '50001') {
      this.setRespuestaServicio('', 'Documento y/o Dispositivo biométrico no soportan la operación solicitada');
      this.propagarRespuestaServicio.emit(this.respuestaServicio);
      this.siHabilitarBoton();
    } else if (this.validarRespuesta(dataResponseVerificacionMOC.codigoRespuesta)) {
      this.setRespuestaServicio(dataResponseVerificacionMOC.codigoRespuesta, dataResponseVerificacionMOC.mensajeRespuesta );
      this.propagarRespuestaServicio.emit(this.respuestaServicio);
      this.siHabilitarBoton();

    } else {
      // tslint:disable-next-line:max-line-length
      this.setRespuestaServicio('', dataResponseVerificacionMOC.mensajeRespuesta);
      this.propagarRespuestaServicio.emit(this.respuestaServicio);
      this.siHabilitarBoton();
    }
  }, error => {this.toast.addToast({ title: 'Error', msg: 'Error de conectividad', timeOut: 5000, type: 'error' }); });
      this.siHabilitarBoton();
}


  setDataRequest() {
    // tslint:disable-next-line:max-line-length
    let valorRequestMOC = '';
    if (this.tipoCaptura === 0) {
      // tslint:disable-next-line:max-line-length
      valorRequestMOC = this.utilitario.generarJsonVerificacionMOC(this.constant.HUELLA.DOCUMENTO_PERSONA, this.formatoTipoCaptura(this.tipoCaptura), this.huellaDerecha, this.nistDerecha);
    } else if (this.tipoCaptura === 1) {
      // tslint:disable-next-line:max-line-length
      valorRequestMOC = this.utilitario.generarJsonVerificacionMOC(this.constant.HUELLA.DOCUMENTO_PERSONA, this.formatoTipoCaptura(this.tipoCaptura), this.huellaIzquierda, this.nistIzquierda);
    } else if (this.tipoCaptura === 2) {
      // tslint:disable-next-line:max-line-length
      valorRequestMOC = this.utilitario.generarJsonVerificacionMOC(this.constant.HUELLA.DOCUMENTO_PERSONA, this.formatoTipoCaptura(this.tipoCaptura), this.huellaDerecha, this.nistDerecha);
    }
    return valorRequestMOC;
  }

  setRespuestaServicio(codigo: String, mensaje: String) {
    this.respuestaServicio.codigoRespuesta = codigo;
    this.respuestaServicio.mensajeRespuesta = mensaje;
}

  formatoTipoCaptura(captura) {
    if (captura === 0) {
       captura = '0104';
    } else if (captura === 1) {
       captura = '0204';
    } else if (captura === 2) {
       captura = '0104';
    }
    return captura;
  }
  setSendRespuesta(codigoR, mensajeR) {
    this.setRespuestaServicio(codigoR, mensajeR);
    this.propagarRespuestaServicio.emit(this.respuestaServicio);
}
habilitarBoton() {
  if (localStorage.getItem('botonMOC') === 'true') {
   return true;
  } else {
    return false;
  }
}
desHabilitarBoton() {
localStorage.setItem('botonMOC', 'false');
}
siHabilitarBoton() {
  localStorage.setItem('botonMOC', 'true');
}
limpiarMensaje() {
  this.setRespuestaServicio(null, null);
  this.propagarRespuestaServicio.emit(this.mensajeVacio);
}

validarRespuesta(c) {
  // tslint:disable-next-line:max-line-length
  if (c === '30906' || c === '30302' || c === '30101' || c === '10302') {
      return true;
  } else {
      return false;
  }
}

}
