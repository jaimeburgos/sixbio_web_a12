import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import {  ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { of, Subject, throwError } from 'rxjs';
import { EnvService } from 'src/app/env.service';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { HuellaService } from '../../serviciosDeUI/huella.service';
import { VerificarComponent } from './verificar.component';

fdescribe('VerificarComponent', () => {
  let component: VerificarComponent;
  let fixture: ComponentFixture<VerificarComponent>;
  // Instanciamos los servicios que utilizaremos
  let servicio: ServicioService;
  let huellaService: HuellaService;
  class FakeRouter {
    navigate(params) {

    }
  }

  class FakeActivatedRoute {
    // params: Observable<any> = Observable.empty();
    private subject = new Subject();

    push(valor) {
      this.subject.next(valor);
    }
    get params() {
      return this.subject.asObservable();
    }
  }
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ VerificarComponent , ToastComponent ],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService,
        HuellaService,
        { provide: Router, useClass: FakeRouter },
        { provide: ToastrService, useClass: ToastrService },
        { provide: ActivatedRoute, useClass: FakeActivatedRoute }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(VerificarComponent);
    // Injectamos nuestro servicio con TestBed.inject
    servicio = TestBed.inject(ServicioService);
    // Injectamos nuestro servicio de huella con TestBed.inject
    huellaService = TestBed.inject(HuellaService);
    component = fixture.componentInstance;
  });


  it('VerificarComponent creado correctamente', () => {
    expect(component).toBeTruthy();
  });

  it('Verificamos la funcion setMensajeErrorInput(bandera: boolean, msgError: String, tipoInput: String) ', () => {
    // Mockeamos los valores de entrada
    const mockBanderaError = true;
    const mockMensajeError = 'Test MSM';
    const mockTipoInput = 'Test tipoInput';
    // Pasamos los valores entrada mockeados
    expect(component.setMensajeErrorInput(mockBanderaError, mockMensajeError, mockTipoInput));
    // Verificamos los valores que se asigne al objeto mensajeErrorInput
    expect(component.mensajeErrorInput.banderaError).toBe(mockBanderaError);
    expect(component.mensajeErrorInput.mensajeError).toEqual(mockMensajeError);
    expect(component.mensajeErrorInput.tipoInput).toEqual(mockTipoInput);
  });
  it('Verificamos el funcionamiento esperado de la funcion salir()', () => {
    const vacio: Object = {  };
    component.dataRequest = vacio;
    // Fakeamos localStorage clear caso contrario tendremos un error por la dependencia entre verificarComponent y verificacionComponent
    spyOn(localStorage, 'clear').and.callFake(() => { 'No hago nada'; });
    // Creamos un espia para el servicio
    const spyService = spyOn(servicio, 'serviceSignOff').and.callThrough();
    // Llamamos nuestra funcion salir() desde el componente
    component.salir();
    // Verificamos que nuestro servicio fuera llamado
    expect(spyService).toHaveBeenCalled();

    // Mockeamos dataResponse para probar la validaciones , el primer caso
    const mockDataResponse = {
      codigoRespuesta : Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    };
    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate').and.callThrough();

    // Fakeamos una llamada al servicio para que nos de el mock de lo que deseamos
    const spyServiceFake = spyOn(servicio, 'serviceSignOff').and.callFake(() => {
      return of (mockDataResponse);
    });

    component.salir();

    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();
    // Verificamos que nuestro servicio Fake fuera llamado
    expect(spyServiceFake).toHaveBeenCalled();


    // Comprobamos la siguiente condicion cuando es diferente de COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    mockDataResponse.codigoRespuesta = 'Diferente de correcto';

    // Volvemos a llamar a la funcion
    component.salir();

    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();


    // Ahora verificamos en caso de un error al llamar al servicio
    const spyError = spyOn(servicio, 'serviceSignOff').and.returnValue(throwError('Esto es un error'));
    // Volvemos a llamar a la funcion
    component.salir();
    // Verificamos que fuera llamado con error nuestro servicio
    expect(spyError).toHaveBeenCalled();
    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();
  });

  it('Verificamos la funcion habilitarBoton()', () => {
    // Seteamos un item botonVerificar con valor 'true' en localStorage
    localStorage.setItem('botonVerificar', 'true');
    // Llamamos nuestra funcion y verificamos que nos devuelva true
    expect(component.habilitarBoton()).toBeTruthy();
    // Para el caso contrario cuando el valor es diferente de 'true'
    localStorage.removeItem('botonVerificar');
    localStorage.setItem('botonVerificar', 'Lo que sea diferente de true');
    // Llamamos nuestra funcion y verificamos que de false
    expect(component.habilitarBoton()).toBeFalsy();
  });

  it('Verificamos la funcion siHabilitarBotonVerificacion()', () => {
    // Llamamos nuestra funcion siHabilitarBotonVerificacion()
    component.siHabilitarBotonVerificacion();
    // Verificamos que setee los items 'botonVerificar' con valor 'true' y 'botonMOC' con valor 'true'
    expect(localStorage.getItem('botonVerificar')).toEqual('true');
    expect(localStorage.getItem('botonMOC')).toEqual('true');
  });

  it('Verificamos la funcion desHabilitarBoton()', () => {
    // Llamamos nuestra funcion desHabilitarBoton()
    component.desHabilitarBoton();
    // Verificamos que setee los items 'botonVerificar' con valor 'false' y 'botonMOC' con valor 'false'
    expect(localStorage.getItem('botonVerificar')).toEqual('false');
    expect(localStorage.getItem('botonMOC')).toEqual('false');
  });

  it('Verificamos la funcion habilitarBotones()', () => {
    // Llamamos nuestra funcion desHabilitarBoton()
    component.habilitarBotones();
    // Verificamos que setee los items 'botonFoto' con valor 'true' y 'botonDatos' con valor 'true'
    expect(localStorage.getItem('botonFoto')).toEqual('true');
    expect(localStorage.getItem('botonDatos')).toEqual('true');
  });



  it('Verificamos la funcion cleanAll()', () => {
    // Espiamos nuestra funcion clearDatosConsolidado()
    const spyclearDatosConsolidado = spyOn(component, 'clearDatosConsolidado').and.callThrough();
    // Espiamos nuestra funcion cleanFirma()
    const spycleanFirma = spyOn(component, 'cleanFirma').and.callThrough();
    // Espiamos nuestra funcion cleanImagen()
    const spycleanImagen = spyOn(component, 'cleanImagen').and.callThrough();
    // Llamamos nuestra funcion clearAll()
    component.cleanAll();
    // Verificamos que nuestros espias fueran llamados
    expect(spyclearDatosConsolidado).toHaveBeenCalled();
    expect(spycleanFirma).toHaveBeenCalled();
    expect(spycleanImagen).toHaveBeenCalled();
    // Verificamos que se limpien las variables
    expect(component.codigoRespuestaServicio).toEqual('');
    expect(component.mensageRespuestaServicio).toEqual('');
    expect(component.isValidMessageServices).toBeFalsy();

  });

  it('Verificamos la funcion cleanMensajeServicio()', () => {
    // Llamamos nuestra funcion
    component.cleanMensajeServicio();
    // Verificamos que se asigne correctamente nuestras variables
    expect(component.codigoRespuestaServicio).toEqual('');
    expect(component.mensageRespuestaServicio).toEqual('');
    expect(component.isValidMessageServices).toBeFalsy();
  });

  it('Verificamos nuestra funcion clearDatosConsolidado()', () => {
    // Llamamos nuestra funcion clearDatosConsolidado()
    component.clearDatosConsolidado();
    // Verificamos que se asignen correctamente nuestras variables
    expect(component.datosConsolidado.apellidos).toEqual('');
    expect(component.datosConsolidado.fechaNacimiento).toEqual('');
    expect(component.datosConsolidado.constanciaVotacionDesc).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoProvDomicilio).toEqual('');
    expect(component.datosConsolidado.estatura).toEqual('');
    expect(component.datosConsolidado.constanciaVotacionCodigo).toEqual('');
    expect(component.datosConsolidado.localidadDomicilio).toEqual('');
    expect(component.datosConsolidado.provinciaNacimiento).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoDistNacimiento).toEqual('');
    expect(component.datosConsolidado.nombres).toEqual('');
    expect(component.datosConsolidado.restricciones).toEqual('');
    expect(component.datosConsolidado.caducidadDescripcion).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoDistDomicilio).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoProvNacimiento).toEqual('');
    expect(component.datosConsolidado.nombrePadre).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoLocalidadNacimiento).toEqual('');
    expect(component.datosConsolidado.numeroLibro).toEqual('');
    expect(component.datosConsolidado.localidad).toEqual('');
    expect(component.datosConsolidado.distritoNacimiento).toEqual('');
    expect(component.datosConsolidado.ubigeoVotacion).toEqual('');
    expect(component.datosConsolidado.dni).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoLocalidadDomicilio).toEqual('');
    expect(component.datosConsolidado.departamentoDomicilio).toEqual('');
    expect(component.datosConsolidado.grupoVotacion).toEqual('');
    expect(component.datosConsolidado.provinciaDomicilio).toEqual('');
    expect(component.datosConsolidado.restriccionesDesc).toEqual('');
    expect(component.datosConsolidado.caducidadCodigo).toEqual('');
    expect(component.datosConsolidado.anioEstudio).toEqual('');
    expect(component.datosConsolidado.departamentoNacimiento).toEqual('');
    expect(component.datosConsolidado.numeroDocSustentarioIdentidad).toEqual('');
    expect(component.datosConsolidado.estadoCivilDescripcion).toEqual('');
    expect(component.datosConsolidado.fechaInscripcion).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoDeptoDomicilio).toEqual('');
    expect(component.datosConsolidado.estadoCivilCodigo).toEqual('');
    expect(component.datosConsolidado.fechaExpedicion).toEqual('');
    expect(component.datosConsolidado.tipoDocSustentarioIdentidad).toEqual('');
    expect(component.datosConsolidado.sexoCodigo).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoDeptoNacimiento).toEqual('');
    expect(component.datosConsolidado.sexoDescripcion).toEqual('');
    expect(component.datosConsolidado.foto).toEqual('assets/images/perfil.png');
    expect(component.datosConsolidado.firma).toEqual('assets/images/firma.jpg');
  });



});
