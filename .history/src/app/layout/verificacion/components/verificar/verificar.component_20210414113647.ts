import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { ServicioService } from '../../../../servicio/servicio.service';
import { ButtonDirective } from 'src/app/my-directive/directive/button/button.directive';
import { VerificacionComponent } from '../../verificacion.component';
import { IObjVerificacion } from 'src/app/nucleo/interface/IObjVerificacion';
import { Constant } from '../../../../nucleo/constante/Constant';
import { Router, ActivatedRoute } from '@angular/router';
import { IUsuarioSCA } from 'src/app/nucleo/interface/IUsuarioSCA';
import { IRespuestaServicio } from 'src/app/nucleo/interface/IRespuestaServicio';
import { Utilitario } from 'src/app/nucleo/util/Utilitario';
import { HuellaService } from '../../serviciosDeUI/huella.service';
import { IDatosHuellaDerecha } from 'src/app/nucleo/interface/IDatosHuellaDerecha';
import { IDatosHuellaIzquierda } from 'src/app/nucleo/interface/IDatosHuellaIzquierda';
import { IResponseDatosConsolidado } from 'src/app/nucleo/interface/IResponseDatosConsolidado';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { HeaderComponent } from 'src/app/layout/components/header/header.component';
import { ITipoVerificacion } from 'src/app/nucleo/interface/ITipoVerificacion';
import { IErrorValidador } from 'src/app/nucleo/interface/IErrorValidador';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';

@Component({
    selector: 'app-verificar',
    templateUrl: './verificar.component.html',
    styleUrls: ['./verificar.component.scss']
})
export class VerificarComponent implements OnInit {

    constant: Constant;
    utilitario: Utilitario;
    headerComponent: HeaderComponent;
    data = {};
    verificacionComponent: VerificacionComponent;
    @ViewChild(ButtonDirective, { static: true })
    buttonDirective = null;
    @ViewChild(ToastComponent, { static: true }) toast: ToastComponent;
    @Input()
    dniConsultor;

    @Input()
    tipoVerificacion;
    @Input()
    tipoCaptura;

    @Input()
    dniValidador;

    @Output()
    propagarRespuestaServicio = new EventEmitter<IRespuestaServicio>();

    @Output()
    propagarDatos = new EventEmitter<IResponseDatosConsolidado>();

    @Output()
    propagarMensajeErrorInput = new EventEmitter<IErrorValidador>();

    mensajeErrorInput: IErrorValidador = {
        banderaError: false,
        mensajeError: '',
        tipoInput: ''
    };
    dataRequest: IUsuarioSCA = {
        apPaterno: '',
        apMaterno: '',
        nombre: '',
        tipoDoc: '',
        numDoc: '',
        telef: '',
        correo: '',
        fecNac: '',
        usuario: '',
        estado: '',
        rol: '',
        audFecModif: '',
        audFecCreac: '',
        audUsuModif: '',
        audUsuCreac: '',
        navigationId: 0
    };
    mensageRespuestaServicio: String = '';

    codigoRespuestaServicio: String = '';
    respuestaServicio: IRespuestaServicio = {
        codigoRespuesta: '',
        mensajeRespuesta: ''
    };

    isDatosConsolidado = true;
    isDniValidatorValid = false;

    isDniConsultorValid = false;

    isValidDedoDerecho = true;
    isValidDedoIzquierdo: boolean;

    isValidMessageServices = false;
    datosConsolidadoLimpio: IResponseDatosConsolidado = {
        apellidos: '',
        fechaNacimiento: '',
        constanciaVotacionDesc: '',
        nombreMadre: '',
        codigoUbigeoProvDomicilio: '',
        direccion: '',
        estatura: '',
        constanciaVotacionCodigo: '',
        localidadDomicilio: '',
        provinciaNacimiento: '',
        codigoUbigeoDistNacimiento: '',
        nombres: '',
        restricciones: '',
        caducidadDescripcion: '',
        codigoUbigeoDistDomicilio: '',
        codigoUbigeoProvNacimiento: '',
        nombrePadre: '',
        codigoUbigeoLocalidadNacimiento: '',
        numeroLibro: '',
        localidad: '',
        distritoNacimiento: '',
        ubigeoVotacion: '',
        dni: '',
        codigoUbigeoLocalidadDomicilio: '',
        departamentoDomicilio: '',
        grupoVotacion: '',
        provinciaDomicilio: '',
        restriccionesDesc: '',
        caducidadCodigo: '',
        anioEstudio: '',
        departamentoNacimiento: '',
        numeroDocSustentarioIdentidad: '',
        estadoCivilDescripcion: '',
        fechaInscripcion: '',
        codigoUbigeoDeptoDomicilio: '',
        estadoCivilCodigo: '',
        fechaExpedicion: '',
        tipoDocSustentarioIdentidad: '',
        sexoCodigo: '',
        codigoUbigeoDeptoNacimiento: '',
        sexoDescripcion: '',
        foto: 'assets/images/perfil.png',
        firma: 'assets/images/firma.jpg'
    };

    datosConsolidado: IResponseDatosConsolidado = {
        apellidos: '',
        fechaNacimiento: '',
        constanciaVotacionDesc: '',
        nombreMadre: '',
        codigoUbigeoProvDomicilio: '',
        direccion: '',
        estatura: '',
        constanciaVotacionCodigo: '',
        localidadDomicilio: '',
        provinciaNacimiento: '',
        codigoUbigeoDistNacimiento: '',
        nombres: '',
        restricciones: '',
        caducidadDescripcion: '',
        codigoUbigeoDistDomicilio: '',
        codigoUbigeoProvNacimiento: '',
        nombrePadre: '',
        codigoUbigeoLocalidadNacimiento: '',
        numeroLibro: '',
        localidad: '',
        distritoNacimiento: '',
        ubigeoVotacion: '',
        dni: '',
        codigoUbigeoLocalidadDomicilio: '',
        departamentoDomicilio: '',
        grupoVotacion: '',
        provinciaDomicilio: '',
        restriccionesDesc: '',
        caducidadCodigo: '',
        anioEstudio: '',
        departamentoNacimiento: '',
        numeroDocSustentarioIdentidad: '',
        estadoCivilDescripcion: '',
        fechaInscripcion: '',
        codigoUbigeoDeptoDomicilio: '',
        estadoCivilCodigo: '',
        fechaExpedicion: '',
        tipoDocSustentarioIdentidad: '',
        sexoCodigo: '',
        codigoUbigeoDeptoNacimiento: '',
        sexoDescripcion: '',
        foto: 'assets/images/perfil.png',
        firma: 'assets/images/firma.jpg'
    };

    foto: String = 'assets/images/perfil.png';
    firma: String = 'assets/images/firma.jpg';
    huellaDerechaTemplate = '';
    huellaIzquierdaTemplate = '';
    huellaDerechaWSQ = '';
    huellaIzquierdaWSQ = '';
    huellaDerechaTrama = '';
    huellaIzquierdaTrama = '';
    nistDerecha: String = '';
    nistIzquierda: String = '';
    huellaVivaIzquierda: String = '';
    huellaVivaDerecha: String = '';
    imagenHuellaVivaIzquierda: String = 'assets/images/blank2.png';
    imagenHuellaVivaDerecha: String = 'assets/images/blank2.png';
    imagenNistIzquierda: String = 'assets/images/blank2.png';
    imagenNistDerecha: String = 'assets/images/blank2.png';
    msgError: String = '';
    huellaIzquierdaCompaCard = '';
    huellaDerechaCompaCard = '';

    datosHuellaDerecha: IDatosHuellaDerecha = {
        huellaD: '',
        huellaCompaCardD: '',
        huellaWSQD: '',
        huellaTemplateD: '',
        nistD: '',
        huellavivaD: '',
        imagenNistD: '',
        imagenhuellavivaD: ''
    };
    datosHuellaIzquierda: IDatosHuellaIzquierda = {
        huellaI: '',
        huellaCompaCardI: '',
        huellaWSQI: '',
        huellaTemplateI: '',
        nistI: '',
        huellavivaI: '',
        imagenNistI: '',
        imagenhuellavivaI: ''
    };

    // tslint:disable-next-line:max-line-length
    constructor(private servicioService: ServicioService, private router: Router, private rutaActual: ActivatedRoute, private huellaService: HuellaService) {
        this.constant = new Constant();
        this.utilitario = new Utilitario();

    }
    ngOnInit() {
        this.huellaService.huellaDerecha.subscribe(huellaDerechaCapturada => {
            this.huellaDerechaTemplate = huellaDerechaCapturada.huellaTemplateD;
            this.huellaDerechaWSQ = huellaDerechaCapturada.huellaWSQD;
            this.nistDerecha = huellaDerechaCapturada.nistD;
            this.huellaVivaDerecha = this.convertirFakeDetection(huellaDerechaCapturada.huellavivaD);
        });
        this.huellaService.huellaIzquierda.subscribe(huellaIzquierdaCapturada => {
            this.huellaIzquierdaTemplate = huellaIzquierdaCapturada.huellaTemplateI;
            this.huellaIzquierdaWSQ = huellaIzquierdaCapturada.huellaWSQI;
            this.nistIzquierda = huellaIzquierdaCapturada.nistI;
            this.huellaVivaIzquierda = this.convertirFakeDetection(huellaIzquierdaCapturada.huellavivaI);
        });
        /*     this.entidad.tipoCaptura = this.tipoCaptura;
            this.utilitario = new Utilitario(); */
    }

    servicioVerificacionDactilar() {
        this.emitirValidacionInput(false, '', '');
        this.setSendRespuesta(null, 'cleanFront');
        this.cleanFirma();
        this.cleanImagen();
        this.cleanMensajeServicio();
        if (this.isValidInputs()) {
            this.executeServiceVerificacionBiometrica();
        } else {
            this.emitirMensajeRespuestaServicio(Constant.CODIGO_VACIO, Constant.MENSAJE_VACIO);
        }

    }

    executeServiceVerificacionBiometrica() {
        this.desHabilitarBoton();
        this.clearDatosConsolidado();
        this.setSendRespuesta('', this.constant.VERIFICACION_BIOMETRICA.MENSAJE_ESPERA);
        this.servicioService.serviceVerificacionBiometrica(this.setDataRequest()).subscribe((dataResponse: any) => {
            if (dataResponse.codigoRespuesta === Constant.COD_ERROR_SESION) {
                this.salir();
                return;
            }
            if (dataResponse.codigoRespuesta === this.constant.VERIFICACION_BIOMETRICA.RENIEC.SUCCESS_WARN.CODIGO ||
                dataResponse.codigoRespuesta === this.constant.VERIFICACION_BIOMETRICA.RENIEC.SUCCES.CODIGO ||
                dataResponse.codigoRespuesta === this.constant.VERIFICACION_BIOMETRICA.LOCAL.SUCCES.CODIGO ||
                dataResponse.codigoRespuesta === this.constant.VERIFICACION_BIOMETRICA.LOCAL.SUCCESS_WARN.CODIGO) {
                this.setSendRespuesta(dataResponse.codigoRespuesta, dataResponse.mensajeRespuesta);
                this.setDatosResponseWithObjtDatosConsolidado(dataResponse);
                this.propagarDatos.emit(this.datosConsolidado);
                this.siHabilitarBotonVerificacion();
                if (dataResponse.foto !== null) {
                    this.setFoto(dataResponse);
                    this.propagarDatos.emit(this.datosConsolidado);
                    this.siHabilitarBotonVerificacion();
                }
                if (dataResponse.codigoRespuesta === this.constant.VERIFICACION_BIOMETRICA.RENIEC.SUCCES.CODIGO) {
                    this.setSendRespuesta(this.constant.VERIFICACION_BIOMETRICA.RENIEC.SUCCES.CODIGO, this.constant.VERIFICACION_BIOMETRICA.RENIEC.SUCCES.MENSAJE);
                    this.cleanImagen();
                    this.siHabilitarBotonVerificacion();
                    this.habilitarBotones();
                } else if (dataResponse.codigoRespuesta === this.constant.VERIFICACION_BIOMETRICA.LOCAL.SUCCES.CODIGO) {
                    this.setSendRespuesta(this.constant.VERIFICACION_BIOMETRICA.LOCAL.SUCCES.CODIGO, this.constant.VERIFICACION_BIOMETRICA.LOCAL.SUCCES.MENSAJE);
                    this.habilitarBotones();
                    this.siHabilitarBotonVerificacion();
                } else if (dataResponse.codigoRespuesta === this.constant.VERIFICACION_BIOMETRICA.RENIEC.SUCCESS_WARN.CODIGO) {
                    this.setSendRespuesta(this.constant.VERIFICACION_BIOMETRICA.RENIEC.SUCCESS_WARN.CODIGO, this.constant.VERIFICACION_BIOMETRICA.RENIEC.SUCCESS_WARN.MENSAJE);
                } else if (dataResponse.codigoRespuesta === this.constant.VERIFICACION_BIOMETRICA.LOCAL.SUCCESS_WARN.CODIGO) {
                    this.setSendRespuesta(this.constant.VERIFICACION_BIOMETRICA.LOCAL.SUCCESS_WARN.CODIGO, this.constant.VERIFICACION_BIOMETRICA.LOCAL.SUCCESS_WARN.MENSAJE);
                } else {
                    this.setSendRespuesta(dataResponse.codigoRespuesta, this.utilitario.encodeMensaje(dataResponse.mensajeRespuesta));
                    this.siHabilitarBotonVerificacion();
                }
            } else if (dataResponse.codigoRespuesta === this.constant.CONSTANTE.ERROR_SESION.CODIGO) {
                setTimeout(() => {this.salir(); }, 500);
                // redirect inicio
            } else if (dataResponse.codigoRespuesta === '15000' || dataResponse.codigoRespuesta === '00101' || dataResponse.codigoRespuesta === '00114' || dataResponse.codigoRespuesta === '00113' || dataResponse.codigoRespuesta === '00101') {
                this.setSendRespuesta(null, 'cleanFront');
                this.cleanFirma();
                this.desHabilitarBoton();
                this.setSendRespuesta(dataResponse.codigoRespuesta, this.utilitario.encodeMensaje(dataResponse.mensajeRespuesta));
            } else {
                this.setSendRespuesta('', '');
                this.toast.addToast({ title: 'Error', msg: 'Error de conectividad', timeOut: 5000, type: 'error' });
                setTimeout(() => {this.salir(); }, 500);
            }
        }, (error) => {
            this.setSendRespuesta(this.constant.CONSTANTE.OPERACION_ERROR.CODIGO, this.constant.CONSTANTE.OPERACION_ERROR.MENSAJE);
            setTimeout(() => {this.salir(); }, 500);
        });
    }




    setDataRequest() {
        let valorRequestVerificacionBiometrica: Object;
        // tslint:disable-next-line:max-line-length
        valorRequestVerificacionBiometrica = this.utilitario.generarJsonVerificacionBiometricaRequest(this.validarVerificacion(JSON.parse(localStorage.getItem('datosSesion'))._serviciosVerificacion.default), this.dniConsultor, this.tipoCaptura, this.huellaDerechaWSQ, this.huellaDerechaTemplate, this.nistDerecha, this.huellaVivaDerecha, this.huellaIzquierdaWSQ, this.huellaIzquierdaTemplate, this.nistIzquierda, this.huellaVivaIzquierda);

        return valorRequestVerificacionBiometrica;

    }

    setRespuestaServicio(codigo: String, mensaje: String) {
        this.respuestaServicio.codigoRespuesta = codigo;
        this.respuestaServicio.mensajeRespuesta = mensaje;
    }

    setSendRespuesta(codigoR, mensajeR) {
        this.setRespuestaServicio(codigoR, mensajeR);
        this.propagarRespuestaServicio.emit(this.respuestaServicio);
    }
    convertirFakeDetection(fakeDetection) {
        if (fakeDetection != null) {
            fakeDetection = fakeDetection.replace(',', '.');
        } else {
            fakeDetection = '0';
        }
        const newFakeDetection = fakeDetection;
        return newFakeDetection;
    }

    validarVerificacion(verificacionType) {
        if (verificacionType === '' || verificacionType === null || verificacionType === '301001') {
            verificacionType = '301001';
            return verificacionType;
        } else {
            return verificacionType;
        }

    }

    habilitarBoton() {
        if (localStorage.getItem('botonVerificar') === 'true') {
            return true;
        } else {
            return false;
        }
    }
    siHabilitarBotonVerificacion() {
        localStorage.setItem('botonVerificar', 'true');
        localStorage.setItem('botonMOC', 'true');
    }
    desHabilitarBoton() {
        localStorage.setItem('botonVerificar', 'false');
        localStorage.setItem('botonMOC', 'false');
    }
    habilitarBotones() {
        localStorage.setItem('botonFoto', 'true');
        localStorage.setItem('botonDatos', 'true');
    }

    ////////////// CLEAN//////////////////////////

    cleanAll() {
        this.codigoRespuestaServicio = '';
        this.mensageRespuestaServicio = '';
        this.isValidMessageServices = false;
        this.clearDatosConsolidado();
        this.cleanImagen();
        this.cleanFirma();
    }

    cleanMensajeServicio() {
        this.codigoRespuestaServicio = '';
        this.mensageRespuestaServicio = '';
        this.isValidMessageServices = false;
    }


    clearDatosConsolidado() {
        this.datosConsolidado.apellidos = '';
        this.datosConsolidado.fechaNacimiento = '';
        this.datosConsolidado.constanciaVotacionDesc = '';
        this.datosConsolidado.nombreMadre = '';
        this.datosConsolidado.codigoUbigeoProvDomicilio = '';
        this.datosConsolidado.direccion = '';
        this.datosConsolidado.estatura = '';
        this.datosConsolidado.constanciaVotacionCodigo = '';
        this.datosConsolidado.localidadDomicilio = '';
        this.datosConsolidado.provinciaNacimiento = '';
        this.datosConsolidado.codigoUbigeoDistNacimiento = '';
        this.datosConsolidado.nombres = '';
        this.datosConsolidado.restricciones = '';
        this.datosConsolidado.caducidadDescripcion = '';
        this.datosConsolidado.codigoUbigeoDistDomicilio = '';
        this.datosConsolidado.codigoUbigeoProvNacimiento = '';
        this.datosConsolidado.nombrePadre = '';
        this.datosConsolidado.codigoUbigeoLocalidadNacimiento = '';
        this.datosConsolidado.numeroLibro = '';
        this.datosConsolidado.localidad = '';
        this.datosConsolidado.distritoNacimiento = '';
        this.datosConsolidado.ubigeoVotacion = '';
        this.datosConsolidado.dni = '';
        this.datosConsolidado.codigoUbigeoLocalidadDomicilio = '';
        this.datosConsolidado.departamentoDomicilio = '';
        this.datosConsolidado.grupoVotacion = '';
        this.datosConsolidado.provinciaDomicilio = '';
        this.datosConsolidado.restriccionesDesc = '';
        this.datosConsolidado.caducidadCodigo = '';
        this.datosConsolidado.anioEstudio = '';
        this.datosConsolidado.departamentoNacimiento = '';
        this.datosConsolidado.numeroDocSustentarioIdentidad = '';
        this.datosConsolidado.estadoCivilDescripcion = '';
        this.datosConsolidado.fechaInscripcion = '';
        this.datosConsolidado.codigoUbigeoDeptoDomicilio = '';
        this.datosConsolidado.estadoCivilCodigo = '';
        this.datosConsolidado.fechaExpedicion = '';
        this.datosConsolidado.tipoDocSustentarioIdentidad = '';
        this.datosConsolidado.sexoCodigo = '';
        this.datosConsolidado.codigoUbigeoDeptoNacimiento = '';
        this.datosConsolidado.sexoDescripcion = '';
        this.datosConsolidado.foto = 'assets/images/perfil.png';
        this.datosConsolidado.firma = 'assets/images/firma.jpg';
        this.propagarDatos.emit(this.datosConsolidado);
    }

    cleanImagen() {
        this.foto = 'assets/images/perfil.png';
    }

    cleanFirma() {
        this.firma = 'assets/images/firma.jpg';
    }
    /////// MOSTRAR DATOS ///////////////
    setDatosResponseWithObjtDatosConsolidado(response: any): void {
        this.datosConsolidado.apellidos = '';
        this.datosConsolidado.fechaExpedicion = '';
        // tslint:disable-next-line:max-line-length
        if ((response.apellidoPaterno !== '' && response.apellidoPaterno !== 'null' && response.apellidoPaterno !== null) && (response.apellidoMaterno !== 'null' && response.apellidoMaterno !== '' && response.apellidoMaterno !== null)) {
            this.datosConsolidado.apellidos = response.apellidoPaterno + ' ' + response.apellidoMaterno;
        }
       /*  this.datosConsolidado.fechaNacimiento = this.formatearFecha(response.fechaNacimiento); */
        /* this.datosConsolidado.direccion = response.direccionDomicilio + ' / ' + response.distritoDomicilio; */
        /* this.datosConsolidado.estatura = response.estatura; */
        this.datosConsolidado.nombres = response.nombres;
        this.datosConsolidado.caducidadDescripcion = response.fechaCaducidad;
        this.datosConsolidado.dni = response.numeroDocumento;
/*         this.datosConsolidado.estadoCivilDescripcion = response.estadoCivilDescripcion; */
       /*  this.datosConsolidado.estadoCivilCodigo = response.estadoCivilCodigo; */
        this.datosConsolidado.fechaExpedicion = this.formatearFecha(response.fechaEmision);
        /* this.datosConsolidado.sexoDescripcion = response.sexoDescripcion; */
        /* if (response.firma !== null) {
            this.datosConsolidado.firma = 'data:image/png;base64,' + response.firma;
        } else {
            this.cleanFirma();
        } */
    }

    setFoto(response: any): void {
        this.datosConsolidado.foto = 'data:image/png;base64,' + response.foto;
    }

    formatearFecha(fecha: String): String {
        const year = fecha.substr(4, 7);
        const month = fecha.substr(2, 2);
        const day = fecha.substr(0, 2);
        const fechaFormateada = [day, month, year].filter(Boolean).join('-');
        return fechaFormateada;
    }

    requiereDniValidador(): boolean {
        if (localStorage.getItem('isWithDniValidator') !== null && localStorage.getItem('isWithDniValidator') === 'true') {
            return true;
        } else {
            return false;
        }
    }

    isValidInputs(): boolean {

        // Validacion del input dniValidador
        if (this.requiereDniValidador()) {
            if (this.isEmpty(this.dniValidador)) {
                this.emitirValidacionInput(true, Constant.MENSAJE_ERROR_DNI_VALIDADOR_VACIO, 'dniValidador');
                return false;
            } else if (this.isInvalidRegexInputDni(this.dniValidador)) {
                this.emitirValidacionInput(true, Constant.MENSAJE_ERROR_DNI_VALIDADOR_INVALIDO, 'dniValidador');
                return false;
            } else {
                this.emitirValidacionInput(false, '', 'dniValidador');
            }
        }

        // Validacion del input dniConsultor
        if (this.isEmpty(this.dniConsultor)) {
            this.emitirValidacionInput(true, Constant.MENSAJE_ERROR_DNI_CONSULTOR_VACIO, 'dniConsultor');
            return false;
        } else if (this.isInvalidRegexInputDni(this.dniConsultor)) {
            this.emitirValidacionInput(true, Constant.MENSAJE_ERROR_DNI_CONSULTOR_INVALIDO, 'dniConsultor');
            return false;
        } else {
            this.emitirValidacionInput(false, '', 'dniConsultor');
        }

        return true;

    }

    isEmpty(param: any): boolean {
        const dni: string = param;
        if (dni.length === 0) {
            return true;
        }
        return false;
    }

    isInvalidRegexInputDni(param: any): boolean {
        const dni: string = param;
        const regexpNumber = new RegExp('^[+0-9]{8}$');
        if (!regexpNumber.test(dni)) {
            return true;
        }
        return false;
    }

    emitirValidacionInput(bandera: boolean, mensaje: string, tipoInput: string) {
        this.setMensajeErrorInput(bandera, mensaje, tipoInput);
        this.propagarMensajeErrorInput.emit(this.mensajeErrorInput);
    }

    setMensajeErrorInput(bandera: boolean, msgError: String, tipoInput: String) {
        this.mensajeErrorInput.banderaError = bandera;
        this.mensajeErrorInput.mensajeError = msgError;
        this.mensajeErrorInput.tipoInput = tipoInput;
    }

    emitirMensajeRespuestaServicio(codigo: String, mensaje: String) {
        this.setRespuestaServicio(codigo, mensaje);
        this.propagarRespuestaServicio.emit(this.respuestaServicio);
    }
    respuestaExtra(cod, men) {
    }
    salir() {
        this.servicioService.serviceSignOff(this.dataRequest).subscribe((dataResponse: any) => {

            if (dataResponse.codigoRespuesta === Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO) {
                localStorage.clear();
                this.router.navigate(['/login']);
            } else {

                localStorage.clear();
                this.router.navigate(['/login']);

            }
        }
            , error => {
                localStorage.clear();
                this.router.navigate(['/login']);
            });
    }

}
