import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import {  ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { of, Subject, throwError } from 'rxjs';
import { EnvService } from 'src/app/env.service';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { HuellaService } from '../../serviciosDeUI/huella.service';
import { VerificarComponent } from './verificar.component';

fdescribe('VerificarComponent', () => {
  let component: VerificarComponent;
  let fixture: ComponentFixture<VerificarComponent>;
  // Instanciamos los servicios que utilizaremos
  let translateService: TranslateService;
  let servicio: ServicioService;
  let huellaService: HuellaService;
  class FakeRouter {
    navigate(params) {

    }
  }

  class FakeActivatedRoute {
    // params: Observable<any> = Observable.empty();
    private subject = new Subject();

    push(valor) {
      this.subject.next(valor);
    }
    get params() {
      return this.subject.asObservable();
    }
  }
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ VerificarComponent , ToastComponent ],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService,
        HuellaService,
        { provide: Router, useClass: FakeRouter },
        { provide: ToastrService, useClass: ToastrService },
        { provide: ActivatedRoute, useClass: FakeActivatedRoute }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(VerificarComponent);
    // Injectamos nuestro servicio con TestBed.inject
    servicio = TestBed.inject(ServicioService);
    // Injectamos nuestro servicio de huella con TestBed.inject
    huellaService = TestBed.inject(HuellaService);
    component = fixture.componentInstance;
  });


  it('VerificarComponent creado correctamente', () => {
    expect(component).toBeTruthy();
  });

  it('Verificamos la funcion setMensajeErrorInput(bandera: boolean, msgError: String, tipoInput: String) ', () => {
    // Mockeamos los valores de entrada
    const mockBanderaError = true;
    const mockMensajeError = 'Test MSM';
    const mockTipoInput = 'Test tipoInput';
    // Pasamos los valores entrada mockeados
    expect(component.setMensajeErrorInput(mockBanderaError, mockMensajeError, mockTipoInput));
    // Verificamos los valores que se asigne al objeto mensajeErrorInput
    expect(component.mensajeErrorInput.banderaError).toBe(mockBanderaError);
    expect(component.mensajeErrorInput.mensajeError).toEqual(mockMensajeError);
    expect(component.mensajeErrorInput.tipoInput).toEqual(mockTipoInput);
  });
  it('Verificamos el funcionamiento esperado de la funcion salir()', () => {
    const vacio: Object = {  };
    component.dataRequest = vacio;
    // Fakeamos localStorage clear caso contrario tendremos un error por la dependencia entre verificarComponent y verificacionComponent
    spyOn(localStorage, 'clear').and.callFake(() => { 'No hago nada'; });
    // Creamos un espia para el servicio
    const spyService = spyOn(servicio, 'serviceSignOff').and.callThrough();
    // Llamamos nuestra funcion salir() desde el componente
    component.salir();
    // Verificamos que nuestro servicio fuera llamado
    expect(spyService).toHaveBeenCalled();

    // Mockeamos dataResponse para probar la validaciones , el primer caso
    const mockDataResponse = {
      codigoRespuesta : Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    };
    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate').and.callThrough();

    // Fakeamos una llamada al servicio para que nos de el mock de lo que deseamos
    const spyServiceFake = spyOn(servicio, 'serviceSignOff').and.callFake(() => {
      return of (mockDataResponse);
    });

    component.salir();

    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();
    // Verificamos que nuestro servicio Fake fuera llamado
    expect(spyServiceFake).toHaveBeenCalled();


    // Comprobamos la siguiente condicion cuando es diferente de COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    mockDataResponse.codigoRespuesta = 'Diferente de correcto';

    // Volvemos a llamar a la funcion
    component.salir();

    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();


    // Ahora verificamos en caso de un error al llamar al servicio
    const spyError = spyOn(servicio, 'serviceSignOff').and.returnValue(throwError('Esto es un error'));
    // Volvemos a llamar a la funcion
    component.salir();
    // Verificamos que fuera llamado con error nuestro servicio
    expect(spyError).toHaveBeenCalled();
    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();
  });

});
