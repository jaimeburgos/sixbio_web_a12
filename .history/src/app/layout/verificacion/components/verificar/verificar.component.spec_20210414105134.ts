import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import {  ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateService, TranslateModule } from '@ngx-translate/core';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { of, Subject, throwError } from 'rxjs';
import { EnvService } from 'src/app/env.service';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { IDatosHuellaDerecha } from 'src/app/nucleo/interface/IDatosHuellaDerecha';
import { IDatosHuellaIzquierda } from 'src/app/nucleo/interface/IDatosHuellaIzquierda';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { HuellaService } from '../../serviciosDeUI/huella.service';
import { VerificarComponent } from './verificar.component';

fdescribe('VerificarComponent', () => {



  let component: VerificarComponent;
  let fixture: ComponentFixture<VerificarComponent>;
  // Instanciamos los servicios que utilizaremos
  let servicio: ServicioService;
  let huellaService: HuellaService;
  class FakeRouter {
    navigate(params) {

    }
  }

  class FakeActivatedRoute {
    // params: Observable<any> = Observable.empty();
    private subject = new Subject();

    push(valor) {
      this.subject.next(valor);
    }
    get params() {
      return this.subject.asObservable();
    }
  }
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ VerificarComponent , ToastComponent ],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService,
        HuellaService,
        { provide: Router, useClass: FakeRouter },
        { provide: ToastrService, useClass: ToastrService },
        { provide: ActivatedRoute, useClass: FakeActivatedRoute }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  });


  beforeEach(() => {
    fixture = TestBed.createComponent(VerificarComponent);
    // Injectamos nuestro servicio con TestBed.inject
    servicio = TestBed.inject(ServicioService);
    // Injectamos nuestro servicio de huella con TestBed.inject
    huellaService = TestBed.inject(HuellaService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });


  it('VerificarComponent creado correctamente', () => {
    expect(component).toBeTruthy();
  });

  it('Verificamos la funcion setMensajeErrorInput(bandera: boolean, msgError: String, tipoInput: String) ', () => {
    // Mockeamos los valores de entrada
    const mockBanderaError = true;
    const mockMensajeError = 'Test MSM';
    const mockTipoInput = 'Test tipoInput';
    // Pasamos los valores entrada mockeados
    expect(component.setMensajeErrorInput(mockBanderaError, mockMensajeError, mockTipoInput));
    // Verificamos los valores que se asigne al objeto mensajeErrorInput
    expect(component.mensajeErrorInput.banderaError).toBe(mockBanderaError);
    expect(component.mensajeErrorInput.mensajeError).toEqual(mockMensajeError);
    expect(component.mensajeErrorInput.tipoInput).toEqual(mockTipoInput);
  });
  it('Verificamos el funcionamiento esperado de la funcion salir()', () => {
    const vacio: Object = {  };
    component.dataRequest = vacio;
    // Fakeamos localStorage clear caso contrario tendremos un error por la dependencia entre verificarComponent y verificacionComponent
    spyOn(localStorage, 'clear').and.callFake(() => { 'No hago nada'; });
    // Creamos un espia para el servicio
    const spyService = spyOn(servicio, 'serviceSignOff').and.callThrough();
    // Llamamos nuestra funcion salir() desde el componente
    component.salir();
    // Verificamos que nuestro servicio fuera llamado
    expect(spyService).toHaveBeenCalled();

    // Mockeamos dataResponse para probar la validaciones , el primer caso
    const mockDataResponse = {
      codigoRespuesta : Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    };
    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate').and.callThrough();

    // Fakeamos una llamada al servicio para que nos de el mock de lo que deseamos
    const spyServiceFake = spyOn(servicio, 'serviceSignOff').and.callFake(() => {
      return of (mockDataResponse);
    });

    component.salir();

    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();
    // Verificamos que nuestro servicio Fake fuera llamado
    expect(spyServiceFake).toHaveBeenCalled();


    // Comprobamos la siguiente condicion cuando es diferente de COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    mockDataResponse.codigoRespuesta = 'Diferente de correcto';

    // Volvemos a llamar a la funcion
    component.salir();

    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();


    // Ahora verificamos en caso de un error al llamar al servicio
    const spyError = spyOn(servicio, 'serviceSignOff').and.returnValue(throwError('Esto es un error'));
    // Volvemos a llamar a la funcion
    component.salir();
    // Verificamos que fuera llamado con error nuestro servicio
    expect(spyError).toHaveBeenCalled();
    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();
  });

  it('Verificamos la funcion habilitarBoton()', () => {
    // Seteamos un item botonVerificar con valor 'true' en localStorage
    localStorage.setItem('botonVerificar', 'true');
    // Llamamos nuestra funcion y verificamos que nos devuelva true
    expect(component.habilitarBoton()).toBeTruthy();
    // Para el caso contrario cuando el valor es diferente de 'true'
    localStorage.removeItem('botonVerificar');
    localStorage.setItem('botonVerificar', 'Lo que sea diferente de true');
    // Llamamos nuestra funcion y verificamos que de false
    expect(component.habilitarBoton()).toBeFalsy();
  });

  it('Verificamos la funcion siHabilitarBotonVerificacion()', () => {
    // Llamamos nuestra funcion siHabilitarBotonVerificacion()
    component.siHabilitarBotonVerificacion();
    // Verificamos que setee los items 'botonVerificar' con valor 'true' y 'botonMOC' con valor 'true'
    expect(localStorage.getItem('botonVerificar')).toEqual('true');
    expect(localStorage.getItem('botonMOC')).toEqual('true');
  });

  it('Verificamos la funcion desHabilitarBoton()', () => {
    // Llamamos nuestra funcion desHabilitarBoton()
    component.desHabilitarBoton();
    // Verificamos que setee los items 'botonVerificar' con valor 'false' y 'botonMOC' con valor 'false'
    expect(localStorage.getItem('botonVerificar')).toEqual('false');
    expect(localStorage.getItem('botonMOC')).toEqual('false');
  });

  it('Verificamos la funcion habilitarBotones()', () => {
    // Llamamos nuestra funcion desHabilitarBoton()
    component.habilitarBotones();
    // Verificamos que setee los items 'botonFoto' con valor 'true' y 'botonDatos' con valor 'true'
    expect(localStorage.getItem('botonFoto')).toEqual('true');
    expect(localStorage.getItem('botonDatos')).toEqual('true');
  });



  it('Verificamos la funcion cleanAll()', () => {
    // Espiamos nuestra funcion clearDatosConsolidado()
    const spyclearDatosConsolidado = spyOn(component, 'clearDatosConsolidado').and.callThrough();
    // Espiamos nuestra funcion cleanFirma()
    const spycleanFirma = spyOn(component, 'cleanFirma').and.callThrough();
    // Espiamos nuestra funcion cleanImagen()
    const spycleanImagen = spyOn(component, 'cleanImagen').and.callThrough();
    // Llamamos nuestra funcion clearAll()
    component.cleanAll();
    // Verificamos que nuestros espias fueran llamados
    expect(spyclearDatosConsolidado).toHaveBeenCalled();
    expect(spycleanFirma).toHaveBeenCalled();
    expect(spycleanImagen).toHaveBeenCalled();
    // Verificamos que se limpien las variables
    expect(component.codigoRespuestaServicio).toEqual('');
    expect(component.mensageRespuestaServicio).toEqual('');
    expect(component.isValidMessageServices).toBeFalsy();

  });

  it('Verificamos la funcion cleanMensajeServicio()', () => {
    // Llamamos nuestra funcion
    component.cleanMensajeServicio();
    // Verificamos que se asigne correctamente nuestras variables
    expect(component.codigoRespuestaServicio).toEqual('');
    expect(component.mensageRespuestaServicio).toEqual('');
    expect(component.isValidMessageServices).toBeFalsy();
  });

  it('Verificamos nuestra funcion clearDatosConsolidado()', () => {
    // Llamamos nuestra funcion clearDatosConsolidado()
    component.clearDatosConsolidado();
    // Verificamos que se asignen correctamente nuestras variables
    expect(component.datosConsolidado.apellidos).toEqual('');
    expect(component.datosConsolidado.fechaNacimiento).toEqual('');
    expect(component.datosConsolidado.constanciaVotacionDesc).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoProvDomicilio).toEqual('');
    expect(component.datosConsolidado.estatura).toEqual('');
    expect(component.datosConsolidado.constanciaVotacionCodigo).toEqual('');
    expect(component.datosConsolidado.localidadDomicilio).toEqual('');
    expect(component.datosConsolidado.provinciaNacimiento).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoDistNacimiento).toEqual('');
    expect(component.datosConsolidado.nombres).toEqual('');
    expect(component.datosConsolidado.restricciones).toEqual('');
    expect(component.datosConsolidado.caducidadDescripcion).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoDistDomicilio).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoProvNacimiento).toEqual('');
    expect(component.datosConsolidado.nombrePadre).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoLocalidadNacimiento).toEqual('');
    expect(component.datosConsolidado.numeroLibro).toEqual('');
    expect(component.datosConsolidado.localidad).toEqual('');
    expect(component.datosConsolidado.distritoNacimiento).toEqual('');
    expect(component.datosConsolidado.ubigeoVotacion).toEqual('');
    expect(component.datosConsolidado.dni).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoLocalidadDomicilio).toEqual('');
    expect(component.datosConsolidado.departamentoDomicilio).toEqual('');
    expect(component.datosConsolidado.grupoVotacion).toEqual('');
    expect(component.datosConsolidado.provinciaDomicilio).toEqual('');
    expect(component.datosConsolidado.restriccionesDesc).toEqual('');
    expect(component.datosConsolidado.caducidadCodigo).toEqual('');
    expect(component.datosConsolidado.anioEstudio).toEqual('');
    expect(component.datosConsolidado.departamentoNacimiento).toEqual('');
    expect(component.datosConsolidado.numeroDocSustentarioIdentidad).toEqual('');
    expect(component.datosConsolidado.estadoCivilDescripcion).toEqual('');
    expect(component.datosConsolidado.fechaInscripcion).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoDeptoDomicilio).toEqual('');
    expect(component.datosConsolidado.estadoCivilCodigo).toEqual('');
    expect(component.datosConsolidado.fechaExpedicion).toEqual('');
    expect(component.datosConsolidado.tipoDocSustentarioIdentidad).toEqual('');
    expect(component.datosConsolidado.sexoCodigo).toEqual('');
    expect(component.datosConsolidado.codigoUbigeoDeptoNacimiento).toEqual('');
    expect(component.datosConsolidado.sexoDescripcion).toEqual('');
    expect(component.datosConsolidado.foto).toEqual('assets/images/perfil.png');
    expect(component.datosConsolidado.firma).toEqual('assets/images/firma.jpg');
  });


  it('Verificamos la funcion cleanImagen()', () => {
    // Llamamos nuestra funcion  cleanImagen()
    component.cleanImagen();
    // Verificamos que se asigne el valor a nuestra variable foto
    expect(component.foto).toEqual('assets/images/perfil.png');
  });

  it('Verificamos la funcion cleanFirma()', () => {
    // Llamamos nuestra funcion  cleanFirma()
    component.cleanFirma();
    // Verificamos que se asigne el valor a nuestra variable firma
    expect(component.firma).toEqual('assets/images/firma.jpg');
  });

  it('Verificamos nuestra funcion setDatosResponseWithObjtDatosConsolidado(response :any)', () => {
    // Mockeamos un objeto mockResponse para pasarlo a nuestro funcion , le pasamos valores valido para probar la primera condicion
    const mockResponse = {
      apellidoPaterno : 'Test apellidoPaterno',
      apellidoMaterno : 'Test apellidoMaterno',
      nombres : 'Test_nombres',
      fechaCaducidad : '01/02/2000',
      numeroDocumento : '12345678',
      fechaEmision : '06062006'
    };
    // Espiamos nuestra funcion
    const spyformatearFecha = spyOn(component, 'formatearFecha').and.callThrough();
    // Llamamos nuestra funcion
    component.setDatosResponseWithObjtDatosConsolidado(mockResponse);
    // Verificamos los valores asignados por nuestra funcion a la variables
    expect(component.datosConsolidado.apellidos).toEqual(mockResponse.apellidoPaterno + ' ' + mockResponse.apellidoMaterno);
    expect(component.datosConsolidado.nombres).toEqual(mockResponse.nombres);
    expect(component.datosConsolidado.caducidadDescripcion).toEqual(mockResponse.fechaCaducidad);
    expect(component.datosConsolidado.dni).toEqual(mockResponse.numeroDocumento);
    // Verificamos que datosConsolidado.fechaExpedicion sea igual a formatearFecha(mockResponse.fechaEmision)
    const fecha = component.formatearFecha(mockResponse.fechaEmision);
    expect(component.datosConsolidado.fechaExpedicion).toEqual(fecha);
    // Verificamos que nuestra funcion formatearFecha fuera llamada dos veces dado que nosotros la llamamos una vez para asignarlo a fecha
    // Y otra cuando se llame la funcion setDatosResponseWithObjetDatosConsolidado
    expect(spyformatearFecha).toHaveBeenCalledTimes(2);

    // Ahora verificamos para el caso donde response.apellidoPaterno o response.apellidoMaterno es null o vacio
    mockResponse.apellidoMaterno = '';
    mockResponse.apellidoPaterno = '';
    // Llamamos nuestra funcion
    component.setDatosResponseWithObjtDatosConsolidado(mockResponse);
    // Verificamos los valores asignados
    // Verificamos los valores asignados por nuestra funcion a la variables
    // Apellidos en este caso sera igual a ''
    expect(component.datosConsolidado.apellidos).toEqual('');
    expect(component.datosConsolidado.nombres).toEqual(mockResponse.nombres);
    expect(component.datosConsolidado.caducidadDescripcion).toEqual(mockResponse.fechaCaducidad);
    expect(component.datosConsolidado.dni).toEqual(mockResponse.numeroDocumento);
    // Verificamos que datosConsolidado.fechaExpedicion sea igual a formatearFecha(mockResponse.fechaEmision)
    expect(component.datosConsolidado.fechaExpedicion).toEqual(fecha);
    // En este punto hemos llamado tres veces a nuestra funcion formatearFecha
    expect(spyformatearFecha).toHaveBeenCalledTimes(3);
  });


  it('Verificamos nuestra funcion setFoto(response: any)', () => {
    // Mockeamos un mockResponse para nuestra funcion
    const mockResponse = {
      foto: 'Test_Foto'
    };
    // Llamamos nuestra funcion setFoto
    component.setFoto(mockResponse);
    // Verificamos la asignacion datosConsolidado.foto
    expect(component.datosConsolidado.foto).toEqual('data:image/png;base64,' + mockResponse.foto);
  });


  it('Verificamos la funcion formatearFecha()', () => {
    // Mockeamos un mockFecha para probar nuestra funcion
    const mockFecha = '06062006';
    // Verificamos el valor que nos retorna nuestra funcion
    expect(component.formatearFecha(mockFecha)).toEqual('06-06-2006');
  });

  it('Verificamos la funcion requiereDniValidador()', () => {
    // Seteamos en el localStorage el item isWithDniValidator con valor 'true' para verificar la primera condicion que regresa true
    localStorage.setItem('isWithDniValidator', 'true');
    // Llamamos nuestra funcion requiereDniValidador() y verificamos que nos retorna true
    expect(component.requiereDniValidador()).toBeTruthy();
    // Ahora verificamos para el caso contrario , eliminamos de nuestra localStorage el item isWithDniValidator
    localStorage.removeItem('isWithDniValidator');
    // Llamamos nuestra funcion requiereDniValidador() y verificamos que nos retorna false
    expect(component.requiereDniValidador()).toBeFalsy();
  });

  it('Verificamos nuestra funcion isEmpty(param :any)', () => {
    // Mockeamos un valor de entra para nuestra funcion
    let mockParam = '12345678';
    // Llamamos nuestra funcion y verificamos que nos retorne FALSE para este primer caso
    expect(component.isEmpty(mockParam)).toBeFalsy();
    // Para el caso contrario mockeamos vacio y nos deberia devolver TRUE nuestra funcion
    mockParam = '';
    expect(component.isEmpty(mockParam)).toBeTruthy();
  });


  it('Verificamos nuestra funcion isInvalidRegexInputDni(param :any)', () => {
    // mockeamos un valor de entrada para nuestra funcion valido para probar la primera condicion
    let mockParam = '12345678';
    // Verificamos que nuestra funcio nos retorne FALSE para este caso
    expect(component.isInvalidRegexInputDni(mockParam)).toBeFalsy();

    // Ahora verificamos para el caso que el parametro de entrada no cumpla con la expresion regular
    mockParam = '1234567FFFF';
    // Verificamos que nuestra funcio nos retorne true para este caso
    expect(component.isInvalidRegexInputDni(mockParam)).toBeTruthy();
  });


  it('Verificamos nuestra funcion emitar emitirValidacionInput(bandera: boolean, mensaje: string, tipoInput: string)', () => {
    // Espiamos nuestra funcion setMensajeErrorInput
    const spysetMensajeErrorInput = spyOn(component, 'setMensajeErrorInput').and.callThrough();
    // Espiamos nuestra funcion el evento emit
    const spyEmit = spyOn(component.propagarMensajeErrorInput, 'emit');
    // Mockeamos valores de entrada para nuestra funcion
    const mockBandera = true;
    const mockMensaje = 'Test_Mensaje';
    const mockTipoInput = 'Test_TipoInput';
    // Llamamos nuestra funcion
    component.emitirValidacionInput(mockBandera, mockMensaje, mockTipoInput);
    // Verificamos que llame nuestra funcion setMensajeErrorInput
    expect(spysetMensajeErrorInput).toHaveBeenCalled();
    // Verificamos que el evento emit fuera llamado
    expect(spyEmit).toHaveBeenCalled();
  });

  it('Verificamos nuestra funcion setMensajeErrorInput(bandera: boolean, msgError: String, tipoInput: String)', () => {
    // Mockeamos valores de entrada para nuestra funcion
    const mockBandera = true;
    const mockMensaje = 'Test_Mensaje';
    const mockTipoInput = 'Test_TipoInput';
    // Llamamos nuestra funcion setMensajeErrorInput
    component.setMensajeErrorInput(mockBandera, mockMensaje, mockTipoInput);
    // Verificamos la asignacion a nuestras variables
    expect(component.mensajeErrorInput.banderaError).toBe(mockBandera);
    expect(component.mensajeErrorInput.mensajeError).toEqual(mockMensaje);
    expect(component.mensajeErrorInput.tipoInput).toEqual(mockTipoInput);
  });


  it('Verificamos la funcion emitirMensajeRespuestaServicio(codigo: String, mensaje: String)', () => {
    // Espiamos nuestra funcion setRespuestaServicio
    const spysetRespuestaServicio = spyOn(component, 'setRespuestaServicio').and.callThrough();
    // Espiamos nuestro emit
    const spyEmit = spyOn(component.propagarRespuestaServicio, 'emit');
    // Mockeamos los valores de entrada para nuestra funcion
    const mockCodigo = 'Test Codigo';
    const mockMensaje = 'Test_Mensaje';
    // Llamamos nuestra funcion
    component.emitirMensajeRespuestaServicio(mockCodigo, mockMensaje);
    // Verificamos que llame nueestro espia spysetRespuestaServicio
    expect(spysetRespuestaServicio).toHaveBeenCalled();
    // Verificamos que llame a propagarRespuestaServicio.emit del evento Emiter
    expect(spyEmit).toHaveBeenCalled();
  });


  it('Verificamos nuestra funcion servicioVerificacionDactilar()', () => {
    // Espiamos nuestra funcion emitirValidacionInput
    const spyemitirValidacionInput = spyOn(component, 'emitirValidacionInput');
    // Espiamos nuestra funcion setSendRespuesta()
    const spysetSendRespuesta = spyOn(component, 'setSendRespuesta');
    // Espiamos nuestra funcion cleanFirma()
    const spycleanFirma = spyOn(component, 'cleanFirma');
    // Espiamos nuestra funcion cleanImagen()
    const spycleanImagen = spyOn(component, 'cleanImagen');
    // Espiamos nuestra funcion cleanMensajeServicio()
    const spycleanMensajeServicio = spyOn(component, 'cleanMensajeServicio');
    // Espiamos nuestra funcion executeServiceVerificacionBiometrica()
    const spyexecuteServiceVerificacionBiometrica = spyOn(component, 'executeServiceVerificacionBiometrica');
    // Seteamos valores para verificar la primera condicion cuando isValidInputs() sea TRUE
    localStorage.setItem('isWithDniValidator', 'true');
    component.dniValidador = '12345678';
    component.dniConsultor = '12345678';
    // Llamamos nuestra funcion servicioVerificacionDactilar
    component.servicioVerificacionDactilar();
    // Verificamos que llame nuestros espias
    expect(spyemitirValidacionInput).toHaveBeenCalled();
    expect(spysetSendRespuesta).toHaveBeenCalled();
    expect(spycleanFirma).toHaveBeenCalled();
    expect(spycleanImagen).toHaveBeenCalled();
    expect(spycleanMensajeServicio).toHaveBeenCalled();
    expect(spyexecuteServiceVerificacionBiometrica).toHaveBeenCalled();

    // Ahora verificamos para el caso contrario que isValidInputs() sea FALSE
    localStorage.setItem('isWithDniValidator', 'false');
    component.dniValidador = '1234567FF';
    component.dniConsultor = '1234567FF';

    // Espiamos nuestra funcion emitirMensajeRespuestaServicio()
    const spyemitirMensajeRespuestaServicio = spyOn(component, 'emitirMensajeRespuestaServicio');
      // Llamamos nuestra funcion servicioVerificacionDactilar
      component.servicioVerificacionDactilar();
    // Verificamos que llame nuestra funcion emitirMensajeRespuestaServicio
    expect(spyemitirMensajeRespuestaServicio).toHaveBeenCalled();
  });


  it('Verificamos la funcion setRespuestaServicio()', () => {
    // Mockeamos los valores de entrada para la funcion setRespuestaServicio();
    const mockCodigo = 'Test_Code';
    const mockMensaje = 'Test_Mensaje';
    // pasamos los valores de entrada nuestra funcion y la llamamos
    component.setRespuestaServicio(mockCodigo, mockMensaje);
    // Verificamos que se asginen los valores adecuados a a nuestro objeto respuestaServicio
    expect(component.respuestaServicio.codigoRespuesta).toEqual(mockCodigo);
    expect(component.respuestaServicio.mensajeRespuesta).toEqual(mockMensaje);
  });

  it('Verificamos el funcionamiento de nuestra funcion setSendRespuesta(codigoR, mensajeR)', () => {
    // MOCKEAMOS los valores de entrar para nuestra funcion
    const mockCodigoR = 'Test CodigoR';
    const mockMensajeR = 'Test MensajeR';
    // Espiamos el evento emit
    const spyEmit = spyOn(component.propagarRespuestaServicio, 'emit');
    // Espiamos nuestra funcion setRespuestaServicio();
    const spysetRespuestaServicio = spyOn(component, 'setRespuestaServicio');

    // Llamamos nuestra funcion y verificamos que funcione pasandole nuestro valores de entrada
    component.setSendRespuesta(mockCodigoR, mockMensajeR);
    // Verificamos que llame nuestros espias
    expect(spyEmit).toHaveBeenCalled();
    expect(spysetRespuestaServicio).toHaveBeenCalled();
  });


  /*

      setDataRequest() {
        let valorRequestVerificacionBiometrica: Object;
        // tslint:disable-next-line:max-line-length
        valorRequestVerificacionBiometrica = this.utilitario.generarJsonVerificacionBiometricaRequest(this.validarVerificacion(JSON.parse(localStorage.getItem('datosSesion'))._serviciosVerificacion.default), this.dniConsultor, this.tipoCaptura, this.huellaDerechaWSQ, this.huellaDerechaTemplate, this.nistDerecha, this.huellaVivaDerecha, this.huellaIzquierdaWSQ, this.huellaIzquierdaTemplate, this.nistIzquierda, this.huellaVivaIzquierda);

        return valorRequestVerificacionBiometrica;

    }

*/

  it('Verificamos nuestra funcion convertirFakeDetection(fakeDetection) ', () => {
    // Mockeamos el valor de entrada de nuestra funcion
    let mockFakeDetection = 'Test';
    // Llamamos nuestra funcion
    expect(component.convertirFakeDetection(mockFakeDetection)).toEqual(mockFakeDetection);

    // Verificamos para el parametro de estada sea null
    mockFakeDetection = null;
    // Verificamos que nos retorne '0' nuestra funcion
    expect(component.convertirFakeDetection(mockFakeDetection)).toEqual('0');


  });




  it('Verificamos nuestra funcion validarVerificacion(verificacionType)', () => {
    // Mockeamos el valor de entrada de nuestra funcion
    let mockVerificacionType = 'Test';
    // Llamamos nuestra funciony verificamos que en caso de ser diferente de null , '' o 301001 nos retorne su valor
    expect(component.validarVerificacion(mockVerificacionType)).toEqual(mockVerificacionType);
    // Ahora el caso sea '' o null  o '301001' , nos retornara '301001'
    mockVerificacionType = null;
    expect(component.validarVerificacion(mockVerificacionType)).toEqual('301001');
  });

  it('Verificamos la funcion isValidInputs()', () => {
    // Fakeamos el resultado de nuestra funcion requiereDniValidador a true
    const spyrequiereDniValidador = spyOn(component, 'requiereDniValidador').and.callFake(() => true);
    // Fakeamos el resultado de nuestra funcion isEmpty a true
    const spyisEmpty = spyOn(component, 'isEmpty').and.callFake(() => true);
    // Espiamos nuestra funcion emitirValidacionInput
    const spyemitirValidacionInput = spyOn(component, 'emitirValidacionInput');
    // Verificamos nuestra funcion nos devuelva FALSE
    expect(component.isValidInputs()).toBeFalsy();
    // Verificamos que que llame a isEmpty , requiereDniValidador() y emitirValidacionInput
    expect(spyrequiereDniValidador).toHaveBeenCalled();
    expect(spyisEmpty).toHaveBeenCalled();
    expect(spyemitirValidacionInput).toHaveBeenCalled();


    fixture = TestBed.createComponent(VerificarComponent);
    component = fixture.componentInstance;
    // Ahora probamos con la condicion de isInvalidRegexInputDni sea true , para ello la funcion isEmpty sea false
    spyOn(component, 'isEmpty').and.callFake(() => false);
    spyOn(component, 'requiereDniValidador').and.callFake(() => true);
    const spyisInvalidRegexInputDni = spyOn(component, 'isInvalidRegexInputDni').and.callFake(() => true);
    // Verificamos nuestra funcion nos devuelva FALSE
    expect(component.isValidInputs()).toBeFalsy();
    // Verificamos que se llame   requiereDniValidador
    expect(spyrequiereDniValidador).toHaveBeenCalled();
    // Verificamos que llame la funcion isInvalidRegexInputDni
    expect(spyisInvalidRegexInputDni).toHaveBeenCalled();
    // Verificamos que que llame nuestro espia de emitirValidacionInput
    expect(spyemitirValidacionInput).toHaveBeenCalled();

    fixture = TestBed.createComponent(VerificarComponent);
    component = fixture.componentInstance;
    spyOn(component, 'isEmpty').and.callFake(() => false);
    spyOn(component, 'requiereDniValidador').and.callFake(() => true);
    // Ahora verificamos para el caso que isInvalidRegexInputDni y isEmpty sean FALSE
    spyOn(component, 'isInvalidRegexInputDni').and.callFake(() => false);
    // Verificamos nuestra funcion nos devuelva TRUE
    expect(component.isValidInputs()).toBeTruthy();
    // Verificamos que que llame nuestro espia de emitirValidacionInput
    expect(spyemitirValidacionInput).toHaveBeenCalled();


    fixture = TestBed.createComponent(VerificarComponent);
    component = fixture.componentInstance;
    // Probamos la segunda parte de validaciones de nuestra funcion
    spyOn(component, 'requiereDniValidador').and.callFake(() => false);
    spyOn(component, 'isEmpty').and.callFake(() => true);
    spyOn(component, 'isInvalidRegexInputDni').and.callFake(() => false);
    // Verificamos nuestra funcion nos devuelva FALSE
    expect(component.isValidInputs()).toBeFalsy();
    // Verificamos que que llame nuestro espia de emitirValidacionInput
    expect(spyemitirValidacionInput).toHaveBeenCalled();

    fixture = TestBed.createComponent(VerificarComponent);
    component = fixture.componentInstance;
    // Ahora probamos para el caso que isEmpty es false
    spyOn(component, 'isEmpty').and.callFake(() => false);
    spyOn(component, 'requiereDniValidador').and.callFake(() => false);
    spyOn(component, 'isInvalidRegexInputDni').and.callFake(() => true);
    // Verificamos nuestra funcion nos devuelva FALSE
    expect(component.isValidInputs()).toBeFalsy();
    // Verificamos que que llame nuestro espia de emitirValidacionInput
    expect(spyemitirValidacionInput).toHaveBeenCalled();

    fixture = TestBed.createComponent(VerificarComponent);
    component = fixture.componentInstance;
    spyOn(component, 'isEmpty').and.callFake(() => false);
    spyOn(component, 'requiereDniValidador').and.callFake(() => false);
    // Ahora verificamos para el caso que isInvalidRegexInputDni y isEmpty sean FALSE
    spyOn(component, 'isInvalidRegexInputDni').and.callFake(() => false);
    // Verificamos nuestra funcion nos devuelva TRUE
    expect(component.isValidInputs()).toBeTruthy();
    // Verificamos que que llame nuestro espia de emitirValidacionInput
    expect(spyemitirValidacionInput).toHaveBeenCalled();
  });


  it('Verificamos la funcion ngOnInit()', () => {
    // Llamamos nuestro servicio huellaDerecha Y huellaIzquierda y fakeamos su retorno
    const mockHuellaDerechaCaptura = {
      huellaTemplateD : 'Test_huellaTemplateD',
      huellaWSQD : 'Test_huellaWSQD',
      nistD : 'Test_nistD',
      huellavivaD : '0',
    };
    const mockHuellaIzquierda = {
      huellaTemplateI : 'Test_huellaTemplateI',
      huellaWSQI : 'Test_huellaWSQI',
      nistI : 'Test_nistI',
      huellavivaI : '0',
    };

    const spyFakeHuellaDerecha = spyOn(huellaService.huellaDerecha, 'subscribe').and.callFake(() => {
      huellaService.huellaDerecha.next(mockHuellaDerechaCaptura);

    });
    const spyFakeHuellaIzquierda = spyOn(huellaService.huellaIzquierda, 'subscribe').and.callFake(() => {
      huellaService.huellaIzquierda.next(mockHuellaIzquierda);
    });
    // Espiamos nuestra funcion convertirFakeDetection
    const spyconvertirFakeDetection = spyOn(component, 'convertirFakeDetection').and.callThrough();
    // Llamamos nuestra funcion ngOnInit()
    component.ngOnInit();
    // Verificamos que se llame nuestros servicio fakeados
    expect(spyFakeHuellaIzquierda).toHaveBeenCalled();
    expect(spyFakeHuellaDerecha).toHaveBeenCalled();
    // Verificamos que que llame dos veces a nuestra funcion convertirFakeDetection
    expect(spyconvertirFakeDetection).toHaveBeenCalledTimes(2);
    // Verificamos la asignacion de los valores a nuestras variables para HuellaDerecha
    expect(component.huellaDerechaTemplate).toEqual(mockHuellaDerechaCaptura.huellaTemplateD);
    expect(component.huellaDerechaWSQ).toEqual(mockHuellaDerechaCaptura.huellaWSQD);
    expect(component.nistDerecha).toEqual(mockHuellaDerechaCaptura.nistD);
    expect(component.huellaVivaDerecha).toEqual(mockHuellaDerechaCaptura.huellavivaD);
    // Ahora verificamos para HuellaIzquiera
    expect(component.huellaIzquierdaTemplate).toEqual(mockHuellaIzquierda.huellaTemplateI);
    expect(component.huellaIzquierdaWSQ).toEqual(mockHuellaIzquierda.huellaWSQI);
    expect(component.nistIzquierda).toEqual(mockHuellaIzquierda.nistI);
    expect(component.huellaVivaIzquierda).toEqual(mockHuellaIzquierda.huellavivaI);
  });



  it('Verificamos la funcion setDataRequest()', () => {
    // Espiamos nuestra funcion generarJsonVerificacionBiometricaRequest de utilitario
    const spyUtilitario = spyOn(component.utilitario, 'generarJsonVerificacionBiometricaRequest').and.callThrough();
    // Espiamos nuestra funcion validarVerificacion
    const spyvalidarVerificacion = spyOn(component, 'validarVerificacion').and.callThrough();
    // Seteamos los datosSesion en nuestro localStorage para verificar la funcion
    const testObject = {
      _ningunaHuella: '0',
      _huellaVivaAmarillo: '0',
      _noHuellaViva: '0',
      _nistRojo: '0',
      _umbralHuellaViva: '50.0',
      _nistAmarillo: '0',
      _huellaVivaRojo: '0',
      _validarSixser: false,
      _serviciosVerificacion: {
        default: '301001', tipos:
          [{ descripcion: 'T33-ANSI', codigo: '301001' },
          { descripcion: 'T33-ANSI', codigo: '301001' },
          { descripcion: 'T35-WSQ', codigo: '301100' }],
        tiposServicioVerificacion: [{ _descripcion: 'T33-ANSI', _codigo: '301001' },
        { _descripcion: 'T33-ANSI', _codigo: '301001' }, { _descripcion: 'T35-WSQ', '_codigo': '301100' }]
      },
      _acciones: [{ _descripcion: 'Mejor Huella', _codigo: 'sixbio' }, { _descripcion: 'Capturar', _codigo: 'sixbio' },
      { _descripcion: 'Verificar', _codigo: 'sixbio' }, { _descripcion: 'VerificarMOCard', _codigo: 'sixbio' },
      { _descripcion: 'Foto', _codigo: 'sixser' }, { _descripcion: 'Datos', _codigo: 'sixser' }],
      _idSesion: '000000202103111120504380242',
      _validarHuellaViva: true,
      _validarDniValidador: true,
      _visualizar: true,
      _sixbioVersion: '1.6',
      _usuario: 'jburgos6',
      _noHuellaCapturada: '0',
      _validarIpCliente: true,
      _rolesSCA: [{ _permisos: [], _rol: 'EMP_ADM' }],
      _codigoRespuesta: '00000',
      _mensajeRespuesta: 'OPERACION REALIZADA SATISFACTORIAMENTE'
    };
    localStorage.setItem('datosSesion', JSON.stringify(testObject));
    // Llamamos nuestra funcion
    expect(component.setDataRequest()['codigoTransaccion']).toContain(testObject. _serviciosVerificacion.default);
    // Verificamos que llame nuestros espias
    expect(spyUtilitario).toHaveBeenCalled();
    expect(spyvalidarVerificacion).toHaveBeenCalled();

  });

  it('Verificamos la funcion executeServiceVerificacionBiometrica()', () => {
    // Espiamos nuestra funcion desHabilitarBoton
    const spydesHabilitarBoton = spyOn(component, 'desHabilitarBoton');
    // Espiamos nuestra funcion clearDatosConsolidado
    const spyclearDatosConsolidado = spyOn(component, 'clearDatosConsolidado');
    // Espiamos nuestra funcion setSendRespuesta
    const spysetSendRespuesta = spyOn(component, 'setSendRespuesta');
    // Espiamos nuestro servicio serviceVerificacionBiometrica
    const spyService = spyOn(servicio, 'serviceVerificacionBiometrica').and.callThrough();
    // Espiamos nuestra funcion setDataRequest
    const spysetDataRequest = spyOn(component, 'setDataRequest');
    // Llamamos nuestra funcion
    component.executeServiceVerificacionBiometrica();
    // Verificamos que llame nuestros espias
    expect(spydesHabilitarBoton).toHaveBeenCalled();
    expect(spyclearDatosConsolidado).toHaveBeenCalled();
    expect(spysetSendRespuesta).toHaveBeenCalled();
    expect(spyService).toHaveBeenCalled();
    expect(spysetDataRequest).toHaveBeenCalled();

    // Ahora mockeamos un dataResponse para nuestro servicio
    // Probaremos la primera condicion cuando codigoRespuesta sea Constant.COD_ERROR_SESION
    const mockDataResponse = {
      codigoRespuesta: Constant.COD_ERROR_SESION,
      foto : null
    };
    // Con and.callFake hacemos que nuestra funcion nos devuelva la data mockeada
    const spyServiceFake = spyOn(servicio, 'serviceVerificacionBiometrica').and.callFake(() => {
      return of(mockDataResponse);
    });
    // Espiamos nuestra funcion salir()
    const spySalir = spyOn(component, 'salir');
    // Llamamos nuestra funcion
    component.executeServiceVerificacionBiometrica();
    // Verificamos que se llame nuestro servicio FAKE
    expect(spyServiceFake).toHaveBeenCalled();
    // Verificamos que llame nuestro espia de salir()
    expect(spySalir).toHaveBeenCalled();

    // Ahora verificamos para el caso que codigoRespuesta pueda ser this.constant.VERIFICACION_BIOMETRICA.RENIEC.SUCCESS_WARN.CODIGO
    // this.constant.VERIFICACION_BIOMETRICA.RENIEC.SUCCES.CODIGO
    // this.constant.VERIFICACION_BIOMETRICA.LOCAL.SUCCES.CODIGO
    // this.constant.VERIFICACION_BIOMETRICA.LOCAL.SUCCESS_WARN.CODIGO
    // Asiganamos alguno de los valores a nuestro codigoRespuesta mockeado
    mockDataResponse.codigoRespuesta = component.constant.VERIFICACION_BIOMETRICA.LOCAL.SUCCESS_WARN.CODIGO;
    // Espiamos nuestra funcion setDatosResponseWithObjtDatosConsolidado
    const spysetDatosResponseWithObjtDatosConsolidado = spyOn(component, 'setDatosResponseWithObjtDatosConsolidado');
    // Espiamos el evento emit de propagarDatos
    const spypropagarDatos = spyOn(component.propagarDatos, 'emit');
    // Espiamos nuestra funcion siHabilitarBotonVerificacion
    const spysiHabilitarBotonVerificacion = spyOn(component, 'siHabilitarBotonVerificacion');
    // Llamamos nuestra funcion
    component.executeServiceVerificacionBiometrica();
    // Verificamos que que llame los espias de nuestras funcion
    expect(spysetDatosResponseWithObjtDatosConsolidado).toHaveBeenCalled();
    expect(spysiHabilitarBotonVerificacion).toHaveBeenCalled();
    expect(spysetSendRespuesta).toHaveBeenCalled();
    // Verificamos que llame al espia del evento emit
    expect(spypropagarDatos).toHaveBeenCalled();

    // Para probar la siguiente condicion asignamos un valor foto
    mockDataResponse.foto = 'something';
    // Espiamos nuestra funcion setFoto
    const spysetFoto = spyOn(component, 'setFoto');
    // Llamamos nuestra funcion
    component.executeServiceVerificacionBiometrica();
    // Verificamos que que llame los espias de nuestras funciones
    expect(spysetFoto).toHaveBeenCalled();
    expect(spysiHabilitarBotonVerificacion).toHaveBeenCalled();
    // Verificamos que llame al espia del evento emit
    expect(spypropagarDatos).toHaveBeenCalled();

    // Ahora verificamos para el caso que codigoRespuesta sea constant.VERIFICACION_BIOMETRICA.RENIEC.SUCCES.CODIGO
    mockDataResponse.codigoRespuesta = component.constant.VERIFICACION_BIOMETRICA.RENIEC.SUCCES.CODIGO;
    // Espiamos nuestra funcion cleanImagen
    const spycleanImagen = spyOn(component, 'cleanImagen');
    // Espiamos nuestra funcion habilitarBotones
    const spyhabilitarBotones = spyOn(component, 'habilitarBotones');
    // Llamamos nuestra funcion
    component.executeServiceVerificacionBiometrica();
    // Verificamos que llame nuestra funciones
    expect(spycleanImagen).toHaveBeenCalled();
    expect(spyhabilitarBotones).toHaveBeenCalled();
    expect(spysetSendRespuesta).toHaveBeenCalled();
    expect(spysiHabilitarBotonVerificacion).toHaveBeenCalled();



  });

/*
  executeServiceVerificacionBiometrica() {
        this.desHabilitarBoton();
        this.clearDatosConsolidado();
        this.setSendRespuesta('', this.constant.VERIFICACION_BIOMETRICA.MENSAJE_ESPERA);
        this.servicioService.serviceVerificacionBiometrica(this.setDataRequest()).subscribe((dataResponse: any) => {
            if (dataResponse.codigoRespuesta === Constant.COD_ERROR_SESION) {
                this.salir();
                return;
            }
            if (dataResponse.codigoRespuesta === this.constant.VERIFICACION_BIOMETRICA.RENIEC.SUCCESS_WARN.CODIGO ||
                dataResponse.codigoRespuesta === this.constant.VERIFICACION_BIOMETRICA.RENIEC.SUCCES.CODIGO ||
                dataResponse.codigoRespuesta === this.constant.VERIFICACION_BIOMETRICA.LOCAL.SUCCES.CODIGO ||
                dataResponse.codigoRespuesta === this.constant.VERIFICACION_BIOMETRICA.LOCAL.SUCCESS_WARN.CODIGO) {
                this.setSendRespuesta(dataResponse.codigoRespuesta, dataResponse.mensajeRespuesta);
                this.setDatosResponseWithObjtDatosConsolidado(dataResponse);
                this.propagarDatos.emit(this.datosConsolidado);
                this.siHabilitarBotonVerificacion();
                if (dataResponse.foto !== null) {
                    this.setFoto(dataResponse);
                    this.propagarDatos.emit(this.datosConsolidado);
                    this.siHabilitarBotonVerificacion();
                }
                if (dataResponse.codigoRespuesta === this.constant.VERIFICACION_BIOMETRICA.RENIEC.SUCCES.CODIGO) {
                    this.setSendRespuesta(this.constant.VERIFICACION_BIOMETRICA.RENIEC.SUCCES.CODIGO, this.constant.VERIFICACION_BIOMETRICA.RENIEC.SUCCES.MENSAJE);
                    this.cleanImagen();
                    this.siHabilitarBotonVerificacion();
                    this.habilitarBotones();
                } else if (dataResponse.codigoRespuesta === this.constant.VERIFICACION_BIOMETRICA.LOCAL.SUCCES.CODIGO) {
                    this.setSendRespuesta(this.constant.VERIFICACION_BIOMETRICA.LOCAL.SUCCES.CODIGO, this.constant.VERIFICACION_BIOMETRICA.LOCAL.SUCCES.MENSAJE);
                    this.habilitarBotones();
                    this.siHabilitarBotonVerificacion();
                } else if (dataResponse.codigoRespuesta === this.constant.VERIFICACION_BIOMETRICA.RENIEC.SUCCESS_WARN.CODIGO) {
                    this.setSendRespuesta(this.constant.VERIFICACION_BIOMETRICA.RENIEC.SUCCESS_WARN.CODIGO, this.constant.VERIFICACION_BIOMETRICA.RENIEC.SUCCESS_WARN.MENSAJE);
                } else if (dataResponse.codigoRespuesta === this.constant.VERIFICACION_BIOMETRICA.LOCAL.SUCCESS_WARN.CODIGO) {
                    this.setSendRespuesta(this.constant.VERIFICACION_BIOMETRICA.LOCAL.SUCCESS_WARN.CODIGO, this.constant.VERIFICACION_BIOMETRICA.LOCAL.SUCCESS_WARN.MENSAJE);
                } else {
                    this.setSendRespuesta(dataResponse.codigoRespuesta, this.utilitario.encodeMensaje(dataResponse.mensajeRespuesta));
                    this.siHabilitarBotonVerificacion();
                }
            } else if (dataResponse.codigoRespuesta === this.constant.CONSTANTE.ERROR_SESION.CODIGO) {
                setTimeout(() => {this.salir(); }, 500);
                // redirect inicio
            } else if (dataResponse.codigoRespuesta === '15000' || dataResponse.codigoRespuesta === '00101' || dataResponse.codigoRespuesta === '00114' || dataResponse.codigoRespuesta === '00113' || dataResponse.codigoRespuesta === '00101') {
                this.setSendRespuesta(null, 'cleanFront');
                this.cleanFirma();
                this.desHabilitarBoton();
                this.setSendRespuesta(dataResponse.codigoRespuesta, this.utilitario.encodeMensaje(dataResponse.mensajeRespuesta));
            } else {
                this.setSendRespuesta('', '');
                this.toast.addToast({ title: 'Error', msg: 'Error de conectividad', timeOut: 5000, type: 'error' });
                setTimeout(() => {this.salir(); }, 500);
            }
        }, (error) => {
            this.setSendRespuesta(this.constant.CONSTANTE.OPERACION_ERROR.CODIGO, this.constant.CONSTANTE.OPERACION_ERROR.MENSAJE);
            setTimeout(() => {this.salir(); }, 500);
        });
    }



*/

});
