import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import {  ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs/internal/Subject';
import { EnvService } from 'src/app/env.service';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { HuellaService } from '../../serviciosDeUI/huella.service';

import { FotoComponent } from './foto.component';

fdescribe('FotoComponent', () => {
  let component: FotoComponent;
  let fixture: ComponentFixture<FotoComponent>;
 // Instanciamos los servicios que utilizaremos
 let translateService: TranslateService;
 let servicio: ServicioService;
 let huellaService: HuellaService;
 class FakeRouter {
   navigate(params) {

   }
 }

 class FakeActivatedRoute {
   // params: Observable<any> = Observable.empty();
   private subject = new Subject();

   push(valor) {
     this.subject.next(valor);
   }
   get params() {
     return this.subject.asObservable();
   }
 }
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ FotoComponent, ToastComponent ],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService,
        HuellaService,
        { provide: Router, useClass: FakeRouter },
        { provide: ToastrService, useClass: ToastrService },
        { provide: ActivatedRoute, useClass: FakeActivatedRoute }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FotoComponent);
    // Injectamos nuestro servicio con TestBed.inject
    servicio = TestBed.inject(ServicioService);
    // Injectamos nuestro servicio de huella con TestBed.inject
    huellaService = TestBed.inject(HuellaService);
    component = fixture.componentInstance;
  });

  it('FotoComponent creado correctamente', () => {
    expect(component).toBeTruthy();
  });

  it('Verificamos la funcion setDataObtenerFoto(dniConsultor: String)', () => {
      // Mockeamos un objetivo para verificarlo en nuestra funcion
      const mockDniConsultor = '12345678';
      const mockTest = {
        numeroDocumentoPersona: mockDniConsultor
      };
      // Verificamos que nuestra funcion nos devuelva el objeto como esperamos
      expect(component.setDataObtenerFoto(mockDniConsultor)).toEqual(mockTest);
  });
/*
     setDataObtenerFoto(dniConsultor: String) {
        return {
            numeroDocumentoPersona: dniConsultor
        };
      }
*/



});
