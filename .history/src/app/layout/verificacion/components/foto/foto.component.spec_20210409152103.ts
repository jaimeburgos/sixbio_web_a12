import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import {  ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router, ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { of, throwError } from 'rxjs';
import { Subject } from 'rxjs/internal/Subject';
import { EnvService } from 'src/app/env.service';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { Constant } from 'src/app/nucleo/constante/Constant';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { HuellaService } from '../../serviciosDeUI/huella.service';

import { FotoComponent } from './foto.component';

fdescribe('FotoComponent', () => {
  let component: FotoComponent;
  let fixture: ComponentFixture<FotoComponent>;
 // Instanciamos los servicios que utilizaremos
 let translateService: TranslateService;
 let servicio: ServicioService;
 let huellaService: HuellaService;
 class FakeRouter {
   navigate(params) {

   }
 }

 class FakeActivatedRoute {
   // params: Observable<any> = Observable.empty();
   private subject = new Subject();

   push(valor) {
     this.subject.next(valor);
   }
   get params() {
     return this.subject.asObservable();
   }
 }
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ FotoComponent, ToastComponent ],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService,
        HuellaService,
        { provide: Router, useClass: FakeRouter },
        { provide: ToastrService, useClass: ToastrService },
        { provide: ActivatedRoute, useClass: FakeActivatedRoute }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FotoComponent);
    // Injectamos nuestro servicio con TestBed.inject
    servicio = TestBed.inject(ServicioService);
    // Injectamos nuestro servicio de huella con TestBed.inject
    huellaService = TestBed.inject(HuellaService);
    component = fixture.componentInstance;
  });

  it('FotoComponent creado correctamente', () => {
    expect(component).toBeTruthy();
  });

  it('Verificamos la funcion setDataObtenerFoto(dniConsultor: String)', () => {
      // Mockeamos un objetivo para verificarlo en nuestra funcion
      const mockDniConsultor = '12345678';
      const mockTest = {
        numeroDocumentoPersona: mockDniConsultor
      };
      // Verificamos que nuestra funcion nos devuelva el objeto como esperamos
      expect(component.setDataObtenerFoto(mockDniConsultor)).toEqual(mockTest);
  });

  it('Verificamos la funcion setErrorValidador(bandera: boolean, msgError: String, tipoInput: String) ', () => {
    // Mockeamos los valores de entrada
    const mockBanderaError = true;
    const mockMensajeError = 'Test MSM';
    const mockTipoInput = 'Test tipoInput';
    // Pasamos los valores entrada mockeados
    expect(component.setErrorValidador(mockBanderaError, mockMensajeError, mockTipoInput));
    // Verificamos los valores que se asigne al objeto errorValidador
    expect(component.errorValidador.banderaError).toBe(mockBanderaError);
    expect(component.errorValidador.mensajeError).toEqual(mockMensajeError);
    expect(component.errorValidador.tipoInput).toEqual(mockTipoInput);
  });

  it('Verificamos nuestra funcion setRespuestaServicio', () => {
    // Mockeamos los valores de entrada
    const mockCodigo = 'Test Code';
    const mockMensaje = 'Test message';
    // Pasamos los valores de entrada a nuestra funcion
    component.setRespuestaServicio(mockCodigo, mockMensaje);
    // Verificamos las asignaciones al objeto respuestaServicio
    expect(component.respuestaServicio.codigoRespuesta).toEqual(mockCodigo);
    expect(component.respuestaServicio.mensajeRespuesta).toEqual(mockMensaje);
  });

  it('Verificamos la funcion habilitarBoton()', () => {
    // Seteamos un item botonFoto con valor 'true' en localStorage
    localStorage.setItem('botonFoto', 'true');
    // Llamamos nuestra funcion y verificamos que nos devuelva true
    expect(component.habilitarBoton()).toBeTruthy();
    // Para el caso contrario cuando el valor es diferente de 'true'
    localStorage.removeItem('botonFoto');
    localStorage.setItem('botonFoto', 'Lo que sea diferente de true');
    // Llamamos nuestra funcion y verificamos que de false
    expect(component.habilitarBoton()).toBeFalsy();
  });


  it('Verificamos la funcion habilitarBotonSi()', () => {
    // Llamamos nuestra funcion que debe setear el item en el localStorage 'true' el valor 'true'
    component.habilitarBotonSi();
    // Verificamos el localStorage
    expect(localStorage.getItem('true')).toEqual('true');
  });

  it('Verificamos la funcion desHabilitarBoton()', () => {
    // Llamamos nuestra funcion que debe setear el item en el localStorage 'true' el valor 'true'
    component.desHabilitarBoton();
    // Verificamos el localStorage
    expect(localStorage.getItem('true')).toEqual('true');
  });


  it('Verificamos el funcionamiento esperado de la funcion salir()', () => {
    const vacio: Object = {  };
    component.dataRequest = vacio;
    // Fakeamos localStorage clear caso contrario tendremos un error por la dependencia entre fotoComponent y verificacionComponent
    spyOn(localStorage, 'clear').and.callFake(() => { 'No hago nada'; });
    // Creamos un espia para el servicio
    const spyService = spyOn(servicio, 'serviceSignOff').and.callThrough();
    // Llamamos nuestra funcion salir() desde el componente
    component.salir();
    // Verificamos que nuestro servicio fuera llamado
    expect(spyService).toHaveBeenCalled();

    // Mockeamos dataResponse para probar la validaciones , el primer caso
    const mockDataResponse = {
      codigoRespuesta : Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    };
    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate').and.callThrough();

    // Fakeamos una llamada al servicio para que nos de el mock de lo que deseamos
    const spyServiceFake = spyOn(servicio, 'serviceSignOff').and.callFake(() => {
      return of (mockDataResponse);
    });

    component.salir();

    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();
    // Verificamos que nuestro servicio Fake fuera llamado
    expect(spyServiceFake).toHaveBeenCalled();


    // Comprobamos la siguiente condicion cuando es diferente de COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    mockDataResponse.codigoRespuesta = 'Diferente de correcto';

    // Volvemos a llamar a la funcion
    component.salir();

    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();


    // Ahora verificamos en caso de un error al llamar al servicio
    const spyError = spyOn(servicio, 'serviceSignOff').and.returnValue(throwError('Esto es un error'));
    // Volvemos a llamar a la funcion
    component.salir();
    // Verificamos que fuera llamado con error nuestro servicio
    expect(spyError).toHaveBeenCalled();
    // Verificamos que el router navigate fuera llamado
    expect(spyRouter).toHaveBeenCalled();
  });



});
