import { TestBed } from '@angular/core/testing';

import { HuellaService } from './huella.service';

describe('HuellaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HuellaService = TestBed.get(HuellaService);
    expect(service).toBeTruthy();
  });
});
