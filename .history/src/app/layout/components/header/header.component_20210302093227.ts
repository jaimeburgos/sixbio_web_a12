import { Component, OnInit, ViewChild, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ServicioService } from '../../../servicio/servicio.service';
import { Constant } from '../../../nucleo/constante/Constant';
import { Util } from 'src/app/nucleo/util/Util';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalComponent } from 'src/app/layout/bs-component/components';
import { ToastComponent } from 'src/app/layout/bs-component/components/toast/toast.component';
import { ITipoVerificacion } from 'src/app/nucleo/interface/ITipoVerificacion';
import { stringify } from 'querystring';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit , OnChanges {
    data: Object = {};
    tipoVerificacion: ITipoVerificacion = {
        codigo: '',
        descripcion: ''
      };
    rolesSCA = {
        _permisos: Array,
        _rol: ''
    };

    dniValidador: string ;
    @Output() propagarDniValidador = new EventEmitter<String>();
    usuarioLogueado = '';

    dniValidadorEditado = '';
    public pushRightClass: string;
    public holaSignoff: string;

    /* var objetoRegresado = localStorage.getItem('datosSesion'); */

    util: Util;
    nombreAPP = Constant.NOMBRE_APP;


    @ViewChild(ToastComponent, { static: true }) toast: ToastComponent;

    dataRequest: Object = {};
    // tslint:disable-next-line:max-line-length
    constructor(private translate: TranslateService, public router: Router, public servicioService: ServicioService, private modalService: NgbModal) {
        window.addEventListener('beforeunload', () => {
            this.servicioService.serviceEndCapturaDactilar(this.data).subscribe((dataR: any) => {});
            if (performance.navigation.type !== performance.navigation.TYPE_RELOAD) {
                this.servicioService.serviceSignOff(this.dataRequest).subscribe((dataResponse: any) => {});
                localStorage.clear();
            }
         }, false);
        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
        this.setBotones();

        this.util = new Util(modalService);

    }

    ngOnChanges(cambio: SimpleChanges) {
    }

    ngOnInit() {

        this.pushRightClass = 'push-right';
        this.tipoVerificacion = JSON.parse(localStorage.getItem('datosSesion'))._serviciosVerificacion.tipos;
        this.rolesSCA = JSON.parse(localStorage.getItem('datosSesion'))._rolesSCA[0];
        this.usuarioLogueado =  localStorage.getItem('usuarioLogueado');
    }


    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }
    setDataSignOff() {
        return {
        };
    }

    onLoggedout() {


        // tslint:disable-next-line:max-line-length
        this.util.showDefaultModalComponent(ModalComponent, '', 'Está seguro de realizar el cierre de la sesión').result.then((closeResult) => {

            if (closeResult !== 'Confirm') {return; }
            this.dataRequest = this.setDataSignOff();

            this.servicioService.serviceSignOff(this.dataRequest).subscribe((dataResponse: any) => {

                if (dataResponse.codigoRespuesta === Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO) {
                    localStorage.clear();
                    this.router.navigate(['/login']);
                } else {

                    localStorage.clear();
                    this.router.navigate(['/login']);

                }
            }
                , error => {
                    localStorage.clear();
                    this.router.navigate(['/login']);
                });
        },
            (reason) => {
                this.util.getDismissReason(reason);
            }
        );

    }
    cambiarVerificacion() {
        if (this.tipoVerificacion.codigo = '301000' ) {
            localStorage.setItem('tipoVerificacion', '301000');
        } else if (this.tipoVerificacion.codigo = '301001' ) {
            localStorage.setItem('tipoVerificacion', '301001');
        } else if (this.tipoVerificacion.codigo = '301100') {
            localStorage.setItem('tipoVerificacion', '301100');
        } else if (this.tipoVerificacion.codigo = '310000') {
            localStorage.setItem('tipoVerificacion', '310000');
        }

    }

    cambiarContrasenia() {
            localStorage.setItem('isFirstLogin', 'true');
            localStorage.setItem('changePassword', 'true');
            this.router.navigate(['/firstLogin']);

    }

/*     cambiarContrasenia() {
        // tslint:disable-next-line:max-line-length
        this.util.showDefaultModalComponent(ModalComponent, '',
        '¿Está seguro de realizar el cambio de contraseña?').result.then((closeResult) => {
            if (closeResult !== 'Confirm') {return; }
            localStorage.setItem('isFirstLogin', 'true');
            localStorage.setItem('changePassword', 'true');
            this.router.navigate(['/firstLogin']);
        },
        (reason) => {
            this.util.getDismissReason(reason);
        }
    );
    } */

    cambiarTipoVerificacion(value, descripcion) {
        this.toast.addToast({ title: 'Cambio de verificación', msg: descripcion +
        ' seleccionado correctamente', timeOut: 2000, type: 'success'});
            localStorage.setItem('tipoVerificacion', value);
    }

    changeLang(language: string) {
        this.translate.use(language);
    }
    setBotones() {
        localStorage.setItem('botonVerificar', 'false');
        localStorage.setItem('botonMOC', 'false');
        localStorage.setItem('botonMejorHuella', 'false');
        localStorage.setItem('botonFoto', 'false');
        localStorage.setItem('botonDatos', 'false');
        localStorage.setItem('campoCantidad', 'true');
        localStorage.setItem('campoFechaDesde', 'false');
        localStorage.setItem('campoFechaHasta', 'false');
    }

    isVerificador() {
        if (this.rolesSCA._rol === 'EMP_VER') {
            return true;
        } else {
            return false;
        }
    }
}
