import { Component, Output, EventEmitter, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

    isActive: boolean;
    collapsed: boolean;
    showMenu: string;
    pushRightClass: string;

    public menus: Array<any>;

    // Permisos rol Administrador
    rolAMD: Array<any> = [
        {
            nameTitle : 'Módulo de Gestión',
            expandClass: 'MGestion',
            subMenu : [
                {
                    name : 'Gestión de Usuarios',
                    // path : 'gestion usuarios'
                    path : 'gestion-usuarios',
                    icon : 'fa fa-users'
                }
            ]
        }
        ,
        {
            nameTitle : 'Módulo de Consultas',
            expandClass: 'MConsultor',
            subMenu : [
                {
                    name : 'Consultar Validaciones',
                    // path : 'consultaValidacion'
                    path : 'consultar-validaciones',
                    icon : 'fa fa-search'
                }
            ]
        }
    ];

    // Permisos del rol Consultor
    rolCST: Array<any> = [
        {
            nameTitle : 'Módulo de Verificación',
            expandClass: 'MVerificacion',
            subMenu : [
                {
                    name : 'Verificación Biométrica',
                    path : 'verificacionBiometrica',
                    icon : 'fa fa-hand-o-up'
                }
            ]
        }
        ,
        {
            nameTitle : 'Módulo de Consultas',
            expandClass: 'MConsultor',
            subMenu : [
                {
                    name : 'Consultar Validaciones',
                    path : 'consultar-validaciones',
                    icon : 'fa fa-search'
                }
            ]
        }
    ];


    @Output() collapsedEvent = new EventEmitter<boolean>();

    constructor(private translate: TranslateService, public router: Router) {
        this.getRolUser();

        this.router.events.subscribe(val => {

            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }

        });

    }

    ngOnInit() {
        this.isActive = false;
        this.collapsed = false;
        if (this.menus.length > 0) {
            this.showMenu = this.menus[0].expandClass;
        }
        this.pushRightClass = 'push-right';
    }

    getRolUser() {
        if (localStorage.getItem('RolUsuario') !== null) {
            if (localStorage.getItem('RolUsuario') === 'EMP_ADM') {
                this.menus = this.rolAMD;
                this.router.navigate([this.menus[0].subMenu[0].path]);
            } else if (localStorage.getItem('RolUsuario') === 'EMP_VER') {
                this.menus = this.rolCST;
                this.router.navigate([this.menus[0].subMenu[0].path]);
            }
        }
    }

    eventCalled() {
        this.isActive = !this.isActive;
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    toggleCollapsed() {
        this.collapsed = !this.collapsed;
        this.collapsedEvent.emit(this.collapsed);
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    changeLang(language: string) {
        this.translate.use(language);
    }

    onLoggedout() {
        localStorage.removeItem('isLoggedin');
        localStorage.removeItem('isFirstLogin');
    }
}
