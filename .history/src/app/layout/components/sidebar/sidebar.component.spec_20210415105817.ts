import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { SidebarComponent } from './sidebar.component';
import { LayoutModule } from '../../layout.module';
import { CommonModule, DatePipe } from '@angular/common';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { BsDatepickerModule, BsLocaleService } from 'ngx-bootstrap/datepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { EnvService } from 'src/app/env.service';
import { ServicioService } from 'src/app/servicio/servicio.service';
import { Subject } from 'rxjs';

class FakeRouter {
  public url;
  private subject = new Subject();
  public events = this.subject.asObservable();

  navigate(url: string) {
    this.url = url;
    this.triggerNavEvents(url);
  }

  triggerNavEvents(url) {
    const ne = new NavigationEnd(0, url, null);
    this.subject.next(ne);
  }
  createUrlTree () {
  }
}

class FakeActivatedRoute {
  // params: Observable<any> = Observable.empty();
  private subject = new Subject();

  push( valor ) {
    this.subject.next( valor );
  }
  get params() {
    return this.subject.asObservable();
  }
}
fdescribe('SidebarComponent', () => {
  let component: SidebarComponent;
  let fixture: ComponentFixture<SidebarComponent>;
  // Instanciamos los servicios que utilizaremos
  let translateService: TranslateService;
  let servicio: ServicioService;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarComponent ],
      imports: [
        CommonModule,
        BrowserAnimationsModule,
        HttpClientTestingModule,
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        TranslateModule.forRoot(),
        BsDatepickerModule.forRoot(),
      ],
      providers: [
        TranslateService,
        BsLocaleService,
        DatePipe,
        EnvService,
        ServicioService,
        { provide: Router, useClass: FakeRouter },
        { provide: ToastrService, useClass: ToastrService },
        { provide: ActivatedRoute, useClass: FakeActivatedRoute }
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
      .compileComponents();
  });

  beforeEach(() => {
    localStorage.setItem('RolUsuario', 'EMP_ADM');
    fixture = TestBed.createComponent(SidebarComponent);
    translateService = TestBed.inject(TranslateService);
    // Injectamos nuestro servicio con TestBed.inject
    servicio = TestBed.inject(ServicioService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('SidebarComponent creado correctamente', () => {
    // Inyectamos router previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos a router.events y su subscribe
    expect(component).toBeTruthy();
    // Verificamos que nuestro espia fuera llamado

  });
});
