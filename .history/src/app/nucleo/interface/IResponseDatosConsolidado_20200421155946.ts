export interface IResponseDatosConsolidado {
    apellidos: String;
    apellidoPaterno?: String;
    fechaNacimiento: String;
    constanciaVotacionDesc: String;
    nombreMadre: String;
    codigoUbigeoProvDomicilio: String;
    direccion: String;
    direccionDomicilio?: String;
    estatura: String;
    constanciaVotacionCodigo: String;
    localidadDomicilio: String;
    provinciaNacimiento: String;
    apellidoMaterno?: String;
    codigoUbigeoDistNacimiento: String;
    nombres: String;
    restricciones: String;
    caducidadDescripcion: String;
    codigoUbigeoDistDomicilio: String;
    codigoUbigeoProvNacimiento: String;
    nombrePadre: String;
    codigoUbigeoLocalidadNacimiento: String;
    numeroLibro: String;
    localidad: String;
    distritoNacimiento: String;
    ubigeoVotacion: String;
    dni: String;
    codigoUbigeoLocalidadDomicilio: String;
    departamentoDomicilio: String;
    grupoVotacion: String;
    provinciaDomicilio: String;
    restriccionesDesc: String;
    caducidadCodigo: String;
    anioEstudio: String;
    departamentoNacimiento: String;
    numeroDocSustentarioIdentidad: String;
    estadoCivilDescripcion: String;
    fechaInscripcion: String;
    codigoUbigeoDeptoDomicilio: String;
    estadoCivilCodigo: String;
    fechaExpedicion: String;
    tipoDocSustentarioIdentidad: String;
    distritoDomicilio?: String;
    sexoCodigo: String;
    codigoUbigeoDeptoNacimiento: String;
    sexoDescripcion: String;
    foto: String;
    firma: String;
}
