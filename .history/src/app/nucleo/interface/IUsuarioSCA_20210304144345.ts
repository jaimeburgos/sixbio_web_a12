export interface IUsuarioSCA {
    usuario?: String;
    nombre?: String;
    apPaterno?: String;
    apMaterno?: String;
    fecNac?: String;
    numDoc?: String;
    tipoDoc?: String;
    correo?: string;
    telef?: string;
    rol?: String;
    estado?: String;
    usuarioLogueado?: String;
    audFecModif?: String;
    audFecCreac?: String;
    audUsuModif?: String;
    audUsuCreac?: String;
    navigationId?: Number;
    usuarioAResetear?: String;
    usuariosAEliminar?: String[];
}
