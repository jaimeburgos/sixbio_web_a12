import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AccessDeniedComponent } from './access-denied/access-denied.component';
import { FirstLoginComponent } from './first-login/first-login/first-login.component';
import { VerificacionComponent } from './layout/verificacion/verificacion.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { ServerErrorComponent } from './server-error/server-error.component';
import { AuthGuard , FirstLoginGuard } from './shared';


const routes: Routes = [
    { path: '', redirectTo: 'home',
   // pathMatch: 'prefix'
   pathMatch: 'full',
},
    { path: 'home', loadChildren: () => import('./layout/layout.module').then(m => m.LayoutModule), canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent},
    // tslint:disable-next-line:max-line-length
    { path: 'firstLogin', component: FirstLoginComponent, canActivate: [FirstLoginGuard]},
    { path: 'error',  component: ServerErrorComponent },
    { path: 'access-denied', component: AccessDeniedComponent },
    { path: 'not-found', component:  NotFoundComponent },
    { path: '**', redirectTo: 'not-found' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
