import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LoginModule } from './login.module';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { ServicioService } from '../servicio/servicio.service';
import { EnvService } from '../env.service';



fdescribe('LoginModule', () => {
    let loginModule: LoginModule;
    let fixture: ComponentFixture<LoginModule>;

    beforeEach(async() => {
        await TestBed.configureTestingModule({
            declarations: [ loginModule  ],
            imports: [   CommonModule,
                HttpClientTestingModule,
                RouterTestingModule,
                FormsModule,
                NgxMyDatePickerModule.forRoot(),
                NgxPaginationModule,
                ToastrModule.forRoot(),
                TranslateModule.forRoot(),
              ],
              providers: [TranslateService, EnvService, ServicioService , {provide: ToastrService, useClass: ToastrService}],
            schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
          })
          .compileComponents();
    });


    beforeEach(() => {
        translateService = TestBed.get(TranslateService); // deprecatado desde angular 9 , recomendable usar Inject

        fixture = TestBed.createComponent(CrearUsuarioComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
      });


    it('Componente Login Creado Correctamente ', () => {
        expect(loginModule).toBeTruthy();
    });







});
