import { async, ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientTestingModule , HttpTestingController} from '@angular/common/http/testing';
import { LoginComponent } from './login.component';
import { EnvService } from '../env.service';
import { FormBuilder, FormsModule , ReactiveFormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { ToastrModule, ToastrService } from 'ngx-toastr';
import { TranslateModule, TranslatePipe, TranslateService } from '@ngx-translate/core';
import { CommonModule } from '@angular/common';
import { ServicioService } from '../servicio/servicio.service';
import { CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA } from '@angular/core';
import { By } from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';
import { of, Subject, throwError } from 'rxjs';
import { Constant } from '../nucleo/constante/Constant';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgbActiveModal, NgbDropdownModule, NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BsComponentModule } from '../layout/bs-component/bs-component.module';
import { LanguageTranslationModule } from '../shared/modules/language-translation/language-translation.module';
import { translate } from '@angular/localize/src/translate';

class FakeRouter {
  navigate(params) {

  }
}

class FakeActivatedRoute {
  // params: Observable<any> = Observable.empty();
  private subject = new Subject();

  push( valor ) {
    this.subject.next( valor );
  }
  get params() {
    return this.subject.asObservable();
  }
}

fdescribe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let servicio: ServicioService;
  let servicioTranslate: TranslateService;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [LoginComponent],
      imports: [   CommonModule,
        HttpClientTestingModule,
        RouterTestingModule,
        NgbDropdownModule,
        NgbModule,
        BsComponentModule,
        LanguageTranslationModule,
        FormsModule,
        ReactiveFormsModule,
        NgxPaginationModule,
        ToastrModule.forRoot(),
        BrowserAnimationsModule,
        ModalModule.forRoot(),
      ],
      providers: [
        TranslateService,
        EnvService,
        ServicioService,
        FormBuilder ,
        {provide: Router , useClass: FakeRouter},
        NgbActiveModal,
        NgbModal,
        TranslatePipe ,
        { provide: ActivatedRoute, useClass: FakeActivatedRoute },
        ReactiveFormsModule,
        {provide: ToastrService, useClass: ToastrService}],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    servicio = TestBed.inject(ServicioService);
    component = fixture.componentInstance;
    fixture.detectChanges();
    // Habilitamos en jasmine el re espiar las funciones , caso contrario tendriamos un error
    jasmine.getEnv().allowRespy(true);


  });

    it('Login Component Creado Correctamente', () => {
       expect(component).toBeTruthy();
    });

    // Validar restricciones del Formulario Login User
    it('Usuario es obligatorio en Login', () => {
      // Obtenemos en una constante el ControlForm que deseamos evaluar
      const usuario = component.loginForm.get('usuario');
      // Le asignamos un valor
      usuario.setValue('');
      // La expectativa de la prueba de ser falso ya que no permite vacio
      expect(usuario.valid).toBeFalsy();
    });

    // Validar restricciones del Formulario Login Pass
    it('Contraseña es obligatorio en Login', () => {
      const pass = component.loginForm.get('pass');
      pass.setValue('');
      expect(pass.valid).toBeFalsy();
    });

    it('Contraseña Longitud Minima es 6 caracteres en Form Login', () => {
      const pass = component.loginForm.get('pass');
      pass.setValue('12345');
      expect(pass.valid).toBeFalsy();
      pass.setValue('123456');
      expect(pass.valid).toBeTruthy();
    });

    it('Usuario Longitud Minima es 5 caracteres en Form Login', () => {
      const usuario = component.loginForm.get('usuario');
      usuario.setValue('1234');
      expect(usuario.valid).toBeFalsy();
      usuario.setValue('12345');
      expect(usuario.valid).toBeTruthy();
    });


    it('Usuario Longitud Maxima es 20 caracteres en Form Login', () => {
      const usuario = component.loginForm.get('usuario');
      usuario.setValue('12345678901234567890');
      expect(usuario.valid).toBeTruthy();
      usuario.setValue('123456789012345678901');
      expect(usuario.valid).toBeFalsy();
    });

    it('Contraseña Longitud Maxima es 16 caracteres en Form Login', () => {
      const pass = component.loginForm.get('pass');
      pass.setValue('1234567890123456');
      expect(pass.valid).toBeTruthy();
      pass.setValue('12345678901234567');
      expect(pass.valid).toBeFalsy();
    });

    // Usuario solo permite letras y numeros en FormLogin
    it('Usuario con caracteres especiales en FormLogin', () => {
      const user = component.loginForm.get('usuario');
      const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b'];
      const U_No_Permitidos =
      ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
      '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
      '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]', '          '];

      // Bucle para caracteres permitidos en usuario FormLogin
      for ( let j = 0; j <= U_Permitidos.length - 1; j++) {
        user.setValue(U_Permitidos[j]);
        expect(user.valid).toBe(true);
        // Cuando sea verdadero !user.valid no se cumple la condicion de verificacion
        if ( !user.valid) {
          console.log('Fallo en el caracter Permitido ' + U_Permitidos[j] );
        }
      }

      // Bucle para caracteres no permitidos en Usuario FormLogin
      for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
        user.setValue(U_No_Permitidos[i]);
        expect(user.valid).toBe(false);
        // Cuando sea verdadero no se cumple la condicion de verificacion
        if (user.valid) {
          console.log('Fallo en el caracter No Permitido ' + U_No_Permitidos[i] );
        }
      }

    });

    // Contraseña permite caracteres especiales como “:”,”.”,”/”, “-”, “_”,“@” y”()” , no acepta espacio.

    it('Contraseña con caracteres permitidos y no permitidos', () => {
      const pass = component.loginForm.get('pass');
      const P_Permitidos = [
        'ññññññññññ', 'ÑÑÑÑÑÑÑ', ':::::::::', '............', ',,,,,,,,,,,', ';;;;;;;;;;;;', '--------------',
      '______________', '((((((((((', '))))))))))', '@@@@@@@@@', '/////////'
      ];

      const P_No_Permitidos = [
        'O☺☻♥♠○◘♣•', '[[[[]]]]', '{{{{{{}}}}}}}', '          ', '???????¿¿¿¿¿¿', '´´´´´´´´',
        '============', '<<<<<>>>>>', '**********', '++++++++++', '$$$$$$$$', '""""""""""""', '^^^^^^^^^^^^'
      ];

      // Bucle para caracteres permitidos en pass FormLogin
      for ( let j = 0; j <= P_Permitidos.length - 1; j++) {
        pass.setValue(P_Permitidos[j]);
        expect(pass.valid).toBe(true);
        // Cuando sea verdadero !pass.valid no se cumple la condicion de verificacion
        if ( !pass.valid) {
          console.log('Fallo en el caracter ' + P_Permitidos[j] );
        }
      }

      // Bucle para caracteres no permitidos en pass FormLogin
      for ( let i = 0 ; i <= P_No_Permitidos.length - 1 ; i++ ) {
        pass.setValue(P_No_Permitidos[i]);
        expect(pass.valid).toBe(false);
        // Cuando sea verdadero no se cumple la condicion de verificacion
        if (pass.valid) {
          console.log('Fallo en el caracter ' + P_No_Permitidos[i] );
        }
      }
    });


  it('Evaluamos funcionamiento correcto funcion setDataSignOn', () => {
    const  user = 'TestUser123';
    const  pass =  'PasswordUser123';
    const spy = spyOn(component, 'encrypted').and.callThrough();
    const test = component.setDataSignOn(user, pass);
    expect(test.usuario).toEqual(user.trim());
    expect(test.password).toEqual(component.encrypted(pass.trim()));
    expect(spy).toHaveBeenCalledTimes(2);
  });



  it('Evaluamos funcionamiento correcto funcion setDataSignOff', () => {
    const  vacio = {};
    const test = component.setDataSignOff();
    expect(test).toEqual(vacio);
  });

  it('Verificamos funcion validarRespuesta(c)', () => {
    let c = '1004';
    expect(component.validarRespuesta(c)).toBeTruthy();

    c = '00000';
    expect(component.validarRespuesta(c)).toBeTruthy();

    c = '00001';
    expect(component.validarRespuesta(c)).toBeTruthy();

    c = '1009';
    expect(component.validarRespuesta(c)).toBeTruthy();

    c = '1002';
    expect(component.validarRespuesta(c)).toBeTruthy();

    c = '1001';
    expect(component.validarRespuesta(c)).toBeTruthy();

    c = '1003';
    expect(component.validarRespuesta(c)).toBeTruthy();

    c = '1006';
    expect(component.validarRespuesta(c)).toBeTruthy();

    c = '1005';
    expect(component.validarRespuesta(c)).toBeTruthy();

    c = '1013';
    expect(component.validarRespuesta(c)).toBeTruthy();

    c = '1016';
    expect(component.validarRespuesta(c)).toBeTruthy();

    c = '0000';
    expect(component.validarRespuesta(c)).toBeTruthy();

    c = 'N.A.';
    expect(component.validarRespuesta(c)).toBeFalsy();
  });



  it('Evaluacion funcionamiento de cancelarContraseñaPorCaducar()', () => {
    const vacio = {};
    // llamamos la funcion setDataSingOff , verificamos que nos devuelva vacio y se asigna a  DataRequestSingOff
    const test_setDataSignOff = component.setDataSignOff();
    expect(test_setDataSignOff).toEqual(vacio);
    expect(component.dataRequestSingOff).toEqual(vacio);
    // Espiamos la funcion setDataSingOff para verificar que se llame en cancelarContraseñaPorCaducar()
    const spy = spyOn(component, 'setDataSignOff').and.callThrough();
    // Creamos un espia para nuestro servicio
    const spyService = spyOn(servicio, 'serviceSignOff').and.callThrough();
    // Lllamos a la funcion cancelarContraseñaPorCaducar()
    component.cancelarContraseñaPorCaducar();
    // Verificamos la funcion espiada del component
    expect(spy).toHaveBeenCalled();
    // Verificamos el espia del servicio si fue llamado
    expect(spyService).toHaveBeenCalled();

    // mockeamos una respuesta del servicio para verificar la condiciones
    // Primero verificamos If
    const mockDataResponseSignOff = {
      codigoRespuesta : Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO
    };
    // forzamos al servicio a devolver la datamockeada
    const spyFakeServiceResponse = spyOn(servicio, 'serviceSignOff').and.callFake(() => {
      return of (mockDataResponseSignOff);
    });
    component.dataRequestSingOff = vacio;
     // llamamos la funcion
    component.cancelarContraseñaPorCaducar();
    expect(spyFakeServiceResponse).toHaveBeenCalled();


    // Verificamos Else
    mockDataResponseSignOff.codigoRespuesta = 'Else';
    component.cancelarContraseñaPorCaducar();
    expect(spyFakeServiceResponse).toHaveBeenCalled();


    // Forzamos al servicio a retornarnos un error
    const spyErrorService = spyOn(servicio, 'serviceSignOff').and.returnValue(throwError('Error Service'));
    component.cancelarContraseñaPorCaducar();
    expect(spyErrorService).toHaveBeenCalled();
  });




  it('Evaluacion de funcion Login(Values)', () => {

    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate');



    // Creamos nuestros espias para el servicio y componente que tenemos dentro de nuestra funcion Login
    const spyService = spyOn(servicio, 'serviceSignOn').and.callThrough();
    const spy = spyOn(component, 'setDataSignOn').and.callThrough();
    // Creamos un mockValues para pasarlo a la funcion Login(Values)
    const mockValues = {
      usuario : 'UserTest123',
      pass : 'PasswordTest123'
    };
    // Lllamamos la funcion Login()
    component.login(mockValues);
    // Verificamos que nuestros espias fueran llamados al ejecutarse la funcion Login
    expect(spyService).toHaveBeenCalled();
    expect(spy).toHaveBeenCalled();


    // Creamos un MockData para la primera condicion de respuesta del servicio
    const mockData = {
        'datosSesion': {
          'ningunaHuella': '0',
          'huellaVivaAmarillo': '0',
          'noHuellaViva': '0', 'nistRojo': '0',
          'umbralHuellaViva': '50.0',
          'nistAmarillo': '0',
          'huellaVivaRojo': '0',
          'validarSixser': false,
          'servicios': {'default': '301001', 'tipos':
          [{'descripcion': 'T33-ANSI', 'codigo': '301001'},
          {'descripcion': 'T33-ANSI', 'codigo': '301001'},
          {'descripcion': 'T35-WSQ', 'codigo': '301100'}]},
          'acciones': [{'descripcion': 'Mejor Huella',
          'codigo': 'mejorHuella', 'grupo': 'sixbio'},
          {'descripcion': 'Capturar', 'codigo': 'capturar', 'grupo': 'sixbio'},
          {'descripcion': 'Verificar', 'codigo': 'verificarReniec', 'grupo': 'sixbio'},
          {'descripcion': 'VerificarMOCard', 'codigo': 'verificarMoc', 'grupo': 'sixbio'},
          {'descripcion': 'Foto', 'codigo': 'consultarFoto', 'grupo': 'sixser'},
          {'descripcion': 'Datos', 'codigo': 'consultarConsolidado', 'grupo': 'sixser'}],
          'idSesion': '000000202103231248569920396',
          'validarHuellaViva': true,
          'validarDniValidador': true,
          'visualizar': 'true',
          'sixbioVersion': '1.6',
          'usuario': 'jburgos6',
          'noHuellaCapturada': '0',
          'validarIpCliente': true,
          'rolesSCA': [{'rol': 'EMP_ADM'}],
          'codigoRespuesta': '00000',
          'mensajeRespuesta': 'OPERACION REALIZADA SATISFACTORIAMENTE'},
          'listarRoles':
          {'Rol': [{
              'idRol': '2061', 'rol': 'sixbioweb_adm', 'tipo': 'OPCION', 'tipoRol': 'AGRUPADOR', 'estado': 'HABILITADO',
              'roles': ['EMP_ADM', 'administrador'],
              'permisos': null,
              'audUsuCreac': 'fbenito5',
              'audFecCreac': '23/02/2021',
              'audUsuModif': 'abenito9',
              'audFecModif': '26/02/2021'}],
              'codigoRespuesta': '0000',
              'mensajeRespuesta': 'OPERACION REALIZADA SATISFACTORIAMENTE'},
        codigoRespuesta: Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SCA,
        'mensajeRespuesta': 'OPERACION REALIZADA SATISFACTORIAMENTE'};

    // Fakeamos un retorno de nuestro servicio con un spy
    const spyFakeService = spyOn(servicio, 'serviceSignOn').and.callFake(() => {
      return of (mockData);
    });
    // Espiamos guardarDatosLogin del component
     const spyGuardarDatosLogin = spyOn(component, 'guardarDatosLogin').and.callThrough();




    // llamamos nuestra funcion Login del component
    component.login(mockValues);

    // Verificamos la llamada al servicio serviceSignOn
    expect(spyFakeService).toHaveBeenCalled();
    // Verificamos la llamada a GuardarDatosLogin
    expect(spyGuardarDatosLogin).toHaveBeenCalled();
    expect(spyRouter).toHaveBeenCalled();


    // Probamos el siguiente condicional de contraseña por caducar
    mockData.codigoRespuesta = Constant.COD_CONTRASENA_POR_CADUCAR;
    // Espiamos la funcion showModal
    const spyModal = spyOn(component, 'showModal').and.callThrough();
    // Lllamamos la funcion
    component.login(mockValues);

    // verificamos valor de messageModal
    expect(component.messageModal).toEqual(Constant.COD_CONTRASENA_POR_CADUCAR);
    // Verificamos que llamara nuestro modal
    expect(spyModal).toHaveBeenCalled();


    // Probamos el condicional de Primer Login
    mockData.codigoRespuesta = Constant.COD_PRIMER_LOGIN;
    // Lllamamos la funcion
    component.login(mockValues);
    // verificamos valor de messageModal
    expect(component.messageModal).toEqual(Constant.COD_PRIMER_LOGIN);
    // Verificamos que llamara nuestro modal
    expect(spyModal).toHaveBeenCalled();

    // Probamos la condicional de ValidarRespuesta(c) , probabamos cualquiera de sus valores validos
    // No se prueban todos ya que se probo en otro caso
    mockData.codigoRespuesta = '00000';
    component.login(mockValues);
    expect(component.validarRespuesta(mockData.codigoRespuesta)).toBeTruthy();

    // Caso que no se cumpla niguna de la anteriores
    mockData.codigoRespuesta = 'Ninguna de la anteriores';

    component.login(mockValues);
    expect(component.validarRespuesta(mockData.codigoRespuesta)).toBeFalsy();


    // En caso que el suscribe al servicio nos retorne un ERROR
    // Creamos nuestro espia que nos retorna un error del servicio
    const spyError = spyOn(servicio, 'serviceSignOn').and.returnValue(throwError('Error!!! Service Login'));
    // Lllamamos la funcion
    component.login(mockValues);

    // vericamos que sea llamado el servicio con error
    expect(spyError).toHaveBeenCalled();


  });


  it('Verificar funcion confirm()', () => {
    component.confirm();
    expect(component.aceptar).toBeTruthy();
  });

  it('Verificar funcion decline()', () => {
    component.decline();
    expect(component.aceptar).toBeFalsy();
  });

  it('Verificamos funcion showModal()', () => {
    // Espiamos al modal para verificar la funcion show
    const spyModal = spyOn(component.modal, 'show').and.callThrough();
    component.showModal();
    // Verificamos que fuera llamado la funcion show del modal
    expect(spyModal).toHaveBeenCalled();
    // Verificamos que aceptar sea false
    expect(component.aceptar).toBeFalsy();
  });





  it('Verficar funcion Evento()', () => {
    // Probamos la primera condicion del Switch de la funcion
    component.messageModal = Constant.COD_CONTRASENA_POR_CADUCAR;

    // Y caso de "CONFIRMACION" de cambio de contraseña por CADUCAR
    component.aceptar = true;

    // Inyectamos Router que fue previamente fakeado
    const router = TestBed.inject(Router);
    // Espiamos que llame a navigate router
    const spyRouter = spyOn(router, 'navigate');

    // Lllamamos la funcion evento()
    component.evento();

    // Verificamos que fuera llamado router
    expect(spyRouter).toHaveBeenCalled();


    // verificamos en caso contrario decline  (CERRAR O CLIC FUERA DEL MODAL)
    component.aceptar = false;
    const spyCancelarContraseñaPorCaducar = spyOn(component, 'cancelarContraseñaPorCaducar').and.callThrough();

    component.evento();
    // Verificamos que fuera llamanda la funcion CancelarContraseñaPorCaducar
    expect(spyCancelarContraseñaPorCaducar).toHaveBeenCalled();


    // Probamos la siguiente condicional del Switch Constant.COD_PRIMER_LOGIN
    component.messageModal = Constant.COD_PRIMER_LOGIN;
      // Y caso de "CONFIRMACION" de cambio de contraseña por PRIMER LOGIN
    component.aceptar = true;
    // Lllamamos la funcion evento()
    component.evento();
    // Verificamos que fuera llamado router
    expect(spyRouter).toHaveBeenCalled();

    // verificamos en caso contrario decline (CERRAR O CLIC FUERA DEL MODAL)
    component.aceptar = false;
    component.evento();
    // Solo devuelve return se puede verificar con un console.log





  });

/*

  it('Test Translate', () => {

    component.translate.get('Dashboard').subscribe((data: any) => {
      console.log('Test Translate ' + data);
      console.log(data);
     }); }
  );

console.log('traducido' +  component.translate.instant('Dashboard'));


*/

});
