import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { routerTransition } from '../router.animations';
import { IUsuario } from '../nucleo/interface/IUsuario';
import { IValidation } from '../nucleo/interface/IValidation';
import { ServicioService } from '../servicio/servicio.service';
import { Constant } from '../nucleo/constante/Constant';
// import { debug } from 'util';
import { ModalComponent } from '../layout/bs-component/components/modal/modal.component';
import { AlertComponent } from '../layout/bs-component/components/alert/alert.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Util } from '../nucleo/util/Util';
import { ToastComponent } from '../layout/bs-component/components/toast/toast.component';
import { DatosSesion } from '../nucleo/entity/DatoSesion';
import { TipoServicioVerificacion } from '../nucleo/entity/TipoServicioVerificacion';
import { Accion } from '../nucleo/entity/Accion';
import { RolesSCA } from '../nucleo/entity/RolesSCA';
import { FormBuilder } from '@angular/forms';
import { FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';
import { PipeTransform } from '@angular/core';
import { SpinnerService } from '../services/spinner.service';

declare const AesUtil: any;

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

    @ViewChild(ToastComponent, { static: true }) toast: ToastComponent;
    @ViewChild(ModalDirective, { static: false }) modal: ModalDirective;

    alert: AlertComponent;
    datosSesion: DatosSesion;
    footer_logo = Constant.FOOTER_LOGO;
    footer = Constant.FOOTER_NOVATRONIC;
    dataRequestSingOff: Object = {};
    entidad: IUsuario = {
        user: '',
        password: ''
    };

    dataRequest: Object = {};
    closeResult: string;
    util: Util;
    loginForm: FormGroup;
    aceptar = false;
    messageModal: string;
    showModal() {
        this.aceptar = false;
        this.modal.show();
    }

    constructor(public router: Router, private rutaActual: ActivatedRoute, public servicioService: ServicioService,
        private spinnerService: SpinnerService,
        public translate: TranslateService,
        private modalService: NgbModal, private fb: FormBuilder) {
        this.util = new Util(modalService);
    }
    // \u00f1\u00d1 son ñ y Ñ
    ngOnInit() {
        this.loginForm = this.fb.group({
            usuario: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(5),
                Validators.maxLength(20),
                Validators.pattern('[A-Za-z0-9]+')
            ])),
            pass: new FormControl('', Validators.compose([
                Validators.required,
                Validators.minLength(6),
                Validators.maxLength(16),
                Validators.pattern('[a-zA-Z0-9-_@\.\:\.\,\;\(\)\/\u00f1\u00d1]+')
            ])),

        });
    }

    confirm() {
        this.aceptar = true;
        this.modal.hide();
    }

    decline() {
        this.aceptar = false;
        this.modal.hide();
    }

    evento() {
        switch (this.messageModal) {
            case Constant.COD_CONTRASENA_POR_CADUCAR:
                if (this.aceptar) {
                    localStorage.setItem('isFirstLogin', 'true');
                    this.router.navigate(['/firstLogin']);
                } else {
                    this.cancelarContraseñaPorCaducar();
                }
                break;
            case Constant.COD_PRIMER_LOGIN:
                if (this.aceptar) {
                    localStorage.setItem('isFirstLogin', 'true');
                    this.router.navigate(['/firstLogin']);
                } else {
                    return;
                }
                break;
        }
    }

    login(values) {
        //   this.dataRequest = this.setDataSignOn(this.entidad.user, this.entidad.password);
        this.dataRequest = this.setDataSignOn(values.usuario, values.pass);
        this.servicioService.serviceSignOn(this.dataRequest).subscribe((dataResponse: any) => {
            if (dataResponse.codigoRespuesta === Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SCA) {
                localStorage.setItem('isLoggedin', 'true');
                debugger;
                localStorage.removeItem('isFirstLogin');
                this.guardarDatosLogin(dataResponse);
                // this.router.navigate(['/verificacion']);
                localStorage.setItem('usuarioLogueado', values.usuario);

                if (localStorage.getItem('RolUsuario') === 'EMP_VER') {
                    this.router.navigate(['/home/verificacionBiometrica']);
                } else if (localStorage.getItem('RolUsuario') === 'EMP_ADM') {
                    this.router.navigate(['/home/gestion-usuarios']);
                }

                //     this.router.navigate(['/home/gestion-usuarios']);

            } else if (dataResponse.codigoRespuesta === Constant.COD_CONTRASENA_POR_CADUCAR) {

                localStorage.setItem('changePasswordPorCaducar', 'true');
                this.messageModal = dataResponse.codigoRespuesta;
                this.showModal();

            } else if (dataResponse.codigoRespuesta === Constant.COD_PRIMER_LOGIN) {
                this.messageModal = dataResponse.codigoRespuesta;
                this.showModal();

            } else if (this.validarRespuesta(dataResponse.codigoRespuesta)) {

                this.toast.addToast({ title: 'Error', msg: dataResponse.codigoRespuesta, timeOut: 5000, type: 'error' });
                localStorage.clear();
            } else {

                this.toast.addToast({ title: 'Error', msg: 'Error de conectividad', timeOut: 5000, type: 'error' });
            }

        }, error => {
            this.toast.addToast({ title: 'Error', msg: 'Error de conectividad', timeOut: 5000, type: 'error' });
            localStorage.clear();
        });

    }






    setDataSignOn(user: String, password: String) {
        return {
            usuario: user.trim(),
            password: this.encrypted(password.trim())
        };
    }

    setDataSignOff() {
        return {};
    }

    guardarDatosLogin(datosLoginResponse) {
        this.datosSesion = new DatosSesion();
        this.datosSesion.ningunaHuella = datosLoginResponse.datosSesion.ningunaHuella;
        this.datosSesion.huellaVivaAmarillo = datosLoginResponse.datosSesion.huellaVivaAmarillo;
        this.datosSesion.noHuellaViva = datosLoginResponse.datosSesion.noHuellaViva;
        this.datosSesion.nistRojo = datosLoginResponse.datosSesion.nistRojo;
        this.datosSesion.umbralHuellaViva = datosLoginResponse.datosSesion.umbralHuellaViva;
        this.datosSesion.nistAmarillo = datosLoginResponse.datosSesion.nistAmarillo;
        this.datosSesion.huellaVivaRojo = datosLoginResponse.datosSesion.huellaVivaRojo;
        this.datosSesion.validarSixser = datosLoginResponse.datosSesion.validarSixser;
        // servicios
        this.datosSesion.serviciosVerificacion = datosLoginResponse.datosSesion.servicios;
        this.datosSesion.serviciosVerificacion.default = datosLoginResponse.datosSesion.servicios.default;


        let arrayTiposServicioVerificacion: Array<TipoServicioVerificacion>;
        arrayTiposServicioVerificacion = datosLoginResponse.datosSesion.servicios.tipos;
        this.datosSesion.serviciosVerificacion.tiposServicioVerificacion = new Array<TipoServicioVerificacion>();
        arrayTiposServicioVerificacion.forEach(tiposServicioVerificacion => {

            const tiposServicio = new TipoServicioVerificacion();
            tiposServicio.descripcion = tiposServicioVerificacion.descripcion;
            tiposServicio.codigo = tiposServicioVerificacion.codigo;

            this.datosSesion.serviciosVerificacion.tiposServicioVerificacion.push(tiposServicio);

        });



        // servicios


        // acciones


        let arrayAcciones: Array<Accion>;
        arrayAcciones = datosLoginResponse.datosSesion.acciones;

        this.datosSesion.acciones = new Array<Accion>();


        arrayAcciones.forEach(acciones => {

            const accionTemp = new Accion();
            accionTemp.descripcion = acciones.descripcion;
            accionTemp.codigo = acciones.codigo;
            accionTemp.codigo = acciones.grupo;
            this.datosSesion.acciones.push(accionTemp);
        });

        // acciones

        this.datosSesion.idSesion = datosLoginResponse.datosSesion.idSesion;
        this.datosSesion.validarHuellaViva = datosLoginResponse.datosSesion.validarHuellaViva;
        this.datosSesion.validarDniValidador = datosLoginResponse.datosSesion.validarDniValidador;
        this.datosSesion.visualizar = datosLoginResponse.datosSesion.visualizar;
        this.datosSesion.sixbioVersion = datosLoginResponse.datosSesion.sixbioVersion;
        this.datosSesion.usuario = datosLoginResponse.datosSesion.usuario;
        this.datosSesion.noHuellaCapturada = datosLoginResponse.datosSesion.noHuellaCapturada;
        this.datosSesion.validarIpCliente = datosLoginResponse.datosSesion.validarIpCliente;

        // rolesSCA

        let arrayRolesSCA: Array<RolesSCA>;




        arrayRolesSCA = datosLoginResponse.datosSesion.rolesSCA;


        this.datosSesion.rolesSCA = new Array<RolesSCA>();



        arrayRolesSCA.forEach(rolesSCA => {

            const rolesSCATemp = new RolesSCA();

            /*let arrayPermisos: Array<string>;
            arrayPermisos = rolesSCA.permisos;
            arrayPermisos.forEach(permiso => {
                rolesSCATemp.permisos.push(permiso);
            });
            */

            rolesSCATemp.rol = rolesSCA.rol;

            this.datosSesion.rolesSCA.push(rolesSCATemp);
        });

        // rolesSCA

        this.datosSesion.codigoRespuesta = datosLoginResponse.datosSesion.codigoRespuesta;
        this.datosSesion.mensajeRespuesta = datosLoginResponse.datosSesion.mensajeRespuesta;
        this.datosSesion.rolesSCA.forEach(element => {
            if (element.rol === Constant.ROL_ADM || element.rol === Constant.ROL_CST) {
                localStorage.setItem('RolUsuario', element.rol);
            }
        });
        localStorage.setItem('datosSesion', JSON.stringify(this.datosSesion));
    }

    encrypted(password: String) {
        const iv = 'F27D5C9927726BCEFE7510B1BDD3D137';
        const salt = '3FF2EC019C627B945225DEBAD71A01B6985FE84C95A70EB132882F88C0A59A55';
        const keySize = 128;
        const iterationCount = 10000;
        const passPhrase = 'sixbiowebaclient';
        const aesUtil = new AesUtil(keySize, iterationCount);
        const encrypt = aesUtil.encrypt(salt, iv, passPhrase, password);

        return encrypt;
    }

    validarRespuesta(c) {
        // eslint-disable-next-line max-len
        if (c === '1004' || c === '00000' || c === '00001' || c === '1009' || c === '1002' || c === '1003' || c === '1001' || c === '0000' || c === '1005' || c === '1013' || c === '1006' || c === '1016') {
            return true;
        } else {
            return false;
        }
    }

    cancelarContraseñaPorCaducar() {
        this.dataRequestSingOff = this.setDataSignOff();
        this.servicioService.serviceSignOff(this.dataRequestSingOff).subscribe((dataResponseSignOff: any) => {
            if (dataResponseSignOff.codigoRespuesta === Constant.COD_OPERACION_SATISFACTORIA_SIGNON_SIXBIO) {
                localStorage.clear();
                // this.redirigirLogin();
            } else {
                localStorage.clear();
                // this.redirigirLogin();
            }
        }, error => {
            localStorage.clear();
            // this.redirigirLogin();
        });
        return;
    }
}
