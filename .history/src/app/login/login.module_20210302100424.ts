import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { BsComponentModule } from '../layout/bs-component/bs-component.module';
import { FooterComponent } from '../layout/components/footer/footer.component';

@NgModule({
    imports:
    [
        CommonModule,
        TranslateModule,
        LoginRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        BsComponentModule
    ],
    declarations: [LoginComponent]
})
export class LoginModule {}
