import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { BsComponentModule } from '../layout/bs-component/bs-component.module';

@NgModule({
    imports:
    [
        CommonModule,
        TranslateModule,
        LoginRoutingModule,
        FormsModule,
        BsComponentModule
    ],
    declarations: [LoginComponent]
})
export class LoginModule {}
