(function (window) {
    window.__env = window.__env || {};

    // Whether or not to enable debug mode
    // Setting this to false will disable console output
    window.__env.enableDebug = true;
    window.__env.servicio = 'localhost',
    window.__env.puerto = '8180';
    window.__env.app = 'SIXBIO-webcore';
}(this));
