import { browser, by, element } from 'protractor';

export class AppPage {
   navigateTo(s: string) {
    return browser.get(browser.baseUrl + s);
  }

   getElementText(s: string) {
    return element(by.css(s)).getText();
  }

  getFormControl (s: string) {
    return element(by.css('[formControlName="' + s + '"]'));
  }

  getValueFormControl ( s: string) {
    return this.getFormControl(s).getAttribute('value');
  }

  getLocalStorageItem( s: string ) {
    return browser.executeScript('return window.localStorage.getItem(\'' + s + '\');');
  }

}
