import { browser, by, element, logging } from 'protractor';
import { AppPage } from './app.po';

describe('Pruebas E2E Login', () => {
  let page: AppPage;

  beforeEach(async () => {
    page = new AppPage();
   });

  it('Debe mostrar titulo Sistema de Verificación de Identidad en Login', async () => {
    await page.navigateTo('/');
    expect(await page.getTitleText('app-login h1')).toEqual('Sistema de Verificación de Identidad');
  });

  it('Verificamos el tipeo en User y Login', async () => {
     page.navigateTo('/');
        element(by.css('app-login input[formControlName=usuario]')).sendKeys('TestUser');

    element(by.css('app-login input[formControlName=usuario]')).getText().then(function(text) {
      console.log(text);
    });
  //  await    element(by.css('app-login input[formControlName=pass]')).sendKeys('');



  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
