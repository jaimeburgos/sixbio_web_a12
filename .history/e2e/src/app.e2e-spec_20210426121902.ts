import { CONNREFUSED } from 'node:dns';
import { browser, by, element, logging } from 'protractor';
import { AppPage } from './app.po';

describe('Pruebas E2E Login', () => {
  let page: AppPage;

  beforeEach( async() => {
    page = new AppPage();
   });

  it('Debe mostrar titulo Sistema de Verificación de Identidad en Login', async () => {
    await page.navigateTo('/');
    expect(await page.getElementText('app-login h1')).toEqual('Sistema de Verificación de Identidad');          console.log('Test');

  });

  it('Verificamos el tipeo en User y Login', async () => {
    await  page.navigateTo('/');
    await page.getFormControl('usuario').sendKeys('$$$$$$1');
    // tslint:disable-next-line:prefer-const
    let user = await page.getValueFormControl('usuario');
    expect(user).toEqual('1');
    console.log('Valor es ' + user );
    await page.getFormControl('usuario').clear();
    console.log('Valor despues de limpiar es ' + user);


  });

/*
  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });*/


});
