import { CONNREFUSED } from 'node:dns';
import { browser, by, element, logging } from 'protractor';
import { AppPage } from '../app.po';

describe('Pruebas E2E Login', () => {
  let page: AppPage;

  beforeEach( async() => {
    page = new AppPage();
   });

  it('Debe mostrar titulo Sistema de Verificación de Identidad en Login', async () => {
    await page.navigateTo('/');
    expect(await page.getElementText('app-login h1')).toEqual('Sistema de Verificación de Identidad');
  });


  it('Verificamos los campos User y Login', async () => {
    await page.navigateTo('/');
    await page.getFormControl('usuario').sendKeys('$$$$$$1');
    // Validamos que el valor sea igual a 1 ya que $ es un caracter no permitido
    expect(await page.getValueFormControl('usuario')).toEqual('1');
    // Verificamos los caracteres permitidos usuario
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b'];
    for (let i = 0; i <= U_Permitidos.length - 1; i++) {
      await page.getFormControl('usuario').clear();
      await page.getFormControl('usuario').sendKeys(U_Permitidos[i]);
      expect(await page.getValueFormControl('usuario')).toEqual(U_Permitidos[i]);
      if (await page.getValueFormControl('usuario') !== U_Permitidos[i]) {
        console.log('Fallo usuario en ' + U_Permitidos[i]);
      }
    }
    // Verificamos los caracteres no permitidos para usuario
    const U_No_Permitidos =
      ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
        '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
        '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@☺☻♥♠○◘♣•', '[[[[]]]]', '          '];
    for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
      await page.getFormControl('usuario').clear();
      await page.getFormControl('usuario').sendKeys(U_No_Permitidos[i]);
      expect(await page.getValueFormControl('usuario')).toEqual('');
      if (await page.getValueFormControl('usuario') !== '') {
        console.log('Fallo usuario en ' + U_No_Permitidos[i]);
      }
    }

    // Limpiamos nuestro input para realizar otra prueba
    await page.getFormControl('usuario').clear();
    // Verificamos la longitud maxima del campo usuario
    await page.getFormControl('usuario').sendKeys('123456789012345678901');
    expect(await page.getValueFormControl('usuario')).toEqual('12345678901234567890');

    // Verificamos los caracteres no permitidos para password
    const P_No_Permitidos = [
      '☺☻♥♠○◘♣•', '[[[[]]]]', '{{{{{{}}}}}}}', '          ', '???????¿¿¿¿¿¿', '´´´´´´´´', ',,,,,,,,,,,', ';;;;;;;;;;;;', 'ññññññññññ', 'ÑÑÑÑÑÑÑ',
      '============', '<<<<<>>>>>', '**********', '++++++++++', '$$$$$$$$', '""""""""""""', '^^^^^^^^^^^^'];
    for (let i = 0; i <= P_No_Permitidos.length - 1; i++) {
      await page.getFormControl('pass').clear();
      await page.getFormControl('pass').sendKeys(P_No_Permitidos[i]);
      expect(await page.getValueFormControl('pass')).toEqual('');
      if (await page.getValueFormControl('pass') !== '') {
        console.log('Fallo pass en ' + P_No_Permitidos[i]);
      }
    }

 // Verificamos los valores permitidos para password
    const P_Permitidos = [
      ':::::::::', '............',  '--------------',
    '______________', '((((((((((', '))))))))))', '@@@@@@@@@', '/////////'
    ];
    for (let i = 0; i <= P_Permitidos.length - 1; i++) {
      await page.getFormControl('pass').clear();
      await page.getFormControl('pass').sendKeys(P_Permitidos[i]);
      expect(await page.getValueFormControl('pass')).toEqual(P_Permitidos[i]);
      if (await page.getValueFormControl('pass') !== P_Permitidos[i]) {
        console.log('Fallo pass en' + P_Permitidos[i]);
      }
    }

    // Verificamos la longitud maxima del campo pass
    await page.getFormControl('pass').clear();
    await page.getFormControl('pass').sendKeys('12345678901234567');
    expect(await page.getValueFormControl('pass')).toEqual('1234567890123456');
  });



  it('Verificamos mensajes de campo User', async () => {
    await page.navigateTo('/');
    await page.getFormControl('usuario').sendKeys('123');
    await page.getFormControl('pass').sendKeys('');
    expect(await page.getElementText('#msgMinLogin')).toEqual('Tamaño mínimo de 5 caracteres');
    console.log (' valor mensaje testing es ' + await page.getElementText('#msgMinLogin'));

    await page.navigateTo('/');
    await page.getFormControl('usuario').sendKeys('');
    await page.getFormControl('pass').sendKeys('');
    expect(await page.getElementText('#msgReqLogin')).toEqual('Ingrese usuario');
    console.log (' valor mensaje testing es ' + await page.getElementText('#msgReqLogin'));
  });

  it('Verificamos mensaje de campo Password', async() => {
    await page.navigateTo('/');
    await page.getFormControl('pass').sendKeys('123');
    await page.getFormControl('usuario').sendKeys('');
    expect(await page.getElementText('#msgMinPass')).toEqual('Tamaño mínimo de 6 caracteres');
    console.log (' valor mensaje  password min es ' + await page.getElementText('#msgMinPass'));

    await page.navigateTo('/');
    await page.getFormControl('pass').sendKeys('');
    await page.getFormControl('usuario').sendKeys('');
    expect(await page.getElementText('#msgReqPass')).toEqual('Ingrese contraseña');
    console.log (' valor mensaje password requerido es ' + await page.getElementText('#msgReqPass'));
  });

  it('Verificamos logeo', async() => {
    await page.navigateTo('/');
    await page.getFormControl('usuario').sendKeys('jburgos6');
    await page.getFormControl('pass').sendKeys('Admin1');
    browser.sleep(100);
    console.log ('boton habilitado  es ' + await element(by.css('app-login #TestButton')).isEnabled());
    await element(by.css('app-login #PruebaButton')).click();
  });

/*
  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
*/
});
