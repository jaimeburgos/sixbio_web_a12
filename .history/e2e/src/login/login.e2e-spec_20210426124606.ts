import { CONNREFUSED } from 'node:dns';
import { browser, by, element, logging } from 'protractor';
import { AppPage } from '../app.po';

describe('Pruebas E2E Login', () => {
  let page: AppPage;

  beforeEach( async() => {
    page = new AppPage();
   });

  it('Debe mostrar titulo Sistema de Verificación de Identidad en Login', async () => {
    await page.navigateTo('/');
    expect(await page.getElementText('app-login h1')).toEqual('Sistema de Verificación de Identidad');
  });

  it('Verificamos los campos User y Login', async () => {
    await page.navigateTo('/');
    await page.getFormControl('usuario').sendKeys('$$$$$$1');
    // Validamos que el valor sea igual a 1 ya que $ es un caracter no permitido
    expect(await page.getValueFormControl('usuario')).toEqual('1');

    // Limpiamos nuestro input para realizar otra prueba
    await page.getFormControl('usuario').clear();
    // Verificamos la longitud maxima del campo usuario
    await page.getFormControl('usuario').sendKeys('123456789012345678901');
    expect(await page.getValueFormControl('usuario')).toEqual('12345678901234567890');

    const U_No_Permitidos =
    ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
    '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
    '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@O☺☻♥♠○◘♣•', '[[[[]]]]', '          '];

    for ( let i = 0 ; i <= U_No_Permitidos.length - 1 ; i++ ) {
      await page.getFormControl('pass').clear();
      await page.getFormControl('usuario').sendKeys(U_No_Permitidos[i]);
      expect(await page.getValueFormControl('usuario')).toEqual('');
      // Cuando sea diferente de '' fallo el caracter
      if (await page.getValueFormControl('usuario') !== '') {
        console.log('Fallo en el caracter No Permitido ' + U_No_Permitidos[i] );
      }
    }


    // Verificamos la longitud maxima del campo pass
    await page.getFormControl('pass').sendKeys('12345678901234567');
    expect(await page.getValueFormControl('pass')).toEqual('1234567890123456');
  });

/*
  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });*/


});
