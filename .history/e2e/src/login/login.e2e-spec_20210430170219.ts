import { CONNREFUSED } from 'node:dns';
import { $$, Browser, browser, by, element, logging, protractor } from 'protractor';
import { AppPage } from '../app.po';

describe('Pruebas E2E Login', () => {
  let page: AppPage;

  beforeEach( async() => {
    page = new AppPage();
    await browser.driver.manage().window().maximize();

   });

  it('Debe mostrar titulo Sistema de Verificación de Identidad en Login', async () => {
    await page.navigateTo('');
    expect(await page.getElementText('app-login h1')).toEqual('Sistema de Verificación de Identidad');
  });


  it('Verificamos las validaciones de entrada de caracteres permitidos y no permitidos campos User y Login', async () => {
    await page.navigateTo('');
    await page.getFormControl('usuario').sendKeys('$$$$$$1');
    // Validamos que el valor sea igual a 1 ya que $ es un caracter no permitido
    expect(await page.getValueFormControl('usuario')).toEqual('1');
    // Verificamos los caracteres permitidos usuario
    const U_Permitidos = ['aaaaaaaa', 'cccccbbbbb', 'c123bbb4b'];
    for (let i = 0; i <= U_Permitidos.length - 1; i++) {
      await page.getFormControl('usuario').clear();
      await page.getFormControl('usuario').sendKeys(U_Permitidos[i]);
      expect(await page.getValueFormControl('usuario')).toEqual(U_Permitidos[i]);
      if (await page.getValueFormControl('usuario') !== U_Permitidos[i]) {
        console.log('Fallo usuario en ' + U_Permitidos[i]);
      }
    }
    // Verificamos los caracteres no permitidos para usuario
    const U_No_Permitidos =
      ['**********', '///////', '$$$$$$$$', '::::::::::', '.........', '------------', '""""""""',
        '\'\'\'\'\'\'\'\'\'', '%%%%%%%%%%', '(()))()))', '???????¿¿¿¿¿¿', '´´´´´´´´', '============',
        '<<<<<>>>>>', ';;;;;;;;;;;', ',,,,,,,,,', '@@@☺☻♥♠○◘♣•', '[[[[]]]]', '          '];
    for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
      await page.getFormControl('usuario').clear();
      await page.getFormControl('usuario').sendKeys(U_No_Permitidos[i]);
      expect(await page.getValueFormControl('usuario')).toEqual('');
      if (await page.getValueFormControl('usuario') !== '') {
        console.log('Fallo usuario en ' + U_No_Permitidos[i]);
      }
    }

    // Limpiamos nuestro input para realizar otra prueba
    await page.getFormControl('usuario').clear();
    // Verificamos la longitud maxima del campo usuario
    await page.getFormControl('usuario').sendKeys('123456789012345678901');
    expect(await page.getValueFormControl('usuario')).toEqual('12345678901234567890');

    // Verificamos los caracteres no permitidos para password
    const P_No_Permitidos = [
      '☺☻♥♠○◘♣•', '[[[[]]]]', '{{{{{{}}}}}}}', '          ', '???????¿¿¿¿¿¿', '´´´´´´´´', ',,,,,,,,,,,', ';;;;;;;;;;;;', 'ññññññññññ', 'ÑÑÑÑÑÑÑ',
      '============', '<<<<<>>>>>', '**********', '++++++++++', '$$$$$$$$', '""""""""""""', '^^^^^^^^^^^^'];
    for (let i = 0; i <= P_No_Permitidos.length - 1; i++) {
      await page.getFormControl('pass').clear();
      await page.getFormControl('pass').sendKeys(P_No_Permitidos[i]);
      expect(await page.getValueFormControl('pass')).toEqual('');
      if (await page.getValueFormControl('pass') !== '') {
        console.log('Fallo pass en ' + P_No_Permitidos[i]);
      }
    }

 // Verificamos los valores permitidos para password
    const P_Permitidos = [
      ':::::::::', '............',  '--------------',
    '______________', '((((((((((', '))))))))))', '@@@@@@@@@', '/////////'
    ];
    for (let i = 0; i <= P_Permitidos.length - 1; i++) {
      await page.getFormControl('pass').clear();
      await page.getFormControl('pass').sendKeys(P_Permitidos[i]);
      expect(await page.getValueFormControl('pass')).toEqual(P_Permitidos[i]);
      if (await page.getValueFormControl('pass') !== P_Permitidos[i]) {
        console.log('Fallo pass en' + P_Permitidos[i]);
      }
    }

    // Verificamos la longitud maxima del campo pass
    await page.getFormControl('pass').clear();
    await page.getFormControl('pass').sendKeys('12345678901234567');
    expect(await page.getValueFormControl('pass')).toEqual('1234567890123456');
  });



  it('Verificamos mensajes de campo User', async () => {
    await page.navigateTo('');
    await browser.sleep(500);

    await page.getFormControl('usuario').sendKeys('123');
    await browser.sleep(500);
    await page.getFormControl('pass').sendKeys('');
    await browser.sleep(500);
   // console.log (' valor mensaje testing es ' + await page.getElementText('#msgMinLogin'));
    await  expect(await page.getElementText('#msgMinLogin')).toEqual('Tamaño mínimo de 5 caracteres');

    await page.navigateTo('');
    await browser.sleep(500);

    await page.getFormControl('usuario').sendKeys('');
    await browser.sleep(500);

    await page.getFormControl('pass').sendKeys('');
    await browser.sleep(500);

  //  console.log (' valor mensaje testing es ' + await page.getElementText('#msgReqLogin'));
    await  expect(await page.getElementText('#msgReqLogin')).toEqual('Ingrese usuario');

  });

  it('Verificamos mensajes de campo Password', async() => {
    await page.navigateTo('');
    await browser.sleep(500);

    await page.getFormControl('pass').sendKeys('123');
    await page.getFormControl('usuario').sendKeys('');
    await browser.sleep(500);

   // console.log (' valor mensaje  password min es ' + await page.getElementText('#msgMinPass'));
    await  expect(await page.getElementText('#msgMinPass')).toEqual('Tamaño mínimo de 6 caracteres');

    await page.navigateTo('');
    await browser.sleep(500);

    await page.getFormControl('pass').sendKeys('');
    await page.getFormControl('usuario').sendKeys('');
    await browser.sleep(500);

  //  console.log (' valor mensaje password requerido es ' + await page.getElementText('#msgReqPass'));
    await  expect(await page.getElementText('#msgReqPass')).toEqual('Ingrese contraseña');

  });

  it('Verificamos mensaje Error Usuario y/o contraseña incorrecta', async () => {
    await page.navigateTo('');
    await browser.sleep(1000);

    await page.getFormControl('usuario').sendKeys('dfgfdgfdgfdgfd');
    await page.getFormControl('pass').sendKeys('fdgfdgfdgfdgfgd');
    await browser.sleep(2000);
    await browser.executeScript('arguments[0].click();', element(by.css('button[name="submit"]')));
    await browser.sleep(1000);
  //  console.log('Toast Dice 1  : ' + await element(by.css('div[role=alertdialog]')).getText());
    await browser.sleep(500);

  //  console.log('Contain es ' + await element(by.cssContainingText('div[role=alertdialog]', 'Usuario y/o Contraseña incorrecta, intente nuevamente.')).getText());

  //  await console.log( 'Exe es ' +  await browser.executeScript('return document.querySelector("div[role=alertdialog]").innerHTML'));
   // console.log('Toast Dice 3 : ' + await element(by.css('div[aria-live=polite]')).getText());
  //  console.log('Toast Dice 4  : ' + await element(by.id('toast-container')).getText());
    await  expect(await browser.executeScript('return document.querySelector("div[role=alertdialog]").innerHTML')).toEqual('Usuario y/o Contraseña incorrecta, intente nuevamente.');

    // await   browser.wait(protractor.ExpectedConditions.alertIsPresent()); aria-live="polite"

  });


  it('Verificamos login satisfactorio', async () => {
    await page.navigateTo('');
    await page.getFormControl('usuario').sendKeys('jburgos7');
    await page.getFormControl('pass').sendKeys('Admin1');
    //  console.log('boton habilitado  es ' + await element(by.css('button[name="submit"]')).isEnabled());
    await  expect(await element(by.css('button[name="submit"]')).isEnabled()).toBeTruthy();
    await expect(browser.getCurrentUrl()).toContain('login');
    //   await element(by.css('button[name="submit"]')).click();
    // await element(by.css('button[name="submit"]')).click();
    //    await element(by.xpath('//form')).submit();
    // await browser.actions().mouseMove(element(by.css('button[name="submit"]'))).click().perform();
    // await browser.executeScript('arguments[0].click();', element(by.css('button[name="submit"]')));
    await browser.sleep(1000);
    // Solo para Firefox
    //  await  element(by.css('button[name="submit"]')).click();
    // Instruccion valido para Click tanto en FireFox como Chrome
    await browser.executeScript('arguments[0].click();', element(by.css('button[name="submit"]')));
    await browser.sleep(1000);
    await expect(browser.getCurrentUrl()).toContain('home/gestion-usuarios');
    await expect(await page.getLocalStorageItem('RolUsuario')).not.toBeNull();
    await expect(await page.getLocalStorageItem('RolUsuario')).not.toBeUndefined();
  });



/*
  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });*/

});
