
import { browser, by, element } from 'protractor';
import { AppPage } from '../app.po';

describe('Pruebas E2E First-Login', () => {
  let page: AppPage;

  beforeEach( async() => {
    page = new AppPage();
    await browser.driver.manage().window().maximize();
  });

  it('Verificamos mensajes reseteo de contraseña', async () => {
    await page.navigateTo('');
    await browser.sleep(1000);
    await page.getFormControl('usuario').sendKeys( browser.params.USER_RESETEO);
    await page.getFormControl('pass').sendKeys( browser.params.PASS_RESETEO);
    await browser.executeScript('arguments[0].click();', element(by.css('button[name="submit"]')));
    await browser.sleep(1000);

    await browser.sleep(1000);
    await expect(await browser.executeScript('return document.querySelector("#msgModalLogin").innerHTML')).toEqual('Primer Login, se le sugiere realizar el cambio de contraseña.');
    await browser.sleep(500);
    await browser.executeScript('arguments[0].click();', element(by.buttonText('Confirmar')));
    await browser.sleep(500);
    await expect( await browser.getCurrentUrl()).toContain('firstLogin');

    await page.getFormControl('old').sendKeys( '');
    await page.getFormControl('new').sendKeys( '');
    await page.getFormControl('confirm').sendKeys( '');
    await page.getFormControl('old').sendKeys( '');
    await browser.sleep(1000);

    await  expect(await page.getElementText('#msgFirstReqOld')).toEqual('Ingrese contraseña antigua');
    await  expect(await page.getElementText('#msgFirstReqNew')).toEqual('Ingrese contraseña nueva');
    await  expect(await page.getElementText('#msgFirstReqConfirm')).toEqual('Ingrese contraseña de confirmacion');
    await expect(await page.getElementText('h1')).toEqual('Cambiar Contraseña');
    await  expect(await element(by.css('button[name="submit"]')).isEnabled()).toBeFalsy();


    await page.getFormControl('old').sendKeys( '123');
    await page.getFormControl('new').sendKeys( '123');
    await page.getFormControl('confirm').sendKeys( '123');
    await page.getFormControl('old').sendKeys( '1');
    await browser.sleep(1000);

    await  expect(await page.getElementText('#msgFirstMinOld')).toEqual('Tamaño mínimo de 6 caracteres');
    await  expect(await page.getElementText('#msgFirstMinNew')).toEqual('Tamaño mínimo de 6 caracteres');
    await  expect(await page.getElementText('#msgFirstMinConfirm')).toEqual('Tamaño mínimo de 6 caracteres');
    await  expect(await element(by.css('button[name="submit"]')).isEnabled()).toBeFalsy();
  });


  it('Verificamos navegacion de FirstLogin a Login', async () => {
    await page.navigateTo('');
    await browser.sleep(1000);
    await page.getFormControl('usuario').sendKeys( browser.params.USER_RESETEO);
    await page.getFormControl('pass').sendKeys( browser.params.PASS_RESETEO);
    await browser.executeScript('arguments[0].click();', element(by.css('button[name="submit"]')));
    await browser.sleep(1000);

    await browser.sleep(1000);
    await expect(await browser.executeScript('return document.querySelector("#msgModalLogin").innerHTML')).toEqual('Primer Login, se le sugiere realizar el cambio de contraseña.');
    await browser.sleep(500);
    await browser.executeScript('arguments[0].click();', element(by.buttonText('Confirmar')));
    await browser.sleep(500);
    await expect( await browser.getCurrentUrl()).toContain('firstLogin');
    await browser.sleep(500);
    await browser.executeScript('arguments[0].click();', element(by.css('button[name="cancel"]')));
    await expect( await browser.getCurrentUrl()).toContain('login');
  });



  xit('Verificamos modal de confirmacion cambio contraseña', async () => {
    await page.navigateTo('');
    await browser.sleep(1000);
    await page.getFormControl('usuario').sendKeys( browser.params.USER_RESETEO);
    await page.getFormControl('pass').sendKeys( browser.params.PASS_RESETEO);
    await browser.executeScript('arguments[0].click();', element(by.css('button[name="submit"]')));
    await browser.sleep(1000);

    await browser.sleep(1000);
    await expect(await browser.executeScript('return document.querySelector("#msgModalLogin").innerHTML')).toEqual('Primer Login, se le sugiere realizar el cambio de contraseña.');
    await browser.sleep(500);
    await browser.executeScript('arguments[0].click();', element(by.buttonText('Confirmar')));
    await browser.sleep(500);
    await expect(await browser.getCurrentUrl()).toContain('firstLogin');
    await browser.sleep(500);

    await page.getFormControl('old').sendKeys('sdgfdgdfgfdgf');
    await page.getFormControl('new').sendKeys('Test123FD');
    await page.getFormControl('confirm').sendKeys('Test123FD');
    await expect(await element(by.css('button[name="submit"]')).isEnabled()).toBeTruthy();
    await browser.executeScript('arguments[0].click();', element(by.css('button[name="submit"]')));
    await browser.sleep(1000);
    await expect(await browser.executeScript('return document.querySelector("#msgModalLogin").innerHTML'))
      .toEqual('¿Está seguro de confirmar el cambio de contraseña?');
      await browser.sleep(500);

    await browser.executeScript('arguments[0].click();', element(by.buttonText('Confirmar')));
    await browser.sleep(3000);


    await expect(await browser.executeScript('return document.querySelector("div[role=alertdialog]").innerHTML')).toEqual('Usuario y/o Contraseña incorrecta, intente nuevamente.');

  });



  it('Verificamos Mensaje La contraseña de confirmacion no es igual a la nueva', async () => {
    await page.navigateTo('');
    await browser.sleep(1000);
    await page.getFormControl('usuario').sendKeys( browser.params.USER_RESETEO);
    await page.getFormControl('pass').sendKeys( browser.params.PASS_RESETEO);
    await browser.executeScript('arguments[0].click();', element(by.css('button[name="submit"]')));
    await browser.sleep(1000);

    await browser.sleep(1000);
    await expect(await browser.executeScript('return document.querySelector("#msgModalLogin").innerHTML')).toEqual('Primer Login, se le sugiere realizar el cambio de contraseña.');
    await browser.sleep(500);
    await browser.executeScript('arguments[0].click();', element(by.buttonText('Confirmar')));
    await browser.sleep(500);
    await expect(await browser.getCurrentUrl()).toContain('firstLogin');
    await browser.sleep(500);

    await page.getFormControl('old').sendKeys('sdgfdgdfgfdgf');
    await page.getFormControl('new').sendKeys('Test123FD');
    await page.getFormControl('confirm').sendKeys('Test123FD');
    await browser.sleep(500);

    await expect(await element(by.css('button[name="submit"]')).isEnabled()).toBeTruthy();
    await browser.executeScript('arguments[0].click();', element(by.css('button[name="submit"]')));
    await browser.sleep(1000);
    await expect(await browser.executeScript('return document.querySelector("div[role=alertdialog]").innerHTML')).toEqual('La contraseña de confirmacion no es igual a la nueva');
  });










});
