import { browser, by, element } from 'protractor';

export class AppPage {
  async navigateTo(s: string): Promise<unknown> {
    return browser.get(browser.baseUrl + s);
  }

  async getElementText(s: string): Promise<string> {
    return element(by.model(s)).getText();
  }
}
