import { browser, by, element } from 'protractor';

export class AppPage {
  async navigateTo(s: string): Promise<unknown> {
    return browser.get(browser.baseUrl + s);
  }

  async getElementText(s: string): Promise<string> {
    return element(by.css(s)).getText();
  }

  async getFormControl (s: string): Promise<string> {
    return element(by.css('[formControlName="' + s + '"]'));
  }

  async getValueFormControl ( s: string): Promise<string>  {
    return this.getFormControl(s).getAttribute('value');
  }

  async getLocalStorageItem( s: string ): Promise<string> {
    return browser.executeScript('return window.localStorage.getItem(\'' + s + '\');');
  }

}
