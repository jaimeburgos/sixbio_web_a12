import { browser, by, element, logging } from 'protractor';
import { AppPage } from './app.po';

describe('Pruebas E2E Login', () => {
  let page: AppPage;

  beforeEach( async() => {
    page = new AppPage();
   });

  it('Debe mostrar titulo Sistema de Verificación de Identidad en Login', async () => {
    await page.navigateTo('/');
    expect(await page.getElementText('app-login h1')).toEqual('Sistema de Verificación de Identidad');          console.log('Test');

  });

  it('Verificamos el tipeo en User y Login', async () => {
    await  page.navigateTo('/');
    await  element(by.css('app-login input[formControlName=usuario]')).sendKeys('TestUser');

    //  expect(await page.getElementText('app-login input[formControlName=usuario]')).toEqual('TestUser');
  //  await    element(by.css('app-login input[formControlName=pass]')).sendKeys('');



  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
