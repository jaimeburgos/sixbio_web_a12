import { browser, by, element } from 'protractor';

export class AppPage {
  async navigateTo(s: string): Promise<unknown> {
    return browser.get(browser.baseUrl + s);
  }

  async getElementText(s: string): Promise<string> {
    return element(by.css(s)).getText();
  }

  getFormControl (s: string) {
    return element(by.css('[formControlName="' + s + '"]'));
  }

  getValueFormControl ( s: string) {
    return this.getFormControl(s).getAttribute('value');
  }
}
