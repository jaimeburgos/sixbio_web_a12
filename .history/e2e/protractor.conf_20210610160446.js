// @ts-nocheck
// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter, StacktraceOption } = require('jasmine-spec-reporter');

/**
 * @type { import("protractor").Config }
 */
 const ruta  = 'D:/SIX BIO/Web-VerificacionIdentidad/Fuentes/FrontEnd/SB-Admin-BS4-Angular-8-master/node_modules/webdriver-manager/selenium/'

exports.config = {
    // webdriver-manager start --edge D:\SIX_BIO\Web-VerificacionIdentidad\Fuentes\FrontEnd\SB-Admin-BS4-Angular-8-master\node_modules\webdriver-manager\selenium\msedgedriver.exe
    //   seleniumAddress: 'http://localhost:4444/wd/hub',
    // webdriver-manager start --edge D:\SIX BIO\Web-VerificacionIdentidad\Fuentes\FrontEnd\SB-Admin-BS4-Angular-8-master\node_modules\webdriver-manager\selenium\msedgedriver.exe
    allScriptsTimeout: 11000,
    specs: ["./src/**/*.e2e-spec.ts"],
    /* multiCapabilities: [
       {
            browserName: "firefox",
        },    
        {
            browserName: "chrome",
            chromeoptions: {
                chromeOptions: {
                    args: [
                        "--headless",
                        // "--disable-gpu",
                    ],
                },
            },
        },
       {  browserName: "MicrosoftEdge"    ,   'platform': 'windows', },
    ],*/
    multiCapabilities: [
        {
            browserName: "firefox",
            directConnect: true,
            "moz:firefoxOptions": {
                prefs: {
                    "intl.accept_languages": "es",
                },
            },
        },
        {
            browserName: "chrome",
            directConnect: true,
            chromeoptions: {
                chromeOptions: {
                    args: [
                        //   "--headless",
                        //    "--disable-gpu",
                    ],
                },
            },
        },
        { browserName: "MicrosoftEdge", directConnect: false },
    ],
    jvmArgs: [
        "-Dwebdriver.chrome.driver=" + ruta + "chromedriver_90.0.4430.24.exe",
        "-Dwebdriver.gecko.driver=" + ruta + "geckodriver-v0.29.1.exe",
        "-Dwebdriver.edge.driver=" + ruta + "msedgedriver.exe",
    ],
    /* 
TASKKILL /IM chromedriver_90.0.4430.24.exe /F 
TASKKILL /IM geckodriver-v0.29.1.exe /F
TASKKILL /IM msedgedriver.exe /F

*/

    SELENIUM_PROMISE_MANAGER: false,
    baseUrl: "http://localhost:4200/",
    framework: "jasmine",
    jasmineNodeOpts: {
        showColors: true,
        defaultTimeoutInterval: 30000,
        print: function () {},
    },
    onPrepare() {
        browser.params.USER_SATISFACTORIO = "jburgos6";
        browser.params.PASS_SATISFACTORIO = "Admin2";
        // browser.params.USER_RESETEO = 'jburgos9';
        // browser.params.PASS_RESETEO = 'j4ycq22w';
        // browser.params.USER_RESETEO = 'jburgos8';
        // browser.params.PASS_RESETEO = '63m4yrg5';
        browser.params.USER_RESETEO = "UserResetTest";
        browser.params.PASS_RESETEO = 'ol48ooja';
        browser.params.USER_EXCEDIDO = "jburgos7";
        browser.params.PASS_EXCEDIDO = "qiuf306t";

        require("ts-node").register({
            project: require("path").join(__dirname, "./tsconfig.json"),
        });

        jasmine.getEnv().addReporter(
            new SpecReporter({
                spec: {
                    displayStacktrace: StacktraceOption.PRETTY,
                },
            })
        );
    },
};