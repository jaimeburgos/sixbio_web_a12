/// <reference types="cypress" />

context("Login Pruebas Funcionalidad", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const userBloq = Cypress.env("userBloq");
    const passBloq = Cypress.env("passBloq");
    const userVer = Cypress.env("userVer");
    const passVer = Cypress.env("passVer");
    const userReset = Cypress.env("userReset");
    const passReset = Cypress.env("passReset");
    const url = Cypress.env("url");

    beforeEach(() => {
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();
    });

    it("Primer Login al sistema - Cancelar", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });
        cy.wait("@signon");

        //  cy.wait(1000); cypress run --browser chrome

        cy.get("#msgModalLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "Primer Login, se le sugiere realizar el cambio de contraseña."
                );
            });

        cy.get("div")
            .find("button")
            .contains("Cancelar")
            .click({ force: true });

        cy.url().should("include", "/login");
    });

    it("Primer Login al sistema", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });
        cy.wait("@signon");

        //  cy.wait(1000); cypress run --browser chrome

        cy.get("#msgModalLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "Primer Login, se le sugiere realizar el cambio de contraseña."
                );
            });

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");

        cy.url().should("include", "/firstLogin");

        cy.url().should(() => {
            expect(localStorage.getItem("isFirstLogin")).to.eq("true");
        });
    });

    it("Ingreso al sistema con usuario y/o contraseña incorrecta", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        cy.get('button[name="submit"]').click({ force: true });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Usuario")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "Usuario y/o Contraseña incorrecta, intente nuevamente."
                );
            });
    });
/*
    it("Su cuenta ha sido bloqueada por exceder el máximo de intentos fallidos", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userBloq)
            .should("have.value", userBloq);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type("sdfsddfsdsf")
            .should("have.value", "sdfsddfsdsf");

        cy.get('button[name="submit"]').click({ force: true });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Usuario")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "Usuario y/o Contraseña incorrecta, intente nuevamente."
                );
            });

        cy.get('button[name="submit"]').click({ force: true });
        cy.get('button[name="submit"]').click({ force: true });
        cy.get('button[name="submit"]').click({ force: true });
        cy.get('button[name="submit"]').click({ force: true });
        //Su cuenta se encuentra bloqueada por seguridad
        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("cuenta")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "Su cuenta se encuentra bloqueada por seguridad."
                );
            });
    });*/

    it("Verificamos mensaje de error de conectividad Login ( Response Vacio )", () => {
        cy.intercept("POST", url + "SIXBIO-webcore-Nova/service/signon", []).as(
            "errorConectividad"
        );
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@errorConectividad");
        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("conectividad")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Error de conectividad.");
            });
    });
});
