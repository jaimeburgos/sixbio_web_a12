/// <reference types="cypress" />

context("Login Mensajes Error", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const userReset = Cypress.env("userReset");
    const passReset = Cypress.env("passReset");
    const url = Cypress.env("url");

    beforeEach(() => {
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();
    });


    it("Verificamos mensaje Toast Error Usuario y/o contraseña incorrecta", () => {

        // Ingresamos de manera incorrecta nuestras credenciales de usuario
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");
        //Realizamos click en el boton entrar
        cy.get('button[name="submit"]').click({ force: true });
        //Verificamos el toast error
        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });
            //Verificamos el contenido de nuestro toast error
        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Usuario")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "Usuario y/o Contraseña incorrecta, intente nuevamente."
                );
            });
    });

    it("Verificamos mensaje de error de conectividad Login ( Response Vacio )", () => {
        // Interceptamos el servicio signon y mockeamos un response vacio para que nos de error
        cy.intercept("POST", url + "SIXBIO-webcore-Nova/service/signon", []).as(
            "errorConectividad"
        );
        //Ingresamos nuestras credenciales
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);
            //Realizamos click en el boton entrar
        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@errorConectividad");
        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("conectividad")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Error de conectividad.");
            });

        //SIXBIO-webcore-Nova/service/signon
    });

});
 