/// <reference types="cypress" />

context("Login Validacion Campos", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const userVer = Cypress.env("userVer");
    const passVer = Cypress.env("passVer");
    const userReset = Cypress.env("userReset");
    const passReset = Cypress.env("passReset");
    const url = Cypress.env("url");

    beforeEach(() => {
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos campo Usuario", () => {
        // ingresamos caracteres alfanumerico en nuestro usuario y verificamos
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const U_Permitidos = ["aaaaaaaa", "cccccbbbbb", "c123bbb4b"];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="usuario"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="usuario"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
            "          ",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="usuario"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="usuario"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos campo Contraseña", () => {
        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const P_Permitidos = [
            ":::::::::",
            "............",
            "--------------",
            "______________",
            "((((((((((",
            "))))))))))",
            "@@@@@@@@@",
            "/////////",
        ];

        for (let i = 0; i <= P_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="pass"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="pass"]')
                .click({ force: true })
                .type(P_Permitidos[i])
                .should("have.value", P_Permitidos[i]);
        }

        const P_No_Permitidos = [
            "☺☻♥♠○◘♣•",
            "[[[[]]]]",
            "{{{{",
            "          ",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            ",,,,,,,,,,,",
            ";;;;;;;;;;;;",
            "ññññññññññ",
            "ÑÑÑÑÑÑÑ",
            "============",
            "<<<<<>>>>>",
            "**********",
            "++++++++++",
            "$$$$$$$$",
            '""""""""""""',
            "^^^^^^^^^^^^",
        ];

        for (let i = 0; i <= P_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="pass"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="pass"]')
                .click({ force: true })
                .type(P_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos longitud maxima del campo usuario", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type("123456789012345678901")
            .should("have.value", "12345678901234567890");
    });

    it("Verificamos longitud maxima del campo contraseña", () => {
        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type("12345678901234567")
            .should("have.value", "1234567890123456");
    });

    it("Verificamos mensajes de validacion campo Usuario", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get("#msgMinLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 5 caracteres");
            });

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();

        cy.get("#msgReqLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese usuario");
            });
    });

    it("Verificamos mensajes de validacion campo Contraseña", () => {
        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();

        cy.get("#msgMinPass")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 6 caracteres");
            });

        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get("#msgReqPass")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese contraseña");
            });
    });


});

