/// <reference types="cypress" />

context("Login Validacion Campos", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const userVer = Cypress.env("userVer");
    const passVer = Cypress.env("passVer");
    const userReset = Cypress.env("userReset");
    const passReset = Cypress.env("passReset");
    const url = Cypress.env("url");

    beforeEach(() => {
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos campo Usuario", () => {
        // ingresamos caracteres alfanumerico en nuestro usuario y verificamos
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const U_Permitidos = ["aaaaaaaa", "cccccbbbbb", "c123bbb4b"];
        // Interamos el array de caracteres permitodos y verificamos que se escriban correctamente

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="usuario"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="usuario"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
            "          ",
        ];
        //Interamos el arreglo de caracteres no permitodos y verificamos que sea vacio

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="usuario"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="usuario"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos campo Contraseña", () => {
                // ingresamos caracteres alfanumerico en nuestra contraseña y verificamos

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const P_Permitidos = [
            ":::::::::",
            "............",
            "--------------",
            "______________",
            "((((((((((",
            "))))))))))",
            "@@@@@@@@@",
            "/////////",
        ];
        // Interamos el array de caracteres permitodos y verificamos que se escriban correctamente

        for (let i = 0; i <= P_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="pass"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="pass"]')
                .click({ force: true })
                .type(P_Permitidos[i])
                .should("have.value", P_Permitidos[i]);
        }

        const P_No_Permitidos = [
            "☺☻♥♠○◘♣•",
            "[[[[]]]]",
            "{{{{",
            "          ",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            ",,,,,,,,,,,",
            ";;;;;;;;;;;;",
            "ññññññññññ",
            "ÑÑÑÑÑÑÑ",
            "============",
            "<<<<<>>>>>",
            "**********",
            "++++++++++",
            "$$$$$$$$",
            '""""""""""""',
            "^^^^^^^^^^^^",
        ];
        //Interamos el arreglo de caracteres no permitodos y verificamos que sea vacio

        for (let i = 0; i <= P_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="pass"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="pass"]')
                .click({ force: true })
                .type(P_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos longitud maxima del campo usuario", () => {
        //Verificamos la logintud maxima del campo usuario escribiendo la cantidad caracteres maxima +1 , verificamos sean todos menos el excedido
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type("123456789012345678901")
            .should("have.value", "12345678901234567890");
    });

    it("Verificamos longitud maxima del campo contraseña", () => {
           //Verificamos la logintud maxima del campo contraseña escribiendo la cantidad caracteres maxima +1 , verificamos sean todos menos el excedido

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type("12345678901234567")
            .should("have.value", "1234567890123456");
    });

    it("Verificamos mensajes de validacion campo Usuario", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");
        //Validamos el mensaje de tamaño minimo de caracteres para campo usuario escribiendo la cantidad inferior a 5
        cy.get("#msgMinLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 5 caracteres");
            });

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        //Validamos el mensaje de ingrese usuario sin escribir texto en el campo usuario
        cy.get("#msgReqLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese usuario");
            });
    });

    it("Verificamos mensajes de validacion campo Contraseña", () => {
        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();

        cy.get("#msgMinPass")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 6 caracteres");
            });

        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get("#msgReqPass")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese contraseña");
            });
    });
});
