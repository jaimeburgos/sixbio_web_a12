/// <reference types="cypress" />

context("Login Interfaz", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const userVer = Cypress.env("userVer");
    const passVer = Cypress.env("passVer");
    const userReset = Cypress.env("userReset");
    const passReset = Cypress.env("passReset");
    const url = Cypress.env("url");

    beforeEach(() => {
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();
    });

    it("Logeo Exitoso del Usuario Verificador", () => {
        //Ingresamos nuestras credenciales de usuario administrador
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);
        //Realizamos click en el boton entrar
        cy.get('button[name="submit"]').click({ force: true });
        //Interceptamos el servicio listarrolesagrupador
        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");
        cy.wait("@listarrolesagrupador");
        //Verificamos la entrar a la pantalla gestion de usuarios
        cy.url().should("include", "/home/gestion-usuarios");
        //Verificamos en nuestro localStorage este seteado el item isLoggedin con valor true
        cy.url().should(() => {
            expect(localStorage.getItem("isLoggedin")).to.eq("true");
        });
    });

    it("Logeo Exitoso del Usuario Consultor", () => {
        //Ingresamos nuestras credenciales de usuario Consultor
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userVer)
            .should("have.value", userVer);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passVer)
            .should("have.value", passVer);
        //Realizamos click en el boton entrar

        cy.get('button[name="submit"]').click({ force: true });
        //Interceptamos el servicio listarrolesagrupador
        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");
        cy.wait("@listarrolesagrupador");
        //Verificamos la entrar a la pantalla verificacion Biometrica

        cy.url().should("include", "/home/verificacionBiometrica");
        //Verificamos en nuestro localStorage este seteado el item isLoggedin con valor true

        cy.url().should(() => {
            expect(localStorage.getItem("isLoggedin")).to.eq("true");
        });
    });

    it("Primer logeo", () => {
        //Ingresamos nuestras credenciales de primer logeo
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);
        //Interceptamos nuestro service signon
        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");
        //Realizamos click en el boton entrar
        cy.get('button[name="submit"]').click({ force: true });
        //Esperamos la llamada y carga del servicio sigon
        cy.wait("@signon");

        //Verificamos el mensaje de primer login
        cy.get("#msgModalLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "Primer Login, se le sugiere realizar el cambio de contraseña."
                );
            });
            //Realizamos click en el boton confirmar
        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });
            //Intercetapmos el servicio consultardatoscambioclave
        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");
        //Verificamos que nos encontremos en la pantalla cambio de contraseña
        cy.url().should("include", "/firstLogin");
        //Verificamos en el localstorage que el item isFirstLogin este seteado con valor true
        cy.url().should(() => {
            expect(localStorage.getItem("isFirstLogin")).to.eq("true");
        });
    });

    it("Ingreso al sistema con usuario y/o contraseña incorrecta", () => {
        //Ingresamos nuestras credenciales de manera erronea
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");
        //Realizamos click en entrar
        cy.get('button[name="submit"]').click({ force: true });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Usuario")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "Usuario y/o Contraseña incorrecta, intente nuevamente."
                );
            });
    });

    it("Verificamos mensaje de error de conectividad Login ( Response Vacio )", () => {
        cy.intercept("POST", url + "SIXBIO-webcore-Nova/service/signon", []).as(
            "errorConectividad"
        );
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@errorConectividad");
        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("conectividad")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Error de conectividad.");
            });

        //SIXBIO-webcore-Nova/service/signon
    });
});
