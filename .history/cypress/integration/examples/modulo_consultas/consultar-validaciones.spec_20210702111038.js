/// <reference types="cypress" />

context("Pruebas consultar-validaciones", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const url = Cypress.env("url");
    //    const userReset = Cypress.env("userReset");
    //    const passReset = Cypress.env("passReset");

    beforeEach(() => {
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
        }).as("listarusuarios");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);

        cy.get("a").contains("Módulo de Consultas").click({ force: true });
        cy.wait(500);
        cy.get("a").contains("Consultar Validaciones").click({ force: true });
        cy.wait("@listarusuarios");
    });

    it("Verificamos entrada a consultar-validaciones", () => {
        cy.url().should("include", "/home/consultar-validaciones");
        // Verificamos los valores que deben setear en el localStorage
        cy.url().should(() => {
            expect(localStorage.getItem("isLoggedin")).to.eq("true");
            expect(localStorage.getItem("RolUsuario")).to.eq("EMP_ADM");
            expect(localStorage.getItem("usuarioLogueado")).to.eq(userAdmin);
        });
        cy.get("div .card-header")
            .invoke("text")
            .then((text) => {
                expect(text).to.contain("Consulta Validaciones");
            });
    });

    it("Verificamos nombres de las columnas", () => {
        cy.get("table")
            .find("thead")
            .find("tr")
            .find("th")
            .invoke("text")
            .then((text) => {
                expect(text).to.contain("TIPO VERIFICACIÓN");
                expect(text).to.contain("ORIGEN RESPUESTA");
                expect(text).to.contain("CÓDIGO RESPUESTA");
                expect(text).to.contain("MENSAJE RESPUESTA");
                expect(text).to.contain("FECHA CONSULTA");
                expect(text).to.contain("HORA CONSULTA");
                expect(text).to.contain("USUARIO CONSULTA");
                expect(text).to.contain("TIPO DOCUMENTO CONSULTADO");
                expect(text).to.contain("NÚMERO DOCUMENTO CONSULTADO");
                expect(text).to.contain("DETALLE");
            });
    });

    it.only("Verificamos label del formulario consultar validaciones", () => {
        cy.get("label[for=searchDate]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Búsqueda por fechas");
            });

        cy.get("label[for=searchQuantity]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Búsqueda por cantidad");
            });
    });
});
