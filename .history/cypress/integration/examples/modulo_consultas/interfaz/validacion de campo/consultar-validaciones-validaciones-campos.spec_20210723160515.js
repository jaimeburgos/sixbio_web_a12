/// <reference types="cypress" />

context("Pruebas Interfaz Modulo Consultas Validacion Campos", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const userSinData = Cypress.env("userSinData");
    const userData = Cypress.env("userData");
    const url = Cypress.env("url");

    beforeEach(() => {
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixbio/consultavalidaciones",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/service/reportevalidaciones",
            []
        );

        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarrolesagrupador");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarusuarios");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixbio/consultavalidaciones",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("consultavalidaciones");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/service/reportevalidaciones",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("reportevalidaciones");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);

        cy.get("a").contains("Módulo de Consultas").click({ force: true });
        cy.wait(500);
        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarusuarios2");

        cy.get("a").contains("Consultar Validaciones").click({ force: true });
        cy.wait("@listarusuarios2");
    });

    it("Consultar Validacions longitud maxima y caracteres numericos campo Cantidad", () => {
        //Validamos la logintud maxima del campo cantidad sea 4 caracteres
        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("12345")
            .should("have.value", "1234");

        const U_Permitidos = ["1234"];
        // Iteramos los campos permitidos para campo cantidad
        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="cantidad"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="cantidad"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "asdsdfgsdf",
            "sdfdñdhgfgdf",
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="cantidad"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="cantidad"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });
});
