/// <reference types="cypress" />

context("Pruebas consultar-validaciones", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const userSinData = Cypress.env("userSinData");
    const userData = Cypress.env("userData");
    const url = Cypress.env("url");
    //    const userReset = Cypress.env("userReset");
    //    const passReset = Cypress.env("passReset");

    beforeEach(() => {
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
        }).as("listarusuarios");

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixbio/consultavalidaciones",
        }).as("consultavalidaciones");

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/reportevalidaciones",
        }).as("reportevalidaciones");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);

        cy.get("a").contains("Módulo de Consultas").click({ force: true });
        cy.wait(500);
        cy.get("a").contains("Consultar Validaciones").click({ force: true });
        cy.wait("@listarusuarios");
    });

    it("Verificamos entrada a consultar-validaciones", () => {
        cy.url().should("include", "/home/consultar-validaciones");
        // Verificamos los valores que deben setear en el localStorage
        cy.url().should(() => {
            expect(localStorage.getItem("isLoggedin")).to.eq("true");
            expect(localStorage.getItem("RolUsuario")).to.eq("EMP_ADM");
            expect(localStorage.getItem("usuarioLogueado")).to.eq(userAdmin);
        });
        cy.get("div .card-header")
            .invoke("text")
            .then((text) => {
                expect(text).to.contain("Consulta Validaciones");
            });

        cy.get("button[id=excel]").should("be.disabled");
        cy.get("button[id=pdf]").should("be.disabled");
        cy.get('button[type="submit"]').should("be.disabled");
        cy.get('input[id="defaultChecked"]').should("be.enabled");
        cy.get('input[id="defaultUnchecked"]').should("be.enabled");
    });

    it("Verificamos nombres de las columnas", () => {
        cy.get("table")
            .find("thead")
            .find("tr")
            .find("th")
            .invoke("text")
            .then((text) => {
                expect(text).to.contain("TIPO VERIFICACIÓN");
                expect(text).to.contain("ORIGEN RESPUESTA");
                expect(text).to.contain("CÓDIGO RESPUESTA");
                expect(text).to.contain("MENSAJE RESPUESTA");
                expect(text).to.contain("FECHA CONSULTA");
                expect(text).to.contain("HORA CONSULTA");
                expect(text).to.contain("USUARIO CONSULTA");
                expect(text).to.contain("TIPO DOCUMENTO CONSULTADO");
                expect(text).to.contain("NÚMERO DOCUMENTO CONSULTADO");
                expect(text).to.contain("DETALLE");
            });
    });

    it("Verificamos label del formulario consultar validaciones", () => {
        cy.get("label[for=defaultUnchecked]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Búsqueda por fechas");
            });

        cy.get("label[for=defaultChecked]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Búsqueda por cantidad");
            });
        cy.get("label[for=inputFechaDesde]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Desde");
            });

        cy.get("label[for=inputCantidad]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Cantidad");
            });

        cy.get("label[for=inputFechaHasta]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Hasta");
            });

        cy.get("label[for=inputEstado]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Usuario");
            });
    });

    it("Verificamos caracteres permitidos y no permitidos en campo cantidad", () => {
        const U_Permitidos = ["1234"];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="cantidad"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="cantidad"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "asdsdfgsdf",
            "sdfdñdhgfgdf",
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="cantidad"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="cantidad"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos una busqueda de validacion consulta correctamente  usando filtro cantidad ", () => {
        cy.get('select[formControlName="filterUsuario"]').select("evaldivia", {
            force: true,
        });
        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("4444");

        cy.get('button[type="submit"]').click({ force: true });
    });

    it("Verificamos una busqueda de validacion consulta correctamente  usando filtro por fechas ", () => {
        cy.get("#defaultUnchecked").click({ force: true });
        cy.get('select[formControlName="filterUsuario"]').select("evaldivia", {
            force: true,
        });
        var fecha = new Date();
        var year = fecha.getFullYear();
        cy.get('input[formControlName="filterfechaDesde"]').click({
            force: true,
        });
        cy.get('button[class="current"]').contains(year).click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains(year - 7)
            .click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains("enero")
            .click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });

        cy.get('input[formControlName="filterfechaHasta"]').click({
            force: true,
        });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });

        cy.get('button[type="submit"]').click({ force: true });
        cy.wait("@consultavalidaciones");
        cy.get("button[id=excel]").click({ force: true });

        cy.get("button[id=pdf]").click({ force: true });
    });

    it("Verificamos botones excel y pdf despues busqueda por fecha ", () => {
        cy.get("#defaultUnchecked").click({ force: true });
        cy.get('select[formControlName="filterUsuario"]').select("evaldivia", {
            force: true,
        });
        var fecha = new Date();
        var year = fecha.getFullYear();
        cy.get('input[formControlName="filterfechaDesde"]').click({
            force: true,
        });
        cy.get('button[class="current"]').contains(year).click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains(year - 7)
            .click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains("enero")
            .click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });

        cy.get('input[formControlName="filterfechaHasta"]').click({
            force: true,
        });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });

        cy.get('button[type="submit"]').click({ force: true });
        cy.wait("@consultavalidaciones");
        cy.get("button[id=excel]").click({ force: true });
        //
        cy.wait("@consultavalidaciones");
        cy.wait("@reportevalidaciones");
        cy.get("button[id=pdf]").click({ force: true });
    });

    it("Verificamos botones excel y pdf despues busqueda por cantidad", () => {
        cy.get('select[formControlName="filterUsuario"]').select("evaldivia", {
            force: true,
        });
        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("9999");

        cy.get('button[type="submit"]').click({ force: true });

        cy.wait("@consultavalidaciones");
        cy.get("button[id=excel]").click({ force: true });
        //
        cy.wait("@consultavalidaciones");
        cy.wait("@reportevalidaciones");
        cy.get("button[id=pdf]").click({ force: true });
    });

    it("Verificamos el mensaje Toast cuando una consulta de usuario no posee datos con filtro cantidad", () => {
        cy.get('select[formControlName="filterUsuario"]').select(userSinData, {
            force: true,
        });
        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("9999");

        cy.get('button[type="submit"]').click({ force: true });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("CONSULTA")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("NO HAY DATOS PARA LA CONSULTA");
            });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });
    });

    it("Verificamos el mensaje Toast cuando una consulta de usuario no posee datos con filtro fechas", () => {
        cy.get("#defaultUnchecked").click({ force: true });
        cy.get('select[formControlName="filterUsuario"]').select(userSinData, {
            force: true,
        });
        var fecha = new Date();
        var year = fecha.getFullYear();
        cy.get('input[formControlName="filterfechaDesde"]').click({
            force: true,
        });
        cy.get('button[class="current"]').contains(year).click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains(year - 7)
            .click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains("enero")
            .click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });

        cy.get('input[formControlName="filterfechaHasta"]').click({
            force: true,
        });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });

        cy.get('button[type="submit"]').click({ force: true });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("CONSULTA")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("NO HAY DATOS PARA LA CONSULTA");
            });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });
    });

    it("Verificamos funcionamiento boton ver detalle-validacion con el filtro cantidad", () => {
        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });

        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("444");
        cy.get('button[type="submit"]').click({ force: true });

        cy.wait("@consultavalidaciones");
        cy.get('button[title="Ver detalle"]').first().click({ force: true });

        cy.url().should(
            "include",
            "/home/consultar-validaciones/detalle-validacion"
        );
    });

    it("Verificamos funcionamiento boton ver detalle-validacion con el filtro fecha", () => {
        cy.get("#defaultUnchecked").click({ force: true });

        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });
        var fecha = new Date();
        var year = fecha.getFullYear();
        cy.get('input[formControlName="filterfechaDesde"]').click({
            force: true,
        });
        cy.get('button[class="current"]').contains(year).click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains(year - 7)
            .click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains("enero")
            .click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });

        cy.get('input[formControlName="filterfechaHasta"]').click({
            force: true,
        });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });

        cy.get('button[type="submit"]').click({ force: true });

        cy.wait("@consultavalidaciones");
        cy.get('button[title="Ver detalle"]').first().click({ force: true });

        cy.url().should(
            "include",
            "/home/consultar-validaciones/detalle-validacion"
        );
    });

    it("Verificamos labels de la pagina detalla-validacion buscando con el filtro cantidad ", () => {
        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });

        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("444");
        cy.get('button[type="submit"]').click({ force: true });

        cy.wait("@consultavalidaciones");
        cy.get('button[title="Ver detalle"]').first().click({ force: true });

        cy.get("div[id=detalleValidacion]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Detalle Validación");
            });

        cy.get("label[for='inputUsuario']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Usuario Consulta");
            });

        cy.get("label[for='inputValidacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Tipo Verificación");
            });

        cy.get("label[for='inputMensajeRpta']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Mensaje Respuesta");
            });

        cy.get("label[for='inputFechaValidacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Consulta");
            });

        cy.get("label[for='inputSistLocalizacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Sistema Localización");
            });

        cy.get("label[for='inputLalitud']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Latitud Localización");
            });

        cy.get("label[for='inputIDValidacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Id Validacion");
            });

        cy.get("label[for='inputOrigenR']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Origen Respuesta");
            });

        cy.get("label[for='inputCodigoRespuesta']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Codigo Respuesta");
            });

        cy.get("label[for='inputHoraConsulta']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Hora Consulta");
            });

        cy.get("label[for='inputLongitud']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Longitud Localización");
            });

        cy.get("label[for='inputApellidoPaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Paterno");
            });

        cy.get("label[for='inputNombres']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nombres");
            });

        cy.get("label[for='inputSexo']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Sexo");
            });

        cy.get("label[for='inputFechaNac']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Nacimiento");
            });

        cy.get("label[for='inputFechaEmision']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Emision");
            });

        cy.get("label[for='inputApellidoMaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Materno");
            });

        cy.get("label[for='inputTipoDoc']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Tipo Documento");
            });

        cy.get("label[for='inputNroDocumento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Número de Documento");
            });

        cy.get("div[id=datosPersona]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Datos de la persona consultada");
            });
    });

    it("Verificamos labels de la pagina detalla-validacion buscando con el filtro fecha ", () => {
        cy.get("#defaultUnchecked").click({ force: true });

        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });

        var fecha = new Date();
        var year = fecha.getFullYear();
        cy.get('input[formControlName="filterfechaDesde"]').click({
            force: true,
        });
        cy.get('button[class="current"]').contains(year).click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains(year - 7)
            .click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains("enero")
            .click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });

        cy.get('input[formControlName="filterfechaHasta"]').click({
            force: true,
        });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });

        cy.get('button[type="submit"]').click({ force: true });

        cy.wait("@consultavalidaciones");
        cy.get('button[title="Ver detalle"]').first().click({ force: true });

        cy.get("div[id=detalleValidacion]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Detalle Validación");
            });

        cy.get("label[for='inputUsuario']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Usuario Consulta");
            });

        cy.get("label[for='inputValidacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Tipo Verificación");
            });

        cy.get("label[for='inputMensajeRpta']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Mensaje Respuesta");
            });

        cy.get("label[for='inputFechaValidacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Consulta");
            });

        cy.get("label[for='inputSistLocalizacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Sistema Localización");
            });

        cy.get("label[for='inputLalitud']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Latitud Localización");
            });

        cy.get("label[for='inputIDValidacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Id Validacion");
            });

        cy.get("label[for='inputOrigenR']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Origen Respuesta");
            });

        cy.get("label[for='inputCodigoRespuesta']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Codigo Respuesta");
            });

        cy.get("label[for='inputHoraConsulta']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Hora Consulta");
            });

        cy.get("label[for='inputLongitud']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Longitud Localización");
            });

        cy.get("label[for='inputApellidoPaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Paterno");
            });

        cy.get("label[for='inputNombres']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nombres");
            });

        cy.get("label[for='inputSexo']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Sexo");
            });

        cy.get("label[for='inputFechaNac']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Nacimiento");
            });

        cy.get("label[for='inputFechaEmision']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Emision");
            });

        cy.get("label[for='inputApellidoMaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Materno");
            });

        cy.get("label[for='inputTipoDoc']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Tipo Documento");
            });

        cy.get("label[for='inputNroDocumento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Número de Documento");
            });

        cy.get("div[id=datosPersona]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Datos de la persona consultada");
            });
    });

//select id=inputSizePagination

    it.only('Verificamos cambios en la grilla despues una busqueda con fecha',() => {
        cy.get("#defaultUnchecked").click({ force: true });

        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });

        var fecha = new Date();
        var year = fecha.getFullYear();
        cy.get('input[formControlName="filterfechaDesde"]').click({
            force: true,
        });
        cy.get('button[class="current"]').contains(year).click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains(year - 7)
            .click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains("enero")
            .click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });

        cy.get('input[formControlName="filterfechaHasta"]').click({
            force: true,
        });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });

        cy.get('button[type="submit"]').click({ force: true });

        cy.wait("@consultavalidaciones"); 
    
    
        cy.get('select[id=inputSizePagination]').select("5", {
            force: true,
        });

        cy.wait("@consultavalidaciones"); 

        cy.get('select[id=inputSizePagination]').select("10", {
            force: true,
        });

        cy.wait("@consultavalidaciones"); 
        
        cy.get('select[id=inputSizePagination]').select("15", {
            force: true,
        });
        cy.wait("@consultavalidaciones"); 

        cy.get('select[id=inputSizePagination]').select("20", {
            force: true,
        });
    })




});
