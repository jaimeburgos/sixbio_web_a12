/// <reference types="cypress" />

context("Pruebas consultar-validaciones", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const url = Cypress.env("url");
    //    const userReset = Cypress.env("userReset");
    //    const passReset = Cypress.env("passReset");

    beforeEach(() => {
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
        }).as("listarusuarios");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);

        cy.get("a").contains("Módulo de Consultas").click({ force: true });
        cy.wait(500);
        cy.get("a").contains("Consultar Validaciones").click({ force: true });
        cy.wait("@listarusuarios");
    });

    it("Verificamos entrada a consultar-validaciones", () => {
        cy.url().should("include", "/home/consultar-validaciones");
        // Verificamos los valores que deben setear en el localStorage
        cy.url().should(() => {
            expect(localStorage.getItem("isLoggedin")).to.eq("true");
            expect(localStorage.getItem("RolUsuario")).to.eq("EMP_ADM");
            expect(localStorage.getItem("usuarioLogueado")).to.eq(userAdmin);
        });
        cy.get("div .card-header")
            .invoke("text")
            .then((text) => {
                expect(text).to.contain("Consulta Validaciones");
        });

        cy.get("button[id=excel]").should("be.disabled");
        cy.get("button[id=pdf]").should("be.disabled");
        cy.get('button[type="submit"]').should("be.disabled");
        cy.get('input[id="defaultChecked"]').should("be.enabled");
        cy.get('input[id="defaultUnchecked"]').should("be.enabled");
    });

    it("Verificamos nombres de las columnas", () => {
        cy.get("table")
            .find("thead")
            .find("tr")
            .find("th")
            .invoke("text")
            .then((text) => {
                expect(text).to.contain("TIPO VERIFICACIÓN");
                expect(text).to.contain("ORIGEN RESPUESTA");
                expect(text).to.contain("CÓDIGO RESPUESTA");
                expect(text).to.contain("MENSAJE RESPUESTA");
                expect(text).to.contain("FECHA CONSULTA");
                expect(text).to.contain("HORA CONSULTA");
                expect(text).to.contain("USUARIO CONSULTA");
                expect(text).to.contain("TIPO DOCUMENTO CONSULTADO");
                expect(text).to.contain("NÚMERO DOCUMENTO CONSULTADO");
                expect(text).to.contain("DETALLE");
            });
    });

    it("Verificamos label del formulario consultar validaciones", () => {
        cy.get("label[for=defaultUnchecked]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Búsqueda por fechas");
            });

        cy.get("label[for=defaultChecked]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Búsqueda por cantidad");
            });
        cy.get("label[for=inputFechaDesde]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Desde");
            });

        cy.get("label[for=inputCantidad]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Cantidad");
            });

        cy.get("label[for=inputFechaHasta]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Hasta");
            });

        cy.get("label[for=inputEstado]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Usuario");
            });
    });

    it('Verificamos caracteres permitidos y no permitidos en campo cantidad', () => {
        const U_Permitidos = ["1234"];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="cantidad"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="cantidad"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "asdsdfgsdf",
            "sdfdñdhgfgdf",
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="cantidad"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="cantidad"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it('Verificamos una busqueda de validacion consulta correctamente  usando filtro cantidad ',()=>{
        cy.get('select[formControlName="filterUsuario"]').select("evaldivia", {
            force: true,
        });
        cy.get('input[formControlName="cantidad"]')
        .click({ force: true })
        .type('4444');

        cy.get('button[type="submit"]').click({ force: true});
    });

    it.only('Verificamos una busqueda de validacion consulta correctamente  usando filtro por fechas ',()=>{
        cy.get('select[formControlName="filterUsuario"]').select("evaldivia", {
            force: true,
        });

        cy.get('input[formControlName="filterfechaDesde"]').click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });

        cy.get('button[type="submit"]').click({ force: true});
    });



});
