/// <reference types="cypress" />

context("Pruebas consultar-validaciones", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const url = Cypress.env("url");
    //    const userReset = Cypress.env("userReset");
    //    const passReset = Cypress.env("passReset");

    beforeEach(() => {
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
        }).as("listarusuarios");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);
    });

    it.only("Verificamos entrada a consultar-validaciones", () => {
        cy.get("a").contains("Módulo de Consultas").click({ force: true });
        cy.wait(500);
        cy.get("a").contains("Consultar Validaciones").click({ force: true });


        cy.url().should("include", "/home/consultar-validaciones");
        // Verificamos los valores que deben setear en el localStorage
        cy.url().should(() => {
            expect(localStorage.getItem("isLoggedin")).to.eq("true");
            expect(localStorage.getItem("RolUsuario")).to.eq("EMP_ADM");
            expect(localStorage.getItem("usuarioLogueado")).to.eq(userAdmin);
        });
        cy.get("div .card-header")
            .invoke("text")
            .then((text) => {
                expect(text).to.contain("Consulta Validaciones");
            });
    });

});
