/// <reference types="cypress" />

context("Pruebas Funcionalidad Modulo Consultas Ver Detalle Validacion", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const userSinData = Cypress.env("userSinData");
    const userData = Cypress.env("userData");
    const url = Cypress.env("url");

    beforeEach(() => {
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixbio/consultavalidaciones",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/service/reportevalidaciones",
            []
        );

        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarrolesagrupador");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarusuarios");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixbio/consultavalidaciones",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("consultavalidaciones");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/service/reportevalidaciones",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("reportevalidaciones");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);

        cy.get("a").contains("Módulo de Consultas").click({ force: true });
        cy.wait(500);
        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarusuarios2");

        cy.get("a").contains("Consultar Validaciones").click({ force: true });
        cy.wait("@listarusuarios2");
    });

    it("Ver detalle de la validación (Busqueda por Campo Cantidad)", () => {
        // Seleccionamos un usuario con data
        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });
        //Realizamos la busqueda por cantidad
        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("444");
        // Realizamos click en el boton buscar
        cy.get('button[type="submit"]').click({ force: true });

        cy.wait("@consultavalidaciones");

        // Seleccionamos el primer usuario en la grilla , boton Ver Detalle
        cy.get('button[title="Ver detalle"]').first().click({ force: true });
        // Verificamos que estemos en la ruta /home/consultar-validaciones/detalle-validacion
        cy.url().should(
            "include",
            "/home/consultar-validaciones/detalle-validacion"
        );
        //Validamos los nombres de todos los campos
        cy.get("div[id=detalleValidacion]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Detalle Validación");
            });

        cy.get("label[for='inputUsuario']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Usuario Consulta");
            });

        cy.get("label[for='inputValidacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Tipo Verificación");
            });

        cy.get("label[for='inputMensajeRpta']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Mensaje Respuesta");
            });

        cy.get("label[for='inputFechaValidacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Consulta");
            });

        cy.get("label[for='inputSistLocalizacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Sistema Localización");
            });

        cy.get("label[for='inputLalitud']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Latitud Localización");
            });

        cy.get("label[for='inputIDValidacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Id Validacion");
            });

        cy.get("label[for='inputOrigenR']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Origen Respuesta");
            });

        cy.get("label[for='inputCodigoRespuesta']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Codigo Respuesta");
            });

        cy.get("label[for='inputHoraConsulta']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Hora Consulta");
            });

        cy.get("label[for='inputLongitud']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Longitud Localización");
            });

        cy.get("label[for='inputApellidoPaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Paterno");
            });

        cy.get("label[for='inputNombres']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nombres");
            });

        cy.get("label[for='inputSexo']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Sexo");
            });

        cy.get("label[for='inputFechaNac']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Nacimiento");
            });

        cy.get("label[for='inputFechaEmision']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Emision");
            });

        cy.get("label[for='inputApellidoMaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Materno");
            });

        cy.get("label[for='inputTipoDoc']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Tipo Documento");
            });

        cy.get("label[for='inputNroDocumento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Número de Documento");
            });

        cy.get("div[id=datosPersona]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Datos de la persona consultada");
            });
    });

    it("Ver detalle de la validación (Busqueda por Campos Fecha)", () => {
        //Habilitamos el check en Busqueda por Fecha
        cy.get("#defaultUnchecked").click({ force: true });

        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });
        var fecha = new Date();
        var year = fecha.getFullYear();
        cy.get('input[formControlName="filterfechaDesde"]').click({
            force: true,
        });
        cy.get('button[class="current"]').contains(year).click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains(year - 7)
            .click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains("enero")
            .click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });

        cy.get('input[formControlName="filterfechaHasta"]').click({
            force: true,
        });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });

        cy.get('button[type="submit"]').click({ force: true });

        cy.wait("@consultavalidaciones");
        cy.get('button[title="Ver detalle"]').first().click({ force: true });

        cy.url().should(
            "include",
            "/home/consultar-validaciones/detalle-validacion"
        );

        cy.get("div[id=detalleValidacion]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Detalle Validación");
            });

        cy.get("label[for='inputUsuario']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Usuario Consulta");
            });

        cy.get("label[for='inputValidacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Tipo Verificación");
            });

        cy.get("label[for='inputMensajeRpta']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Mensaje Respuesta");
            });

        cy.get("label[for='inputFechaValidacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Consulta");
            });

        cy.get("label[for='inputSistLocalizacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Sistema Localización");
            });

        cy.get("label[for='inputLalitud']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Latitud Localización");
            });

        cy.get("label[for='inputIDValidacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Id Validacion");
            });

        cy.get("label[for='inputOrigenR']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Origen Respuesta");
            });

        cy.get("label[for='inputCodigoRespuesta']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Codigo Respuesta");
            });

        cy.get("label[for='inputHoraConsulta']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Hora Consulta");
            });

        cy.get("label[for='inputLongitud']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Longitud Localización");
            });

        cy.get("label[for='inputApellidoPaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Paterno");
            });

        cy.get("label[for='inputNombres']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nombres");
            });

        cy.get("label[for='inputSexo']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Sexo");
            });

        cy.get("label[for='inputFechaNac']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Nacimiento");
            });

        cy.get("label[for='inputFechaEmision']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Emision");
            });

        cy.get("label[for='inputApellidoMaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Materno");
            });

        cy.get("label[for='inputTipoDoc']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Tipo Documento");
            });

        cy.get("label[for='inputNroDocumento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Número de Documento");
            });

        cy.get("div[id=datosPersona]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Datos de la persona consultada");
            });
    });
});
