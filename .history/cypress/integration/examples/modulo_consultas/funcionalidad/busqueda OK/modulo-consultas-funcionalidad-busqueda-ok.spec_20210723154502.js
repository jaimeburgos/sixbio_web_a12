/// <reference types="cypress" />

context("Pruebas Funcionalidad Modulo Consultas Busqueda OK", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const userSinData = Cypress.env("userSinData");
    const userData = Cypress.env("userData");
    const url = Cypress.env("url");

    beforeEach(() => {
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixbio/consultavalidaciones",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/service/reportevalidaciones",
            []
        );
        // Visitamos nuestras URL Base e ingresamos nuestras credenciales de usuario
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);
        //Realizamos click en el boton entrar
        cy.get('button[name="submit"]').click({ force: true });
        //Interceptamos nuestros servicios para usar el cy.wait para esperar su tiempo de carga para la renderizacion de objetos
        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarrolesagrupador");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarusuarios");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixbio/consultavalidaciones",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("consultavalidaciones");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/service/reportevalidaciones",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("reportevalidaciones");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);
        // Realizamos click sobre el menu Modulo de Consultas
        cy.get("a").contains("Módulo de Consultas").click({ force: true });
        cy.wait(500);
        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarusuarios2");
        // Hacemos click en el menu Coonsultar validaciones
        cy.get("a").contains("Consultar Validaciones").click({ force: true });
        cy.wait("@listarusuarios2");
    });

    it("Busqueda con rol administrador (Campo Cantidad)", () => {
        // Seleccionamos el usuario que posee data para la consulta
        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });
        //Ingresamos un valor en cantidad
        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("4444");
        //Realizamos la busqueda de manera correcta
        cy.get('button[type="submit"]').click({ force: true });
        cy.wait("@consultavalidaciones");

        cy.url().should("include", "/home/consultar-validaciones");
    });

    it("Busqueda con rol administrador (Campos Fechas)", () => {
        //Realizamos click el check Búsqueda por fechas para activarlo
        cy.get("#defaultUnchecked").click({ force: true });
        // Seleccionamos nuestro usuario con data
        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });
        var fecha = new Date();
        var year = fecha.getFullYear();
        cy.get('input[formControlName="filterfechaDesde"]').click({
            force: true,
        });
        cy.get('button[class="current"]').contains(year).click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains(year - 7)
            .click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains("enero")
            .click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });

        cy.get('input[formControlName="filterfechaHasta"]').click({
            force: true,
        });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });

        cy.get('button[type="submit"]').click({ force: true });
        cy.wait("@consultavalidaciones");
        cy.url().should("include", "/home/consultar-validaciones");

//       cy.get("button[id=excel]").click({ force: true });
//        cy.get("button[id=pdf]").click({ force: true });
    });

    it("Busqueda OK - cambio de tamaño página (Campo Cantidad)", () => {
        cy.get("#defaultUnchecked").click({ force: true });

        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });

        var fecha = new Date();
        var year = fecha.getFullYear();
        cy.get('input[formControlName="filterfechaDesde"]').click({
            force: true,
        });
        cy.get('button[class="current"]').contains(year).click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains(year - 7)
            .click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains("enero")
            .click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });

        cy.get('input[formControlName="filterfechaHasta"]').click({
            force: true,
        });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });

        cy.get('button[type="submit"]').click({ force: true });

        cy.wait("@consultavalidaciones");

        cy.get("select[id=inputSizePagination]").select("5", {
            force: true,
        });

        cy.wait("@consultavalidaciones");

        cy.get("select[id=inputSizePagination]").select("10", {
            force: true,
        });

        cy.wait("@consultavalidaciones");

        cy.get("select[id=inputSizePagination]").select("15", {
            force: true,
        });
        cy.wait("@consultavalidaciones");

        cy.get("select[id=inputSizePagination]").select("20", {
            force: true,
        });
        cy.url().should("include", "/home/consultar-validaciones");

    });

    it("Busqueda OK - cambio de tamaño página (Campos Fechas)", () => {
        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });

        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("444");

        cy.get('button[type="submit"]').click({ force: true });

        cy.wait("@consultavalidaciones");

        cy.get("select[id=inputSizePagination]").select("5", {
            force: true,
        });

        cy.wait("@consultavalidaciones");

        cy.get("select[id=inputSizePagination]").select("10", {
            force: true,
        });

        cy.wait("@consultavalidaciones");

        cy.get("select[id=inputSizePagination]").select("15", {
            force: true,
        });
        cy.wait("@consultavalidaciones");

        cy.get("select[id=inputSizePagination]").select("20", {
            force: true,
        });
        cy.url().should("include", "/home/consultar-validaciones");

    });

});
