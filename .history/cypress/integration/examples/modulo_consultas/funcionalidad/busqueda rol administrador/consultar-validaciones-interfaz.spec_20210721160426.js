/// <reference types="cypress" />

context("Pruebas consultar-validaciones", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const userSinData = Cypress.env("userSinData");
    const userData = Cypress.env("userData");
    const url = Cypress.env("url");

    beforeEach(() => {
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixbio/consultavalidaciones",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/service/reportevalidaciones",
            []
        );

        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarrolesagrupador");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarusuarios");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixbio/consultavalidaciones",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("consultavalidaciones");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/service/reportevalidaciones",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("reportevalidaciones");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);

        cy.get("a").contains("Módulo de Consultas").click({ force: true });
        cy.wait(500);
        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarusuarios2");

        cy.get("a").contains("Consultar Validaciones").click({ force: true });
        cy.wait("@listarusuarios2");
    });

 
    it("Busqueda con rol administrador (Campo Cantidad)", () => {
        cy.get('select[formControlName="filterUsuario"]').select("evaldivia", {
            force: true,
        });
        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("4444");

        cy.get('button[type="submit"]').click({ force: true });
    });

    it("Busqueda con rol administrador (Campos Fechas)", () => {
        cy.get("#defaultUnchecked").click({ force: true });
        cy.get('select[formControlName="filterUsuario"]').select("evaldivia", {
            force: true,
        });
        var fecha = new Date();
        var year = fecha.getFullYear();
        cy.get('input[formControlName="filterfechaDesde"]').click({
            force: true,
        });
        cy.get('button[class="current"]').contains(year).click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains(year - 7)
            .click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains("enero")
            .click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });

        cy.get('input[formControlName="filterfechaHasta"]').click({
            force: true,
        });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });

        cy.get('button[type="submit"]').click({ force: true });
        cy.wait("@consultavalidaciones");
        cy.get("button[id=excel]").click({ force: true });

        cy.get("button[id=pdf]").click({ force: true });
    });
  
});
