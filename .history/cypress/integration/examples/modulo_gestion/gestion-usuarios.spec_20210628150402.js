/// <reference types="cypress" />

context("Pruebas gestion-usuarios", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const url = Cypress.env("url");
    //    const userReset = Cypress.env("userReset");
    //    const passReset = Cypress.env("passReset");

    beforeEach(() => {
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
        }).as("listarusuarios");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);
    });

    it("Verificamos entrada a gestion usuario", () => {
        cy.url().should("include", "/home/gestion-usuarios");
        // Verificamos los valores que deben setear en el localStorage
        cy.url().should(() => {
            expect(localStorage.getItem("isLoggedin")).to.eq("true");
            expect(localStorage.getItem("RolUsuario")).to.eq("EMP_ADM");
            expect(localStorage.getItem("usuarioLogueado")).to.eq("jburgos6");
        });
        cy.get("div .card-header")
            .invoke("text")
            .then((text) => {
                expect(text).to.contain("Gestión de Usuarios");
            });
    });

    it("Verificamos nombres de los campos de entrada", () => {
        cy.get("label")
            .contains("Usuario")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Usuario");
            });

        cy.get("#inputUsuario")
            .invoke("attr", "placeholder")
            .should("contain", "Usuario");
        cy.get("#inputApellido")
            .invoke("attr", "placeholder")
            .should("contain", "Apellido");
        cy.get("#inputNombre")
            .invoke("attr", "placeholder")
            .should("contain", "Nombre");
        cy.get("#inputEstado")
            .invoke("text")
            .then((text) => {
                expect(text).to.contain(
                    "Seleccione...",
                    "CREADO",
                    "IMPORTADO",
                    "HABILITADO",
                    "BLOQUEADO",
                    "RESETEADO",
                    "INHABILITADO"
                );
            });
    });

    it("Verificamos nombres de los botones", () => {
        cy.get("button")
            .find("a")
            .contains("Nuevo")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" Nuevo");
            });

        cy.get("button")
            .find("a")
            .contains("Eliminar")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" Eliminar");
            });

        cy.get("button")
            .find("a")
            .contains("Excel")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Excel ");
            });

        cy.get("button")
            .find("a")
            .contains("PDF")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("PDF ");
            });
    });

    it("Verificamos nombres de las columnas", () => {
        cy.get("table")
            .find("thead")
            .find("tr")
            .find("th")
            .invoke("text")
            .then((text) => {
                expect(text).to.contain("Todos");
                expect(text).to.contain("Usuario");
                expect(text).to.contain("Nombre");
                expect(text).to.contain("Apellido");
                expect(text).to.contain("Restablecer clave");
                expect(text).to.contain("Operaciones");
            });
    });

    it("Verificamos longitud maxima del filtro Usuario", () => {
        cy.get('input[formControlName="filterUsuario"]')
            .click({ force: true })
            .type("123456789012345678901", { delay: 100 })
            .should("have.value", "12345678901234567890");
    });

    it("Verificamos longitud maxima del filtro Apellido", () => {
        cy.get('input[formControlName="filterApellido"]')
            .click({ force: true })
            .type("933333333333333333333333333333333333333333333333339", {
                delay: 100,
            })
            .should(
                "have.value",
                "93333333333333333333333333333333333333333333333333"
            );
    });

    it("Verificamos longitud maxima del filtro Nombre", () => {
        cy.get('input[formControlName="filterNombre"]')
            .click({ force: true })
            .type("933333333333333333333333333333333333333333333333339", {
                delay: 100,
            })
            .should(
                "have.value",
                "93333333333333333333333333333333333333333333333333"
            );
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en filtro Usuario", () => {
        cy.get('input[formControlName="filterUsuario"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const U_Permitidos = ["aaaaaaaa", "cccccbbbbb", "c123bbb4b"];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="filterUsuario"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="filterUsuario"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
            "          ",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="filterUsuario"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="filterUsuario"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en filtro Apellido", () => {
        cy.get('input[formControlName="filterApellido"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const U_Permitidos = [
            "aaaaaaaa",
            "cccccbbbbb",
            "c123bbb4b",
            "test testzx",
        ];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="filterApellido"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="filterApellido"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="filterApellido"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="filterApellido"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en filtro Nombre", () => {
        cy.get('input[formControlName="filterNombre"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const U_Permitidos = [
            "aaaaaaaa",
            "cccccbbbbb",
            "c123bbb4b",
            "test testzx",
        ];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="filterNombre"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="filterNombre"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="filterNombre"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="filterNombre"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos funcionamiento boton nuevo", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.url()
            .should("include", "/home/gestion-usuarios/crear-usuario")
            .wait(1000);
    });

    it("Verificamos nombres de los campos de entrada de crear-usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);
        cy.get("label[for=inputUsuario]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Usuario (*)");
            });

        cy.get("label[for=inputNombre]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Nombre (*)");
            });

        cy.get("label[for=inputApellidoPaterno]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Apellido Paterno (*)");
            });

        cy.get("label[for=inputApellidoMaterno]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Apellido Materno (*)");
            });

        cy.get("label[for=inputTelefono]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Teléfono");
            });

        cy.get("label[for=inputEstado]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" Estado");
            });

        cy.get("div[for=inputNacimiento]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" Fecha de Nacimiento");
            });

        cy.get("label[for=inputTipoDocumento]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tipo de documento (*)");
            });

        cy.get("label[for=inputNroDocumento]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Número de documento(*)");
            });

        cy.get("label[for=inputCorreo]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Correo (*)");
            });

        cy.get("label[for=inputRolAgrupador]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Rol Agrupador (*)");
            });

        cy.get("#inputUser")
            .invoke("attr", "placeholder")
            .should("contain", "Usuario");

        cy.get("#inputNombre")
            .invoke("attr", "placeholder")
            .should("contain", "Nombre");

        cy.get("#inputApellidoPaterno")
            .invoke("attr", "placeholder")
            .should("contain", "Apellido Paterno");

        cy.get("#inputApellidoMaterno")
            .invoke("attr", "placeholder")
            .should("contain", "Apellido Materno");

        cy.get("#inputTelefono")
            .invoke("attr", "placeholder")
            .should("contain", "Teléfono");

        cy.get("#inputTelefono")
            .invoke("attr", "placeholder")
            .should("contain", "Teléfono");

        cy.get("#inputNroDocumento")
            .invoke("attr", "placeholder")
            .should("contain", "Número de documento");

        cy.get("#inputCorreo")
            .invoke("attr", "placeholder")
            .should("contain", "email@example.com");
    });

    it("Verificamos nombres de los botones en crear-usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);

        cy.get('button[name="submit"]')
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Guardar");
            });

        cy.get('button[name="cancel"]')
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Cancelar ");
            });
    });

    it("Verificamos mensajes de validaciones del formulario crear-usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();

        cy.get('input[formControlName="nombre"]')
            .click({ force: true })
            .clear();

        cy.get('input[formControlName="apPaterno"]')
            .click({ force: true })
            .clear();

        cy.get('input[formControlName="apMaterno"]')
            .click({ force: true })
            .clear();

        cy.get('input[formControlName="telef"]').click({ force: true }).clear();

        cy.get('input[formControlName="fecNac"]').click({ force: true });
        cy.get('input[formControlName="fecNac"]').click({ force: true });

        cy.get('select[formControlName="tipoDoc"]').select("Seleccione...", {
            force: true,
        });

        cy.get('input[formControlName="numDoc"]').click({ force: true });

        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();

        cy.get("small[id=reqUser]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese usuario");
            });

        cy.get("small[id=reqName]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese nombre");
            });

        cy.get("small[id=reqPaternal]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese apellido paterno");
            });

        cy.get("small[id=reqMaternal]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" Ingrese apellido materno");
            });

        cy.get("small[id=reqDate]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingresar fecha de nacimiento");
            });

        cy.get("small[id=reqEmail]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese un email");
            });

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type("123");

        cy.get('input[formControlName="telef"]')
            .click({ force: true })
            .type("123");

        cy.get('select[formControlName="tipoDoc"]').select("DNI", {
            force: true,
        });
        cy.get('input[formControlName="numDoc"]')
            .click({ force: true })
            .type("123");

        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .type("123");

        cy.get('input[formControlName="numDoc"]').click({ force: true });
        cy.get("#minUser")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 5 caracteres");
            });

        cy.get("#minTelefono")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño minimo de 9 caracteres");
            });

        cy.get("#minNDoc")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" El tamaño minimo de caracteres es 8");
            });

        cy.get("#invalidEmail")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "El correo electrónico debe ser una dirección de correo electrónico válida"
                );
            });

        cy.get('button[name="submit"]').should("be.disabled");
    });

    it.only("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en campo usuario de crear-usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const U_Permitidos = ["aaaaaaaa", "cccccbbbbb", "c123bbb4b"];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="usuario"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="usuario"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
            "          ",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="usuario"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="usuario"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it.only("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en campo nombre de crear-usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);

        cy.get('input[formControlName="nombre"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const U_Permitidos = [
            "aaaaaaaa",
            "cccccbbbbb",
            "c123bbb4b",
            "test testzx",
        ];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="nombre"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="nombre"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="nombre"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="nombre"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it.only("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en campo apellido paterno de crear-usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);

        cy.get('input[formControlName="apPaterno"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const U_Permitidos = [
            "aaaaaaaa",
            "cccccbbbbb",
            "c123bbb4b",
            "test testzx",
        ];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="apPaterno"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="apPaterno"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="apPaterno"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="apPaterno"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it.only("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en campo apellido materno de crear-usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);

        cy.get('input[formControlName="apMaterno"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const U_Permitidos = [
            "aaaaaaaa",
            "cccccbbbbb",
            "c123bbb4b",
            "test testzx",
        ];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="apMaterno"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="apMaterno"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="apMaterno"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="apMaterno"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it.only("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en campo telefono de crear-usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);

        const U_Permitidos = [
            "123456789",
        ];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="telef"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="telef"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "asdsdfgsdf",
            "sdfdñdhgfgdf",
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="telef"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="telef"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });



    it("Verificamos registro de usuario random", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);
        const userRandom = generateRandomString(11).replace(".", "");
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userRandom);

        cy.get('input[formControlName="nombre"]')
            .click({ force: true })
            .type(generateRandomString(11));

        cy.get('input[formControlName="apPaterno"]')
            .click({ force: true })
            .type(generateRandomString(11));

        cy.get('input[formControlName="apMaterno"]')
            .click({ force: true })
            .type(generateRandomString(11));

        cy.get('select[formControlName="tipoDoc"]').select("DNI", {
            force: true,
        });

        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .type("jburgos@novatronic.com");

        cy.get('input[formControlName="numDoc"]')
            .click({ force: true })
            .type("12345678");

        cy.get('input[formControlName="fecNac"]').click({ force: true });
        cy.get("span.ng-star-inserted").contains("13").click({ force: true });
        cy.get("span.ng-star-inserted").contains("13").click({ force: true });

        cy.get('button[name="submit"]').click({ force: true });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("satisfactoriamente")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "El Usuario: " +
                        userRandom +
                        " fue creado satisfactoriamente"
                );
            });
    });

    it("Verificamos funcionamiento boton ver Detalles", () => {
        cy.get('button[title="Ver detalle"]').first().click({ force: true });
        cy.wait(1000);
        cy.url().should("include", "/home/gestion-usuarios/detalle-usuario");
    });
    //ng-tns-c153-9

    it("Verificamos funcionamiento boton Restablecer clave", () => {
        cy.get('button[title="Restablecer clave"]')
            .first()
            .click({ force: true });
        cy.wait(1000);
        cy.get("p")
            .contains("restablecer")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "¿Desea restablecer la contraseña del usuario?"
                );
            });
    });

    it("Verificamos funcionamiento boton Editar Usuario", () => {
        cy.get('button[title="Editar"]').first().click({ force: true });
        cy.wait(1000);
        cy.url().should("include", "/home/gestion-usuarios/editar-usuario");
    });

    it("Verificamos funcionamiento boton Excel", () => {
        cy.get("button").contains("Excel").click({ force: true });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("reporte")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Generando reporte, espere un momento");
            });
    });

    it("Verificamos funcionamiento boton PDF", () => {
        cy.get("button").contains("PDF").click({ force: true });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("reporte")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Generando reporte, espere un momento");
            });
    });

    // Generando reporte, espere un momento
    //Generador de cadena random
    const generateRandomString = (num) => {
        const characters =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        let result1 = Math.random().toString(36).substring(0, num);

        return result1;
    };

    /*
cy.get('input[name="dob"]').first()
                .then((dtp) => {
                    cy.wrap(dtp).click()
                    .get('.current').contains('2020').click()
                    .get('.previous').click()
                    .get('.years').contains('2000').click()
                    .get('.months').contains('January').click()
                    .get('.days').contains('1').click() //.invoke('text')
                    .then((item) => {
                        cy.wrap(item).click(); // Click on the option
                    });
                });

*/
});

//  cy.url().should('have.value', 'http://localhost:4200/#/home/gestion-usuarios');
/*
      // .type() with special character sequences
      .type('{leftarrow}{rightarrow}{uparrow}{downarrow}')
      .type('{del}{selectall}{backspace}')

      // .type() with key modifiers
      .type('{alt}{option}') //these are equivalent
      .type('{ctrl}{control}') //these are equivalent
      .type('{meta}{command}{cmd}') //these are equivalent
      .type('{shift}')

      // Delay each keypress by 0.1 sec
      .type('slow.typing@email.com', { delay: 100 })
      .should('have.value', 'slow.typing@email.com')

    cy.get('.action-disabled')
      // Ignore error checking prior to type
      // like whether the input is visible or disabled
      .type('disabled error checking', { force: true })
      .should('have.value', 'disabled error checking')*/

/*
  it('.focus() - focus on a DOM element', () => {
    // https://on.cypress.io/focus
    cy.get('.action-focus').focus()
      .should('have.class', 'focus')
      .prev().should('have.attr', 'style', 'color: orange;')
  })

  it('.blur() - blur off a DOM element', () => {
    // https://on.cypress.io/blur
    cy.get('.action-blur').type('About to blur').blur()
      .should('have.class', 'error')
      .prev().should('have.attr', 'style', 'color: red;')
  })

  it('.clear() - clears an input or textarea element', () => {
    // https://on.cypress.io/clear
    cy.get('.action-clear').type('Clear this text')
      .should('have.value', 'Clear this text')
      .clear()
      .should('have.value', '')
  })

  it('.submit() - submit a form', () => {
    // https://on.cypress.io/submit
    cy.get('.action-form')
      .find('[type="text"]').type('HALFOFF')

    cy.get('.action-form').submit()
      .next().should('contain', 'Your form has been submitted!')
  })

  it('.click() - click on a DOM element', () => {
    // https://on.cypress.io/click
    cy.get('.action-btn').click()

    // You can click on 9 specific positions of an element:
    //  -----------------------------------
    // | topLeft        top       topRight |
    // |                                   |
    // |                                   |
    // |                                   |
    // | left          center        right |
    // |                                   |
    // |                                   |
    // |                                   |
    // | bottomLeft   bottom   bottomRight |
    //  -----------------------------------

    // clicking in the center of the element is the default
    cy.get('#action-canvas').click()

    cy.get('#action-canvas').click('topLeft')
    cy.get('#action-canvas').click('top')
    cy.get('#action-canvas').click('topRight')
    cy.get('#action-canvas').click('left')
    cy.get('#action-canvas').click('right')
    cy.get('#action-canvas').click('bottomLeft')
    cy.get('#action-canvas').click('bottom')
    cy.get('#action-canvas').click('bottomRight')

    // .click() accepts an x and y coordinate
    // that controls where the click occurs :)

    cy.get('#action-canvas')
      .click(80, 75) // click 80px on x coord and 75px on y coord
      .click(170, 75)
      .click(80, 165)
      .click(100, 185)
      .click(125, 190)
      .click(150, 185)
      .click(170, 165)

    // click multiple elements by passing multiple: true
    cy.get('.action-labels>.label').click({ multiple: true })

    // Ignore error checking prior to clicking
    cy.get('.action-opacity>.btn').click({ force: true })
  })

  it('.dblclick() - double click on a DOM element', () => {
    // https://on.cypress.io/dblclick

    // Our app has a listener on 'dblclick' event in our 'scripts.js'
    // that hides the div and shows an input on double click
    cy.get('.action-div').dblclick().should('not.be.visible')
    cy.get('.action-input-hidden').should('be.visible')
  })

  it('.rightclick() - right click on a DOM element', () => {
    // https://on.cypress.io/rightclick

    // Our app has a listener on 'contextmenu' event in our 'scripts.js'
    // that hides the div and shows an input on right click
    cy.get('.rightclick-action-div').rightclick().should('not.be.visible')
    cy.get('.rightclick-action-input-hidden').should('be.visible')
  })

  it('.check() - check a checkbox or radio element', () => {
    // https://on.cypress.io/check

    // By default, .check() will check all
    // matching checkbox or radio elements in succession, one after another
    cy.get('.action-checkboxes [type="checkbox"]').not('[disabled]')
      .check().should('be.checked')

    cy.get('.action-radios [type="radio"]').not('[disabled]')
      .check().should('be.checked')

    // .check() accepts a value argument
    cy.get('.action-radios [type="radio"]')
      .check('radio1').should('be.checked')

    // .check() accepts an array of values
    cy.get('.action-multiple-checkboxes [type="checkbox"]')
      .check(['checkbox1', 'checkbox2']).should('be.checked')

    // Ignore error checking prior to checking
    cy.get('.action-checkboxes [disabled]')
      .check({ force: true }).should('be.checked')

    cy.get('.action-radios [type="radio"]')
      .check('radio3', { force: true }).should('be.checked')
  })

  it('.uncheck() - uncheck a checkbox element', () => {
    // https://on.cypress.io/uncheck

    // By default, .uncheck() will uncheck all matching
    // checkbox elements in succession, one after another
    cy.get('.action-check [type="checkbox"]')
      .not('[disabled]')
      .uncheck().should('not.be.checked')

    // .uncheck() accepts a value argument
    cy.get('.action-check [type="checkbox"]')
      .check('checkbox1')
      .uncheck('checkbox1').should('not.be.checked')

    // .uncheck() accepts an array of values
    cy.get('.action-check [type="checkbox"]')
      .check(['checkbox1', 'checkbox3'])
      .uncheck(['checkbox1', 'checkbox3']).should('not.be.checked')

    // Ignore error checking prior to unchecking
    cy.get('.action-check [disabled]')
      .uncheck({ force: true }).should('not.be.checked')
  })

  it('.select() - select an option in a <select> element', () => {
    // https://on.cypress.io/select

    // at first, no option should be selected
    cy.get('.action-select')
      .should('have.value', '--Select a fruit--')

    // Select option(s) with matching text content
    cy.get('.action-select').select('apples')
    // confirm the apples were selected
    // note that each value starts with "fr-" in our HTML
    cy.get('.action-select').should('have.value', 'fr-apples')

    cy.get('.action-select-multiple')
      .select(['apples', 'oranges', 'bananas'])
      // when getting multiple values, invoke "val" method first
      .invoke('val')
      .should('deep.equal', ['fr-apples', 'fr-oranges', 'fr-bananas'])

    // Select option(s) with matching value
    cy.get('.action-select').select('fr-bananas')
      // can attach an assertion right away to the element
      .should('have.value', 'fr-bananas')

    cy.get('.action-select-multiple')
      .select(['fr-apples', 'fr-oranges', 'fr-bananas'])
      .invoke('val')
      .should('deep.equal', ['fr-apples', 'fr-oranges', 'fr-bananas'])

    // assert the selected values include oranges
    cy.get('.action-select-multiple')
      .invoke('val').should('include', 'fr-oranges')
  })

  it('.scrollIntoView() - scroll an element into view', () => {
    // https://on.cypress.io/scrollintoview

    // normally all of these buttons are hidden,
    // because they're not within
    // the viewable area of their parent
    // (we need to scroll to see them)
    cy.get('#scroll-horizontal button')
      .should('not.be.visible')

    // scroll the button into view, as if the user had scrolled
    cy.get('#scroll-horizontal button').scrollIntoView()
      .should('be.visible')

    cy.get('#scroll-vertical button')
      .should('not.be.visible')

    // Cypress handles the scroll direction needed
    cy.get('#scroll-vertical button').scrollIntoView()
      .should('be.visible')

    cy.get('#scroll-both button')
      .should('not.be.visible')

    // Cypress knows to scroll to the right and down
    cy.get('#scroll-both button').scrollIntoView()
      .should('be.visible')
  })

  it('.trigger() - trigger an event on a DOM element', () => {
    // https://on.cypress.io/trigger

    // To interact with a range input (slider)
    // we need to set its value & trigger the
    // event to signal it changed

    // Here, we invoke jQuery's val() method to set
    // the value and trigger the 'change' event
    cy.get('.trigger-input-range')
      .invoke('val', 25)
      .trigger('change')
      .get('input[type=range]').siblings('p')
      .should('have.text', '25')
  })

  it('cy.scrollTo() - scroll the window or element to a position', () => {
    // https://on.cypress.io/scrollto

    // You can scroll to 9 specific positions of an element:
    //  -----------------------------------
    // | topLeft        top       topRight |
    // |                                   |
    // |                                   |
    // |                                   |
    // | left          center        right |
    // |                                   |
    // |                                   |
    // |                                   |
    // | bottomLeft   bottom   bottomRight |
    //  -----------------------------------

    // if you chain .scrollTo() off of cy, we will
    // scroll the entire window
    cy.scrollTo('bottom')

    cy.get('#scrollable-horizontal').scrollTo('right')

    // or you can scroll to a specific coordinate:
    // (x axis, y axis) in pixels
    cy.get('#scrollable-vertical').scrollTo(250, 250)

    // or you can scroll to a specific percentage
    // of the (width, height) of the element
    cy.get('#scrollable-both').scrollTo('75%', '25%')

    // control the easing of the scroll (default is 'swing')
    cy.get('#scrollable-vertical').scrollTo('center', { easing: 'linear' })

    // control the duration of the scroll (in ms)
    cy.get('#scrollable-both').scrollTo('center', { duration: 2000 })
  })*/
