/// <reference types="cypress" />

context("Pruebas gestion-usuarios", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const url = Cypress.env("url");
    //    const userReset = Cypress.env("userReset");
    //    const passReset = Cypress.env("passReset");

    beforeEach(() => {


        cy.intercept('GET', url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador", []);
        cy.intercept('GET', url + "SIXBIO-webcore-Nova/sixsca/listarusuarios", []);

        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
        }).as("listarusuarios");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);
    });

    it("Verificamos entrada a gestion usuario", () => {
        cy.url().should("include", "/home/gestion-usuarios");
        // Verificamos los valores que deben setear en el localStorage
        cy.url().should(() => {
            expect(localStorage.getItem("isLoggedin")).to.eq("true");
            expect(localStorage.getItem("RolUsuario")).to.eq("EMP_ADM");
            expect(localStorage.getItem("usuarioLogueado")).to.eq(userAdmin);
        });
        cy.get("div .card-header")
            .invoke("text")
            .then((text) => {
                expect(text).to.contain("Gestión de Usuarios");
            });
    });

    it("Verificamos nombres de los campos de entrada crear-usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });
        cy.wait(1500);

        cy.get("label[for='inputUsuario']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Usuario (*)");
            });

        cy.get("label[for='inputNombre']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nombre (*)");
            });

        cy.get("label[for='inputApellidoPaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Paterno (*)");
            });

        cy.get("label[for='inputApellidoMaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Materno (*)");
            });

        cy.get("label[for='inputTelefono']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Teléfono");
            });

        cy.get("label[for='inputEstado']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Estado");
            });

        cy.get("label[for='inputNacimiento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha de Nacimiento");
            });

        cy.get("label[for='inputTipoDocumento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Tipo de documento (*)");
            });

        cy.get("label[for='inputNroDocumento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Número de documento(*)");
            });

        cy.get("label[for='inputCorreo']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Correo (*)");
            });

        cy.get("label[for='inputRolAgrupador']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Rol Agrupador (*)");
            });
    });

    it("Verificamos nombres de los botones", () => {
        cy.get("button")
            .find("a")
            .contains("Nuevo")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" Nuevo");
            });

        cy.get("button")
            .find("a")
            .contains("Eliminar")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" Eliminar");
            });

        cy.get("button")
            .find("a")
            .contains("Excel")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Excel ");
            });

        cy.get("button")
            .find("a")
            .contains("PDF")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("PDF ");
            });
    });

    it("Verificamos nombres de las columnas", () => {
        cy.get("table")
            .find("thead")
            .find("tr")
            .find("th")
            .invoke("text")
            .then((text) => {
                expect(text).to.contain("Todos");
                expect(text).to.contain("Usuario");
                expect(text).to.contain("Nombre");
                expect(text).to.contain("Apellido");
                expect(text).to.contain("Restablecer clave");
                expect(text).to.contain("Operaciones");
            });
    });

    it("Verificamos mensaje error boton Eliminar sin selecciona usuarios", () => {
        cy.get("button").find("a").contains("Eliminar").click({ force: true });
        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("eliminar")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Seleccione usuario(s) a eliminar");
            });
    });

    it("Verificamos longitud maxima del filtro Usuario", () => {
        cy.get('input[formControlName="filterUsuario"]')
            .click({ force: true })
            .type("123456789012345678901", { delay: 100 })
            .should("have.value", "12345678901234567890");
    });

    it("Verificamos longitud maxima del filtro Apellido", () => {
        cy.get('input[formControlName="filterApellido"]')
            .click({ force: true })
            .type("933333333333333333333333333333333333333333333333339", {
                delay: 100,
            })
            .should(
                "have.value",
                "93333333333333333333333333333333333333333333333333"
            );
    });

    it("Verificamos longitud maxima del filtro Nombre", () => {
        cy.get('input[formControlName="filterNombre"]')
            .click({ force: true })
            .type("933333333333333333333333333333333333333333333333339", {
                delay: 100,
            })
            .should(
                "have.value",
                "93333333333333333333333333333333333333333333333333"
            );
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en filtro Usuario", () => {
        cy.get('input[formControlName="filterUsuario"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const U_Permitidos = ["aaaaaaaa", "cccccbbbbb", "c123bbb4b"];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="filterUsuario"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="filterUsuario"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
            "          ",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="filterUsuario"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="filterUsuario"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en filtro Apellido", () => {
        cy.get('input[formControlName="filterApellido"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const U_Permitidos = [
            "aaaaaaaa",
            "cccccbbbbb",
            "c123bbb4b",
            "test testzx",
        ];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="filterApellido"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="filterApellido"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="filterApellido"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="filterApellido"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en filtro Nombre", () => {
        cy.get('input[formControlName="filterNombre"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const U_Permitidos = [
            "aaaaaaaa",
            "cccccbbbbb",
            "c123bbb4b",
            "test testzx",
        ];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="filterNombre"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="filterNombre"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="filterNombre"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="filterNombre"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos funcionamiento boton nuevo", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.url()
            .should("include", "/home/gestion-usuarios/crear-usuario")
            .wait(1000);
    });

    it("Verificamos el retorno de crear-usuario a gestion usuario", () => {
        cy.url().should("include", "/home/gestion-usuarios");
        cy.get("button").find("a").contains("Nuevo").click({ force: true });
        cy.url().should("include", "/home/gestion-usuarios/crear-usuario");
        cy.wait(1500);
        cy.get("button").find("a").contains("Cancelar").click({ force: true });
        cy.url().should("include", "/home/gestion-usuarios");
    });

    it("Verificamos nombres de los campos de entrada de crear-usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);
        cy.get("label[for=inputUsuario]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Usuario (*)");
            });

        cy.get("label[for=inputNombre]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Nombre (*)");
            });

        cy.get("label[for=inputApellidoPaterno]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Apellido Paterno (*)");
            });

        cy.get("label[for=inputApellidoMaterno]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Apellido Materno (*)");
            });

        cy.get("label[for=inputTelefono]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Teléfono");
            });

        cy.get("label[for=inputEstado]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" Estado");
            });

        cy.get("label[for=inputNacimiento]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" Fecha de Nacimiento");
            });

        cy.get("label[for=inputTipoDocumento]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tipo de documento (*)");
            });

        cy.get("label[for=inputNroDocumento]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Número de documento(*)");
            });

        cy.get("label[for=inputCorreo]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Correo (*)");
            });

        cy.get("label[for=inputRolAgrupador]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Rol Agrupador (*)");
            });

        cy.get("#inputUser")
            .invoke("attr", "placeholder")
            .should("contain", "Usuario");

        cy.get("#inputNombre")
            .invoke("attr", "placeholder")
            .should("contain", "Nombre");

        cy.get("#inputApellidoPaterno")
            .invoke("attr", "placeholder")
            .should("contain", "Apellido Paterno");

        cy.get("#inputApellidoMaterno")
            .invoke("attr", "placeholder")
            .should("contain", "Apellido Materno");

        cy.get("#inputTelefono")
            .invoke("attr", "placeholder")
            .should("contain", "Teléfono");

        cy.get("#inputTelefono")
            .invoke("attr", "placeholder")
            .should("contain", "Teléfono");

        cy.get("#inputNroDocumento")
            .invoke("attr", "placeholder")
            .should("contain", "Número de documento");

        cy.get("#inputCorreo")
            .invoke("attr", "placeholder")
            .should("contain", "email@example.com");

        cy.get("#inputRolAgrupador")
            .invoke("text")
            .then((text) => {
                expect(text).to.contain("Administrador", "Verificador");
            });
    });

    it("Verificamos nombres de los botones en crear-usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);

        cy.get('button[name="submit"]')
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Guardar");
            });

        cy.get('button[name="cancel"]')
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Cancelar ");
            });
    });

    it("Verificamos mensajes de validaciones del formulario crear-usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();

        cy.get('input[formControlName="nombre"]')
            .click({ force: true })
            .clear();

        cy.get('input[formControlName="apPaterno"]')
            .click({ force: true })
            .clear();

        cy.get('input[formControlName="apMaterno"]')
            .click({ force: true })
            .clear();

        cy.get('input[formControlName="telef"]').click({ force: true }).clear();

        cy.get('input[formControlName="fecNac"]').click({ force: true });
        cy.get('input[formControlName="fecNac"]').click({ force: true });

        cy.get('select[formControlName="tipoDoc"]').select("Seleccione...", {
            force: true,
        });

        cy.get('input[formControlName="numDoc"]').click({ force: true });

        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();

        cy.get("small[id=reqUser]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese usuario");
            });

        cy.get("small[id=reqName]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese nombre");
            });

        cy.get("small[id=reqPaternal]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese apellido paterno");
            });

        cy.get("small[id=reqMaternal]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" Ingrese apellido materno");
            });

        cy.get("small[id=reqDate]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingresar fecha de nacimiento");
            });

        cy.get("small[id=reqEmail]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese un email");
            });

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type("123");

        cy.get('input[formControlName="telef"]')
            .click({ force: true })
            .type("123");

        cy.get('select[formControlName="tipoDoc"]').select("DNI", {
            force: true,
        });
        cy.get('input[formControlName="numDoc"]')
            .click({ force: true })
            .type("123");

        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .type("123");

        cy.get('input[formControlName="numDoc"]').click({ force: true });
        cy.get("#minUser")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 5 caracteres");
            });

        cy.get("#minTelefono")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño minimo de 9 caracteres");
            });

        cy.get("#minNDoc")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" El tamaño minimo de caracteres es 8");
            });

        cy.get("#invalidEmail")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "El correo electrónico debe ser una dirección de correo electrónico válida"
                );
            });

        cy.get('button[name="submit"]').should("be.disabled");
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en campo usuario de crear-usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const U_Permitidos = ["aaaaaaaa", "cccccbbbbb", "c123bbb4b"];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="usuario"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="usuario"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
            "          ",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="usuario"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="usuario"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en campo nombre de crear-usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);

        cy.get('input[formControlName="nombre"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const U_Permitidos = [
            "aaaaaaaa",
            "cccccbbbbb",
            "c123bbb4b",
            "test testzx",
        ];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="nombre"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="nombre"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="nombre"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="nombre"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en campo apellido paterno de crear-usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);

        cy.get('input[formControlName="apPaterno"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const U_Permitidos = [
            "aaaaaaaa",
            "cccccbbbbb",
            "c123bbb4b",
            "test testzx",
        ];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="apPaterno"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="apPaterno"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="apPaterno"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="apPaterno"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en campo apellido materno de crear-usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);

        cy.get('input[formControlName="apMaterno"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const U_Permitidos = [
            "aaaaaaaa",
            "cccccbbbbb",
            "c123bbb4b",
            "test testzx",
        ];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="apMaterno"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="apMaterno"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="apMaterno"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="apMaterno"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en campo telefono de crear-usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);

        const U_Permitidos = ["123456789"];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="telef"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="telef"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "asdsdfgsdf",
            "sdfdñdhgfgdf",
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="telef"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="telef"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en campo Numero de Documento de crear-usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);
        // Validamos caso DNI
        cy.get('select[formControlName="tipoDoc"]').select("DNI", {
            force: true,
        });
        cy.get('input[formControlName="numDoc"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="numDoc"]')
            .click({ force: true })
            .type("12345678")
            .should("have.value", "12345678");
        // Validamos caso PASAPORTE
        cy.get('select[formControlName="tipoDoc"]').select("PASAPORTE", {
            force: true,
        });
        cy.get('input[formControlName="numDoc"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="numDoc"]')
            .click({ force: true })
            .type("123456789012")
            .should("have.value", "123456789012");
        // Validamos caso RUC
        cy.get('select[formControlName="tipoDoc"]').select("RUC", {
            force: true,
        });
        cy.get('input[formControlName="numDoc"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="numDoc"]')
            .click({ force: true })
            .type("12345678901")
            .should("have.value", "12345678901");

        const U_No_Permitidos = [
            "asdsdfgsdf",
            "sdfdñdhgfgdf",
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="numDoc"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="numDoc"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en campo correo de crear-usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);
        // Validamos caso DNI  “.”, “-”, “_” y ”@”

        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .clear();

        const U_Permitidos = [
            "..zxssssddd---___@gmail.com",
            "___@gmail.com",
            "---@---@gmail.com",
        ];
        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="correo"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="correo"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="correo"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="correo"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos registro de usuario random", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);
        const userRandom = generateRandomString(11).replace(".", "");
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userRandom);

        cy.get('input[formControlName="nombre"]')
            .click({ force: true })
            .type(generateRandomString(11));

        cy.get('input[formControlName="apPaterno"]')
            .click({ force: true })
            .type(generateRandomString(11));

        cy.get('input[formControlName="apMaterno"]')
            .click({ force: true })
            .type(generateRandomString(11));

        cy.get('select[formControlName="tipoDoc"]').select("DNI", {
            force: true,
        });

        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .type("jburgos@novatronic.com");

        cy.get('input[formControlName="numDoc"]')
            .click({ force: true })
            .type("12345678");

        cy.get('input[formControlName="fecNac"]').click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });

        cy.get('button[name="submit"]').click({ force: true });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("satisfactoriamente")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "El Usuario: " +
                        userRandom +
                        " fue creado satisfactoriamente"
                );
            });
    });



    it("Verificamos mensaje de error para usuario ya existente en crear usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);
        
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin);

        cy.get('input[formControlName="nombre"]')
            .click({ force: true })
            .type(generateRandomString(11));

        cy.get('input[formControlName="apPaterno"]')
            .click({ force: true })
            .type(generateRandomString(11));

        cy.get('input[formControlName="apMaterno"]')
            .click({ force: true })
            .type(generateRandomString(11));

        cy.get('select[formControlName="tipoDoc"]').select("DNI", {
            force: true,
        });

        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .type("jburgos@novatronic.com");

        cy.get('input[formControlName="numDoc"]')
            .click({ force: true })
            .type("12345678");

        cy.get('input[formControlName="fecNac"]').click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });

        cy.get('button[name="submit"]').click({ force: true });

     cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("usuario")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                 "No se pudo registrar el usuario"
                );
            });
    });








    it("Verificamos funcionamiento boton ver Detalles", () => {
        cy.get('button[title="Ver detalle"]').first().click({ force: true });
        cy.wait(1000);
        cy.url().should("include", "/home/gestion-usuarios/detalle-usuario");
    });

    it("Verificamos labels de detalle-usuario", () => {
        cy.get('button[title="Ver detalle"]').first().click({ force: true });
        cy.wait(1000);
        cy.get("label[for='inputUsuario']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Usuario (*)");
            });

        cy.get("label[for='inputNombre']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nombre (*)");
            });

        cy.get("label[for='inputApellidoPaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Paterno (*)");
            });

        cy.get("label[for='inputApellidoMaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Materno (*)");
            });

        cy.get("label[for='inputTelefono']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Teléfono");
            });

        cy.get("label[for='inputEstados']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Estado (*)");
            });

        cy.get("label[for='inputFechaNacimiento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha de Nacimiento");
            });

        cy.get("label[for='inputTipoDocumentos']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Tipo de documento (*)");
            });

        cy.get("label[for='inputNroDocumento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Número de documento(*)");
            });

        cy.get("label[for='inputCorreo']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Correo (*)");
            });

        cy.get("label[for='inputRolAgrupador']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Rol Agrupador (*)");
            });

        cy.get("button[type=button]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Regresar");
            });
    });

    it("Verificamos inputs de detalle-usuario esten deshabilitados", () => {
        cy.get('button[title="Ver detalle"]').first().click({ force: true });
        cy.wait(1000);
        cy.get("#inputUsuario").should("be.disabled");
        cy.get("#inputNombre").should("be.disabled");
        cy.get("#inputApellidoPaterno").should("be.disabled");
        cy.get("#inputApellidoMaterno").should("be.disabled");
        cy.get("#inputTelefono").should("be.disabled");
        cy.get("#inputEstados").should("be.disabled");
        cy.get("input[name=inputFechaNacimiento]").should("be.disabled");
        cy.get("#inputTipoDocumentos").should("be.disabled");
        cy.get("#inputNroDocumento").should("be.disabled");
        cy.get("#inputCorreo").should("be.disabled");
        cy.get("#inputRolAgrupador").should("be.disabled");
    });

    it("Verificamos funcionamiento boton Restablecer clave", () => {
        cy.get('button[title="Restablecer clave"]')
            .first()
            .click({ force: true });
        cy.wait(1000);
        cy.get("p")
            .contains("restablecer")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "¿Desea restablecer la contraseña del usuario?"
                );
            });
    });

    it("Verificamos funcionamiento boton Editar Usuario", () => {
        cy.get('button[title="Editar"]').first().click({ force: true });
        cy.wait(1000);
        cy.url().should("include", "/home/gestion-usuarios/editar-usuario");
    });

    it("Verificamos label de editar-usuario", () => {
        cy.get('button[title="Editar"]').first().click({ force: true });
        cy.wait(1000);

        cy.get("label[for=inputUsuario]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Usuario (*)");
            });

        cy.get("label[for=inputNombre]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nombre (*)");
            });

        cy.get("label[for=inputApellidoPaterno]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Paterno (*)");
            });

        cy.get("label[for=inputApellidoMaterno]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Materno (*)");
            });

        cy.get("label[for=inputTelefono]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Teléfono");
            });

        cy.get("label[for=inputEstado]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Estado (*)");
            });

        cy.get("label[for=inputNacimiento]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha de Nacimiento (*)");
            });

        cy.get("label[for=inputTipoDocumento]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Tipo de documento (*)");
            });

        cy.get("label[for=inputNroDocumento]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Número de documento(*)");
            });

        cy.get("label[for=inputCorreo]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Correo (*)");
            });

        cy.get("label[for=inputRolAgrupador]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Rol Agrupador (*)");
            });
    });

    it("Verificamos mensajes de validacion de form editar-usuario", () => {
        cy.get('button[title="Editar"]').first().click({ force: true });
        cy.wait(1000);

        cy.get('input[formControlName="nombre"]')
            .click({ force: true })
            .clear();

        cy.get('input[formControlName="apPaterno"]')
            .click({ force: true })
            .clear();

        cy.get('input[formControlName="apMaterno"]')
            .click({ force: true })
            .clear();

        cy.get('input[formControlName="telef"]').click({ force: true }).clear();

        cy.get('select[formControlName="estado"]').select("Seleccione...", {
            force: true,
        });

        cy.get('input[formControlName="numDoc"]').click({ force: true });

        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .clear();

        cy.get("small[id=reqName]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese nombre");
            });

        cy.get("small[id=reqPaternal]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese apellido paterno");
            });

        cy.get("small[id=reqMaternal]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" Ingrese apellido materno");
            });
        cy.get('input[formControlName="apMaterno"]').click({ force: true });
        cy.get("small[id=reqEmail]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese un email");
            });

        cy.get('input[formControlName="telef"]')
            .click({ force: true })
            .type("123");

        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .type("123");

        cy.get("#minTelefono")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño minimo de 9 caracteres");
            });

        cy.get("#invalidEmail")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "El correo electrónico debe ser una dirección de correo electrónico válida"
                );
            });

        cy.get('button[type="submit"]').should("be.disabled");
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en campo nombre de editar-usuario", () => {
        cy.get('button[title="Editar"]').first().click({ force: true });
        cy.wait(1000);
        cy.get('input[formControlName="nombre"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="nombre"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const U_Permitidos = [
            "aaaaaaaa",
            "cccccbbbbb",
            "c123bbb4b",
            "test testzx",
        ];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="nombre"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="nombre"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="nombre"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="nombre"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en campo apellido paterno de editar-usuario", () => {
        cy.get('button[title="Editar"]').first().click({ force: true });
        cy.wait(1000);

        cy.get('input[formControlName="apPaterno"]')
            .click({ force: true })
            .clear();

        cy.get('input[formControlName="apPaterno"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const U_Permitidos = [
            "aaaaaaaa",
            "cccccbbbbb",
            "c123bbb4b",
            "test testzx",
        ];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="apPaterno"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="apPaterno"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="apPaterno"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="apPaterno"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en campo apellido materno de editar-usuario", () => {
        cy.get('button[title="Editar"]').first().click({ force: true });

        cy.wait(1000);
        cy.get('input[formControlName="apMaterno"]')
            .click({ force: true }).clear();
        cy.get('input[formControlName="apMaterno"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const U_Permitidos = [
            "aaaaaaaa",
            "cccccbbbbb",
            "c123bbb4b",
            "test testzx",
        ];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="apMaterno"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="apMaterno"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="apMaterno"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="apMaterno"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en campo telefono de editar-usuario", () => {
        cy.get('button[title="Editar"]').first().click({ force: true });

        cy.wait(1000);

        const U_Permitidos = ["123456789"];

        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="telef"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="telef"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "asdsdfgsdf",
            "sdfdñdhgfgdf",
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="telef"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="telef"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos en campo correo de editar-usuario", () => {
        cy.get('button[title="Editar"]').first().click({ force: true });

        cy.wait(1000);
        // Validamos caso DNI  “.”, “-”, “_” y ”@”

        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .clear();

        const U_Permitidos = [
            "..zxssssddd---___@gmail.com",
            "___@gmail.com",
            "---@---@gmail.com",
        ];
        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="correo"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="correo"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="correo"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="correo"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos editar registro de usuario random ", () => {
        cy.get('button[title="Editar"]').first().click({ force: true });

        cy.wait(1000);
        const userRandom = generateRandomString(11).replace(".", "");

        cy.get('input[formControlName="nombre"]')
            .click({ force: true })
            .type(generateRandomString(11));

        cy.get('input[formControlName="apPaterno"]')
            .click({ force: true })
            .type(generateRandomString(11));

        cy.get('input[formControlName="apMaterno"]')
            .click({ force: true })
            .type(generateRandomString(11));

            cy.get('input[formControlName="correo"]')
            .click({ force: true }).clear();

        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .type("jburgos@novatronic.com");

        cy.get('button[type="submit"]').click({ force: true });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("satisfactoriamente")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal(
                    "Operación realizada satisfactoriamente."
                );
            });
    });

    it("Verificamos funcionamiento boton Excel", () => {
        cy.get("button").contains("Excel").click({ force: true });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("reporte")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Generando reporte, espere un momento");
            });
    });

    it("Verificamos funcionamiento boton PDF", () => {
        cy.get("button").contains("PDF").click({ force: true });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("reporte")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Generando reporte, espere un momento");
            });
    });


    //Generador de cadena random
    const generateRandomString = (num) => {
        const characters =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        let result1 = Math.random().toString(36).substring(0, num);

        return result1;
    };
});
