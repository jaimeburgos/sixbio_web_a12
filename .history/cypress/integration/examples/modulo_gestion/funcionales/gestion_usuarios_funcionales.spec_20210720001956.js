/// <reference types="cypress" />

context("Pruebas gestion-usuarios", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const url = Cypress.env("url");
    //    const userReset = Cypress.env("userReset");
    //    const passReset = Cypress.env("passReset");

    beforeEach(() => {


        cy.intercept('GET', url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador", []);
        cy.intercept('GET', url + "SIXBIO-webcore-Nova/sixsca/listarusuarios", []);

        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
        }).as("listarusuarios");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);
    });

    it("Verificamos entrada a gestion usuario", () => {
        cy.url().should("include", "/home/gestion-usuarios");
        // Verificamos los valores que deben setear en el localStorage
        cy.url().should(() => {
            expect(localStorage.getItem("isLoggedin")).to.eq("true");
            expect(localStorage.getItem("RolUsuario")).to.eq("EMP_ADM");
            expect(localStorage.getItem("usuarioLogueado")).to.eq(userAdmin);
        });
        cy.get("div .card-header")
            .invoke("text")
            .then((text) => {
                expect(text).to.contain("Gestión de Usuarios");
            });
    });

    it("Verificamos nombres de los campos de entrada crear-usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });
        cy.wait(1500);

        cy.get("label[for='inputUsuario']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Usuario (*)");
            });

        cy.get("label[for='inputNombre']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nombre (*)");
            });

        cy.get("label[for='inputApellidoPaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Paterno (*)");
            });

        cy.get("label[for='inputApellidoMaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Materno (*)");
            });

        cy.get("label[for='inputTelefono']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Teléfono");
            });

        cy.get("label[for='inputEstado']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Estado");
            });

        cy.get("label[for='inputNacimiento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha de Nacimiento");
            });

        cy.get("label[for='inputTipoDocumento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Tipo de documento (*)");
            });

        cy.get("label[for='inputNroDocumento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Número de documento(*)");
            });

        cy.get("label[for='inputCorreo']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Correo (*)");
            });

        cy.get("label[for='inputRolAgrupador']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Rol Agrupador (*)");
            });
    });

    it("Verificamos nombres de los botones", () => {
        cy.get("button")
            .find("a")
            .contains("Nuevo")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" Nuevo");
            });

        cy.get("button")
            .find("a")
            .contains("Eliminar")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" Eliminar");
            });

        cy.get("button")
            .find("a")
            .contains("Excel")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Excel ");
            });

        cy.get("button")
            .find("a")
            .contains("PDF")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("PDF ");
            });
    });
});
