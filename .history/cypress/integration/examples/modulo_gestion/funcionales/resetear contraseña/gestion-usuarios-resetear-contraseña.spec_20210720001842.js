/// <reference types="cypress" />

context("Pruebas Funcionales Resetear Contraseña", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const url = Cypress.env("url");
    //    const userReset = Cypress.env("userReset");
    //    const passReset = Cypress.env("passReset");

    beforeEach(() => {
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            []
        );

        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
        }).as("listarusuarios");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);
    });

    it("Resetear Contraseña - Cancelar", () => {
        cy.get('button[name="reset"]').first().click({ force: true });
        cy.wait(1000);
        cy.url().should("include", "/home/gestion-usuarios");
        cy.wait(1000);
        cy.get("p")
            .contains("restablecer")
            .invoke("text")
            .then((text) => {
                expect(text).to.contain(
                    "¿Desea restablecer la contraseña del usuario?"
                );
            });
        cy.wait(1000);
        cy.get("button").contains("Cerrar").click({ force: true });
        cy.url().should("include", "/home/gestion-usuarios");
    });

    it("Resetear Contraseña - OK (MOCKEADO)", () => {

        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/resetearusuario",
            {codigoRespuesta: "0000"}
        );
        cy.intercept(
            "POST",
            url + "SIXBIO-webcore-Nova/sixsca/resetearusuario",
            {codigoRespuesta: "0000"}
        ).as("resetear");

        cy.get('button[name="reset"]').first().click({ force: true });
        cy.wait(1000);
        cy.url().should("include", "/home/gestion-usuarios");
        cy.wait(1000);
        cy.get("p")
            .contains("restablecer")
            .invoke("text")
            .then((text) => {
                expect(text).to.contain(
                    "¿Desea restablecer la contraseña del usuario?"
                );
            });
        cy.wait(1000);
        cy.get("button").contains("Confirmar").click({ force: true });
        cy.wait(500);
        
        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("satisfactoriamente")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal(
                    "Operación realizada satisfactoriamente."
                );
            });
        cy.wait(1500);
        cy.url().should("include", "/home/gestion-usuarios");
    });
//Operación realizada satisfactoriamente

});
