/// <reference types="cypress" />

context("Pruebas gestion-usuarios", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const url = Cypress.env("url");

    beforeEach(() => {
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            []
        );

        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
        }).as("listarusuarios");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);
    });

    it("Verificamos mensaje Eliminar Usuario - cancelar", () => {
        cy.wait(500);
        cy.get('input[type="checkbox"]').first().click({ force: true });
        cy.wait(500);
        cy.get("button").find("a").contains("Eliminar").click({ force: true });
        cy.wait(1000);

        cy.get("p")
        .contains("¿")
        .invoke("text")
        .then((text) => {
            expect(text).to.contain(
                "¿Ud. esta seguro de realizar la eliminación?"
            );
        });
        cy.wait(500);
        cy.get("button").contains("Cerrar").click({ force: true });
        cy.url().should("include", "/home/gestion-usuarios");

    });

    it.only("Verificamos mensaje Eliminar Usuario - Error", () => {
        cy.wait(500);
        cy.get('input[type="checkbox"]').first().click({ force: true });
        cy.wait(500);
        cy.get("button").find("a").contains("Eliminar").click({ force: true });
        cy.wait(1000);

        cy.get("p")
        .contains("¿")
        .invoke("text")
        .then((text) => {
            expect(text).to.contain(
                "¿Ud. esta seguro de realizar la eliminación?"
            );
        });
        cy.wait(500);
        cy.intercept('POST', url + "SIXBIO-webcore-Nova/sixsca/eliminarusuario", [9999]);

        cy.get("button").contains("Confirmar").click({ force: true });
        cy.intercept('POST', url + "SIXBIO-webcore-Nova/sixsca/eliminarusuario", [9999]);

    
    });

  //  cy.intercept('POST', url + "SIXBIO-webcore-Nova/sixsca/eliminarusuario", [9999]);

    //SIXBIO-webcore-Nova/sixsca/eliminarusuario
});
