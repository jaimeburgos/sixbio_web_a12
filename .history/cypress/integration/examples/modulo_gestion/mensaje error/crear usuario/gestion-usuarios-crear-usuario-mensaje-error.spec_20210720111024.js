/// <reference types="cypress" />

context("Pruebas Mensaje Error Modulo Gestion", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const url = Cypress.env("url");

    beforeEach(() => {
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            []
        );

        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
        }).as("listarusuarios");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);
    });


    it.only("Crear Usuario - Usuario existente", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin);

        cy.get('input[formControlName="nombre"]')
            .click({ force: true })
            .type(generateRandomString(11));

        cy.get('input[formControlName="apPaterno"]')
            .click({ force: true })
            .type(generateRandomString(11));

        cy.get('input[formControlName="apMaterno"]')
            .click({ force: true })
            .type(generateRandomString(11));

        cy.get('select[formControlName="tipoDoc"]').select("DNI", {
            force: true,
        });

        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .type("jburgos@novatronic.com");

        cy.get('input[formControlName="numDoc"]')
            .click({ force: true })
            .type("12345678");

        cy.get('input[formControlName="fecNac"]').click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });

        cy.get('button[name="submit"]').click({ force: true });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("usuario")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("No se pudo registrar el usuario");
            });
    });

        //Generador de cadena random
        const generateRandomString = (num) => {
            const characters =
                "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            let result1 = Math.random().toString(36).substring(0, num);
    
            return result1;
        };
});
