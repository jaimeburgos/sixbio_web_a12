/// <reference types="cypress" />

context("Pruebas First-Login Interfaz", () => {
    const userReset = Cypress.env("userReset");
    const passReset = Cypress.env("passReset");
    const url = Cypress.env("url");

    beforeEach(() => {
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos campo Antigua Contraseña", () => {
        // Nos logeamos cson el usuario userReset para entrar a la pantalla cambio de contraseña
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");

        // En el campo contraseña antigua interamos nuestros array de caracteres permitidos y no permitidos para verificar la validez
        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const P_Permitidos = [
            ":::::::::",
            "............",
            "--------------",
            "______________",
            "((((((((((",
            "))))))))))",
            "@@@@@@@@@",
            "/////////",
        ];

        for (let i = 0; i <= P_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="old"]')
                .click({ force: true })
                .clear();
            // Si el caracter es permitido se escribira y se verificara
            cy.get('input[formControlName="old"]')
                .click({ force: true })
                .type(P_Permitidos[i])
                .should("have.value", P_Permitidos[i]);
        }

        const P_No_Permitidos = [
            "☺☻♥♠○◘♣•",
            "[[[[]]]]",
            "{{{{",
            "          ",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            ",,,,,,,,,,,",
            ";;;;;;;;;;;;",
            "ññññññññññ",
            "ÑÑÑÑÑÑÑ",
            "============",
            "<<<<<>>>>>",
            "**********",
            "++++++++++",
            "$$$$$$$$",
            '""""""""""""',
            "^^^^^^^^^^^^",
        ];

        for (let i = 0; i <= P_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="old"]')
                .click({ force: true })
                .clear();
            // Si el caracter no es permitido no se escribira por lo tanto se verificar que este vacio
            cy.get('input[formControlName="old"]')
                .click({ force: true })
                .type(P_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos campo Nueva Contraseña", () => {
        // Nos logeamos cson el usuario userReset para entrar a la pantalla cambio de contraseña
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");
        // En el campo nueva contraseña interamos nuestros array de caracteres permitidos y no permitidos para verificar la validez

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const P_Permitidos = [
            ":::::::::",
            "............",
            "--------------",
            "______________",
            "((((((((((",
            "))))))))))",
            "@@@@@@@@@",
            "/////////",
        ];

        for (let i = 0; i <= P_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="new"]')
                .click({ force: true })
                .clear();
            // Si el caracter es permitido se escribira y se verificara

            cy.get('input[formControlName="new"]')
                .click({ force: true })
                .type(P_Permitidos[i])
                .should("have.value", P_Permitidos[i]);
        }

        const P_No_Permitidos = [
            "☺☻♥♠○◘♣•",
            "[[[[]]]]",
            "{{{{",
            "          ",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            ",,,,,,,,,,,",
            ";;;;;;;;;;;;",
            "ññññññññññ",
            "ÑÑÑÑÑÑÑ",
            "============",
            "<<<<<>>>>>",
            "**********",
            "++++++++++",
            "$$$$$$$$",
            '""""""""""""',
            "^^^^^^^^^^^^",
        ];

        for (let i = 0; i <= P_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="new"]')
                .click({ force: true })
                .clear();
            // Si el caracter no es permitido no se escribira por lo tanto se verificar que este vacio

            cy.get('input[formControlName="new"]')
                .click({ force: true })
                .type(P_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos campo Confirmar Contraseña", () => {
        // Nos logeamos cson el usuario userReset para entrar a la pantalla cambio de contraseña

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");
        // En el campo nueva contraseña interamos nuestros array de caracteres permitidos y no permitidos para verificar la validez

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const P_Permitidos = [
            ":::::::::",
            "............",
            "--------------",
            "______________",
            "((((((((((",
            "))))))))))",
            "@@@@@@@@@",
            "/////////",
        ];

        for (let i = 0; i <= P_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="confirm"]')
                .click({ force: true })
                .clear();
            // Si el caracter es permitido se escribira y se verificara

            cy.get('input[formControlName="confirm"]')
                .click({ force: true })
                .type(P_Permitidos[i])
                .should("have.value", P_Permitidos[i]);
        }

        const P_No_Permitidos = [
            "☺☻♥♠○◘♣•",
            "[[[[]]]]",
            "{{{{",
            "          ",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            ",,,,,,,,,,,",
            ";;;;;;;;;;;;",
            "ññññññññññ",
            "ÑÑÑÑÑÑÑ",
            "============",
            "<<<<<>>>>>",
            "**********",
            "++++++++++",
            "$$$$$$$$",
            '""""""""""""',
            "^^^^^^^^^^^^",
        ];

        for (let i = 0; i <= P_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="confirm"]')
                .click({ force: true })
                .clear();
            // Si el caracter no es permitido no se escribira por lo tanto se verificar que este vacio

            cy.get('input[formControlName="confirm"]')
                .click({ force: true })
                .type(P_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos longitud maxima de campo Antigua Contraseña", () => {
        // Nos logeamos cson el usuario userReset para entrar a la pantalla cambio de contraseña

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");
        // Escribimos la cantidad maxima de caracteres permitidos + 1 segun el AyD y verificamos que se escriba todos menos uno
        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("12345678901234567")
            .should("have.value", "1234567890123456");
    });

    it("Verificamos longitud maxima de campo Nueva Contraseña", () => {
        // Nos logeamos cson el usuario userReset para entrar a la pantalla cambio de contraseña

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");
        // Escribimos la cantidad maxima de caracteres permitidos + 1 segun el AyD y verificamos que se escriba todos menos uno

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("12345678901234567")
            .should("have.value", "1234567890123456");
    });

    it("Verificamos longitud maxima de campo Confirmar Contraseña", () => {
        // Nos logeamos cson el usuario userReset para entrar a la pantalla cambio de contraseña

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");
        // Escribimos la cantidad maxima de caracteres permitidos + 1 segun el AyD y verificamos que se escriba todos menos uno

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("12345678901234567")
            .should("have.value", "1234567890123456");
    });

    it("Verificamos mensajes de validacion campo Antigua Contraseña", () => {
        // Nos logeamos cson el usuario userReset para entrar a la pantalla cambio de contraseña

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");

        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get('input[formControlName="new"]').click({ force: true }).clear();
        // Verificamos los mensajes de validacion para el campo antigua contraseña , se lee las letras en rojo debajo del input
        cy.get("#msgFirstMinOld")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 6 caracteres");
            });

        cy.get('input[formControlName="old"]').click({ force: true }).clear();

        cy.get("#msgFirstReqOld")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese contraseña antigua");
            });
    });

    it("Verificamos mensajes de validacion campo Nueva Contraseña", () => {
        // Nos logeamos cson el usuario userReset para entrar a la pantalla cambio de contraseña

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");
        // Verificamos los mensajes de validacion para el campo nueva contraseña , se lee las letras en rojo debajo del input

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get('input[formControlName="old"]').click({ force: true }).clear();

        cy.get("#msgFirstMinNew")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 6 caracteres");
            });

        cy.get('input[formControlName="new"]').click({ force: true }).clear();

        cy.get("#msgFirstReqNew")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese contraseña nueva");
            });
    });

    it("Verificamos mensajes de validacion campo Nueva Contraseña", () => {
        // Nos logeamos cson el usuario userReset para entrar a la pantalla cambio de contraseña

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");
        // Verificamos los mensajes de validacion para el campo confirmar contraseña , se lee las letras en rojo debajo del input

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get('input[formControlName="old"]').click({ force: true }).clear();

        cy.get("#msgFirstMinConfirm")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 6 caracteres");
            });

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .clear();

        cy.get("#msgFirstReqConfirm")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese contraseña de confirmacion");
            });
    });
});
