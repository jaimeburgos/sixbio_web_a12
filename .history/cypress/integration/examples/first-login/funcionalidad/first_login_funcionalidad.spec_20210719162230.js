/// <reference types="cypress" />

context("Pruebas First-Login Funcionalidad", () => {
    const userReset = Cypress.env("userReset");
    const passReset = Cypress.env("passReset");
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const url = Cypress.env("url");

    beforeEach(() => {
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();
    });

    it("Cambio de Contraseña - Cancelar operación cambio de contraseña primer Login", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");

        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("sdgfdgdfgfdgf")
            .should("have.value", "sdgfdgdfgfdgf");

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");

        cy.get('button[name="submit"]').click({ force: true });

        cy.get("#msgFirstLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "¿Está seguro de confirmar el cambio de contraseña?"
                );
            });
        cy.wait(500);
        cy.get("#cerrar").click({ force: true });

        cy.get('button[name="cancel"]').click({ force: true });

        cy.url().should("include", "/login");
    });

    it("Cambio de Contraseña - Cancelar confirmación de cambio de contraseña", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");

        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("sdgfdgdfgfdgf")
            .should("have.value", "sdgfdgdfgfdgf");

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");

        cy.get('button[name="submit"]').click({ force: true });

        cy.get("#msgFirstLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "¿Está seguro de confirmar el cambio de contraseña?"
                );
            });
        cy.wait(500);
        cy.get("#cerrar").click({ force: true });

        cy.get('input[formControlName="confirm"]').should(
            "have.value",
            "Test123FD"
        );
        cy.get('input[formControlName="new"]').should(
            "have.value",
            "Test123FD"
        );
        cy.get('input[formControlName="old"]').should(
            "have.value",
            "sdgfdgdfgfdgf"
        );
    });

  
});
