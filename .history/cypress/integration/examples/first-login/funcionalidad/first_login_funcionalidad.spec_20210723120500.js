/// <reference types="cypress" />

context("Pruebas First-Login Funcionalidad", () => {
    // Asignamos los valores de nuestras variables globales del arribo cypress.json
    const userReset = Cypress.env("userReset");
    const passReset = Cypress.env("passReset");
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const url = Cypress.env("url");

    beforeEach(() => {
        // Visitamos la URL base
        cy.visit(url);
        // Seleccionamos el input usuario realizamos y realizamos click , con clear() lo limpiamos
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        // Seleccionamos el inputo password y realizamos click , con clear() lo limpiamos
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();
    });

    it("Cambio de Contraseña - Cancelar operación cambio de contraseña primer Login", () => {
        // En el usuario escribimos el userReset (variable global)
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);
        // En el passowrd escribimos passReset ( variable global)
        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);
        // Interceptamos el servicio sigon 
        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");
        // realizamos click en entrar
        cy.get('button[name="submit"]').click({ force: true });
        // esperamos que termine de carga el servicio signon previamente interceptado , con el wait esperamos 
        cy.wait("@signon");
        // Realizamos click en el boton confirmar del modal de cambio de contraseña
        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });
        // Interceptamos el servicio consultardatoscambioclave
        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
                // esperamos que termine de carga el servicio consultardatoscambioclave previamente interceptado , con el wait esperamos 

        cy.wait("@consultardatoscambioclave");

        // Escribimos datos en el input Antigua Contraseña
        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("sdgfdgdfgfdgf")
            .should("have.value", "sdgfdgdfgfdgf");
        // Escribimos datos en el input Nueva Contraseña

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");
        // Escribimos datos en el input Confirmar Contraseña

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");
        // Realizamos click en cambiar contraseña
        cy.get('button[name="submit"]').click({ force: true });

        // Verificamos el mensaje que aparece en el modal
        cy.get("#msgFirstLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "¿Está seguro de confirmar el cambio de contraseña?"
                );
            });
        cy.wait(500);
        //Cerramos el modal haciendo click en el boton cerrar
        cy.get("#cerrar").click({ force: true });
        // Cancelamos la operacion para volver a la pantalla login
        cy.get('button[name="cancel"]').click({ force: true });
            // Verificamos la URL si estamos en la pantalla login
        cy.url().should("include", "/login");
    });

    it("Cambio de Contraseña - Cancelar confirmación de cambio de contraseña", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");

        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("sdgfdgdfgfdgf")
            .should("have.value", "sdgfdgdfgfdgf");

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");

        cy.get('button[name="submit"]').click({ force: true });

        cy.get("#msgFirstLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "¿Está seguro de confirmar el cambio de contraseña?"
                );
            });
        cy.wait(500);
        cy.get("#cerrar").click({ force: true });

        cy.get('input[formControlName="confirm"]').should(
            "have.value",
            "Test123FD"
        );
        cy.get('input[formControlName="new"]').should(
            "have.value",
            "Test123FD"
        );
        cy.get('input[formControlName="old"]').should(
            "have.value",
            "sdgfdgdfgfdgf"
        );
    });

    it("Cambio de Contraseña - Cancelar cambio de contraseña por demanda", () => {
        // Escribimos las credenciales de nuestro usuario administrador y nos logeamos
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);
        // Realizamos click en entrar
        cy.get('button[name="submit"]').click({ force: true });
        // Interceptamos el servicio listarrolesagrupador para esperar que cargue la lista de usuario de la primera ventana gestion de usuarios
        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");
        cy.wait("@listarrolesagrupador");

        // Verificamos que nos encontremos en gestion de usuario
        cy.url().should("include", "/home/gestion-usuarios");
        // Nos dirigimos al menu desplegable con el nombre del usuario logeado
        cy.get("a").contains(userAdmin).click({ force: true });
        cy.wait(1000);
        // selecionamos la oopcion cambio de contraseña y hacemos click
        cy.get("a").contains("Contraseña").click({ force: true });

        cy.wait(2000);
        // entramos en la ventana  cambio de contraseña
        cy.url().should("include", "/firstLogin");
        // cancelamos la operacion
        cy.get('button[name="cancel"]').click({ force: true });
        // verificamos que retornemos a la pantalla gestion de usuarios
        cy.url().should("include", "/home/gestion-usuarios");
    });
});
