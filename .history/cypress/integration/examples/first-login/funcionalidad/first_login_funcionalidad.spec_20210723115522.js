/// <reference types="cypress" />

context("Pruebas First-Login Funcionalidad", () => {
    // Asignamos los valores de nuestras variables globales del arribo cypress.json
    const userReset = Cypress.env("userReset");
    const passReset = Cypress.env("passReset");
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const url = Cypress.env("url");

    beforeEach(() => {
        // Visitamos la URL base
        cy.visit(url);
        // Seleccionamos el input usuario realizamos y realizamos click , con clear() lo limpiamos
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        // Seleccionamos el inputo password y realizamos click , con clear() lo limpiamos
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();
    });

    it("Cambio de Contraseña - Cancelar operación cambio de contraseña primer Login", () => {
        // En el usuario escribimos el userReset (variable global)
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);
        // En el passowrd escribimos passReset ( variable global)
        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");

        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("sdgfdgdfgfdgf")
            .should("have.value", "sdgfdgdfgfdgf");

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");

        cy.get('button[name="submit"]').click({ force: true });

        cy.get("#msgFirstLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "¿Está seguro de confirmar el cambio de contraseña?"
                );
            });
        cy.wait(500);
        cy.get("#cerrar").click({ force: true });

        cy.get('button[name="cancel"]').click({ force: true });

        cy.url().should("include", "/login");
    });

    it("Cambio de Contraseña - Cancelar confirmación de cambio de contraseña", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");

        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("sdgfdgdfgfdgf")
            .should("have.value", "sdgfdgdfgfdgf");

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");

        cy.get('button[name="submit"]').click({ force: true });

        cy.get("#msgFirstLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "¿Está seguro de confirmar el cambio de contraseña?"
                );
            });
        cy.wait(500);
        cy.get("#cerrar").click({ force: true });

        cy.get('input[formControlName="confirm"]').should(
            "have.value",
            "Test123FD"
        );
        cy.get('input[formControlName="new"]').should(
            "have.value",
            "Test123FD"
        );
        cy.get('input[formControlName="old"]').should(
            "have.value",
            "sdgfdgdfgfdgf"
        );
    });

    it("Cambio de Contraseña - Cancelar cambio de contraseña por demanda", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");
        cy.wait("@listarrolesagrupador");
        cy.url().should("include", "/home/gestion-usuarios");

        cy.get("a").contains(userAdmin).click({ force: true });
        cy.wait(1000);
        cy.get("a").contains("Contraseña").click({ force: true });

        cy.wait(2000);

        cy.url().should("include", "/firstLogin");

        cy.get('button[name="cancel"]').click({ force: true });

        cy.url().should("include", "/home/gestion-usuarios");
    });
});
