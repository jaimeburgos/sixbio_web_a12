/// <reference types="cypress" />

context("Pruebas First-Login", () => {
    beforeEach(() => {
        cy.visit("http://localhost:4200");
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();
    });

    // https://on.cypress.io/interacting-with-elements

    it("Verificamos entrada a first-login", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type("UserResetTest")
            .should("have.value", "UserResetTest");

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type("ol48ooja")
            .should("have.value", "ol48ooja");

        cy.get('button[name="submit"]').click({ force: true });
        cy.wait(1000);

        cy.get("#msgModalLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "Primer Login, se le sugiere realizar el cambio de contraseña."
                );
            });

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.url().should("include", "/firstLogin", () => {
            expect(localStorage.getItem("isFirstLogin")).to.eq("true");
        });

        cy.get('input[formControlName="old"]')
        .click({ force: true })

    

        cy.get('input[formControlName="new"]')
        .click({ force: true })
  

        cy.get('input[formControlName="confirm"]')
        .click({ force: true })


        cy.get("#msgFirstReqOld")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "Ingrese contraseña antigua"
                );
            });

            cy.get("#msgFirstReqNew")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "Ingrese contraseña nueva"
                );
            });

            cy.get("#msgFirstReqConfirm")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "Ingrese contraseña de confirmacion"
                );
            });


    });

    /*

  it('Verificamos mensajes reseteo de contraseña', async () => {
    await page.navigateTo('');
    await browser.sleep(1000);
    await page.getFormControl('usuario').sendKeys( browser.params.USER_RESETEO);
    await page.getFormControl('pass').sendKeys( browser.params.PASS_RESETEO);
    await browser.executeScript('arguments[0].click();', element(by.css('button[name="submit"]')));
    await browser.sleep(1000);

    await browser.sleep(1000);
    await expect(await browser.executeScript('return document.querySelector("#msgModalLogin").innerHTML')).toEqual('Primer Login, se le sugiere realizar el cambio de contraseña.');
    await browser.sleep(500);
    await browser.executeScript('arguments[0].click();', element(by.buttonText('Confirmar')));
    await browser.sleep(500);
    await expect( await browser.getCurrentUrl()).toContain('firstLogin');

    await page.getFormControl('old').sendKeys( '');
    await page.getFormControl('new').sendKeys( '');
    await page.getFormControl('confirm').sendKeys( '');
    await page.getFormControl('old').sendKeys( '');
    await browser.sleep(1000);

    await  expect(await page.getElementText('#msgFirstReqOld')).toEqual('Ingrese contraseña antigua');
    await  expect(await page.getElementText('#msgFirstReqNew')).toEqual('Ingrese contraseña nueva');
    await  expect(await page.getElementText('#msgFirstReqConfirm')).toEqual('Ingrese contraseña de confirmacion');
    await expect(await page.getElementText('h1')).toEqual('Cambiar Contraseña');
    await  expect(await element(by.css('button[name="submit"]')).isEnabled()).toBeFalsy();


    await page.getFormControl('old').sendKeys( '123');
    await page.getFormControl('new').sendKeys( '123');
    await page.getFormControl('confirm').sendKeys( '123');
    await page.getFormControl('old').sendKeys( '1');
    await browser.sleep(1000);

    await  expect(await page.getElementText('#msgFirstMinOld')).toEqual('Tamaño mínimo de 6 caracteres');
    await  expect(await page.getElementText('#msgFirstMinNew')).toEqual('Tamaño mínimo de 6 caracteres');
    await  expect(await page.getElementText('#msgFirstMinConfirm')).toEqual('Tamaño mínimo de 6 caracteres');
    await  expect(await element(by.css('button[name="submit"]')).isEnabled()).toBeFalsy();
  });







*/
});
  