/// <reference types="cypress" />

context("Pruebas First-Login", () => {
    const userReset = Cypress.env("userReset");
    const passReset = Cypress.env("passReset");

    beforeEach(() => {
        cy.visit("http://localhost:4200");
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();
    });

    it("Verificamos entrada a first-login", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.get('button[name="submit"]').click({ force: true });
        cy.wait(1000);

        cy.get("#msgModalLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "Primer Login, se le sugiere realizar el cambio de contraseña."
                );
            });

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.url().should("include", "/firstLogin", () => {
            expect(localStorage.getItem("isFirstLogin")).to.eq("true");
        });

        cy.get('input[formControlName="old"]').click({ force: true });

        cy.get('input[formControlName="new"]').click({ force: true });

        cy.get('input[formControlName="confirm"]').click({ force: true });

        cy.get('input[formControlName="old"]').click({ force: true });

        cy.get("#msgFirstReqOld")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese contraseña antigua");
            });

        cy.get("#msgFirstReqNew")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese contraseña nueva");
            });

        cy.get("#msgFirstReqConfirm")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese contraseña de confirmacion");
            });
        cy.get('button[name="submit"]').should("be.disabled");

        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get("#msgFirstMinOld")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 6 caracteres");
            });

        cy.get("#msgFirstMinNew")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 6 caracteres");
            });

        cy.get("#msgFirstMinConfirm")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 6 caracteres");
            });

        cy.get('button[name="submit"]').should("be.disabled");
    });

    it("Verificamos navegacion de FirstLogin a Login", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.get('button[name="submit"]').click({ force: true });
        cy.wait(1000);

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });
        cy.wait(500);

        cy.get('button[name="cancel"]').click({ force: true });

        cy.url().should("include", "/login");
    });

    it("Verificamos modal de confirmacion cambio contraseña", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.get('button[name="submit"]').click({ force: true });
        cy.wait(1000);

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("sdgfdgdfgfdgf")
            .should("have.value", "sdgfdgdfgfdgf");

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");

        cy.get('button[name="submit"]').click({ force: true });

        cy.get("#msgFirstLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "¿Está seguro de confirmar el cambio de contraseña?"
                );
            });
    });

    it.only("Verificamos Mensaje La contraseña de confirmacion no es igual a la nueva", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.get('button[name="submit"]').click({ force: true });
        cy.wait(1000);

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("sdgfdgdfgfdgf")
            .should("have.value", "sdgfdgdfgfdgf");

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("dfgdfgdfgfd")
            .should("have.value", "dfgdfgdfgfd");

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("dfgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfd");

        cy.get('button[name="submit"]').click({ force: true });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("contraseña")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "La contraseña de confirmacion no es igual a la nueva"
                );
            });
    });

    /*
 it('Verificamos Mensaje La contraseña de confirmacion no es igual a la nueva', async () => {
    await page.navigateTo('');
    await browser.sleep(1000);
    await page.getFormControl('usuario').sendKeys( browser.params.USER_RESETEO);
    await page.getFormControl('pass').sendKeys( browser.params.PASS_RESETEO);
    await browser.executeScript('arguments[0].click();', element(by.css('button[name="submit"]')));
    await browser.sleep(1000);

    await expect(await browser.executeScript('return document.querySelector("#msgModalLogin").innerHTML')).toEqual('Primer Login, se le sugiere realizar el cambio de contraseña.');
    await browser.sleep(500);
    await browser.executeScript('arguments[0].click();', element(by.buttonText('Confirmar')));
    await browser.sleep(500);
    await expect(await browser.getCurrentUrl()).toContain('firstLogin');
    await browser.sleep(500);

    await page.getFormControl('old').sendKeys('sdgfdgdfgfdgf');
    await page.getFormControl('new').sendKeys('dfgdfgdfgfd');
    await page.getFormControl('confirm').sendKeys('dfgfdgfdgfd');
    await browser.sleep(1000);
    await expect(await element(by.css('button[name="submit"]')).isEnabled()).toBeTruthy();
    await browser.executeScript('arguments[0].click();', element(by.css('button[name="submit"]')));
    await browser.sleep(1200);
  //  await expect().toEqual('La contraseña de confirmacion no es igual a la nueva');
  await  console.log('El mensaje Toast es ' +
  await browser.executeScript('return document.querySelector("div[role=alertdialog]").innerHTML'));

  });


  */
});
