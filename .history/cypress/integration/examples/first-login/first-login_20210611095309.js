/// <reference types="cypress" />

context('Pruebas First-Login', () => {
  
  beforeEach(() => {
      cy.visit("http://localhost:4200");
      cy.get('input[formControlName="usuario"]').click({ force: true }).clear();
      cy.get('input[formControlName="pass"]').click({ force: true }).clear();
  });

  // https://on.cypress.io/interacting-with-elements

  it("Verificamos Login Satisfactorio", () => {
      // https://on.cypress.io/type
      //  cy.wait(1000);
      cy.get('input[formControlName="usuario"]')
          .click({ force: true })
          .type("jburgos6")
          .should("have.value", "jburgos6");
      //  cy.wait(1000);
      cy.get('[formControlName="pass"]')
          .click({ force: true })
          .type("Admin3")
          .should("have.value", "Admin3");
      //   cy.wait(1000);
      cy.get('button[name="submit"]').click({ force: true });

      cy.url().should("include", "/home/gestion-usuarios", () => {
          expect(localStorage.getItem("isLoggedin")).to.eq("true");
      });
  });
  







});
  