/// <reference types="cypress" />

context("Pruebas First-Login", () => {
    const userReset = Cypress.env("userReset");
    const passReset = Cypress.env("passReset");
    const url = Cypress.env("url");

    beforeEach(() => {
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();
    });

    it("Verificamos entrada a first-login", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("#msgModalLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "Primer Login, se le sugiere realizar el cambio de contraseña."
                );
            });

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.url().should("include", "/firstLogin");

        cy.url().should(() => {
            expect(localStorage.getItem("isFirstLogin")).to.eq("true");
        });

        cy.get('input[formControlName="old"]').click({ force: true });

        cy.get('input[formControlName="new"]').click({ force: true });

        cy.get('input[formControlName="confirm"]').click({ force: true });

        cy.get('input[formControlName="old"]').click({ force: true });

        cy.get("#msgFirstReqOld")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese contraseña antigua");
            });

        cy.get("#msgFirstReqNew")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese contraseña nueva");
            });

        cy.get("#msgFirstReqConfirm")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese contraseña de confirmacion");
            });
        cy.get('button[name="submit"]').should("be.disabled");

        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get("#msgFirstMinOld")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 6 caracteres");
            });

        cy.get("#msgFirstMinNew")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 6 caracteres");
            });

        cy.get("#msgFirstMinConfirm")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 6 caracteres");
            });

        cy.get('button[name="submit"]').should("be.disabled");
    });

    it("Verificamos navegacion de FirstLogin a Login", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });
        cy.wait(500);

        cy.get('button[name="cancel"]').click({ force: true });

        cy.url().should("include", "/login");
    });

    it("Verificamos modal de confirmacion cambio contraseña", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");

        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("sdgfdgdfgfdgf")
            .should("have.value", "sdgfdgdfgfdgf");

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");

        cy.get('button[name="submit"]').click({ force: true });

        cy.get("#msgFirstLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "¿Está seguro de confirmar el cambio de contraseña?"
                );
            });
    });

    it("Verificamos Mensaje La contraseña de confirmacion no es igual a la nueva", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");

        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("sdgfdgdfgfdgf")
            .should("have.value", "sdgfdgdfgfdgf");

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("dfgdfgdfgfd")
            .should("have.value", "dfgdfgdfgfd");

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("dfgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfd");

        cy.get('button[name="submit"]').click({ force: true });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("contraseña")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "La contraseña de confirmacion no es igual a la nueva"
                );
            });
    });

    it("Verificamos mensaje de error de conectividad al cambiar contraseña ( Response Vacio )", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");

        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("sdgfdgdfgfdgf")
            .should("have.value", "sdgfdgdfgfdgf");

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");

        cy.get('button[name="submit"]').click({ force: true });

        cy.get("#msgFirstLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "¿Está seguro de confirmar el cambio de contraseña?"
                );
            });
        cy.intercept(
            "POST",
            url + "SIXBIO-webcore-Nova/sixsca/cambiarclave",
            []
        ).as("errorConectividad");

        cy.get("#btnConfirmFirstLogin").click({ force: true });
        cy.wait("@errorConectividad");
        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("conectividad")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Error de conectividad.");
            });
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos campo Antigua Contraseña", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");

        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const P_Permitidos = [
            ":::::::::",
            "............",
            "--------------",
            "______________",
            "((((((((((",
            "))))))))))",
            "@@@@@@@@@",
            "/////////",
        ];

        for (let i = 0; i <= P_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="old"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="old"]')
                .click({ force: true })
                .type(P_Permitidos[i])
                .should("have.value", P_Permitidos[i]);
        }

        const P_No_Permitidos = [
            "☺☻♥♠○◘♣•",
            "[[[[]]]]",
            "{{{{",
            "          ",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            ",,,,,,,,,,,",
            ";;;;;;;;;;;;",
            "ññññññññññ",
            "ÑÑÑÑÑÑÑ",
            "============",
            "<<<<<>>>>>",
            "**********",
            "++++++++++",
            "$$$$$$$$",
            '""""""""""""',
            "^^^^^^^^^^^^",
        ];

        for (let i = 0; i <= P_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="old"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="old"]')
                .click({ force: true })
                .type(P_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos campo Nueva Contraseña", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const P_Permitidos = [
            ":::::::::",
            "............",
            "--------------",
            "______________",
            "((((((((((",
            "))))))))))",
            "@@@@@@@@@",
            "/////////",
        ];

        for (let i = 0; i <= P_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="new"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="new"]')
                .click({ force: true })
                .type(P_Permitidos[i])
                .should("have.value", P_Permitidos[i]);
        }

        const P_No_Permitidos = [
            "☺☻♥♠○◘♣•",
            "[[[[]]]]",
            "{{{{",
            "          ",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            ",,,,,,,,,,,",
            ";;;;;;;;;;;;",
            "ññññññññññ",
            "ÑÑÑÑÑÑÑ",
            "============",
            "<<<<<>>>>>",
            "**********",
            "++++++++++",
            "$$$$$$$$",
            '""""""""""""',
            "^^^^^^^^^^^^",
        ];

        for (let i = 0; i <= P_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="new"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="new"]')
                .click({ force: true })
                .type(P_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos las validaciones de entrada de caracteres permitidos y no permitidos campo Confirmar Contraseña", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        const P_Permitidos = [
            ":::::::::",
            "............",
            "--------------",
            "______________",
            "((((((((((",
            "))))))))))",
            "@@@@@@@@@",
            "/////////",
        ];

        for (let i = 0; i <= P_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="confirm"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="confirm"]')
                .click({ force: true })
                .type(P_Permitidos[i])
                .should("have.value", P_Permitidos[i]);
        }

        const P_No_Permitidos = [
            "☺☻♥♠○◘♣•",
            "[[[[]]]]",
            "{{{{",
            "          ",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            ",,,,,,,,,,,",
            ";;;;;;;;;;;;",
            "ññññññññññ",
            "ÑÑÑÑÑÑÑ",
            "============",
            "<<<<<>>>>>",
            "**********",
            "++++++++++",
            "$$$$$$$$",
            '""""""""""""',
            "^^^^^^^^^^^^",
        ];

        for (let i = 0; i <= P_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="confirm"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="confirm"]')
                .click({ force: true })
                .type(P_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Verificamos mensajes de validacion campo Antigua Contraseña", () => {
     
     
     
        cy.get('input[formControlName="usuario"]')
        .click({ force: true })
        .type(userReset)
        .should("have.value", userReset);

    cy.get('input[formControlName="pass"]')
        .click({ force: true })
        .type(passReset)
        .should("have.value", passReset);

    cy.intercept({
        method: "POST",
        url: url + "SIXBIO-webcore-Nova/service/signon",
    }).as("signon");

    cy.get('button[name="submit"]').click({ force: true });

    cy.wait("@signon");

    cy.get("div")
        .find("button")
        .contains("Confirmar")
        .click({ force: true });

    cy.intercept({
        method: "POST",
        url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
    }).as("consultardatoscambioclave");
    cy.wait("@consultardatoscambioclave");

        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get('input[formControlName="old"]').click({ force: true }).clear();

        cy.get("#msgMinPass")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 6 caracteres");
            });

        cy.get('input[formControlName="old"]').click({ force: true }).clear();

        cy.get("#msgReqPass")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese contraseña antigua");
            });
    });

    it("Verificamos mensajes de validacion campo Nueva Contraseña", () => {
        cy.get('input[formControlName="usuario"]')
        .click({ force: true })
        .type(userReset)
        .should("have.value", userReset);

    cy.get('input[formControlName="pass"]')
        .click({ force: true })
        .type(passReset)
        .should("have.value", passReset);

    cy.intercept({
        method: "POST",
        url: url + "SIXBIO-webcore-Nova/service/signon",
    }).as("signon");

    cy.get('button[name="submit"]').click({ force: true });

    cy.wait("@signon");

    cy.get("div")
        .find("button")
        .contains("Confirmar")
        .click({ force: true });

    cy.intercept({
        method: "POST",
        url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
    }).as("consultardatoscambioclave");
    cy.wait("@consultardatoscambioclave");





        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get('input[formControlName="new"]').click({ force: true }).clear();

        cy.get("#msgMinPass")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 6 caracteres");
            });

        cy.get('input[formControlName="new"]').click({ force: true }).clear();

        cy.get("#msgReqPass")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese contraseña nueva");
            });
    });

    it("Verificamos mensajes de validacion campo Nueva Contraseña", () => {
        cy.get('input[formControlName="usuario"]')
        .click({ force: true })
        .type(userReset)
        .should("have.value", userReset);

    cy.get('input[formControlName="pass"]')
        .click({ force: true })
        .type(passReset)
        .should("have.value", passReset);

    cy.intercept({
        method: "POST",
        url: url + "SIXBIO-webcore-Nova/service/signon",
    }).as("signon");

    cy.get('button[name="submit"]').click({ force: true });

    cy.wait("@signon");

    cy.get("div")
        .find("button")
        .contains("Confirmar")
        .click({ force: true });

    cy.intercept({
        method: "POST",
        url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
    }).as("consultardatoscambioclave");
    cy.wait("@consultardatoscambioclave");






        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .clear();

        cy.get("#msgMinPass")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 6 caracteres");
            });

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .clear();

        cy.get("#msgReqPass")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese contraseña de confirmacion");
            });
    });
});
