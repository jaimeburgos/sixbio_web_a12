/// <reference types="cypress" />

context("Pruebas First-Login", () => {
    beforeEach(() => {
        cy.visit("http://localhost:4200");
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();
    });

    // https://on.cypress.io/interacting-with-elements

    it("Verificamos entrada a first-login", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type("UserResetTest")
            .should("have.value", "UserResetTest");

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type("ol48ooja")
            .should("have.value", "ol48ooja");

        cy.get('button[name="submit"]').click({ force: true });
        cy.wait(1000);

        cy.get("#msgModalLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "Primer Login, se le sugiere realizar el cambio de contraseña."
                );
            });

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.url().should("include", "/firstLogin", () => {
            expect(localStorage.getItem("isFirstLogin")).to.eq("true");
        });

        cy.get('input[formControlName="old"]').click({ force: true });

        cy.get('input[formControlName="new"]').click({ force: true });

        cy.get('input[formControlName="confirm"]').click({ force: true });

        cy.get('input[formControlName="old"]').click({ force: true });

        cy.get("#msgFirstReqOld")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese contraseña antigua");
            });

        cy.get("#msgFirstReqNew")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese contraseña nueva");
            });

        cy.get("#msgFirstReqConfirm")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese contraseña de confirmacion");
            });
        cy.get('button[name="submit"]').should("be.disabled");

        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get("#msgFirstMinOld")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 6 caracteres");
            });

        cy.get("#msgFirstMinNew")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 6 caracteres");
            });

        cy.get("#msgFirstMinConfirm")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 6 caracteres");
            });

        cy.get('button[name="submit"]').should("be.disabled");
    });

    it("Verificamos navegacion de FirstLogin a Login", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type("UserResetTest")
            .should("have.value", "UserResetTest");

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type("ol48ooja")
            .should("have.value", "ol48ooja");

        cy.get('button[name="submit"]').click({ force: true });
        cy.wait(1000);

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });
        cy.wait(500);

        cy.get('button[name="cancel"]').click({ force: true });

        cy.url().should("include", "/login");
    });

    it.only("Verificamos modal de confirmacion cambio contraseña", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type("UserResetTest")
            .should("have.value", "UserResetTest");

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type("ol48ooja")
            .should("have.value", "ol48ooja");

        cy.get('button[name="submit"]').click({ force: true });
        cy.wait(1000);

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("sdgfdgdfgfdgf")
            .should("have.value", "sdgfdgdfgfdgf");

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");

            cy.get('button[name="submit"]').click({ force: true });



        cy.get("#msgFirstLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "¿Está seguro de confirmar el cambio de contraseña?"
                );
            });
      
      
            cy.get("#btnConfirmFirstLogin").click({ force: true });

            


    });

    /*
    
fit('Verificamos modal de confirmacion cambio contraseña', async () => {
    await page.navigateTo('');
    await browser.sleep(1000);
    await page.getFormControl('usuario').sendKeys( browser.params.USER_RESETEO);
    await page.getFormControl('pass').sendKeys( browser.params.PASS_RESETEO);
    await browser.executeScript('arguments[0].click();', element(by.css('button[name="submit"]')));
    await browser.sleep(1000);

   // await browser.sleep(1000);
    await console.log('En #msgModalLogin');
    await expect(await browser.executeScript('return document.querySelector("#msgModalLogin").innerHTML')).toEqual('Primer Login, se le sugiere realizar el cambio de contraseña.');
    await browser.sleep(500);
    await browser.executeScript('arguments[0].click();', element(by.buttonText('Confirmar')));
    await browser.sleep(500);
    await expect(await browser.getCurrentUrl()).toContain('firstLogin');
    await browser.sleep(500);

    await page.getFormControl('old').sendKeys('sdgfdgdfgfdgf');
    await page.getFormControl('new').sendKeys('Test123FD');
    await page.getFormControl('confirm').sendKeys('Test123FD');
    await expect(await element(by.css('button[name="submit"]')).isEnabled()).toBeTruthy();
    await browser.executeScript('arguments[0].click();', element(by.css('button[name="submit"]')));
    await browser.sleep(1000);
    await console.log('En #msgFirstLogin');
    await expect(await browser.executeScript('return document.querySelector("#msgFirstLogin").innerHTML'))
      .toEqual('¿Está seguro de confirmar el cambio de contraseña?');
      await browser.sleep(1000);

     await browser.executeScript('arguments[0].click();', element(by.buttonText('Confirmar')));
   // await browser.executeScript('arguments[0].click();', element(by.css('#btnConfirmFirstLogin')));

    // btnConfirmFirstLogin
     await browser.sleep(6000);

    await  console.log('El mensaje Toast changePassword es ' +
    await browser.executeScript('return document.querySelector("div[role=alertdialog]").innerHTML'));
   //  await expect(await browser.executeScript('return document.querySelector("div[role=alertdialog]")
   // .innerHTML')).toEqual('Usuario y/o Contraseña incorrecta, intente nuevamente.');

  });


  */
});
