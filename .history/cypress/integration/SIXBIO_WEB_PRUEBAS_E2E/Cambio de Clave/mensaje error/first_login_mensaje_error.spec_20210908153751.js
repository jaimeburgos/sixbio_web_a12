/// <reference types="cypress" />

context("Pruebas First-Login Mensaje Error", () => {
    // Asignamos los valores de nuestras variables globales del arribo cypress.json
    const userReset = Cypress.env("userReset");
    const passReset = Cypress.env("passReset");
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const url = Cypress.env("url");
    //El beforeEach Ejecuta antes de cada caso
    beforeEach(() => {
        // Visitamos la URL base
        cy.visit(url);
        // Seleccionamos el input usuario realizamos y realizamos click , con clear() lo limpiamos
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        // Seleccionamos el inputo password y realizamos click , con clear() lo limpiamos
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();
    });

    it("Datos Vacíos", () => {
        // Nos logeamos cson el usuario userReset para entrar a la pantalla cambio de contraseña

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");

        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get('input[formControlName="new"]').click({ force: true }).clear();
        // Verificamos los mensajes de validacion para el campo antigua contraseña , se lee las letras en rojo debajo del input
        cy.get("#msgFirstMinOld")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 6 caracteres");
            });

        cy.get('input[formControlName="old"]').click({ force: true }).clear();

        cy.get("#msgFirstReqOld")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese contraseña antigua");
            });

        // Verificamos los mensajes de validacion para el campo nueva contraseña , se lee las letras en rojo debajo del input

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get('input[formControlName="old"]').click({ force: true }).clear();

        cy.get("#msgFirstMinNew")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 6 caracteres");
            });

        cy.get('input[formControlName="new"]').click({ force: true }).clear();

        cy.get("#msgFirstReqNew")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese contraseña nueva");
            });

        // Verificamos los mensajes de validacion para el campo confirmar contraseña , se lee las letras en rojo debajo del input

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get('input[formControlName="old"]').click({ force: true }).clear();

        cy.get("#msgFirstMinConfirm")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 6 caracteres");
            });

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .clear();

        cy.get("#msgFirstReqConfirm")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese contraseña de confirmacion");
            });
    });

    it("Error en contraseña nueva igual a la antigua", () => {
        // Escribimos las credenciales de nuestro usuario administrador y nos logeamos
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);
        // Realizamos click en entrar
        cy.get('button[name="submit"]').click({ force: true });
        // Interceptamos el servicio listarrolesagrupador para esperar que cargue la lista de usuario de la primera ventana gestion de usuarios
        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");
        cy.wait("@listarrolesagrupador");

        // Verificamos que nos encontremos en gestion de usuario
        cy.url().should("include", "/home/gestion-usuarios");
        // Nos dirigimos al menu desplegable con el nombre del usuario logeado
        cy.get("a").contains(userAdmin).click({ force: true });
        cy.wait(1000);
        // selecionamos la oopcion cambio de contraseña y hacemos click
        cy.get("a").contains("Contraseña").click({ force: true });

        cy.wait(2000);
        // entramos en la ventana  cambio de contraseña
        cy.url().should("include", "/firstLogin");

        // Escribimos contraseñas diferentes los input antigua contraseña , nueva contraseña y confirmar contraseña
        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);
        // realizamos click en boton cambiar contraseña
        cy.get('button[name="submit"]').click({ force: true });
        cy.wait(500);
        cy.get("#btnConfirmFirstLogin").click({ force: true });
        // Verificamos el toast de error
        cy.get("#toast-container", { timeout: 5000 })
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });
        // Verificamos el mensaje del toast de error
        cy.get("#toast-container", { timeout: 5000 })
            .find("div")
            .find("div")
            .contains("contraseña")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal(
                    "No puede utilizar una contraseña antigua"
                );
            });
    });

    it("Error contraseña de confirmación diferente", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");

        // Escribimos contraseñas diferentes los input antigua contraseña , nueva contraseña y confirmar contraseña
        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("sdgfdgdfgfdgf")
            .should("have.value", "sdgfdgdfgfdgf");

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("dfgdfgdfgfd")
            .should("have.value", "dfgdfgdfgfd");

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("dfgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfd");
        // realizamos click en boton cambiar contraseña
        cy.get('button[name="submit"]').click({ force: true });
        // Verificamos el toast de error
        cy.get("#toast-container", { timeout: 5000 })
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });
        // Verificamos el mensaje del toast de error
        cy.get("#toast-container", { timeout: 5000 })
            .find("div")
            .find("div")
            .contains("contraseña")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "La contraseña de confirmacion no es igual a la nueva"
                );
            });
    });

    it("Cancelar operación cambio de contraseña a demanda", () => {
        // Escribimos las credenciales de nuestro usuario administrador y nos logeamos
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);
        // Realizamos click en entrar
        cy.get('button[name="submit"]').click({ force: true });
        // Interceptamos el servicio listarrolesagrupador para esperar que cargue la lista de usuario de la primera ventana gestion de usuarios
        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");
        cy.wait("@listarrolesagrupador");

        // Verificamos que nos encontremos en gestion de usuario
        cy.url().should("include", "/home/gestion-usuarios");
        // Nos dirigimos al menu desplegable con el nombre del usuario logeado
        cy.get("a").contains(userAdmin).click({ force: true });
        cy.wait(1000);
        // selecionamos la oopcion cambio de contraseña y hacemos click
        cy.get("a").contains("Contraseña").click({ force: true });

        cy.wait(2000);
        // entramos en la ventana  cambio de contraseña
        cy.url().should("include", "/firstLogin");
        // cancelamos la operacion
        cy.get('button[name="cancel"]').click({ force: true });
        // verificamos que retornemos a la pantalla gestion de usuarios
        cy.url().should("include", "/home/gestion-usuarios");
    });

    it("Cancelar operación cambio de contraseña por primer login", () => {
        // En el usuario escribimos el userReset (variable global)
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);
        // En el passowrd escribimos passReset ( variable global)
        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);
        // Interceptamos el servicio sigon
        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");
        // realizamos click en entrar
        cy.get('button[name="submit"]').click({ force: true });
        // esperamos que termine de carga el servicio signon previamente interceptado , con el wait esperamos
        cy.wait("@signon");
        // Realizamos click en el boton confirmar del modal de cambio de contraseña
        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });
        // Interceptamos el servicio consultardatoscambioclave
        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        // esperamos que termine de carga el servicio consultardatoscambioclave previamente interceptado , con el wait esperamos

        cy.wait("@consultardatoscambioclave");

        // Escribimos datos en el input Antigua Contraseña
        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("sdgfdgdfgfdgf")
            .should("have.value", "sdgfdgdfgfdgf");
        // Escribimos datos en el input Nueva Contraseña

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");
        // Escribimos datos en el input Confirmar Contraseña

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");
        // Realizamos click en cambiar contraseña
        cy.get('button[name="submit"]').click({ force: true });

        // Verificamos el mensaje que aparece en el modal
        cy.get("#msgFirstLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "¿Está seguro de confirmar el cambio de contraseña?"
                );
            });
        cy.wait(500);
        //Cerramos el modal haciendo click en el boton cerrar
        cy.get("#cerrar").click({ force: true });
        // Cancelamos la operacion para volver a la pantalla login
        cy.get('button[name="cancel"]').click({ force: true });
        // Verificamos la URL si estamos en la pantalla login
        cy.url().should("include", "/login");
    });

    it("Cancelar confirmación de cambio de contraseña", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");

        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("sdgfdgdfgfdgf")
            .should("have.value", "sdgfdgdfgfdgf");

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");

        cy.get('button[name="submit"]').click({ force: true });

        cy.get("#msgFirstLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "¿Está seguro de confirmar el cambio de contraseña?"
                );
            });
        cy.wait(500);
        cy.get("#cerrar").click({ force: true });

        cy.get('input[formControlName="confirm"]').should(
            "have.value",
            "Test123FD"
        );
        cy.get('input[formControlName="new"]').should(
            "have.value",
            "Test123FD"
        );
        cy.get('input[formControlName="old"]').should(
            "have.value",
            "sdgfdgdfgfdgf"
        );
    });

    // El response esta mockeado como un vacio para los casos Error de conectividad con SCA y Error de conectividad con SIXBIO Backend
    it("Error de conectividad con SIXBIO Backend ( Mockeado Response Vacio )", () => {
        // Nos logeamos con nuestro usuario UserReset para entrar a la pantalla cambiar contraseña
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");

        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@signon");

        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");
        // Escribimos contraseñas correctas los input antigua contraseña , nueva contraseña y confirmar contraseña

        cy.get('input[formControlName="old"]')
            .click({ force: true })
            .type("sdgfdgdfgfdgf")
            .should("have.value", "sdgfdgdfgfdgf");

        cy.get('input[formControlName="new"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");

        cy.get('input[formControlName="confirm"]')
            .click({ force: true })
            .type("Test123FD")
            .should("have.value", "Test123FD");
        // Realizamos click en el boton cambiar contraseña
        cy.get('button[name="submit"]').click({ force: true });
        // Verificamos el mensaje del modal de cambio de contraseña
        cy.get("#msgFirstLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "¿Está seguro de confirmar el cambio de contraseña?"
                );
            });
        cy.intercept(
            "POST",
            url + "SIXBIO-webcore-Nova/sixsca/cambiarclave",
            []
        ).as("errorConectividad");

        cy.get("#btnConfirmFirstLogin").click({ force: true });
        cy.wait("@errorConectividad");
        cy.get("#toast-container", { timeout: 5000 })
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });
        // Verificamos el mensaje del toast de error

        cy.get("#toast-container", { timeout: 5000 })
            .find("div")
            .find("div")
            .contains("conectividad")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Error de conectividad.");
            });
    });
});
