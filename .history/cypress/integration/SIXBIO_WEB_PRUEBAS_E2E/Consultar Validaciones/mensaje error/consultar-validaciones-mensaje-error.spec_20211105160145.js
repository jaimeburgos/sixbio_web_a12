/// <reference types="cypress" />

context("Pruebas consultar-validaciones Mensaje Error", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const userSinData = Cypress.env("userSinData");
    const userData = Cypress.env("userData");
    const url = Cypress.env("url");

    beforeEach(() => {
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixbio/consultavalidaciones",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/service/reportevalidaciones",
            []
        );

        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarrolesagrupador");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarusuarios");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixbio/consultavalidaciones",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("consultavalidaciones");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/service/reportevalidaciones",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("reportevalidaciones");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);

        cy.get("a").contains("Módulo de Consultas").click({ force: true });
        cy.wait(500);
        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarusuarios2");

        cy.get("a").contains("Consultar Validaciones").click({ force: true });
        cy.wait("@listarusuarios2");
    });

    it("Verificamos el mensaje Toast cuando una consulta de usuario no posee datos con filtro cantidad", () => {
        // Seleccionamos un usuario sin data
        cy.get('select[formControlName="filterUsuario"]').select(userSinData, {
            force: true,
        });
        // Ingresamos una cantidad en el filtro cantidad
        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("9999");
        // Realizamos click en la busqueda
        cy.get('button[type="submit"]').click({ force: true });
        // Validamos el contenido de nuestro Toast de Error
        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("CONSULTA")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("NO HAY DATOS PARA LA CONSULTA");
            });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });
    });

    it.only("Verificamos el mensaje Toast cuando una consulta de usuario no posee datos con filtro fechas", () => {
        //Realizamos click en el check de filtro por fechas
        cy.get("#defaultUnchecked").click({ force: true });
        // Seleccionamos un usuario sin data
        cy.get('select[formControlName="filterUsuario"]').select(userSinData, {
            force: true,
        });

        // Seleccionamos fecha Desde y Hasta
        var fecha = new Date();
        var year = fecha.getFullYear();        
        cy.get('button[class="current"]').contains(year).click({ force: true });
        // Obtenemos de la variable fechaDesde año el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaDesde.año)
            .click({ force: true });
        // Obtenemos de la variable fechaDesde nombreMes el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaDesde.nombreMes)
            .click({ force: true });
        // Obtenemos de la variable fechaDesde numDia el valor que asignamos en cypress.json ( es una variable global )
        cy.get("span.ng-star-inserted")
            .contains(fechaDesde.numDia)
            .click({ force: true });
        cy.get("span.ng-star-inserted")
            .contains(fechaDesde.numDia)
            .click({ force: true });

        cy.get('input[formControlName="filterfechaHasta"]').click({
            force: true,
        });
        cy.get('button[class="current"]').contains(year).click({ force: true });
        // Obtenemos de la variable fechaHasta año el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaHasta.año)
            .click({ force: true });
        // Obtenemos de la variable fechaHasta nombreMes el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaHasta.nombreMes)
            .click({ force: true });
        // Obtenemos de la variable fechaHasta numDia el valor que asignamos en cypress.json ( es una variable global )
        cy.get("span.ng-star-inserted")
            .contains(fechaHasta.numDia)
            .click({ force: true });
        cy.get("span.ng-star-inserted")
            .contains(fechaHasta.numDia)
            .click({ force: true });
        //Realizamos click en buscar
        // Realizamos click en el boton busqueda
        cy.get('button[type="submit"]').click({ force: true });
        // Verificamos el contenido de nuestro toast de Error
        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("CONSULTA")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("NO HAY DATOS PARA LA CONSULTA");
            });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });
    });

    it.only("Verificamos cambios en la grilla despues una busqueda con fecha", () => {
        //Realizamos click en el check de filtro por fechas
        cy.get("#defaultUnchecked").click({ force: true });
        // Seleccionamos un usuario con data
        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });
        // Seleccionamos fecha Desde y Hasta

        var fecha = new Date();
        var year = fecha.getFullYear();        
        cy.get('button[class="current"]').contains(year).click({ force: true });
        // Obtenemos de la variable fechaDesde año el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaDesde.año)
            .click({ force: true });
        // Obtenemos de la variable fechaDesde nombreMes el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaDesde.nombreMes)
            .click({ force: true });
        // Obtenemos de la variable fechaDesde numDia el valor que asignamos en cypress.json ( es una variable global )
        cy.get("span.ng-star-inserted")
            .contains(fechaDesde.numDia)
            .click({ force: true });
        cy.get("span.ng-star-inserted")
            .contains(fechaDesde.numDia)
            .click({ force: true });

        cy.get('input[formControlName="filterfechaHasta"]').click({
            force: true,
        });
        cy.get('button[class="current"]').contains(year).click({ force: true });
        // Obtenemos de la variable fechaHasta año el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaHasta.año)
            .click({ force: true });
        // Obtenemos de la variable fechaHasta nombreMes el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaHasta.nombreMes)
            .click({ force: true });
        // Obtenemos de la variable fechaHasta numDia el valor que asignamos en cypress.json ( es una variable global )
        cy.get("span.ng-star-inserted")
            .contains(fechaHasta.numDia)
            .click({ force: true });
        cy.get("span.ng-star-inserted")
            .contains(fechaHasta.numDia)
            .click({ force: true });
        // Realizamos click en el boton busqueda
        cy.get('button[type="submit"]').click({ force: true });
        //Esperamos que cargue la busqueda realizada
        cy.wait("@consultavalidaciones");
        // En la grilla de tamaño de pagina seleccionamos 5
        cy.get("select[id=inputSizePagination]").select("5", {
            force: true,
        });
        // Esperamos cambie la cantidad
        cy.wait("@consultavalidaciones");
        // En la grilla de tamaño de pagina seleccionamos 10
        cy.get("select[id=inputSizePagination]").select("10", {
            force: true,
        });
        // Esperamos cambie la cantidad
        cy.wait("@consultavalidaciones");
        // En la grilla de tamaño de pagina seleccionamos 15
        cy.get("select[id=inputSizePagination]").select("15", {
            force: true,
        });
        // Esperamos cambie la cantidad
        cy.wait("@consultavalidaciones");
        // En la grilla de tamaño de pagina seleccionamos 20
        cy.get("select[id=inputSizePagination]").select("20", {
            force: true,
        });
    });

    it("Verificamos cambios en la grilla despues una busqueda con cantidad", () => {
        // Seleccionamos un usuario con data

        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });
        // Seleccionamos el filtro cantidad y escribimos la cantidad
        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("444");
        // Realizamos click en el boton busqueda
        cy.get('button[type="submit"]').click({ force: true });
        // Esperamos carguen los datos
        cy.wait("@consultavalidaciones");
        // En tamaño pagina seleccionamos 5
        cy.get("select[id=inputSizePagination]").select("5", {
            force: true,
        });
        // Esperamos los cambios
        cy.wait("@consultavalidaciones");
        // En tamaño pagina seleccionamos 10
        cy.get("select[id=inputSizePagination]").select("10", {
            force: true,
        });
        // Esperamos los cambios
        cy.wait("@consultavalidaciones");
        // En tamaño pagina seleccionamos 15
        cy.get("select[id=inputSizePagination]").select("15", {
            force: true,
        });
        // Esperamos los cambios
        cy.wait("@consultavalidaciones");
        // En tamaño pagina seleccionamos 20
        cy.get("select[id=inputSizePagination]").select("20", {
            force: true,
        });
    });

    it("Verificamos funcionamiento boton regresar desde detalle-validacion ", () => {
         // Seleccionamos un usuario en el combo con data
         cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });
        // Escribimos una cantidad en el filtro cantidad
        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("444");
        // Hacemos click en el boton busqueda
        cy.get('button[type="submit"]').click({ force: true });
        // Esperamos que cargue la data
        cy.wait("@consultavalidaciones");
        // Realizamos click en el primer boton "Ver Detalle"
        cy.get('button[title="Ver detalle"]').first().click({ force: true });
        // Verificamos que entramos a la ruta /home/consultar-validaciones/detalle-validacion
        cy.url().should(
            "include",
            "/home/consultar-validaciones/detalle-validacion"
        );
        // Realizamos click en el boton "Regresar"
        cy.get("button").contains("Regresar").click({ force: true });
        // Esperamos realice la carga
        cy.wait(["@consultavalidaciones", "@listarusuarios"]);
        // Verificamos que nos encontramos en la pantalla consultar-validaciones
        cy.url().should("include", "/home/consultar-validaciones");
    });
});
