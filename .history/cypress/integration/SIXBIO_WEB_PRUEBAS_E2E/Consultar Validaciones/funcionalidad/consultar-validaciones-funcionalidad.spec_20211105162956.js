/// <reference types="cypress" />

context("Pruebas consultar-validaciones", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const userSinData = Cypress.env("userSinData");
    const userData = Cypress.env("userData");
    const url = Cypress.env("url");
    const fechaHasta = Cypress.env("fechaHasta");
    const fechaDesde = Cypress.env("fechaDesde");
    beforeEach(() => {
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixbio/consultavalidaciones",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/service/reportevalidaciones",
            []
        );

        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarrolesagrupador");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarusuarios");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixbio/consultavalidaciones",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("consultavalidaciones");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/service/reportevalidaciones",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("reportevalidaciones");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);

        cy.get("a").contains("Módulo de Consultas").click({ force: true });
        cy.wait(500);
        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarusuarios2");

        cy.get("a").contains("Consultar Validaciones").click({ force: true });
        cy.wait("@listarusuarios2");
    });
 

    it("No hay datos para la consulta (Campo Cantidad)", () => {
        cy.get('select[formControlName="filterUsuario"]').select(userSinData, {
            force: true,
        });
        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("9999");

        cy.get('button[type="submit"]').click({ force: true });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("CONSULTA")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("NO HAY DATOS PARA LA CONSULTA");
            });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });
    });

    it("No hay datos para la consulta (Campo Fecha)", () => {
        cy.get("#defaultUnchecked").click({ force: true });
        cy.get('select[formControlName="filterUsuario"]').select(userSinData, {
            force: true,
        });
        // Seleccionamos fecha Desde y Hasta
        var fecha = new Date();
        var year = fecha.getFullYear();        
        cy.get('input[formControlName="filterfechaDesde"]').click({
            force: true,
        });     
        cy.get('button[class="current"]').contains(year).click({ force: true });
        // Obtenemos de la variable fechaDesde año el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaDesde.año)
            .click({ force: true });
        // Obtenemos de la variable fechaDesde nombreMes el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaDesde.nombreMes)
            .click({ force: true });
        // Obtenemos de la variable fechaDesde numDia el valor que asignamos en cypress.json ( es una variable global )
        cy.get("span.ng-star-inserted")
            .contains(fechaDesde.numDia)
            .click({ force: true });
        cy.get("span.ng-star-inserted")
            .contains(fechaDesde.numDia)
            .click({ force: true });

        cy.get('input[formControlName="filterfechaHasta"]').click({
            force: true,
        });
        cy.get('button[class="current"]').contains(year).click({ force: true });
        // Obtenemos de la variable fechaHasta año el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaHasta.año)
            .click({ force: true });
        // Obtenemos de la variable fechaHasta nombreMes el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaHasta.nombreMes)
            .click({ force: true });
        // Obtenemos de la variable fechaHasta numDia el valor que asignamos en cypress.json ( es una variable global )
        cy.get("span.ng-star-inserted")
            .contains(fechaHasta.numDia)
            .click({ force: true });
        cy.get("span.ng-star-inserted")
            .contains(fechaHasta.numDia)
            .click({ force: true });

        cy.get('button[type="submit"]').click({ force: true });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("CONSULTA")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("NO HAY DATOS PARA LA CONSULTA");
            });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });
    });

    // Este caso se mockea un response para verificar el mensaje en los casos
    // Error de conectividad con SIXBIO BACKEND. 
    // Error de conectividad con SIXBIO CORE. 
    it("Error de conectividad con SIXBIO (MOCKEADO)", () => {
 
        cy.intercept('GET',  url + "SIXBIO-webcore-Nova/sixbio/consultavalidaciones", {
            statusCode: 404,
            body: {
              name:'NOT FOUND',
            },
          })
        cy.intercept('POST',  url + "SIXBIO-webcore-Nova/sixbio/consultavalidaciones", {
            statusCode: 404,
            body: {
              name:'NOT FOUND',
            },
          })


        cy.get('select[formControlName="filterUsuario"]').select(userSinData, {
            force: true,
        });
        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("9999");

        cy.get('button[type="submit"]').click({ force: true });



        cy.get("#toast-container",{timeout:2000})
            .find("div")
            .find("div")
            .contains("conectividad")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Error de conectividad.");
            });

        cy.get("#toast-container",{timeout:2000})
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });


    });

 
});
