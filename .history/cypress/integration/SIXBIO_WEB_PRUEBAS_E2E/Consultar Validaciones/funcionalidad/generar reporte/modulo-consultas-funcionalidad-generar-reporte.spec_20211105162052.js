/// <reference types="cypress" />

context("Pruebas Funcionalidad Modulo Consultas Generar Reporte", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const userSinData = Cypress.env("userSinData");
    const userData = Cypress.env("userData");
    const url = Cypress.env("url");
    const fechaHasta = Cypress.env("fechaHasta");
    const fechaDesde = Cypress.env("fechaDesde");

    beforeEach(() => {
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixbio/consultavalidaciones",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/service/reportevalidaciones",
            []
        );
        // Visitamos nuestras URL Base e ingresamos nuestras credenciales de usuario
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);
        //Realizamos click en el boton entrar
        cy.get('button[name="submit"]').click({ force: true });
        //Interceptamos nuestros servicios para usar el cy.wait para esperar su tiempo de carga para la renderizacion de objetos
        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarrolesagrupador");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarusuarios");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixbio/consultavalidaciones",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("consultavalidaciones");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/service/reportevalidaciones",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("reportevalidaciones");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);
        // Realizamos click sobre el menu Modulo de Consultas
        cy.get("a").contains("Módulo de Consultas").click({ force: true });
        cy.wait(500);
        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarusuarios2");
        // Hacemos click en el menu Coonsultar validaciones
        cy.get("a").contains("Consultar Validaciones").click({ force: true });
        cy.wait("@listarusuarios2");
    });

    it("Reporte Validaciones - Excel y PDF (Campo Fechas) ", () => {
        //Realizamos click el check Búsqueda por fechas para activarlo

        cy.get("#defaultUnchecked").click({ force: true });
        // Seleccionamos nuestro usuario con data

        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });
   
        // Seleccionamos fecha Desde y Hasta
        var fecha = new Date();
        var year = fecha.getFullYear();        
        cy.get('input[formControlName="filterfechaDesde"]').click({
            force: true,
        });     
        cy.get('button[class="current"]').contains(year).click({ force: true });
        // Obtenemos de la variable fechaDesde año el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaDesde.año)
            .click({ force: true });
        // Obtenemos de la variable fechaDesde nombreMes el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaDesde.nombreMes)
            .click({ force: true });
        // Obtenemos de la variable fechaDesde numDia el valor que asignamos en cypress.json ( es una variable global )
        cy.get("span.ng-star-inserted")
            .contains(fechaDesde.numDia)
            .click({ force: true });
        cy.get("span.ng-star-inserted")
            .contains(fechaDesde.numDia)
            .click({ force: true });

        cy.get('input[formControlName="filterfechaHasta"]').click({
            force: true,
        });
        cy.get('button[class="current"]').contains(year).click({ force: true });
        // Obtenemos de la variable fechaHasta año el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaHasta.año)
            .click({ force: true });
        // Obtenemos de la variable fechaHasta nombreMes el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaHasta.nombreMes)
            .click({ force: true });
        // Obtenemos de la variable fechaHasta numDia el valor que asignamos en cypress.json ( es una variable global )
        cy.get("span.ng-star-inserted")
            .contains(fechaHasta.numDia)
            .click({ force: true });
        cy.get("span.ng-star-inserted")
            .contains(fechaHasta.numDia)
            .click({ force: true });
        //Realizamos click en el boton busqueda
        cy.get('button[type="submit"]').click({ force: true });
        cy.wait("@consultavalidaciones");
        cy.get("button[id=excel]").click({ force: true });
        //
        cy.wait("@consultavalidaciones");
        // cy.wait("@reportevalidaciones");
        //Validamos que se llame el servicio y la ruta actual
        cy.url().should("include", "/home/consultar-validaciones");

        cy.get("button[id=pdf]").click({ force: true });
    });

    it("Reporte Validaciones - Excel y PDF (Campo Cantidad)", () => {
        //Selecionamos un usuario con data
        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });
        //Realizamos una busqueda por cantidad
        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("100");
        //Realizamos click en busqueda
        cy.get('button[type="submit"]').click({ force: true });
        //Realizamos la descarga del archivo xls
        cy.wait("@consultavalidaciones");
        cy.get("button[id=excel]").click({ force: true });
        //
        cy.wait("@consultavalidaciones");
        cy.wait("@reportevalidaciones");
        //Realizamos la descarga del archivo PDF
        cy.get("button[id=pdf]").click({ force: true });
        cy.wait("@consultavalidaciones");
        cy.wait("@reportevalidaciones");
    });
});
