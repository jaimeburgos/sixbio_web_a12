/// <reference types="cypress" />

context("Pruebas Interfaz Modulo Consultas", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const userSinData = Cypress.env("userSinData");
    const userData = Cypress.env("userData");
    const url = Cypress.env("url");
    const fechaHasta = Cypress.env("fechaHasta");
    const fechaDesde = Cypress.env("fechaDesde");

    beforeEach(() => {
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixbio/consultavalidaciones",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/service/reportevalidaciones",
            []
        );

        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarrolesagrupador");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarusuarios");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixbio/consultavalidaciones",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("consultavalidaciones");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/service/reportevalidaciones",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("reportevalidaciones");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);

        cy.get("a").contains("Módulo de Consultas").click({ force: true });
        cy.wait(500);
        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarusuarios2");

        cy.get("a").contains("Consultar Validaciones").click({ force: true });
        cy.wait("@listarusuarios2");
    });

    it("Consultar Validacions longitud maxima y caracteres numericos campo Cantidad", () => {
        const U_Permitidos = ["1234"];
        // Iteramos los caracteres validos para el campo cantidad en el cual validara si se escriben o no
        for (let i = 0; i <= U_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="cantidad"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="cantidad"]')
                .click({ force: true })
                .type(U_Permitidos[i])
                .should("have.value", U_Permitidos[i]);
        }

        const U_No_Permitidos = [
            "asdsdfgsdf",
            "sdfdñdhgfgdf",
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
        ];
        // Iteramos los caracteres NO validos para el campo cantidad en el cual validara si se escriben o no
        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get('input[formControlName="cantidad"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="cantidad"]')
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Consultar por cantidad ", () => {
        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });
        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("4444");

        cy.get('button[type="submit"]').click({ force: true });
    });

    it("Consultar por fechas ", () => {
        cy.get("#defaultUnchecked").click({ force: true });
        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });
      
        // Seleccionamos fecha Desde y Hasta
        var fecha = new Date();
        var year = fecha.getFullYear();        
        cy.get('input[formControlName="filterfechaDesde"]').click({
            force: true,
        });     
        cy.get('button[class="current"]').contains(year).click({ force: true });
        // Obtenemos de la variable fechaDesde año el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaDesde.año)
            .click({ force: true });
        // Obtenemos de la variable fechaDesde nombreMes el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaDesde.nombreMes)
            .click({ force: true });
        // Obtenemos de la variable fechaDesde numDia el valor que asignamos en cypress.json ( es una variable global )
        cy.get("span.ng-star-inserted")
            .contains(fechaDesde.numDia)
            .click({ force: true });
        cy.get("span.ng-star-inserted")
            .contains(fechaDesde.numDia)
            .click({ force: true });

        cy.get('input[formControlName="filterfechaHasta"]').click({
            force: true,
        });
        cy.get('button[class="current"]').contains(year).click({ force: true });
        // Obtenemos de la variable fechaHasta año el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaHasta.año)
            .click({ force: true });
        // Obtenemos de la variable fechaHasta nombreMes el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaHasta.nombreMes)
            .click({ force: true });
        // Obtenemos de la variable fechaHasta numDia el valor que asignamos en cypress.json ( es una variable global )
        cy.get("span.ng-star-inserted")
            .contains(fechaHasta.numDia)
            .click({ force: true });
        cy.get("span.ng-star-inserted")
            .contains(fechaHasta.numDia)
            .click({ force: true });

        cy.get('button[type="submit"]').click({ force: true });
        cy.wait("@consultavalidaciones");
        cy.get("button[id=excel]").click({ force: true });

        cy.get("button[id=pdf]").click({ force: true });
    });

    it("Consultar por fechas Excel o PDF ", () => {
        cy.get("#defaultUnchecked").click({ force: true });
        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });
        // Seleccionamos fecha Desde y Hasta
        var fecha = new Date();
        var year = fecha.getFullYear();        
        cy.get('input[formControlName="filterfechaDesde"]').click({
            force: true,
        });     
        cy.get('button[class="current"]').contains(year).click({ force: true });
        // Obtenemos de la variable fechaDesde año el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaDesde.año)
            .click({ force: true });
        // Obtenemos de la variable fechaDesde nombreMes el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaDesde.nombreMes)
            .click({ force: true });
        // Obtenemos de la variable fechaDesde numDia el valor que asignamos en cypress.json ( es una variable global )
        cy.get("span.ng-star-inserted")
            .contains(fechaDesde.numDia)
            .click({ force: true });
        cy.get("span.ng-star-inserted")
            .contains(fechaDesde.numDia)
            .click({ force: true });

        cy.get('input[formControlName="filterfechaHasta"]').click({
            force: true,
        });
        cy.get('button[class="current"]').contains(year).click({ force: true });
        // Obtenemos de la variable fechaHasta año el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaHasta.año)
            .click({ force: true });
        // Obtenemos de la variable fechaHasta nombreMes el valor que asignamos en cypress.json ( es una variable global )
        cy.get("td[class='ng-star-inserted']")
            .contains(fechaHasta.nombreMes)
            .click({ force: true });
        // Obtenemos de la variable fechaHasta numDia el valor que asignamos en cypress.json ( es una variable global )
        cy.get("span.ng-star-inserted")
            .contains(fechaHasta.numDia)
            .click({ force: true });
        cy.get("span.ng-star-inserted")
            .contains(fechaHasta.numDia)
            .click({ force: true });

        cy.get('button[type="submit"]').click({ force: true });
        cy.wait("@consultavalidaciones");
        cy.get("button[id=excel]").click({ force: true });
        //
        cy.wait("@consultavalidaciones");
        // cy.wait("@reportevalidaciones");
        cy.get("button[id=pdf]").click({ force: true });
    });

    it("Consultar por cantidad Excel o PDF", () => {
        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });
        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("9999");

        cy.get('button[type="submit"]').click({ force: true });

        cy.wait("@consultavalidaciones");
        cy.get("button[id=excel]").click({ force: true });
        //
        cy.wait("@consultavalidaciones");
        cy.wait("@reportevalidaciones");
        cy.get("button[id=pdf]").click({ force: true });
    });

    it("Pantalla Detalle Validacion (Busqueda por Campo Cantidad)", () => {
        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });

        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("444");
        cy.get('button[type="submit"]').click({ force: true });

        cy.wait("@consultavalidaciones");
        cy.get('button[title="Ver detalle"]').first().click({ force: true });

        cy.url().should(
            "include",
            "/home/consultar-validaciones/detalle-validacion"
        );

        cy.get("div[id=detalleValidacion]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Detalle Validación");
            });

        cy.get("label[for='inputUsuario']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Usuario Consulta");
            });

        cy.get("label[for='inputValidacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Tipo Verificación");
            });

        cy.get("label[for='inputMensajeRpta']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Mensaje Respuesta");
            });

        cy.get("label[for='inputFechaValidacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Consulta");
            });

        cy.get("label[for='inputSistLocalizacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Sistema Localización");
            });

        cy.get("label[for='inputLalitud']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Latitud Localización");
            });

        cy.get("label[for='inputIDValidacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Id Validacion");
            });

        cy.get("label[for='inputOrigenR']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Origen Respuesta");
            });

        cy.get("label[for='inputCodigoRespuesta']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Codigo Respuesta");
            });

        cy.get("label[for='inputHoraConsulta']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Hora Consulta");
            });

        cy.get("label[for='inputLongitud']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Longitud Localización");
            });

        cy.get("label[for='inputApellidoPaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Paterno");
            });

        cy.get("label[for='inputNombres']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nombres");
            });

        cy.get("label[for='inputSexo']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Sexo");
            });

        cy.get("label[for='inputFechaNac']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Nacimiento");
            });

        cy.get("label[for='inputFechaEmision']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Emision");
            });

        cy.get("label[for='inputApellidoMaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Materno");
            });

        cy.get("label[for='inputTipoDoc']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Tipo Documento");
            });

        cy.get("label[for='inputNroDocumento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Número de Documento");
            });

        cy.get("div[id=datosPersona]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Datos de la persona consultada");
            });
    });

    it("Pantalla Detalle Validacion (Busqueda por Campos Fecha)", () => {
        cy.get("#defaultUnchecked").click({ force: true });

        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });
         // Seleccionamos fecha Desde y Hasta
         var fecha = new Date();
         var year = fecha.getFullYear();        
         cy.get('input[formControlName="filterfechaDesde"]').click({
             force: true,
         });     
         cy.get('button[class="current"]').contains(year).click({ force: true });
         // Obtenemos de la variable fechaDesde año el valor que asignamos en cypress.json ( es una variable global )
         cy.get("td[class='ng-star-inserted']")
             .contains(fechaDesde.año)
             .click({ force: true });
         // Obtenemos de la variable fechaDesde nombreMes el valor que asignamos en cypress.json ( es una variable global )
         cy.get("td[class='ng-star-inserted']")
             .contains(fechaDesde.nombreMes)
             .click({ force: true });
         // Obtenemos de la variable fechaDesde numDia el valor que asignamos en cypress.json ( es una variable global )
         cy.get("span.ng-star-inserted")
             .contains(fechaDesde.numDia)
             .click({ force: true });
         cy.get("span.ng-star-inserted")
             .contains(fechaDesde.numDia)
             .click({ force: true });
 
         cy.get('input[formControlName="filterfechaHasta"]').click({
             force: true,
         });
         cy.get('button[class="current"]').contains(year).click({ force: true });
         // Obtenemos de la variable fechaHasta año el valor que asignamos en cypress.json ( es una variable global )
         cy.get("td[class='ng-star-inserted']")
             .contains(fechaHasta.año)
             .click({ force: true });
         // Obtenemos de la variable fechaHasta nombreMes el valor que asignamos en cypress.json ( es una variable global )
         cy.get("td[class='ng-star-inserted']")
             .contains(fechaHasta.nombreMes)
             .click({ force: true });
         // Obtenemos de la variable fechaHasta numDia el valor que asignamos en cypress.json ( es una variable global )
         cy.get("span.ng-star-inserted")
             .contains(fechaHasta.numDia)
             .click({ force: true });
         cy.get("span.ng-star-inserted")
             .contains(fechaHasta.numDia)
             .click({ force: true });
        cy.get('button[type="submit"]').click({ force: true });

        cy.wait("@consultavalidaciones");
        cy.get('button[title="Ver detalle"]').first().click({ force: true });

        cy.url().should(
            "include",
            "/home/consultar-validaciones/detalle-validacion"
        );

        cy.get("div[id=detalleValidacion]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Detalle Validación");
            });

        cy.get("label[for='inputUsuario']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Usuario Consulta");
            });

        cy.get("label[for='inputValidacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Tipo Verificación");
            });

        cy.get("label[for='inputMensajeRpta']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Mensaje Respuesta");
            });

        cy.get("label[for='inputFechaValidacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Consulta");
            });

        cy.get("label[for='inputSistLocalizacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Sistema Localización");
            });

        cy.get("label[for='inputLalitud']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Latitud Localización");
            });

        cy.get("label[for='inputIDValidacion']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Id Validacion");
            });

        cy.get("label[for='inputOrigenR']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Origen Respuesta");
            });

        cy.get("label[for='inputCodigoRespuesta']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Codigo Respuesta");
            });

        cy.get("label[for='inputHoraConsulta']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Hora Consulta");
            });

        cy.get("label[for='inputLongitud']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Longitud Localización");
            });

        cy.get("label[for='inputApellidoPaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Paterno");
            });

        cy.get("label[for='inputNombres']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nombres");
            });

        cy.get("label[for='inputSexo']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Sexo");
            });

        cy.get("label[for='inputFechaNac']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Nacimiento");
            });

        cy.get("label[for='inputFechaEmision']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha Emision");
            });

        cy.get("label[for='inputApellidoMaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Materno");
            });

        cy.get("label[for='inputTipoDoc']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Tipo Documento");
            });

        cy.get("label[for='inputNroDocumento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Número de Documento");
            });

        cy.get("div[id=datosPersona]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Datos de la persona consultada");
            });
    });

    it("Verificamos cambios en la grilla despues una busqueda con cantidad", () => {
        cy.get("#defaultUnchecked").click({ force: true });

        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });

         // Seleccionamos fecha Desde y Hasta
         var fecha = new Date();
         var year = fecha.getFullYear();        
         cy.get('input[formControlName="filterfechaDesde"]').click({
             force: true,
         });     
         cy.get('button[class="current"]').contains(year).click({ force: true });
         // Obtenemos de la variable fechaDesde año el valor que asignamos en cypress.json ( es una variable global )
         cy.get("td[class='ng-star-inserted']")
             .contains(fechaDesde.año)
             .click({ force: true });
         // Obtenemos de la variable fechaDesde nombreMes el valor que asignamos en cypress.json ( es una variable global )
         cy.get("td[class='ng-star-inserted']")
             .contains(fechaDesde.nombreMes)
             .click({ force: true });
         // Obtenemos de la variable fechaDesde numDia el valor que asignamos en cypress.json ( es una variable global )
         cy.get("span.ng-star-inserted")
             .contains(fechaDesde.numDia)
             .click({ force: true });
         cy.get("span.ng-star-inserted")
             .contains(fechaDesde.numDia)
             .click({ force: true });
 
         cy.get('input[formControlName="filterfechaHasta"]').click({
             force: true,
         });
         cy.get('button[class="current"]').contains(year).click({ force: true });
         // Obtenemos de la variable fechaHasta año el valor que asignamos en cypress.json ( es una variable global )
         cy.get("td[class='ng-star-inserted']")
             .contains(fechaHasta.año)
             .click({ force: true });
         // Obtenemos de la variable fechaHasta nombreMes el valor que asignamos en cypress.json ( es una variable global )
         cy.get("td[class='ng-star-inserted']")
             .contains(fechaHasta.nombreMes)
             .click({ force: true });
         // Obtenemos de la variable fechaHasta numDia el valor que asignamos en cypress.json ( es una variable global )
         cy.get("span.ng-star-inserted")
             .contains(fechaHasta.numDia)
             .click({ force: true });
         cy.get("span.ng-star-inserted")
             .contains(fechaHasta.numDia)
             .click({ force: true });

        cy.get('button[type="submit"]').click({ force: true });

        cy.wait("@consultavalidaciones");

        cy.get("select[id=inputSizePagination]").select("5", {
            force: true,
        });

        cy.wait("@consultavalidaciones");

        cy.get("select[id=inputSizePagination]").select("10", {
            force: true,
        });

        cy.wait("@consultavalidaciones");

        cy.get("select[id=inputSizePagination]").select("15", {
            force: true,
        });
        cy.wait("@consultavalidaciones");

        cy.get("select[id=inputSizePagination]").select("20", {
            force: true,
        });
    });

    it("Verificamos cambios en la grilla despues una busqueda con fecha", () => {
        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });

        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("444");

        cy.get('button[type="submit"]').click({ force: true });

        cy.wait("@consultavalidaciones");

        cy.get("select[id=inputSizePagination]").select("5", {
            force: true,
        });

        cy.wait("@consultavalidaciones");

        cy.get("select[id=inputSizePagination]").select("10", {
            force: true,
        });

        cy.wait("@consultavalidaciones");

        cy.get("select[id=inputSizePagination]").select("15", {
            force: true,
        });
        cy.wait("@consultavalidaciones");

        cy.get("select[id=inputSizePagination]").select("20", {
            force: true,
        });
    });

    it("Consulta sin datos encontrados (Campo Cantidad)", () => {
        cy.get('select[formControlName="filterUsuario"]').select(userSinData, {
            force: true,
        });
        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("9999");

        cy.get('button[type="submit"]').click({ force: true });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("CONSULTA")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("NO HAY DATOS PARA LA CONSULTA");
            });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });
    });

    it("Consulta sin datos encontrados (Campo Fecha)", () => {
        cy.get("#defaultUnchecked").click({ force: true });
        cy.get('select[formControlName="filterUsuario"]').select(userSinData, {
            force: true,
        });
         // Seleccionamos fecha Desde y Hasta
         var fecha = new Date();
         var year = fecha.getFullYear();        
         cy.get('input[formControlName="filterfechaDesde"]').click({
             force: true,
         });     
         cy.get('button[class="current"]').contains(year).click({ force: true });
         // Obtenemos de la variable fechaDesde año el valor que asignamos en cypress.json ( es una variable global )
         cy.get("td[class='ng-star-inserted']")
             .contains(fechaDesde.año)
             .click({ force: true });
         // Obtenemos de la variable fechaDesde nombreMes el valor que asignamos en cypress.json ( es una variable global )
         cy.get("td[class='ng-star-inserted']")
             .contains(fechaDesde.nombreMes)
             .click({ force: true });
         // Obtenemos de la variable fechaDesde numDia el valor que asignamos en cypress.json ( es una variable global )
         cy.get("span.ng-star-inserted")
             .contains(fechaDesde.numDia)
             .click({ force: true });
         cy.get("span.ng-star-inserted")
             .contains(fechaDesde.numDia)
             .click({ force: true });
 
         cy.get('input[formControlName="filterfechaHasta"]').click({
             force: true,
         });
         cy.get('button[class="current"]').contains(year).click({ force: true });
         // Obtenemos de la variable fechaHasta año el valor que asignamos en cypress.json ( es una variable global )
         cy.get("td[class='ng-star-inserted']")
             .contains(fechaHasta.año)
             .click({ force: true });
         // Obtenemos de la variable fechaHasta nombreMes el valor que asignamos en cypress.json ( es una variable global )
         cy.get("td[class='ng-star-inserted']")
             .contains(fechaHasta.nombreMes)
             .click({ force: true });
         // Obtenemos de la variable fechaHasta numDia el valor que asignamos en cypress.json ( es una variable global )
         cy.get("span.ng-star-inserted")
             .contains(fechaHasta.numDia)
             .click({ force: true });
         cy.get("span.ng-star-inserted")
             .contains(fechaHasta.numDia)
             .click({ force: true });

        cy.get('button[type="submit"]').click({ force: true });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("CONSULTA")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("NO HAY DATOS PARA LA CONSULTA");
            });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });
    });

    it("Verificamos funcionamiento boton regresar desde detalle-validacion ", () => {
        // Seleccionamos un usuario en el combo con data
        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });
        // Escribimos una cantidad en el filtro cantidad
        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("444");
        // Hacemos click en el boton busqueda
        cy.get('button[type="submit"]').click({ force: true });
        // Esperamos que cargue la data
        cy.wait("@consultavalidaciones");
        // Realizamos click en el primer boton "Ver Detalle"
        cy.get('button[title="Ver detalle"]').first().click({ force: true });
        // Verificamos que entramos a la ruta /home/consultar-validaciones/detalle-validacion
        cy.url().should(
            "include",
            "/home/consultar-validaciones/detalle-validacion"
        );
        // Realizamos click en el boton "Regresar"
        cy.get("button").contains("Regresar").click({ force: true });
        // Esperamos realice la carga
        cy.wait(["@consultavalidaciones", "@listarusuarios"]);
        // Verificamos que nos encontramos en la pantalla consultar-validaciones
        cy.url().should("include", "/home/consultar-validaciones");
    });
});
