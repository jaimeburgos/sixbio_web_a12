/// <reference types="cypress" />

context("Pruebas Modulo Verificador Validacion de Campos", () => {
    const userVer = Cypress.env("userVer");
    const passVer = Cypress.env("passVer");
    const userSinData = Cypress.env("userSinData");
    const sixbioVisualizar = Cypress.env("sixbioVisualizar");
    const userData = Cypress.env("userData");
    const url = Cypress.env("url");

    beforeEach(() => {
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userVer)
            .should("have.value", userVer);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passVer)
            .should("have.value", passVer);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
        }).as("listarusuarios");

       // cy.wait(["@listarrolesagrupador", "@listarusuarios"]);
    });

    it("Dni no ingresado", () => {
        // Escribimos un DNI Validador cualquier
        cy.get("#inputValidador").click({ force: true }).type("12345678");
        //Realizamos Click en el boton mejor huella
        cy.get("#mejorHuella").click({ force: true });
        cy.wait(1000);
        // Verificamos el mensaje Ingrese un DNI a consultar
        cy.get("#msgDNIConsultar")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Ingrese un DNI a consultar");
            });
    });

    it("Longitud maxima y caracteres numericos campo DNI", () => {
        //Verificamos la longitud maxima del inputDniConsultor sea de maximo 20 caracteres
        cy.get("#inputDniConsultor")
            .click({ force: true })
            .type("123456789012345678901")
            .should("have.value", "12345678901234567890");
        // No se permite caracteres NO numericos
        const U_No_Permitidos = [
            "sdafsdfsdfsd",
            "sdflfdbvcbv@",
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
            "          ",
        ];
        //Interamos el arreglo de caracteres no permitodos y verificamos que sea vacio

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get("#inputDniConsultor").click({ force: true }).clear();
            cy.get("#inputDniConsultor")
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Longitud maxima y caracteres numericos campo DNI validador", () => {
        //Verificamos la longitud maxima del DNI Validador sea de 8 caracteres
        cy.get("#inputValidador")
            .click({ force: true })
            .type("123456789")
            .should("have.value", "12345678");
        // No se permite caracteres NO numericos

        const U_No_Permitidos = [
            "sdafsdfsdfsd",
            "sdflfdbvcbv@",
            "**********",
            "///////",
            "$$$$$$$$",
            "::::::::::",
            ".........",
            "------------",
            '""""""""',
            "'''''''''",
            "%%%%%%%%%%",
            "(()))()))",
            "???????¿¿¿¿¿¿",
            "´´´´´´´´",
            "============",
            "<<<<<>>>>>",
            ";;;;;;;;;;;",
            ",,,,,,,,,",
            "@@@☺☻♥♠○◘♣•",
            "[[[[]]]]",
            "          ",
        ];
        //Interamos el arreglo de caracteres no permitodos y verificamos que sea vacio

        for (let i = 0; i <= U_No_Permitidos.length - 1; i++) {
            cy.get("#inputValidador").click({ force: true }).clear();
            cy.get("#inputValidador")
                .click({ force: true })
                .type(U_No_Permitidos[i])
                .should("have.value", "");
        }
    });

    it("Dni Validador no ingresado", () => {
        //Realizamos Click en el boton mejor huella
        cy.get("#mejorHuella").click({ force: true });
        cy.wait(1000);
        // Verificamos el mensaje de DNI Validador
        cy.get("#dniValidadorlHelp")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Ingrese un DNI validador");
            });
    });
});
