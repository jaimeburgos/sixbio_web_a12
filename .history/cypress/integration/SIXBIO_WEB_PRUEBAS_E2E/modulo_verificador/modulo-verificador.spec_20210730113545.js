/// <reference types="cypress" />

context("Pruebas Modulo Verificador", () => {
    const userVer = Cypress.env("userVer");
    const passVer = Cypress.env("passVer");
    const userSinData = Cypress.env("userSinData");
    const sixbioVisualizar = Cypress.env("sixbioVisualizar");
    const userData = Cypress.env("userData");
    const url = Cypress.env("url");

    beforeEach(() => {
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userVer)
            .should("have.value", userVer);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passVer)
            .should("have.value", passVer);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
        }).as("listarusuarios");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);
    });

    it("Logeo Exitoso usuario verificador", () => {
        //Verificamos que nos encontremos en la pantalla verificacion Biometrica
        cy.url().should("include", "/home/verificacionBiometrica");
    });

    it.only("Pantalla Verificación Datos o Foto (Según variable sixbioVisualizar)", () => {
        // Verificamos el nombre de los label y tabla al ingresar a la pantalla de Verificacion
        cy.get("label[for=inputDniConsultor]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Ingrese DNI");
            });

        cy.get("label[for=dedoDerecho]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Dedo Derecho");
            });

        cy.get("label[for=dedoIzquierdo]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Dedo Izquierdo");
            });

        cy.get("label[for=ambosDedos]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Ambos Dedos");
            });

        //Verificamos el check por default de HuellaDerecha para visualizar tabla de huella derecha , nist y titulo Indice Derecha

        cy.get("#tituloDeTipoHuellaDerecha")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Indice Derecho");
            });

        cy.get("#nistDerecha")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nist");
            });

        cy.get("#huellaVivaDerecha")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Huella  Viva");
            });

        //Realizamos click en el check HuellaIzquierda para visualizar tabla de huella Izquierda , nist y titulo Indice Izquierdo
        cy.get("label[for=dedoIzquierdo]").click({ force: true });
        cy.wait(1000);

        cy.get("#tituloDeTipoHuellaIzquierdo")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Indice Izquierdo");
            });

        cy.get("#nistIzquierda")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nist");
            });

        cy.get("#huellaVivaIzquierda")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Huella  Viva");
            });

        // Realizamos click en el check AmbosDedos esperando visualizar ambas tablas para huella derecha e izquieda
        cy.get("label[for=ambosDedos]").click({ force: true });
        cy.wait(1000);
        cy.get("#tituloDeTipoHuellaIzquierdo")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Indice Izquierdo");
            });

        cy.get("#nistIzquierda")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nist");
            });

        cy.get("#huellaVivaIzquierda")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Huella  Viva");
            });

        cy.get("#tituloDeTipoHuellaDerecha")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Indice Derecho");
            });

        cy.get("#nistDerecha")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nist");
            });

        cy.get("#huellaVivaDerecha")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Huella  Viva");
            });
        // Realizamos click en el check dedoDerecho para verificar su recuadro y nist
        cy.get("label[for=dedoDerecho]").click({ force: true });
        cy.wait(1000);

        cy.get("#tituloDeTipoHuellaDerecha")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Indice Derecho");
            });

        cy.get("#nistDerecha")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nist");
            });

        cy.get("#huellaVivaDerecha")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Huella  Viva");
            });

        if (sixbioVisualizar) {
            // Verificamos nombres de los botones para el caso  sixbioVisualizar=true
            cy.get("#mejorHuella")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Mejor Huella");
                });

            cy.get("#capturar")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Capturar");
                });

            cy.get("#verificar")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Verificar");
                });

            cy.get("#verificarmoc")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Verificación MOC");
                });

            cy.get("#datos")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Datos");
                });

            cy.get("#limpiar")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Limpiar");
                });
            // Verificamos Seccion Datos Persona , el nombre los label para sixbioVisualizar = true   
            cy.get("#codigoRespuesta")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Código Respuesta");
                });

            cy.get("#dni")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Número DNI");
                });

            cy.get("#apellidos")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Apellidos");
                });

            cy.get("#nombres")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Nombres");
                });

            cy.get("#caducidad")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Fecha de Caducidad");
                });

            cy.get("#expedicion")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Fecha de Expedición");
                });

            cy.get("#nacimiento")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Fecha de Nacimiento");
                });

            cy.get("#estatura")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Estatura");
                });

            cy.get("#sexo")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Sexo");
                });

            cy.get("#estadoCivil")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Estado Civil");
                });

            cy.get("#direccion")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Dirección");
                });
        } else {
            // Verificamos Seccion Datos Persona , el nombre los label para sixbioVisualizar = false   
            cy.get("#codigoRespuesta")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Código Respuesta");
                });

            cy.get("#dni")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Número DNI");
                });

            cy.get("#apellidos")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Apellidos");
                });

            cy.get("#nombres")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Nombres");
                });

            cy.get("#caducidad")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Fecha de Caducidad");
                });
                // Verificamos nombre de los botones 
                cy.get("#mejorHuella")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Mejor Huella");
                });

            cy.get("#capturar")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Capturar");
                });

            cy.get("#verificar")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Verificar");
                });

            cy.get("#verificarmoc")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Verificación MOC");
                });

            cy.get("#datos")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Datos");
                });

            cy.get("#limpiar")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Limpiar");
                });

        }
    });
});
