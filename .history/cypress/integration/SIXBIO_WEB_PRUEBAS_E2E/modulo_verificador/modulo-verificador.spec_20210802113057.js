/// <reference types="cypress" />

context("Pruebas Modulo Verificador", () => {
    const userVer = Cypress.env("userVer");
    const passVer = Cypress.env("passVer");
    const dniValidadorValido = Cypress.env("dniValidadorValido");
    const dniConsultarValido = Cypress.env("dniConsultarValido");
    const url = Cypress.env("url");
    const sixbioVisualizar = Cypress.env("sixbioVisualizar");
    const dniNoRegistradoLocalmente = Cypress.env("dniNoRegistradoLocalmente");
    beforeEach(() => {
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userVer)
            .should("have.value", userVer);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passVer)
            .should("have.value", passVer);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
        }).as("listarusuarios");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);
    });
    //42728713
    it("Logeo Exitoso usuario verificador", () => {
        //Verificamos que nos encontremos en la pantalla verificacion Biometrica
        cy.url().should("include", "/home/verificacionBiometrica");
    });

    it("Pantalla Verificación Datos o Foto (Según variable sixbioVisualizar)", () => {
        // Verificamos el nombre de los label y tabla al ingresar a la pantalla de Verificacion
        cy.get("label[for=inputDniConsultor]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Ingrese DNI");
            });

        cy.get("label[for=dedoDerecho]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Dedo Derecho");
            });

        cy.get("label[for=dedoIzquierdo]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Dedo Izquierdo");
            });

        cy.get("label[for=ambosDedos]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Ambos Dedos");
            });

        // Verificamos el placeholder de los DNIConsultor y DNIValidador
        cy.get("input[name=inputDniConsultor]")
            .invoke("attr", "placeholder")
            .should("contain", "DNI a Consultar");

        cy.get("#inputValidador")
            .invoke("attr", "placeholder")
            .should("contain", "DNI validador");

        //Verificamos el check por default de HuellaDerecha para visualizar tabla de huella derecha , nist y titulo Indice Derecha
        cy.get("#tituloDeTipoHuellaDerecha")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Indice Derecho");
            });

        cy.get("#nistDerecha")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nist");
            });

        cy.get("#huellaVivaDerecha")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Huella  Viva");
            });

        //Realizamos click en el check HuellaIzquierda para visualizar tabla de huella Izquierda , nist y titulo Indice Izquierdo
        cy.get("label[for=dedoIzquierdo]").click({ force: true });
        cy.wait(1000);

        cy.get("#tituloDeTipoHuellaIzquierdo")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Indice Izquierdo");
            });

        cy.get("#nistIzquierda")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nist");
            });

        cy.get("#huellaVivaIzquierda")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Huella  Viva");
            });

        // Realizamos click en el check AmbosDedos esperando visualizar ambas tablas para huella derecha e izquieda
        cy.get("label[for=ambosDedos]").click({ force: true });
        cy.wait(1000);
        cy.get("#tituloDeTipoHuellaIzquierdo")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Indice Izquierdo");
            });

        cy.get("#nistIzquierda")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nist");
            });

        cy.get("#huellaVivaIzquierda")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Huella  Viva");
            });

        cy.get("#tituloDeTipoHuellaDerecha")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Indice Derecho");
            });

        cy.get("#nistDerecha")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nist");
            });

        cy.get("#huellaVivaDerecha")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Huella  Viva");
            });
        // Realizamos click en el check dedoDerecho para verificar su recuadro y nist
        cy.get("label[for=dedoDerecho]").click({ force: true });
        cy.wait(1000);

        cy.get("#tituloDeTipoHuellaDerecha")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Indice Derecho");
            });

        cy.get("#nistDerecha")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nist");
            });

        cy.get("#huellaVivaDerecha")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Huella  Viva");
            });

        if (sixbioVisualizar) {
            // Verificamos nombres de los botones para el caso  sixbioVisualizar=true
            cy.get("#mejorHuella")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Mejor Huella");
                });

            cy.get("#capturar")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Capturar");
                });

            cy.get("#verificar")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Verificar");
                });

            //Verificamos el boton verificar este deshabilitado
            cy.get("#verificar").should("be.disabled");

            cy.get("#verificarmoc")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Verificación MOC");
                });

            //Verificamos el boton verificarmoc este deshabilitado
            cy.get("#verificarmoc").should("be.disabled");

            cy.get("#datos")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Datos");
                });

            //Verificamos el boton datos este deshabilitado
            cy.get("#datos").should("be.disabled");

            cy.get("#limpiar")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Limpiar");
                });
            // Verificamos Seccion Datos Persona , el nombre los label para sixbioVisualizar = true
            cy.get("#codigoRespuesta")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Código Respuesta");
                });

            cy.get("#dni")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Número DNI");
                });

            cy.get("#apellidos")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Apellidos");
                });

            cy.get("#nombres")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Nombres");
                });

            cy.get("#caducidad")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Fecha de Caducidad");
                });

            cy.get("#expedicion")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Fecha de Expedición");
                });

            cy.get("#nacimiento")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Fecha de Nacimiento");
                });

            cy.get("#estatura")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Estatura");
                });

            cy.get("#sexo")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Sexo");
                });

            cy.get("#estadoCivil")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Estado Civil");
                });

            cy.get("#direccion")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Dirección");
                });
        } else {
            // Verificamos Seccion Datos Persona , el nombre los label para sixbioVisualizar = false
            cy.get("#codigoRespuesta")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Código Respuesta");
                });

            cy.get("#dni")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Número DNI");
                });

            cy.get("#apellidos")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Apellidos");
                });

            cy.get("#nombres")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Nombres");
                });

            cy.get("#caducidad")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Fecha de Caducidad");
                });
            // Verificamos nombre de los botones
            cy.get("#mejorHuella")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Mejor Huella");
                });

            cy.get("#capturar")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Capturar");
                });

            cy.get("#verificar")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Verificar");
                });
            //Verificamos el boton verificar este deshabilitado
            cy.get("#verificar").should("be.disabled");

            cy.get("#verificarmoc")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Verificación MOC");
                });

            //Verificamos el boton verificarMOC este deshabilitado
            cy.get("#verificarmoc").should("be.disabled");

            cy.get("#foto")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Foto");
                });

            //Verificamos el boton Foto este deshabilitado
            cy.get("#foto").should("be.disabled");

            cy.get("#limpiar")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Limpiar");
                });
        }
    });

    it("Dni incorrecto", () => {
        // Escribimos un DNI Validador cualquier
        cy.get("#inputValidador").click({ force: true }).type("12345678");
        //Escribimos un DNI a consultar incorrecto
        cy.get("#inputDniConsultor").click({ force: true }).type("123");

        //Realizamos Click en el boton mejor huella
        cy.get("#mejorHuella").click({ force: true });

        // Verificamos el mensaje Ingrese un DNI a consultar
        cy.get("#msgDNIConsultar")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("DNI a consultar incorrecto");
            });
    });

    it("Agente Biometrico inactivo", () => {
        //Realizamos Click en el boton mejor huella
        cy.get("#capturar").click({ force: true });
        cy.wait(7000);
        // Verificamos el mensaje Ingrese un DNI a consultar
        cy.get("div[role=alert]")
            .contains("procesos")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal(
                    "Verifique accesibilidad a los procesos"
                );
            });
    });

    it("Mejor Huella- Ok  (Mockeado)", () => {
        // Escribimos un DNI Validador cualquier
        cy.get("#inputValidador")
            .click({ force: true })
            .type(dniValidadorValido);
        //Escribimos un DNI a consultar incorrecto
        cy.get("#inputDniConsultor")
            .click({ force: true })
            .type(dniConsultarValido);
        // Interceptamos la llamada al servicio  consultamejorhuella y Mockeamos su Response
        cy.intercept(
            "POST",
            url + "SIXBIO-webcore-Nova/sixbio/consultamejorhuella",
            {
                codigoRespuesta: "00000",
                mensajeRespuesta: "OPERACIÓN REALIZADA SATISFACTORIAMENTE",
                descripcionHuellaDerecha: "MOCK 1",
                descripcionHuellaIzquierda: "MOCK 2",
                codigoHuellaDerecha: "0104",
                codigoHuellaIzquierda: "0204",
            }
        ).as("consultamejorhuella");
        //Realizamos Click en el boton mejor huella
        cy.get("#mejorHuella").click({ force: true });
        // Esparamos que llame nuestro servicio consultamejorhuella
        cy.wait("@consultamejorhuella");
        //Verificamos el div etiquetado con msgResService que contenga el texto indicado
        cy.get("#msgResService")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal(
                    "CONSULTA DE MEJOR HUELLA REALIZADA : MOCK 1, MOCK 2"
                );
            });
    });

    it("Mejor Huella- Ok", () => {
        // Escribimos un DNI Validador cualquier
        cy.get("#inputValidador")
            .click({ force: true })
            .type(dniValidadorValido);
        //Escribimos un DNI a consultar incorrecto
        cy.get("#inputDniConsultor")
            .click({ force: true })
            .type(dniConsultarValido);
        // Interceptamos la llamada al servicio  consultamejorhuella
        cy.intercept(
            "POST",
            url + "SIXBIO-webcore-Nova/sixbio/consultamejorhuella"
        ).as("consultamejorhuella");
        //Realizamos Click en el boton mejor huella
        cy.get("#mejorHuella").click({ force: true });
        // Esparamos que llame nuestro servicio consultamejorhuella
        cy.wait("@consultamejorhuella");
        //Verificamos el div etiquetado con msgResService que contenga el texto indicado
        cy.get("#msgResService")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal(
                    "CONSULTA DE MEJOR HUELLA REALIZADA : Pulgar Derecho, Pulgar Izquierdo"
                );
            });
    });

    it("Usuario no registrado localmente", () => {
        // Escribimos un DNI Validador cualquier
        cy.get("#inputValidador")
            .click({ force: true })
            .type(dniValidadorValido);
        //Escribimos un DNI a consultar NO REGISTRADO
        cy.get("#inputDniConsultor")
            .click({ force: true })
            .type(dniNoRegistradoLocalmente);
        // Interceptamos la llamada al servicio  consultamejorhuella
        cy.intercept(
            "POST",
            url + "SIXBIO-webcore-Nova/sixbio/consultamejorhuella"
        ).as("consultamejorhuella");
        //Realizamos Click en el boton mejor huella
        cy.get("#mejorHuella").click({ force: true });
        // Esparamos que llame nuestro servicio consultamejorhuella
        cy.wait("@consultamejorhuella");
        //Verificamos el div etiquetado con msgResService que contenga el texto indicado "Persona no registrada localmente"
        cy.get("#msgResService")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal(
                    "Persona no registrada localmente"
                );
            });
    });

    it("Test lectura LOG", () => {
        cy.readFile(
            "C:/SIXLOG/SIXBIO/SIXBIO-WEBCORE-NOVA/SIXBIOWBTR20210802.log",
            "utf8"
        ).then((text) => {
            expect(text).contains(
                "Identificador de sesion generados correctamente [16xv-Jz5ilXnamqe9258OD6_1oybRC0DK7GRz5Tk, 000000202108021103455010151]"
            );
        });
    });

    it("Test lectura LOG 2 ", () => {
        cy.readFile(
            "C:/SIXLOG/SIXBIO/SIXBIO-WEBCORE-NOVA/SIXBIOWBTR20210802.log",
            "utf8"
        ).then((text) => {
            expect(text).contains(
                "T 02 I lun ago 02 11:01:39.498 2021     WEBCORE      01.00.00      14900         default task-1 000000       sixbio-webcore-novatronic-000000               com.novatronic.sixbio.webcore.controller.state.imp.DisconnectState               signOn    57 [JRLPO03EVOAZNSYL] - [00000, OPERACIÓN REALIZADA SATISFACTORIAMENTE]"
            );
        });
    });

    it("Test lectura LOG 3 ", () => {
        cy.readFile(
            "C:/SIXLOG/SIXBIO/SIXBIO-WEBCORE-NOVA/SIXBIOWBTR20210802.log",
            "utf8"
        ).then((text) => {
            expect(text).contains(
                "T 02 I lun ago 02 09:41:34.047 2021     WEBCORE      01.00.00      14900         default task-2 000000       sixbio-webcore-novatronic-000000                                    com.novatronic.api.sca.ManejadorAdministrador             getRoles   100 Consultando roles , parámetros : {TIPO=, TIPO_ROL=, USUARIO=VerUser, ROL=, APLICACION=, PAGINA=1}"
            );
        });
    });

    it("Test lectura LOG 4  ", () => {
        cy.readFile(
            "C:/SIXLOG/SIXBIO/SIXBIO-WEBCORE-NOVA/SIXBIOWBTR20210802.log",
            "utf8"
        ).then((text) => {
            expect(text).contains(
                "Identificador de sesion generados correctamente [16xv-Jz5ilXnamqe9258OD6_1oybRC0DK7GRz5Tk, 000000202108021103455010151]"
            );
        });
    });

    it("Test lectura LOG 5  ", () => {
        cy.readFile(
            "C:/SIXLOG/SIXBIO/SIXBIO-WEBCORE-NOVA/SIXBIOWBTR20210802.log",
            "utf8"
        ).then((text) => {
            expect(text).contains(
                "T 02 I lun ago 02 09:42:06.642 2021     WEBCORE      01.00.00      14900         default task-2 000000       sixbio-webcore-novatronic-000000               com.novatronic.sixbio.webcore.controller.state.imp.DisconnectState            signOnSCA    92 [QLU5OQQSYTQWIX6U] - Procesando : SignOnSCA "
            );
        });
    });

    it("Test lectura LOG 6  ", () => {
        cy.readFile(
            "C:/SIXLOG/SIXBIO/SIXBIO-WEBCORE-NOVA/SIXBIOWBTR20210802.log",
            "utf8"
        ).then((text) => {
            expect(text).contains(
                "T 02 I lun ago 02 09:41:44.196 2021     WEBCORE      01.00.00      14900         default task-1 000000       sixbio-webcore-novatronic-000000                  com.novatronic.sixbio.webcore.controller.state.imp.ConnectState listarRolesAgrupador   272 [YJXM3M6VPGZHJIDE] - [0000, OPERACION REALIZADA SATISFACTORIAMENTE]"
            );
        });
    });

    it("Test lectura LOG 7  ", () => {
        cy.readFile(
            "C:/SIXLOG/SIXBIO/SIXBIO-WEBCORE-NOVA/SIXBIOWBTR20210802.log",
            "utf8"
        ).then((text) => {
            expect(text).contains(
                "Identificador de sesion generados correctamente [16xv-Jz5ilXnamqe9258OD6_1oybRC0DK7GRz5Tk, 000000202108021103455010151]"
            );
        });
    });

    it("Test lectura LOG 8  ", () => {
        cy.readFile(
            "C:/SIXLOG/SIXBIO/SIXBIO-WEBCORE-NOVA/SIXBIOWBTR20210802.log",
            "utf8"
        ).then((text) => {
            expect(text).contains(
                "T 02 I lun ago 02 09:41:34.666 2021     WEBCORE      01.00.00      14900         default task-2 000000       sixbio-webcore-novatronic-000000                  com.novatronic.sixbio.webcore.controller.state.imp.ConnectState listarRolesAgrupador   265 [VQB51NQETWIYPWAC] - Procesando : Listar Roles Agrupador"
            );
        });
    });

    it("Test lectura LOG 9  ", () => {
        cy.readFile(
            "C:/SIXLOG/SIXBIO/SIXBIO-WEBCORE-NOVA/SIXBIOWBTR20210802.log",
            "utf8"
        ).then((text) => {
            expect(text).contains(
                "Identificador de sesion generados correctamente [16xv-Jz5ilXnamqe9258OD6_1oybRC0DK7GRz5Tk, 000000202108021103455010151]"
            );
        });
    });
});
