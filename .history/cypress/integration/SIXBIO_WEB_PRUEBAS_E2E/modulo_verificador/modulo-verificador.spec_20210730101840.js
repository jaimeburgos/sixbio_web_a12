/// <reference types="cypress" />

context("Pruebas Modulo Verificador", () => {
    const userVer = Cypress.env("userVer");
    const passVer = Cypress.env("passVer");
    const userSinData = Cypress.env("userSinData");
    const sixbioVisualizar = Cypress.env("sixbioVisualizar");
    const userData = Cypress.env("userData");
    const url = Cypress.env("url");

    beforeEach(() => {
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userVer)
            .should("have.value", userVer);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passVer)
            .should("have.value", passVer);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
        }).as("listarusuarios");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);
    });

    it("Logeo Exitoso usuario verificador", () => {
        //Verificamos que nos encontremos en la pantalla verificacion Biometrica
        cy.url().should("include", "/home/verificacionBiometrica");
    });

    it("Pantalla Verificación Datos o Foto (Según variable sixbioVisualizar)", () => {
        // Verificamos nombres de los botones para el caso  sixbioVisualizar=true
        if (sixbioVisualizar) {
            cy.get("#mejorHuella")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Mejor Huella");
                });

            cy.get("#capturar")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Capturar");
                });
            cy.get("#verificar")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Verificar");
                });
            cy.get("#verificarMoc")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Verificación MOC");
                });

            cy.get("#datos")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Datos");
                });

            cy.get("#limpiar")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Limpiar");
                });
        }
    });
});
