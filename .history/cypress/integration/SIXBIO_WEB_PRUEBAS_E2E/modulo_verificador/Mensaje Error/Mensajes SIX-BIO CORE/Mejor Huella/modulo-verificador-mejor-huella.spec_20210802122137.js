/// <reference types="cypress" />

context(
    "Pruebas Modulo Verificador Mensaje Error SIXBIO CORE Mejor Huella",
    () => {
        const userVer = Cypress.env("userVer");
        const passVer = Cypress.env("passVer");
        const dniValidadorValido = Cypress.env("dniValidadorValido");
        const dniConsultarValido = Cypress.env("dniConsultarValido");
        const url = Cypress.env("url");
        const sixbioVisualizar = Cypress.env("sixbioVisualizar");
        const dniNoRegistradoLocalmente = Cypress.env(
            "dniNoRegistradoLocalmente"
        );
        beforeEach(() => {
            cy.visit(url);
            cy.get('input[formControlName="usuario"]')
                .click({ force: true })
                .clear();
            cy.get('input[formControlName="pass"]')
                .click({ force: true })
                .clear();

            cy.get('input[formControlName="usuario"]')
                .click({ force: true })
                .type(userVer)
                .should("have.value", userVer);

            cy.get('[formControlName="pass"]')
                .click({ force: true })
                .type(passVer)
                .should("have.value", passVer);

            cy.get('button[name="submit"]').click({ force: true });

            cy.intercept({
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            }).as("listarrolesagrupador");

            cy.intercept({
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            }).as("listarusuarios");

            cy.wait(["@listarrolesagrupador", "@listarusuarios"]);
        });
        //42728713

        it("Error de conexion Reniec  (Mockeado)", () => {
            // Escribimos un DNI Validador cualquier
            cy.get("#inputValidador")
                .click({ force: true })
                .type(dniValidadorValido);
            //Escribimos un DNI a consultar incorrecto
            cy.get("#inputDniConsultor")
                .click({ force: true })
                .type(dniConsultarValido);
            // Interceptamos la llamada al servicio  consultamejorhuella y Mockeamos su Response
            cy.intercept(
                "POST",
                url + "SIXBIO-webcore-Nova/sixbio/consultamejorhuella",
                {
                    codigoRespuesta: "20001",
                    mensajeRespuesta: "ERROR DE CONEXION CON RENIEC",
                    // descripcionHuellaDerecha: "MOCK 1",
                    //   descripcionHuellaIzquierda: "MOCK 2",
                    //     codigoHuellaDerecha: "0104",
                    //     codigoHuellaIzquierda: "0204",
                }
            ).as("consultamejorhuella");
            //Realizamos Click en el boton mejor huella
            cy.get("#mejorHuella").click({ force: true });
            // Esparamos que llame nuestro servicio consultamejorhuella
            cy.wait("@consultamejorhuella");
            //Verificamos el div etiquetado con msgResService que contenga el texto indicado
            cy.get("#msgResService")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal(
                        "ERROR DE CONEXION CON RENIEC"
                    );
                });
        });

        it.only("No hay conectividad con SIX/BIO CORE  (Mockeado)", () => {
            // Escribimos un DNI Validador cualquier
            cy.get("#inputValidador")
                .click({ force: true })
                .type(dniValidadorValido);
            //Escribimos un DNI a consultar incorrecto
            cy.get("#inputDniConsultor")
                .click({ force: true })
                .type(dniConsultarValido);
            // Interceptamos la llamada al servicio  consultamejorhuella y Mockeamos su Response
            cy.intercept(
                "POST",
                url + "SIXBIO-webcore-Nova/sixbio/consultamejorhuella",
                {
                    codigoRespuesta: "40002",
                    mensajeRespuesta: "Error de conexion al servidor SIXBIO",
                    // descripcionHuellaDerecha: "MOCK 1",
                    //   descripcionHuellaIzquierda: "MOCK 2",
                    //     codigoHuellaDerecha: "0104",
                    //     codigoHuellaIzquierda: "0204",
                }
            ).as("consultamejorhuella");
            //Realizamos Click en el boton mejor huella
            cy.get("#mejorHuella").click({ force: true });
            // Esparamos que llame nuestro servicio consultamejorhuella
            cy.wait("@consultamejorhuella");

            // Verificamos el toast de error
            cy.get("#toast-container", { timeout: 5000 })
                .find("div")
                .find("div")
                .contains("Error")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal("Error");
                });
            // Verificamos el mensaje del toast de error
            cy.get("#toast-container", { timeout: 5000 })
                .find("div")
                .find("div")
                .contains("conectividad")
                .invoke("text")
                .then((text) => {
                    expect(text).to.equal("Error de conectividad.");
                });
        });

        // 40002
        it("Usuario no registrado localmente", () => {
            // Escribimos un DNI Validador cualquier
            cy.get("#inputValidador")
                .click({ force: true })
                .type(dniValidadorValido);
            //Escribimos un DNI a consultar NO REGISTRADO
            cy.get("#inputDniConsultor")
                .click({ force: true })
                .type(dniNoRegistradoLocalmente);
            // Interceptamos la llamada al servicio  consultamejorhuella
            cy.intercept(
                "POST",
                url + "SIXBIO-webcore-Nova/sixbio/consultamejorhuella"
            ).as("consultamejorhuella");
            //Realizamos Click en el boton mejor huella
            cy.get("#mejorHuella").click({ force: true });
            // Esparamos que llame nuestro servicio consultamejorhuella
            cy.wait("@consultamejorhuella");
            //Verificamos el div etiquetado con msgResService que contenga el texto indicado "Persona no registrada localmente"
            cy.get("#msgResService")
                .invoke("text")
                .then((text) => {
                    expect(text.trim()).to.equal(
                        "Persona no registrada localmente"
                    );
                });
        });
        //Error de conexion Reniec
        /*  it("Test lectura LOG", () => {
        cy.readFile(
            "C:/SIXLOG/SIXBIO/SIXBIO-WEBCORE-NOVA/SIXBIOWBTR20210802.log",
            "utf8"
        ).then((text) => {
            expect(text).contains(
                "Identificador de sesion generados correctamente [16xv-Jz5ilXnamqe9258OD6_1oybRC0DK7GRz5Tk, 000000202108021103455010151]"
            );
        });
    });*/
    }
);
