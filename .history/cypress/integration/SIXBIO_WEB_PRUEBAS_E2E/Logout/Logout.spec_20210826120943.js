/// <reference types="cypress" />

context("Logout Pruebas Funcionalidad", () => {
    // Asignamos los valores de nuestras variables globales del archivo cypress.json
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const userBloq = Cypress.env("userBloq");
    const passBloq = Cypress.env("passBloq");
    const userVer = Cypress.env("userVer");
    const passVer = Cypress.env("passVer");
    const userReset = Cypress.env("userReset");
    const passReset = Cypress.env("passReset");
    const url = Cypress.env("url");

    //El beforeEach Ejecuta antes de cada caso
    beforeEach(() => {
        // Visitamos la URL BASE y seleccionamos los campos
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();
    });

    it("Cierre de sesion a demanda", () => {
        //Ingresamos con nuestro usuario administrador
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);
        //Realizamos click en el boton entrar

        cy.get('button[name="submit"]').click({ force: true });
        // Interceptamos nuestro servicio listarrolesagrupador
        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");
        cy.wait("@listarrolesagrupador");
        cy.url().should("include", "/home/gestion-usuarios");
        // Realizamos click en el boton cerrar sesion
        cy.get("#changepassword").click({ force: true });

        cy.wait(2000);
        // interceptamos el servicio signoff
        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixbio/signoff",
        }).as("signoff");
        // Realizamos click en el boton confirmar para cerrar nuestra sesion actual
        cy.get('button[class="btn btn-success anchoCienPorCiento"]')
            .contains("Confirmar")
            .click({ force: true });
        //Esperamos cargue el sigonff
        cy.wait("@signoff");
        // Verificamos que nos encontremos en la pantalla de login
        cy.url().should("include", "/login");
    });


    it.only("Cancelar cierre de sesión", () => {
        //Ingresamos con nuestro usuario administrador
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);
        //Realizamos click en el boton entrar

        cy.get('button[name="submit"]').click({ force: true });
        // Interceptamos nuestro servicio listarrolesagrupador
        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");
        cy.wait("@listarrolesagrupador");
        cy.url().should("include", "/home/gestion-usuarios");
        // Realizamos click en el boton cerrar sesion
        cy.get("#changepassword").click({ force: true });

        cy.wait(2000);
        // interceptamos el servicio signoff
        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixbio/signoff",
        }).as("signoff");
        // Realizamos click en el boton confirmar para cerrar nuestra sesion actual
        cy.get('button[class="btn btn-success anchoCienPorCiento"]')
            .contains("Cerrar")
            .click({ force: true });
      
       
        // Verificamos que nos encontremos en la pantalla de login
        cy.url().should("include", "/home/gestion-usuarios");
    });



});
