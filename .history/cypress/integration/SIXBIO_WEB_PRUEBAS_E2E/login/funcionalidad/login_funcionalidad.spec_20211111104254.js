/// <reference types="cypress" />

context("Login Pruebas Funcionalidad", () => {
    // Asignamos los valores de nuestras variables globales del archivo cypress.json
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const userBloq = Cypress.env("userBloq");
    const passBloq = Cypress.env("passBloq");
    const userVer = Cypress.env("userVer");
    const passVer = Cypress.env("passVer");
    const userReset = Cypress.env("userReset");
    const passReset = Cypress.env("passReset");
    const url = Cypress.env("url");

    //El beforeEach Ejecuta antes de cada caso
    beforeEach(() => {
        // Visitamos la URL BASE y seleccionamos los campos
        cy.visit('url', {
            onBeforeLoad(win) {
              Object.defineProperty(win.navigator, 'language', { value: 'es-ES' });
              Object.defineProperty(win.navigator, 'languages', { value: ['es'] });
              Object.defineProperty(win.navigator, 'accept_languages', { value: ['es'] });
            },
            headers: {
              'Accept-Language': 'de',
            },
        });
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();
    });

    it("Primer Login", () => {
        //Ingresamos nuestras credenciales de primer logeo
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);
        //Interceptamos nuestro service signon
        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");
        //Realizamos click en el boton entrar
        cy.get('button[name="submit"]').click({ force: true });
        //Esperamos la llamada y carga del servicio sigon
        cy.wait("@signon");

        //Verificamos el mensaje de primer login
        cy.get("#msgModalLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "Primer Login, se le sugiere realizar el cambio de contraseña."
                );
            });
        //Realizamos click en el boton confirmar
        cy.get("div")
            .find("button")
            .contains("Confirmar")
            .click({ force: true });
        //Intercetapmos el servicio consultardatoscambioclave
        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/consultardatoscambioclave",
        }).as("consultardatoscambioclave");
        cy.wait("@consultardatoscambioclave");
        //Verificamos que nos encontremos en la pantalla cambio de contraseña
        cy.url().should("include", "/firstLogin");
        //Verificamos en el localstorage que el item isFirstLogin este seteado con valor true
        cy.url().should(() => {
            expect(localStorage.getItem("isFirstLogin")).to.eq("true");
        });
    });

    it("Primer Login al sistema - Cancelar", () => {
        // Escribimos nuestras credenciales de usuario de primer login
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userReset)
            .should("have.value", userReset);

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type(passReset)
            .should("have.value", passReset);
        // interceptamos la llamada al servicio signon
        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/service/signon",
        }).as("signon");
        // Realizamos click en entrar al sistema
        cy.get('button[name="submit"]').click({ force: true });
        cy.wait("@signon");

        // Verificamos el mensaje del modal de primer login
        cy.get("#msgModalLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "Primer Login, se le sugiere realizar el cambio de contraseña."
                );
            });
        // realizamos click en el boton cerrar
        cy.get("div").find("button").contains("Cerrar").click({ force: true });
        // Verificamos que nos redirige a la pantalla login
        cy.url().should("include", "/login");
    });
 
    it("Datos incompletos", () => {
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");
        //Validamos el mensaje de tamaño minimo de caracteres para campo usuario escribiendo la cantidad inferior a 5
        cy.get("#msgMinLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 5 caracteres");
            });

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        //Validamos el mensaje de ingrese usuario sin escribir texto en el campo usuario
        cy.get("#msgReqLogin")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese usuario");
            });
        cy.get("button[name=submit]").should("be.disabled");

        cy.get('input[formControlName="pass"]')
            .click({ force: true })
            .type("123")
            .should("have.value", "123");

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        //Validamos el mensaje de tamaño minimo de caracteres para campo contraseña escribiendo la cantidad inferior a 6

        cy.get("#msgMinPass")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 6 caracteres");
            });
        //Validamos el mensaje de ingrese contraseña sin escribir texto en el campo usuario

        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get("#msgReqPass")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese contraseña");
            });

        cy.get("button[name=submit]").should("be.disabled");
    });

    it("Usuario y/o contraseña incorrectos", () => {
        // Escribimos credenciales de manera incorrecta
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");
        //Realizamos click en el boton entrar
        cy.get('button[name="submit"]').click({ force: true });
        // Verificamos el toast Error
        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });
        // Verificamos el contenido del mensaje del toast error
        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Usuario")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "Usuario y/o Contraseña incorrecta, intente nuevamente."
                );
            });
    });

    // Este mockeo aplica a 	Error de Conectividad SIX/BIO Core ,	Error de Conectividad SIX/BIO Web Backend ,	Error de Conectividad SCA
    it("Verificamos mensaje de error de conectividad Login (Mockeo Response Vacio )", () => {
        // Mockeamos nuestro servicio sigon para que nos devuelva error conectividad dando un response vacio
        cy.intercept("POST", url + "SIXBIO-webcore-Nova/service/signon", []).as(
            "errorConectividad"
        );
        //Ingresamos nuestras credenciales
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);
        //Realizamos click en el boton entrar
        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@errorConectividad");
        // Verificamos nuestro toast error
        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });
        // Verificamos el contenido de nuestro toast error
        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("conectividad")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Error de conectividad.");
            });
    });

    it("Cierre de sesion a demanda", () => {
        //Ingresamos con nuestro usuario administrador
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);
        //Realizamos click en el boton entrar

        cy.get('button[name="submit"]').click({ force: true });
        // Interceptamos nuestro servicio listarrolesagrupador
        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");
        cy.wait("@listarrolesagrupador");
        cy.url().should("include", "/home/gestion-usuarios");
        // Realizamos click en el boton cerrar sesion
        cy.get("#changepassword").click({ force: true });

        cy.wait(2000);
        // interceptamos el servicio signoff
        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixbio/signoff",
        }).as("signoff");
        // Realizamos click en el boton confirmar para cerrar nuestra sesion actual
        cy.get('button[class="btn btn-success anchoCienPorCiento"]')
            .contains("Confirmar")
            .click({ force: true });
        //Esperamos cargue el sigonff
        cy.wait("@signoff");
        // Verificamos que nos encontremos en la pantalla de login
        cy.url().should("include", "/login");
    });
});
