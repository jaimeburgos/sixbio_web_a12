/// <reference types="cypress" />

context("Login Interfaz", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const userVer = Cypress.env("userVer");
    const passVer = Cypress.env("passVer");
    const userReset = Cypress.env("userReset");
    const passReset = Cypress.env("passReset");
    const url = Cypress.env("url");

    beforeEach(() => {
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();
    });

    it("Logeo Exitoso del Usuario Verificador", () => {
        // Validamos los nombres placeholder del login, boton y titulo
        cy.get("h1")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal(
                    "Sistema de Verificación de Identidad"
                );
            });

        cy.get("#loginButton")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Entrar");
            });
        cy.get('input[formControlName="usuario"]')
            .invoke("attr", "placeholder")
            .should("contain", "Usuario");

        cy.get('input[formControlName="pass"]')
            .invoke("attr", "placeholder")
            .should("contain", "Contraseña");

        //Ingresamos nuestras credenciales de usuario administrador
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);
        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);
        //Realizamos click en el boton entrar
        cy.get('button[name="submit"]').click({ force: true });
        //Interceptamos el servicio listarrolesagrupador
        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");
        cy.wait("@listarrolesagrupador");
        //Verificamos la entrar a la pantalla gestion de usuarios
        cy.url().should("include", "/home/gestion-usuarios");
        //Verificamos en nuestro localStorage este seteado el item isLoggedin con valor true
        cy.url().should(() => {
            expect(localStorage.getItem("isLoggedin")).to.eq("true");
        });
    });

    it("Logeo Exitoso del Usuario Consultor", () => {
        // Validamos los nombres placeholder del login, boton y titulo
        cy.get("h1")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal(
                    "Sistema de Verificación de Identidad"
                );
            });
        cy.get("#loginButton")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Entrar");
            });
        cy.get('input[formControlName="usuario"]')
            .invoke("attr", "placeholder")
            .should("contain", "Usuario");

        cy.get('input[formControlName="pass"]')
            .invoke("attr", "placeholder")
            .should("contain", "Contraseña");

        //Ingresamos nuestras credenciales de usuario Consultor
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userVer)
            .should("have.value", userVer);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passVer)
            .should("have.value", passVer);
        //Realizamos click en el boton entrar

        cy.get('button[name="submit"]').click({ force: true });
        //Interceptamos el servicio listarrolesagrupador
        /*cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");
        cy.wait("@listarrolesagrupador");*/
        //Verificamos la entrar a la pantalla verificacion Biometrica

        cy.url().should("include", "/home/verificacionBiometrica");
        //Verificamos en nuestro localStorage este seteado el item isLoggedin con valor true

        cy.url().should(() => {
            expect(localStorage.getItem("isLoggedin")).to.eq("true");
        });
    });


});
