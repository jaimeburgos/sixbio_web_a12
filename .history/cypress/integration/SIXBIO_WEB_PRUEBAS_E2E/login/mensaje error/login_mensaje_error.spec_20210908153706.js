/// <reference types="cypress" />

context("Login Pruebas Mensaje Error", () => {
    // Asignamos los valores de nuestras variables globales del archivo cypress.json
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const userBloq = Cypress.env("userBloq");
    const passBloq = Cypress.env("passBloq");
    const userVer = Cypress.env("userVer");
    const passVer = Cypress.env("passVer");
    const userReset = Cypress.env("userReset");
    const passReset = Cypress.env("passReset");
    const url = Cypress.env("url");

    //El beforeEach Ejecuta antes de cada caso
    beforeEach(() => {
        // Visitamos la URL BASE y seleccionamos los campos
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();
    });

 

    it("Usuario y/o contraseña incorrectos", () => {
        // Escribimos credenciales de manera incorrecta
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type("dfgfdgfdgfdgfd")
            .should("have.value", "dfgfdgfdgfdgfd");
        //Realizamos click en el boton entrar
        cy.get('button[name="submit"]').click({ force: true });
        // Verificamos el toast Error
        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });
        // Verificamos el contenido del mensaje del toast error
        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Usuario")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "Usuario y/o Contraseña incorrecta, intente nuevamente."
                );
            });
    });

    // Este mockeo aplica a 	Error de Conectividad SIX/BIO Core ,	Error de Conectividad SIX/BIO Web Backend ,	Error de Conectividad SCA
    it("Verificamos mensaje de error de conectividad Login (Mockeo Response Vacio )", () => {
        // Mockeamos nuestro servicio sigon para que nos devuelva error conectividad dando un response vacio
        cy.intercept("POST", url + "SIXBIO-webcore-Nova/service/signon", []).as(
            "errorConectividad"
        );
        //Ingresamos nuestras credenciales
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);
        //Realizamos click en el boton entrar
        cy.get('button[name="submit"]').click({ force: true });

        cy.wait("@errorConectividad");
        // Verificamos nuestro toast error
        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });
        // Verificamos el contenido de nuestro toast error
        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("conectividad")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Error de conectividad.");
            });
    });

});
