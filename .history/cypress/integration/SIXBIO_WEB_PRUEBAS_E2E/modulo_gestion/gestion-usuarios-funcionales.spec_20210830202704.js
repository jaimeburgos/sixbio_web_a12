/// <reference types="cypress" />

context("Pruebas Interfaz Modulo Gestion", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const url = Cypress.env("url");
    //    const userReset = Cypress.env("userReset");
    //    const passReset = Cypress.env("passReset");

    beforeEach(() => {
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            []
        );

        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
        }).as("listarusuarios");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);
    });

    it("Pantalla Busqueda Usuario", () => {
        cy.url().should("include", "/home/gestion-usuarios");
        // Verificamos los valores que deben setear en el localStorage
        cy.url().should(() => {
            expect(localStorage.getItem("isLoggedin")).to.eq("true");
            expect(localStorage.getItem("RolUsuario")).to.eq("EMP_ADM");
            expect(localStorage.getItem("usuarioLogueado")).to.eq(userAdmin);
        });
        cy.get("div .card-header")
            .invoke("text")
            .then((text) => {
                expect(text).to.contain("Gestión de Usuarios");
            });

        cy.get("button")
            .find("a")
            .contains("Nuevo")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" Nuevo");
            });

        cy.get("button")
            .find("a")
            .contains("Eliminar")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" Eliminar");
            });

        cy.get("button")
            .find("a")
            .contains("Excel")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Excel ");
            });

        cy.get("button")
            .find("a")
            .contains("PDF")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("PDF ");
            });
        cy.get("table")
            .find("thead")
            .find("tr")
            .find("th")
            .invoke("text")
            .then((text) => {
                expect(text).to.contain("Todos");
                expect(text).to.contain("Usuario");
                expect(text).to.contain("Nombre");
                expect(text).to.contain("Apellido");
                expect(text).to.contain("Restablecer clave");
                expect(text).to.contain("Operaciones");
            });

            cy.wait(1000);
    });

    it("Pantalla Crear Usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.url()
            .should("include", "/home/gestion-usuarios/crear-usuario")
            .wait(1000);

        cy.get("label[for='inputUsuario']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Usuario (*)");
            });

        cy.get("label[for='inputNombre']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nombre (*)");
            });

        cy.get("label[for='inputApellidoPaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Paterno (*)");
            });

        cy.get("label[for='inputApellidoMaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Materno (*)");
            });

        cy.get("label[for='inputTelefono']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Teléfono");
            });

        cy.get("label[for='inputEstado']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Estado");
            });

        cy.get("label[for='inputNacimiento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha de Nacimiento");
            });

        cy.get("label[for='inputTipoDocumento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Tipo de documento (*)");
            });

        cy.get("label[for='inputNroDocumento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Número de documento(*)");
            });

        cy.get("label[for='inputCorreo']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Correo (*)");
            });

        cy.get("label[for='inputRolAgrupador']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Rol Agrupador (*)");
            });

 
        cy.get("label[for=inputUsuario]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Usuario (*)");
            });

        cy.get("label[for=inputNombre]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Nombre (*)");
            });

        cy.get("label[for=inputApellidoPaterno]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Apellido Paterno (*)");
            });

        cy.get("label[for=inputApellidoMaterno]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Apellido Materno (*)");
            });

        cy.get("label[for=inputTelefono]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Teléfono");
            });

        cy.get("label[for=inputEstado]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" Estado");
            });

        cy.get("label[for=inputNacimiento]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" Fecha de Nacimiento");
            });

        cy.get("label[for=inputTipoDocumento]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tipo de documento (*)");
            });

        cy.get("label[for=inputNroDocumento]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Número de documento(*)");
            });

        cy.get("label[for=inputCorreo]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Correo (*)");
            });

        cy.get("label[for=inputRolAgrupador]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Rol Agrupador (*)");
            });

        cy.get("#inputUser")
            .invoke("attr", "placeholder")
            .should("contain", "Usuario");

        cy.get("#inputNombre")
            .invoke("attr", "placeholder")
            .should("contain", "Nombre");

        cy.get("#inputApellidoPaterno")
            .invoke("attr", "placeholder")
            .should("contain", "Apellido Paterno");

        cy.get("#inputApellidoMaterno")
            .invoke("attr", "placeholder")
            .should("contain", "Apellido Materno");

        cy.get("#inputTelefono")
            .invoke("attr", "placeholder")
            .should("contain", "Teléfono");

        cy.get("#inputTelefono")
            .invoke("attr", "placeholder")
            .should("contain", "Teléfono");

        cy.get("#inputNroDocumento")
            .invoke("attr", "placeholder")
            .should("contain", "Número de documento");

        cy.get("#inputCorreo")
            .invoke("attr", "placeholder")
            .should("contain", "email@example.com");

        cy.get("#inputRolAgrupador")
            .invoke("text")
            .then((text) => {
                expect(text).to.contain("Administrador", "Verificador");
            });
       

        cy.wait(1000);

        cy.get('button[name="submit"]')
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Guardar");
            });

        cy.get('button[name="cancel"]')
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Cancelar ");
            });
    });

    it("Pantalla Detalle Usuario", () => {
        cy.get('button[title="Ver detalle"]').first().click({ force: true });
        cy.wait(1000);
        cy.url().should("include", "/home/gestion-usuarios/detalle-usuario");

        cy.get("label[for='inputUsuario']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Usuario (*)");
            });

        cy.get("label[for='inputNombre']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nombre (*)");
            });

        cy.get("label[for='inputApellidoPaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Paterno (*)");
            });

        cy.get("label[for='inputApellidoMaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Materno (*)");
            });

        cy.get("label[for='inputTelefono']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Teléfono");
            });

        cy.get("label[for='inputEstados']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Estado (*)");
            });

        cy.get("label[for='inputFechaNacimiento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha de Nacimiento");
            });

        cy.get("label[for='inputTipoDocumentos']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Tipo de documento (*)");
            });

        cy.get("label[for='inputNroDocumento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Número de documento(*)");
            });

        cy.get("label[for='inputCorreo']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Correo (*)");
            });

        cy.get("label[for='inputRolAgrupador']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Rol Agrupador (*)");
            });

        cy.get("button[type=button]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Regresar");
            });

        cy.wait(1000);
        cy.get("#inputUsuario").should("be.disabled");
        cy.get("#inputNombre").should("be.disabled");
        cy.get("#inputApellidoPaterno").should("be.disabled");
        cy.get("#inputApellidoMaterno").should("be.disabled");
        cy.get("#inputTelefono").should("be.disabled");
        cy.get("#inputEstados").should("be.disabled");
        cy.get("input[name=inputFechaNacimiento]").should("be.disabled");
        cy.get("#inputTipoDocumentos").should("be.disabled");
        cy.get("#inputNroDocumento").should("be.disabled");
        cy.get("#inputCorreo").should("be.disabled");
        cy.get("#inputRolAgrupador").should("be.disabled");
    });

    it("Verificamos el retorno de crear-usuario a gestion usuario", () => {
        cy.url().should("include", "/home/gestion-usuarios");
        cy.get("button").find("a").contains("Nuevo").click({ force: true });
        cy.url().should("include", "/home/gestion-usuarios/crear-usuario");
        cy.wait(1500);
        cy.get("button").find("a").contains("Cancelar").click({ force: true });
        cy.url().should("include", "/home/gestion-usuarios");
    });
 
    it("Pantalla Editar Usuario", () => {
        cy.get('button[title="Editar"]').first().click({ force: true });
        cy.wait(1000);

        cy.get("label[for=inputUsuario]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Usuario (*)");
            });

        cy.get("label[for=inputNombre]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nombre (*)");
            });

        cy.get("label[for=inputApellidoPaterno]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Paterno (*)");
            });

        cy.get("label[for=inputApellidoMaterno]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Materno (*)");
            });

        cy.get("label[for=inputTelefono]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Teléfono");
            });

        cy.get("label[for=inputEstado]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Estado (*)");
            });

        cy.get("label[for=inputNacimiento]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha de Nacimiento (*)");
            });

        cy.get("label[for=inputTipoDocumento]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Tipo de documento (*)");
            });

        cy.get("label[for=inputNroDocumento]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Número de documento(*)");
            });

        cy.get("label[for=inputCorreo]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Correo (*)");
            });

        cy.get("label[for=inputRolAgrupador]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Rol Agrupador (*)");
            });
    });
});
