/// <reference types="cypress" />

context("Pruebas Funcionales Editar Usuario", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const email = Cypress.env("email");
    const url = Cypress.env("url");
    
    beforeEach(() => {
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            []
        );

        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
        }).as("listarusuarios");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);
    });

    it("Editar Usuario - Ok", () => {
        cy.get('button[title="Editar"]').first().click({ force: true });

        cy.wait(1000);
        const userRandom = generateRandomString(11).replace(".", "");

        cy.get('input[formControlName="nombre"]')
            .click({ force: true })
            .type(generateRandomString(11));

        cy.get('input[formControlName="apPaterno"]')
            .click({ force: true })
            .type(generateRandomString(11));

        cy.get('input[formControlName="apMaterno"]')
            .click({ force: true })
            .type(generateRandomString(11));

        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .clear();

        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .type("jburgos@novatronic.com");

        cy.get('button[type="submit"]').click({ force: true });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("satisfactoriamente")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal(
                    "Operación realizada satisfactoriamente."
                );
            });
    });

    it("Editar Usuario - Cancelar", () => {
        cy.get('button[title="Editar"]').first().click({ force: true });
        cy.wait(1000);
        cy.get('input[formControlName="nombre"]')
            .click({ force: true })
            .clear()
            .type(generateRandomString(11));

        cy.get('input[formControlName="apPaterno"]')
            .click({ force: true })
            .clear()
            .type(generateRandomString(11));

        cy.get('input[formControlName="apMaterno"]')
            .click({ force: true })
            .clear()
            .type(generateRandomString(11));

        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .clear();

        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .type(email);

        cy.get('button[name="cancel"]').click({ force: true });

        cy.url().should("include", "/home/gestion-usuarios");

    });

    it("Editar usuario, campos inválidos", () => {
        cy.get('button[title="Editar"]').first().click({ force: true });
        cy.wait(1000);

        cy.get('input[formControlName="nombre"]')
            .click({ force: true })
            .clear();

        cy.get('input[formControlName="apPaterno"]')
            .click({ force: true })
            .clear();

        cy.get('input[formControlName="apMaterno"]')
            .click({ force: true })
            .clear();

        cy.get('input[formControlName="telef"]').click({ force: true }).clear();

        cy.get('select[formControlName="estado"]').select("Seleccione...", {
            force: true,
        });

        cy.get('input[formControlName="numDoc"]').click({ force: true });

        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .clear();

        cy.get("small[id=reqName]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese nombre");
            });

        cy.get("small[id=reqPaternal]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese apellido paterno");
            });

        cy.get("small[id=reqMaternal]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" Ingrese apellido materno");
            });
        cy.get('input[formControlName="apMaterno"]').click({ force: true });
        cy.get("small[id=reqEmail]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese un email");
            });

        cy.get('input[formControlName="telef"]')
            .click({ force: true })
            .type("123");

        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .type("123");

        cy.get("#minTelefono")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño minimo de 9 caracteres");
            });

        cy.get("#invalidEmail")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "El correo electrónico debe ser una dirección de correo electrónico válida"
                );
            });

        cy.get('button[type="submit"]').should("be.disabled");
    });
    //Generador de cadena random
    const generateRandomString = (num) => {
        const characters =
            "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        let result1 = Math.random().toString(36).substring(0, num);

        return result1;
    };
});
