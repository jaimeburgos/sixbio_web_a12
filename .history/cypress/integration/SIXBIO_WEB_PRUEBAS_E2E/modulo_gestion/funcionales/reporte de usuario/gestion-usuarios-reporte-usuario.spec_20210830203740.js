/// <reference types="cypress" />

context("Pruebas Funcionales Reporte Usuario", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const url = Cypress.env("url");
    //    const userReset = Cypress.env("userReset");
    //    const passReset = Cypress.env("passReset");

    beforeEach(() => {


        cy.intercept('GET', url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador", []);
        cy.intercept('GET', url + "SIXBIO-webcore-Nova/sixsca/listarusuarios", []);

        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
        }).as("listarusuarios");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);
    });

    it("Generar reporte en pdf o Excel (Excel)", () => {
        cy.wait(1000);

        cy.get("button").contains("Excel").click({ force: true });

        cy.get("#toast-container", {timeout: 2000})
            .find("div")
            .find("div")
            .contains("reporte")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Generando reporte, espere un momento");
            });
    });

    it("Generar reporte en pdf o Excel (PDF)", () => {
        cy.wait(1000);

        cy.get("button").contains("PDF").click({ force: true });

        cy.get("#toast-container", {timeout: 2000})
            .find("div")
            .find("div")
            .contains("reporte")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Generando reporte, espere un momento");
            });
    });


    it("Generar reporte en pdf o Excel, no hay datos para la consulta", () => {
        cy.get('input[formControlName="filterUsuario"]')
        .click({ force: true })
        .type("ssssssssssssssssssss", { delay: 100 })
        .should("have.value", "ssssssssssssssssssss");

        cy.get("button").contains("Excel").click({ force: true });

       cy.get("#toast-container", {timeout: 2000})
            .find("div")
            .find("div")
            .contains("DATOS")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("NO HAY DATOS PARA LA CONSULTA");
            });

    });
});
