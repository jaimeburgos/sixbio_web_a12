/// <reference types="cypress" />

context("Pruebas Funcionales Crear Usuario", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const email = Cypress.env("email");
    const url = Cypress.env("url");
    const fechaNacimientoUsuario=Cypress.env("fechaNacimientoUsuario");
    beforeEach(() => {
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            []
        );

        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
        }).as("listarusuarios");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);
    });

    it("Pantalla Crear Usuario", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.url()
            .should("include", "/home/gestion-usuarios/crear-usuario")
            .wait(1000);

        cy.get("label[for='inputUsuario']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Usuario (*)");
            });

        cy.get("label[for='inputNombre']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nombre (*)");
            });

        cy.get("label[for='inputApellidoPaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Paterno (*)");
            });

        cy.get("label[for='inputApellidoMaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Materno (*)");
            });

        cy.get("label[for='inputTelefono']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Teléfono");
            });

        cy.get("label[for='inputEstado']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Estado");
            });

        cy.get("label[for='inputNacimiento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha de Nacimiento");
            });

        cy.get("label[for='inputTipoDocumento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Tipo de documento (*)");
            });

        cy.get("label[for='inputNroDocumento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Número de documento(*)");
            });

        cy.get("label[for='inputCorreo']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Correo (*)");
            });

        cy.get("label[for='inputRolAgrupador']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Rol Agrupador (*)");
            });

 
        cy.get("label[for=inputUsuario]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Usuario (*)");
            });

        cy.get("label[for=inputNombre]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Nombre (*)");
            });

        cy.get("label[for=inputApellidoPaterno]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Apellido Paterno (*)");
            });

        cy.get("label[for=inputApellidoMaterno]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Apellido Materno (*)");
            });

        cy.get("label[for=inputTelefono]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Teléfono");
            });

        cy.get("label[for=inputEstado]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" Estado");
            });

        cy.get("label[for=inputNacimiento]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" Fecha de Nacimiento");
            });

        cy.get("label[for=inputTipoDocumento]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tipo de documento (*)");
            });

        cy.get("label[for=inputNroDocumento]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Número de documento(*)");
            });

        cy.get("label[for=inputCorreo]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Correo (*)");
            });

        cy.get("label[for=inputRolAgrupador]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Rol Agrupador (*)");
            });

        cy.get("#inputUser")
            .invoke("attr", "placeholder")
            .should("contain", "Usuario");

        cy.get("#inputNombre")
            .invoke("attr", "placeholder")
            .should("contain", "Nombre");

        cy.get("#inputApellidoPaterno")
            .invoke("attr", "placeholder")
            .should("contain", "Apellido Paterno");

        cy.get("#inputApellidoMaterno")
            .invoke("attr", "placeholder")
            .should("contain", "Apellido Materno");

        cy.get("#inputTelefono")
            .invoke("attr", "placeholder")
            .should("contain", "Teléfono");

        cy.get("#inputTelefono")
            .invoke("attr", "placeholder")
            .should("contain", "Teléfono");

        cy.get("#inputNroDocumento")
            .invoke("attr", "placeholder")
            .should("contain", "Número de documento");

        cy.get("#inputCorreo")
            .invoke("attr", "placeholder")
            .should("contain", "email@example.com");

        cy.get("#inputRolAgrupador")
            .invoke("text")
            .then((text) => {
                expect(text).to.contain("Administrador", "Verificador");
            });
       

        cy.wait(1000);

        cy.get('button[name="submit"]')
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Guardar");
            });

        cy.get('button[name="cancel"]')
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Cancelar ");
            });
    });

    it("Crear Usuario - Ok", () => {
        // Realizamos click en el boton Nuevo
        cy.get("button").find("a").contains("Nuevo").click({ force: true });
        // Esperamos un 1 seg
        cy.wait(1000);
        // Generamos nuestro usuario Random
        const userRandom = generateRandomString(11).replace(".", "");
        // Escribimos nuestro usuario Random
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userRandom);
        // Escribimos nuestro nombre Random

        cy.get('input[formControlName="nombre"]')
            .click({ force: true })
            .type(generateRandomString(11));
        // Escribimos nuestro Apellido Paterno Random

        cy.get('input[formControlName="apPaterno"]')
            .click({ force: true })
            .type(generateRandomString(11));
        // Escribimos nuestro Apellido Materno Random

        cy.get('input[formControlName="apMaterno"]')
            .click({ force: true })
            .type(generateRandomString(11));
        // Seleccionamos nuestro tipo de documento en este caso DNI

        cy.get('select[formControlName="tipoDoc"]').select("DNI", {
            force: true,
        });
        // Escribimos nuestro correo desde la variable global "email"
        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .type(email);
        // Escribimos nuestro DNI
        cy.get('input[formControlName="numDoc"]')
            .click({ force: true })
            .type("12345678");
        // Seleccionamos una fecha de nacimiento
         var fecha = new Date();
         var year = fecha.getFullYear();        
         cy.get('input[formControlName="fecNac"]').click({
             force: true,
         });     
         cy.get('button[class="current"]').contains(year).click({ force: true });
         // Obtenemos de la variable fechaNacimientoCrearUsuario año el valor que asignamos en cypress.json ( es una variable global )
         cy.get("td[class='ng-star-inserted']")
             .contains(fechaNacimientoUsuario.año)
             .click({ force: true });
         // Obtenemos de la variable fechaNacimientoCrearUsuario nombreMes el valor que asignamos en cypress.json ( es una variable global )
         cy.get("td[class='ng-star-inserted']")
             .contains(fechaNacimientoUsuario.nombreMes)
             .click({ force: true });
         // Obtenemos de la variable fechaNacimientoCrearUsuario numDia el valor que asignamos en cypress.json ( es una variable global )
         cy.get("span.ng-star-inserted")
             .contains(fechaNacimientoUsuario.numDia)
             .click({ force: true });
         cy.get("span.ng-star-inserted")
             .contains(fechaNacimientoUsuario.numDia)
             .click({ force: true });
        // Realizamos click en guardar
        cy.get('button[name="submit"]').click({ force: true });
        // Verificamos el toast de creacion satisfactoriamente
        cy.get("#toast-container", { timeout: 5000 })
            .find("div")
            .find("div")
            .contains("satisfactoriamente")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "El Usuario: " +
                        userRandom +
                        " fue creado satisfactoriamente"
                );
            });
    });

    it("Crear Usuario - Cancelar", () => {
        // Realizamos click en el boton nuevo
        cy.get("button").find("a").contains("Nuevo").click({ force: true });
        // Esperamos 1 seg
        cy.wait(1000);
        // Generamos nuestro usuario Random
        const userRandom = generateRandomString(11).replace(".", "");
        // Escribimos el usuario en el campo usuario
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userRandom);
        // Escribimos un nombre Random
        cy.get('input[formControlName="nombre"]')
            .click({ force: true })
            .type(generateRandomString(11));
        // Escribimos un apellido parterno  Random
        cy.get('input[formControlName="apPaterno"]')
            .click({ force: true })
            .type(generateRandomString(11));
        // Escribimos un apellido materno Random
        cy.get('input[formControlName="apMaterno"]')
            .click({ force: true })
            .type(generateRandomString(11));
        // Seleccionamos el tipo de documento en este caso DNI
        cy.get('select[formControlName="tipoDoc"]').select("DNI", {
            force: true,
        });
        // Escribimos un email
        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .type(email);
        // Escribimos el numero de documento
        cy.get('input[formControlName="numDoc"]')
            .click({ force: true })
            .type("12345678");
        // Seleccionamos una fecha de nacimiento
        cy.get('input[formControlName="fecNac"]').click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });
        // Realizamos click en el boton cancelar
        cy.get('button[name="cancel"]').click({ force: true });
        // Verificamos que retornamos a la ruta  y pantalla de /home/gestion-usuarios
        cy.url().should("include", "/home/gestion-usuarios");
    });

    it("Crear Usuario, usuario repetido", () => {
        cy.get("button").find("a").contains("Nuevo").click({ force: true });

        cy.wait(1000);

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin);

        cy.get('input[formControlName="nombre"]')
            .click({ force: true })
            .type(generateRandomString(11));

        cy.get('input[formControlName="apPaterno"]')
            .click({ force: true })
            .type(generateRandomString(11));

        cy.get('input[formControlName="apMaterno"]')
            .click({ force: true })
            .type(generateRandomString(11));

        cy.get('select[formControlName="tipoDoc"]').select("DNI", {
            force: true,
        });

        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .type("jburgos@novatronic.com");

        cy.get('input[formControlName="numDoc"]')
            .click({ force: true })
            .type("12345678");

        cy.get('input[formControlName="fecNac"]').click({ force: true });
        cy.get("span.ng-star-inserted").contains("5").click({ force: true });
        cy.get("span.ng-star-inserted").contains("5").click({ force: true });

        cy.get('button[name="submit"]').click({ force: true });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("usuario")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("No se pudo registrar el usuario");
            });
    });

    it("Crear Usuario, campos inválidos y campos vacios", () => {
        //Realizamos click en el boton Nuevo
        cy.get("button").find("a").contains("Nuevo").click({ force: true });
        cy.wait(1000);
        // Realizamos click en el input usuario
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        // Realizamos click en el input nombre
        cy.get('input[formControlName="nombre"]')
            .click({ force: true })
            .clear();
        // Realizamos click en el input Apellido Paterno
        cy.get('input[formControlName="apPaterno"]')
            .click({ force: true })
            .clear();
        // Realizamos click en el input Apellido Materno
        cy.get('input[formControlName="apMaterno"]')
            .click({ force: true })
            .clear();
        //Realizamos click en el input Telefono
        cy.get('input[formControlName="telef"]').click({ force: true }).clear();
        //Realizamos click en el input Fecha nacimiento
        cy.get('input[formControlName="fecNac"]').click({ force: true });
        cy.get('input[formControlName="fecNac"]').click({ force: true });

        // Realizamos click en Tipo Documento y lo dejamos en Seleccione...
        cy.get('select[formControlName="tipoDoc"]').select("Seleccione...", {
            force: true,
        });
        // Realizamos click en el input Numero Documento
        cy.get('input[formControlName="numDoc"]').click({ force: true });
        // Realizamos click en el input Correo
        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .clear();
        // Realizamos click en el input usuario
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();

        // Verificamos los mensajes que nos indican que debemos ingresar datos debajo de sus input correspondientes
        cy.get("small[id=reqUser]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese usuario");
            });

        cy.get("small[id=reqName]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese nombre");
            });

        cy.get("small[id=reqPaternal]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese apellido paterno");
            });

        cy.get("small[id=reqMaternal]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" Ingrese apellido materno");
            });

        cy.get("small[id=reqDate]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese fecha de nacimiento");
            });

        cy.get("small[id=reqEmail]")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Ingrese un email");
            });

            // Ingresamos de manera incompleta los datos debajo la longitud minima que requieren
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type("123");

        cy.get('input[formControlName="telef"]')
            .click({ force: true })
            .type("123");

        cy.get('select[formControlName="tipoDoc"]').select("DNI", {
            force: true,
        });
        cy.get('input[formControlName="numDoc"]')
            .click({ force: true })
            .type("123");

        cy.get('input[formControlName="correo"]')
            .click({ force: true })
            .type("123");

        cy.get('input[formControlName="numDoc"]').click({ force: true });

        // Verificamos mensajes de tamaño minimo 
        cy.get("#minUser")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño mínimo de 5 caracteres");
            });

        cy.get("#minTelefono")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("Tamaño minimo de 9 caracteres");
            });

        cy.get("#minNDoc")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(" El tamaño minimo de caracteres es 8");
            });

        cy.get("#invalidEmail")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal(
                    "El correo electrónico debe ser una dirección de correo electrónico válida"
                );
            });
            // Verificamos el boton guardar se encuentre deshabilitado dado que es un reactiveForm hasta que se cumplen la validaciones no se habilitara
        cy.get('button[name="submit"]').should("be.disabled");
    });
        //Generador de cadena random
        const generateRandomString = (num) => {
            const characters =
                "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            let result1 = Math.random().toString(36).substring(0, num);
    
            return result1;
        };
});
