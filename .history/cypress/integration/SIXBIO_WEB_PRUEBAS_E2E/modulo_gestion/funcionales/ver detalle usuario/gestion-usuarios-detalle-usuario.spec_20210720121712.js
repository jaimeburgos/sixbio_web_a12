/// <reference types="cypress" />

context("Pruebas Funcionales Detalle Usuario", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const url = Cypress.env("url");
    //    const userReset = Cypress.env("userReset");
    //    const passReset = Cypress.env("passReset");

    beforeEach(() => {
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            []
        );

        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
        }).as("listarusuarios");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);
    });

    it("Ver Detalle Usuario - Ok", () => {
        cy.get('button[title="Ver detalle"]').first().click({ force: true });
        cy.wait(1000);
        cy.url().should("include", "/home/gestion-usuarios/detalle-usuario");

        cy.get("label[for='inputUsuario']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Usuario (*)");
            });

        cy.get("label[for='inputNombre']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Nombre (*)");
            });

        cy.get("label[for='inputApellidoPaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Paterno (*)");
            });

        cy.get("label[for='inputApellidoMaterno']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Apellido Materno (*)");
            });

        cy.get("label[for='inputTelefono']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Teléfono");
            });

        cy.get("label[for='inputEstados']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Estado (*)");
            });

        cy.get("label[for='inputFechaNacimiento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Fecha de Nacimiento");
            });

        cy.get("label[for='inputTipoDocumentos']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Tipo de documento (*)");
            });

        cy.get("label[for='inputNroDocumento']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Número de documento(*)");
            });

        cy.get("label[for='inputCorreo']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Correo (*)");
            });

        cy.get("label[for='inputRolAgrupador']")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Rol Agrupador (*)");
            });

        cy.get("button[type=button]")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Regresar");
            });

        cy.wait(1000);
        cy.get("#inputUsuario").should("be.disabled");
        cy.get("#inputNombre").should("be.disabled");
        cy.get("#inputApellidoPaterno").should("be.disabled");
        cy.get("#inputApellidoMaterno").should("be.disabled");
        cy.get("#inputTelefono").should("be.disabled");
        cy.get("#inputEstados").should("be.disabled");
        cy.get("input[name=inputFechaNacimiento]").should("be.disabled");
        cy.get("#inputTipoDocumentos").should("be.disabled");
        cy.get("#inputNroDocumento").should("be.disabled");
        cy.get("#inputCorreo").should("be.disabled");
        cy.get("#inputRolAgrupador").should("be.disabled");
    });
});
