/// <reference types="cypress" />

context("Pruebas Rutas SIXBIO WEB", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const userReset = Cypress.env("userReset");
    const passReset = Cypress.env("passReset");
    const userVer = Cypress.env("userVer");
    const passVer = Cypress.env("passVer");
    const url = Cypress.env("url");

    beforeEach(() => {
        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();
    });

    it.only("Verificamos seguridad de la ruta con usuario Admin", () => {
        //Ingresamos nuestras credenciales de usuario
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);
        //Realizamos click en el boton entrar
        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");
         cy.wait("@listarrolesagrupador");
        //Verificamos la entrar a la pantalla gestion de usuarios
        cy.url().should("include", "/home/gestion-usuarios");
        // Intentamos entrar apartado de verificacionBiometrica que pertenece solo al Rol Usuario Verificador
        cy.visit(url+'#/home/verificacionBiometrica');
        // Se volvera a refrescar la pagina cargando otra vez el servicio
        cy.wait("@listarrolesagrupador");
        // Verificamos que nos encontremos aun en gestion usuarios
        cy.url().should("include", "/home/gestion-usuarios");
 
        // Verificamos en caso de entrar directamente editar usuario nos redirige a home/gestion-usuarios
        cy.visit(url+'#/home/gestion-usuarios/editar-usuario');
        // Se volvera a refrescar la pagina cargando otra vez el servicio
        cy.wait("@listarrolesagrupador");
        // Verificamos que nos encontremos aun en gestion usuarios
        cy.url().should("include", "/home/gestion-usuarios");

        // Verificamos en caso de entrar directamente detalle usuario nos redirige a home/gestion-usuarios
        cy.visit(url+'#/home/gestion-usuarios/editar-usuario');
        // Se volvera a refrescar la pagina cargando otra vez el servicio
        cy.wait("@listarrolesagrupador");
        // Verificamos que nos encontremos aun en gestion usuarios
        cy.url().should("include", "/home/gestion-usuarios");
    });


    it("Verificamos seguridad de la ruta con usuario Verificador", () => {
        //Ingresamos nuestras credenciales de usuario
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userVer)
            .should("have.value", userVer);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passVer)
            .should("have.value", passVer);
        //Realizamos click en el boton entrar
        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept({
            method: "POST",
            url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
        }).as("listarrolesagrupador");
        cy.wait("@listarrolesagrupador");
        //Verificamos la entrar a la pantalla verficiacion verificacionBiometrica
        cy.url().should("include", "/home/verificacionBiometrica");
        // Intentamos entrar apartado de gestion usuarios que pertenece solo al Rol Usuario Admin
        cy.visit(url+'#/home/gestion-usuarios');
        // Se volvera a refrescar la pagina cargando otra vez el servicio
        cy.wait(500);
        // Verificamos que nos encontremos aun en  login
        cy.url().should("include", "login");

        //Verificamos seguridad en ruta crear usuario
        cy.visit(url+'#/home/gestion-usuarios/crear-usuario');
        // Se volvera a refrescar la pagina cargando otra vez el servicio
        cy.wait(500);
        // Verificamos que nos encontremos aun en  login
        cy.url().should("include", "login");

        //Verificamos seguridad en ruta editar usuario
        cy.visit(url+'#/home/gestion-usuarios/editar-usuario');
        // Se volvera a refrescar la pagina cargando otra vez el servicio
        cy.wait(500);
        // Verificamos que nos encontremos aun en  login
        cy.url().should("include", "login");

        //Verificamos seguridad en ruta detalle usuario
        cy.visit(url+'#/home/gestion-usuarios/detalle-usuario');
        // Se volvera a refrescar la pagina cargando otra vez el servicio
        cy.wait(500);
        // Verificamos que nos encontremos aun en  login
        cy.url().should("include", "login");
    });


});
