/// <reference types="cypress" />

context("Pruebas consultar-validaciones", () => {
    const userAdmin = Cypress.env("userAdmin");
    const passAdmin = Cypress.env("passAdmin");
    const userSinData = Cypress.env("userSinData");
    const userData = Cypress.env("userData");
    const url = Cypress.env("url");

    beforeEach(() => {
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/sixbio/consultavalidaciones",
            []
        );
        cy.intercept(
            "GET",
            url + "SIXBIO-webcore-Nova/service/reportevalidaciones",
            []
        );

        cy.visit(url);
        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .clear();
        cy.get('input[formControlName="pass"]').click({ force: true }).clear();

        cy.get('input[formControlName="usuario"]')
            .click({ force: true })
            .type(userAdmin)
            .should("have.value", userAdmin);

        cy.get('[formControlName="pass"]')
            .click({ force: true })
            .type(passAdmin)
            .should("have.value", passAdmin);

        cy.get('button[name="submit"]').click({ force: true });

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarrolesagrupador",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarrolesagrupador");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarusuarios");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixbio/consultavalidaciones",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("consultavalidaciones");

        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/service/reportevalidaciones",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("reportevalidaciones");

        cy.wait(["@listarrolesagrupador", "@listarusuarios"]);

        cy.get("a").contains("Módulo de Consultas").click({ force: true });
        cy.wait(500);
        cy.intercept(
            {
                method: "POST",
                url: url + "SIXBIO-webcore-Nova/sixsca/listarusuarios",
            },
            (req) => delete req.headers["if-none-match"]
        ).as("listarusuarios2");

        cy.get("a").contains("Consultar Validaciones").click({ force: true });
        cy.wait("@listarusuarios2");
    });

    it("Verificamos el mensaje Toast cuando una consulta de usuario no posee datos con filtro cantidad", () => {
        // Seleccionamos un usuario sin data
        cy.get('select[formControlName="filterUsuario"]').select(userSinData, {
            force: true,
        });
        // Ingresamos una cantidad en el filtro cantidad
        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("9999");
        // Realizamos click en la busqueda
        cy.get('button[type="submit"]').click({ force: true });
        // Validamos el contenido de nuestro Toast de Error
        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("CONSULTA")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("NO HAY DATOS PARA LA CONSULTA");
            });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });
    });

    it("Verificamos el mensaje Toast cuando una consulta de usuario no posee datos con filtro fechas", () => {
        //Realizamos click en el check de filtro por fechas
        cy.get("#defaultUnchecked").click({ force: true });
        // Seleccionamos un usuario sin data
        cy.get('select[formControlName="filterUsuario"]').select(userSinData, {
            force: true,
        });

        // Seleccionamos fecha Desde y Hasta
        var fecha = new Date();
        var year = fecha.getFullYear();
        cy.get('input[formControlName="filterfechaDesde"]').click({
            force: true,
        });
        cy.get('button[class="current"]').contains(year).click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains(year - 7)
            .click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains("enero")
            .click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });

        cy.get('input[formControlName="filterfechaHasta"]').click({
            force: true,
        });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });
        // Realizamos click en el boton busqueda
        cy.get('button[type="submit"]').click({ force: true });
        // Verificamos el contenido de nuestro toast de Error
        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("CONSULTA")
            .invoke("text")
            .then((text) => {
                expect(text).to.equal("NO HAY DATOS PARA LA CONSULTA");
            });

        cy.get("#toast-container")
            .find("div")
            .find("div")
            .contains("Error")
            .invoke("text")
            .then((text) => {
                expect(text.trim()).to.equal("Error");
            });
    });

    it("Verificamos cambios en la grilla despues una busqueda con cantidad", () => {
        //Realizamos click en el check de filtro por fechas

        cy.get("#defaultUnchecked").click({ force: true });
        // Seleccionamos un usuario con data
        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });
        // Seleccionamos fecha Desde y Hasta

        var fecha = new Date();
        var year = fecha.getFullYear();
        cy.get('input[formControlName="filterfechaDesde"]').click({
            force: true,
        });
        cy.get('button[class="current"]').contains(year).click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains(year - 7)
            .click({ force: true });
        cy.get("td[class='ng-star-inserted']")
            .contains("enero")
            .click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });
        cy.get("span.ng-star-inserted").contains("7").click({ force: true });

        cy.get('input[formControlName="filterfechaHasta"]').click({
            force: true,
        });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });
        cy.get("span.ng-star-inserted").contains("1").click({ force: true });
        // Realizamos click en el boton busqueda
        cy.get('button[type="submit"]').click({ force: true });
        //Esperamos que cargue la busqueda realizada
        cy.wait("@consultavalidaciones");
        // En la grilla de tamaño de pagina seleccionamos 5
        cy.get("select[id=inputSizePagination]").select("5", {
            force: true,
        });
        // Esperamos cambie la cantidad
        cy.wait("@consultavalidaciones");
        // En la grilla de tamaño de pagina seleccionamos 10
        cy.get("select[id=inputSizePagination]").select("10", {
            force: true,
        });
        // Esperamos cambie la cantidad
        cy.wait("@consultavalidaciones");
        // En la grilla de tamaño de pagina seleccionamos 15
        cy.get("select[id=inputSizePagination]").select("15", {
            force: true,
        });
        // Esperamos cambie la cantidad
        cy.wait("@consultavalidaciones");
        // En la grilla de tamaño de pagina seleccionamos 20
        cy.get("select[id=inputSizePagination]").select("20", {
            force: true,
        });
    });

    it("Verificamos cambios en la grilla despues una busqueda con fecha", () => {
        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });

        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("444");

        cy.get('button[type="submit"]').click({ force: true });

        cy.wait("@consultavalidaciones");

        cy.get("select[id=inputSizePagination]").select("5", {
            force: true,
        });

        cy.wait("@consultavalidaciones");

        cy.get("select[id=inputSizePagination]").select("10", {
            force: true,
        });

        cy.wait("@consultavalidaciones");

        cy.get("select[id=inputSizePagination]").select("15", {
            force: true,
        });
        cy.wait("@consultavalidaciones");

        cy.get("select[id=inputSizePagination]").select("20", {
            force: true,
        });
    });

    it("Verificamos funcionamiento boton regresar desde detalle-validacion ", () => {
        cy.get('select[formControlName="filterUsuario"]').select(userData, {
            force: true,
        });

        cy.get('input[formControlName="cantidad"]')
            .click({ force: true })
            .type("444");
        cy.get('button[type="submit"]').click({ force: true });

        cy.wait("@consultavalidaciones");
        cy.get('button[title="Ver detalle"]').first().click({ force: true });

        cy.url().should(
            "include",
            "/home/consultar-validaciones/detalle-validacion"
        );

        cy.get("button").contains("Regresar").click({ force: true });

        cy.wait(["@consultavalidaciones", "@listarusuarios"]);

        cy.url().should("include", "/home/consultar-validaciones");
    });
});
